package com.cit.sellfan.business;

import java.awt.Color;

public class HColor {
	public static final String HGiallo = String.format("%02x%02x%02x", Color.YELLOW.getRed(), Color.YELLOW.getGreen(), Color.YELLOW.getBlue());
    
	public static final void setComponentBackground(String componentID, String hColor) {
    	String comando = "$('#" + componentID + "').css('background', '#" + hColor + "');";
    	com.vaadin.ui.JavaScript.getCurrent().execute(comando);
    }
}
