package com.cit.sellfan.business;

import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Principal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import com.cit.sellfan.ui.MainView;
import com.cit.sellfan.ui.pannelli.pannelloSelezioneBox;
import com.cit.sellfan.ui.pannelli.pannelloSelezioneMotore;
import com.cit.sellfan.ui.pannelli.pannelloSelezioneSilenziatore;
import com.cit.sellfan.ui.template.jDisplayImageWeb;
import com.cit.sellfan.ui.view.AccessoriImageView;
import com.cit.sellfan.ui.view.CatalogoView;
import com.cit.sellfan.ui.view.CompareView;
import com.cit.sellfan.ui.view.DimensionView;
import com.cit.sellfan.ui.view.MultigiriView;
import com.cit.sellfan.ui.view.PrestazioniView;
import com.cit.sellfan.ui.view.PreventiviView;
import com.cit.sellfan.ui.view.SearchView;
import com.cit.sellfan.ui.view.SetupView;
import com.cit.sellfan.ui.view.SpettroSonoroView;
import com.cit.sellfan.ui.view.cliente04.Cliente04GestioneSilenziatori;
import com.cit.sellfan.ui.view.cliente04.assiali.Cliente04AccessoriViewAssiali;
import com.cit.sellfan.ui.view.cliente04.centrifighi.Cliente04AccessoriViewCentrifughi;
import com.cit.sellfan.ui.view.cliente04.centrifighi.Cliente04AccessoriViewFusione;
import com.cit.sellfan.ui.view.mobile.MobileCatalogoView;
import com.cit.sellfan.ui.view.mobile.MobileDimensionView;
import com.cit.sellfan.ui.view.mobile.MobilePrestazioniView;
import com.cit.sellfan.ui.view.mobile.MobileSearchView;
import com.cit.sellfan.ui.view.preventivi.PreventiviVentilatori;
import com.vaadin.data.Item;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinService;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.UI;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.classi.CitFont;
import cit.classi.Clientev21;
import cit.classi.Lingua;
import cit.classi.Paths;
import cit.myistat.UtilityIstat;
import cit.myjavalib.UtilityMath;
import cit.myjavalib.UtilityFisica.Gas;
import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.myjavalib.UtilityFisica.UtilityFisica;
import cit.myjavalib.UtilityFisica.UtilityUnitaMisura;
import cit.saas.SaaSUser;
import cit.sellfan.Costanti;
import cit.sellfan.classi.AccessoriGruppo;
import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.AssiGrafici;
import cit.sellfan.classi.Box;
import cit.sellfan.classi.CondizioniAmbientali;
import cit.sellfan.classi.DBTableQualifier;
import cit.sellfan.classi.ERP327Build;
import cit.sellfan.classi.GestioneAccessori;
import cit.sellfan.classi.GruppoSelezioneSerie;
import cit.sellfan.classi.KitAccessori;
import cit.sellfan.classi.Materiale;
import cit.sellfan.classi.Motore;
import cit.sellfan.classi.Multigiri;
import cit.sellfan.classi.NomiImmagini;
import cit.sellfan.classi.ParametriModelloCompleto;
import cit.sellfan.classi.ParametriVari;
import cit.sellfan.classi.Preventivo;
import cit.sellfan.classi.Ricerca;
//import cit.sellfan.classi.Ricerca;
import cit.sellfan.classi.SelezioneDati;
import cit.sellfan.classi.Serie;
import cit.sellfan.classi.Silenziatore;
import cit.sellfan.classi.StampaOfferta;
import cit.sellfan.classi.UMDefaultIndex;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.Utilizzatore;
import cit.sellfan.classi.Warning;
import cit.sellfan.classi.db.DBAccessori;
import cit.sellfan.classi.db.DBCRMv2;
import cit.sellfan.classi.db.DBParam;
import cit.sellfan.classi.db.DBPrivato;
import cit.sellfan.classi.db.DBTecnico;
import cit.sellfan.classi.db.LoadClassFromRs;
import cit.sellfan.classi.grafici.GraficoPrestazioniMultigiri;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.classi.ventilatore.VentilatoreFisicaParameter;
import cit.sellfan.classi.ventilatore.VentilatorePreventivo;
import cit.sellfan.classi.ventilatore.VentilatoreXSelezione;
import cit.sellfan.classi.ventilatore.VentilatoriSelezione;
import cit.sellfan.classi.ventilatore.VentilatoriSort;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import cit.sellfan.regolamento327.regolamento327Algoritmo;
//import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
//import cit.sellfan.personalizzazioni.cliente04.*;
import cit.sellfan.regolamento327.regolamento327Dati;
import cit.sellfan.regolamento327.regolamento327Target;
import cit.utility.NumeriRomani;
import cit.utility.Utility;
import cit.utility.UtilitySql;
import cit.utility.UtilityTraduzioni;
import com.cit.sellfan.ui.view.cliente04.Cliente04AccessoriViewDonaldson;
import de.steinwedel.messagebox.MessageBox;
import java.util.List;
import cit.sellfan.classi.PresetRicerca;
import cit.sellfan.personalizzazioni.cliente04.UtilityClienteMoro;
import java.util.Arrays;
import org.apache.commons.collections4.ListUtils;

public class VariabiliGlobali {
    public boolean testVersion = true;
    public boolean debugFlag = false;
    public boolean HideInox302 = true;
    public boolean HideInox304 = true;
    public boolean forcedDesktop = false;
    public String dataVersione;
    public Principal principal;
    public Map<String, String[]> parametri;
    public Enumeration<String> attributi;
    public String remoteUser;
    public boolean menuStyle = false;
    public VariabiliGlobaliCliente variabiliGlobaliCliente;
    public final int minBrowserWidth = 1000;
    public final int minBrowserHeight = 700;
    public int browserWidth;
    public int browserHeight;
    public double browserAspectRatio;
    public String browserDescription;
    public boolean LayoutForced = false;
    public boolean MobileMode = false;
    public String OSDescription = "";
    public TraceUserWeb traceUser = new TraceUserWeb();

//modifiche predefiniti
    public boolean searchIsCentrifughi = true;
    public boolean searchIsDiretto = true;

//correzioni bug
    public boolean dataConsegnaInserita = false;
    public boolean ricercaTotale = true;
    public boolean ricercaAspirazione = false;
    public String catalogoCondizioniTemperatura = "20";
    public String catalogoCondizioniAltezza = "0";
    public String catalogoCondizioniFrequenza = "50";
    public Integer catalogoCondizioniUmidita = 0;
    public boolean editVentola = false;
    private final PreventiviVentilatori ventilatoriView = new PreventiviVentilatori();

//aggiornamenti
    public final int MainViewIndex = 0;
    public final int SetupViewIndex = 1;
    public final int CatalogoViewIndex = 2;
    public final int SearchViewIndex = 3;
    public final int PreventiviViewIndex = 4;
    public final int CompareViewIndex = 5;
    public final int RiepilogoViewIndex = 6;
    public final int PrestazioniViewIndex = 7;
    public final int Ingombro2ImgViewIndex = 8;
    public final int DimensioniAngoloViewIndex = 9;
    public final int DimensioniViewIndex = 10;
    public final int AccessoriViewIndex = 11;
    public final int MultigiriViewIndex = 12;
    public final int SpettroSonoroViewIndex = 13;
    public final int MaxViewIndex = this.SpettroSonoroViewIndex + 1;
    public XdevView ElencoView[] = new XdevView[this.MaxViewIndex];
    public XdevView ElencoMobileView[] = new XdevView[this.MaxViewIndex];
    public int CurrentViewIndex = this.MainViewIndex;
    public boolean ventilatoreCambiato = false;
    public Paths Paths = new Paths();
    public ParametriVari ParametriVari = null;//new ParametriVari();
    public ParametriVariHTML ParametriVariHTML = null;//new ParametriVariHTML();
    public NomiImmagini nomiImmagini = new NomiImmagini();
    public boolean mzNascosta = false;
    public Silenziatore silenziatoreMandata;
    public Silenziatore silenziatoreAspirazione;
    public PresetRicerca presetRicerca = null;

    //view
    public MainView MainView;
//*****view
    public int progressivoGenerico = 0;
    public StreamResource logostreamresource;
    public String Error;
    public String Ambiente;
    public String AmbientiDisponibili[];
    public String serverName;
    public String serverUser;
    public String serverPassword;
    public String serverurl;
    public Connection conPrincipale = null;
    public Connection conTecnica = null;
    public Connection conPrivata = null;
    public Connection conTranscodifica = null;
    public Connection conCRMv2 = null;
    public Connection conISTAT = null;
    public Connection conConfig = null;
    public SaaSUser currentUserSaaS;
    public int currentLivelloUtente = Costanti.utenteIndefinito;
    public int idClienteIndexVG;
    public int idSottoClienteIndexVG;
    public int idTranscodificaIndex = -1;
    public ArrayList<Warning> listaW = new ArrayList<>();
//utility
    public Utility utility = new Utility();
    public UtilityFisica utilityFisica = new UtilityFisica();
    public DBTecnico dbTecnico = new DBTecnico();
    public DBCRMv2 dbCRMv2 = new DBCRMv2();
    public DBAccessori dbAccessori;
    public DBPrivato dbPrivato;
    public DBParam dbParam;
    public DBConfigHTML dbConfig = new DBConfigHTML();
    public UtilityIstat utilityIstat;
//variabile ambientali
//condizioni anbientali, densita' fluido
    public CondizioniAmbientali CACatalogo = null;
    public CondizioniAmbientali CA020Catalogo = null;
    public CondizioniAmbientali CARicerca = null;
    public CondizioniAmbientali CA020Ricerca = null;
    public CondizioniAmbientali CAFree = null;
    //****variabile ambientali
    public boolean catalogoAutoSerieFlag = true;
    public LoadClassFromRs loadClassFromRs = new LoadClassFromRs();
    public boolean trasmissioneFlag = false;
    public Multigiri multigiri = new Multigiri();
//
    public ArrayList<Serie> ElencoSerie = null;
    public ArrayList<Boolean> ElencoSerieDisponibili = null;
    public GruppoSelezioneSerie ElencoGruppiSelezioneSerie[] = new GruppoSelezioneSerie[4];
    public ArrayList<KitAccessori> ElencoKitAccessori = new ArrayList<KitAccessori>();

    public ArrayList<Materiale> ElencoMateriali = new ArrayList<>();
    public ArrayList<Gas> ElencoGas = new ArrayList<>();
    public ArrayList<Ventilatore> ElencoVentilatoriFromCatalogo = new ArrayList<>();
    public ArrayList<Ventilatore> ElencoVentilatoriFromRicerca = new ArrayList<>();
    public ArrayList<Ventilatore> ElencoVentilatoriFromPreventivo = new ArrayList<>();
    public ArrayList<Ventilatore> ElencoVentilatoriFromCompare = new ArrayList<>();
    public ArrayList<Ventilatore> ElencoVentilatoriVisualizzati = null;
    public Ventilatore VentilatoreCurrent = null;
    public VentilatoreConfronto ventilatoreConfronto = new VentilatoreConfronto();
    public int VentilatoreCurrentIndex = -1;
    public VentilatoreFisica ventilatoreFisicaCurrent = null;
    public VentilatoreFisicaParameter ventilatoreFisicaParameter = new VentilatoreFisicaParameter();
    /*
    public final int VentilatoreCurrentFromIndefinito = -1;
	public final int VentilatoreCurrentFromCatalogo = 0;
	public final int VentilatoreCurrentFromRicerca = 1;
	public final int VentilatoreCurrentFromPreventivo = 2;
     */
    public String nBoccheCanalizzateDesc[] = {"Bocca Aspirante Canalizzata", "Bocca Premente Canalizzata", "Entrambe le Bocche Canalizzate"};
    public int VentilatoreCurrentFromVieneDa = Costanti.vieneDaIndefinito;
    public ParametriModelloCompleto parametriModelloCompleto = new ParametriModelloCompleto();
    public UtilityMath utilityMath = new UtilityMath();
    public UtilityTraduzioni utilityTraduzioni = new UtilityTraduzioni();
    public UtilitySql utilitySql = new UtilitySql();
    public HashMap<String, String> mapEsecuzioniDesc = new HashMap<>();
//default in panello prestazioni
    public boolean scegliMotoreTRFlag = false;
    public boolean scegliMotoreDAFlag = true;
    public boolean PD2TestEnabledFlag = false;
    public int LimitiGiriSceltaPoli50Hz[] = {2250, 1250, 875};
    public int LimitiGiriSceltaPoli60Hz[] = {2700, 1500, 1050};
    public boolean fisicaPressioneInMandata = true;
    public boolean motoreDAPrezzoMotoreCompresoFlag = true;         //true=nel prezzo del ventilatore è compreso il motore
    public int temperatureLimite[] = {60, 100, 150, 200, 250, 300, 350, 400, 450, 500};
    public double percentualeRpmLimite[] = {0.96, 0.9, 0.84, 0.77, 0.7, 0.63, 0.55, 0.46, 0.34, 0 - 34};
    public boolean pannelloLimitiGiriTemperatureEspanse = true;
    public final int nTemperatureLimiteDefault = this.temperatureLimite.length;
    public int nTemperatureLimite = this.nTemperatureLimiteDefault;
    public UtilityUnitaMisura utilityUnitaMisura = new UtilityUnitaMisura();
//grafici
    public String sfondoGraficiPrestazioni;
    public String sfondoGraficiSpettro;
    //unità misura
    public UMDefaultIndex UMDefaultIndex;
    public UnitaMisura UMPortataCorrente = null;
    public UnitaMisura UMPressioneCorrente = null;
    public UnitaMisura UMPotenzaCorrente = null;
    public UnitaMisura UMAltezzaCorrente = null;
    public UnitaMisura UMDiametroCorrente = null;
    public UnitaMisura UMTemperaturaCorrente = null;
    public UnitaMisura umGirante = null;

    public NumberFormat fmtNd = NumberFormat.getInstance();
    public NumberFormat fmtNdTable = NumberFormat.getInstance();
    public int NDecimaliRumore = 2;
    public boolean pannelloPrestazioniMarkerOrizontalSpecialFlag = true;
    public boolean pannelloCatalogoForzaGiriMassimiFlag = false;
    public boolean pannelloPrestazionititoloCompletoFlag = true;
    //public double pannelloRicercaTolleranzaSelezioneMENO = 0.05;
    //public double pannelloRicercaTolleranzaSelezionePIU = 0.1;
    public boolean pannelloRicercaRpmMotoreDAFlag = false;
    public boolean pannelloRicercaTolleranzaPressioneFlag = true;
    public String pannelloRicercaEsecuzioniDA[] = null;
    public String pannelloRicercaEsecuzioniTR1[] = null;
    public String pannelloRicercaEsecuzioniTR2[] = null;
    public boolean immagineRedCrossFlag = false;
    public final int labelStyleEsplicite = 0;
    public final int labelStyleUNI = 1;
    public int labelStyle = this.labelStyleEsplicite;
    public CitFont currentCitFont;// = new CitFont();
    public ArrayList<Lingua> elencoLingue = new ArrayList<>();
    public Font currentFontSpecial = new Font("Arial Unicode MS", Font.PLAIN, 11);
    public Font currentFontNormal = new Font("Tahoma", Font.PLAIN, 11);
    public Font currentFontBoldSpecial = new Font("Arial Unicode MS", Font.BOLD, 11);
    public Font currentFontBoldNormal = new Font("Tahoma", Font.BOLD, 11);
    public GraficoPrestazioniMultigiri currentGraficoPrestazioniMultigiri = new GraficoPrestazioniMultigiri();
    public GraficoPrestazioniCurveWeb currentGraficoPrestazioniCurveWeb = null;
    public AssiGrafici assiGraficiDefault = null;
    public Accessorio accessorioInEsame = null;
    public GestioneAccessori gestioneAccessori = new GestioneAccessori();
    public String stringaDirettamenteAccoppiato = "Direttamente accoppiato";
    public String stringaTrasmissioneARinvio = "Trasmissione a Rinvio";
    public ArrayList<AccessoriGruppo> elencoGruppiAccessori = new ArrayList<>();
    public ParametriModelloCompleto parametriModelloCompletoForPrint = new ParametriModelloCompleto();
    public ParametriModelloCompleto parametriModelloCompletoForRicerca = new ParametriModelloCompleto();
//selezione/ricerca
    public Ricerca ricerca = new Ricerca();
    public VentilatoriSort ventilatoreSort = new VentilatoriSort();
    public int campoVentilatoreSort = VentilatoriSort.sortDefault;
    public boolean crescenteVentilatoreSort = true;
    public SelezioneDati SelezioneDati = null;
    public boolean pannelloRicercaMaggiorazioneDAGiranteFlag = false;
    public double pannelloRicercaMaggiorazioneDAGiranteMin = -0.05;
    public double pannelloRicercaMaggiorazioneDAGiranteMax = 0.05;
    public int pannelloRicercaMaggiorazioneDAGiranteNStep = 2000;
    public double pannelloRicercaDArpmMenoPercentuale = 0.0;
    public double pannelloRicercaDArpmPiuPercentuale = 0.0;
    public boolean pannelloRicercaAutoSceltaMotoreFlag = false;
    public boolean pannelloRicercaDArpmPiuMenoFlag = false;
    public int CorrettorePotenzaMotoreDaConfig = 20;
    public int CorrettorePotenzaMotoreMinimo = 5;
    public int CorrettorePotenzaMotoreCorrente = this.CorrettorePotenzaMotoreDaConfig;
    public VentilatoriSelezione VentilatoreSelezionato = new VentilatoriSelezione();
    public String descrizioneFluido[] = {"Aria secca", "Aria umida", "Esplicito", "Miscela"};
    public int descrizioneFluidoIndex = 0;
    public DBTableQualifier dbTableQualifier = new DBTableQualifier();
    public int angoliPossibiliCurrent[] = Costanti.angoliPossibiliDefault;
    public static String orientamentoLG = "LG";
    public static String orientamentoRD = "RD";
    public String orientamentoDefault = orientamentoRD;
    public int angoloRotazioneDefault = 0;
//ERP
    public ERP327Build ERP327Build = new ERP327Build();
    public boolean datiReali327 = false;
    public ArrayList<regolamento327Dati> elencoRegolamenti327 = new ArrayList<>();
    public regolamento327Algoritmo regolamento327Algoritmo = new regolamento327Algoritmo();
//preventivi
    public ArrayList<Preventivo> ElencoPreventivi = new ArrayList<>();
    public ArrayList<Clientev21> ElencoClienti = new ArrayList<>();
    public Clientev21 currentCliente = null;

    public Preventivo preventivoCurrent = null;
    public int preventivoCurrentIndex = -1;

    public Utilizzatore currentUtilizzatore = null;
    public String pannelloPreventivoAzioniSpeciali0Desc = null;
    public String pannelloPreventivoAzioniSpeciali1Desc = null;
    public String pannelloPreventivoAzioniSpeciali2Desc = null;
    public UtilityCliente utilityCliente;
    public UtilityClienteMoro utilityClienteMoro;
    public AccessoriAzioni accessoriAzioni;
    public StampaOfferta stampaOfferta = null;
    public boolean vuoleSostegnoSilenziatore = false;

    public XdevTable<CustomComponent> tableSilenziatori;
    public UnitaMisura catalogoCondizioniAltezzaUM;
    public UnitaMisura catalogoCondizioniTemperaturaUM;
    
    public ArrayList<String> ListaSerieMoro = new ArrayList<>();

    public VariabiliGlobali() {
        this.fmtNd.setGroupingUsed(false);
        this.fmtNdTable.setGroupingUsed(false);
        String computerName = "?";
        this.Paths.rootResources = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/resources/";
        final String nomeFileINI = this.Paths.rootResources + "SellFanHTMLParam.xml";
        final Map<String, String> env = System.getenv();
        if (env.containsKey("COMPUTERNAME")) {
            computerName = env.get("COMPUTERNAME");
        } else if (env.containsKey("HOSTNAME")) {
            computerName = env.get("HOSTNAME");
        }
        String step = "0";
        try {
            step = "1";
            final org.dom4j.io.SAXReader reader = new org.dom4j.io.SAXReader();
            //org.dom4j.Document documentIn = reader.read(new File(nomeFileINI));
            step = "2";
            final File fileIn = new File(nomeFileINI);
            step = "3";
            final InputStream fis = new FileInputStream(fileIn);
            step = "4";
            final org.dom4j.Document documentIn = reader.read(fis);
            step = "5";
            final org.dom4j.Element rootElement = documentIn.getRootElement();
            step = "6";
            final org.dom4j.Element serverElement = rootElement.element("sqlServer");
            step = "7";
            final org.dom4j.Element ambientiElement = rootElement.element("Ambienti");
            step = "8";
            this.serverName = serverElement.attributeValue("Name");
            this.serverUser = serverElement.attributeValue("User");
            this.serverPassword = serverElement.attributeValue("Password");
            if (computerName.equals("ERALDO-CIT")) {
                this.serverName = "ERALDO-CIT\\SQLEXPRESS";
                this.serverUser = "mz";
                this.serverPassword = "mz";
            }
            final String schemaPrincipaleStr = serverElement.attributeValue("SasSDB");
            final String AmbDisp = ambientiElement.attributeValue("Ambiente");
            this.AmbientiDisponibili = AmbDisp.split(",");
            step = "9";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            step = "10";
            this.serverurl = "jdbc:sqlserver://" + this.serverName + ";databaseName=" + schemaPrincipaleStr;
            this.conPrincipale = DriverManager.getConnection(this.serverurl, this.serverUser, this.serverPassword);
            //utilityFisica.settemperaturaRiferimentoC((int)CA020Catalogo.temperatura);
            this.Error = this.conPrincipale.toString();
        } catch (final Exception e) {
            this.conPrincipale = null;
            this.Error = step + ": " + e.toString() + "\n" + nomeFileINI;
        }
    }

    public boolean isDonaldson() {
        return this.utilityCliente.idSottoClienteIndex == 10;
    }
    public void addVentilatoreCorrenteToCompare() {
        completaCaricamentoVentilatore(this.VentilatoreCurrent);
        this.MainView.prestazioni.setVentilatoreCurrent();	//per completare i dati
        copiaVentilatoreInCurrentCompare(this.VentilatoreCurrent);
    }

    public void addToPreventivo() {
        this.addVentilatoreCorrenteToPreventivo();
        final MessageBox msgBox = MessageBox.createInfo();
        msgBox.withCaption(this.utilityTraduzioni.TraduciStringa("Fatto"));
        msgBox.withMessage(this.utilityTraduzioni.TraduciStringa("Ventilatore aggiunto al Preventivo corrente"));
        msgBox.withOkButton();
        msgBox.open();
    }

    public void addVentilatoreCorrenteToPreventivo() {
        if (this.VentilatoreCurrent.FromRicerca) {
            this.VentilatoreCurrent.selezioneCorrente.UmiditaRichiesta = this.SelezioneDati.umidita;
        } else {
            this.VentilatoreCurrent.selezioneCorrente.UmiditaRichiesta = this.VentilatoreCurrent.CAProgettazione.umidita;
        }

        completaCaricamentoVentilatore(this.VentilatoreCurrent);
        if (this.MobileMode) {
            this.MainView.prestazioniMobile.setVentilatoreCurrent();	//per completare i dati
        } else {
            this.MainView.prestazioni.setVentilatoreCurrent();		//per completare i dati
        }
        this.MainView.spettrosonoro.setVentilatoreCurrent();			//per completare i dati
        //dbCRMv2.storePreventivoFan(preventivoCurrent, ElencoVentilatoriFromPreventivo);
        copiaVentilatoreInCurrentPreventivo(this.VentilatoreCurrent);
        //dbCRMv2.storePreventivoFan(preventivoCurrent, ElencoVentilatoriFromPreventivo);
        //preventivoCurrent.PreventivoCambiato = false;
    }

    public void addVentilatoreToPreventivo(final Ventilatore l_v) {
        completaCaricamentoVentilatore(l_v);
        //dbCRMv2.storePreventivoFan(preventivoCurrent, ElencoVentilatoriFromPreventivo);
        copiaVentilatoreInCurrentPreventivo(l_v);
        //dbCRMv2.storePreventivoFan(preventivoCurrent, ElencoVentilatoriFromPreventivo);
        //preventivoCurrent.PreventivoCambiato = false;
    }

    public void buildDatiERP327ForSellFan(final Ventilatore l_v) {
        String query = "";
        if (l_v == null || l_v.campiCliente == null || l_v.selezioneCorrente.Esecuzione == null) {
            return;
        }
        if (l_v.ERPCaricato) {
            l_v.DatiCompleti = l_v.AccessoriCaricati && l_v.AccessoriCostiCaricati && l_v.ERPCaricato;
        } else {
            l_v.datiERP327.ventilatore327 = (Ventilatore) l_v.cloneNoAccessori();
            try {
                final Statement stmt = this.conTecnica.createStatement();
                query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "SerieRegolamento327" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
                query += " where Serie = '" + l_v.Serie + "'";
                final ResultSet rs = stmt.executeQuery(query);
                rs.next();
                l_v.datiERP327.categoriaMisura = rs.getString("CategoriaMisura");
                //this.MainView.setMemo("buildDatiERP327ForSellFan  "+l_v.datiERP327.categoriaMisura);
                l_v.datiERP327.tipoVentilatore = rs.getInt("tipoVentilatore");
                try {
                    l_v.datiERP327.tipoVentilatoreDesc = rs.getString("tipoVentilatoreDesc");
                    l_v.datiERP327.categoriaMisuraPressione = rs.getString("CategoriaMisuraPressione");
                } catch (final Exception e1) {
                    l_v.datiERP327.tipoVentilatoreDesc = "";
                    l_v.datiERP327.categoriaMisuraPressione = "";
                }
                rs.close();
                stmt.close();
                this.datiReali327 = true;
            } catch (final Exception e) {
                //this.MainView.setMemo(e.toString());
                this.datiReali327 = false;
                l_v.datiERP327.categoriaMisura = regolamento327Dati.categoriaMisuraIndefinita;
                l_v.datiERP327.tipoVentilatore = regolamento327Target.tipoCentrifugoPaleAvanti;
                l_v.datiERP327.tipoVentilatoreDesc = "";
                l_v.datiERP327.categoriaMisuraPressione = "";
            }
            l_v.datiERP327.datiReali = this.datiReali327;
            //            l_v.datiERP327.potenzaPersaCuscinettiW = utilityCliente.getPotenzaPersaMonobloccoW(l_v);
            l_v.datiERP327.esecuzione = l_v.selezioneCorrente.Esecuzione;
            l_v.datiERP327.haVariatore = l_v.campiCliente.inverterPresente;
            l_v.datiERP327.usaVariatore = false;
            l_v.datiERP327.trasmissioneBassaEfficienza = l_v.campiCliente.trasmissioneBassoRendimento;
            l_v.datiERP327.giunto = l_v.campiCliente.giuntoPresente;
            if (l_v.Trasmissione) {
                l_v.datiERP327.rpm = (int) l_v.datiERP327.ventilatore327.RpmInit;
            }
            //completaCaricamentoVentilatore(l_v);
            if (l_v.selezioneCorrente.MotoreInstallato == null) {
                this.utilityCliente.loadMotore(l_v, l_v.MotoreOriginale);
            }
        }
        this.utilityCliente.caricaPrezziVentilatore(l_v);
        l_v.datiERP327.potenzaPersaCuscinettiW = this.utilityCliente.getPotenzaPersaMonobloccoW(l_v);
        final VentilatoreFisica l_vf = new VentilatoreFisica();
        if (this.currentUserSaaS.idSottoClienteIndex == 6)
            l_vf.setVentilatoreFisica(this.conTecnica, this.dbTableQualifier.dbTableQualifierPreDBTecnico, this.dbTableQualifier.dbTableQualifierPostDBTecnico, l_v, this.ventilatoreFisicaParameter, "Moro");
            else
            l_vf.setVentilatoreFisica(this.conTecnica, this.dbTableQualifier.dbTableQualifierPreDBTecnico, this.dbTableQualifier.dbTableQualifierPostDBTecnico, l_v, this.ventilatoreFisicaParameter);
        this.utilityCliente.personalizzaRegolamento327Dati(l_v);
        l_v.datiERP327.esecuzione = l_v.selezioneCorrente.Esecuzione;
        this.regolamento327Algoritmo.set(l_v, l_vf);
        l_v.datiERP327.ventilatore327.ERPCaricato = true;
        l_v.ERPCaricato = true;
        l_v.DatiCompleti = l_v.AccessoriCaricati && l_v.AccessoriCostiCaricati && l_v.ERPCaricato;
    }

    public String buildModelloCompleto(final Ventilatore l_v) {
        return this.utilityCliente.buildModelloCompleto(l_v, this.parametriModelloCompleto);
    }

    public String buildModelloCompleto(final Ventilatore l_v, final ParametriModelloCompleto l_md) {
        return this.utilityCliente.buildModelloCompleto(l_v, l_md);
    }

    public void completaCaricamentoVentilatore(final Ventilatore l_v) {
        if (l_v == null) {
            return;
        }
        if (this.VentilatoreCurrentFromVieneDa != Costanti.vieneDaRicerca && l_v.DatiCompleti && l_v.AccessoriCaricati) {
            return;
        }
        final boolean ventilatoreSelezionato = true;
        boolean motoreSelezionato = true;
        this.angoliPossibiliCurrent = this.dbTecnico.getAngoliPossibili(l_v);
        if (l_v.selezioneCorrente.OrientamentoAngolo < 0) {
            l_v.selezioneCorrente.Orientamento = this.orientamentoDefault;
            l_v.selezioneCorrente.OrientamentoAngolo = this.dbTecnico.getAngoloCurrent(this.angoliPossibiliCurrent, this.angoloRotazioneDefault);
        }
        if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaCatalogo) {
            l_v.UMPortata = (UnitaMisura) this.UMPortataCorrente.clone();
            l_v.UMPressione = (UnitaMisura) this.UMPressioneCorrente.clone();
            l_v.UMPotenza = (UnitaMisura) this.UMPotenzaCorrente.clone();
            l_v.UMAltezza = (UnitaMisura) this.UMAltezzaCorrente.clone();
            l_v.UMTemperatura = (UnitaMisura) this.UMTemperaturaCorrente.clone();
        }
        if (l_v.selezioneCorrente.Esecuzione.equals("-")) {
            l_v.selezioneCorrente.Esecuzione = l_v.Esecuzioni[0];
        }
        if (!l_v.AccessoriCaricati) {
            if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
                //ventilatoreSelezionato = ElencoPreventivi.get((int)currentPreventivoIndex).Ventilatori.get(indexVentilatoreCorrente).VentilatoreSelezionato;
                //motoreSelezionato = ElencoPreventivi.get((int)currentPreventivoIndex).Ventilatori.get(indexVentilatoreCorrente).MotoreSelezionato;
            } else {
                motoreSelezionato = this.utilityCliente.isAlberoNudo(l_v.selezioneCorrente.Esecuzione);
            }
            if (ListaSerieMoro.contains(l_v.Serie)) {
                utilityClienteMoro.loadAccessori(l_v, this.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
            } else {
                utilityCliente.loadAccessori(l_v, this.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
            }
            final Accessorio giunto = this.gestioneAccessori.getAccessorio(l_v, "GE");
            if (giunto != null) {
                giunto.Descrizione = this.utilityTraduzioni.TraduciStringa(giunto.DescrizioneIT) + " - " + giunto.parametriVal[0];
            }
        }
        l_v.AccessoriCaricati = true;
        buildDatiERP327ForSellFan(l_v);
        if (l_v.Trasmissione && utilityCliente.idClienteIndex== 4 && l_v.FromRicerca)
        {
            double PotenzaRichiestaW = (l_v.selezioneCorrente.Potenza + utilityCliente.getPotenzaPersaMonobloccoW(l_v)) / l_v.datiERP327.efficienzaTrasmissione;
            double kw = getDefaultKw(l_v.MotoreOriginale)*1000;
            double kwRicerca = PotenzaRichiestaW > kw ? PotenzaRichiestaW : kw;
            String l_Motore = utilityCliente.ScegliMotore(l_v, kwRicerca, l_v.selezioneCorrente.NumeroGiri, VentilatoreSelezionato.PD2);
            if (l_Motore == null) {
                if (l_v.selezioneCorrente.MotoreInstallato == null) l_v.selezioneCorrente.MotoreInstallato = new Motore();
                else l_v.selezioneCorrente.MotoreInstallato.reset();
            } else {
                if(l_v.selezioneCorrente.MotoreInstallato.CodiceCliente.equals("-")  || l_v.selezioneCorrente.MotoreInstallato.CodiceCliente.equals(l_v.MotoreOriginale))
                    utilityCliente.loadMotore(l_v, l_Motore);                
                if (!l_v.Trasmissione && pannelloRicercaDArpmPiuMenoFlag) {
                    if (VentilatoreSelezionato.rpm < (l_v.selezioneCorrente.MotoreInstallato.Rpm * pannelloRicercaDArpmMenoPercentuale) ||
                        VentilatoreSelezionato.rpm > (l_v.selezioneCorrente.MotoreInstallato.Rpm * pannelloRicercaDArpmPiuPercentuale)) {
                            return;
                    }
                }
            }    
        }
        if (l_v.Classe.equals("-") && l_v.elencoClassi.size() > 0) {
            l_v.Classe = NumeriRomani.daAraboARomano(l_v.elencoClassi.get(0));
        }
        this.utilityCliente.caricaPrezziVentilatore(l_v);
        setPrezzoVentilatore("completaCaricamentoVentilatore", l_v);
        //ERP327Build.setERP327(l_v, regolamento327Algoritmo, ventilatoreFisicaParameter);
        if (l_v.selezioneCorrente.MotoreInstallato == null) {
            this.utilityCliente.loadMotore(l_v, l_v.MotoreOriginale);
        }
        l_v.DiametroGiranteDefault = l_v.DiametroGirante;
    }

    private double getDefaultKw(final String nomeMotore) {
        double result = -1;
        String query = "SELECT PotkW from Motori where CodiceCliente = '" + nomeMotore + "'";
        try {
            Statement stmt = conTecnica.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            result = rs.getDouble("PotkW");
        } catch (Exception e) {
            
        }
        return result;
    }
        
    public void calcolaCostoVentilatore(final VentilatorePreventivo l_vp, final Ventilatore l_v) {
        double costo = 0.;
        double sovrapprezzo = (this.idSottoClienteIndexVG == 7)? 1.0 : 1.4;
        if (l_vp.VentilatoreSelezionato) {
            int esecuzione = -1;
            for (int i = 0; i < l_v.Esecuzioni.length; i++) {
                if (l_v.Esecuzioni[i] == l_v.selezioneCorrente.Esecuzione.toString()) {
                    esecuzione = i;
                }
            }
            costo = l_v.PrezzoEsecuzioni[esecuzione];
        }
        if (l_vp.HaMotore && l_vp.MotoreSelezionato) {
            costo += l_v.selezioneCorrente.MotoreInstallato.Prezzo;;
        }
        for (int i = 0; i < l_v.selezioneCorrente.ElencoAccessori.size(); i++) {
            final Accessorio l_a = l_v.selezioneCorrente.ElencoAccessori.get(i);
            if (l_a.Selezionato && l_a.Prezzo > 0.) {
                costo += l_a.Prezzo * l_a.qta;
            }
        }
        for (int i = 0; i < l_v.selezioneCorrente.ElencoAccessoriFree.size(); i++) {
            final Accessorio l_a = l_v.selezioneCorrente.ElencoAccessoriFree.get(i);
            if (l_a.Selezionato && l_a.Prezzo > 0.) {
                costo += l_a.Prezzo * l_a.qta;
            }
        }
        if (l_v.selezioneCorrente.vuoleSilMandata && l_v.selezioneCorrente.silMandata != null) {
            costo += l_v.selezioneCorrente.silMandata.prezzo * l_v.selezioneCorrente.silMandata.qta;
            if (l_v.selezioneCorrente.vuoleTramoggiaMandata) {
                costo += l_v.selezioneCorrente.silMandata.tramoggiaConnessa.prezzo * l_v.selezioneCorrente.silMandata.tramoggiaConnessa.qta;
            }
            for (final Accessorio a : l_v.selezioneCorrente.silMandata.listaAccessori) {
                if (a.Selezionato) {
                    costo += a.Prezzo * a.qta;
                }
            }
        }
        if (l_v.selezioneCorrente.vuoleSilAspirazione && l_v.selezioneCorrente.silAspirazione != null) {
            costo += l_v.selezioneCorrente.silAspirazione.prezzo * l_v.selezioneCorrente.silAspirazione.qta;
            if (l_v.selezioneCorrente.vuoleTramoggiaAspirazione) {
                costo += l_v.selezioneCorrente.silAspirazione.tramoggiaConnessa.prezzo * l_v.selezioneCorrente.silAspirazione.tramoggiaConnessa.qta;
            }
            if (l_v.selezioneCorrente.vuoleSostegno) {
                costo += l_v.selezioneCorrente.silAspirazione.sostegnoSilenziatore.prezzoSostegno * l_v.selezioneCorrente.silAspirazione.sostegnoSilenziatore.qta;
            }
            for (final Accessorio a : l_v.selezioneCorrente.silAspirazione.listaAccessori) {
                if (a.Selezionato) {
                    costo += a.Prezzo * a.qta;
                }
            }
        }
        if (l_v.selezioneCorrente.vuoleBox && l_v.selezioneCorrente.box != null) {
            costo += l_v.selezioneCorrente.box.prezzo * l_v.selezioneCorrente.box.qta;
            //aggiungi costi accessori box
            if (l_v.selezioneCorrente.box.onBTC) {
                costo += l_v.selezioneCorrente.box.BTC;
            }
            if (l_v.selezioneCorrente.box.onBTS) {
                costo += l_v.selezioneCorrente.box.BTS;
            }
            if (l_v.selezioneCorrente.box.onGATGPT) {
                costo += l_v.selezioneCorrente.box.GATGPT;
            }
            if (l_v.selezioneCorrente.box.onsrA) {
                costo += l_v.selezioneCorrente.box.srA;
            }
            if (l_v.selezioneCorrente.box.onsrv3) {
                costo += l_v.selezioneCorrente.box.srv3;
            }
        }
        l_vp.CostoVentilatore = costo;
    }

    public double calcolaCostoVentilatore(final Ventilatore l_v) {
        double costo = 0.;
        int esecuzione = Arrays.asList(l_v.Esecuzioni).indexOf(l_v.selezioneCorrente.Esecuzione.toString());
        costo = l_v.PrezzoEsecuzioni[esecuzione];
        if (l_v.selezioneCorrente.MotoreInstallato != null)
            costo += l_v.selezioneCorrente.MotoreInstallato.Prezzo;
        for (Accessorio a: ListUtils.union(l_v.selezioneCorrente.ElencoAccessori, l_v.selezioneCorrente.ElencoAccessoriFree)) 
            if (a.Selezionato && a.Prezzo > 0)
                costo += a.Prezzo * a.qta;
        if (l_v.selezioneCorrente.vuoleSilMandata && l_v.selezioneCorrente.silMandata != null) {
            costo += l_v.selezioneCorrente.silMandata.prezzo * l_v.selezioneCorrente.silMandata.qta;
            if (l_v.selezioneCorrente.vuoleTramoggiaMandata) {
                costo += l_v.selezioneCorrente.silMandata.tramoggiaConnessa.prezzo * l_v.selezioneCorrente.silMandata.tramoggiaConnessa.qta;
            }
            for (final Accessorio a : l_v.selezioneCorrente.silMandata.listaAccessori) {
                if (a.Selezionato) {
                    costo += a.Prezzo * a.qta;
                }
            }
        }
        if (l_v.selezioneCorrente.vuoleSilAspirazione && l_v.selezioneCorrente.silAspirazione != null) {
            costo += l_v.selezioneCorrente.silAspirazione.prezzo * l_v.selezioneCorrente.silAspirazione.qta;
            if (l_v.selezioneCorrente.vuoleTramoggiaAspirazione) {
                costo += l_v.selezioneCorrente.silAspirazione.tramoggiaConnessa.prezzo * l_v.selezioneCorrente.silAspirazione.tramoggiaConnessa.qta;
            }
            if (l_v.selezioneCorrente.vuoleSostegno) {
                costo += l_v.selezioneCorrente.silAspirazione.sostegnoSilenziatore.prezzoSostegno * l_v.selezioneCorrente.silAspirazione.sostegnoSilenziatore.qta;
            }
            for (final Accessorio a : l_v.selezioneCorrente.silAspirazione.listaAccessori) {
                if (a.Selezionato) {
                    costo += a.Prezzo * a.qta;
                }
            }
        }
        if (l_v.selezioneCorrente.vuoleBox && l_v.selezioneCorrente.box != null) {
            costo += l_v.selezioneCorrente.box.prezzo * l_v.selezioneCorrente.box.qta;
            //aggiungi costi accessori box
            if (l_v.selezioneCorrente.box.onBTC) {
                costo += l_v.selezioneCorrente.box.BTC;
            }
            if (l_v.selezioneCorrente.box.onBTS) {
                costo += l_v.selezioneCorrente.box.BTS;
            }
            if (l_v.selezioneCorrente.box.onGATGPT) {
                costo += l_v.selezioneCorrente.box.GATGPT;
            }
            if (l_v.selezioneCorrente.box.onsrA) {
                costo += l_v.selezioneCorrente.box.srA;
            }
            if (l_v.selezioneCorrente.box.onsrv3) {
                costo += l_v.selezioneCorrente.box.srv3;
            }
        }
        return costo;
    }
    
    
    public void cambiaAngolo(final int deltaIndex) {
        boolean ventilatoreSelezionato = true;
        boolean motoreSelezionato = true;
        if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            this.preventivoCurrent.PreventivoCambiato = true;
            this.VentilatoreCurrent.VentilatoreCambiato = true;
            this.ElencoVentilatoriFromPreventivo.get(this.VentilatoreCurrentIndex).VentilatoreCambiato = true;
        }
        this.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo = getNextAngoloOrientamento(this.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo, deltaIndex);
        if (this.VentilatoreCurrent.Serie.contains("Q") && this.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo % 90 != 0) {
            this.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo = getNextAngoloOrientamento(this.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo, deltaIndex);
        }
        if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            ventilatoreSelezionato = this.preventivoCurrent.Ventilatori.get(this.VentilatoreCurrentIndex).VentilatoreSelezionato;
            motoreSelezionato = this.preventivoCurrent.Ventilatori.get(this.VentilatoreCurrentIndex).MotoreSelezionato;
        }
        this.utilityCliente.reloadAccessori(this.VentilatoreCurrent, this.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
        this.MainView.setAccessoriVentilatoreCurrent();
    }

    /*
	    public int calcolaNPoli(double NumeroGiri) {
	        int l_NPoli;
	        if (CACatalogo.frequenza == 50) {
	            for (l_NPoli = 0; l_NPoli < 3; l_NPoli++) {
	                if (NumeroGiri >= LimitiGiriSceltaPoli50Hz[l_NPoli]) {
	                    break;
	                }
	            }
	        } else {
	            for (l_NPoli = 0; l_NPoli < 3; l_NPoli++) {
	                if (NumeroGiri >= LimitiGiriSceltaPoli60Hz[l_NPoli]) {
	                    break;
	                }
	            }
	        }
	        l_NPoli++;
	        l_NPoli *= 2;
	        return l_NPoli;
	    }
     */
    public boolean cambiaClasse(final String newClasseIn) {
        if (this.VentilatoreCurrent == null) {
            return false;
        }
        int index = -1;
        int newClasseArabo;

        //final String newClasse = newClasseIn.replace("Class ", "");
        final String newClasse = newClasseIn.split(" ")[1];
        //    	if (this.VentilatoreCurrent.Classe != null && this.VentilatoreCurrent.Classe.equals(newClasse)) {
        //			return false;
        //		}
        newClasseArabo = NumeriRomani.daRomanoAArabo(newClasse);
        for (int i = 0; i < this.VentilatoreCurrent.elencoClassi.size(); i++) {
            if (this.VentilatoreCurrent.elencoClassi.get(i) == newClasseArabo) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return false;
        }
        this.VentilatoreCurrent.Classe = NumeriRomani.daAraboARomano(this.VentilatoreCurrent.elencoClassi.get(index));
        this.VentilatoreCurrent.RpmLimite[0] = this.VentilatoreCurrent.elencoRpmClassi.get(index);
        this.utilityCliente.fillRpmLimite(this.VentilatoreCurrent);
        this.VentilatoreCurrent.RpmMaxTeorici = RpmMaxTemperatura(this.VentilatoreCurrent.CAProgettazione.temperatura, this.VentilatoreCurrent);
        this.ventilatoreCambiato = true;
        return true;
    }

    public String cambiaEsecuzione(final String newEsecuzione) {
        boolean ventilatoreSelezionato = true;
        boolean motoreSelezionato = true;
        String ret = "";
        if (newEsecuzione == "" || this.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals(newEsecuzione)) {
            return "";
        }
        this.VentilatoreCurrent.selezioneCorrente.Esecuzione = newEsecuzione;
        if (this.utilityCliente.isAlberoNudo(newEsecuzione)) {
            cambiaMotore("-");
        }
        this.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Prezzo = this.utilityCliente.getPrezzoMotore(this.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente, this.VentilatoreCurrent.selezioneCorrente.Esecuzione);
        ret = getDescrizioneEsecuzione(newEsecuzione);
        final PreventiviView view = (PreventiviView) this.ElencoView[this.PreventiviViewIndex];
        if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            ventilatoreSelezionato = this.preventivoCurrent.Ventilatori.get(view.ventilatoreIndex).VentilatoreSelezionato;
            motoreSelezionato = this.preventivoCurrent.Ventilatori.get(view.ventilatoreIndex).MotoreSelezionato;
        }
        this.utilityCliente.reloadAccessori(this.VentilatoreCurrent, this.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
        final Accessorio giunto = this.gestioneAccessori.getAccessorio(this.VentilatoreCurrent, "GE");
        if (giunto != null) {
            giunto.Descrizione = this.utilityTraduzioni.TraduciStringa(giunto.DescrizioneIT) + " - " + giunto.parametriVal[0];
        }
        this.regolamento327Algoritmo.setVariatore(this.VentilatoreCurrent.datiERP327.usaVariatore);
        this.ventilatoreCambiato = true;
        this.MainView.setAccessoriVentilatoreCurrent();
        this.MainView.abilitaTab();
        return ret;
    }

    public void cambiaManualmenteMotore() {
        this.VentilatoreCurrent.selezioneCorrente.box = null;
        if (this.VentilatoreCurrent.selezioneCorrente.Potenza <= 0.) {
            cambiaMotore(this.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
        }
        final pannelloSelezioneMotore pannello = new pannelloSelezioneMotore();
        pannello.setVariabiliGlobali(this);
        pannello.setVentilatoreCorrente();
        final MessageBox msgBox = MessageBox.create();
        msgBox.withMessage(pannello);
        msgBox.withAbortButton();
        msgBox.withOkButton(() -> {
            final String newCodice = pannello.getNewCodice();
            if (newCodice.equals("Custom")) {
                VentilatoreCurrent.selezioneCorrente.MotoreInstallato = pannello.getMotoreCustom();
                cambiaMotore(VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
            } else {
            if (!newCodice.equals("") && !newCodice.equals(pannello.getOldCodice())) {
                final int grandezzaMotoreMax = this.utilityCliente.getGrandezzaMaxMotore(this.VentilatoreCurrent);
                if (grandezzaMotoreMax > 0 && !this.utilityCliente.testGrandezzaMotoreMax) {
                    final Motore l_m = this.utilityCliente.getMotore(newCodice);
                    if (l_m != null && l_m.Grandezza > grandezzaMotoreMax) {
                        MessageBox msgBox1 = MessageBox.create();
                        msgBox1 = MessageBox.create();
                        msgBox1.withMessage(this.utilityTraduzioni.TraduciStringa("Attenzione: taglia motore fuori standard - verificare fattibilità con ufficio tecnico")
                                + "\n" + this.utilityTraduzioni.TraduciStringa("Confermi?"));
                        msgBox1.withYesButton(() -> {
                            cambiaMotore(newCodice);;
                        });
                        msgBox1.withNoButton();
                        msgBox1.open();
                    } else {
                        cambiaMotore(newCodice);
                    }
                } else {
                    cambiaMotore(newCodice);
                }
                if (this.VentilatoreCurrent.FromPreventivo) {
                    final PreventiviView view = (PreventiviView) this.ElencoView[this.PreventiviViewIndex];
                    view.aggiornaVentilatoreCorrente();
                }
            }
            }
        });
        msgBox.open();
    }

    public boolean cambiaMotore(final String NuovoMotore) {
        final boolean ventilatoreSelezionato = true;
        boolean motoreSelezionato = true;
        boolean ret = false;
        if (!NuovoMotore.equals(this.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente)) {
            if (NuovoMotore.equals("-")) {
                if (this.VentilatoreCurrent.selezioneCorrente.MotoreInstallato == null) {
                    this.VentilatoreCurrent.selezioneCorrente.MotoreInstallato = new Motore();
                } else {
                    this.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.reset();
                }
            } else {
                ret = this.utilityCliente.loadMotore(this.VentilatoreCurrent, NuovoMotore);
                if (!ret) {
                    return false;
                }
            }
            this.VentilatoreCurrent.VentilatoreCambiato = true;
            final PreventiviView view = (PreventiviView) this.ElencoView[this.PreventiviViewIndex];
            if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
                if (NuovoMotore.equals("-")) {
                    this.preventivoCurrent.Ventilatori.get(view.ventilatoreIndex).HaMotore = false;
                    this.preventivoCurrent.Ventilatori.get(view.ventilatoreIndex).MotoreSelezionato = false;
                } else {
                    this.preventivoCurrent.Ventilatori.get(view.ventilatoreIndex).HaMotore = true;
                }
                this.preventivoCurrent.PreventivoCambiato = true;
                this.preventivoCurrent.Ventilatori.get(view.ventilatoreIndex).VentilatoreCambiato = true;
                this.preventivoCurrent.PreventivoCambiato = true;
                this.ElencoVentilatoriFromPreventivo.get(view.ventilatoreIndex).VentilatoreCambiato = true;
                motoreSelezionato = this.preventivoCurrent.Ventilatori.get(view.ventilatoreIndex).MotoreSelezionato;
            }
            this.ventilatoreCambiato = true;
            this.utilityCliente.reloadAccessori(this.VentilatoreCurrent, this.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
            final Accessorio giunto = this.gestioneAccessori.getAccessorio(this.VentilatoreCurrent, "GE");
            if (giunto != null) {
                giunto.Descrizione = this.utilityTraduzioni.TraduciStringa(giunto.DescrizioneIT) + " - " + giunto.parametriVal[0];
            }
            this.regolamento327Algoritmo.setVariatore(this.VentilatoreCurrent.datiERP327.usaVariatore);
        }
        if (this.currentUserSaaS.idClienteIndex == 4) {
            final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.VentilatoreCurrent.campiCliente;
            if (isDonaldson()) {
                final Cliente04AccessoriViewDonaldson view = (Cliente04AccessoriViewDonaldson) this.ElencoView[this.AccessoriViewIndex];
                if (view != null) {
                    view.setVentilatoreCurrent();
                }
            }
            else if (l_vcc04.Tipo.equals("centrifugo")) {
                final Cliente04AccessoriViewCentrifughi view = (Cliente04AccessoriViewCentrifughi) this.ElencoView[this.AccessoriViewIndex];
                if (view != null) {
                    view.setVentilatoreCurrent();
                }
            } else if (l_vcc04.Tipo.equals("fusione")) {
                final Cliente04AccessoriViewFusione view = (Cliente04AccessoriViewFusione) this.ElencoView[this.AccessoriViewIndex];
                if (view != null) {
                    view.setVentilatoreCurrent();
                }
            } else if (l_vcc04.Tipo.equals("assiale")) {
                final Cliente04AccessoriViewAssiali view = (Cliente04AccessoriViewAssiali) this.ElencoView[this.AccessoriViewIndex];
                if (view != null) {
                    view.setVentilatoreCurrent();
                }
            }
        } else {
            //non serve nella accessori view di default non c'è il motore
        }
        if (this.MobileMode) {
            final MobilePrestazioniView viewPrestazioni = (MobilePrestazioniView) this.ElencoMobileView[this.PrestazioniViewIndex];
            viewPrestazioni.setVentilatoreCurrent();
        } else {
            final PrestazioniView viewPrestazioni = (PrestazioniView) this.ElencoView[this.PrestazioniViewIndex];
            viewPrestazioni.setVentilatoreCurrent();
        }
        return true;
    }

    public void cambiaOrientamento() {
        boolean ventilatoreSelezionato = true;
        boolean motoreSelezionato = true;
        if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            this.preventivoCurrent.PreventivoCambiato = true;
            this.VentilatoreCurrent.VentilatoreCambiato = true;
            this.ElencoVentilatoriFromPreventivo.get(this.VentilatoreCurrentIndex).VentilatoreCambiato = true;
        }
        if (this.VentilatoreCurrent.selezioneCorrente.Orientamento.equals("LG")) {
            this.VentilatoreCurrent.selezioneCorrente.Orientamento = "RD";
        } else {
            this.VentilatoreCurrent.selezioneCorrente.Orientamento = "LG";
        }
        if (this.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            ventilatoreSelezionato = this.preventivoCurrent.Ventilatori.get(this.VentilatoreCurrentIndex).VentilatoreSelezionato;
            motoreSelezionato = this.preventivoCurrent.Ventilatori.get(this.VentilatoreCurrentIndex).MotoreSelezionato;
        }
        this.utilityCliente.reloadAccessori(this.VentilatoreCurrent, this.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
        this.MainView.setAccessoriVentilatoreCurrent();
    }
    
    public String calcolaRpMMax() {
        cambiaClasse(" " + VentilatoreCurrent.Classe);
        final int giri[] = new int[VentilatoreCurrent.RpmLimite.length];
        for (int i=0 ; i < VentilatoreCurrent.RpmLimite.length ; i++) {
            giri[i] = VentilatoreCurrent.RpmLimite[i];
        }
        if (giri[0] < 0) {
            giri[0] = (int)VentilatoreCurrent.RpmInit;
        }
        int index = 0;
        for (index=0 ; index < VentilatoreCurrent.RpmLimite.length-1 ; index++) {
            if (VentilatoreCurrent.CAProgettazione.temperatura <= temperatureLimite[index]) {
                break;
            }
        }
        return Integer.toString(giri[index]);
    }

    public void caricaElencoGas() {
        Gas gas;
        try {
            final Statement stmt = this.conTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Gas" + this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where Formula = 'ARIA'"; //aria
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            gas = readDatiGas(rs);
            if (gas == null) {
                return;
            }
            gas.densitaRelativa = -gas.densitaRelativa;
            this.ElencoGas.add(gas);
            rs.close();
            query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Gas" + this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where Formula <> '" + "ARIA" + "'";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                gas = readDatiGas(rs);
                if (gas == null) {
                    continue;
                }
                this.ElencoGas.add(gas);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {

        }
    }

    public Silenziatore cercaInLista(final ArrayList<Silenziatore> lista, final String codice) {
        for (final Silenziatore sil : lista) {
            if (sil.codiceSilenziatore.equals(codice)) {
                return sil;
            }
        }
        return null;
    }

    public boolean condizionaAccessori() {
        boolean ret = verificaMutueEsclusioniAccessori();
        ret |= verificaAlmenoUnoAccessori();
        if (this.accessorioInEsame != null)
            ret |= this.utilityCliente.setAccessoriCorrelati(this.VentilatoreCurrent, this.accessorioInEsame);
        return ret;
    }

    private void copiaVentilatoreInCurrentCompare(final Ventilatore l_v) {
        final Ventilatore l_vNew = (Ventilatore) l_v.clone();
        l_vNew.VentilatoreCambiato = false;
        l_vNew.AccessoriCambiati = false;
        l_vNew.IDVentilatore = -1;
        l_vNew.FromPreventivo = false;
        this.ElencoVentilatoriFromCompare.add(l_vNew);
    }

    private void copiaVentilatoreInCurrentPreventivo(final Ventilatore l_v) {
        boolean realodPreventivi = false;
        if (this.preventivoCurrent == null) {
            this.preventivoCurrent = new Preventivo();
            this.ElencoVentilatoriFromPreventivo.clear();
            this.utilityCliente.inizializzaPreventivo(this.preventivoCurrent);
            this.preventivoCurrent.IDPreventivo = -1;
            realodPreventivi = true;
            //ElencoPreventivi.add(preventivoCurrent);
        }
        final Ventilatore l_vNew = (Ventilatore) l_v.clone();

        if (this.ElencoVentilatoriFromPreventivo.size() > 0) {
            l_vNew.CodiceVentilatoreForPreventivo = this.ElencoVentilatoriFromPreventivo.get(this.ElencoVentilatoriFromPreventivo.size() - 1).CodiceVentilatoreForPreventivo + 1;
        } else {
            l_vNew.CodiceVentilatoreForPreventivo = 1;
        }
        l_vNew.VentilatoreCambiato = true;
        l_vNew.AccessoriCambiati = true;
        l_vNew.IDVentilatore = -1;
        l_vNew.FromPreventivo = true;
        this.ElencoVentilatoriFromPreventivo.add(l_vNew);
        l_vNew.selezioneCorrente.CodiceVentilatoreForPreventivo = l_vNew.CodiceVentilatoreForPreventivo;
        final VentilatorePreventivo l_vp = new VentilatorePreventivo();
        l_vp.IDVentilatore = -1;
        l_vp.HaPreventivo = true;
        l_vp.CodiceVentilatoreForPreventivo = l_vNew.CodiceVentilatoreForPreventivo;
        l_vp.VentilatoreCambiato = true;
        if (l_vNew.selezioneCorrente.MotoreInstallato.CodiceCliente == null || l_vNew.selezioneCorrente.MotoreInstallato.CodiceCliente.equals("") || l_vNew.selezioneCorrente.MotoreInstallato.CodiceCliente.equals("-")) {
            l_vp.HaMotore = false;
            l_vp.MotoreSelezionato = false;
        } else {
            l_vp.HaMotore = true;
            l_vp.MotoreSelezionato = true;
        }
        calcolaCostoVentilatore(l_vp, l_v);
        this.preventivoCurrent.CostoTotale += l_vp.CostoVentilatore;
        this.preventivoCurrent.Ventilatori.add(l_vp);
        this.preventivoCurrent.PreventivoCambiato = false;
        this.dbCRMv2.storePreventivoFan(this.preventivoCurrent, this.ElencoVentilatoriFromPreventivo);
        this.preventivoCurrent.PreventivoCambiato = true;
        if (realodPreventivi) {
            this.dbCRMv2.loadFullElencoPreventiviFan(this.ElencoPreventivi, 0, this.currentUserSaaS._ID);
        }
    }

    @SuppressWarnings("unused")
    public void eseguiRicerca() {
        final Calendar calendarStart = Calendar.getInstance();
        final long millisecondiStart = calendarStart.get(Calendar.MILLISECOND) + 1000 * calendarStart.get(Calendar.SECOND) + 60000 * calendarStart.get(Calendar.MINUTE) + 3600000 * calendarStart.get(Calendar.HOUR);
        //tipoMotoreCurrent = tipoMotoreDefault[0];
        this.SelezioneDati.modeRicerca = Costanti.modeRicercaStandard; //modeRicerca;;
        this.SelezioneDati.Temperatura = this.CARicerca.temperatura;
        this.SelezioneDati.pannelloRicercaTRLimitePortataFlag = false;//pannelloRicercaTRLimitePortataFlag;
        this.SelezioneDati.pannelloRicercaTRLimitePressioneFlag = false;//pannelloRicercaTRLimitePressioneFlag;
        this.SelezioneDati.pannelloRicercaDALimitePortataFlag = false;//pannelloRicercaDALimitePortataFlag;
        this.SelezioneDati.pannelloRicercaDALimitePressioneFlag = false;//pannelloRicercaDALimitePressioneFlag;
        this.SelezioneDati.pannelloRicercaLimiteTemperaturaProgettazioneFlag = false;//pannelloRicercaLimiteTemperaturaProgettazioneFlag;
        this.SelezioneDati.needModelloNull = false;
        this.SelezioneDati.ElencoSerie = this.ElencoSerie;
        this.SelezioneDati.ElencoSerieDisponibili = this.ElencoSerieDisponibili;
        this.SelezioneDati.ElencoMateriali = this.ElencoMateriali;
        this.SelezioneDati.CategorieSerieIndex = 1;	//selezione esplicita
        String memo = "Ricerca Portata=" + this.SelezioneDati.PortataRichiestam3h + "[m3/h] Pressione=" + this.SelezioneDati.PressioneRichiestaPa + "[Pa] Trasmissione=" + this.SelezioneDati.Trasmissione;
        this.traceUser.traceAzioneUser(this.traceUser.AzioneSearch, memo);

        this.VentilatoreSelezionato.portataRichiestam3h = this.SelezioneDati.PortataRichiestam3h;
        this.VentilatoreSelezionato.pressioneRichiestaPa = this.SelezioneDati.PressioneRichiestaPa;
        this.VentilatoreSelezionato.tolleranzaSuPortataFlag = this.SelezioneDati.tolleranzaPortataFlag;
        if (SelezioneDati.precisioneMenoPressione > 0.) {
            VentilatoreSelezionato.deltaPaMeno = SelezioneDati.PressioneRichiestaPa * SelezioneDati.precisioneMenoPressione;
        } else {
            VentilatoreSelezionato.deltaPaMeno = SelezioneDati.PressioneRichiestaPa * 0.00005;
        }
        if (SelezioneDati.precisionePiuPressione > 0.) {
            VentilatoreSelezionato.deltaPaPiu = SelezioneDati.PressioneRichiestaPa * SelezioneDati.precisionePiuPressione;
        } else {
            VentilatoreSelezionato.deltaPaPiu = SelezioneDati.PressioneRichiestaPa * 0.00005;
        }
        if (this.SelezioneDati.precisionePiu > 0.) {
        //    this.VentilatoreSelezionato.deltaPaPiu = this.SelezioneDati.PressioneRichiestaPa * this.SelezioneDati.precisionePiu;
            this.VentilatoreSelezionato.deltam3Piu = this.SelezioneDati.PortataRichiestam3h * this.SelezioneDati.precisionePiu;
        } else {
//            this.VentilatoreSelezionato.deltaPaPiu = this.SelezioneDati.PressioneRichiestaPa * 0.00005;
            this.VentilatoreSelezionato.deltam3Piu = this.SelezioneDati.PortataRichiestam3h * 0.00005;
        }
        if (this.SelezioneDati.precisioneMeno > 0.) {
//            this.VentilatoreSelezionato.deltaPaMeno = this.SelezioneDati.PressioneRichiestaPa * this.SelezioneDati.precisioneMeno;
            this.VentilatoreSelezionato.deltam3Meno = this.SelezioneDati.PortataRichiestam3h * this.SelezioneDati.precisioneMeno;
        } else {
//            this.VentilatoreSelezionato.deltaPaMeno = this.SelezioneDati.PressioneRichiestaPa * 0.00005;
            this.VentilatoreSelezionato.deltam3Meno = this.SelezioneDati.PortataRichiestam3h * 0.00005;
        }
        this.VentilatoreSelezionato.giriMassimiGirante = this.SelezioneDati.giriMassimiGirante;
        this.VentilatoreSelezionato.CorrettorePotMotore = 0;//utilityCliente.getCorrettorePotenzaMotore(VentilatoreSelezionato.potenza);
        this.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.utilityFisica.getpressioneAtmosfericaPa(this.CARicerca.temperatura, this.CARicerca.altezza);

        this.ricerca.initDynamicVariable(this.CARicerca, this.CA020Ricerca, this.utilityCliente.tipoMotoreDefault, this.CorrettorePotenzaMotoreCorrente);
        final int nVent = this.ricerca.eseguiRicerca(this.ElencoVentilatoriFromRicerca, this.VentilatoreSelezionato, this.SelezioneDati, this.ventilatoreFisicaParameter);
        for (int i = 0; i < this.ElencoVentilatoriFromRicerca.size(); i++) {
            final Ventilatore l_v = this.ElencoVentilatoriFromRicerca.get(i);
            completaCaricamentoVentilatore(l_v);
            l_v.DescrizioneFluidoRicerca = this.descrizioneFluido[this.descrizioneFluidoIndex];
            Ambiente = utilityCliente.idSottoClienteIndex== 6 ? "Moro" : "";
            this.ERP327Build.setERP327(l_v, this.regolamento327Algoritmo, this.ventilatoreFisicaParameter, Ambiente);
            final VentilatoreXSelezione l_vXS = this.VentilatoreSelezionato.ElencoVentilatori.get(i);
            l_vXS.ModelloCompleto = buildModelloCompleto(l_v, this.parametriModelloCompletoForPrint);
        }
        this.ventilatoreSort.sortCampo = this.campoVentilatoreSort;
        this.ventilatoreSort.sortCrescente = this.crescenteVentilatoreSort;
        this.ventilatoreSort.sortRicerca(this.ElencoVentilatoriFromRicerca, this.VentilatoreSelezionato);
        final Calendar calendarEnd = Calendar.getInstance();
        final long millisecondiEnd = calendarEnd.get(Calendar.MILLISECOND) + 1000 * calendarEnd.get(Calendar.SECOND) + 60000 * calendarEnd.get(Calendar.MINUTE) + 3600000 * calendarEnd.get(Calendar.HOUR);
        if (this.ElencoVentilatoriFromRicerca.isEmpty()) {
            memo = "[" + Long.toString(millisecondiEnd - millisecondiStart) + "ms/" + "0v] Nessun ventilatore selezionato";
        } else {
            String msg = " [";
            for (int i = 0; i < this.ElencoVentilatoriFromRicerca.size() - 1; i++) {
                msg += this.ElencoVentilatoriFromRicerca.get(i).Modello + ", ";
            }
            msg += this.ElencoVentilatoriFromRicerca.get(this.ElencoVentilatoriFromRicerca.size() - 1).Modello + "]";
            memo = "[" + Long.toString(millisecondiEnd - millisecondiStart) + "ms/" + Long.toString(this.ElencoVentilatoriFromRicerca.size()) + "v] Trovati " + Integer.toString(this.ElencoVentilatoriFromRicerca.size()) + " ventilatori" + msg;
        }
        this.traceUser.traceAzioneUser(this.traceUser.AzioneRisultatiSearch, memo);
    }

    public void getBrowserAspectRatio() {
        final Page page = UI.getCurrent().getPage();
        this.browserWidth = page.getBrowserWindowWidth();
        this.browserHeight = page.getBrowserWindowHeight();
        this.browserAspectRatio = (double) this.browserWidth / this.browserHeight;
        final WebBrowser webBrowser = page.getWebBrowser();
        this.browserDescription = webBrowser.getBrowserApplication() + " " + Integer.toString(webBrowser.getBrowserMajorVersion()) + "." + Integer.toString(webBrowser.getBrowserMinorVersion());
        if (webBrowser.isAndroid()) {
            this.OSDescription = "Android";
        } else if (webBrowser.isIOS()) {
            this.OSDescription = "iOS";
        } else if (webBrowser.isIPad()) {
            this.OSDescription = "iPad";
        } else if (webBrowser.isIPhone()) {
            this.OSDescription = "iPhone";
        } else if (webBrowser.isLinux()) {
            this.OSDescription = "Linux";
        } else if (webBrowser.isMacOSX()) {
            this.OSDescription = "Mac X";
        } else if (webBrowser.isWindows()) {
            this.OSDescription = "Windows";
        } else if (webBrowser.isWindowsPhone()) {
            this.OSDescription = "Windows Phone";
        } else {
            this.OSDescription = "Unknow";
        }
        if (webBrowser.isTouchDevice()) {
            this.OSDescription += " on Touch Screen";
        }
        /*
			MobileMode = false;
			if (browserWidth < minBrowserWidth || browserHeight < minBrowserHeight) {
				MobileMode = true;
			} else if (webBrowser.isAndroid()) {
				if (browserHeight > browserWidth) {
					MobileMode = true;
	    		}
			} else if (webBrowser.isIOS() || webBrowser.isIPhone() || webBrowser.isWindowsPhone()) {
	    		MobileMode = true;
	    	}
         */
    }

    public String getDescrizioneEsecuzione(final String esecuzione) {
        final String descrizione = this.mapEsecuzioniDesc.get(esecuzione);
        if (descrizione != null) {
            return this.utilityTraduzioni.TraduciStringa(descrizione);
        } else {
            return "";
        }
    }

    private int getNextAngoloOrientamento(final int angoloIn, final int indexStep) {
        int index = -1;
        for (int i = 0; i < this.angoliPossibiliCurrent.length; i++) {
            if (this.angoliPossibiliCurrent[i] == angoloIn) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return angoloIn;
        }
        index += indexStep;
        if (index < 0) {
            index += this.angoliPossibiliCurrent.length;
        }
        if (index >= this.angoliPossibiliCurrent.length) {
            index -= this.angoliPossibiliCurrent.length;
        }
        return this.angoliPossibiliCurrent[index];
    }

    public StreamResource getStreamResourceForImageFile(final String nomeFile) {
        try {
            final FileInputStream fileIn = new FileInputStream(nomeFile);
            final jDisplayImageWeb imageWeb = new jDisplayImageWeb();
            imageWeb.DisplayfromInputStream(fileIn);
            final StreamResource imagesource = new StreamResource(imageWeb, "myimage" + Integer.toString(this.progressivoGenerico++) + ".png");
            imagesource.setCacheTime(0);
            return imagesource;
        } catch (final Exception e) {
            return null;
        }
    }

    public Double getTemperaturaProgettazioneC() {
        return this.CACatalogo.temperatura;
    }

    public void getUMForDefaultIndex() {
        this.UMDefaultIndex.indexPressione = this.UMPressioneCorrente.index;
        this.UMDefaultIndex.indexPortata = this.UMPortataCorrente.index;
        this.UMDefaultIndex.indexPotenza = this.UMPotenzaCorrente.index;
        this.UMDefaultIndex.indexAltezza = this.UMAltezzaCorrente.index;
        this.UMDefaultIndex.indexTemperatura = this.UMTemperaturaCorrente.index;
        this.UMDefaultIndex.indexDiametro = this.UMDiametroCorrente.index;
    }

    public double getVeloc(final double Marker, final int diametro) {

        final double raggio = Double.valueOf(diametro + "") / 2000;
        final double area = raggio * raggio * Math.PI;
        final double velMH = Marker / area;
        final double velMS = velMH / 3600;
        return velMS;
    }

    public void impostaBox(final XdevTable<CustomComponent> tableBox, int codiceBox, ArrayList<Integer> misure) {
        this.tableSilenziatori = tableBox;
        final pannelloSelezioneBox pannello = new pannelloSelezioneBox(this, codiceBox, misure);
        final Box sel = pannello.select();
        final Object boxTab[] = new Object[4];
        boxTab[0] = "---";
        boxTab[1] = sel.codiceBox;
        boxTab[2] = this.utilityTraduzioni.TraduciStringa("Box insonorizzato");
        boxTab[3] = "";
        final MessageBox msgBox = MessageBox.create();
        msgBox.withMessage(pannello);
        msgBox.withWidth("600px");
        msgBox.withHeight("300px");
        msgBox.withNoButton(() -> {
            this.VentilatoreCurrent.selezioneCorrente.box = null;
            if (this.tableSilenziatori.getItemIds().size() > 0) {
                final Object idLast = this.tableSilenziatori.lastItemId();
                final Item lastItem = this.tableSilenziatori.getItem(idLast);
                if (lastItem.getItemProperty("Direction").getValue().toString().equals("---")) {
                    this.tableSilenziatori.removeItem(idLast);
                }
            }
            this.VentilatoreCurrent.selezioneCorrente.vuoleBox = false;
            this.dbCRMv2.storePreventivoFan(this.preventivoCurrent, this.ElencoVentilatoriFromPreventivo);
        });
        msgBox.withYesButton(() -> {
            final Cliente04GestioneSilenziatori silenziatori = new Cliente04GestioneSilenziatori(this);
            silenziatori.caricaBoxConAccessori(tableSilenziatori, sel);
        });
        msgBox.open();

    }

    public void impostaSilenziatoreAspirazione(final ArrayList<Silenziatore> elencoAspirazione, final XdevTable<CustomComponent> tableSilenziatori) {
        final pannelloSelezioneSilenziatore pannello = new pannelloSelezioneSilenziatore(this);
        pannello.loadTabellaSilenziatori(elencoAspirazione, false);
        pannello.Nazionalizza(false);
        final MessageBox msgBox = MessageBox.create();
        msgBox.withWidth("800px");
        msgBox.withMessage(pannello);
        msgBox.withAbortButton();
        msgBox.withOkButton(() -> {
            final Cliente04GestioneSilenziatori silenziatori = new Cliente04GestioneSilenziatori(this);
            if (pannello.selectInlet()) {
                silenziatori.caricaSilenziatoreInTabellaWithInfo(tableSilenziatori, false);
            }
        });
        msgBox.open();
    }

    public void impostaSilenziatoreMandata(final ArrayList<Silenziatore> elencoMandata, final XdevTable<CustomComponent> tableSilenziatori) {
        final pannelloSelezioneSilenziatore pannello = new pannelloSelezioneSilenziatore(this);
        pannello.loadTabellaSilenziatori(elencoMandata, true);
        pannello.Nazionalizza(false);
        final MessageBox msgBox = MessageBox.create();
        msgBox.withWidth("800px");
        msgBox.withMessage(pannello);
        msgBox.withAbortButton();
        msgBox.withOkButton(() -> {
            final Cliente04GestioneSilenziatori silenziatori = new Cliente04GestioneSilenziatori(this);
            if (pannello.selectOutlet()) {
                silenziatori.caricaSilenziatoreInTabellaWithInfo(tableSilenziatori, true);
            }
        });
        msgBox.open();
    }

    public ArrayList<Ventilatore> loadVentilatoriSimiliForMultigiri(final Ventilatore l_vin) {
        final ArrayList<Ventilatore> elencoVentilatoriSimili = new ArrayList<>();
        try {
            int freq = -1;
            if (!this.catalogoCondizioniFrequenza.equals("")) {
                freq = Integer.parseInt(this.catalogoCondizioniFrequenza.split(" ")[0]);
            }
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Trasmissione" + this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where ";
            query += "Serie = '" + l_vin.Serie + "' and Modello = '" + l_vin.Modello + "' and Modello <> '" + l_vin.Serie + " MASTER' order by RpmMax20 desc";
            final Statement stmt = this.conTecnica.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                final Ventilatore l_v = this.dbTecnico.loadVentilatoreForCatalogo(rs, this.nTemperatureLimite + 1, this.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, true, -1, -1, this.CACatalogo, this.CA020Catalogo, this.idTranscodificaIndex, freq);
                l_v.Trasmissione = true;
                elencoVentilatoriSimili.add(l_v);
            }
            rs.close();
            query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "DirettamenteAccoppiati" + this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where ";
            query += "Serie = '" + l_vin.Serie + "' and Modello = '" + l_vin.Modello + "' and Modello <> '" + l_vin.Serie + " MASTER' order by RpmMax20 desc";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                final Ventilatore l_v = this.dbTecnico.loadVentilatoreForCatalogo(rs, this.nTemperatureLimite + 1, this.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, true, -1, -1, this.CACatalogo, this.CA020Catalogo, this.idTranscodificaIndex, freq);
                l_v.Trasmissione = false;
                elencoVentilatoriSimili.add(l_v);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {

        }
        return elencoVentilatoriSimili;
    }

    private void nazionalizzaView() {
        final MainView view = (MainView) this.ElencoView[this.MainViewIndex];
        view.Nazionalizza();
        switch (this.CurrentViewIndex) {
            case SetupViewIndex:
                final SetupView view1 = (SetupView) this.ElencoView[this.CurrentViewIndex];
                view1.Nazionalizza();
                break;
            case CatalogoViewIndex:
                if (this.MobileMode) {
                    final MobileCatalogoView view2 = (MobileCatalogoView) this.ElencoMobileView[this.CurrentViewIndex];
                    view2.Nazionalizza();
                    view2.buildTableSerie();
                } else {
                    final CatalogoView view2 = (CatalogoView) this.ElencoView[this.CurrentViewIndex];
                    view2.Nazionalizza();
                    view2.buildTableSerie();
                }
                break;
            case SearchViewIndex:
                if (this.MobileMode) {
                    final MobileSearchView view3 = (MobileSearchView) this.ElencoMobileView[this.CurrentViewIndex];
                    view3.Nazionalizza();
                } else {
                    final SearchView view3 = (SearchView) this.ElencoView[this.CurrentViewIndex];
                    view3.Nazionalizza();
                }
                break;
            case PreventiviViewIndex:
                final PreventiviView view4 = (PreventiviView) this.ElencoView[this.CurrentViewIndex];
                view4.Nazionalizza();
                break;
            case CompareViewIndex:
                final CompareView view5 = (CompareView) this.ElencoView[this.CurrentViewIndex];
                view5.Nazionalizza();
                break;
            case PrestazioniViewIndex:
                if (this.MobileMode) {
                    final MobilePrestazioniView view6 = (MobilePrestazioniView) this.ElencoMobileView[this.CurrentViewIndex];
                    view6.Nazionalizza();
                    view6.setVentilatoreCurrent();
                } else {
                    final PrestazioniView view6 = (PrestazioniView) this.ElencoView[this.CurrentViewIndex];
                    view6.Nazionalizza();
                    view6.setVentilatoreCurrent();
                }
                break;
            case DimensioniAngoloViewIndex:
                if (this.MobileMode) {
                    final MobileDimensionView view7 = (MobileDimensionView) this.ElencoMobileView[this.CurrentViewIndex];
                    view7.Nazionalizza();
                    view7.setVentilatoreCurrent();
                } else {
                    final DimensionView view7 = (DimensionView) this.ElencoView[this.CurrentViewIndex];
                    view7.Nazionalizza();
                    view7.setVentilatoreCurrent();
                }
                break;
            case DimensioniViewIndex:
                if (this.MobileMode) {
                    final MobileDimensionView view7 = (MobileDimensionView) this.ElencoMobileView[this.CurrentViewIndex];
                    view7.angoloEnabled = false;
                    view7.Nazionalizza();
                    view7.setVentilatoreCurrent();
                } else {
                    final DimensionView view7 = (DimensionView) this.ElencoView[this.CurrentViewIndex];
                    view7.angoloEnabled = false;
                    view7.Nazionalizza();
                    view7.setVentilatoreCurrent();
                }
                break;
            case AccessoriViewIndex:
                if (this.currentUserSaaS.idClienteIndex == 4) {
                    final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.VentilatoreCurrent.campiCliente;
                    if (l_vcc04.Tipo.equals("centrifugo")) {
                        final Cliente04AccessoriViewCentrifughi view8 = (Cliente04AccessoriViewCentrifughi) this.ElencoView[this.AccessoriViewIndex];
                        view8.Nazionalizza();
                        view8.setVentilatoreCurrent();
                    } else if (l_vcc04.Tipo.equals("fusione")) {
                        final Cliente04AccessoriViewFusione view8 = (Cliente04AccessoriViewFusione) this.ElencoView[this.AccessoriViewIndex];
                        view8.Nazionalizza();
                        view8.setVentilatoreCurrent();
                    } else if (l_vcc04.Tipo.equals("assiale")) {
                        final Cliente04AccessoriViewAssiali view8 = (Cliente04AccessoriViewAssiali) this.ElencoView[this.AccessoriViewIndex];
                        view8.Nazionalizza();
                        view8.setVentilatoreCurrent();
                    }
                } else {
                    final AccessoriImageView view8 = (AccessoriImageView) this.ElencoView[this.AccessoriViewIndex];
                    view8.Nazionalizza();
                    view8.setVentilatoreCurrent();
                }
                break;
            case MultigiriViewIndex:
                final MultigiriView view9 = (MultigiriView) this.ElencoView[this.CurrentViewIndex];
                view9.Nazionalizza();
                break;
            case SpettroSonoroViewIndex:
                final SpettroSonoroView view10 = (SpettroSonoroView) this.ElencoView[this.CurrentViewIndex];
                view10.Nazionalizza();
                view10.drawIstogrammi();
                break;
        }
    }

    private Gas readDatiGas(final ResultSet rs) {
        Gas gas = new Gas();
        try {
            gas.descrizione = rs.getString("Descrizione");
            gas.descrizioneGB = rs.getString("DescrizioneGB");
            gas.formula = rs.getString("Formula");
            gas.densitaRelativa = rs.getDouble("DensitaRelativa");
        } catch (final Exception e) {
            gas = null;
        }
        return gas;
    }

    public int RpmMaxTemperatura(final double TemperaturaC, final Ventilatore l_v) {
        if (l_v.RpmLimite[0] < 0 || this.temperatureLimite.length != l_v.RpmLimite.length - 1) {
            return (int) l_v.RpmInit;
        } else {
            int R1 = -1, R2 = -1;
            double T1 = 0., T2 = 0.;
            double a, b;
            if (TemperaturaC <= this.temperatureLimite[0]) {
                return l_v.RpmLimite[0];
            }
            for (int i = 1; i < this.temperatureLimite.length - 1; i++) {
                if (TemperaturaC < this.temperatureLimite[i]) {
                    R1 = l_v.RpmLimite[i - 1];
                    R2 = l_v.RpmLimite[i];
                    T1 = this.temperatureLimite[i - 1];
                    T2 = this.temperatureLimite[i];
                    break;
                }
            }
            if (R2 == -1) {
                R1 = l_v.RpmLimite[this.temperatureLimite.length - 2];
                R2 = l_v.RpmLimite[this.temperatureLimite.length - 1];
                T1 = this.temperatureLimite[this.temperatureLimite.length - 2];
                T2 = this.temperatureLimite[this.temperatureLimite.length - 1];
            }
            a = (R2 - R1) / (T2 - T1);
            b = R1 - a * T1;
            return (int) (a * TemperaturaC + b);
        }
    }

    public void setLinguaFromId(final String idToTest) {
        Lingua lingua = null;
        for (int i = 0; i < this.elencoLingue.size(); i++) {
            if (this.elencoLingue.get(i).id.equals(idToTest)) {
                lingua = this.elencoLingue.get(i);
                break;
            }
        }
        if (lingua == null) {
            return;
        }
        this.currentCitFont.linguaDisplay = (Lingua) lingua.clone();
        this.currentCitFont.idLinguaDisplay = lingua.id;
        this.currentCitFont.linguaOutput = (Lingua) lingua.clone();
        this.currentCitFont.idLinguaOutput = lingua.id;
        this.utilityTraduzioni.setLingua(this.currentCitFont.linguaDisplay.colonnaLinguaForTraduzioni, this.currentCitFont.linguaDisplay.speciale);
        this.utilityTraduzioni.setLinguaOutput(this.currentCitFont.linguaOutput.colonnaLinguaForTraduzioni, this.currentCitFont.linguaOutput.speciale);
        this.dbConfig.storeFont(this.currentCitFont);
        nazionalizzaView();
    }

    public boolean isItaliano() {
        return this.currentCitFont.idLinguaDisplay.equals("IT");
    }

    public void setItaliano() {
        setLinguaFromId("IT");
    }

    public void setInglese() {
        setLinguaFromId("UK");
    }

    /* private String filtroEsecuzione(final Ventilatore l_v, final SelezioneDati selezioneDati) {
        if (selezioneDati.esecuzioneSelezionata.equals("")) {
			return l_v.Esecuzioni[0];
		}
        for (int i=0 ; i<l_v.Esecuzioni.length ; i++) {
            if (l_v.Esecuzioni[i].equals(selezioneDati.esecuzioneSelezionata)) {
                return l_v.Esecuzioni[i];
            }
        }
        return l_v.Esecuzioni[0];
    }
     */
 /*private boolean isVentilatoreRicercaOK(final Ventilatore l_v, final SelezioneDati selezioneDati, final InterscambioRicerca interscambio) {
        double contributoXRumore;
        boolean ret = false;
//modeRicerca = modeRicercaPuntoEffettivo;
        this.VentilatoreSelezionato.pressioneRiferimento = -1.;
        if (selezioneDati.modeRicerca == Costanti.modeRicercaStandard) {         //standard mode
            if (selezioneDati.tolleranzaPortataFlag) {
                if (l_v.Trasmissione) {
                    ret = this.VentilatoreSelezionato.ventilatoreTRPortataFissaMode0(interscambio.NumeroGiriMin, interscambio.NumeroGiriMax);
                } else {
                    ret = this.VentilatoreSelezionato.ventilatoreDAPortataFissaMode0(interscambio.NumeroGiriMax);
                }
            } else {
                if (l_v.Trasmissione) {
                    ret = this.VentilatoreSelezionato.ventilatoreTRPressioneFissaMode0(Costanti.pannelloRicercaStepRpmStartTRPressioneFissa, interscambio.NumeroGiriMin, interscambio.NumeroGiriMax);
                } else {
                    ret = this.VentilatoreSelezionato.ventilatoreDAPressioneFissaMode0(interscambio.NumeroGiriMax);
                }
            }
            
        } else if (selezioneDati.modeRicerca == Costanti.modeRicercaPuntoEffettivo) {          //su punto effettivo
            if (l_v.Trasmissione) {
                ret = this.VentilatoreSelezionato.ventilatoreTRMode1(interscambio.NumeroGiriMin, interscambio.NumeroGiriMax);
            } else {
                ret = this.VentilatoreSelezionato.ventilatoreDAMode1();
            }
        } else {
        }
        if (ret) {
            if (selezioneDati.nBoccheCanalizzate < 0) {
                interscambio.nBoccheAttuali = l_v.NBoccheCanalizzateBase;
            } else {
                interscambio.nBoccheAttuali = selezioneDati.nBoccheCanalizzate;
            }
            contributoXRumore = this.utilityCliente.getCorrettorePotenzaSonora(l_v.NBoccheCanalizzateBase, interscambio.nBoccheAttuali);
            ret = this.VentilatoreSelezionato.applicaUlterioriLimiti(contributoXRumore);
        }
        return ret;
    }
     */
    private void setPrezzoVentilatore(final String from, final Ventilatore l_v) {
        //utilityCliente.gestioneVentilatori.setPrezziVentilatore(l_v);
        l_v.PrezzoVentilatoreForSort = 0.;
        for (int i = 0; i < l_v.Esecuzioni.length; i++) {
            if (l_v.Esecuzioni[i].equals(l_v.selezioneCorrente.Esecuzione)) {
                l_v.PrezzoVentilatoreForSort = l_v.PrezzoEsecuzioni[i];
                break;
            }
        }
        //MainView.setMemo(from+"  "+l_v.Modello+"  "+l_v.selezioneCorrente.Esecuzione+" "+Double.toString(l_v.PrezzoVentilatoreForSort));
    }

    public void setUMFromDefaultIndex() {
        this.UMPressioneCorrente = this.utilityUnitaMisura.getPressioneUM(this.UMDefaultIndex.indexPressione);
        this.UMPortataCorrente = this.utilityUnitaMisura.getPortataUM(this.UMDefaultIndex.indexPortata);
        this.UMPotenzaCorrente = this.utilityUnitaMisura.getPotenzaUM(this.UMDefaultIndex.indexPotenza);
        this.UMAltezzaCorrente = this.utilityUnitaMisura.getLunghezzaUM(this.UMDefaultIndex.indexAltezza);
        this.UMDiametroCorrente = this.utilityUnitaMisura.getLunghezzaUM(this.UMDefaultIndex.indexDiametro);
        this.UMTemperaturaCorrente = this.utilityUnitaMisura.getTemperaturaUM(this.UMDefaultIndex.indexTemperatura);
    }

    private boolean verificaAlmenoUnoAccessori() {
        String almenoUno[];
        int indexAlmenoUno;
        int nSelezionati = 0;
        for (int i = 0; i < this.VentilatoreCurrent.elencoAlmenoUnoAccessori.size(); i++) {
            almenoUno = this.VentilatoreCurrent.elencoAlmenoUnoAccessori.get(i);
            indexAlmenoUno = -1;
            for (int j = 0; j < almenoUno.length; j++) {
                if (almenoUno[j].equals(this.accessorioInEsame.Codice)) {
                    indexAlmenoUno = j;
                    break;
                }
            }
            if (indexAlmenoUno == -1) {
                continue;
            }
            for (int j = 0; j < almenoUno.length; j++) {
                for (int k = 0; k < this.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); k++) {
                    final Accessorio l_a = this.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(k);
                    if (l_a.Codice.equals(almenoUno[j]) && l_a.Selezionato) {
                        nSelezionati++;
                    }
                }
            }
            if (nSelezionati >= 1) {
                return false;
            }
            for (int k = 0; k < this.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); k++) {
                final Accessorio l_a = this.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(k);
                if (l_a.Codice.equals(almenoUno[0])) {
                    l_a.Selezionato = true;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean verificaMutueEsclusioniAccessori() {
        boolean ret = false;
        if (this.accessorioInEsame == null) 
            return ret;
        if (!this.accessorioInEsame.Selezionato) {
            return ret;
        }
        String esclusioni[];
        int indexEsclusioni;
        for (int i = 0; i < this.VentilatoreCurrent.elencoMutueEsclusioniAccessori.size(); i++) {
            esclusioni = this.VentilatoreCurrent.elencoMutueEsclusioniAccessori.get(i);
            indexEsclusioni = -1;
            for (int j = 0; j < esclusioni.length; j++) {
                if (esclusioni[j].equals(this.accessorioInEsame.Codice)) {
                    indexEsclusioni = j;
                    break;
                }
            }
            if (indexEsclusioni == -1) {
                continue;
            }
            ret = true;
            /*
            for (int j=0 ; j<esclusioni.length ; j++) {
            	if (j == indexEsclusioni) continue;
            	this.gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, esclusioni[j], false);
            }
             */

            for (int k = 0; k < this.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); k++) {
                final Accessorio l_a = this.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(k);
                if (l_a.Codice.equals(this.accessorioInEsame.Codice)) {
                    continue;
                }
                for (int j = 0; j < esclusioni.length; j++) {
                    if (l_a.Codice.equals(esclusioni[j])) {
                        l_a.Selezionato = false;
                    }
                }
            }

        }
        return ret;
    }
   
}
