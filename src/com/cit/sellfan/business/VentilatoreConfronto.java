package com.cit.sellfan.business;

import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.ventilatore.Ventilatore;

public class VentilatoreConfronto {
    public Ventilatore cloneVentilatoreForConfronto(Ventilatore ventilatoreIn) {
//Attenzione:
//nello slave non è inizializzato l'ERP, per farlo occorre ERP327Build.set
        Ventilatore l_vTemp = ventilatoreIn.datiERP327.ventilatore327;
        ventilatoreIn.datiERP327.ventilatore327 = null;
        Ventilatore ventilatoreOut = (Ventilatore)ventilatoreIn.clone();
        ventilatoreIn.datiERP327.ventilatore327 = l_vTemp;
        return ventilatoreOut;
    }
    
	public boolean confrontaModello(Ventilatore master, Ventilatore slave) {
		boolean ret = true;
		if (slave == null) ret = false;
		if (ret) {
			if (!master.Serie.equals(slave.Serie) || !master.Modello.equals(slave.Modello) || 
					!master.Versione.equals(slave.Versione) || !master.Classe.equals(slave.Classe) ||
					master.Trasmissione != slave.Trasmissione) {
				ret = false;
			}
		}
		return ret;
	}
	
	public boolean confrontaAccessori(Ventilatore master, Ventilatore slave) {
		boolean ret = confrontaModello(master, slave);
		if (ret) {
			if (master.selezioneCorrente.ElencoAccessori.size() == slave.selezioneCorrente.ElencoAccessori.size()) {
		        for (int i=0 ; i<master.selezioneCorrente.ElencoAccessori.size() ; i++) {
		            Accessorio accessorioMaster = master.selezioneCorrente.ElencoAccessori.get(i);
		            Accessorio accessorioSlave = slave.selezioneCorrente.ElencoAccessori.get(i);
		            if (!accessorioMaster.Codice.equals(accessorioSlave.Codice) || accessorioMaster.Selezionato != accessorioSlave.Selezionato) {
		                ret = false;
		                break;
		            }
		        }
			} else {
				ret = false;
			}
		}
		return ret;
	}
	
	public boolean confrontaFisica(Ventilatore master, Ventilatore slave) {
		boolean ret = confrontaModello(master, slave);
		if (ret) {
			if (master.selezioneCorrente.Marker != slave.selezioneCorrente.Marker || master.selezioneCorrente.NumeroGiri != slave.selezioneCorrente.NumeroGiri ||
					!master.selezioneCorrente.Esecuzione.equals(slave.selezioneCorrente.Esecuzione) || master.CAProgettazione.rho != slave.CAProgettazione.rho) {
				ret = false;
			}
		}
		return ret;
	}
}
