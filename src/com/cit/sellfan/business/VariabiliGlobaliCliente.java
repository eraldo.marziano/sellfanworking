package com.cit.sellfan.business;

import cit.classi.Lingua;
import cit.sellfan.Costanti;

public class VariabiliGlobaliCliente {
	private VariabiliGlobali l_VG = null;
	
	public VariabiliGlobaliCliente(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
        this.l_VG.dbTecnico.temperatureLimite = new int[this.l_VG.nTemperatureLimite];
        this.l_VG.dbTecnico.percentualeRpmLimite = new double[this.l_VG.nTemperatureLimite];
        int index = 0;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 60;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 100;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 150;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 200;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 250;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 300;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 350;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 400;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 450;
        this.l_VG.dbTecnico.temperatureLimite[index++] = 500;
        index = 0;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.96;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.9;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.84;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.77;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.7;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.63;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.55;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.46;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.34;
        this.l_VG.dbTecnico.percentualeRpmLimite[index++] = 0.34;
	}
	
	public void setCliente() {
		switch (this.l_VG.idClienteIndexVG) {
			case 1:
				this.l_VG.catalogoAutoSerieFlag = false;
				this.l_VG.currentLivelloUtente = Costanti.utenteTecnico;
				this.l_VG.sfondoGraficiPrestazioni = "sfondo_grafici.png";
				if (this.l_VG.idSottoClienteIndexVG == 3) {
					this.l_VG.idTranscodificaIndex = 3;
				} else {
					this.l_VG.idTranscodificaIndex = -1;
				}
				break;
			case 4:
				this.l_VG.sfondoGraficiPrestazioni = "sfondo_grafici.png";
				this.l_VG.sfondoGraficiSpettro = "sfondo_grafici.png";
				if (this.l_VG.idSottoClienteIndexVG == 0 || this.l_VG.idSottoClienteIndexVG == 2) {
					this.l_VG.idTranscodificaIndex = -1;
					this.l_VG.currentLivelloUtente = Costanti.utenteTecnico;
				} else if (this.l_VG.idSottoClienteIndexVG == 1 || this.l_VG.idSottoClienteIndexVG == 7) {
					this.l_VG.idTranscodificaIndex = -1;
					this.l_VG.currentLivelloUtente = Costanti.utenteDefault;
				} else if (this.l_VG.idSottoClienteIndexVG == 3) {
					this.l_VG.idTranscodificaIndex = 3;
					this.l_VG.currentLivelloUtente = Costanti.utenteDefault;
				} else if (this.l_VG.idSottoClienteIndexVG == 4) {
					this.l_VG.idTranscodificaIndex = 3;
					this.l_VG.currentLivelloUtente = Costanti.utenteTecnico;
				} else if (this.l_VG.idSottoClienteIndexVG == 5) {
					this.l_VG.idTranscodificaIndex = 8;
					this.l_VG.currentLivelloUtente = Costanti.utenteDefault;
				} else if (this.l_VG.idSottoClienteIndexVG == 6) {
					this.l_VG.idTranscodificaIndex = 8;
					this.l_VG.currentLivelloUtente = Costanti.utenteDefault;
				} else if (this.l_VG.idSottoClienteIndexVG == 8) {
					this.l_VG.idTranscodificaIndex = 8;
					this.l_VG.currentLivelloUtente = Costanti.utenteDefault;
				} else if (this.l_VG.idSottoClienteIndexVG == 9) {
					this.l_VG.idTranscodificaIndex = 9;
					this.l_VG.currentLivelloUtente = Costanti.utenteTecnico;
				} else if (this.l_VG.idSottoClienteIndexVG == 99) {
					this.l_VG.idTranscodificaIndex = 99;
					this.l_VG.currentLivelloUtente = Costanti.utenteDefault;
				}
				break;
		}
	}
	
    public void setClienteLingue() {
        this.l_VG.elencoLingue.clear();
        if (l_VG.utilityCliente.idSottoClienteIndex == 10) {
            Lingua donaldson = Lingua.buildLinguaStandard("do", "donaldson");
            donaldson.colonnaLinguaForTraduzioni = "Lingua6";
            l_VG.elencoLingue.add(donaldson);
            return;
        }
        this.l_VG.elencoLingue.add(Lingua.buildLinguaStandard("it", "Italiano"));
        switch (this.l_VG.utilityCliente.idClienteIndex) {
            case 1:
                this.l_VG.elencoLingue.add(Lingua.buildLinguaStandard("uk", "Inglese"));
                break;
            case 4:
                this.l_VG.elencoLingue.add(Lingua.buildLinguaStandard("uk", "Inglese"));
                this.l_VG.elencoLingue.add(Lingua.buildLinguaStandard("fr", "Francese"));
                this.l_VG.elencoLingue.add(Lingua.buildLinguaStandard("de", "Tedesco"));
                Lingua es = Lingua.buildLinguaStandard("es", "Spagnolo");
                es.colonnaLinguaForTraduzioni = "Lingua5";
                this.l_VG.elencoLingue.add(es);
                break;
            case 98:
            	this.l_VG.elencoLingue.add(Lingua.buildLinguaStandard("uk", "Inglese"));
                break;
        }
    }
}
