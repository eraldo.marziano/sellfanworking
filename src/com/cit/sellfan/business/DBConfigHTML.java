package com.cit.sellfan.business;

import java.util.Vector;

import cit.sellfan.classi.db.DBConfig;

public class DBConfigHTML extends DBConfig {
    
    @SuppressWarnings("rawtypes")
	public ParametriVariHTML loadParametriVariHTML() {
        try {
            final Vector v = readObjectsFromBlob("ParametriVariHTML");
            if (v == null) {
                return new ParametriVariHTML();
            }
            final ParametriVariHTML parametri = (ParametriVariHTML)v.firstElement();
            v.remove(0);
            return parametri;
        } catch (final Exception e) {
            return new ParametriVariHTML();
        }
    }

    public void storeParametriVariHTML(final ParametriVariHTML parametri) {
        final Vector<Object> v = new Vector<>();
        v.add(parametri);
        writeObjectsOnBlob(v, "ParametriVariHTML");
    }
}
