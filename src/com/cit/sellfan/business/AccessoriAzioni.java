package com.cit.sellfan.business;

import java.util.ArrayList;

import com.vaadin.data.Item;

import cit.sellfan.classi.AccessoriGruppo;
import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.Silenziatore;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.ventilatore.Ventilatore;

public class AccessoriAzioni {
	public VariabiliGlobali l_VG;

	public AccessoriAzioni() {
		
	}
	
	public AccessoriAzioni(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void init(final UtilityCliente utilityClienteIn) {
		
	}

	public void eseguiAzioneAccessorioInEsame(final Item item, final Ventilatore l_v, final Accessorio l_a, final ArrayList<AccessoriGruppo> elencoGruppiAccessori) {
		
	}
    public void caricaPopup(Item item, Ventilatore VentilatoreCurrent, Accessorio accessorioInEsame, ArrayList<AccessoriGruppo> elencoGruppiAccessori) {
    }
    public void caricaPopup(Item item, Ventilatore VentilatoreCurrent, Accessorio l_a, ArrayList<AccessoriGruppo> elencoGruppiAccessori, Silenziatore selez) {}
    public void filtraAccessoriDonaldson(Ventilatore VentilatoreCurrent){}
}
