package com.cit.sellfan.business;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.ui.Notification;

import cit.classi.Versione;
import cit.myjavalib.UtilityMath;
import cit.sellfan.Costanti;
import cit.sellfan.classi.AccessoriGruppo;
import cit.sellfan.classi.CategoriaSpettroSonoroSemplificato;
import cit.sellfan.classi.CondizioniAmbientali;
import cit.sellfan.classi.CurvaRegolazioneReynolds;
import cit.sellfan.classi.DBTableQualifier;
import cit.sellfan.classi.Materiale;
import cit.sellfan.classi.Motore;
import cit.sellfan.classi.Serie;
import cit.sellfan.classi.SpettroSonoroCompleto;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.db.LoadClassFromRs;
import cit.sellfan.classi.ventilatore.Ventilatore;
//import cit.sellfan.classi.ventilatore.VentilatoreCurve;
import cit.sellfan.classi.ventilatore.VentilatoreFisicaParameter;
import cit.utility.NumeriRomani;

/**
 *
 * @author Claudio
 * 
 */
public class DBTecnicoLoc {
    public VariabiliGlobali l_VG = null;
    private Connection ConnTecnica;
    private DBTableQualifier dbTableQualifier;
    private UtilityCliente utilityCliente;
    private final UtilityMath utilityMath = new UtilityMath();
    private final LoadClassFromRs loadClassFromRs = new LoadClassFromRs();
    public boolean catalogoAutoSerieFlag = true;
    public boolean pannelloRicercaRpmMotoreDAFlag = false;
    public int temperatureLimite[] = {20, 100, 200, 300, 400, 500, 600};
    public boolean scegliMotoreTRFlag = false;
    public boolean scegliMotoreDAFlag = false;
    public boolean pannelloCatalogoTutteLeClassiFlag = true;
    public double percentualeRpmLimite[] = null;

    public DBTecnicoLoc() {
        //l_VG = variabiliGlobali;
    }
    
    public void init(final Connection ConnTecnicaIn, final DBTableQualifier dbTableQualifierIn, final UtilityCliente utilityClienteIn) {
        this.ConnTecnica = ConnTecnicaIn;
        this.dbTableQualifier = dbTableQualifierIn;
        this.utilityCliente = utilityClienteIn;
    }
    
    public Connection getConnection() {
        return this.ConnTecnica;
    }

    public Versione getVersioneDB() {
        final Versione versioneDB = new Versione(0, 0 , 0, "?");
        int relCombinata = 0;
        int majorTemp = 0;
        int minorTemp = 0;
        try {
            final Statement stmt = this.ConnTecnica.createStatement();
            final String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "RevisioniTecnico" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            final ResultSet rs = stmt.executeQuery(query);
            Object tipo = null;
            while(rs.next()) {
                tipo = rs.getObject("tipo");
                if (tipo == null) {
					break;
				}
                if (tipo.toString().equals("dati")) {
                    majorTemp = rs.getInt("major");
                    minorTemp = rs.getInt("minor");
                    if (majorTemp * 100 + minorTemp > relCombinata) {
                        versioneDB.Major = majorTemp;
                        versioneDB.Minor = minorTemp;
                        versioneDB.Data = rs.getString("data");
                        if (versioneDB.Data.length() > 10) {
							versioneDB.Data = versioneDB.Data.substring(0, 10);
						}
                        relCombinata = majorTemp * 100 + minorTemp;
                    }
                }
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            
        }
        return versioneDB;
    }
    
    public ArrayList<Serie> getElencoSerie(final int idTranscodificaIndex) {
        final ArrayList<Serie> ElencoSerie = new ArrayList<>();
        try {
            Statement stmt = null;
            ResultSet rs = null;
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Serie" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " order by Ordine";
            rs = stmt.executeQuery(query);
            while(rs.next()) {
                final Serie l_s = this.loadClassFromRs.loadSerieFromRs(rs);
                if (l_s != null) {
					ElencoSerie.add(l_s);
				}
            }
            rs.close();
            stmt.close();
            for (int i=0 ; i<ElencoSerie.size() ; i++) {
                this.utilityCliente.transcodificaSerie(idTranscodificaIndex, ElencoSerie.get(i));
            }
        } catch (final Exception e) {
            
        }
        return ElencoSerie;
    }
    
    public ArrayList<Materiale> getElencoMateriali() {
        final ArrayList<Materiale> ElencoMateriali = new ArrayList<>();
        try {
            Statement stmt = null;
            ResultSet rs = null;
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Materiali" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " order by Ordine";
            rs = stmt.executeQuery(query);
            while(rs.next()){
                final Materiale l_m = new Materiale();
                l_m.Codice = rs.getString("Codice");
                if (l_m.Codice == null) {
					break;
				}
                l_m.Descrizione = rs.getString("Descrizione");
                ElencoMateriali.add(l_m);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
//            utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
        }
        return ElencoMateriali;
    }
    
    public VentilatoreFisicaParameter getVentilatoreFisicaParameter() {
        final VentilatoreFisicaParameter ventilatoreFisicaParameter = new VentilatoreFisicaParameter();
        Statement stmt = null;
        ResultSet rs = null;
        final String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "CategorieSpettroSonoro" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
        ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.clear();
        try {
            stmt = this.ConnTecnica.createStatement();
            rs = stmt.executeQuery(query + " where Codice = 'AScale'");
            rs.next();
            ventilatoreFisicaParameter.AScale = new double[9];
            ventilatoreFisicaParameter.AScale[0] = rs.getDouble("Hz31");
            ventilatoreFisicaParameter.AScale[1] = rs.getDouble("Hz63");
            ventilatoreFisicaParameter.AScale[2] = rs.getDouble("Hz125");
            ventilatoreFisicaParameter.AScale[3] = rs.getDouble("Hz250");
            ventilatoreFisicaParameter.AScale[4] = rs.getDouble("Hz500");
            ventilatoreFisicaParameter.AScale[5] = rs.getDouble("Hz1000");
            ventilatoreFisicaParameter.AScale[6] = rs.getDouble("Hz2000");
            ventilatoreFisicaParameter.AScale[7] = rs.getDouble("Hz4000");
            ventilatoreFisicaParameter.AScale[8] = rs.getDouble("Hz8000");
            rs.close();
        } catch (final Exception e) {
            ventilatoreFisicaParameter.AScale = null;
            ventilatoreFisicaParameter.BScale = null;
            ventilatoreFisicaParameter.CScale = null;
            return ventilatoreFisicaParameter;
        }
        try {
            stmt = this.ConnTecnica.createStatement();
            rs = stmt.executeQuery(query + " where Codice = 'BScale'");
            rs.next();
            ventilatoreFisicaParameter.BScale = new double[9];
            ventilatoreFisicaParameter.BScale[0] = rs.getDouble("Hz31");
            ventilatoreFisicaParameter.BScale[1] = rs.getDouble("Hz63");
            ventilatoreFisicaParameter.BScale[2] = rs.getDouble("Hz125");
            ventilatoreFisicaParameter.BScale[3] = rs.getDouble("Hz250");
            ventilatoreFisicaParameter.BScale[4] = rs.getDouble("Hz500");
            ventilatoreFisicaParameter.BScale[5] = rs.getDouble("Hz1000");
            ventilatoreFisicaParameter.BScale[6] = rs.getDouble("Hz2000");
            ventilatoreFisicaParameter.BScale[7] = rs.getDouble("Hz4000");
            ventilatoreFisicaParameter.BScale[8] = rs.getDouble("Hz8000");
            rs.close();
        } catch (final Exception e) {
            ventilatoreFisicaParameter.BScale = null;
            ventilatoreFisicaParameter.CScale = null;
            return ventilatoreFisicaParameter;
        }
        try {
            stmt = this.ConnTecnica.createStatement();
            rs = stmt.executeQuery(query + " where Codice = 'CScale'");
            rs.next();
            ventilatoreFisicaParameter.CScale = new double[9];
            ventilatoreFisicaParameter.CScale[0] = rs.getDouble("Hz31");
            ventilatoreFisicaParameter.CScale[1] = rs.getDouble("Hz63");
            ventilatoreFisicaParameter.CScale[2] = rs.getDouble("Hz125");
            ventilatoreFisicaParameter.CScale[3] = rs.getDouble("Hz250");
            ventilatoreFisicaParameter.CScale[4] = rs.getDouble("Hz500");
            ventilatoreFisicaParameter.CScale[5] = rs.getDouble("Hz1000");
            ventilatoreFisicaParameter.CScale[6] = rs.getDouble("Hz2000");
            ventilatoreFisicaParameter.CScale[7] = rs.getDouble("Hz4000");
            ventilatoreFisicaParameter.CScale[8] = rs.getDouble("Hz8000");
            rs.close();
        } catch (final Exception e) {
            ventilatoreFisicaParameter.CScale = null;
        }
        try {
            rs = stmt.executeQuery(query + " order by Codice");
            while (rs.next()) {
                final CategoriaSpettroSonoroSemplificato l_cs = new CategoriaSpettroSonoroSemplificato();
                l_cs.Codice = rs.getString("Codice");
                if (l_cs.Codice == null) {
					break;
				}
                l_cs.Valore[0] = rs.getDouble("Hz31");
                l_cs.Valore[1] = rs.getDouble("Hz63");
                l_cs.Valore[2] = rs.getDouble("Hz125");
                l_cs.Valore[3] = rs.getDouble("Hz250");
                l_cs.Valore[4] = rs.getDouble("Hz500");
                l_cs.Valore[5] = rs.getDouble("Hz1000");
                l_cs.Valore[6] = rs.getDouble("Hz2000");
                l_cs.Valore[7] = rs.getDouble("Hz4000");
                l_cs.Valore[8] = rs.getDouble("Hz8000");
                l_cs.BFImin = rs.getDouble("BFImin");
                l_cs.BFImax = rs.getDouble("BFImax");
                l_cs.BFIm1 = rs.getDouble("BFIm1");
                l_cs.BFI = rs.getDouble("BFI");
                l_cs.BFIp1 = rs.getDouble("BFIp1");
                ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.add(l_cs);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            //l_VG.utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
        }
        return ventilatoreFisicaParameter;
    }
    
    public ArrayList<Integer> getElencoCodiciCategorieCode() {
        final ArrayList<Integer> elencoCodiciCategorie = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
        String descrizione = null;
        try {
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "CategoriaSerie" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " order by Categoria";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                descrizione = rs.getString("Descrizione");
                if ((descrizione != null) && (!descrizione.equals(""))) {
                    elencoCodiciCategorie.add(rs.getInt("Categoria"));
               }
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            
        }
        return elencoCodiciCategorie;
    }
    
    public ArrayList<String> getElencoCodiciCategorieDescrizione() {
        final ArrayList<String> elencoDescrizioniCategorie = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
        String descrizione = null;
        try {
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "CategoriaSerie" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " order by Categoria";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                descrizione = rs.getString("Descrizione");
                if ((descrizione != null) && (!descrizione.equals(""))) {
                    elencoDescrizioniCategorie.add(descrizione);
                }
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            
        }
        return elencoDescrizioniCategorie;
    }
    
    public boolean initSpettroSonoroCompleto(final Ventilatore l_v, final SpettroSonoroCompleto spettroSonoroCompletoBase, final SpettroSonoroCompleto spettroSonoroCompletoCorrente, final double rhoRiferimento) {
        spettroSonoroCompletoBase.Modello = "";
        spettroSonoroCompletoBase.IDCampionamenti = null;
        spettroSonoroCompletoBase.frequenzeCampionamenti = null;
        spettroSonoroCompletoBase.portateCampionamenti = null;
        spettroSonoroCompletoBase.elencoCampionamenti = null;
        spettroSonoroCompletoBase.isFiltratoBFI = true;
        if (l_v == null) {
			return false;
		}
        try {
            ResultSet rs = null;
            final Statement stmt = this.ConnTecnica.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String query;
            query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "SpettroSonoroCompleto" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " where (Modello = '" + l_v.Modello + "'";
            query += " or Modello = '" + l_v.CurveEquivalenti + "')";
            query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
            query += " and (Classe = '-' or Classe = '" + l_v.Classe + "') ";
            query += " and (Trasmissione = 2 or Trasmissione = ";
            if (l_v.Trasmissione) {
                query += "1) ";
            } else {
                query += "0) ";
            }
            rs = stmt.executeQuery(query + "and TipoRiga = 'Frequenze'");
            rs.next();
            spettroSonoroCompletoBase.Modello = rs.getString("Modello");
            spettroSonoroCompletoBase.RpmBase = rs.getInt("RpmBase");
            spettroSonoroCompletoBase.TipoCurva = rs.getString("TipoCurva");
            if (spettroSonoroCompletoBase.TipoCurva.toLowerCase().startsWith("pres")) {
                spettroSonoroCompletoBase.isPressione = true;
            } else {
                spettroSonoroCompletoBase.isPressione = false;
            }
            spettroSonoroCompletoBase.UM = rs.getString("UM");
            spettroSonoroCompletoBase.NCampionamenti = rs.getInt("NCampionamenti");
            spettroSonoroCompletoBase.DistanzaBasem = rs.getDouble("DistanzaBasem");
            spettroSonoroCompletoBase.NBoccheBase = rs.getInt("NBoccheBase");
            spettroSonoroCompletoBase.DiametroGiranteBasem = rs.getDouble("DiametroGiranteBasem");
            spettroSonoroCompletoBase.NPaleBase = rs.getInt("NPaleBase");
            spettroSonoroCompletoBase.BFIm1 = rs.getDouble("BFIm1");
            spettroSonoroCompletoBase.BFI = rs.getDouble("BFI");
            spettroSonoroCompletoBase.BFIp1 = rs.getDouble("BFIp1");
            spettroSonoroCompletoBase.DensitaFluidoBasekgm3 = rs.getDouble("DensitaFluidoBasekgm3");
            if (spettroSonoroCompletoBase.DensitaFluidoBasekgm3 <= 0.) {
                spettroSonoroCompletoBase.DensitaFluidoBasekgm3 = rhoRiferimento;
            }
            spettroSonoroCompletoBase.NPunti = rs.getInt("NPunti");
            if ((spettroSonoroCompletoBase.frequenzeCampionamenti == null) || (spettroSonoroCompletoBase.frequenzeCampionamenti.length < spettroSonoroCompletoBase.NPunti)) {
                spettroSonoroCompletoBase.frequenzeCampionamenti = new double[spettroSonoroCompletoBase.NPunti];
                spettroSonoroCompletoCorrente.frequenzeCampionamenti = new double[spettroSonoroCompletoBase.NPunti];
            }
            for (int i=0 ; i<spettroSonoroCompletoBase.NPunti ; i++) {
                spettroSonoroCompletoBase.frequenzeCampionamenti[i] = rs.getDouble("P"+Integer.toString(i));
                spettroSonoroCompletoCorrente.frequenzeCampionamenti[i] = rs.getDouble("P"+Integer.toString(i));
                spettroSonoroCompletoBase.elencoCampionamenti = null;
            }
            rs.close();
            if ((spettroSonoroCompletoBase.IDCampionamenti == null) || (spettroSonoroCompletoBase.IDCampionamenti.length < spettroSonoroCompletoBase.NCampionamenti)) {
                spettroSonoroCompletoBase.IDCampionamenti = new String[spettroSonoroCompletoBase.NCampionamenti];
                spettroSonoroCompletoBase.portateCampionamenti = new double[spettroSonoroCompletoBase.NCampionamenti];
                spettroSonoroCompletoCorrente.IDCampionamenti = new String[spettroSonoroCompletoBase.NCampionamenti];
                spettroSonoroCompletoCorrente.portateCampionamenti = new double[spettroSonoroCompletoBase.NCampionamenti];
                spettroSonoroCompletoBase.elencoCampionamenti = null;
            }
            if (spettroSonoroCompletoBase.elencoCampionamenti == null) {
                spettroSonoroCompletoBase.elencoCampionamenti = new double[spettroSonoroCompletoBase.NCampionamenti][spettroSonoroCompletoBase.NPunti];
                spettroSonoroCompletoCorrente.elencoCampionamenti = new double[spettroSonoroCompletoBase.NCampionamenti][spettroSonoroCompletoBase.NPunti];
            }
            rs = stmt.executeQuery(query + "and TipoRiga = 'Valori' order by Progressivo");
            for (int i=0 ; i<spettroSonoroCompletoBase.NCampionamenti ; i++) {
                rs.next();
                spettroSonoroCompletoBase.IDCampionamenti[i] = rs.getString("IDCampionamento");
                spettroSonoroCompletoBase.portateCampionamenti[i] = rs.getDouble("Portatam3h");
                for (int j=0 ; j<spettroSonoroCompletoBase.NPunti ; j++) {
                    spettroSonoroCompletoBase.elencoCampionamenti[i][j] = rs.getDouble("P"+Integer.toString(j));
                }
            }
            rs.close();
            stmt.close();
            return true;
        } catch (final Exception e) {
//            l_VG.utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
            return false;
        }
    }

    public Ventilatore loadVentilatoreForCatalogo(final ResultSet rs, final int nTemp, final boolean PressioneinMandata, final boolean Trasmissione, final double MarkerPortata, final double NumGiri, final CondizioniAmbientali CAProgettazione, final CondizioniAmbientali CARiferimento, final int idTranscodificaIndex) {
        final Ventilatore l_v = new Ventilatore(nTemp, PressioneinMandata);
        l_v.CAProgettazione = (CondizioniAmbientali)CAProgettazione.clone();
        l_v.CARiferimento = (CondizioniAmbientali)CARiferimento.clone();
        l_v.CAFree = (CondizioniAmbientali)CARiferimento.clone();
        loadVentilatoreForCatalogoRicerca(l_v, rs, Trasmissione, MarkerPortata, NumGiri, PressioneinMandata, idTranscodificaIndex);
        return l_v;
    }
    
    public Ventilatore loadVentilatoreForRicerca(final ResultSet rs, final int nTemp, final boolean PressioneinMandata, final boolean Trasmissione, final double MarkerPortata, final double NumGiri, final CondizioniAmbientali CAProgettazione, final CondizioniAmbientali CARiferimento, final int idTranscodificaIndex) {
        final Ventilatore l_v = new Ventilatore(nTemp, PressioneinMandata);
        l_v.CAProgettazione = (CondizioniAmbientali)CAProgettazione.clone();
        l_v.CARiferimento = (CondizioniAmbientali)CARiferimento.clone();
        l_v.CAFree = (CondizioniAmbientali)CARiferimento.clone();
        loadVentilatoreForCatalogoRicerca(l_v, rs, Trasmissione, MarkerPortata, NumGiri, PressioneinMandata, idTranscodificaIndex);
        return l_v;
    }
    
    private void loadVentilatoreForCatalogoRicerca(final Ventilatore l_v, final ResultSet rs, final boolean Trasmissione, final double MarkerPortata, final double NumGiri, final boolean pressioneInMandata, final int idTranscodificaIndex) {
        l_v.CACurve = (CondizioniAmbientali)Costanti.CADefault.clone();  //impostato da VentilatoreFisica
        precaricaVentilatore(l_v, rs, Trasmissione, MarkerPortata, NumGiri, true);
        l_v.PressioneMandataFlag = pressioneInMandata;            //default i ventilatori sono in mandata
        this.utilityCliente.loadVentilatoreCampiCliente(l_v);
        l_v.selezioneCorrente.NumeroGiri = l_v.RpmInit;
        l_v.MotoreOriginale = this.utilityCliente.condizionaCodiceMotore5060Hz(l_v.MotoreOriginale, l_v.CAProgettazione.frequenza);
        this.utilityCliente.loadMotore(l_v, l_v.MotoreOriginale);
        l_v.condizionaRhoVentilatore();
        this.utilityCliente.transcodificaVentilatore(idTranscodificaIndex, l_v);
     }
    
    private void precaricaVentilatore(final Ventilatore l_v, final ResultSet rs, final boolean Trasmissione, final double MarkerPortata, final double NumGiri, final boolean soloFisica) {
       try {
            if (!loadFromTabellaVentilatore(l_v, Trasmissione, rs, MarkerPortata, NumGiri)) {
				return;
			}
            if (!this.catalogoAutoSerieFlag && l_v.Trasmissione && l_v.RpmInit > l_v.RpmMaxTeorici) {
                l_v.RpmInit = l_v.RpmMaxTeorici;
            }
            if (!soloFisica) {
				this.utilityCliente.caricaPrezziVentilatore(l_v);
			}
            return;
        } catch (final Exception e) {
            return;
        }
    }

    public int RpmMaxTemperatura(final double TemperaturaC, final Ventilatore l_v) {
        if (l_v.RpmLimite[0] < 0 || this.temperatureLimite.length != l_v.RpmLimite.length - 1) {
            return (int)l_v.RpmInit;
        } else {
            int R1 = -1, R2 = -1;
            double T1 = 0., T2 = 0.;
            double a, b;
            if (TemperaturaC <= this.temperatureLimite[0]) {
                return l_v.RpmLimite[0];
            }
            for (int i=1 ; i<this.temperatureLimite.length - 1 ; i++) {
                if (TemperaturaC < this.temperatureLimite[i]) {
                    R1 = l_v.RpmLimite[i - 1];
                    R2 = l_v.RpmLimite[i];
                    T1 = this.temperatureLimite[i -1];
                    T2 = this.temperatureLimite[i];
                    break;
                }
            }
            if (R2 == -1) {
                R1 = l_v.RpmLimite[this.temperatureLimite.length - 2];
                R2 = l_v.RpmLimite[this.temperatureLimite.length - 1];
                T1 = this.temperatureLimite[this.temperatureLimite.length - 2];
                T2 = this.temperatureLimite[this.temperatureLimite.length - 1];
            }
            a = (R2 - R1) / (T2 - T1);
            b = R1 - a * T1;
            return (int) (a * TemperaturaC + b);
        }
    }

    private boolean loadFromTabellaVentilatore(final Ventilatore l_v, final boolean Trasmissione, final ResultSet rs, final double MarkerPortata, final double NumGiri) {
        try {
            l_v.Trasmissione = Trasmissione;
            this.loadClassFromRs.loadVentilatoreFromRs(l_v, rs);
            this.utilityCliente.condizionaVentilatore5060Hz(l_v);
            if (this.pannelloRicercaRpmMotoreDAFlag && !l_v.Trasmissione) {
                this.utilityCliente.loadMotore(l_v, l_v.MotoreOriginale);
                if (l_v.selezioneCorrente.MotoreInstallato.Rpm > 0) {
                    l_v.RpmInit = l_v.selezioneCorrente.MotoreInstallato.Rpm;
                }
            }
            if (this.catalogoAutoSerieFlag && l_v.Classe.equals("-")) {
                setVentilatoreClasseMinima(l_v);
            }
            l_v.RpmMaxTeorici = RpmMaxTemperatura(l_v.CAProgettazione.temperatura, l_v);
            if (this.utilityCliente.isAlberoNudo(l_v.Esecuzioni[0])) {
                if (l_v.selezioneCorrente.MotoreInstallato == null) {
					l_v.selezioneCorrente.MotoreInstallato = new Motore();
				} else {
					l_v.selezioneCorrente.MotoreInstallato.reset();
				}
            }
            /*else {
                String l_motore = null;
                if (l_v.Trasmissione) {
                    if (this.scegliMotoreTRFlag && l_v.MotoreOriginale.equals("-")) {
                        l_motore = this.utilityCliente.ScegliMotore(l_v, (l_v.PotenzaMax + l_v.PotAss)/2., l_v.RpmInit, l_v.PD2);
                    }
                } else {
                    if (this.scegliMotoreDAFlag && l_v.MotoreOriginale.equals("-")) {
                        l_motore = this.utilityCliente.ScegliMotore(l_v, (l_v.PotenzaMax + l_v.PotAss)/2., l_v.RpmInit, l_v.PD2);
                    }
                }
            }*/
            if (NumGiri < 0) {
                l_v.selezioneCorrente.NumeroGiri = l_v.RpmInit;
            } else {
                l_v.selezioneCorrente.NumeroGiri = NumGiri;
            }
            if (MarkerPortata < 0.) {
                l_v.selezioneCorrente.Marker = (l_v.PortataMax + l_v.PortataMin) / 2.;
            } else {
                l_v.selezioneCorrente.Marker = MarkerPortata;
            }
            return true;
        } catch (final Exception e) {
            return false;
        }
    }
    
    public void setVentilatoreClasseMinima(final Ventilatore l_v) {
        try {
            l_v.elencoClassi.clear();
            l_v.elencoRpmClassi.clear();
            Statement stmt = null;
            ResultSet rs = null;
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Classi";
            query += this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where Modello = '" + l_v.Modello + "'";
            query += " and (Materiale = '" + l_v.Materiale + "' or Materiale = '-')";
            query += " and (Frequenza = " + l_v.CAProgettazione.frequenza + " or Frequenza = -1)";
            query += " and (Trasmissione = 2 or Trasmissione = ";
            if (l_v.Trasmissione) {
                query += "1)";
            } else {
                query += "0)";
            }
            rs = stmt.executeQuery(query);
            rs.next();
            int rpm = -1;
            for (int i=1 ; i<9 ; i++) {
                rpm = rs.getInt("RpmCl" + Integer.toString(i));
                if (rpm > 0) {
                    l_v.elencoClassi.add(i);
                    l_v.elencoRpmClassi.add(rpm);
                }
            }
            rs.close();
            stmt.close();
            if (!l_v.elencoClassi.isEmpty()) {
                if (!l_v.Trasmissione && !this.pannelloCatalogoTutteLeClassiFlag) {
                    for (int i=l_v.elencoClassi.size() - 1 ; i>=0 ; i--) {
                        l_v.RpmLimite[0] = l_v.elencoRpmClassi.get(i);
                        this.utilityCliente.fillRpmLimite(l_v);
                        l_v.RpmMaxTeorici = RpmMaxTemperatura(l_v.CAProgettazione.temperatura, l_v);
                        if (l_v.RpmInit > l_v.RpmMaxTeorici) {
                            l_v.elencoClassi.remove(i);
                            l_v.elencoRpmClassi.remove(i);
                        }
                    }
                }
                if (!l_v.elencoClassi.isEmpty()) {
                    l_v.Classe = NumeriRomani.daAraboARomano(l_v.elencoClassi.get(0));
                    l_v.RpmLimite[0] = l_v.elencoRpmClassi.get(0);
                    this.utilityCliente.fillRpmLimite(l_v);
                    l_v.RpmMaxTeorici = RpmMaxTemperatura(l_v.CAProgettazione.temperatura, l_v);
                    if (l_v.Trasmissione && l_v.RpmInit > l_v.RpmMaxTeorici) {
                        l_v.RpmInit = l_v.RpmMaxTeorici;
                    }
                }
            }
        } catch (final Exception e) {
            System.out.println("setVentilatoreClasseMinima "+l_v.Modello+"  "+e);
        }
    }

    public int[] getAngoliPossibili(final Ventilatore l_v) {
        int angoliPossibili[];
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Rotazioni" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " where Serie = '" + l_v.Serie + "' and Modello = '" + l_v.Modello + "'";
            query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
            query += " and (Trasmissione = 2 or Trasmissione = ";
            if (l_v.Trasmissione) {
                query += "1)";
            } else {
                query += "0)";
            }
            query += " order by Versione desc";
            rs = stmt.executeQuery(query);
            rs.next();
            final int na = rs.getInt("NAngoli");
            angoliPossibili= new int[na];
            for (int i=0 ; i<na ; i++) {
                angoliPossibili[i] = rs.getInt("ALFA" + Integer.toString(i));
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            try {
                rs.close();
                stmt.close();
            } catch (final Exception e1) {
                
            }
            angoliPossibili = Costanti.angoliPossibiliDefault;
        }
        return angoliPossibili;
    }
    
    public int getAngoloCurrent(final int angoliPossibiliCurrent[], final int angoloRotazioneDefault) {
        if (angoliPossibiliCurrent.length <= 0) {
			return angoloRotazioneDefault;
		};
        for (int i=0 ; i<angoliPossibiliCurrent.length ; i++) {
            if (angoliPossibiliCurrent[i] == angoloRotazioneDefault) {
                return angoloRotazioneDefault;
            }
        }
        return angoliPossibiliCurrent[0];
    }

    public ArrayList<AccessoriGruppo> getAccessoriGruppi() {
        final ArrayList<AccessoriGruppo> elencoGruppiAccessori = new ArrayList<>();
        try {
            final Statement stmt = this.ConnTecnica.createStatement();
            final String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "AccessoriGruppi" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            final ResultSet rs = stmt.executeQuery(query);
            AccessoriGruppo l_ag;
            while (rs.next()) {
                l_ag = new AccessoriGruppo();
                l_ag.idGruppo = rs.getString("IDGruppo");
                l_ag.titoloTabGruppo = rs.getString("TabGruppo");
                l_ag.descrizioneGruppo = rs.getString("DescrizioneGruppo");
                elencoGruppiAccessori.add(l_ag);
            }
            l_ag = new AccessoriGruppo();
            l_ag.idGruppo = "??";
            l_ag.titoloTabGruppo = "Liberi";
            l_ag.descrizioneGruppo = "Accessori Liberi";
            elencoGruppiAccessori.add(l_ag);
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            elencoGruppiAccessori.clear();
        }
        return elencoGruppiAccessori;
    }

    public void initAlmenoUnoAccessori(final Ventilatore l_v) {
        final String almenoUno[] = new String[16];
        int nAlmenoUno;
        Statement stmt = null;
        ResultSet rs = null;
        l_v.elencoAlmenoUnoAccessori.clear();
        try {
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "AccessoriAlmenoUno" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " where (Trasmissione = 2 or Trasmissione = ";
            if (l_v.Trasmissione == true) {
                query += "1)";
            } else {
                query += "0)";
            }
            query += " and (Serie = '" + l_v.Serie + "' or Serie = '-')";
            query += " and (Modello = '" + l_v.Modello + "' or Modello = '-')";
            query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
            query += " and (Classe = '" + l_v.Classe + "' or Classe = '-')";
            query += " and (Materiale = '" + l_v.Materiale + "' or Materiale = '-')";
            query += " and (Orientamento = '" + l_v.selezioneCorrente.Orientamento + "' or Orientamento = '-')";
            query += " and (Esecuzione = '" + l_v.selezioneCorrente.Esecuzione + "' or Esecuzione = '-')";
            query += " order by Versione desc";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                nAlmenoUno = 0;
                for (int i=0 ; i<16 ; i++) {
                    almenoUno[i] = rs.getString("P" + Integer.toString(i));
                    if (!almenoUno[i].equals("-")) {
						nAlmenoUno++;
					}
                }
                final String almenoUnoOK[] = new String[nAlmenoUno];
                System.arraycopy(almenoUno, 0, almenoUnoOK, 0, nAlmenoUno);
                l_v.elencoAlmenoUnoAccessori.add(almenoUnoOK);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
//            l_VG.utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
            l_v.elencoAlmenoUnoAccessori.clear();
        }
    }

    public void initMutueEsclusioniAccessori(final Ventilatore l_v) {
        final String esclusioni[] = new String[16];
        int nEsclusioni;
        Statement stmt = null;
        ResultSet rs = null;
        l_v.elencoMutueEsclusioniAccessori.clear();
        try {
            stmt = this.ConnTecnica.createStatement();
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "AccessoriEsclusioni" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " where (Trasmissione = 2 or Trasmissione = ";
            if (l_v.Trasmissione == true) {
                query += "1)";
            } else {
                query += "0)";
            }
            query += " and (Serie = '" + l_v.Serie + "' or Serie = '-')";
            query += " and (Modello = '" + l_v.Modello + "' or Modello = '-')";
            query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
            query += " and (Classe = '" + l_v.Classe + "' or Classe = '-')";
            query += " and (Materiale = '" + l_v.Materiale + "' or Materiale = '-')";
            query += " and (Orientamento = '" + l_v.selezioneCorrente.Orientamento + "' or Orientamento = '-')";
            query += " and (Esecuzione = '" + l_v.selezioneCorrente.Esecuzione + "' or Esecuzione = '-')";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                nEsclusioni = 0;
                for (int i=0 ; i<16 ; i++) {
                    esclusioni[i] = rs.getString("P" + Integer.toString(i));
                    if (!esclusioni[i].equals("-")) {
						nEsclusioni++;
					}
                }
                final String esclusioniOK[] = new String[nEsclusioni];
                System.arraycopy(esclusioni, 0, esclusioniOK, 0, nEsclusioni);
                l_v.elencoMutueEsclusioniAccessori.add(esclusioniOK);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            l_v.elencoMutueEsclusioniAccessori.clear();
        }
    }

    public void loadCurveRegolazioneReynolds(final VentilatoreFisicaParameter ventilatoreFisicaParameter) {
        ventilatoreFisicaParameter.curveRegolazioneDapoPressioneReynolds.clear();
        ventilatoreFisicaParameter.curveRegolazioneSerrandaPressioneReynolds.clear();
        final String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "CurveRegolazioneReynolds" + this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where ";
        String query1 = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            query1 = "Dapo = 1 and Serranda = 0 and (Tipo = 'ps' or Tipo = 'pt') order by Parametro";
            stmt = this.ConnTecnica.createStatement();
            rs = stmt.executeQuery(query + query1);
            while (rs.next()) {
                final CurvaRegolazioneReynolds cr = new CurvaRegolazioneReynolds();
                cr.parametro = rs.getDouble("Parametro");
                cr.Re1000Min = rs.getDouble("Re1000Min");
                cr.Re1000Max = rs.getDouble("Re1000Max");
                cr.tipo = rs.getString("Tipo");
                this.utilityMath.parseFormula6Grado(rs.getString("Curva"), cr.ACurva);
                ventilatoreFisicaParameter.curveRegolazioneDapoPressioneReynolds.add(cr);
            }
            rs.close();
        } catch (final Exception e) {
            ventilatoreFisicaParameter.curveRegolazioneDapoPressioneReynolds.clear();
        }
        try {
            query1 = "Dapo = 0 and Serranda = 1 and (Tipo = 'ps' or Tipo = 'pt') order by Parametro";
            rs = stmt.executeQuery(query + query1);
            while (rs.next()) {
                final CurvaRegolazioneReynolds cr = new CurvaRegolazioneReynolds();
                cr.parametro = rs.getDouble("Parametro");
                cr.Re1000Min = rs.getDouble("Re1000Min");
                cr.Re1000Max = rs.getDouble("Re1000Max");
                cr.tipo = rs.getString("Tipo");
                this.utilityMath.parseFormula6Grado(rs.getString("Curva"), cr.ACurva);
                ventilatoreFisicaParameter.curveRegolazioneSerrandaPressioneReynolds.add(cr);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            ventilatoreFisicaParameter.curveRegolazioneSerrandaPressioneReynolds.clear();
        }
    }
    
    public String[] getMapEsecuzioni(final String tipoCode, final String tipoDesc, final HashMap<String, String> mapEsecuzioniDesc) {
        try {
            int n;
            Statement stmt = null;
            ResultSet rs = null;
            stmt = this.ConnTecnica.createStatement();
            String query;
            query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Esecuzioni" + this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where tipo = '";
            rs = stmt.executeQuery(query + tipoCode + "'");
            rs.next();
            n = rs.getInt("N");
            final String EsecuzioniTR1[] = new String[n];
            for (int i=0 ; i<n ; i++) {
                EsecuzioniTR1[i] = rs.getString("E" + Integer.toString(i));
            }
            rs.close();
            
            rs = stmt.executeQuery(query + tipoDesc + "'");
            rs.next();
            for (int i=0 ; i<n ; i++) {
                mapEsecuzioniDesc.put(EsecuzioniTR1[i], rs.getString("E" + Integer.toString(i)));
            }
            rs.close();
            stmt.close();
            return EsecuzioniTR1;
        } catch (final Exception e) {
            return null;
        }
    }
/*
    public void buildMapEsecuzioniDesc() {
        try {
            int n;
            Statement stmt = null;
            ResultSet rs = null;
            stmt = ConnTecnica.createStatement();
            String query;
            query = "select * from " + dbTableQualifier.dbTableQualifierPreDBTecnico + "Esecuzioni" + dbTableQualifier.dbTableQualifierPostDBTecnico + " where tipo = '";
            rs = stmt.executeQuery(query + "TR1'");
            rs.next();
            n = rs.getInt("N");
            l_VG.pannelloRicercaEsecuzioniTR1 = new String[n];
            for (int i=0 ; i<n ; i++) {
                l_VG.pannelloRicercaEsecuzioniTR1[i] = rs.getString("E" + Integer.toString(i));
            }
            rs.close();
            
            rs = stmt.executeQuery(query + "TRDesc1'");
            rs.next();
            for (int i=0 ; i<n ; i++) {
                l_VG.mapEsecuzioniDesc.put(l_VG.pannelloRicercaEsecuzioniTR1[i], rs.getString("E" + Integer.toString(i)));
            }
            rs.close();
            rs = stmt.executeQuery(query + "TR2'");
            rs.next();
            n = rs.getInt("N");
            l_VG.pannelloRicercaEsecuzioniTR2 = new String[n];
            for (int i=0 ; i<n ; i++) {
                l_VG.pannelloRicercaEsecuzioniTR2[i] = rs.getString("E" + Integer.toString(i));
            }
            rs.close();
            rs = stmt.executeQuery(query + "TRDesc2'");
            rs.next();
            for (int i=0 ; i<n ; i++) {
                l_VG.mapEsecuzioniDesc.put(l_VG.pannelloRicercaEsecuzioniTR2[i], rs.getString("E" + Integer.toString(i)));
            }
            rs.close();
            rs = stmt.executeQuery(query + "DA'");
            rs.next();
            n = rs.getInt("N");
            l_VG.pannelloRicercaEsecuzioniDA = new String[n];
            for (int i=0 ; i<n ; i++) {
                l_VG.pannelloRicercaEsecuzioniDA[i] = rs.getString("E" + Integer.toString(i));
            }
            rs.close();
            rs = stmt.executeQuery(query + "DADesc'");
            rs.next();
            for (int i=0 ; i<n ; i++) {
                l_VG.mapEsecuzioniDesc.put(l_VG.pannelloRicercaEsecuzioniDA[i], rs.getString("E" + Integer.toString(i)));
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            l_VG.utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
        }
    }
*/
    public void caricaDimensioni(final Ventilatore l_v, boolean fusione) {
        Statement stmt = null;
        ResultSet rs = null;
        int Index_L = -1;
        int Index_PD2 = -1;
        int Index_PesoTotale = -1;
        l_v.DIM_n = 0;
        String query = "";
        try {
            stmt = this.ConnTecnica.createStatement();
            query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Dimensioni" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " where Serie = '" + l_v.Serie + "' and Modello = 'Label'";
            query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
            query += " and (Trasmissione = 2 or Trasmissione = ";
            if (l_v.Trasmissione == true) {
                query += "1)";
            } else {
                query += "0)";
            }
            query += " order by Versione desc";
            rs = stmt.executeQuery(query);
            rs.next();
            l_v.DIM_n = rs.getInt("NParametri");
            if (l_v.DIM_n > Costanti.NMaxParametriDimensioni) {
                l_v.DIM_n = Costanti.NMaxParametriDimensioni;
            }
            for (int i = 0; i < l_v.DIM_n; i++) {
                l_v.DIM_Label[i] = rs.getString("P"+ Integer.toString(i));
                if (l_v.DIM_Label[i].equals("L")) {
                    Index_L = i;
                }
                if (l_v.DIM_Label[i].toUpperCase().equals("PD2")) {
                    Index_PD2 = i;
                }
                if (l_v.DIM_Label[i].toLowerCase().equals("kg")) {
                    Index_PesoTotale = i;
                }
            }
            rs.close();
            stmt.close();
            stmt = this.ConnTecnica.createStatement();
            try {
                query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + "Dimensioni" + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
                query += " where (Trasmissione = 2 or Trasmissione = ";
                if (l_v.Trasmissione) {
                    query += "1)";
                } else {
                    query += "0)";
                }
                query += " and (Modello = '" + l_v.Modello + "' or Modello = '-')";
                query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
                query += " and (Classe = '" + l_v.Classe + "' or Classe = '-')";
                query += " and (Esecuzione = '" + l_v.selezioneCorrente.Esecuzione + "' or Esecuzione = '-')";
                query += " order by Versione desc";
                rs = stmt.executeQuery(query);
                rs.next();
                l_v.DIM_Img = rs.getString("Immagine");
                for (int i = 0; i < l_v.DIM_n; i++) {
                    l_v.DIM_Val[i] = rs.getString("P"+ Integer.toString(i));
                    if (Index_L == i) {
                        l_v.DIM_L = l_v.DIM_Val[i];
                    }
                    if (Index_PD2 == i) {
                        l_v.DIM_PD2 = l_v.DIM_Val[i];
                    }
                    if (Index_PesoTotale == i) {
                        l_v.DIM_PesoTotale = l_v.DIM_Val[i];
                    }
                }
                rs.close();
                stmt.close();
            } catch (final Exception e1) {
                l_v.DIM_Img = null;
                for (int i = 0; i < l_v.DIM_n; i++) {
                    l_v.DIM_Val[i] = "-";
                }
                l_v.DIM_L = null;
                l_v.DIM_PD2 = null;
                l_v.DIM_PesoTotale = null;
//                l_VG.utilityLog.error(getClass(), e1.getStackTrace(), e1.getMessage());
            }
        } catch (final Exception e) {
//            l_VG.utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
            return;
        }
    }

    public boolean caricaDimensioniEsecuzione(final Ventilatore l_v, boolean fusione) {
        if (!caricaDimensioniOrientamentoEsecuzione(l_v, true)) {
            return caricaDimensioniOrientamentoEsecuzione(l_v, false);
        } else {
            return true;
        }
    }
    
    public boolean caricaDimensioniOrientamentoEsecuzione(final Ventilatore l_v, final boolean orientamentoAngolo) {
        Statement stmt = null;
        ResultSet rs = null;
        int Index_PD2 = -1;
        int Index_PesoTotale = -1;
        String tabella = null;
        if (orientamentoAngolo) {
            tabella = "DimensioniOrientamento";
        } else {
            tabella = "DimensioniEsecuzione";
        }
        String query = "";
        l_v.DIMOrientamento_n = 0;
//        int grandezzaMotore = l_v.selezioneCorrente.MotoreInstallato.Grandezza;
        try {
            stmt = this.ConnTecnica.createStatement();
            query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + tabella + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " where Serie = '" + l_v.Serie + "' and Modello = 'Label'";
            query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
            query += " and (Trasmissione = 2 or Trasmissione = ";
            if (l_v.Trasmissione == true) {
                query += "1)";
            } else {
                query += "0)";
            }
            query += " order by Versione desc";
            rs = stmt.executeQuery(query);
            rs.next();
            l_v.DIMOrientamento_n = rs.getInt("NParametri");
            if (l_v.DIMOrientamento_n > Costanti.NMaxParametriDimensioni) {
                l_v.DIMOrientamento_n = Costanti.NMaxParametriDimensioni;
            }
            String NomeCampo;
            for (int i = 0; i < l_v.DIMOrientamento_n; i++) {
                NomeCampo = "P"+ Integer.toString(i);
                l_v.DIMOrientamento_Label[i] = rs.getString(NomeCampo);
                if (l_v.DIMOrientamento_Label[i].toUpperCase().equals("PD2")) {
                    Index_PD2 = i;
                }
                if (l_v.DIMOrientamento_Label[i].toLowerCase().equals("kg")) {
                    Index_PesoTotale = i;
                }
            }
            rs.close();
            stmt.close();
            stmt = this.ConnTecnica.createStatement();
            try {
                query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico + tabella + this.dbTableQualifier.dbTableQualifierPostDBTecnico;
                query += " where (Trasmissione = 2 or Trasmissione = ";
                if (l_v.Trasmissione) {
                    query += "1)";
                } else {
                    query += "0)";
                }
                query += " and (Modello = '" + l_v.Modello + "' or Modello = '-')";
                query += " and (Versione = '-' or Versione = '" + l_v.Versione + "')";
                query += " and (Classe = '" + l_v.Classe + "' or Classe = '-')";
                query += " and (Esecuzione = '" + l_v.selezioneCorrente.Esecuzione + "' or Esecuzione = '-')";
                if (orientamentoAngolo) {
                    query += " and (Orientamento = '" + l_v.selezioneCorrente.Orientamento + "' or Orientamento = '-')";
                    query += " and (Angolo = " + Integer.toString(l_v.selezioneCorrente.OrientamentoAngolo) + " or Angolo = -1)";
                }
                if (l_v.selezioneCorrente.MotoreInstallato != null && !l_v.selezioneCorrente.MotoreInstallato.CodiceCliente.equals("-")) {
                    query += " and (GrandezzaMotore = " + Integer.toString(l_v.selezioneCorrente.MotoreInstallato.Grandezza) + " or GrandezzaMotore = -1)";
                }
                query += " order by Versione desc";
                rs = stmt.executeQuery(query);
                rs.next();
                l_v.DIMOrientamento_Img = rs.getString("Immagine");
                try {                   //aggiunta dalla versione dati 2.3, compatibile con versione 2.2 o superiori
                    l_v.DIMOrientamento_ImgQ1 = rs.getString("ImmagineQ1");
                } catch (final Exception e) {
                    l_v.DIMOrientamento_ImgQ1 = null;
                }
                try {                   //aggiunta dalla versione dati 2.3, compatibile con versione 2.2 o superiori
                    l_v.DIMOrientamento_ImgQ2 = rs.getString("ImmagineQ2");
                } catch (final Exception e) {
                    l_v.DIMOrientamento_ImgQ2 = null;
                }
                try {                   //aggiunta dalla versione dati 2.3, compatibile con versione 2.2 o superiori
                    l_v.DIMOrientamento_ImgQ3 = rs.getString("ImmagineQ3");
                } catch (final Exception e) {
                    l_v.DIMOrientamento_ImgQ3 = null;
                }
                try {                   //aggiunta dalla versione dati 2.3, compatibile con versione 2.2 o superiori
                    l_v.DIMOrientamento_ImgQ4 = rs.getString("ImmagineQ4");
                } catch (final Exception e) {
                    l_v.DIMOrientamento_ImgQ4 = null;
                }
                if (!orientamentoAngolo) {
                    if (l_v.DIMOrientamento_ImgQ1 != null && !l_v.DIMOrientamento_ImgQ1.equals("-")) {
                        l_v.DIMOrientamento_ImgQ1 = l_v.DIMOrientamento_ImgQ1.replace("alfa", Integer.toString(l_v.selezioneCorrente.OrientamentoAngolo)).replace("orientamento", l_v.selezioneCorrente.Orientamento);
                    }
                    if (l_v.DIMOrientamento_ImgQ2 != null && !l_v.DIMOrientamento_ImgQ2.equals("-")) {
                        l_v.DIMOrientamento_ImgQ2 = l_v.DIMOrientamento_ImgQ2.replace("alfa", Integer.toString(l_v.selezioneCorrente.OrientamentoAngolo)).replace("orientamento", l_v.selezioneCorrente.Orientamento);
                    }
                    if (l_v.DIMOrientamento_ImgQ3 != null && !l_v.DIMOrientamento_ImgQ3.equals("-")) {
                        l_v.DIMOrientamento_ImgQ3 = l_v.DIMOrientamento_ImgQ3.replace("alfa", Integer.toString(l_v.selezioneCorrente.OrientamentoAngolo)).replace("orientamento", l_v.selezioneCorrente.Orientamento);
                    }
                    if (l_v.DIMOrientamento_ImgQ4 != null && !l_v.DIMOrientamento_ImgQ4.equals("-")) {
                        l_v.DIMOrientamento_ImgQ4 = l_v.DIMOrientamento_ImgQ4.replace("alfa", Integer.toString(l_v.selezioneCorrente.OrientamentoAngolo)).replace("orientamento", l_v.selezioneCorrente.Orientamento);
                    }
                }
                for (int i = 0; i < l_v.DIMOrientamento_n; i++) {
                    NomeCampo = "P"+ Integer.toString(i);
                    l_v.DIMOrientamento_Val[i] = rs.getString(NomeCampo);
                    if (Index_PD2 == i) {
                        l_v.DIMOrientamento_PD2 = l_v.DIMOrientamento_Val[i];
                    }
                    if (Index_PesoTotale == i) {
                        l_v.DIMOrientamento_PesoTotale = l_v.DIMOrientamento_Val[i];
                    }
                }
                rs.close();
                stmt.close();
            } catch (final Exception e1) {
//                System.out.println(query);
                l_v.DIMOrientamento_Img = null;
                for (int i = 0; i < l_v.DIMOrientamento_n; i++) {
                    l_v.DIMOrientamento_Val[i] = "-";
                }
                l_v.DIMOrientamento_PD2 = null;
                l_v.DIMOrientamento_PesoTotale = null;
                return false;
            }
        } catch (final Exception e) {
            l_v.DIMOrientamento_n = 0;
//            utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
            return false;
        }
        return true;
    }
    
    public void loadCatalogo(final ArrayList<Ventilatore> ElencoVentilatoriFromCatalogo, final String serie, final int nTemp, final boolean PressioneinMandata, final boolean trasmissione, final CondizioniAmbientali CAProgettazione, final CondizioniAmbientali CARiferimento, final int idTranscodificaIndex) {
        ElencoVentilatoriFromCatalogo.clear();
        try {
            String query = "select * from " + this.dbTableQualifier.dbTableQualifierPreDBTecnico;
            if (trasmissione) {
                query += "Trasmissione";
            } else {
                query += "DirettamenteAccoppiati";
            }
            query += this.dbTableQualifier.dbTableQualifierPostDBTecnico + " where Serie = '" + serie + "'";
            final Statement stmt = this.ConnTecnica.createStatement();
            Notification.show(query);
            final ResultSet rs = stmt.executeQuery(query + " and Modello <> '" + serie + " MASTER'");
            while(rs.next()) {
            	//this.l_VG.MainView.addToMemo("x");
                final Ventilatore l_v = loadVentilatoreForCatalogo(rs, nTemp, PressioneinMandata, trasmissione, -1, -1, CAProgettazione, CARiferimento, idTranscodificaIndex);
                if (l_v == null) {
                	Notification.show("l_v==null");
					continue;
				}
                if (this.catalogoAutoSerieFlag && (l_v.elencoClassi.isEmpty() || l_v.elencoRpmClassi.isEmpty())) {
                	Notification.show("elencoClassi");
					continue;
				}
                ElencoVentilatoriFromCatalogo.add(l_v);
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            //l_VG.utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
        }
    }
    
    public ArrayList<Integer> getTramogge(final String dimensione) {
    	final ArrayList<Integer> risultati = new ArrayList<>();
        try {
    		final String search = "select * from dbo.Tramogge where dimensione = '{0}'";
    		final String query = MessageFormat.format(search, dimensione);
    		final Statement stmt = this.ConnTecnica.createStatement();
    		final ResultSet rs = stmt.executeQuery(query);
    		rs.next();
    		for (int i = 1; i<36; i++) {
    			risultati.add(rs.getInt(i));
    		}
    	} catch (final Exception e) {}
        return risultati;
    }


}
