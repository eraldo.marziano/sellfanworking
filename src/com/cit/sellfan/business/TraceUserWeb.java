package com.cit.sellfan.business;
//tolta da SellFanProxyWeb. là ha smesso di funzionare........
import cit.saas.SaaSUser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;

public class TraceUserWeb {
    public String errorString;
    public final int AzioneTentativoLogIn = 0;
    public final int AzioneLogIn = 1;
    public final int AzioneLogOut = 2;
    public final int AzioneSearch = 3;
    public final int AzioneRisultatiSearch = 4;
    public final int AzioneNuovaOfferta = 5;
    public final int AzioneStampaOfferta = 6;
    public final int AzioneLicenzaScaduta = 7;
    public final int AzioneCondizioniVendidaAccettate = 8;
    public final int AzioneMobileMode = 9;
    public final int AzioneStampaDataSheet = 10;
    public final int AzioneDownloadOfferta = 11;
    public final int AzioneDownloadDataSheet = 12;
    public final String AzioniDescrizione[] = {"TentativoLogin", "Login", "Logout", "Ricerca", "RisultatiRicerca", "NuovaOfferta", "StampaOfferta", "LicenzaScaduta", 
        "CondizioniVenditaAccettate", "MobileMode", "StampaDataSheet", "DownloadOfferta", "DownloadDataSheet"};
    private Connection conn;
    private SaaSUser currentUserSaaS;
    
    public TraceUserWeb() {
    }
    
    public void initConnection(Connection connIn) {
        conn = connIn;
    }
    
    public void initUser(SaaSUser currentUserSaaSIn) {
        currentUserSaaS = currentUserSaaSIn;
    }
    public void traceAzioneUser(int Azione) {
        traceAzioneUser(Azione, "");
    }
    
    public void traceAzioneUser(int Azione, String noteIn) {
        errorString = "";
        try {
            if (conn == null || currentUserSaaS == null || conn.isClosed()) {
                errorString = "conn == null || currentUserSaaS == null || conn.isClosed()";
                return;
            }
        } catch (Exception e) {
            errorString = e.toString();
            return;
        }
        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Timestamp oggi = new java.sql.Timestamp(calendar.getTime().getTime());
            String query = "INSERT INTO [SaaSTraceUser] (Data, idClienteSaaS, Ambiente, idClienteIndex, idSottoClienteIndex, Username, Password, AzioneIndex, AzioneDescrizione) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            String note = AzioniDescrizione[Azione];
            if (!noteIn.equals("")) note += ": " + noteIn;
            if (note.length() > 255) note = note.substring(0, 254);
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setTimestamp(1, oggi);
            stmt.setInt(2, currentUserSaaS._ID);
            stmt.setString(3, currentUserSaaS.Ambiente);
            stmt.setInt(4, currentUserSaaS.idClienteIndex);
            stmt.setInt(5, currentUserSaaS.idSottoClienteIndex);
            stmt.setString(6, currentUserSaaS.Username);
            stmt.setString(7, currentUserSaaS.Password);
            stmt.setInt(8, Azione);
            stmt.setString(9, note);
            stmt.execute();
            errorString = "Fatto";
        } catch (Exception e) {
            errorString = e.toString();
            //System.out.println(e);
        }
    }

    public boolean isActionEnabled(int Azione) {
        return isActionEnabled(Azione, -1);
    }
    
    public boolean isActionEnabled(int Azione, int giaFatti) {
        errorString = "";
        try {
            if (conn == null || currentUserSaaS == null || conn.isClosed()) {
                errorString = "conn == null || currentUserSaaS == null || conn.isClosed()";
                return true;
            }
        } catch (Exception e) {
            errorString = e.toString();
            return false;
        }
        Statement stm;
        String query;
        if (Azione == AzioneNuovaOfferta) {
            if (currentUserSaaS.Tipo == SaaSUser.TipoNOfferte) {
                if (giaFatti < currentUserSaaS.nMaxOfferteTipo4) {
                    currentUserSaaS.currentOfferteTipo4++;
                    query = "update [SaaSUser] set currentOfferteTipo4 = " + Integer.toString(currentUserSaaS.currentOfferteTipo4) + " where _ID = " + Integer.toString(currentUserSaaS._ID);
                    try {
                        stm = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                        stm.execute(query);
                        stm.close();
                        return true;
                    } catch (Exception e) {
                        System.out.println(e);
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else if (Azione == AzioneLogIn) {
            if (currentUserSaaS.Tipo == SaaSUser.TipoNRun) {
                if (currentUserSaaS.currentRunTipo2 < currentUserSaaS.nMaxRunTipo2) {
                    currentUserSaaS.currentRunTipo2++;
                    query = "update [SaaSUser] set currentRunTipo2 = " + Integer.toString(currentUserSaaS.currentRunTipo2) + " where _ID = " + Integer.toString(currentUserSaaS._ID);
                    try {
                        stm = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                        stm.execute(query);
                        stm.close();
                        return true;
                    } catch (Exception e) {
                        System.out.println(e);
                        return false;
                    }
                } else {
                    return false;
                }
            } else if (currentUserSaaS.Tipo == SaaSUser.TipoData) {
                Calendar calendar = Calendar.getInstance();
                java.sql.Date oggi = new java.sql.Date(calendar.getTime().getTime());
                if (oggi.after(currentUserSaaS.dataLimiteTipo1)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else if (Azione == AzioneSearch) {
            if (currentUserSaaS.Tipo == SaaSUser.TipoNSearch) {
                if (currentUserSaaS.currentSearchTipo3 < currentUserSaaS.nMaxSearchTipo3) {
                    currentUserSaaS.currentSearchTipo3++;
                    query = "update [SaaSUser] set currentSearchTipo3 = " + Integer.toString(currentUserSaaS.currentSearchTipo3) + " where _ID = " + Integer.toString(currentUserSaaS._ID);
                    try {
                        stm = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                        stm.execute(query);
                        stm.close();
                        return true;
                    } catch (Exception e) {
                        System.out.println(e);
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        errorString = "Fatto";
        return true;
    }
}
