package com.cit.sellfan.business;

import cit.myjavalib.jGraficoXY;
import cit.sellfan.classi.ParametriVari;
import cit.sellfan.classi.grafici.GraficoPrestazioniCurve;
import cit.sellfan.classi.grafici.GraficoPrestazioniCurveParameter;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;

public class GraficoPrestazioniCurveWeb {
    private VariabiliGlobali l_VG = null;
    public jGraficoXY grafico = null;
    public VentilatoreFisica ventilatoreFisica = null;
    public Ventilatore ventilatore = null;
    private final int NPuntiQuartiche = 200;
    public GraficoPrestazioniCurveParameter graficoPrestazioniCurveParameter = new GraficoPrestazioniCurveParameter();
    public GraficoPrestazioniCurve graficoPrestazioniCurve;
    public double portataMin = 0.;
    public double portataMax = 0.;
    
    public GraficoPrestazioniCurveWeb(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        reset();
    }

    public void showhideCurve(final boolean psVisible, final boolean ptVisible, final boolean potVisible, final boolean etaVisible, final boolean caratteristicaVisible, final boolean rumoreVisible) {
    	if (this.graficoPrestazioniCurve != null) {
			this.graficoPrestazioniCurve.showhideCurve(psVisible, ptVisible, potVisible, etaVisible, caratteristicaVisible, rumoreVisible);
		}
    }
    
    public void reset() {
    	if (this.graficoPrestazioniCurve != null) {
			this.graficoPrestazioniCurve.reset();
		}
    }

    public void setParametriForPannelloPrestazioni(final String titolo1, final String titolo2) {
        setParametriForPannelloPrestazioni(titolo1, titolo2, false, null);
    }

    public void setParametriForPannelloPrestazioni(final String titolo1, final String titolo2, final boolean hieghtLightLimitiRicerca) {
        setParametriForPannelloPrestazioni(titolo1, titolo2, hieghtLightLimitiRicerca, null);
    }

    public void setParametriForPannelloPrestazioni(final String titolo1, final String titolo2, final Ventilatore l_v) {
        setParametriForPannelloPrestazioni(titolo1, titolo2, false, l_v);
    }

    public void setParametriForPannelloPrestazioni(final String titolo1, final String titolo2, final boolean hieghtLightLimitiRicerca, final Ventilatore ventilatoreIn) {
        if (ventilatoreIn == null) {
            if (this.l_VG.VentilatoreCurrentIndex < 0) {
				return;
			}
            this.ventilatore = this.l_VG.VentilatoreCurrent;
        } else {
            this.ventilatore = ventilatoreIn;
        }
        this.ventilatoreFisica = this.l_VG.ventilatoreFisicaCurrent;
        if (l_VG.ParametriVari.clienteSpeciale==ParametriVari.Client.Moro) {
            this.ventilatoreFisica.setVentilatoreFisica(this.l_VG.conTecnica, "", "", this.ventilatore, this.l_VG.ventilatoreFisicaParameter, "Moro");
        } else {
            this.ventilatoreFisica.setVentilatoreFisica(this.l_VG.conTecnica, "", "", this.ventilatore, this.l_VG.ventilatoreFisicaParameter);
        }
        this.graficoPrestazioniCurveParameter.tipoAssePortata = this.l_VG.assiGraficiDefault.tipoAssePortata;
        this.graficoPrestazioniCurveParameter.tipoAssePressione = this.l_VG.assiGraficiDefault.tipoAssePressione;
        this.graficoPrestazioniCurveParameter.tipoAssePotenza = this.l_VG.assiGraficiDefault.tipoAssePotenza;
        this.graficoPrestazioniCurveParameter.tipoAsseRendimento = this.l_VG.assiGraficiDefault.tipoAsseRendimento;
        this.graficoPrestazioniCurveParameter.tipoAsseRumore = this.l_VG.assiGraficiDefault.tipoAsseRumore;
        this.graficoPrestazioniCurveParameter.contributoBocchedb = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(this.ventilatore.NBoccheCanalizzateBase, this.ventilatore.selezioneCorrente.nBoccheCanalizzate);
        this.graficoPrestazioniCurveParameter.graficoFont = this.l_VG.currentCitFont.linguaDisplayFont;
        
        this.graficoPrestazioniCurveParameter.ventilatore = this.ventilatore;
        this.graficoPrestazioniCurveParameter.ventilatoreFisica = this.ventilatoreFisica;
//ParameterUMFromGlobale
        this.graficoPrestazioniCurveParameter.NPuntiCurve = this.NPuntiQuartiche;
        this.graficoPrestazioniCurveParameter.tipoAssePortata = this.l_VG.assiGraficiDefault.tipoAssePortata;
        this.graficoPrestazioniCurveParameter.tipoAssePressione = this.l_VG.assiGraficiDefault.tipoAssePressione;
        this.graficoPrestazioniCurveParameter.tipoAssePotenza = this.l_VG.assiGraficiDefault.tipoAssePotenza;
        this.graficoPrestazioniCurveParameter.tipoAsseRendimento = this.l_VG.assiGraficiDefault.tipoAsseRendimento;
        this.graficoPrestazioniCurveParameter.tipoAsseRumore = this.l_VG.assiGraficiDefault.tipoAsseRumore;
        this.graficoPrestazioniCurveParameter.contributoBocchedb = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(this.ventilatore.NBoccheCanalizzateBase, this.ventilatore.selezioneCorrente.nBoccheCanalizzate);
        this.graficoPrestazioniCurveParameter.graficoFont = this.l_VG.currentCitFont.linguaDisplayFont;
        this.graficoPrestazioniCurveParameter.utilityTraduzioni = this.l_VG.utilityTraduzioni;
//ParameterLunghezzaAssi
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.assePressionePercentuale = this.l_VG.assiGraficiDefault.assePressionePercentuale;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.assePressioneOffset = this.l_VG.assiGraficiDefault.assePressioneOffset;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.assePotenzaPercentuale = this.l_VG.assiGraficiDefault.assePotenzaPercentuale;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.assePotenzaOffset = this.l_VG.assiGraficiDefault.assePotenzaOffset;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.asseRumorePercentuale = this.l_VG.assiGraficiDefault.asseRumorePercentuale;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.asseRumoreOffset = this.l_VG.assiGraficiDefault.asseRumoreOffset;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.asseRendimentoPercentuale = this.l_VG.assiGraficiDefault.asseRendimentoPercentuale;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault.asseRendimentoOffset = this.l_VG.assiGraficiDefault.asseRendimentoOffset;
        this.graficoPrestazioniCurveParameter.titoloGrafico1 = titolo1;
        this.graficoPrestazioniCurveParameter.titoloGrafico2 = titolo2;
        this.graficoPrestazioniCurveParameter.graficoForPrint = false;
        this.graficoPrestazioniCurveParameter.pressioneStaticaFlag = this.l_VG.ParametriVari.pannelloPrestazionipressioneStaticaFlag;
        this.graficoPrestazioniCurveParameter.pressioneTotaleFlag = this.l_VG.ParametriVari.pannelloPrestazionipressioneTotaleFlag;
        this.graficoPrestazioniCurveParameter.potenzaFlag = this.l_VG.ParametriVari.pannelloPrestazionipotenzaFlag;
        this.graficoPrestazioniCurveParameter.potenzaInstallataFlag = this.l_VG.ParametriVari.pannelloPrestazionipotenzaInstallataFlag;
        this.graficoPrestazioniCurveParameter.tratteggioVisibleFlag = this.l_VG.ParametriVari.pannelloPrestazionitratteggioVisibleFlag;
        this.graficoPrestazioniCurveParameter.efficenzaFlag = this.l_VG.ParametriVari.pannelloPrestazioniefficenzaFlag;
        this.graficoPrestazioniCurveParameter.potenzaSonoraFlag = this.l_VG.ParametriVari.pannelloPrestazionipotenzaSonoraFlag;
        this.graficoPrestazioniCurveParameter.rumorePressione = false;
        this.graficoPrestazioniCurveParameter.hieghtLightLimitiRicercaFlag = hieghtLightLimitiRicerca;
        this.graficoPrestazioniCurveParameter.utilityTraduzioni = this.l_VG.utilityTraduzioni;
        this.graficoPrestazioniCurveParameter.assiGraficiDefault = this.l_VG.assiGraficiDefault;
    }

    public void setFlag(final boolean legendaEnabledIn, final int legendaQuadranteIn, final boolean forzaGiriIn, final boolean inizializzaMarkerIn, final boolean aggiornaAltriPannelliIn, final boolean displayCurvaCaratteristicaIn, final boolean displayMarkerIn, final boolean displayRedMarkerIn, final boolean displayOrizzontalMarkerIn, final boolean markerOrizontalSpecialIn) {
        this.graficoPrestazioniCurveParameter.legendaEnabled = legendaEnabledIn;
        this.graficoPrestazioniCurveParameter.legendaQuadrante = legendaQuadranteIn;
        this.graficoPrestazioniCurveParameter.forzaGiri = forzaGiriIn;
        this.graficoPrestazioniCurveParameter.inizializzaMarker = inizializzaMarkerIn;
        this.graficoPrestazioniCurveParameter.displayCurvaCaratteristica = displayCurvaCaratteristicaIn;
        this.graficoPrestazioniCurveParameter.displayMarker = displayMarkerIn;
        this.graficoPrestazioniCurveParameter.displayRedMarker = displayRedMarkerIn;
        this.graficoPrestazioniCurveParameter.displayOrizzontalMarker = displayOrizzontalMarkerIn;
        this.graficoPrestazioniCurveParameter.markerOrizontalSpecial = markerOrizontalSpecialIn;
    }

    public void buildGrafico(final jGraficoXY p_grafico) {
        if (p_grafico == null) {
			return;
		}
        this.grafico = p_grafico;
        if (this.graficoPrestazioniCurve == null) {
			this.graficoPrestazioniCurve = new GraficoPrestazioniCurve(null);
		}
        graficoPrestazioniCurve.isMz = (l_VG.utilityCliente.idClienteIndex == 4);
        l_VG.buildDatiERP327ForSellFan(graficoPrestazioniCurveParameter.ventilatore);
        this.graficoPrestazioniCurve.setCurveParameter(this.graficoPrestazioniCurveParameter);
        this.graficoPrestazioniCurve.buildGrafico(this.grafico);
        try {
            this.portataMin = this.ventilatoreFisica.getPortataMinm3h();
            this.portataMax = this.ventilatoreFisica.getPortataMaxm3h();
        } catch (final Exception e) {
	        
        }
    }
}
