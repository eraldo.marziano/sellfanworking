package com.cit.sellfan.business;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.vaadin.ui.Notification;

import cit.sellfan.Costanti;
import cit.sellfan.classi.CondizioniAmbientali;
import cit.sellfan.classi.DBTableQualifier;
import cit.sellfan.classi.InterscambioRicerca;
import cit.sellfan.classi.Motore;
import cit.sellfan.classi.SelezioneDati;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.db.DBTecnico;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.classi.ventilatore.VentilatoreFisicaParameter;
import cit.sellfan.classi.ventilatore.VentilatoreXSelezione;
import cit.sellfan.classi.ventilatore.VentilatoriSelezione;

public class RicercaWeb {
    private Connection ConnTecnica;
    private DBTableQualifier dbTableQualifier;
    private UtilityCliente utilityCliente;
    private DBTecnico dbTecnico;
//static falg
    private int nTemperatureLimite;
    private boolean catalogoAutoSerieFlag;
    private boolean pannelloRicercaMaggiorazioneDAGiranteFlag;
    private double pannelloRicercaMaggiorazioneDAGiranteMin;
    private double pannelloRicercaMaggiorazioneDAGiranteMax;
    private int pannelloRicercaMaggiorazioneDAGiranteNStep;
    private double pannelloRicercaDArpmMenoPercentuale;
    private double pannelloRicercaDArpmPiuPercentuale;
    private boolean pannelloRicercaAutoSceltaMotoreFlag;
    private boolean pannelloRicercaDArpmPiuMenoFlag;
    private int idTranscodificaIndex;
//dynamin variable
    private CondizioniAmbientali CARicerca;
    private CondizioniAmbientali CA020Ricerca;
    private VentilatoreFisica ventilatoreFisicaForSelezione;
    private ArrayList<Ventilatore> ElencoVentilatoriFromRicerca;
    private double pannelloRicercaCorrettorePotenzaAvviamentoFreddo;    //lo usa solo Cliente03
    @SuppressWarnings("unused")
	private String tipoMotoreDefault[];
    private VentilatoriSelezione VentilatoreSelezionato;
    private int correttorePotenzaMotore;

    public void init(final Connection ConnTecnicaIn, final DBTableQualifier dbTableQualifierIn, final UtilityCliente utilityClienteIn, final DBTecnico dbTecnicoIn) {
        this.ConnTecnica = ConnTecnicaIn;
        this.dbTableQualifier = dbTableQualifierIn;
        this.utilityCliente = utilityClienteIn;
        this.dbTecnico = dbTecnicoIn;
    }
    
    public void initStaticFlag(final int nTemperatureLimiteIn, final boolean catalogoAutoSerieFlagIn, final boolean pannelloRicercaMaggiorazioneDAGiranteFlagIn, final double pannelloRicercaMaggiorazioneDAGiranteMinIn, final double pannelloRicercaMaggiorazioneDAGiranteMaxIn, final int pannelloRicercaMaggiorazioneDAGiranteNStepIn, final double pannelloRicercaDArpmMenoPercentualeIn, final double pannelloRicercaDArpmPiuPercentualeIn, final boolean pannelloRicercaAutoSceltaMotoreFlagIn, final boolean pannelloRicercaDArpmPiuMenoFlagIn, final int idTranscodificaIndexIn) {
        this.nTemperatureLimite = nTemperatureLimiteIn;
        this.catalogoAutoSerieFlag = catalogoAutoSerieFlagIn;
        this.pannelloRicercaMaggiorazioneDAGiranteFlag = pannelloRicercaMaggiorazioneDAGiranteFlagIn;
        this.pannelloRicercaMaggiorazioneDAGiranteMin = pannelloRicercaMaggiorazioneDAGiranteMinIn;
        this.pannelloRicercaMaggiorazioneDAGiranteMax = pannelloRicercaMaggiorazioneDAGiranteMaxIn;
        this.pannelloRicercaMaggiorazioneDAGiranteNStep = pannelloRicercaMaggiorazioneDAGiranteNStepIn;
        this.pannelloRicercaDArpmMenoPercentuale = pannelloRicercaDArpmMenoPercentualeIn;
        this.pannelloRicercaDArpmPiuPercentuale = pannelloRicercaDArpmPiuPercentualeIn;
        this.pannelloRicercaAutoSceltaMotoreFlag = pannelloRicercaAutoSceltaMotoreFlagIn;
        this.pannelloRicercaDArpmPiuMenoFlag = pannelloRicercaDArpmPiuMenoFlagIn;
        this.idTranscodificaIndex = idTranscodificaIndexIn;
    }
    
    public void initDynamicVariable(final CondizioniAmbientali CARicercaIn, final CondizioniAmbientali CA020RicercaIn, final double pannelloRicercaCorrettorePotenzaAvviamentoFreddoIn, final String tipoMotoreDefaultIn[], final int correttorePotenzaMotoreIn) {
        this.CARicerca = CARicercaIn;
        this.CA020Ricerca = CA020RicercaIn;
        this.pannelloRicercaCorrettorePotenzaAvviamentoFreddo = pannelloRicercaCorrettorePotenzaAvviamentoFreddoIn;
        this.tipoMotoreDefault = tipoMotoreDefaultIn;
        this.correttorePotenzaMotore = correttorePotenzaMotoreIn;
    }
    
    public int eseguiRicerca(final ArrayList<Ventilatore> ElencoVentilatoriFromRicercaIn, final VentilatoriSelezione VentilatoreSelezionatoIn, final SelezioneDati SelezioneDati, final VentilatoreFisicaParameter ventilatoreFisicaParameter) {
        this.ventilatoreFisicaForSelezione = new VentilatoreFisica();
        this.ElencoVentilatoriFromRicerca = ElencoVentilatoriFromRicercaIn;
        this.VentilatoreSelezionato = VentilatoreSelezionatoIn;
        this.ElencoVentilatoriFromRicerca.clear();
/*    da mettere prima della chiamata
        Calendar calendarStart = Calendar.getInstance();
        long millisecondiStart = calendarStart.get(Calendar.MILLISECOND) + 1000*calendarStart.get(Calendar.SECOND) + 60000*calendarStart.get(Calendar.MINUTE)+ 3600000*calendarStart.get(Calendar.HOUR);
        if (!saas.isActionEnabled(SaaS.AzioneSearch)) {
            messageBox.showFatalBox("Search", "Numero Masssimo Ricerche Disponibili Raggiunto.");
            return 0;
        }
        tipoMotoreCurrent = tipoMotoreDefault[0];
*/
        final InterscambioRicerca interscambio = new InterscambioRicerca();
        double l_NumeroGiri, l_NumeroGiriMin, l_NumeroGiriMax;
        double l_PortataMinima, l_PortataMassima, l_PotenzaInst;
        Statement stmt = null;
        ResultSet rs = null;
/*    da mettere prima della chiamata
        String memo = "Ricerca Portata=" + SelezioneDati.PortataRichiestam3h + "[m3/h] Pressione=" + SelezioneDati.PressioneRichiestaPa + "[Pa] Trasmissione=" + SelezioneDati.Trasmissione;
        saas.traceClienteSaas(SaaS.AzioneSearch, memo);
        utilityLog.trace(getClass(), memo);
*/
        this.VentilatoreSelezionato.ElencoVentilatori.clear();
        this.VentilatoreSelezionato.portataRichiestam3h = SelezioneDati.PortataRichiestam3h;
        this.VentilatoreSelezionato.pressioneRichiestaPa = SelezioneDati.PressioneRichiestaPa;
        this.VentilatoreSelezionato.tolleranzaSuPortataFlag = SelezioneDati.tolleranzaPortataFlag;
        if (SelezioneDati.precisionePiu > 0.) {
            this.VentilatoreSelezionato.deltaPaPiu = SelezioneDati.PressioneRichiestaPa * SelezioneDati.precisionePiu;
            this.VentilatoreSelezionato.deltam3Piu = SelezioneDati.PortataRichiestam3h * SelezioneDati.precisionePiu;
        } else {
            this.VentilatoreSelezionato.deltaPaPiu = SelezioneDati.PressioneRichiestaPa * 0.00005;
            this.VentilatoreSelezionato.deltam3Piu = SelezioneDati.PortataRichiestam3h * 0.00005;
        }
        if (SelezioneDati.precisioneMeno > 0.) {
            this.VentilatoreSelezionato.deltaPaMeno = SelezioneDati.PressioneRichiestaPa * SelezioneDati.precisioneMeno;
            this.VentilatoreSelezionato.deltam3Meno = SelezioneDati.PortataRichiestam3h * SelezioneDati.precisioneMeno;
        } else {
            this.VentilatoreSelezionato.deltaPaMeno = SelezioneDati.PressioneRichiestaPa * 0.00005;
            this.VentilatoreSelezionato.deltam3Meno = SelezioneDati.PortataRichiestam3h * 0.00005;
        }
        this.VentilatoreSelezionato.giriMassimiGirante = SelezioneDati.giriMassimiGirante;
        this.VentilatoreSelezionato.CorrettorePotMotore = this.correttorePotenzaMotore;
/*    da mettere prima della chiamata
        SelezioneDati.modeRicerca = modeRicerca;
        SelezioneDati.Temperatura = getTemperaturaProgettazioneC();
        SelezioneDati.pannelloRicercaTRLimitePortataFlag = pannelloRicercaTRLimitePortataFlag;
        SelezioneDati.pannelloRicercaTRLimitePressioneFlag = pannelloRicercaTRLimitePressioneFlag;
        SelezioneDati.pannelloRicercaDALimitePortataFlag = pannelloRicercaDALimitePortataFlag;
        SelezioneDati.pannelloRicercaDALimitePressioneFlag = pannelloRicercaDALimitePressioneFlag;
        SelezioneDati.pannelloRicercaLimiteTemperaturaProgettazioneFlag = pannelloRicercaLimiteTemperaturaProgettazioneFlag;
        SelezioneDati.needModelloNull = !SellFanSaaSFlag && DBTecnicoType == UtilitySql.DBTypeExcell;
        SelezioneDati.ElencoSerie = ElencoSerie;
        SelezioneDati.ElencoSerieDisponibili = ElencoSerieDisponibili;
        SelezioneDati.ElencoMateriali = ElencoMateriali;
*/
        int nVent = 0;
        try {
            stmt = this.ConnTecnica.createStatement();
            final String query = this.utilityCliente.buildQueryPerRicerca(SelezioneDati);
            rs = stmt.executeQuery(query);
            while(rs.next()) {
                nVent++;
    			final int freq = -1;
                final Ventilatore l_v = this.dbTecnico.loadVentilatoreForRicerca(rs, this.nTemperatureLimite + 1, ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, SelezioneDati.Trasmissione, -1., -1, this.CARicerca, this.CA020Ricerca, this.idTranscodificaIndex, -1);
                this.utilityCliente.loadMotore(l_v, l_v.MotoreOriginale);
                if (this.catalogoAutoSerieFlag && l_v.elencoClassi.isEmpty()) {
					continue;
				}
//esecuzione esplicita
                if (!SelezioneDati.esecuzioneSelezionata.equals("")) {
                    int i;
                    for (i=0 ; i<l_v.Esecuzioni.length ; i++) {
                        if (l_v.Esecuzioni[i].equals(SelezioneDati.esecuzioneSelezionata)) {
                            break;
                        }
                    }
                    if (i == l_v.Esecuzioni.length) {
						continue;
					}
                    l_v.selezioneCorrente.MotoreInstallato.Esecuzione = SelezioneDati.esecuzioneSelezionata;
                    l_v.selezioneCorrente.MotoreInstallato.Prezzo = this.utilityCliente.getPrezzoMotore(l_v.selezioneCorrente.MotoreInstallato.CodiceCliente, l_v.selezioneCorrente.MotoreInstallato.Esecuzione);
                 }
                l_v.Dapo = SelezioneDati.Dapo;
                l_v.Serranda = SelezioneDati.Serranda;
                l_v.aperturaDapoSerranda = SelezioneDati.aperturaSerrandaDapo;
                l_PortataMinima = l_v.PortataMin;
                l_PortataMassima = l_v.PortataMax;
                l_PotenzaInst = l_v.selezioneCorrente.MotoreInstallato.PotkW * 1000.;
                l_NumeroGiri = l_v.RpmInit;
                l_NumeroGiriMin = l_v.RpmMin;
                l_NumeroGiriMax = this.dbTecnico.RpmMaxTemperatura(l_v.CAProgettazione.temperatura, l_v);
                l_v.RpmMaxTeorici = (int)l_NumeroGiriMax;
                if (!isVentilatoreGiaSelezionato(l_v, this.VentilatoreSelezionato)) {
                    try {
                        this.VentilatoreSelezionato.portataMin = l_PortataMinima;
                        this.VentilatoreSelezionato.portataMax = l_PortataMassima;
                        this.VentilatoreSelezionato.rpm = l_NumeroGiri;
                        this.VentilatoreSelezionato.rpmMin = l_NumeroGiriMin;
                        this.VentilatoreSelezionato.rpmMax = l_NumeroGiriMax;
                        this.VentilatoreSelezionato.potenzaInstallata = l_PotenzaInst;
                        l_v.PressioneMandataFlag = SelezioneDati.PressioneMandataFlag;
                        //ventilatoreFisicaForSelezione = new VentilatoreFisica();
                        this.ventilatoreFisicaForSelezione.setVentilatoreFisica(this.ConnTecnica, this.dbTableQualifier.dbTableQualifierPreDBTecnico, this.dbTableQualifier.dbTableQualifierPostDBTecnico, l_v, ventilatoreFisicaParameter);
                        this.VentilatoreSelezionato.setVentilatore(l_v, this.ventilatoreFisicaForSelezione, SelezioneDati);
                        interscambio.NumeroGiri = l_NumeroGiri;
                        interscambio.NumeroGiriMax = l_NumeroGiriMax;
                        interscambio.NumeroGiriMin = l_NumeroGiriMin;
                        interscambio.rpmMaxTeoriciModificati = l_v.RpmMaxTeorici;
                        if (isVentilatoreRicercaOK(l_v, SelezioneDati, interscambio)) {
                            addVentilatoreRicerca(l_v, this.VentilatoreSelezionato, SelezioneDati, interscambio);
                            l_v.selezioneCorrente.NumeroGiri = Math.round(l_v.selezioneCorrente.NumeroGiri);
                        } else if (this.pannelloRicercaMaggiorazioneDAGiranteFlag && SelezioneDati.Trasmissione == false) {
                            final double diametroGirante = l_v.DiametroGirante;
                            final double QminRicerca = l_v.PortataMinRicerca;
                            final double QmaxRicerca = l_v.PortataMaxRicerca;
                            final double Qmin = l_v.PortataMin;
                            final double Qmax = l_v.PortataMax;
                            double moltDiametro;
                            final double step = (this.pannelloRicercaMaggiorazioneDAGiranteMax - this.pannelloRicercaMaggiorazioneDAGiranteMin) / this.pannelloRicercaMaggiorazioneDAGiranteNStep;
                            double maggiorazione = this.pannelloRicercaMaggiorazioneDAGiranteMin;
                            for (int i=0 ; i<this.pannelloRicercaMaggiorazioneDAGiranteNStep; i++) {
                                l_v.maggiorazioneGirante = maggiorazione;
                                l_v.DiametroGirante = diametroGirante * (1. + maggiorazione);
                                moltDiametro = Math.pow(l_v.DiametroGirante / diametroGirante, 3.);
                                l_v.PortataMinRicerca = QminRicerca * moltDiametro;
                                l_v.PortataMaxRicerca = QmaxRicerca * moltDiametro;
                                l_v.PortataMin = Qmin * moltDiametro;
                                l_v.PortataMax = Qmax * moltDiametro;
                                this.VentilatoreSelezionato.setVentilatore(l_v, this.ventilatoreFisicaForSelezione, SelezioneDati);
                                if (isVentilatoreRicercaOK(l_v, SelezioneDati, interscambio)) {
                                    addVentilatoreRicerca(l_v, this.VentilatoreSelezionato, SelezioneDati, interscambio);
                                    l_v.selezioneCorrente.NumeroGiri = Math.round(l_v.selezioneCorrente.NumeroGiri);
                                    break;
                                }
                                maggiorazione += step;
                            }
                        } else if (this.catalogoAutoSerieFlag) {
                            for (int i=1 ; i<l_v.elencoClassi.size() ; i++) {
                                this.utilityCliente.setVentilatoreClasse(l_v, l_v.CAProgettazione.temperatura, i);
                                interscambio.rpmMaxTeoriciModificati = l_v.RpmMaxTeorici;
                                interscambio.NumeroGiriMax = l_v.RpmMaxTeorici;
                                if (isVentilatoreRicercaOK(l_v, SelezioneDati, interscambio)) {
                                    for (int j=0 ; j<i ; j++) {
                                        l_v.elencoClassi.remove(0);
                                        l_v.elencoRpmClassi.remove(0);
                                    }
                                    addVentilatoreRicerca(l_v, this.VentilatoreSelezionato, SelezioneDati, interscambio);
                                    l_v.selezioneCorrente.NumeroGiri = Math.round(l_v.selezioneCorrente.NumeroGiri);
                                    break;
                                }
                            }
                        }
                    } catch (final Exception e) {
                        //utilityLog.error(this.getClass(), e.getStackTrace(), e.getMessage());
                    }
                }
            }
            rs.close();
            stmt.close();
        } catch (final Exception e) {
            System.out.println(e);
            //utilityLog.error(this.getClass(), e.getStackTrace(), e.getMessage());
        }
/*    da mettere dopo la chiamata
        for (int i=0 ; i<ElencoVentilatoriFromRicerca.size() ; i++) {
            Ventilatore l_v = ElencoVentilatoriFromRicerca.get(i);
            completaCaricamentoVentilatore(l_v);
            l_v.DescrizioneFluidoRicerca = descrizioneFluido[descrizioneFluidoIndex];
            if (SellFanERP327IntegratiFlag) ERP327Build.setERP327(l_v, regolamento327Algoritmo, ventilatoreFisicaParameter);
            VentilatoreXSelezione l_vXS = VentilatoreSelezionato.ElencoVentilatori.get(i);
            l_vXS.ModelloCompleto = buildModelloCompleto(l_v, parametriModelloCompletoForPrint);
            //System.out.println(l_v.Modello+"\t"+l_v.selezioneCorrente.PressioneStatica);
            l_v.PrezzoVentilatoreForSort = 0.;
            for (int j=0 ; j<l_v.Esecuzioni.length ; j++) {
                if (l_v.Esecuzioni[j].equals(ElencoVentilatoriFromRicerca.get(i).selezioneCorrente.Esecuzione)) {
                    l_v.PrezzoVentilatoreForSort = l_v.PrezzoEsecuzioni[j];
//                    l_v.ScontoVentilatore = l_v.ScontoEsecuzioni[j];
                    break;
                }
            }
        }
        ventilatoreSort.sortCampo = campoVentilatoreSort;
        ventilatoreSort.sortCrescente = crescenteVentilatoreSort;
        ventilatoreSort.sortRicerca();
        Calendar calendarEnd = Calendar.getInstance();
        long millisecondiEnd = calendarEnd.get(Calendar.MILLISECOND) + 1000*calendarEnd.get(Calendar.SECOND) + 60000*calendarEnd.get(Calendar.MINUTE)+ 3600000*calendarEnd.get(Calendar.HOUR);
        if (ElencoVentilatoriFromRicerca.isEmpty()) {
            memo = "[" + Long.toString(millisecondiEnd - millisecondiStart) + "ms/" + Long.toString(nVent) + "v] Nessun ventilatore selezionato";
            saas.traceClienteSaas(SaaS.AzioneRisultatiSearch, memo);
            utilityLog.trace(getClass(), memo);
        } else {
            String msg = " [";
            for (int i=0 ; i<ElencoVentilatoriFromRicerca.size()-1 ; i++) {
                msg += ElencoVentilatoriFromRicerca.get(i).Modello + ", ";
            }
            msg += ElencoVentilatoriFromRicerca.get(ElencoVentilatoriFromRicerca.size()-1).Modello + "]";
            memo = "[" + Long.toString(millisecondiEnd - millisecondiStart) + "ms/" + Long.toString(nVent)+ "v] Trovati " + Integer.toString(ElencoVentilatoriFromRicerca.size()) + " ventilatori" + msg;
            saas.traceClienteSaas(SaaS.AzioneRisultatiSearch, memo);
            utilityLog.trace(getClass(), memo);
            if (debugModeFlag) {
                setMemoPrincipaleForm("[" + Long.toString(millisecondiEnd - millisecondiStart) + "ms/" + Long.toString(nVent)+ "v] Trovati " + Integer.toString(ElencoVentilatoriFromRicerca.size()) + " ventilatori");
            }
        }
*/
        return nVent;
    }
    
    private void addVentilatoreRicerca(final Ventilatore l_v, final VentilatoriSelezione VentilatoreSelezionato, final SelezioneDati selezioneDati, final InterscambioRicerca interscambio) {
        final VentilatoreXSelezione l_vXS = new VentilatoreXSelezione();
        l_v.selezioneCorrente.Esecuzione = filtroEsecuzione(l_v, selezioneDati);
        this.utilityCliente.loadVentilatoreCampiCliente(l_v);
        //final VentilatoreCampiCliente l_vcc = (VentilatoreCampiCliente)l_v.campiCliente.clone();
        l_v.FromRicerca = true;
        l_vXS.Trasmissione = selezioneDati.Trasmissione;
        l_vXS.Serie = l_v.Serie;
        l_vXS.Modello = l_v.Modello;
        l_vXS.Versione = l_v.Versione;
        l_vXS.Classe = l_v.Classe;
        l_vXS.Materiale = l_v.Materiale;
        l_vXS.Motore = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
        l_vXS.rpm = VentilatoreSelezionato.rpm;
        l_v.selezioneCorrente.NumeroGiri = VentilatoreSelezionato.rpm;
        l_vXS.Portata = VentilatoreSelezionato.portata;
        l_v.selezioneCorrente.Marker = VentilatoreSelezionato.portata;
        l_vXS.PressioneTotale = VentilatoreSelezionato.pressioneTotale;
        l_v.selezioneCorrente.PressioneTotale = VentilatoreSelezionato.pressioneTotale;
        l_vXS.PressioneStatica = VentilatoreSelezionato.pressioneStatica;
        l_v.selezioneCorrente.PressioneStatica = VentilatoreSelezionato.pressioneStatica;
        l_vXS.NumeroGiri = VentilatoreSelezionato.rpm;
        l_vXS.PotenzaAssorbita = VentilatoreSelezionato.potenza;
        l_v.selezioneCorrente.Potenza = VentilatoreSelezionato.potenza;
        l_vXS.Rendimento = VentilatoreSelezionato.rendimento;
        l_v.selezioneCorrente.Rendimento = VentilatoreSelezionato.rendimento;
        l_vXS.PotenzaSonora = VentilatoreSelezionato.potenzaSonora;
        l_v.selezioneCorrente.PotenzaSonora = VentilatoreSelezionato.potenzaSonora;
        l_vXS.PressioneSonora = VentilatoreSelezionato.pressioneSonora;
        l_v.selezioneCorrente.PressioneSonora = VentilatoreSelezionato.pressioneSonora;
        l_v.selezioneCorrente.nBoccheCanalizzate = interscambio.nBoccheAttuali;
        l_v.selezioneCorrente.distanzaSpettroSonorom = -1.;
        l_v.selezioneCorrente.Marker = VentilatoreSelezionato.portata;
        l_v.selezioneCorrente.RedMarker = VentilatoreSelezionato.portata;
        l_v.selezioneCorrente.NumeroGiri = VentilatoreSelezionato.rpm;
        l_v.PressioneStaticaFlag = selezioneDati.pressioneStaticaFlag;
        l_v.PressioneRichiestaPa = selezioneDati.PressioneRichiestaPa;
        l_v.DiametroMaxGirantem = selezioneDati.DiametroMassimoGirante;
        l_v.VelocitaMax = selezioneDati.giriMassimiGirante;
        l_v.PortataRichiestam3h = selezioneDati.PortataRichiestam3h;
        l_v.NormalPortataRichiesta = selezioneDati.NormalPortataRichiesta;
        l_v.RendimentoMinimoRichiesto = selezioneDati.RendMinimo;
        l_v.PotenzaSonoraMaxRichiesta = selezioneDati.PotSonoraMax;
        l_v.CorrettorePotenzaMotore = this.correttorePotenzaMotore;
        l_v.UMPortata = selezioneDati.UMPortata;
        l_v.UMPressione = selezioneDati.UMPressione;
        l_v.UMPotenza = selezioneDati.UMPotenza;
        l_v.UMAltezza = selezioneDati.UMAltezza;
        l_v.UMTemperatura = selezioneDati.UMTemperatura;
        l_v.distanzaPuntoRichiesto = Math.pow(l_v.selezioneCorrente.Marker - selezioneDati.PortataRichiestam3h, 2.);
        if (selezioneDati.pressioneStaticaFlag) {
            l_v.distanzaPuntoRichiesto += Math.pow(VentilatoreSelezionato.pressioneStatica - selezioneDati.PressioneRichiestaPa, 2.);
        } else {
            l_v.distanzaPuntoRichiesto += Math.pow(VentilatoreSelezionato.pressioneTotale - selezioneDati.PressioneRichiestaPa, 2.);
        }
        l_v.RpmMaxTeorici = this.dbTecnico.RpmMaxTemperatura(l_v.CAProgettazione.temperatura, l_v);
        if (this.utilityCliente.isAlberoNudo(l_v.selezioneCorrente.Esecuzione)) {
            l_v.selezioneCorrente.MotoreInstallato.reset();
        } else if (this.pannelloRicercaAutoSceltaMotoreFlag || l_v.Trasmissione) {
            if (l_v.MotoreOriginale.equals("-")) {
            	final double aaa=this.utilityCliente.getPotenzaPersaMonobloccoW(l_v);
                double PotenzaRichiestaW = (VentilatoreSelezionato.potenza + this.utilityCliente.getPotenzaPersaMonobloccoW(l_v)) * (1. + this.correttorePotenzaMotore/100.) * this.pannelloRicercaCorrettorePotenzaAvviamentoFreddo;
                PotenzaRichiestaW /= l_v.datiERP327.efficienzaTrasmissione;
                if (l_v.Modello.equals("GF 1120R")) {
                	Notification.show("VentilatoreSelezionato.potenza:"+Double.toString(VentilatoreSelezionato.potenza)+"  getPotenzaPersaMonobloccoW:"+Double.toString(aaa)+"  correttorePotenzaMotore:"+Double.toString(this.correttorePotenzaMotore)+"  pannelloRicercaCorrettorePotenzaAvviamentoFreddo:"+Double.toString(this.pannelloRicercaCorrettorePotenzaAvviamentoFreddo)+"  PotenzaRichiestaW: "+Double.toString(PotenzaRichiestaW));
                }
                final String l_Motore = ScegliMotore(l_v, PotenzaRichiestaW, VentilatoreSelezionato.rpm, VentilatoreSelezionato.PD2);
                if (l_Motore == null) {
                    if (l_v.selezioneCorrente.MotoreInstallato == null) {
						l_v.selezioneCorrente.MotoreInstallato = new Motore();
					} else {
						l_v.selezioneCorrente.MotoreInstallato.reset();
					}
                } else {
                    this.utilityCliente.loadMotore(l_v, l_Motore);
                    if (!l_v.Trasmissione && this.pannelloRicercaDArpmPiuMenoFlag) {
                        if (VentilatoreSelezionato.rpm < (l_v.selezioneCorrente.MotoreInstallato.Rpm * this.pannelloRicercaDArpmMenoPercentuale) ||
                            VentilatoreSelezionato.rpm > (l_v.selezioneCorrente.MotoreInstallato.Rpm * this.pannelloRicercaDArpmPiuPercentuale)) {
                                return;
                        }
                    }
                }
            }
        }
        if (!this.utilityCliente.isRpmRapportoOK(l_v, VentilatoreSelezionato, this.ventilatoreFisicaForSelezione)) {
            return;
        }
        if (l_v.Trasmissione && (l_v.selezioneCorrente.MotoreInstallato.PotkW * 1000.) > 0. && (l_v.selezioneCorrente.MotoreInstallato.PotkW * 1000.) < VentilatoreSelezionato.potenza) {
            return;
        }
        VentilatoreSelezionato.ElencoVentilatori.add(l_vXS);
        l_v.MotoreOriginale = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
        this.ElencoVentilatoriFromRicerca.add(l_v);
    }
    
    private boolean isVentilatoreGiaSelezionato(final Ventilatore l_v, final VentilatoriSelezione VentilatoreSelezionato) {
        for (int i=0 ; i<VentilatoreSelezionato.ElencoVentilatori.size() ; i++) {
            final VentilatoreXSelezione l_vXS = VentilatoreSelezionato.ElencoVentilatori.get(i);
            if (l_vXS.Serie.equals(l_v.Serie) && l_vXS.Modello.equals(l_v.Modello) && l_vXS.Versione.equals(l_v.Versione) && l_vXS.Materiale.equals(l_v.Materiale)) {
                return true;
            }
        }
        return false;
    }

    private String filtroEsecuzione(final Ventilatore l_v, final SelezioneDati selezioneDati) {
        if (selezioneDati.esecuzioneSelezionata.equals("")) {
			return l_v.Esecuzioni[0];
		}
        for (int i=0 ; i<l_v.Esecuzioni.length ; i++) {
            if (l_v.Esecuzioni[i].equals(selezioneDati.esecuzioneSelezionata)) {
                return l_v.Esecuzioni[i];
            }
        }
        return l_v.Esecuzioni[0];
    }

    private boolean isVentilatoreRicercaOK(final Ventilatore l_v, final SelezioneDati selezioneDati, final InterscambioRicerca interscambio) {
        double contributoXRumore;
        boolean ret = false;
//modeRicerca = modeRicercaPuntoEffettivo;
        this.VentilatoreSelezionato.pressioneRiferimento = -1.;
        if (selezioneDati.modeRicerca == Costanti.modeRicercaStandard) {         //standard mode
            if (selezioneDati.tolleranzaPortataFlag) {
                if (l_v.Trasmissione) {
                    ret = this.VentilatoreSelezionato.ventilatoreTRPortataFissaMode0(interscambio.NumeroGiriMin, interscambio.NumeroGiriMax);
                } else {
                    if (this.pannelloRicercaDArpmPiuMenoFlag) {
                        this.VentilatoreSelezionato.rpmMax = Math.min(interscambio.NumeroGiri * this.pannelloRicercaDArpmPiuPercentuale, l_v.RpmMaxTeorici);
                        interscambio.rpmMaxTeoriciModificati = (int)this.VentilatoreSelezionato.rpmMax;
                        if (interscambio.NumeroGiri * this.pannelloRicercaDArpmMenoPercentuale > interscambio.rpmMaxTeoriciModificati) {
							return false;
						}
                        ret = this.VentilatoreSelezionato.ventilatoreTRPortataFissaMode0(interscambio.NumeroGiri*this.pannelloRicercaDArpmMenoPercentuale, interscambio.rpmMaxTeoriciModificati);
                    } else {
                        ret = this.VentilatoreSelezionato.ventilatoreDAPortataFissaMode0(interscambio.NumeroGiriMax);
                    }
                }
            } else {
                if (l_v.Trasmissione) {
                    ret = this.VentilatoreSelezionato.ventilatoreTRPressioneFissaMode0(Costanti.pannelloRicercaStepRpmStartTRPressioneFissa, interscambio.NumeroGiriMin, interscambio.NumeroGiriMax);
                } else {
                    if (this.pannelloRicercaDArpmPiuMenoFlag) {
                        this.VentilatoreSelezionato.rpmMax = Math.min(interscambio.NumeroGiri * this.pannelloRicercaDArpmPiuPercentuale, l_v.RpmMaxTeorici);
                        interscambio.rpmMaxTeoriciModificati = (int)this.VentilatoreSelezionato.rpmMax;
                        if (interscambio.NumeroGiri * this.pannelloRicercaDArpmMenoPercentuale > interscambio.rpmMaxTeoriciModificati) {
							return false;
						}
                        ret = this.VentilatoreSelezionato.ventilatoreTRPressioneFissaMode0(Costanti.pannelloRicercaStepRpmStartTRPressioneFissa, interscambio.NumeroGiri*this.pannelloRicercaDArpmMenoPercentuale, interscambio.rpmMaxTeoriciModificati);
                    } else {
                        ret = this.VentilatoreSelezionato.ventilatoreDAPressioneFissaMode0(interscambio.NumeroGiriMax);
                    }
                }
            }
        } else if (selezioneDati.modeRicerca == Costanti.modeRicercaPuntoEffettivo) {          //su punto effettivo
            if (l_v.Trasmissione) {
                ret = this.VentilatoreSelezionato.ventilatoreTRMode1(interscambio.NumeroGiriMin, interscambio.NumeroGiriMax);
            } else {
                ret = this.VentilatoreSelezionato.ventilatoreDAMode1();
            }
        }
        if (ret) {
            if (selezioneDati.nBoccheCanalizzate < 0) {
                interscambio.nBoccheAttuali = l_v.NBoccheCanalizzateBase;
            } else {
                interscambio.nBoccheAttuali = selezioneDati.nBoccheCanalizzate;
            }
            contributoXRumore = this.utilityCliente.getCorrettorePotenzaSonora(l_v.NBoccheCanalizzateBase, interscambio.nBoccheAttuali);
            ret = this.VentilatoreSelezionato.applicaUlterioriLimiti(contributoXRumore);
        }
        return ret;
    }

    public String ScegliMotore(final Ventilatore l_v, final double PotenzaRichiestaW, final double NumeroGiri, final double PD2) {
        if (l_v.Modello.equals("GF 1120R")) {
          //  final int kkkk=0;
        }
        for (int i=0 ; i<this.utilityCliente.tipiMotori.length ; i++) {
            final String motore = ScegliMotore(l_v, PotenzaRichiestaW, NumeroGiri, PD2, this.utilityCliente.tipiMotori[i]);
            if (!motore.equals("-")) {
				return motore;
			}
        }
        return "-";
    }
    
    private String ScegliMotore(final Ventilatore l_v, final double PotenzaRichiestaW, final double NumeroGiri, final double PD2, final String tipoMotore) {
        try {
            Statement stmt = null;
            ResultSet rs = null;
            stmt = this.ConnTecnica.createStatement();
            String query = this.utilityCliente.buildQuerySceltaMotore(l_v, PotenzaRichiestaW, NumeroGiri, PD2);
            //if (!tipoMotore.equals("???")) query += " and CodiceCliente like '%" + tipoMotore + "'";
            query += " and Freq = " + Integer.toString(l_v.CAProgettazione.frequenza);
            rs = stmt.executeQuery(this.utilityCliente.buildQuerySceltaMotoreAdOrderBy(query));
            rs.next();
            String codice = rs.getString("CodiceCliente");
            if (codice.equals("-")) {
                codice = rs.getString("Codice");
            }
            rs.close();
            stmt.close();
            return codice;
        } catch (final Exception e) {
//           _VG.utilityLog.error(getClass(), e.getStackTrace(), e.getMessage());
           return "-";
        }
    }
}
