package com.cit.sellfan.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.Blob;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.cit.sellfan.business.AccessoriAzioni;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.business.VariabiliGlobaliCliente;
import com.cit.sellfan.ui.pannelli.pannelloCondizioniVendita;
import com.cit.sellfan.ui.pannelli.pannelloNewUserRequest;
import com.cit.sellfan.ui.view.cliente04.Cliente04AccessoriAzioni;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevAbsoluteLayout;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevPasswordField;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.navigation.Navigation;

import cit.myistat.UtilityIstat;
import cit.saas.SaaSNewUser;
import cit.saas.SaaSUser;
import cit.sellfan.Costanti;
import cit.sellfan.classi.AssiGrafici;
import cit.sellfan.classi.CondizioniAmbientali;
import cit.sellfan.classi.ParametriVari;
import cit.sellfan.classi.ParametriVari.Client;
import cit.sellfan.classi.PresetRicerca;
import cit.sellfan.classi.SelezioneDati;
import cit.sellfan.classi.db.DBAccessori;
import cit.sellfan.classi.db.DBParam;
import cit.sellfan.classi.db.DBPrivato;
import cit.sellfan.classi.ventilatore.VentilatoriSort;
import cit.sellfan.personalizzazioni.cliente01.Cliente01DbTecnico;
import cit.sellfan.personalizzazioni.cliente01.Cliente01UtilityCliente;
import cit.sellfan.personalizzazioni.cliente04.Cliente04UtilityCliente;
import cit.sellfan.personalizzazioni.cliente04.UtilityClienteMoro;
import cit.sellfan.personalizzazioni.cliente98.Cliente98UtilityCliente;
import de.steinwedel.messagebox.ButtonType;
import de.steinwedel.messagebox.MessageBox;

public class GetUserView extends XdevView {
	private VariabiliGlobali l_VG;
	private MessageBox msgBox;
	private String mainError;

	public GetUserView() {
		super();
		this.initUI();
		this.txtUser.focus();
	}

	@Override
	public void enter(final ViewChangeListener.ViewChangeEvent event) {
		super.enter(event);
		this.l_VG = ((MainUI)this.getUI()).getVariabiliGlobali();
		this.l_VG.getBrowserAspectRatio();
		this.mainError = this.l_VG.Error;
/*
 * 
	    Page page = UI.getCurrent().getPage();
	    l_VG.browserWidth = page.getBrowserWindowWidth();
	    l_VG.browserHeight = page.getBrowserWindowHeight();
	    l_VG.browserAspectRatio = (double)l_VG.browserWidth / l_VG.browserHeight;
	    WebBrowser webBrowser = page.getWebBrowser();
		l_VG.browserDescription = webBrowser.getBrowserApplication();
		if (webBrowser.isAndroid()) {
			if (l_VG.browserHeight > l_VG.browserWidth) {
				l_VG.MobileMode = true;
    		} else {
    			l_VG.MobileMode = false;
    		}
		} else {
    		if (webBrowser.isIOS() || webBrowser.isIPhone() || webBrowser.isWindowsPhone()) {
    			l_VG.MobileMode = true;
    		} else {
    			l_VG.MobileMode = false;
    		}
		}
		//l_VG.MobileMode = true;
*/
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonLogin}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonLogin_buttonClick(final Button.ClickEvent event) {
		this.l_VG.forcedDesktop = this.desktopMode.getValue();
		if (this.l_VG.conPrincipale == null) {
			this.msgBox = MessageBox.createError();
			this.msgBox.withCaption("Open main connection");
			this.msgBox.withMessage(this.mainError);
	       	this.msgBox.withOkButton();
	       	this.msgBox.open();
	       	return;
		}
		String query;
		String step = "0";
	    try {
	        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	   		this.l_VG.conPrincipale = DriverManager.getConnection(this.l_VG.serverurl, this.l_VG.serverUser, this.l_VG.serverPassword);
	   		this.l_VG.traceUser.initConnection(this.l_VG.conPrincipale);
	    } catch (final Exception e) {
	      	this.msgBox = MessageBox.createError();
	      	this.msgBox.withCaption("Open main connection");
	       	this.msgBox.withMessage(e.toString()+"\n\n"+this.l_VG.serverName+"\n"+this.l_VG.serverUser+"\n"+this.l_VG.serverPassword+"\n\n"+this.l_VG.serverurl);
	       	this.msgBox.withOkButton();
	       	this.msgBox.open();
	       	return;
	    }
        step = "1";
        if (this.l_VG.conPrincipale == null) {
        	this.msgBox = MessageBox.createError();
        	this.msgBox.withCaption("Open main connection");
        	this.msgBox.withMessage(this.l_VG.serverName+"\n"+this.l_VG.serverUser+"\n"+this.l_VG.serverPassword+"\n\n"+this.l_VG.serverurl);
        	this.msgBox.withOkButton();
        	this.msgBox.open();
        	return;
        }
        step = "2";
		step = "3";
		this.l_VG.currentUserSaaS = new SaaSUser();
		query = "select * from [SaaSUser] where (Ambiente = '" + this.l_VG.AmbientiDisponibili[0] + "'";
		for (int i=1 ; i<this.l_VG.AmbientiDisponibili.length ; i++) {
			query += " or Ambiente = '" + this.l_VG.AmbientiDisponibili[i] + "'";
		}
        query += ") and [Username] = '" + this.txtUser.getValue().trim() + "' and [Password] = '" + this.passwordField.getValue().trim() + "'";
        query += " and [Attivo] = 1";
        try {
        	Statement stmt = this.l_VG.conPrincipale.createStatement();
        	ResultSet rs = stmt.executeQuery(query);
            rs.next();
            SaaSNewUser NewUserReferenceLoc = null;
            try {
                final Blob bl = rs.getBlob("NewUserReference");
                final InputStream fin = bl.getBinaryStream();
                final ObjectInputStream ois = new ObjectInputStream(fin);
                NewUserReferenceLoc = (SaaSNewUser)ois.readObject();
                ois.close();
             } catch (final Exception e1) {
                 NewUserReferenceLoc = new SaaSNewUser();
             }
            step = "4";
            this.l_VG.currentUserSaaS.NewUserReference = NewUserReferenceLoc;
            if (rs.getRow() == 0) {
            	final String error = "User " + this.txtUser.getValue().trim() + "/" + this.passwordField.getValue().trim() + " not found";
            	this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneTentativoLogIn, error);
            	this.msgBox = MessageBox.createError();
            	this.msgBox.withMessage(error);
            	//this.msgBox.withMessage(query);
            	this.msgBox.withOkButton();
            	this.msgBox.open();
        		return;
            }
            this.l_VG.currentUserSaaS._ID = rs.getInt("_ID");
            this.l_VG.currentUserSaaS.Ambiente = rs.getString("Ambiente");
            this.l_VG.currentUserSaaS.idClienteIndex = rs.getInt("idClienteIndex");
            this.l_VG.currentUserSaaS.idSottoClienteIndex = rs.getInt("idSottoClienteIndex");
            this.l_VG.currentUserSaaS.Username = rs.getString("Username");
            this.l_VG.currentUserSaaS.Password = rs.getString("Password");
            this.l_VG.currentUserSaaS.Tipo = rs.getInt("Tipo");
            this.l_VG.currentUserSaaS.dataLimiteTipo1 = rs.getDate("dataLimiteTipo1");
            this.l_VG.currentUserSaaS.nMaxRunTipo2 = rs.getInt("nMaxRunTipo2");
            this.l_VG.currentUserSaaS.currentRunTipo2 = rs.getInt("currentRunTipo2");
            this.l_VG.currentUserSaaS.nMaxSearchTipo3 = rs.getInt("nMaxSearchTipo3");
            this.l_VG.currentUserSaaS.currentSearchTipo3 = rs.getInt("currentSearchTipo3");
            this.l_VG.currentUserSaaS.nMaxOfferteTipo4 = rs.getInt("nMaxOfferteTipo4");
            this.l_VG.currentUserSaaS.currentOfferteTipo4 = rs.getInt("currentOfferteTipo4");
            this.l_VG.currentUserSaaS.PostFissoDB = "";
            rs.close();
            stmt.close();
            this.l_VG.traceUser.initUser(this.l_VG.currentUserSaaS);
            this.l_VG.Ambiente = "";
            for (int i=0 ; i<this.l_VG.AmbientiDisponibili.length ; i++) {
            	if (this.l_VG.currentUserSaaS.Ambiente.equals(this.l_VG.AmbientiDisponibili[i])) {
            		this.l_VG.Ambiente = this.l_VG.currentUserSaaS.Ambiente;
            	}
            }
            
    		if (this.l_VG.Ambiente.equals("")) {
            	this.msgBox = MessageBox.createError();
            	this.msgBox.withCaption("Ambient");
            	String message = "Program Aveilable only on ";
            	for (int i=0 ; i<this.l_VG.AmbientiDisponibili.length ; i++) {
            		message += this.l_VG.AmbientiDisponibili[i] + " ";
            	}
            	message += "Ambients";
            	this.msgBox.withMessage(message);
            	this.msgBox.withOkButton();
            	this.msgBox.open();
    			return;
    		}
            if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneLogIn)) {
            	String memo = "";
            	if (this.l_VG.currentUserSaaS.Tipo == SaaSUser.TipoNRun) {
            		memo = this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Login Disponibili Raggiunto");
            	} else if (this.l_VG.currentUserSaaS.Tipo == SaaSUser.TipoData) {
            		memo = this.l_VG.utilityTraduzioni.TraduciStringa("Licenza Scaduta");
            	}
            	this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneLicenzaScaduta, memo);
            	this.msgBox = MessageBox.createWarning();
            	this.msgBox.withMessage(memo);
            	this.msgBox.withOkButton();
            	this.msgBox.open();
        		return;
            }
/*
            Calendar calendar = Calendar.getInstance();
            java.sql.Timestamp oggi = new java.sql.Timestamp(calendar.getTime().getTime());
    	    Page page = UI.getCurrent().getPage();
    	    WebBrowser webBrowser = page.getWebBrowser();
    	    Date now = webBrowser.getCurrentDate();
    	    Notification.show(oggi.toString()+"     "+now.toString());
*/
            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneLogIn);
            this.l_VG.idClienteIndexVG = this.l_VG.currentUserSaaS.idClienteIndex;
            this.l_VG.idSottoClienteIndexVG = this.l_VG.currentUserSaaS.idSottoClienteIndex;
            this.l_VG.variabiliGlobaliCliente = new VariabiliGlobaliCliente(this.l_VG);
            this.l_VG.variabiliGlobaliCliente.setCliente();
            
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://" + this.l_VG.serverName + ";databaseName=" + "SellFanSaaSTecnico" + this.l_VG.currentUserSaaS.Ambiente + this.l_VG.currentUserSaaS.PostFissoDB;
            this.l_VG.conTecnica = DriverManager.getConnection(url, this.l_VG.serverUser, this.l_VG.serverPassword);
            if (this.l_VG.conTecnica == null) {
            	this.msgBox = MessageBox.createError();
            	this.msgBox.withCaption("Technical connection");
            	this.msgBox.withMessage("Error opening technical connection");
            	this.msgBox.withOkButton();
            	this.msgBox.open();
            	return;
            }
            step = "5";
            url = "jdbc:sqlserver://" + this.l_VG.serverName + ";databaseName=" + "SellFanSaaSPrivato" + this.l_VG.currentUserSaaS.Ambiente + this.l_VG.currentUserSaaS.PostFissoDB;
            this.l_VG.conPrivata = DriverManager.getConnection(url, this.l_VG.serverUser, this.l_VG.serverPassword);
            if (this.l_VG.conPrivata == null) {
            	this.msgBox = MessageBox.createError();
            	this.msgBox.withCaption("Private connection");
            	this.msgBox.withMessage("Error opening private connection");
            	this.msgBox.withOkButton();
            	this.msgBox.open();
            	return;
            }
            url = "jdbc:sqlserver://" + this.l_VG.serverName + ";databaseName=" + "SellFanSaaSTranscodifica" + this.l_VG.currentUserSaaS.Ambiente + this.l_VG.currentUserSaaS.PostFissoDB;
            this.l_VG.conTranscodifica = DriverManager.getConnection(url, this.l_VG.serverUser, this.l_VG.serverPassword);
            if (this.l_VG.conTranscodifica == null) {
            	this.msgBox = MessageBox.createError();
            	this.msgBox.withCaption("Transcode connection");
            	this.msgBox.withMessage("Error opening transcode connection");
            	this.msgBox.withOkButton();
            	this.msgBox.open();
            	return;
            }
            url = "jdbc:sqlserver://" + this.l_VG.serverName + ";databaseName=" + "SellFanSaaSCRMv2" + this.l_VG.currentUserSaaS.Ambiente + this.l_VG.currentUserSaaS.PostFissoDB;
            this.l_VG.conCRMv2 = DriverManager.getConnection(url, this.l_VG.serverUser, this.l_VG.serverPassword);
            if (this.l_VG.conCRMv2 == null) {
            	this.msgBox = MessageBox.createError();
            	this.msgBox.withCaption("Offer connection");
            	this.msgBox.withMessage("Error opening offer connection");
            	this.msgBox.withOkButton();
            	this.msgBox.open();
            	return;
            }
            step = "5.1";
            url = "jdbc:sqlserver://" + this.l_VG.serverName + ";databaseName=" + "SellFanSaaSConfig" + this.l_VG.currentUserSaaS.Ambiente + this.l_VG.currentUserSaaS.PostFissoDB;
            this.l_VG.conConfig = DriverManager.getConnection(url, this.l_VG.serverUser, this.l_VG.serverPassword);
            if (this.l_VG.conConfig == null) {
            	this.msgBox = MessageBox.createError();
            	this.msgBox.withCaption("Configuration connection");
            	this.msgBox.withMessage("Error opening configuration connection");
            	this.msgBox.withOkButton();
            	this.msgBox.open();
            	return;
            }
            int n = 99;
            stmt = this.l_VG.conTecnica.createStatement();
            query = "select * from " + "[Esecuzioni]" + " where tipo = '";
            step = "TR1";
            rs = stmt.executeQuery(query + "TR1'");
            rs.next();
            n = rs.getInt("N");
            if (n == 0) {
            	n = 1;
            }
            this.l_VG.pannelloRicercaEsecuzioniTR1 = new String[n];
            for (int i=0 ; i<n ; i++) {
                this.l_VG.pannelloRicercaEsecuzioniTR1[i] = rs.getString("E" + Integer.toString(i));
            }
            rs.close();
            step = "TRDesc1";
            rs = stmt.executeQuery(query + "TRDesc1'");
            rs.next();
            for (int i=0 ; i<n ; i++) {
                this.l_VG.mapEsecuzioniDesc.put(this.l_VG.pannelloRicercaEsecuzioniTR1[i], rs.getString("E" + Integer.toString(i)));
            }
            rs.close();
            step = "TR2";
            rs = stmt.executeQuery(query + "TR2'");
            rs.next();
            n = rs.getInt("N");
            if (n == 0) {
				n = 1;
			}
            this.l_VG.pannelloRicercaEsecuzioniTR2 = new String[n];
            for (int i=0 ; i<n ; i++) {
                this.l_VG.pannelloRicercaEsecuzioniTR2[i] = rs.getString("E" + Integer.toString(i));
            }
            rs.close();
            step = "TRDesc2";
             rs = stmt.executeQuery(query + "TRDesc2'");
            rs.next();
            for (int i=0 ; i<n ; i++) {
                this.l_VG.mapEsecuzioniDesc.put(this.l_VG.pannelloRicercaEsecuzioniTR2[i], rs.getString("E" + Integer.toString(i)));
            }
            rs.close();
            step = "DA";
            rs = stmt.executeQuery(query + "DA'");
            rs.next();
            n = rs.getInt("N");
            if (n == 0) {
				n = 1;
			}
            this.l_VG.pannelloRicercaEsecuzioniDA = new String[n];
            for (int i=0 ; i<n ; i++) {
                this.l_VG.pannelloRicercaEsecuzioniDA[i] = rs.getString("E" + Integer.toString(i));
            }
            rs.close();
            step = "DADesc";
            rs = stmt.executeQuery(query + "DADesc'");
            rs.next();
            for (int i=0 ; i<n ; i++) {
                this.l_VG.mapEsecuzioniDesc.put(this.l_VG.pannelloRicercaEsecuzioniDA[i], rs.getString("E" + Integer.toString(i)));
            }
			rs.close();
            stmt.close();
            step = "6";
            query = "dbAccessori";
            this.l_VG.dbAccessori = new DBAccessori(this.l_VG.conTecnica, this.l_VG.conPrivata, this.l_VG.utilityTraduzioni, this.l_VG.dbTableQualifier);
            step = "7";
            query = "dbPrivato";
            this.l_VG.dbPrivato = new DBPrivato(this.l_VG.conPrivata, this.l_VG.dbTableQualifier);
            step = "8";
            query = "utilityCliente";
            if (this.l_VG.currentUserSaaS.idClienteIndex == 1) {
                this.l_VG.utilityCliente = new Cliente01UtilityCliente(this.l_VG.utilitySql.DBTypeMicrosoftSql, this.l_VG.idClienteIndexVG, this.l_VG.idSottoClienteIndexVG, this.l_VG.Paths);
            } else if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                this.l_VG.utilityCliente = new Cliente04UtilityCliente(this.l_VG.utilitySql.DBTypeMicrosoftSql, this.l_VG.idClienteIndexVG, this.l_VG.idSottoClienteIndexVG, this.l_VG.Paths);
            } else if (this.l_VG.currentUserSaaS.idClienteIndex == 98) {
                this.l_VG.utilityCliente = new Cliente98UtilityCliente(this.l_VG.utilitySql.DBTypeMicrosoftSql, this.l_VG.idClienteIndexVG, this.l_VG.idSottoClienteIndexVG, this.l_VG.Paths);
            }
            this.l_VG.UMDefaultIndex = this.l_VG.utilityCliente.getUMDefaultIndex();
            step = "8.1";
            query = "dbParam";
            this.l_VG.dbParam = new DBParam(this.l_VG.utilityCliente, this.l_VG.Paths);
            final String nomeFileINI = this.l_VG.Paths.rootResources + "clienteparam" + Integer.toString(this.l_VG.utilityCliente.idClienteIndex) + "/clienteparam.xml";
            final File fileIn = new File(nomeFileINI);
            @SuppressWarnings("resource")
			InputStream fis = new FileInputStream(fileIn);
            if (!this.l_VG.dbParam.impostaCliente(fis)) {
        		this.msgBox = MessageBox.createError();
        		this.msgBox.withMessage("l_VG.dbParam.impostaCliente Error\nStep:"+step);
        		this.msgBox.withOkButton();
        		this.msgBox.open();
        		return;
            }
            if (this.l_VG.utilityCliente.idClienteIndex != Costanti.SOTTOCLIENTE_INDEFINITO) {
                fis = new FileInputStream(fileIn);
                if (!this.l_VG.dbParam.impostaSottoCliente(fis)) {
            		this.msgBox = MessageBox.createError();
            		this.msgBox.withMessage("l_VG.dbParam.impostaSottoCliente Error\nStep:"+step);
            		this.msgBox.withOkButton();
            		this.msgBox.open();
            		return;
                }
            }
/*
            msgBox.setMessage(Integer.toString(l_VG.utilityCliente.idClienteIndex)+"   "+Integer.toString(l_VG.utilityCliente.idSottoClienteIndex));
            msgBox.setMessage2(l_VG.Paths.rootLOGO+"     "+l_VG.Paths.rootIMG+"\n"+l_VG.utilityCliente.DenominazioneCliente+"\n"+l_VG.utilityCliente.DenominazioneClienteSRL);
            UI.getCurrent().addWindow(msgBox);
*/
            step = "8.2";
            query = "utilityCliente init";
            if (this.l_VG.currentUserSaaS.idClienteIndex == 1)
                this.l_VG.dbTecnico = new Cliente01DbTecnico();

            this.l_VG.dbConfig.init(this.l_VG.conConfig, this.l_VG.currentUserSaaS._ID);
            this.l_VG.ParametriVari = this.l_VG.dbConfig.loadParametriVari();

            if (l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssPIU==0.0 && l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssPIU==0.0) {
                l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssPIU=.2;
                l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssMENO =.1;
            }
            this.l_VG.ParametriVariHTML = this.l_VG.dbConfig.loadParametriVariHTML();
            if (this.l_VG.menuStyle) {
				this.l_VG.ParametriVariHTML.menuStyleEnabled = this.l_VG.menuStyle;
			}
            this.l_VG.utilityCliente.init(this.l_VG.gestioneAccessori, this.l_VG.utilityTraduzioni, this.l_VG.parametriModelloCompletoForPrint, this.l_VG.ParametriVari.pannelloCondizioniOverRpm);
            this.l_VG.utilityCliente.initDBClass(this.l_VG.dbTecnico, this.l_VG.dbAccessori, this.l_VG.dbPrivato, this.l_VG.dbTableQualifier);
            this.l_VG.utilityCliente.openConnectionClienteSaaS(this.l_VG.serverName, this.l_VG.currentUserSaaS.Ambiente, "", this.l_VG.serverUser, this.l_VG.serverPassword);
            this.l_VG.utilityCliente.initConnection(this.l_VG.utilitySql.DBTypeMicrosoftSql, this.l_VG.conTecnica, this.l_VG.conPrivata, this.l_VG.conTranscodifica);
            this.l_VG.CorrettorePotenzaMotoreDaConfig = this.l_VG.utilityCliente.loadPreferenzeCliente(this.l_VG.conConfig, this.l_VG.currentUserSaaS._ID);
            this.l_VG.CorrettorePotenzaMotoreCorrente = this.l_VG.CorrettorePotenzaMotoreDaConfig;
            this.l_VG.listaW = this.l_VG.utilityCliente.getWarning();
            this.l_VG.dataVersione = this.l_VG.utilityCliente.loadVersionNumber();
            step = "9";
            query = "";
            this.l_VG.dbTecnico.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.utilityCliente);
            this.l_VG.dbTecnico.catalogoAutoSerieFlag = this.l_VG.catalogoAutoSerieFlag;
            this.l_VG.dbTecnico.pannelloRicercaRpmMotoreDAFlag = this.l_VG.pannelloRicercaRpmMotoreDAFlag;
            this.l_VG.dbTecnico.scegliMotoreTRFlag = this.l_VG.scegliMotoreTRFlag;
            this.l_VG.dbTecnico.scegliMotoreDAFlag = this.l_VG.scegliMotoreDAFlag;
            this.l_VG.dbTecnico.catalogoAutoSerieFlag = this.l_VG.catalogoAutoSerieFlag;
            step = "9.1";
            this.l_VG.currentCitFont = this.l_VG.dbConfig.loadFont();
            this.l_VG.utilityTraduzioni.init(this.l_VG.conTecnica, "TraduzioniStringheCliente", this.l_VG.utilitySql.DBTypeMicrosoftSql, this.l_VG.conPrincipale, "TraduzioniStringheSellFan", this.l_VG.utilitySql.DBTypeMicrosoftSql);
            String colonna = (this.l_VG.currentCitFont.linguaDisplay.colonnaLinguaForTraduzioni.equals("Lingua??"))? "Lingua5" : this.l_VG.currentCitFont.linguaDisplay.colonnaLinguaForTraduzioni;
            if (this.l_VG.currentUserSaaS.idSottoClienteIndex==10) {
                this.l_VG.utilityTraduzioni.setLingua(6, true);
                this.l_VG.utilityTraduzioni.setLinguaOutput("Lingua6", false);
            } else {
                this.l_VG.utilityTraduzioni.setLingua(colonna, this.l_VG.currentCitFont.linguaDisplay.speciale);
                this.l_VG.utilityTraduzioni.setLinguaOutput(this.l_VG.currentCitFont.linguaOutput.colonnaLinguaForTraduzioni, this.l_VG.currentCitFont.linguaOutput.speciale);
            }
            this.l_VG.dbTecnico.pannelloCatalogoTutteLeClassiFlag = this.l_VG.ParametriVari.pannelloCatalogoTutteLeClassiFlag;
            this.l_VG.currentUtilizzatore = this.l_VG.dbConfig.loadUtilizzatore();
            for (int i=0 ; i<4 ; i++) {
                this.l_VG.ElencoGruppiSelezioneSerie[i] = this.l_VG.dbConfig.loadGruppoSelezioneSerie(i);
            }
            this.l_VG.ElencoKitAccessori.clear();
            
            
            for (int i=0; i<10; i++) {
                this.l_VG.ElencoKitAccessori.add(this.l_VG.dbConfig.loadKitAccessori(i));
            }
            this.l_VG.CACatalogo = this.l_VG.dbConfig.loadCACatalogo();
            this.l_VG.CA020Catalogo = this.l_VG.dbConfig.loadCA020Catalogo();
            this.l_VG.CARicerca = (CondizioniAmbientali)this.l_VG.CACatalogo.clone();
            this.l_VG.CA020Ricerca = (CondizioniAmbientali)this.l_VG.CA020Catalogo.clone();
            this.l_VG.CAFree = (CondizioniAmbientali)this.l_VG.CACatalogo.clone();
            this.l_VG.dbConfig.loadUnitaMisuraIndex(this.l_VG.UMDefaultIndex);
            this.l_VG.setUMFromDefaultIndex();
           
//            l_VG.utilityFisica.settemperaturaRiferimentoC((int)l_VG.CACatalogo.temperatura);
//            l_VG.PressioneAspirazioneRiferimentoPa = l_VG.utilityFisica.getpressioneAtmosfericaRiferimentoPa();
            step = "10";
            this.l_VG.ElencoSerie = this.l_VG.dbTecnico.getElencoSerie(this.l_VG.idTranscodificaIndex);
            //Notification.show(Integer.toString(l_VG.idTranscodificaIndex));
            if (this.l_VG.ElencoSerie == null) {
        		this.msgBox = MessageBox.createError();
        		this.msgBox.withMessage("l_VG.ElencoSerie==null Error\nStep:"+step);
        		this.msgBox.withOkButton();
        		this.msgBox.open();
        		return;
            }
            this.l_VG.ElencoSerieDisponibili = this.l_VG.dbConfig.loadSerieDisponibili();
            if (this.l_VG.ElencoSerieDisponibili == null || this.l_VG.ElencoSerieDisponibili.size() != this.l_VG.ElencoSerie.size()) {
            	if (this.l_VG.ElencoSerieDisponibili == null) {
            		this.l_VG.ElencoSerieDisponibili = new ArrayList<>();
            	} else {
            		this.l_VG.ElencoSerieDisponibili.clear();
            	}
                for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
                    this.l_VG.ElencoSerieDisponibili.add(Boolean.TRUE);
                }
            }
            this.l_VG.dbCRMv2.init(this.l_VG.conCRMv2, this.l_VG.dbTableQualifier);
            this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
            this.l_VG.dbCRMv2.loadElencoAnagraficaClienti(this.l_VG.ElencoClienti);
            this.l_VG.ERP327Build.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.utilityCliente);
            if (l_VG.currentUserSaaS.idSottoClienteIndex==6) {
                l_VG.ParametriVari.clienteSpeciale = Client.Moro;
                l_VG.ListaSerieMoro = l_VG.dbTecnico.getSerieMoro();
                l_VG.utilityClienteMoro = new UtilityClienteMoro((Cliente04UtilityCliente)l_VG.utilityCliente);
            }
            step = "11";
            this.l_VG.ricerca.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.utilityCliente, this.l_VG.dbTecnico);
            this.l_VG.ricerca.initStaticFlag(this.l_VG.nTemperatureLimite, this.l_VG.catalogoAutoSerieFlag, this.l_VG.pannelloRicercaMaggiorazioneDAGiranteFlag, this.l_VG.pannelloRicercaMaggiorazioneDAGiranteMin, this.l_VG.pannelloRicercaMaggiorazioneDAGiranteMax, this.l_VG.pannelloRicercaMaggiorazioneDAGiranteNStep, this.l_VG.pannelloRicercaDArpmMenoPercentuale, this.l_VG.pannelloRicercaDArpmPiuPercentuale, this.l_VG.pannelloRicercaAutoSceltaMotoreFlag, this.l_VG.pannelloRicercaDArpmPiuMenoFlag, this.l_VG.idTranscodificaIndex);
            this.l_VG.campoVentilatoreSort = VentilatoriSort.sortRendimanto;
            this.l_VG.crescenteVentilatoreSort = false;
           //Notification.show(Integer.toString(l_VG.ElencoPreventivi.size())+"  "+Integer.toString(l_VG.ElencoClienti.size()));
            step = "13";
            this.l_VG.elencoGruppiAccessori = this.l_VG.dbTecnico.getAccessoriGruppi();
/*
            if (l_VG.elencoGruppiAccessori == null) {
            	Notification.show("l_VG.elencoGruppiAccessori == null");
            } else {
            	Notification.show("elencoGruppiAccessori "+Integer.toString(l_VG.elencoGruppiAccessori.size()));
            }
*/
            step = "14";
            this.l_VG.ventilatoreFisicaParameter = this.l_VG.dbTecnico.getVentilatoreFisicaParameter();
			this.l_VG.logostreamresource = this.l_VG.getStreamResourceForImageFile(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logo.png");
			if (this.l_VG.currentUserSaaS.idClienteIndex == 1) {
	            this.l_VG.accessoriAzioni = new AccessoriAzioni(this.l_VG);
	            this.l_VG.accessoriAzioni.init(this.l_VG.utilityCliente);
			} else if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
	            this.l_VG.accessoriAzioni = new Cliente04AccessoriAzioni(this.l_VG);
	            this.l_VG.accessoriAzioni.init(this.l_VG.utilityCliente);
			}
			step = "14.1";
            url = "jdbc:sqlserver://" + this.l_VG.serverName + ";databaseName=" + "CitISTAT" + this.l_VG.currentUserSaaS.PostFissoDB;
            this.l_VG.conISTAT = DriverManager.getConnection(url, this.l_VG.serverUser, this.l_VG.serverPassword);
            if (this.l_VG.conISTAT == null) {
        		this.msgBox = MessageBox.createError();
        		this.msgBox.withMessage(url+"\nStep:"+step);
        		this.msgBox.withOkButton();
        		this.msgBox.open();
            	return;
            }
            step = "14.2";
            this.l_VG.utilityIstat = new UtilityIstat(this.l_VG.conISTAT);
            this.l_VG.utilityIstat.caricaElencoRegioni();
            this.l_VG.assiGraficiDefault = new AssiGrafici(this.l_VG.utilityTraduzioni);
            this.l_VG.presetRicerca = loadPresetRicerca();
            this.l_VG.dbConfig.loadAssiGrafici(this.l_VG.assiGraficiDefault);
            this.l_VG.pannelloPreventivoAzioniSpeciali0Desc = "Motore " + this.l_VG.utilityCliente.DenominazioneCliente;
            this.l_VG.pannelloPreventivoAzioniSpeciali1Desc = "Motore Cliente";
            this.l_VG.pannelloPreventivoAzioniSpeciali2Desc = "Senza Motore";
            step = "14.3";
            this.l_VG.caricaElencoGas();
            this.l_VG.preventivoCurrent = null;
            this.l_VG.preventivoCurrentIndex = -1;
            this.l_VG.SelezioneDati = new SelezioneDati();
            step = "14.4";
            this.l_VG.variabiliGlobaliCliente.setClienteLingue();
            if (this.l_VG.currentUserSaaS.idClienteIndex == 4 && this.l_VG.currentLivelloUtente != Costanti.utenteTecnico && !isCondizioniVenditaAccettate()) {
            	step = "14.5a";
            	final pannelloCondizioniVendita pannello = new pannelloCondizioniVendita();
            	step = "14.6";
            	pannello.setVariabiliGlobali(this.l_VG);
            	step = "14.7";
            	if (pannello.streamFile == null) {
            		Navigation.to("MainView").navigate();
            		return;
            	}
            	step = "14.8";
            	this.msgBox = MessageBox.createQuestion();
            	//msgBox.withCaption("General Terms and Condiction of Sale");
            	this.msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Condizioni Generali di Vendita"));
            	this.msgBox.withMessage(pannello);
        		this.msgBox.withOkButton(() -> {
        				this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneCondizioniVendidaAccettate, "Accettate");
        				Navigation.to("MainView").navigate();
        			});
            	this.msgBox.withNoButton(() -> {
	            		try {
	                		this.l_VG.conTecnica.close();
	                		this.l_VG.conPrivata.close();
	                		this.l_VG.conTranscodifica.close();
	                		this.l_VG.conCRMv2.close();
	                		this.l_VG.conConfig.close();
	            		} catch (final Exception e) {
	            			
	            		}
        			});
            	//msgBox.getButton(ButtonType.OK).setCaption("Agree");
            	//msgBox.getButton(ButtonType.NO).setCaption("Don't Agree");
            	this.msgBox.getButton(ButtonType.OK).setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Accetto"));
            	this.msgBox.getButton(ButtonType.NO).setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Rifiuto"));
            	this.msgBox.open();
            } else {
            	step = "14.5b";
                query = "select * from [SaaSParam] where Chiave = 'PrezziAggiornati'";
                boolean PrezziAggiornati;
		try {
                    stmt = this.l_VG.conPrincipale.createStatement();
                    rs = stmt.executeQuery(query);
                    rs.next();
                    PrezziAggiornati = rs.getBoolean("Valore");
                } catch (Exception e) {
                    PrezziAggiornati = true;
                }
                if (PrezziAggiornati == false) {
                    MessageBox warning = MessageBox.createInfo();
                    warning.withOkButton();
                    warning.withMessage(l_VG.utilityTraduzioni.TraduciStringaOutput("prezzi di listino non aggiornati"));
                    warning.open();
                }
                Navigation.to("MainView").navigate();
            }
        } catch (final Exception e) {
        	this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneTentativoLogIn, e.toString());
        	//StringWriter errors = new StringWriter();
        	//e.printStackTrace(new PrintWriter(errors));
        	this.msgBox = MessageBox.createError();
        	this.msgBox.withCaption("Fatal Error");
        	this.msgBox.withMessage(e.toString()+"\n"+step+"\n"+"Wrong User/Password");
        	this.msgBox.withOkButton();
        	//msgBox.open();
    		return;
        	//Notification.show(e.toString());
    		
        	//Notification.show("User/Password not Found.");
        }
	}
	
	private boolean isCondizioniVenditaAccettate() {
		try {
			final String query = "select * from [SaaSTraceUser] where idClienteSaaS = " + Integer.toString(this.l_VG.currentUserSaaS._ID) + " and AzioneIndex = " + Integer.toString(this.l_VG.traceUser.AzioneCondizioniVendidaAccettate);
        	final Statement stmt = this.l_VG.conPrincipale.createStatement();
        	final ResultSet rs = stmt.executeQuery(query);
        	rs.next();
        	//final int idClienteSaaS = rs.getInt("idClienteSaaS");
        	rs.close();
        	stmt.close();
        	return true;
		} catch (final Exception e) {
			//Notification.show(e.toString());
			return false;
		}
	}
	
	public void enableNewUserRequestOK(final boolean value) {
		this.msgBox.getButton(ButtonType.OK).setEnabled(value);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonNewUserRequest}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonNewUserRequest_buttonClick(final Button.ClickEvent event) {
		final pannelloNewUserRequest pannello = new pannelloNewUserRequest();
		pannello.setGetUserView(this);
		this.msgBox = MessageBox.create();
		this.msgBox.withCaption("New Access Authorization Request");
		this.msgBox.withMessage(pannello);
		this.msgBox.withAbortButton();
		this.msgBox.withOkButton(() -> {
			MessageBox msgBox1;
			if (this.l_VG.conPrincipale == null) {
		        try {
		            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		       		this.l_VG.conPrincipale = DriverManager.getConnection(this.l_VG.serverurl, this.l_VG.serverUser, this.l_VG.serverPassword);
		        } catch (final Exception e) {
		        	
		        }
			}
	        if (this.l_VG.conPrincipale != null) {
	        	try {
		            final String sqlString = "INSERT INTO [SaaSNewUser] (pending, Ambiente, data, name, surname, mail, azienda, telefono, indirizzo0, indirizzo1, indirizzo2, agent, agentmail, memo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		            final PreparedStatement stmt = this.l_VG.conPrincipale.prepareStatement(sqlString);
		            stmt.setBoolean(1, true);
		            stmt.setString(2, this.l_VG.Ambiente);
		            if (pannello.getDati(stmt)) {
		            	stmt.execute();
		            }
	        	} catch (final Exception e) {
		        	msgBox1 = MessageBox.createError();
		        	msgBox1.withCaption("New Access Authorization Request");
		        	msgBox1.withMessage("An Error Occours\n\nRetry Later Please");
		        	msgBox1.withOkButton();
		        	msgBox1.open();
		        	return;
	        	}
	        	msgBox1 = MessageBox.createInfo();
	        	msgBox1.withCaption("New Access Authorization Request");
	        	msgBox1.withMessage("As Soon as Possible You Will Received the Authorization on Your e-mail");
	        	msgBox1.withOkButton();
	        	msgBox1.open();
	        }
			});
		this.msgBox.getButton(ButtonType.OK).setEnabled(false);
		this.msgBox.open();
	}

        public PresetRicerca loadPresetRicerca () {
            PresetRicerca result = null;
            String query = "select Dati, Caricato from [Bridge] where UserID = '" + l_VG.currentUserSaaS._ID + "'";
            try {
                String res = "";
        	Statement stmt = this.l_VG.conPrincipale.createStatement();
        	ResultSet rs = stmt.executeQuery(query);
                while(rs.next()) 
                {
                    if (rs.getBoolean("Caricato"))
                        return result;
                    res = rs.getString("Dati");
                }
                rs.close();
                stmt.close();
                String[] datas = res.split("/");
                result = new PresetRicerca(Double.parseDouble(datas[0].replace(",",".")), Double.parseDouble(datas[1].replace(",",".")));
                return result;
            }
            catch (Exception e) {
                return result;
                    }
        }
	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout2 = new XdevVerticalLayout();
		this.panel2 = new XdevPanel();
		this.absoluteLayout2 = new XdevAbsoluteLayout();
		this.txtUser = new XdevTextField();
		this.buttonLogin = new XdevButton();
		this.buttonNewUserRequest = new XdevButton();
		this.passwordField = new XdevPasswordField();
		this.desktopMode = new XdevCheckBox();
		this.navigator = new MainView();
	
		this.setResponsive(true);
		this.verticalLayout2.setMargin(new MarginInfo(false));
		this.panel2.setCaption("Login");
		this.panel2.setResponsive(true);
		this.absoluteLayout2.setResponsive(true);
		this.txtUser.setCaption("Username");
		this.txtUser.setTabIndex(1);
		this.txtUser.setResponsive(true);
		this.buttonLogin.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/logon.png"));
		this.buttonLogin.setCaption("Login");
		this.buttonLogin.setTabIndex(3);
		this.buttonLogin.setResponsive(true);
		this.buttonNewUserRequest.setCaption("New User Request");
		this.buttonNewUserRequest.setTabIndex(4);
		this.passwordField.setCaption("Password");
		this.passwordField.setTabIndex(2);
		this.desktopMode.setCaption("Force Desktop Mode");
	
		this.txtUser.setWidth(150, Unit.PIXELS);
		this.absoluteLayout2.addComponent(this.txtUser, "left:75px; top:29px");
		this.absoluteLayout2.addComponent(this.desktopMode, "left:75px; top:120px");
		this.absoluteLayout2.addComponent(this.buttonLogin, "left:212px; top:154px");
		this.absoluteLayout2.addComponent(this.buttonNewUserRequest, "left:22px; top:154px");
		this.passwordField.setWidth(150, Unit.PIXELS);
		this.absoluteLayout2.addComponent(this.passwordField, "left:75px; top:85px");
		this.absoluteLayout2.setSizeFull();
		this.panel2.setContent(this.absoluteLayout2);
		this.panel2.setWidth(300, Unit.PIXELS);
		this.panel2.setHeight(230, Unit.PIXELS);
		//this.desktopMode.setWidth(100, Unit.PERCENTAGE);
		//this.desktopMode.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.panel2);
		this.verticalLayout2.setComponentAlignment(this.panel2, Alignment.MIDDLE_CENTER);
		//this.verticalLayout2.addComponent(this.desktopMode);
		
		this.verticalLayout2.setExpandRatio(this.panel2, 10.0F);
		this.verticalLayout2.setSizeFull();
		this.setContent(this.verticalLayout2);
		this.setSizeFull();
	
		this.buttonLogin.addClickListener(event -> this.buttonLogin_buttonClick(event));
		this.buttonNewUserRequest.addClickListener(event -> this.buttonNewUserRequest_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	@SuppressWarnings("unused")
	private MainView navigator;
	private XdevButton buttonLogin, buttonNewUserRequest;
	private XdevAbsoluteLayout absoluteLayout2;
	private XdevPasswordField passwordField;
	private XdevPanel panel2;
	private XdevTextField txtUser;
	private XdevVerticalLayout verticalLayout2;
	private XdevCheckBox desktopMode;
	// </generated-code>


}
