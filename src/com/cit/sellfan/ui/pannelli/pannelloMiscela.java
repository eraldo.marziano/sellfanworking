package com.cit.sellfan.ui.pannelli;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.myjavalib.UtilityFisica.Gas;
import cit.myjavalib.UtilityFisica.UtilityFormuleChimiche;
import cit.utility.LettereGreche;

public class pannelloMiscela extends Panel {
	private VariabiliGlobali l_VG;
    private final UtilityFormuleChimiche utilityFormuleChimiche = new UtilityFormuleChimiche();
	private final Container containerGas = new IndexedContainer();
	private final ArrayList<TextFieldDouble> elencoPesi = new ArrayList<>();
	private double densitaGasLocale;
        private StringBuilder descrizioneGas = new StringBuilder("");

	/**
	 * 
	 */
	public pannelloMiscela() {
		super();
		this.initUI();

		this.containerGas.removeAllItems();
		this.containerGas.addContainerProperty("descrizione", String.class, "");
		this.containerGas.addContainerProperty("formula", XdevLabel.class, "");
		this.containerGas.addContainerProperty("densita", Double.class, 0.0);
		this.containerGas.addContainerProperty("UM", XdevLabel.class, "");
		this.containerGas.addContainerProperty("peso", TextFieldDouble.class, 0);
		this.containerGas.addContainerProperty("per100", String.class, "%");
		this.tableGas.setContainerDataSource(this.containerGas);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		Nazionalizza();
	}
	
	public void Nazionalizza() {
		this.labelDensitaFluido.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Densità fluido a")+" 20 [°C] "+this.l_VG.utilityTraduzioni.TraduciStringa("e") + " 0 [m] " + this.l_VG.utilityTraduzioni.TraduciStringa("s.l.m"));
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.UMPressioneCorrente.nDecimaliStampa);
		this.labelPressione.setValue(this.l_VG.utility.giuntaStringHtml("--> ", this.l_VG.utilityTraduzioni.TraduciStringa("pressione"), " ", this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.utilityFisica.getpressioneAtmosfericaRiferimentoPa(), this.l_VG.UMPressioneCorrente)), " ", this.l_VG.UMPressioneCorrente.simbolo));
		
		this.tableGas.setColumnHeader("descrizione", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableGas.setColumnHeader("formula", "");
		this.tableGas.setColumnHeader("densita", LettereGreche.rhoMinuscolo);
		this.tableGas.setColumnHeader("UM", "");
		this.tableGas.setColumnHeader("peso", "%");
		this.tableGas.setColumnHeader("per100", "");
	}
	
	public void init() {
		this.tableGas.setEnabled(false);
		try {
			this.tableGas.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
			
		}
		this.elencoPesi.clear();
		for (int i=0 ; i<this.l_VG.ElencoGas.size() ; i++) {
			final String itemID = Integer.toString(i);
			final Object obj[] = new Object[6];
			final Gas l_gas = this.l_VG.ElencoGas.get(i);
			if (this.l_VG.currentCitFont.idLinguaDisplay.equals("IT")) {
				obj[0] = l_gas.descrizione;
			} else {
				obj[0] = l_gas.descrizioneGB;
			}
			final XdevLabel flb = new XdevLabel();
			flb.setWidth(100, Unit.PERCENTAGE);
			flb.setHeight(-1, Unit.PIXELS);
			flb.setContentMode(ContentMode.HTML);
			flb.setValue(this.utilityFormuleChimiche.TraduciFormulaChimicaHTML(l_gas.formula));
			obj[1] = flb;
			obj[2] = l_gas.densitaRelativa * this.l_VG.utilityFisica.densitaAria20C0m;
			final XdevLabel umlb = new XdevLabel();
			umlb.setWidth(100, Unit.PERCENTAGE);
			umlb.setHeight(-1, Unit.PIXELS);
			umlb.setContentMode(ContentMode.HTML);
			umlb.setValue("<html>[kg/m<sup>3</sup>]</html>");
			obj[3] = umlb;
			final TextFieldDouble plb = new TextFieldDouble();
			plb.setWidth(100, Unit.PERCENTAGE);
			plb.setHeight(-1, Unit.PIXELS);
			plb.setValue(l_gas.percentuale);
			plb.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						if (!pannelloMiscela.this.tableGas.isEnabled()) {
							return;
						}
						calcolaDensita();
						pannelloMiscela.this.l_VG.MainView.setMemo(Double.toString(plb.getDoubleValue()));
					}
		    });
			if (i == 0) {
				plb.setEnabled(false);
			}
			obj[4] = plb;
			this.elencoPesi.add(plb);
			obj[5] = "%";
			this.tableGas.addItem(obj, itemID);
		}
		this.tableGas.setEnabled(true);
		calcolaDensita();
	}
	
	public void aggiornaPesiGas() {
		for (int i=0 ; i<this.l_VG.ElencoGas.size() ; i++) {
			this.l_VG.ElencoGas.get(i).percentuale = this.elencoPesi.get(i).getDoubleValue();
		}
	}
	
	public double getDensitaCalcolata() {
                l_VG.SelezioneDati.tipoFluido= descrizioneGas.toString();
		return this.densitaGasLocale;
	}
	
    private void calcolaDensita() {
        double percentualeAria = 100.;
        for (int i=1 ; i<this.elencoPesi.size() ; i++) {
            if (this.elencoPesi.get(i).getDoubleValue() > 100) {
                this.elencoPesi.get(i).setValue(100.0);
            }
            if (this.elencoPesi.get(i).getDoubleValue() < 0) {
                this.elencoPesi.get(i).setValue(0.0);
            }
            percentualeAria -= this.elencoPesi.get(i).getDoubleValue();
        }
        if (percentualeAria >= 0.) {
            this.elencoPesi.get(0).setValue(percentualeAria);
            this.densitaGasLocale = 0.;
            descrizioneGas.setLength(0);
            for (int i=0 ; i<this.elencoPesi.size() ; i++) {
                this.densitaGasLocale += this.l_VG.ElencoGas.get(i).densitaRelativa * this.elencoPesi.get(i).getDoubleValue();
                if (this.elencoPesi.get(i).getDoubleValue()>0) {
                    descrizioneGas.append(l_VG.utilityTraduzioni.TraduciStringa(l_VG.ElencoGas.get(i).descrizione)).append(": ").append(this.elencoPesi.get(i).getDoubleValue()).append("%");
                    descrizioneGas.append(System.lineSeparator());
                }
            }
            this.densitaGasLocale = this.l_VG.utilityFisica.densitaAria20C0m * this.densitaGasLocale / 100.;
            this.l_VG.fmtNd.setMaximumFractionDigits(3);
            this.labelDesnsitaFluidoVal.setValue("<html><b>" + this.l_VG.fmtNd.format(this.densitaGasLocale) + "</html>");
        } else {
            this.labelDesnsitaFluidoVal.setValue("");
        }
    }
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonReset_buttonClick(final Button.ClickEvent event) {
		this.tableGas.setEnabled(false);
		for (int i=0 ; i<this.elencoPesi.size() ; i++) {
			if (i == 0) {
				this.elencoPesi.get(i).setValue(100.0);
			} else {
				this.elencoPesi.get(i).setValue(0.0);
			}
		}
		this.tableGas.setEnabled(true);
		calcolaDensita();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.gridLayout = new XdevGridLayout();
		this.labelDensitaFluido = new XdevLabel();
		this.labelDesnsitaFluidoVal = new XdevLabel();
		this.label4 = new XdevLabel();
		this.labelPressione = new XdevLabel();
		this.buttonReset = new XdevButton();
		this.tableGas = new XdevTable<>();
	
		this.gridLayout.setMargin(new MarginInfo(false));
		this.labelDensitaFluido.setValue("densità");
		this.labelDensitaFluido.setContentMode(ContentMode.HTML);
		this.labelDesnsitaFluidoVal.setValue("val");
		this.labelDesnsitaFluidoVal.setContentMode(ContentMode.HTML);
		this.label4.setValue("[kg/m<sup>3]</sup>");
		this.label4.setContentMode(ContentMode.HTML);
		this.labelPressione.setValue("pressione");
		this.labelPressione.setContentMode(ContentMode.HTML);
		this.buttonReset.setCaption("Reset");
	
		this.gridLayout.setColumns(3);
		this.gridLayout.setRows(3);
		this.labelDensitaFluido.setSizeUndefined();
		this.gridLayout.addComponent(this.labelDensitaFluido, 0, 0);
		this.gridLayout.setComponentAlignment(this.labelDensitaFluido, Alignment.TOP_RIGHT);
		this.labelDesnsitaFluidoVal.setWidth(100, Unit.PERCENTAGE);
		this.labelDesnsitaFluidoVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelDesnsitaFluidoVal, 1, 0);
		this.label4.setWidth(100, Unit.PERCENTAGE);
		this.label4.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label4, 2, 0);
		this.labelPressione.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPressione, 0, 1);
		this.gridLayout.setComponentAlignment(this.labelPressione, Alignment.TOP_RIGHT);
		this.buttonReset.setSizeUndefined();
		this.gridLayout.addComponent(this.buttonReset, 2, 1);
		this.gridLayout.setColumnExpandRatio(0, 30.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 2, 2, 2);
		this.gridLayout.setRowExpandRatio(2, 1.0F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.gridLayout);
		this.tableGas.setSizeFull();
		this.verticalLayout.addComponent(this.tableGas);
		this.verticalLayout.setComponentAlignment(this.tableGas, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableGas, 100.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
                this.setSizeUndefined();
	
		this.buttonReset.addClickListener(event -> this.buttonReset_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelDensitaFluido, labelDesnsitaFluidoVal, label4, labelPressione;
	private XdevButton buttonReset;
	private XdevTable<?> tableGas;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>

}
