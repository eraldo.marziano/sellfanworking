package com.cit.sellfan.ui.pannelli;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;

import cit.sellfan.classi.titoli.StringheUNI;

public class pannelloPreferenzeMultigiri extends Panel {
	private VariabiliGlobali l_VG;
	private final TextFieldDouble etaQmin[] = new TextFieldDouble[4];
	private final TextFieldDouble etaQMax[] = new TextFieldDouble[4];
	private final TextFieldInteger rpm[] = new TextFieldInteger[16];
	private final TextFieldDouble potenza[] = new TextFieldDouble[16];
	/**
	 * 
	 */
	public pannelloPreferenzeMultigiri() {
		super();
		this.initUI();
		int index = 0;
		this.etaQmin[index++] = this.textFieldetaQmin0;
		this.etaQmin[index++] = this.textFieldetaQmin1;
		this.etaQmin[index++] = this.textFieldetaQmin2;
		this.etaQmin[index++] = this.textFieldetaQmin3;
		index = 0;
		this.etaQMax[index++] = this.textFieldetaQMax0;
		this.etaQMax[index++] = this.textFieldetaQMax1;
		this.etaQMax[index++] = this.textFieldetaQMax2;
		this.etaQMax[index++] = this.textFieldetaQMax3;
		this.textFieldetamax.setMaximumFractionDigits(2);
		this.textFieldetaQmin.setMaximumFractionDigits(2);
		this.textFieldetaQMax.setMaximumFractionDigits(2);
		for (int i=0 ; i<4 ; i++) {
			this.etaQmin[i].setMaximumFractionDigits(2);
			this.etaQMax[i].setMaximumFractionDigits(2);
		}
		index = 0;
		this.rpm[index++] = this.textFieldrpm0;
		this.rpm[index++] = this.textFieldrpm1;
		this.rpm[index++] = this.textFieldrpm2;
		this.rpm[index++] = this.textFieldrpm3;
		this.rpm[index++] = this.textFieldrpm4;
		this.rpm[index++] = this.textFieldrpm5;
		this.rpm[index++] = this.textFieldrpm6;
		this.rpm[index++] = this.textFieldrpm7;
		this.rpm[index++] = this.textFieldrpm8;
		this.rpm[index++] = this.textFieldrpm9;
		this.rpm[index++] = this.textFieldrpm10;
		this.rpm[index++] = this.textFieldrpm11;
		this.rpm[index++] = this.textFieldrpm12;
		this.rpm[index++] = this.textFieldrpm13;
		this.rpm[index++] = this.textFieldrpm14;
		this.rpm[index++] = this.textFieldrpm15;
		index = 0;
		this.potenza[index++] = this.textFieldPotenza0;
		this.potenza[index++] = this.textFieldPotenza1;
		this.potenza[index++] = this.textFieldPotenza2;
		this.potenza[index++] = this.textFieldPotenza3;
		this.potenza[index++] = this.textFieldPotenza4;
		this.potenza[index++] = this.textFieldPotenza5;
		this.potenza[index++] = this.textFieldPotenza6;
		this.potenza[index++] = this.textFieldPotenza7;
		this.potenza[index++] = this.textFieldPotenza8;
		this.potenza[index++] = this.textFieldPotenza9;
		this.potenza[index++] = this.textFieldPotenza10;
		this.potenza[index++] = this.textFieldPotenza11;
		this.potenza[index++] = this.textFieldPotenza12;
		this.potenza[index++] = this.textFieldPotenza13;
		this.potenza[index++] = this.textFieldPotenza14;
		this.potenza[index++] = this.textFieldPotenza15;
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		this.gridLayoutEta.setCaption(StringheUNI.RENDIMENTO);
		this.gridLayoutRpm.setCaption(StringheUNI.RPM);
		this.labeletaMax.setValue(StringheUNI.RENDIMENTO + " Max");
		this.labeleqmin.setValue(StringheUNI.RENDIMENTO + " Q<sub>min</sub>");
		this.labeleqmax.setValue(StringheUNI.RENDIMENTO + " Q<sub>Max</sub>");
	}

	public void Nazionalizza() {
		
	}

	public void init() {
		this.gridLayoutPotenza.setCaption(StringheUNI.POTENZAASSE + " " + this.l_VG.VentilatoreCurrent.UMPotenza.simbolo);
		this.textFieldetamax.setValue(this.l_VG.multigiri.multigiriRendimento.get(0));
		this.textFieldetaQmin.setValue(this.l_VG.multigiri.multigiriRendimento.get(1));
		this.textFieldetaQMax.setValue(this.l_VG.multigiri.multigiriRendimento.get(2));
		int index = 0;
		for (int i=3 ; i<this.l_VG.multigiri.multigiriRendimento.size() ; i++) {
			if (this.l_VG.multigiri.multigiriRendimentoPortataMin.get(i)) {
				this.etaQmin[index++].setValue(this.l_VG.multigiri.multigiriRendimento.get(i));
			}
		}
		for ( ; index<4 ; index++) {
			this.etaQmin[index].setValue("");
		}
		index = 0;
		for (int i=3 ; i<this.l_VG.multigiri.multigiriRendimento.size() ; i++) {
			if (!this.l_VG.multigiri.multigiriRendimentoPortataMin.get(i)) {
				this.etaQMax[index++].setValue(this.l_VG.multigiri.multigiriRendimento.get(i));
			}
		}
		for ( ; index<4 ; index++) {
			this.etaQMax[index].setValue("");
		}
		for (index=0 ; index<Math.min(this.l_VG.multigiri.multigiriRpmCurve.size(), 16) ; index++) {
            if (this.l_VG.multigiri.multigiriRpmCurve.get(index) <= 0.) {
				break;
			}
            this.rpm[index].setValue(this.l_VG.multigiri.multigiriRpmCurve.get(index));
		}
        for ( ; index<16 ; index++) {
            this.rpm[index].setValue("");
        }
		for (int i=0 ; i<16 ; i++) {
			this.potenza[i].setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPotenza.nDecimaliStampa);
		}
        for (index=0 ; index<Math.min(this.l_VG.multigiri.multigiriPotenzaW.size(), 16) ; index++) {
            if (this.l_VG.multigiri.multigiriPotenzaW.get(index) <= 0.) {
				break;
			}
            this.potenza[index].setValue(this.l_VG.utilityUnitaMisura.fromWToXX(this.l_VG.multigiri.multigiriPotenzaW.get(index), this.l_VG.VentilatoreCurrent.UMPotenza));
        }
        for ( ; index<16 ; index++) {
            this.potenza[index].setValue("");
        }
	}
	
	public void getValori() {
        final ArrayList<Double> valorieta = new ArrayList<>();
        final ArrayList<Boolean> frommin = new ArrayList<>();
        for (int i=0 ; i<3 ; i++) {
            valorieta.add(this.l_VG.multigiri.multigiriRendimento.get(i));
            frommin.add(this.l_VG.multigiri.multigiriRendimentoPortataMin.get(i));
        }
        
        final double etaMax = this.l_VG.multigiri.multigiriRendimento.get(0);
        double etaMin = this.l_VG.multigiri.multigiriRendimento.get(1);
        double eta;
        for (int i=0 ; i<4 ; i++) {
            eta = this.etaQmin[i].getDoubleValue();
            if (eta > 0. && eta <= etaMax && eta >= etaMin) {
                valorieta.add(eta);
                frommin.add(true);
            }
        }
        etaMin = this.l_VG.multigiri.multigiriRendimento.get(2);
        for (int i=0 ; i<4 ; i++) {
            eta = this.etaQMax[i].getDoubleValue();
            if (eta > 0. && eta <= etaMax && eta >= etaMin) {
                valorieta.add(eta);
                frommin.add(false);
            }
        }
        this.l_VG.multigiri.multigiriRendimento = valorieta;
        this.l_VG.multigiri.multigiriRendimentoPortataMin = frommin;
        final ArrayList<Double> valorirpm = new ArrayList<>();
        for (int i=0 ; i<16 ; i++) {
            if (this.rpm[i].getIntegerValue() > 0) {
                valorirpm.add((double)this.rpm[i].getIntegerValue());
            }
        }
        this.l_VG.multigiri.multigiriRpmCurve = valorirpm;
        final ArrayList<Double> valoriW = new ArrayList<>();
        for (int i=0 ; i<16 ; i++) {
            if (this.potenza[i].getDoubleValue() > 0.) {
                valoriW.add(this.l_VG.utilityUnitaMisura.fromXXToW(this.potenza[i].getDoubleValue(), this.l_VG.VentilatoreCurrent.UMPotenza));
            }
        }
        this.l_VG.multigiri.multigiriPotenzaW = valoriW;
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.gridLayoutEta = new XdevGridLayout();
		this.labeletaMax = new XdevLabel();
		this.textFieldetamax = new TextFieldDouble();
		this.label7 = new XdevLabel();
		this.labeleqmin = new XdevLabel();
		this.textFieldetaQmin = new TextFieldDouble();
		this.labeleqmax = new XdevLabel();
		this.textFieldetaQMax = new TextFieldDouble();
		this.textFieldetaQmin0 = new TextFieldDouble();
		this.textFieldetaQMax0 = new TextFieldDouble();
		this.textFieldetaQmin1 = new TextFieldDouble();
		this.textFieldetaQMax1 = new TextFieldDouble();
		this.textFieldetaQmin2 = new TextFieldDouble();
		this.textFieldetaQMax2 = new TextFieldDouble();
		this.textFieldetaQmin3 = new TextFieldDouble();
		this.textFieldetaQMax3 = new TextFieldDouble();
		this.gridLayoutRpm = new XdevGridLayout();
		this.textFieldrpm0 = new TextFieldInteger();
		this.textFieldrpm1 = new TextFieldInteger();
		this.textFieldrpm2 = new TextFieldInteger();
		this.textFieldrpm3 = new TextFieldInteger();
		this.textFieldrpm4 = new TextFieldInteger();
		this.textFieldrpm5 = new TextFieldInteger();
		this.textFieldrpm6 = new TextFieldInteger();
		this.textFieldrpm7 = new TextFieldInteger();
		this.textFieldrpm8 = new TextFieldInteger();
		this.textFieldrpm9 = new TextFieldInteger();
		this.textFieldrpm10 = new TextFieldInteger();
		this.textFieldrpm11 = new TextFieldInteger();
		this.textFieldrpm12 = new TextFieldInteger();
		this.textFieldrpm13 = new TextFieldInteger();
		this.textFieldrpm14 = new TextFieldInteger();
		this.textFieldrpm15 = new TextFieldInteger();
		this.gridLayoutPotenza = new XdevGridLayout();
		this.textFieldPotenza0 = new TextFieldDouble();
		this.textFieldPotenza1 = new TextFieldDouble();
		this.textFieldPotenza2 = new TextFieldDouble();
		this.textFieldPotenza3 = new TextFieldDouble();
		this.textFieldPotenza4 = new TextFieldDouble();
		this.textFieldPotenza5 = new TextFieldDouble();
		this.textFieldPotenza6 = new TextFieldDouble();
		this.textFieldPotenza7 = new TextFieldDouble();
		this.textFieldPotenza8 = new TextFieldDouble();
		this.textFieldPotenza9 = new TextFieldDouble();
		this.textFieldPotenza10 = new TextFieldDouble();
		this.textFieldPotenza11 = new TextFieldDouble();
		this.textFieldPotenza12 = new TextFieldDouble();
		this.textFieldPotenza13 = new TextFieldDouble();
		this.textFieldPotenza14 = new TextFieldDouble();
		this.textFieldPotenza15 = new TextFieldDouble();
	
		this.horizontalLayout.setCaptionAsHtml(true);
		this.gridLayoutEta.setCaption("eta");
		this.gridLayoutEta.setCaptionAsHtml(true);
		this.labeletaMax.setValue("emax");
		this.labeletaMax.setContentMode(ContentMode.HTML);
		this.textFieldetamax.setColumns(5);
		this.textFieldetamax.setEnabled(false);
		this.label7.setValue("%");
		this.labeleqmin.setValue("eqmin");
		this.labeleqmin.setContentMode(ContentMode.HTML);
		this.textFieldetaQmin.setColumns(5);
		this.textFieldetaQmin.setEnabled(false);
		this.labeleqmax.setValue("eqmax");
		this.labeleqmax.setContentMode(ContentMode.HTML);
		this.textFieldetaQMax.setColumns(5);
		this.textFieldetaQMax.setEnabled(false);
		this.textFieldetaQmin0.setColumns(5);
		this.textFieldetaQMax0.setColumns(5);
		this.textFieldetaQmin1.setColumns(5);
		this.textFieldetaQMax1.setColumns(5);
		this.textFieldetaQmin2.setColumns(5);
		this.textFieldetaQMax2.setColumns(5);
		this.textFieldetaQmin3.setColumns(5);
		this.textFieldetaQMax3.setColumns(5);
		this.gridLayoutRpm.setCaption("rpm");
		this.textFieldrpm0.setColumns(5);
		this.textFieldrpm1.setColumns(5);
		this.textFieldrpm2.setColumns(5);
		this.textFieldrpm3.setColumns(5);
		this.textFieldrpm4.setColumns(5);
		this.textFieldrpm5.setColumns(5);
		this.textFieldrpm6.setColumns(5);
		this.textFieldrpm7.setColumns(5);
		this.textFieldrpm8.setColumns(5);
		this.textFieldrpm9.setColumns(5);
		this.textFieldrpm10.setColumns(5);
		this.textFieldrpm11.setColumns(5);
		this.textFieldrpm12.setColumns(5);
		this.textFieldrpm13.setColumns(5);
		this.textFieldrpm14.setColumns(5);
		this.textFieldrpm15.setColumns(5);
		this.gridLayoutPotenza.setCaption("potenza");
		this.gridLayoutPotenza.setCaptionAsHtml(true);
		this.textFieldPotenza0.setColumns(5);
		this.textFieldPotenza1.setColumns(5);
		this.textFieldPotenza2.setColumns(5);
		this.textFieldPotenza3.setColumns(5);
		this.textFieldPotenza4.setColumns(5);
		this.textFieldPotenza5.setColumns(5);
		this.textFieldPotenza6.setColumns(5);
		this.textFieldPotenza7.setColumns(5);
		this.textFieldPotenza8.setColumns(5);
		this.textFieldPotenza9.setColumns(5);
		this.textFieldPotenza10.setColumns(5);
		this.textFieldPotenza11.setColumns(5);
		this.textFieldPotenza12.setColumns(5);
		this.textFieldPotenza13.setColumns(5);
		this.textFieldPotenza14.setColumns(5);
		this.textFieldPotenza15.setColumns(5);
	
		this.gridLayoutEta.setColumns(5);
		this.gridLayoutEta.setRows(7);
		this.labeletaMax.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.labeletaMax, 0, 0);
		this.textFieldetamax.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetamax, 1, 0);
		this.label7.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.label7, 2, 0);
		this.labeleqmin.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.labeleqmin, 0, 1);
		this.textFieldetaQmin.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQmin, 1, 1);
		this.labeleqmax.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.labeleqmax, 2, 1);
		this.textFieldetaQMax.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQMax, 3, 1);
		this.textFieldetaQmin0.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQmin0, 1, 2);
		this.textFieldetaQMax0.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQMax0, 3, 2);
		this.textFieldetaQmin1.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQmin1, 1, 3);
		this.textFieldetaQMax1.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQMax1, 3, 3);
		this.textFieldetaQmin2.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQmin2, 1, 4);
		this.textFieldetaQMax2.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQMax2, 3, 4);
		this.textFieldetaQmin3.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQmin3, 1, 5);
		this.textFieldetaQMax3.setSizeUndefined();
		this.gridLayoutEta.addComponent(this.textFieldetaQMax3, 3, 5);
		final CustomComponent gridLayoutEta_hSpacer = new CustomComponent();
		gridLayoutEta_hSpacer.setSizeFull();
		this.gridLayoutEta.addComponent(gridLayoutEta_hSpacer, 4, 0, 4, 5);
		this.gridLayoutEta.setColumnExpandRatio(4, 1.0F);
		final CustomComponent gridLayoutEta_vSpacer = new CustomComponent();
		gridLayoutEta_vSpacer.setSizeFull();
		this.gridLayoutEta.addComponent(gridLayoutEta_vSpacer, 0, 6, 3, 6);
		this.gridLayoutEta.setRowExpandRatio(6, 1.0F);
		this.gridLayoutRpm.setColumns(3);
		this.gridLayoutRpm.setRows(9);
		this.textFieldrpm0.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm0, 0, 0);
		this.textFieldrpm1.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm1, 1, 0);
		this.textFieldrpm2.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm2, 0, 1);
		this.textFieldrpm3.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm3, 1, 1);
		this.textFieldrpm4.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm4, 0, 2);
		this.textFieldrpm5.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm5, 1, 2);
		this.textFieldrpm6.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm6, 0, 3);
		this.textFieldrpm7.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm7, 1, 3);
		this.textFieldrpm8.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm8, 0, 4);
		this.textFieldrpm9.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm9, 1, 4);
		this.textFieldrpm10.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm10, 0, 5);
		this.textFieldrpm11.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm11, 1, 5);
		this.textFieldrpm12.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm12, 0, 6);
		this.textFieldrpm13.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm13, 1, 6);
		this.textFieldrpm14.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm14, 0, 7);
		this.textFieldrpm15.setSizeUndefined();
		this.gridLayoutRpm.addComponent(this.textFieldrpm15, 1, 7);
		final CustomComponent gridLayoutRpm_hSpacer = new CustomComponent();
		gridLayoutRpm_hSpacer.setSizeFull();
		this.gridLayoutRpm.addComponent(gridLayoutRpm_hSpacer, 2, 0, 2, 7);
		this.gridLayoutRpm.setColumnExpandRatio(2, 1.0F);
		final CustomComponent gridLayoutRpm_vSpacer = new CustomComponent();
		gridLayoutRpm_vSpacer.setSizeFull();
		this.gridLayoutRpm.addComponent(gridLayoutRpm_vSpacer, 0, 8, 1, 8);
		this.gridLayoutRpm.setRowExpandRatio(8, 1.0F);
		this.gridLayoutPotenza.setColumns(3);
		this.gridLayoutPotenza.setRows(9);
		this.textFieldPotenza0.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza0, 0, 0);
		this.textFieldPotenza1.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza1, 1, 0);
		this.textFieldPotenza2.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza2, 0, 1);
		this.textFieldPotenza3.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza3, 1, 1);
		this.textFieldPotenza4.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza4, 0, 2);
		this.textFieldPotenza5.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza5, 1, 2);
		this.textFieldPotenza6.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza6, 0, 3);
		this.textFieldPotenza7.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza7, 1, 3);
		this.textFieldPotenza8.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza8, 0, 4);
		this.textFieldPotenza9.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza9, 1, 4);
		this.textFieldPotenza10.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza10, 0, 5);
		this.textFieldPotenza11.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza11, 1, 5);
		this.textFieldPotenza12.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza12, 0, 6);
		this.textFieldPotenza13.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza13, 1, 6);
		this.textFieldPotenza14.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza14, 0, 7);
		this.textFieldPotenza15.setSizeUndefined();
		this.gridLayoutPotenza.addComponent(this.textFieldPotenza15, 1, 7);
		final CustomComponent gridLayoutPotenza_hSpacer = new CustomComponent();
		gridLayoutPotenza_hSpacer.setSizeFull();
		this.gridLayoutPotenza.addComponent(gridLayoutPotenza_hSpacer, 2, 0, 2, 7);
		this.gridLayoutPotenza.setColumnExpandRatio(2, 1.0F);
		final CustomComponent gridLayoutPotenza_vSpacer = new CustomComponent();
		gridLayoutPotenza_vSpacer.setSizeFull();
		this.gridLayoutPotenza.addComponent(gridLayoutPotenza_vSpacer, 0, 8, 1, 8);
		this.gridLayoutPotenza.setRowExpandRatio(8, 1.0F);
		this.gridLayoutEta.setSizeUndefined();
		this.horizontalLayout.addComponent(this.gridLayoutEta);
		this.horizontalLayout.setComponentAlignment(this.gridLayoutEta, Alignment.MIDDLE_CENTER);
		this.gridLayoutRpm.setSizeUndefined();
		this.horizontalLayout.addComponent(this.gridLayoutRpm);
		this.horizontalLayout.setComponentAlignment(this.gridLayoutRpm, Alignment.MIDDLE_CENTER);
		this.gridLayoutPotenza.setSizeUndefined();
		this.horizontalLayout.addComponent(this.gridLayoutPotenza);
		this.horizontalLayout.setComponentAlignment(this.gridLayoutPotenza, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setSizeUndefined();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labeletaMax, label7, labeleqmin, labeleqmax;
	private XdevHorizontalLayout horizontalLayout;
	private TextFieldDouble textFieldetamax, textFieldetaQmin, textFieldetaQMax, textFieldetaQmin0, textFieldetaQMax0,
			textFieldetaQmin1, textFieldetaQMax1, textFieldetaQmin2, textFieldetaQMax2, textFieldetaQmin3,
			textFieldetaQMax3, textFieldPotenza0, textFieldPotenza1, textFieldPotenza2, textFieldPotenza3,
			textFieldPotenza4, textFieldPotenza5, textFieldPotenza6, textFieldPotenza7, textFieldPotenza8,
			textFieldPotenza9, textFieldPotenza10, textFieldPotenza11, textFieldPotenza12, textFieldPotenza13,
			textFieldPotenza14, textFieldPotenza15;
	private XdevGridLayout gridLayoutEta, gridLayoutRpm, gridLayoutPotenza;
	private TextFieldInteger textFieldrpm0, textFieldrpm1, textFieldrpm2, textFieldrpm3, textFieldrpm4,
			textFieldrpm5, textFieldrpm6, textFieldrpm7, textFieldrpm8, textFieldrpm9,
			textFieldrpm10, textFieldrpm11, textFieldrpm12, textFieldrpm13, textFieldrpm14,
			textFieldrpm15;
	// </generated-code>

}
