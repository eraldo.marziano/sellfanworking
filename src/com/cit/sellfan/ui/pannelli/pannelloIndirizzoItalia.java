package com.cit.sellfan.ui.pannelli;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.classi.Clientev21;

public class pannelloIndirizzoItalia extends Panel {
	private VariabiliGlobali l_VG;
	private String currentRegione;
	private String currentProvincia;
	private String currentComune;

	/**
	 * 
	 */
	public pannelloIndirizzoItalia() {
		super();
		this.initUI();
		textFieldCAP.setEnabled(false);
	}

	public void setVariabiliGlobali(VariabiliGlobali variabiliGlobali) {
		l_VG = variabiliGlobali;
		initCombo();
	}
	
	private void initCombo() {
		comboBoxRegione.setEnabled(false);
		comboBoxProvincia.setEnabled(false);
		comboBoxComune.setEnabled(false);
		comboBoxRegione.removeAllItems();
		comboBoxProvincia.removeAllItems();
		comboBoxComune.removeAllItems();
        for (int i=0 ; i<l_VG.utilityIstat.elencoRegioni.size() ; i++) {
        	comboBoxRegione.addItem(l_VG.utilityIstat.elencoRegioni.get(i).denominazione);
        }
        comboBoxRegione.setEnabled(true);
	}
	
	public void Nazionalizza() {
		labelRegione.setValue(l_VG.utilityTraduzioni.TraduciStringa("Regione"));
		labelProvincia.setValue(l_VG.utilityTraduzioni.TraduciStringa("Provincia"));
		labelComune.setValue(l_VG.utilityTraduzioni.TraduciStringa("Comune"));
		labelLocalita.setValue(l_VG.utilityTraduzioni.TraduciStringa("Località"));
		labelIndirizzo.setValue(l_VG.utilityTraduzioni.TraduciStringa("Indirizzo"));
	}
	
	public void setClienteInUse(Clientev21 clienteInUse) {
		if (clienteInUse == null) {
			return;
		}
		enableFields(false);
		if (clienteInUse.indirizzo.regione.equals("")) {
			initCombo();
		} else {
			comboBoxRegione.setEnabled(true);
			comboBoxRegione.setValue(clienteInUse.indirizzo.regione);
			if (!clienteInUse.indirizzo.provincia.equals("")) {
				comboBoxProvincia.setEnabled(true);
				comboBoxProvincia.setValue(clienteInUse.indirizzo.provincia);
				if (!clienteInUse.indirizzo.comune.equals("")) {
					comboBoxComune.setEnabled(true);
					comboBoxComune.setValue(clienteInUse.indirizzo.comune);
				}
			}
		}
		textFieldLocalita.setValue(clienteInUse.indirizzo.localita);
		textFieldIndirizzo.setValue(clienteInUse.indirizzo.indirizzo);
		textFieldN.setValue(clienteInUse.indirizzo.numerocivico);
		enableFields(false);
	}
/*	
	public void setCurrentCliente() {
		if (l_VG.currentCliente == null) {
			return;
		}
		enableFields(false);
		//l_VG.MainView.setMemo("indirizzo italia [" + l_VG.currentCliente.indirizzo.regione + "]");
		if (l_VG.currentCliente.indirizzo.regione.equals("")) {
			initCombo();
		} else {
			comboBoxRegione.setEnabled(true);
			comboBoxRegione.setValue(l_VG.currentCliente.indirizzo.regione);
			if (!l_VG.currentCliente.indirizzo.provincia.equals("")) {
				comboBoxProvincia.setEnabled(true);
				comboBoxProvincia.setValue(l_VG.currentCliente.indirizzo.provincia);
				if (!l_VG.currentCliente.indirizzo.comune.equals("")) {
					comboBoxComune.setEnabled(true);
					comboBoxComune.setValue(l_VG.currentCliente.indirizzo.comune);
				}
			}
		}
		textFieldLocalita.setValue(l_VG.currentCliente.indirizzo.localita);
		textFieldIndirizzo.setValue(l_VG.currentCliente.indirizzo.indirizzo);
		textFieldN.setValue(l_VG.currentCliente.indirizzo.numerocivico);
		enableFields(false);
	}
*/	
	public void enableFields(boolean editEnabled) {
		comboBoxRegione.setEnabled(editEnabled);
		comboBoxProvincia.setEnabled(editEnabled);
		comboBoxComune.setEnabled(editEnabled);
		textFieldLocalita.setEnabled(editEnabled);
		textFieldIndirizzo.setEnabled(editEnabled);
		textFieldN.setEnabled(editEnabled);
	}
/*
	public void getCurrentCliente() {
		if (l_VG.currentCliente == null) {
			return;
		}
		l_VG.currentCliente.indirizzo.nazione = "Italia";
		try {
			l_VG.currentCliente.indirizzo.regione = comboBoxRegione.getValue().toString();
		} catch (Exception e) {
			l_VG.currentCliente.indirizzo.regione = "";
		}
		try {
			l_VG.currentCliente.indirizzo.provincia = comboBoxProvincia.getValue().toString();
		} catch (Exception e) {
			l_VG.currentCliente.indirizzo.provincia = "";
		}
		try {
			l_VG.currentCliente.indirizzo.comune = comboBoxComune.getValue().toString();
		} catch (Exception e) {
			l_VG.currentCliente.indirizzo.comune = "";
		}
		l_VG.currentCliente.indirizzo.localita = textFieldLocalita.getValue();
		l_VG.currentCliente.indirizzo.indirizzo = textFieldIndirizzo.getValue();
		l_VG.currentCliente.indirizzo.cap = textFieldCAP.getValue();
		l_VG.currentCliente.indirizzo.numerocivico = textFieldN.getValue();
	}
*/	
	public void getClienteInUse(Clientev21 clienteInUse) {
		if (clienteInUse == null) {
			return;
		}
		clienteInUse.indirizzo.nazione = "Italia";
		try {
			clienteInUse.indirizzo.regione = comboBoxRegione.getValue().toString();
		} catch (Exception e) {
			clienteInUse.indirizzo.regione = "";
		}
		try {
			clienteInUse.indirizzo.provincia = comboBoxProvincia.getValue().toString();
		} catch (Exception e) {
			clienteInUse.indirizzo.provincia = "";
		}
		try {
			clienteInUse.indirizzo.comune = comboBoxComune.getValue().toString();
		} catch (Exception e) {
			clienteInUse.indirizzo.comune = "";
		}
		clienteInUse.indirizzo.localita = textFieldLocalita.getValue();
		clienteInUse.indirizzo.indirizzo = textFieldIndirizzo.getValue();
		clienteInUse.indirizzo.cap = textFieldCAP.getValue();
		clienteInUse.indirizzo.numerocivico = textFieldN.getValue();
	}
	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxRegione}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxRegione_valueChange(Property.ValueChangeEvent event) {
		if (!comboBoxRegione.isEnabled()) return;
		comboBoxProvincia.setEnabled(false);
		comboBoxComune.setEnabled(false);
		textFieldCAP.setValue("");
		comboBoxProvincia.removeAllItems();
		comboBoxComune.removeAllItems();
		currentRegione = "";
		if (comboBoxRegione.getValue() == null) return;
		currentRegione = comboBoxRegione.getValue().toString();
		l_VG.utilityIstat.caricaElencoProvince(l_VG.utilityIstat.getRegionefromDenominazione(currentRegione));
        for (int i=0 ; i<l_VG.utilityIstat.elencoProvince.size() ; i++) {
            comboBoxProvincia.addItem(l_VG.utilityIstat.elencoProvince.get(i).denominazione);
        }
        comboBoxProvincia.setEnabled(true);
	}
	
	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxProvincia}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxProvincia_valueChange(Property.ValueChangeEvent event) {
		if (!comboBoxProvincia.isEnabled()) return;
		comboBoxComune.setEnabled(false);
		textFieldCAP.setValue("");
		comboBoxComune.removeAllItems();
		currentProvincia = "";
		if (comboBoxProvincia.getValue() == null) return;
		currentProvincia = comboBoxProvincia.getValue().toString();
        l_VG.utilityIstat.caricaElencoComuni(l_VG.utilityIstat.getRegionefromDenominazione(currentRegione), l_VG.utilityIstat.getProvinciafromDenominazione(currentProvincia));
        for (int i=0 ; i<l_VG.utilityIstat.elencoComuni.size() ; i++) {
            comboBoxComune.addItem(l_VG.utilityIstat.elencoComuni.get(i).denominazione);
        }
        comboBoxComune.setEnabled(true);
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxComune}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxComune_valueChange(Property.ValueChangeEvent event) {
		if (!comboBoxComune.isEnabled()) return;
		currentComune = "";
		if (comboBoxComune.getValue() == null) return;
		currentComune = comboBoxComune.getValue().toString();
		textFieldCAP.setValue(l_VG.utilityIstat.getCAPfromDenominazione(currentComune));
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.gridLayout = new XdevGridLayout();
		this.labelRegione = new XdevLabel();
		this.comboBoxRegione = new XdevComboBox<>();
		this.labelProvincia = new XdevLabel();
		this.comboBoxProvincia = new XdevComboBox<>();
		this.labelComune = new XdevLabel();
		this.comboBoxComune = new XdevComboBox<>();
		this.labelCAP = new XdevLabel();
		this.textFieldCAP = new XdevTextField();
		this.labelLocalita = new XdevLabel();
		this.textFieldLocalita = new XdevTextField();
		this.labelIndirizzo = new XdevLabel();
		this.textFieldIndirizzo = new XdevTextField();
		this.labelN = new XdevLabel();
		this.textFieldN = new XdevTextField();
	
		this.gridLayout.setMargin(new MarginInfo(false));
		this.labelRegione.setValue("Regione");
		this.labelProvincia.setValue("Provincia");
		this.labelComune.setValue("Comune");
		this.labelCAP.setValue("CAP");
		this.labelLocalita.setValue("Localita");
		this.labelIndirizzo.setValue("Indirizzo");
		this.labelN.setValue("N");
	
		this.gridLayout.setColumns(4);
		this.gridLayout.setRows(6);
		this.labelRegione.setSizeUndefined();
		this.gridLayout.addComponent(this.labelRegione, 0, 0);
		this.gridLayout.setComponentAlignment(this.labelRegione, Alignment.TOP_RIGHT);
		this.comboBoxRegione.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxRegione.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxRegione, 1, 0);
		this.labelProvincia.setSizeUndefined();
		this.gridLayout.addComponent(this.labelProvincia, 0, 1);
		this.gridLayout.setComponentAlignment(this.labelProvincia, Alignment.TOP_RIGHT);
		this.comboBoxProvincia.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxProvincia.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxProvincia, 1, 1);
		this.labelComune.setSizeUndefined();
		this.gridLayout.addComponent(this.labelComune, 0, 2);
		this.gridLayout.setComponentAlignment(this.labelComune, Alignment.TOP_RIGHT);
		this.comboBoxComune.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxComune.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxComune, 1, 2);
		this.labelCAP.setSizeUndefined();
		this.gridLayout.addComponent(this.labelCAP, 2, 2);
		this.gridLayout.setComponentAlignment(this.labelCAP, Alignment.TOP_RIGHT);
		this.textFieldCAP.setWidth(100, Unit.PERCENTAGE);
		this.textFieldCAP.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldCAP, 3, 2);
		this.labelLocalita.setSizeUndefined();
		this.gridLayout.addComponent(this.labelLocalita, 0, 3);
		this.gridLayout.setComponentAlignment(this.labelLocalita, Alignment.TOP_RIGHT);
		this.textFieldLocalita.setWidth(100, Unit.PERCENTAGE);
		this.textFieldLocalita.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldLocalita, 1, 3, 3, 3);
		this.labelIndirizzo.setSizeUndefined();
		this.gridLayout.addComponent(this.labelIndirizzo, 0, 4);
		this.gridLayout.setComponentAlignment(this.labelIndirizzo, Alignment.TOP_RIGHT);
		this.textFieldIndirizzo.setWidth(100, Unit.PERCENTAGE);
		this.textFieldIndirizzo.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldIndirizzo, 1, 4);
		this.labelN.setSizeUndefined();
		this.gridLayout.addComponent(this.labelN, 2, 4);
		this.gridLayout.setComponentAlignment(this.labelN, Alignment.TOP_RIGHT);
		this.textFieldN.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldN, 3, 4);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setColumnExpandRatio(3, 2.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 5, 3, 5);
		this.gridLayout.setRowExpandRatio(5, 1.0F);
		this.gridLayout.setSizeFull();
		this.setContent(this.gridLayout);
		this.setSizeFull();
	
		this.comboBoxRegione.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloIndirizzoItalia.this.comboBoxRegione_valueChange(event);
			}
		});
		this.comboBoxProvincia.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloIndirizzoItalia.this.comboBoxProvincia_valueChange(event);
			}
		});
		this.comboBoxComune.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloIndirizzoItalia.this.comboBoxComune_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelRegione, labelProvincia, labelComune, labelCAP, labelLocalita, labelIndirizzo, labelN;
	private XdevTextField textFieldCAP, textFieldLocalita, textFieldIndirizzo, textFieldN;
	private XdevComboBox<CustomComponent> comboBoxRegione, comboBoxProvincia, comboBoxComune;
	private XdevGridLayout gridLayout; // </generated-code>


}
