package com.cit.sellfan.ui.pannelli;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.SelezioneCorrente;
import cit.sellfan.classi.Silenziatore;

public class pannelloSelezioneSilenziatore extends Panel {
	private final VariabiliGlobali l_VG;
	private final Container containerSilenziatori = new IndexedContainer();
	private ArrayList<Silenziatore> listaInterna;
	private final ArrayList<Coppia> listaSelezione = new ArrayList<>();
	ValueChangeListener vc = new ValueChangeListener() {
		@Override
		public void valueChange(final ValueChangeEvent event) {
		clean((CheckBox)event.getProperty());
	}
	};
	private boolean Mandata;
	
	public void Nazionalizza(final boolean isMandata) {
		if (isMandata) {
			this.labelTitolo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore in Mandata"));
			//this.checkBoxSostegno.setVisible(false);
		} else {
			this.labelTitolo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore in Aspirazione"));
			//this.checkBoxSostegno.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungere supporto anteriore"));
		}
		}
	
	//elenco dei silenziatori

	public pannelloSelezioneSilenziatore(final VariabiliGlobali vg) {
		super();
		this.initUI();
		this.l_VG = vg;
		this.checkBoxSostegno.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSostegno);
		this.containerSilenziatori.removeAllItems();
		this.containerSilenziatori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerSilenziatori.addContainerProperty("Code", String.class, "");
		this.containerSilenziatori.addContainerProperty("Description", String.class, "");
		this.containerSilenziatori.addContainerProperty("Speed", String.class, 0.0);
		this.tableSilenziatori.setContainerDataSource(this.containerSilenziatori);
	}
	
//	public void loadTabellaSilenziatoriAspirazione(final ArrayList<Silenziatore> lista) {
//		this.listaInterna = lista;
//		this.tableSilenziatori.removeAllItems();
//		for (final Silenziatore sil : lista) {
//			final Coppia pair = new Coppia();
//			final CheckBox box = new CheckBox();
//			final Object silTab[] = new Object[4];
//			box.setImmediate(true);
//			box.setValue(false);
//			box.setData(sil.codiceSilenziatore);
//			if ((this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreAspirazione != null) && (sil.codiceSilenziatore.equals(this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreAspirazione))) {
//				box.setValue(true);
//				pair.activated = true;
//			}
//			box.addValueChangeListener(this.vc);
//			final DecimalFormat dec = new DecimalFormat("#0.0");
//
//
//		}
//	}
//
	
	
	public void loadTabellaSilenziatori(final ArrayList<Silenziatore> lista, final boolean isMandata) {
		this.listaInterna = lista;
		this.tableSilenziatori.removeAllItems();
		final SelezioneCorrente selCor = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		for(final Silenziatore silenziatore : lista) {
			final Coppia pair = new Coppia();
			final CheckBox chk = new CheckBox();
			final Object silTab[] = new Object[4];
			chk.setImmediate(true);
			chk.setValue(false);
			chk.setData(silenziatore.codiceSilenziatore);
			if (isMandata && selCor.silMandata != null && selCor.silMandata.codiceSilenziatore.equals(silenziatore.codiceSilenziatore)) {
				chk.setValue(true);
				pair.activated = true;
			}
			if (!isMandata && selCor.silAspirazione != null && selCor.silAspirazione.codiceSilenziatore.equals(silenziatore.codiceSilenziatore)) {
				chk.setValue(true);
				pair.activated = true;
			}
			chk.addValueChangeListener(this.vc);
			final DecimalFormat dec = new DecimalFormat("#0.0");
			silTab[0] = chk;
			silTab[1] = silenziatore.codiceSilenziatore;
			pair.Codice = silenziatore.codiceSilenziatore;
			silTab[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Cilindrico ") + silenziatore.descrizioneSilenziatore;
			silTab[3] = dec.format(this.l_VG.getVeloc(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, silenziatore.diametroCircolare));
			this.tableSilenziatori.addItem(silTab, null);
			this.listaSelezione.add(pair);
		}
	}

	public void clean(final CheckBox ticked) {
		
		final String codice = (String) ticked.getData();
		if (ticked.getValue() == false) {
			seleziona("");
		} else {
		seleziona(codice);
		}
		for (final Object line: this.tableSilenziatori.getItemIds()) {
			
				final int searched = cercaPerCodice(this.tableSilenziatori.getContainerProperty(line, "Code").getValue().toString());
			if (ticked.getValue() == true) {
				if (searched != -1) {
					boolean found;
					if (searched == 0) {
						found = false;
					} else {
						found = true;
					}
					((CheckBox)this.tableSilenziatori.getContainerProperty(line, "Selected").getValue()).removeValueChangeListener(this.vc);
					((CheckBox)this.tableSilenziatori.getContainerProperty(line, "Selected").getValue()).setValue(found);
					((CheckBox)this.tableSilenziatori.getContainerProperty(line, "Selected").getValue()).addValueChangeListener(this.vc);
				}
			} else {
				if (searched != -1) {
				((CheckBox)this.tableSilenziatori.getContainerProperty(line, "Selected").getValue()).removeValueChangeListener(this.vc);
				((CheckBox)this.tableSilenziatori.getContainerProperty(line, "Selected").getValue()).setValue(false);
				((CheckBox)this.tableSilenziatori.getContainerProperty(line, "Selected").getValue()).addValueChangeListener(this.vc);
				}
			}
		}
	}
	public boolean selectInlet() {
		final String codiceSelezionato = cercaAttivo();
		if (codiceSelezionato == null) {
			this.l_VG.VentilatoreCurrent.selezioneCorrente.silAspirazione = null;
		} else {
			for (final Silenziatore s : this.listaInterna) {
				if (s.codiceSilenziatore.equals(codiceSelezionato)) {
					this.l_VG.VentilatoreCurrent.selezioneCorrente.silAspirazione = s;
					this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSilAspirazione = true;
					this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleTramoggiaAspirazione = true;
					this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSostegno = false;
					this.l_VG.utilityCliente.loadAccessoriForSilenziatore(s);
				}
			}
		}
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
		return (this.l_VG.VentilatoreCurrent.selezioneCorrente.silAspirazione != null);
	}
	
	public boolean selectOutlet() {
		final String codiceSelezionato = cercaAttivo();
		if (codiceSelezionato == null) {
			this.l_VG.VentilatoreCurrent.selezioneCorrente.silMandata = null;
		} else {
			for (final Silenziatore s : this.listaInterna) {
				if (s.codiceSilenziatore.equals(codiceSelezionato)) {
					this.l_VG.VentilatoreCurrent.selezioneCorrente.silMandata = s;
					this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSilMandata = true;
					this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleTramoggiaMandata = true;
					this.l_VG.utilityCliente.loadAccessoriForSilenziatore(s);
				}
			}
		}
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
		return (this.l_VG.VentilatoreCurrent.selezioneCorrente.silMandata != null);
	}

	public boolean returnSelectedFromFile(final boolean isMandata) {
		String search = "";
		Silenziatore ret = null;
		search = cercaAttivo();
		if (search == null) {
			if (isMandata) {
				this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreMandata = null;
			} else {
				this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreAspirazione = null;
		}}
		else {
		for (final Silenziatore s : this.listaInterna) {
			if (s.codiceSilenziatore.equals(search)) {
				ret = s;
			}
		}
		final SelezioneCorrente selCorr = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		if (ret.direzione.equals("Mandata")) {
			selCorr.silMandata = ret;
			selCorr.silMandata.tramoggiaConnessa.descrizione = " DN= " + ret.tramoggiaConnessa.diametroCircolare + " L= " + ret.tramoggiaConnessa.lunghezza;
		} else {
			selCorr.silAspirazione = ret;
			selCorr.silAspirazione.tramoggiaConnessa.descrizione = " DN= " + ret.tramoggiaConnessa.diametroCircolare + " L= " + ret.tramoggiaConnessa.lunghezza;
		}
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
		}
		return this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSostegno;
		//this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
	}
	public boolean returnSelected(final boolean isMandata) {
		String search = "";
		Silenziatore ret = null;
		search = cercaAttivo();
		if (search == null) {
			if (isMandata) {
				this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreMandata = null;
			} else {
				this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreAspirazione = null;
		}}
		else {
		for (final Silenziatore s : this.listaInterna) {
			if (s.codiceSilenziatore.equals(search)) {
				ret = s;
			}
		}
		final SelezioneCorrente selCorr = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		if (ret.direzione.equals("Mandata")) {
			selCorr.silenziatoreMandata = ret.codiceSilenziatore;
			selCorr.prezzoSilenziatoreMandata = ret.prezzo;
			selCorr.prezzoTramoggiaMandata = ret.tramoggiaConnessa.prezzo;
			selCorr.descSilMandata = ret.descrizioneSilenziatore;
			selCorr.lunghTramMandata = ret.tramoggiaConnessa.lunghezza;
			selCorr.descTramMandata = " DN= " + ret.tramoggiaConnessa.diametroCircolare + " L= " + ret.tramoggiaConnessa.lunghezza;
			
		} else {
			selCorr.silenziatoreAspirazione = ret.codiceSilenziatore;
			selCorr.prezzoSilenziatoreAspirazione = ret.prezzo;
			selCorr.prezzoTramoggiaAspirazione = ret.tramoggiaConnessa.prezzo;
			selCorr.descSilAspirazione = ret.descrizioneSilenziatore;
			selCorr.lunghTramAspirazione = ret.tramoggiaConnessa.lunghezza;
			selCorr.prezzoSostegno = ret.sostegnoSilenziatore.prezzoSostegno;
			selCorr.descSostegno = ret.sostegnoSilenziatore.descrizioneSostegno;
			selCorr.codiceSostegno = ret.sostegnoSilenziatore.codiceSostegno;
			selCorr.descTramAspirazione = " DN= " + ret.tramoggiaConnessa.diametroCircolare + " L= " + ret.tramoggiaConnessa.lunghezza;
			
		}
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
		}
		return this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSostegno;
		//this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
	}
	
 	private String cercaAttivo() {
 		for (final Coppia c : this.listaSelezione) {
 			if (c.activated) {
				return c.Codice;
			}
 		}
		return null;
	}
 	
 	private void seleziona(final String codice) {
 		for (final Coppia c : this.listaSelezione) {
 			if (c.Codice.equals(codice)) {
				c.activated = true;
			} else {
				c.activated = false;
			}
 		}
 	}
 	
 	private int cercaPerCodice(final String codice) {
 		for (final Coppia c : this.listaSelezione) {
 			if (c.Codice.equals(codice)) {
 				if (c.activated) {
					return 1;
				} else {
					return 0;
				}
				}
 		}
 		return -1;
 	}

	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelTitolo = new XdevLabel();
		this.labelSostegno = new XdevLabel();
		this.tableSilenziatori = new XdevTable<>();
		this.checkBoxSostegno = new XdevCheckBox();
		this.labelTitolo.setContentMode(ContentMode.HTML);
		this.tableSilenziatori.setStyleName("small striped");
		this.checkBoxSostegno.setCaption("Aggiungere supporto anteriore");

		
		this.verticalLayout.addComponent(this.labelTitolo);
		this.verticalLayout.setComponentAlignment(this.labelTitolo, Alignment.MIDDLE_CENTER);
		this.tableSilenziatori.setSizeFull();
		this.verticalLayout.addComponent(this.tableSilenziatori);
		//this.verticalLayout.addComponent(this.checkBoxSostegno);
		this.verticalLayout.setComponentAlignment(this.tableSilenziatori, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableSilenziatori, 100.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setWidth(80, Unit.PERCENTAGE);
		this.setHeight(80, Unit.PERCENTAGE);
		}

	private XdevLabel labelTitolo, labelSostegno;
	private XdevTable<CustomComponent> tableSilenziatori;
	private XdevCheckBox checkBoxSostegno;
	private XdevVerticalLayout verticalLayout;
}

class Coppia {
	public String Codice;
	public boolean activated;
	
	
}
