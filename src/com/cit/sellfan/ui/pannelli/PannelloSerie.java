/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.pannelli;


import cit.sellfan.classi.GruppoSelezioneSerie;
import cit.sellfan.classi.Serie;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.utility.UtilityTraduzioni;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.utility.oggettoInfoRicerca;
import com.cit.sellfan.ui.utility.oggettoSelect;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Dictionary;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;

/**
 *
 * @author Eraldo Marziano
 */
public class PannelloSerie extends Panel{
    private VariabiliGlobali vGlobali;
    private boolean isMistral = false;
    private Dictionary<String, ArrayList<String>> elencoSerie; 
    boolean modificabile;
    private XdevHorizontalLayout LayoutPrincipale, LayoutBottoni;
    private XdevVerticalLayout LayoutSelezione, LayoutFiltriSelezione, LayoutTabella;
    private OptionGroup FS_AssialiCentrifughiEsplicita, FS_AccoppiatiTrasmissione, FS_TipoFluido;
    private TextFieldDouble DV_DiametroGirante;
    private TextFieldInteger DV_CorrettorePotenza, DV_GiriMaxGirante;
    private XdevLabel TL_NumeroSerieSelezionate, DV_CorrettorePotenzaLabel, DV_CorrettorePotenzaLabelUM, DV_DiametroGiranteLabel, DV_DiametroGiranteLabelUM, DV_GiriMaxGiranteLabel, DV_GiriMaxGiranteLabelUM;
    private XdevGridLayout GridDV;
    private Panel PVentilatore, PFluido, PPredefinite;
    private pannelloSelezioneSerieDefault tabella;
    private oggettoSelect FS_ACE_Assiali, FS_ACE_Centrifughi, FS_ACE_Esplicita, FS_AT_Accoppiati,
            FS_AT_Trasmissione, FS_TF_Esplicita, FS_TF_AriaPulitaMedioBassaPressione, FS_TF_AriaPulitaAltaPressione,
            FS_TF_AriaPocoPolverosa, FS_TF_AriaMoltoPolverosa, FS_TF_MaterialeSolido;
    public boolean setTrasmissione, setAssiali;
    private final Container containerSerie = new IndexedContainer();
    private XdevTable<CustomComponent> tableSerie;
    private Button TL_Tutti, TL_Nessuno;
    private XdevComboBox<CustomComponent> SeriePredefinite;

    public PannelloSerie() {
        super();
        
        this.elencoSerie = new Hashtable<>();
        initUI();
        containerSerie.removeAllItems();		
        containerSerie.addContainerProperty("Selected", XdevCheckBox.class, true);
        containerSerie.addContainerProperty("Serie", String.class, null);
        containerSerie.addContainerProperty("QMin", Double.class, null);
        containerSerie.addContainerProperty("QMax", Double.class, null);
        containerSerie.addContainerProperty("PTMin", Double.class, null);
        containerSerie.addContainerProperty("PTMax", Double.class, null);
        containerSerie.addContainerProperty("Description", String.class, null);
        tableSerie.setContainerDataSource(containerSerie);
        
    }
    
    private boolean getOptionGroupValue (OptionGroup og, String item) {
        return ((oggettoSelect) og.getValue()).theSame(item);
    }
    
    public void setVariabiliGlobali(VariabiliGlobali vg) {
        vGlobali = vg;
        DV_DiametroGiranteLabelUM.setValue(vGlobali.UMDiametroCorrente.simbolo.replace("[","").replace("]",""));
        if (vGlobali.utilityCliente.idSottoClienteIndex ==0) isMistral = true;
        if (isMistral) {
            LayoutTabella.setVisible(false);
            LayoutBottoni.setVisible(false);
            PFluido.setVisible(false);
            PPredefinite.setVisible(false);
        }
        Nazionalizza();
    }
    
    public void Nazionalizza() {
        UtilityTraduzioni traduci = vGlobali.utilityTraduzioni;
        PVentilatore.setCaption(traduci.TraduciStringa("Caratteristiche del Ventilatore"));
        PFluido.setCaption(traduci.TraduciStringa("Caratteristiche del Fluido"));
        PPredefinite.setCaption(traduci.TraduciStringa("Gruppi Serie Predefinite"));
        tableSerie.setCaption(traduci.TraduciStringa("Selezionare le serie di ventilatori che si desidera includere"));
        FS_AssialiCentrifughiEsplicita.setCaption(traduci.TraduciStringa("Seleziona Tipo di Ventilatore"));
        FS_ACE_Assiali.valore = traduci.TraduciStringa(FS_ACE_Assiali.nome);
        FS_ACE_Centrifughi.valore = traduci.TraduciStringa(FS_ACE_Centrifughi.nome);
        FS_ACE_Esplicita.valore = traduci.TraduciStringa(FS_ACE_Esplicita.nome);
        FS_AccoppiatiTrasmissione.setCaption(traduci.TraduciStringa("Seleziona Tipo di Trasmissione"));
        FS_AT_Accoppiati.valore = traduci.TraduciStringa(FS_AT_Accoppiati.nome);
        FS_AT_Trasmissione.valore = traduci.TraduciStringa(FS_AT_Trasmissione.nome);
        DV_CorrettorePotenza.setCaption(traduci.TraduciStringa("Correttore Potenza Motore"));
        DV_DiametroGirante.setCaption(traduci.TraduciStringa("Diametro massimo girante"));
        DV_GiriMaxGirante.setCaption(traduci.TraduciStringa("Giri massimi girante"));
        FS_TF_AriaMoltoPolverosa.valore = traduci.TraduciStringa(FS_TF_AriaMoltoPolverosa.nome);
        FS_TF_AriaPocoPolverosa.valore = traduci.TraduciStringa(FS_TF_AriaPocoPolverosa.nome);
        FS_TF_AriaPulitaAltaPressione.valore = traduci.TraduciStringa(FS_TF_AriaPulitaAltaPressione.nome);
        FS_TF_AriaPulitaMedioBassaPressione.valore = traduci.TraduciStringa(FS_TF_AriaPulitaMedioBassaPressione.nome);
        FS_TF_Esplicita.valore = traduci.TraduciStringa(FS_TF_Esplicita.nome);
        FS_TF_MaterialeSolido.valore = traduci.TraduciStringa(FS_TF_MaterialeSolido.nome);
        TL_Nessuno.setCaption(traduci.TraduciStringa("Deseleziona tutte"));
        TL_Tutti.setCaption(traduci.TraduciStringa("Seleziona tutte"));
    }
    
    public boolean loadElencoSerie() {
        try {
            Statement stmt = vGlobali.utilityCliente.ConnCliente.createStatement();
            String query = "select * from mzCategorie";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String key = rs.getString(2);
                ArrayList<String> elenco = new ArrayList<String>();
                int NCampi = rs.getInt("N");
                for (int i = 0; i<NCampi; i++) {
                    elenco.add(rs.getString("P"+ Integer.toString(i)));
                }
                elencoSerie.put(key, elenco);
            }
            rs.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //final boolean TRFlag, final boolean AssialiFlag, final boolean CentrifighiFlag
    public void init(boolean Trasmissione, boolean Assiali) {
        setTrasmissione = Trasmissione;
        setAssiali = Assiali;
        UtilityCliente uCliente = vGlobali.utilityCliente;
        if (!isMistral) {
            for (GruppoSelezioneSerie g : vGlobali.ElencoGruppiSelezioneSerie) {
                if (g.elencoSerie != null && g.elencoSerie.length>0) {
                SeriePredefinite.addItem(g.descrizione);
                SeriePredefinite.setEnabled(true);
                }
            }
            if (elencoSerie.isEmpty())
                loadElencoSerie();
        }
        DV_CorrettorePotenza.setValue(vGlobali.CorrettorePotenzaMotoreCorrente);
        DV_DiametroGirante.setValue(vGlobali.utilityUnitaMisura.frommToXX(vGlobali.SelezioneDati.DiametroMassimoGirante, vGlobali.UMDiametroCorrente));
        DV_GiriMaxGirante.setValue(vGlobali.SelezioneDati.giriMassimiGirante);
        modificabile = false;
        if (Assiali) FS_AssialiCentrifughiEsplicita.setValue(FS_ACE_Assiali);
        else FS_AssialiCentrifughiEsplicita.setValue(FS_ACE_Centrifughi);
        if (Trasmissione) FS_AccoppiatiTrasmissione.setValue(FS_AT_Trasmissione);
        else FS_AccoppiatiTrasmissione.setValue(FS_AT_Accoppiati);
        if (!isMistral) {
            controllaVincoli();
            buildTabellaSerie();
            contaSerieSelezionate();
        }
        if (isMistral) {
            FS_AssialiCentrifughiEsplicita.setVisible(false);
            FS_AccoppiatiTrasmissione.setVisible(false);
        }
        modificabile = true;
	}
    
    public oggettoInfoRicerca aggiornaValori() {
        oggettoSelect Trasmissione = (oggettoSelect) FS_AccoppiatiTrasmissione.getValue(); 
        String TipoTrasmissione = ((oggettoSelect) FS_AccoppiatiTrasmissione.getValue()).nome;
        String TipoVentilatore = ((oggettoSelect) FS_AssialiCentrifughiEsplicita.getValue()).nome;
        Double DiametroMassimoGirante = vGlobali.utilityUnitaMisura.fromXXTom(DV_DiametroGirante.getDoubleValue(), vGlobali.UMDiametroCorrente);
        Integer GiriMassimiGirante = DV_GiriMaxGirante.getIntegerValue();
        Integer CorrettorePotMotore = DV_CorrettorePotenza.getIntegerValue();
        if (!isMistral) {
            ArrayList<Serie> ElencoSerie = vGlobali.ElencoSerie;
            for(Serie s: ElencoSerie)
                s.UtilizzabileRicerca = false;
            Collection<?> ElencoID = containerSerie.getItemIds();
            for (Object id : ElencoID) {
                XdevCheckBox cb = (XdevCheckBox) containerSerie.getItem(id).getItemProperty("Selected").getValue();
                if (cb.getValue()) {
                    String serie = containerSerie.getItem(id).getItemProperty("Serie").getValue().toString();
                    for(Serie s : ElencoSerie) {
                        if (s.Serie.equals(serie)) {
                            s.UtilizzabileRicerca = true;
                            break;
                        }
                    }
                }
            }
        }
        return new oggettoInfoRicerca(TipoTrasmissione, TipoVentilatore, DiametroMassimoGirante, GiriMassimiGirante, CorrettorePotMotore);
    }
    
    private void controllaVincoli() {
        if (modificabile) return;
        if (getOptionGroupValue(FS_AssialiCentrifughiEsplicita, "Centrifugo")) {
            FS_TipoFluido.setItemEnabled(FS_TF_Esplicita, true);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaPulitaMedioBassaPressione, true);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaPulitaAltaPressione, true);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaPocoPolverosa, true);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaMoltoPolverosa, true);
            FS_TipoFluido.setItemEnabled(FS_TF_MaterialeSolido, true);
        } else {
            FS_TipoFluido.setItemEnabled(FS_TF_Esplicita, true);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaPulitaMedioBassaPressione, false);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaPulitaAltaPressione, false);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaPocoPolverosa, false);
            FS_TipoFluido.setItemEnabled(FS_TF_AriaMoltoPolverosa, false);
            FS_TipoFluido.setItemEnabled(FS_TF_MaterialeSolido, false);
        }
    }
    
    private void buildTabellaSerie() {
        ArrayList<String> elenco = new ArrayList<>();
        if (!SeriePredefinite.getValue().toString().equals("-")) {
            String selezionata = SeriePredefinite.getValue().toString();
            for (GruppoSelezioneSerie g : vGlobali.ElencoGruppiSelezioneSerie) {
                if (selezionata.equals(g.descrizione)) {
                    ArrayList<String> ex = new ArrayList<>();
                    for(String s : g.elencoSerie) {
                        ex.add(s);
                    }
                    elenco = ex;
                    break;
                }
            }
        }
        else if (getOptionGroupValue(FS_AssialiCentrifughiEsplicita, FS_ACE_Esplicita.nome))
        {
            if (getOptionGroupValue(FS_AccoppiatiTrasmissione, FS_AT_Accoppiati.nome))
                elenco = elencoSerie.get("EsplicitaDAEsplicita");
            else
                elenco = elencoSerie.get("EsplicitaTREsplicita");
        }
        else if (getOptionGroupValue(FS_AssialiCentrifughiEsplicita, FS_ACE_Centrifughi.nome))
        {
            if (getOptionGroupValue(FS_AccoppiatiTrasmissione, FS_AT_Accoppiati.nome)) {
                String tipoFluido = ((oggettoSelect)FS_TipoFluido.getValue()).nome;
                if (tipoFluido.equals(FS_TF_AriaPulitaMedioBassaPressione.nome))
                    elenco = elencoSerie.get("CentrifughiDAAriaPulitaBassa");
                else if (tipoFluido.equals(FS_TF_AriaPulitaAltaPressione.nome))
                    elenco = elencoSerie.get("CentrifughiDAAriaPulitaAlta");
                else if (tipoFluido.equals(FS_TF_AriaPocoPolverosa.nome))
                    elenco = elencoSerie.get("CentrifughiDAAriaPolverosa");
                else if (tipoFluido.equals(FS_TF_AriaMoltoPolverosa.nome))
                    elenco = elencoSerie.get("CentrifughiDAAriaMoltoPolverosa");
                else if (tipoFluido.equals(FS_TF_MaterialeSolido.nome))
                    elenco = elencoSerie.get("CentrifughiDAMaterialeSolido");
                else
                    elenco = elencoSerie.get("CentrifughiDA");
            } else {
                String tipoFluido = ((oggettoSelect)FS_TipoFluido.getValue()).nome;
                if (tipoFluido.equals(FS_TF_AriaPulitaMedioBassaPressione.nome))
                    elenco = elencoSerie.get("CentrifughiTRAriaPulitaBassa");
                else if (tipoFluido.equals(FS_TF_AriaPulitaAltaPressione.nome))
                    elenco = elencoSerie.get("CentrifughiTRAriaPulitaAlta");
                else if (tipoFluido.equals(FS_TF_AriaPocoPolverosa.nome))
                    elenco = elencoSerie.get("CentrifughiTRAriaPolverosa");
                else if (tipoFluido.equals(FS_TF_AriaMoltoPolverosa.nome))
                    elenco = elencoSerie.get("CentrifughiTRAriaMoltoPolverosa");
                else if (tipoFluido.equals(FS_TF_MaterialeSolido.nome))
                    elenco = elencoSerie.get("CentrifughiTRMaterialeSolido");
                else
                    elenco = elencoSerie.get("CentrifughiTR");
            }
        }
        else if (getOptionGroupValue(FS_AssialiCentrifughiEsplicita, FS_ACE_Assiali.nome))
        {
            if (getOptionGroupValue(FS_AccoppiatiTrasmissione, FS_AT_Accoppiati.nome))
                elenco = elencoSerie.get("AssialiDA");
            else
                elenco = elencoSerie.get("AssialiTR");
        }
        ArrayList<Serie> serieDaUsare = new ArrayList<>();
        for (String serieAttiva : elenco) {
            for(Serie serieLista : vGlobali.ElencoSerie) {
                if (serieLista.Serie.equals(serieAttiva)) {
                    serieDaUsare.add(serieLista);
                    break;
                }
            }
        }
        if (elenco != null && elenco.size() > 0)
            generaTabella(serieDaUsare);
	}
    
    public void generaTabella(ArrayList<Serie> elenco) {
            tableSerie.removeAllItems();
            tableSerie.setColumnHeader("QMin", "<html>" + StringheUNI.PORTATA + " min " + vGlobali.UMPortataCorrente.simbolo + "</html>");
            tableSerie.setColumnHeader("QMax", "<html>" + StringheUNI.PORTATA + " max " + vGlobali.UMPortataCorrente.simbolo + "</html>");
            tableSerie.setColumnHeader("PTMin", "<html>" + StringheUNI.PRESSIONETOTALE + " min " + vGlobali.UMPressioneCorrente.simbolo + "</html>");
            tableSerie.setColumnHeader("PTMax", "<html>" + StringheUNI.PRESSIONETOTALE + " max " + vGlobali.UMPressioneCorrente.simbolo + "</html>");
            for(Serie serie : elenco) {
                final Object obj[] = new Object[7];
                final XdevCheckBox cb = new XdevCheckBox();
		cb.setValue(serie.UtilizzabileRicerca);
		cb.addValueChangeListener((final com.vaadin.data.Property.ValueChangeEvent event) -> {
                    contaSerieSelezionate();
                });
		obj[0] = cb;
		obj[1] = serie.SerieTrans;
		obj[2] = Math.floor(vGlobali.utilityUnitaMisura.fromm3hToXX(serie.QMinm3h, vGlobali.UMPortataCorrente) * 100.) / 100.;
		obj[3] = Math.floor(vGlobali.utilityUnitaMisura.fromm3hToXX(serie.QMaxm3h, vGlobali.UMPortataCorrente) * 100.) / 100.;
		obj[4] = Math.floor(vGlobali.utilityUnitaMisura.fromPaToXX(serie.PTMinPa, vGlobali.UMPressioneCorrente) * 100.) / 100.;
		obj[5] = Math.floor(vGlobali.utilityUnitaMisura.fromPaToXX(serie.PTMaxPa, vGlobali.UMPressioneCorrente) * 100.) / 100.;
		obj[6] = vGlobali.utilityTraduzioni.TraduciStringa(serie.DescrizioneTrans);
                Object added = tableSerie.addItem(obj, null);
            }
    }
    
    public void contaSerieSelezionate() {
        Collection<?> ElencoID = containerSerie.getItemIds();
        int NumeroSerieSelezionate = 0;
        tableSerie.setEnabled(false);
        for (Object id : ElencoID) {
            XdevCheckBox cb = (XdevCheckBox) containerSerie.getItem(id).getItemProperty("Selected").getValue();
            if (cb.getValue())
                NumeroSerieSelezionate++;
        }
        tableSerie.setEnabled(true);
        TL_NumeroSerieSelezionate.setValue(MessageFormat.format("<b><font color='blue'>{0}: {1}</b>", vGlobali.utilityTraduzioni.TraduciStringa("Serie selezionate"), NumeroSerieSelezionate));
	}
    
    private void initUI() {
        PVentilatore = new Panel("Caratteristiche del Ventilatore");
        PFluido = new Panel("Caratteristiche del Fluido");
        PPredefinite = new Panel("Gruppi Serie Predefinite");
        
        tableSerie = new XdevTable<>();
        LayoutPrincipale = new XdevHorizontalLayout();
        LayoutSelezione = new XdevVerticalLayout();
        LayoutFiltriSelezione = new XdevVerticalLayout();
        LayoutTabella = new XdevVerticalLayout();
        FS_AssialiCentrifughiEsplicita = new OptionGroup("Seleziona Tipo di Ventilatore");
        FS_AccoppiatiTrasmissione = new OptionGroup("Seleziona Tipo di Trasmissione");
        GridDV = new XdevGridLayout(2, 3);
        DV_CorrettorePotenzaLabel = new XdevLabel("");
        DV_CorrettorePotenza = new TextFieldInteger();
        DV_CorrettorePotenzaLabelUM = new XdevLabel("%");
        DV_DiametroGiranteLabel = new XdevLabel("Diametro Massimo Girante");
        DV_DiametroGirante = new TextFieldDouble();
        DV_DiametroGiranteLabelUM = new XdevLabel("mm");
        DV_GiriMaxGiranteLabel = new XdevLabel("Giri Massimi Girante");
        DV_GiriMaxGirante = new TextFieldInteger();
        DV_GiriMaxGiranteLabelUM = new XdevLabel("RPM");
        TL_Tutti = new Button("Seleziona Tutti");
        TL_Nessuno = new Button("Deseleziona Tutti");
        TL_NumeroSerieSelezionate = new XdevLabel("Serie selezionate", ContentMode.HTML);
        LayoutBottoni = new XdevHorizontalLayout(TL_NumeroSerieSelezionate, TL_Tutti, TL_Nessuno);
        SeriePredefinite = new XdevComboBox<>();
        SeriePredefinite.addItem("-");
        SeriePredefinite.setValue("-");
        SeriePredefinite.setEnabled(false);
        
//tabella = new pannelloSelezioneSerieDefault();
        
        DV_CorrettorePotenzaLabel.setSizeFull();
        DV_CorrettorePotenza.setCaption("Correttore Potenza Motore");
        DV_CorrettorePotenza.setSizeFull();
        DV_CorrettorePotenzaLabelUM.setSizeFull();
        DV_DiametroGiranteLabel.setSizeFull();
        DV_DiametroGirante.setCaption("Diametro Massimo Girante");
        DV_DiametroGirante.setSizeFull();
        DV_DiametroGiranteLabelUM.setSizeFull();
        DV_GiriMaxGiranteLabel.setSizeFull();
        DV_GiriMaxGirante.setSizeFull();
        DV_GiriMaxGirante.setCaption("Giri Massimi Girante");
        DV_GiriMaxGiranteLabelUM.setSizeFull();
        GridDV.setAutoFill(XdevGridLayout.AutoFill.HORIZONTAL);
        GridDV.setWidth(100, Unit.PERCENTAGE);
        
//        GridDV.setColumnExpandRatios(4,1);
        FS_TipoFluido = new OptionGroup("Seleziona Tipo di Fluido");
                
        
        FS_ACE_Assiali = new oggettoSelect("Assiale");
        FS_ACE_Centrifughi = new oggettoSelect("Centrifugo");
        FS_ACE_Esplicita = new oggettoSelect("Selezione Esplicita");
        FS_AT_Accoppiati = new oggettoSelect("Direttamente Accoppiato");
        FS_AT_Trasmissione = new oggettoSelect("Trasmissione");
        FS_TF_Esplicita = new oggettoSelect("Selezione Esplicita");
        FS_TF_AriaPulitaMedioBassaPressione = new oggettoSelect("Aria pulita medio bassa pressione");
        FS_TF_AriaPulitaAltaPressione = new oggettoSelect("Aria pulita alta pressione");
        FS_TF_AriaPocoPolverosa = new oggettoSelect("Aria poco polverosa");
        FS_TF_AriaMoltoPolverosa = new oggettoSelect("Aria molto polverosa");
        FS_TF_MaterialeSolido = new oggettoSelect("Materiale solido");
        FS_AssialiCentrifughiEsplicita.addItems(FS_ACE_Assiali, FS_ACE_Centrifughi, FS_ACE_Esplicita);
        FS_AccoppiatiTrasmissione.addItems(FS_AT_Accoppiati, FS_AT_Trasmissione);
        GridDV.addComponent(DV_CorrettorePotenza, 0, 0);
        GridDV.addComponent(DV_CorrettorePotenzaLabelUM, 1, 0);
        GridDV.addComponent(DV_DiametroGirante, 0, 1);
        GridDV.addComponent(DV_DiametroGiranteLabelUM, 1, 1);
        GridDV.addComponent(DV_GiriMaxGirante, 0, 2);
        GridDV.addComponent(DV_GiriMaxGiranteLabelUM, 1, 2);
        GridDV.setComponentAlignment(DV_CorrettorePotenzaLabelUM, Alignment.BOTTOM_LEFT);
        GridDV.setComponentAlignment(DV_DiametroGiranteLabelUM, Alignment.BOTTOM_LEFT);
        GridDV.setComponentAlignment(DV_GiriMaxGiranteLabelUM, Alignment.BOTTOM_LEFT);
        GridDV.setMargin(new MarginInfo(false, false, false, false));
        LayoutFiltriSelezione.addComponents(FS_AssialiCentrifughiEsplicita, FS_AccoppiatiTrasmissione, GridDV);
        LayoutFiltriSelezione.setMargin(new MarginInfo(false, false, false, false));
        
        LayoutTabella.addComponents(tableSerie, LayoutBottoni);
        LayoutTabella.setComponentAlignment(LayoutBottoni, Alignment.MIDDLE_RIGHT);
        LayoutTabella.setExpandRatios(10,1);
        LayoutTabella.setMargin(new MarginInfo(false, false, false, false));
        LayoutTabella.setSizeFull();
        FS_TipoFluido.addItems(FS_TF_Esplicita, FS_TF_AriaPulitaMedioBassaPressione, FS_TF_AriaPulitaAltaPressione, FS_TF_AriaPocoPolverosa, FS_TF_AriaMoltoPolverosa, FS_TF_MaterialeSolido);
        FS_TipoFluido.setValue(FS_TF_Esplicita);
        PVentilatore.setContent(LayoutFiltriSelezione);
        PFluido.setContent(FS_TipoFluido);
        LayoutSelezione.addComponents(PVentilatore, PFluido, PPredefinite);
        LayoutSelezione.setMargin(new MarginInfo(false, false, false, false));
        tableSerie.setCaption("Selezionare le serie di ventilatori che si desidera includere");
        tableSerie.setWidth(600, Unit.PIXELS);
        tableSerie.setHeight(100, Unit.PERCENTAGE);
        LayoutPrincipale.addComponents(LayoutSelezione, LayoutTabella);
        PPredefinite.setContent(SeriePredefinite);
        PFluido.setStyleName("pannelliRicerca");
        PVentilatore.setStyleName("pannelliRicerca");
        PPredefinite.setStyleName("pannelliRicerca");
        this.setSizeFull();
        this.setContent(LayoutPrincipale);
        
        
        FS_AccoppiatiTrasmissione.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            Reload_valueChange();
        });
        FS_AssialiCentrifughiEsplicita.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            Reload_valueChange();
        });
        FS_TipoFluido.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            Reload_valueChange();
        });
        SeriePredefinite.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            Reload_valueChangePredefinite();
        });
        TL_Tutti.addClickListener(event -> TL_TuttiONessuno_buttonClick(true));
        TL_Nessuno.addClickListener(event -> TL_TuttiONessuno_buttonClick(false));
    }
    
    private void Reload_valueChangePredefinite() {
        if (!modificabile) return;
        modificabile = false;
        if (!isMistral) {
            buildTabellaSerie();
            contaSerieSelezionate();
        }
        modificabile = true;
    }
    
    private void Reload_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        SeriePredefinite.setValue("-");
        if (!isMistral) {
            buildTabellaSerie();
            contaSerieSelezionate();
        }
        modificabile = true;
    }
    
    private void TL_TuttiONessuno_buttonClick(boolean tutti) {
        Collection<?> ElencoID = containerSerie.getItemIds();
        tableSerie.setEnabled(false);
        for (Object id : ElencoID) {
            XdevCheckBox cb = (XdevCheckBox) containerSerie.getItem(id).getItemProperty("Selected").getValue();
            cb.setValue(tutti);
        }
        tableSerie.setEnabled(true);
        contaSerieSelezionate();
    }
    
    
    
    
}
