package com.cit.sellfan.ui.pannelli;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.Costanti;
import cit.sellfan.classi.Motore;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.xdev.ui.XdevCheckBox;
import de.steinwedel.messagebox.MessageBox;
import java.util.Arrays;

public class pannelloSelezioneMotore extends Panel {
	private VariabiliGlobali l_VG;
	private double PotenzaRichiestaW;
	private double NumeroGiri;
	private double PD2;
	@SuppressWarnings("unused")
	private boolean Trasmissione;
	private String oldCodice = "";
	private String newCodice = "";
	private final Container containerMotori = new IndexedContainer();
	private final ArrayList<CheckBox> ElencochkSelezionato = new ArrayList<>();
	private final ArrayList<String> ElencoCodici = new ArrayList<>();
    private double elencoPotenzeMotoriForHelpkw[] = null;
	private String elencoClassiMotori[] = null;
        private boolean AtexFan;
        private boolean ChangingPoles = false;
        private String query;
        private Motore custom;
	/**
	 * 
	 */
	public pannelloSelezioneMotore() {
		super();
		this.initUI();
		
		this.containerMotori.removeAllItems();
		this.containerMotori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerMotori.addContainerProperty("Code", String.class, "");
		this.containerMotori.addContainerProperty("Brand", String.class, "");
		
		this.containerMotori.addContainerProperty("IE", String.class, "");
		this.containerMotori.addContainerProperty("Atex", String.class, "");
		this.containerMotori.addContainerProperty("PTC", String.class, "");
		
		this.containerMotori.addContainerProperty("[kW]", String.class, "");
		this.containerMotori.addContainerProperty("Volt", String.class, "");
		this.containerMotori.addContainerProperty("Poli", String.class, "");
		this.tableMotori.setContainerDataSource(this.containerMotori);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		Nazionalizza();
		if (this.l_VG.MobileMode) {
			this.tableMotori.setWidth(300, Unit.PIXELS);
		}
		this.labelWarning.setVisible(false);
		this.labelTitolo.setVisible(!this.l_VG.MobileMode);
		this.gridLayoutFiltri.setVisible(!this.l_VG.MobileMode);
		if (this.l_VG.currentUserSaaS.idClienteIndex != 4) {
			this.tableMotori.setColumnWidth("Class", 0);
			this.tableMotori.setColumnWidth("Atex", 0);
			this.tableMotori.setColumnWidth("PTC", 0);
                        this.comboBoxClasse.setEnabled(false);
                        comboBoxAtex.setEnabled(false);
                        comboBoxPTC.setEnabled(false);
		}
                if (l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Codice.equals("Custom")) {
                    custom = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato;
                }
                this.buttonCustom.setVisible(this.l_VG.ParametriVari.showCustomMotorButton);
	}
	
	public void Nazionalizza() {
		this.labelTitolo.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Selezionare il motore che si desidera utilizzare") + "</html>");
		
		this.tableMotori.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableMotori.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
			this.tableMotori.setColumnHeader("Atex", "Atex");
			this.tableMotori.setColumnHeader("PTC", "PTC");
		} else {
			this.tableMotori.setColumnHeader("Class", "");
			this.tableMotori.setColumnHeader("Atex", "");
			this.tableMotori.setColumnHeader("PTC", "");
		}
		this.tableMotori.setColumnHeader("Volt", this.l_VG.utilityTraduzioni.TraduciStringa("Volt"));
		this.tableMotori.setColumnHeader("Poli", this.l_VG.utilityTraduzioni.TraduciStringa("N. Poli"));
		
                this.labelDescrizione.setValue("Brand");
                this.labelPoli.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Poli"));
		this.comboBoxAtex.removeAllItems();
		this.comboBoxAtex.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Tutti"));
		this.comboBoxAtex.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Si"));
		this.comboBoxAtex.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("No"));
		this.comboBoxPTC.removeAllItems();
		this.comboBoxPTC.addItem("-");
		this.comboBoxPTC.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Si"));
		this.comboBoxPTC.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("No"));
		this.comboBoxPTC.setValue("-");
	}

	public void setVentilatoreCorrente() {
		this.PotenzaRichiestaW = 0.;
                if (l_VG.VentilatoreCurrent.CAProgettazione.temperatura>l_VG.VentilatoreCurrent.CARiferimento.temperatura) {
                    this.PotenzaRichiestaW = this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsternaFree;
                    final MessageBox msgBox = MessageBox.create();
                    msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Dimensionamento motore calcolato in base a temperatura di default (20°)"));
                    msgBox.withOkButton();
                    msgBox.open();
                } else try {
			this.PotenzaRichiestaW = this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza;
		} catch (final Exception e) {
			
		}
        this.PotenzaRichiestaW += this.l_VG.VentilatoreCurrent.datiERP327.potenzaPersaCuscinettiW;
        this.PotenzaRichiestaW /= this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione;
        if (this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            this.PotenzaRichiestaW *= (1. + this.l_VG.VentilatoreCurrent.CorrettorePotenzaMotore / 100.);
        } else {
            this.PotenzaRichiestaW *= (1. + this.l_VG.CorrettorePotenzaMotoreCorrente / 100.);
        }
        this.NumeroGiri = this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri;
        this.PD2 = this.l_VG.VentilatoreCurrent.PD2;
        this.Trasmissione = this.l_VG.VentilatoreCurrent.Trasmissione;
        try {
        	this.oldCodice = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente;
        } catch (final Exception e) {
        	this.oldCodice = "-";
        }
        this.elencoPotenzeMotoriForHelpkw = this.l_VG.utilityCliente.getElencoPotenzeMotorikW(this.l_VG.VentilatoreCurrent);
        this.elencoClassiMotori = this.l_VG.utilityCliente.getElencoClassiMotori();
        this.comboBoxClasse.setEnabled(false);
		this.comboBoxClasse.removeAllItems();
		this.comboBoxClasse.addItem("-");
        if (this.elencoClassiMotori != null) {
            for (int i=0 ; i<this.elencoClassiMotori.length ; i++) {
            	this.comboBoxClasse.addItem(this.elencoClassiMotori[i]);
            }
        }
        this.comboBoxClasse.setValue("-");
        final Collection<?> coll = this.comboBoxClasse.getItemIds();
        if (coll.size() > 1) {
			this.comboBoxClasse.setEnabled(true);
		}
        if (l_VG.currentUserSaaS.Ambiente == "MZ") {
            String isAtex = "No";
            String Atex = this.l_VG.utilityCliente.cliente04FunzioniSpecifiche.isVentilatoreATEX(l_VG.VentilatoreCurrent, this.l_VG.gestioneAccessori);
            if (Atex != null)
                isAtex = "Si";
            if (isAtex.equals("Si"))
                AtexFan = true;
            else
                AtexFan = false;
            this.comboBoxAtex.setValue(this.l_VG.utilityTraduzioni.TraduciStringa(isAtex));
        }
        else {
            AtexFan = false;
            this.comboBoxAtex.setValue(false);
        }
        loadtabellaMotori();
	}
	
	public String getNewCodice() {
            return this.newCodice;
	}

	public String getOldCodice() {
            return this.oldCodice;
	}
        public Motore getMotoreCustom() {
            return this.custom;
        }

        private void filtratabellaMotori() {
		double potenzaW = this.textFieldPot.getDoubleValue() * 1000.;
                boolean potenzaSelezionata = potenzaW == 0 ? false : true;  
                if (!l_VG.ParametriVari.controlloPoliPotenza) {
                    double KwOrig = getDefaultKw(this.l_VG.VentilatoreCurrent.MotoreOriginale) * 1000;
                    if (KwOrig > potenzaW)
                        potenzaW = KwOrig;
                }
		Object obj = this.comboBoxClasse.getValue();
		String classe = "-";
		if (obj != null) {
			classe = obj.toString();
		}
		if (classe.equals("")) {
			classe = "-";
		}
		obj = this.comboBoxAtex.getValue();
		String atex = "-";
		if (obj != null) {
                    if (obj.toString().equals(this.l_VG.utilityTraduzioni.TraduciStringa("Si")) || obj.toString().equals(this.l_VG.utilityTraduzioni.TraduciStringa("No")))
			atex = obj.toString();
                    else atex = "-";
		}
		if (atex.equals("")) {
			atex = "-";
		}
		obj = this.comboBoxPTC.getValue();
		String PTC = "-";
		if (obj != null) {
			PTC = obj.toString();
		}
		if (PTC.equals("")) {
			PTC = "-";
		}
                int poli = -1;
                if (check2Poli.getValue()) poli = 2;
                    else if (check4Poli.getValue()) poli = 4;
                        else if (check6Poli.getValue()) poli = 6;
                            else if (check8Poli.getValue()) poli = 8;
                
        String query = this.l_VG.utilityCliente.buildFiltroSceltaMotoreForSelezioneManuale(this.l_VG.VentilatoreCurrent, this.textFieldLikeCodice.getValue(), this.textFieldLikeDescrizione.getValue(), potenzaW, potenzaSelezionata, this.textFieldV.getIntegerValue(), poli, this.PotenzaRichiestaW, this.NumeroGiri, this.PD2, classe, atex, PTC);
        query = this.l_VG.utilityCliente.buildQuerySceltaMotoreAdOrderBy(query);
        String codiceMonoblocco = "";
        if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
            codiceMonoblocco = ((Cliente04VentilatoreCampiCliente) l_VG.VentilatoreCurrent.campiCliente).Monoblocco;
            if (codiceMonoblocco.equalsIgnoreCase("MZ62"))
                query = query.replace("and Freq", "and PotkW<=7.5 and Freq");
        }
        loadtabellaMotori(query);
	}
	
    private void checkBoxPoli_valueChange(int c) {
        if (ChangingPoles == false) {
        ChangingPoles = true;
        switch (c) {
            case 2:
                check4Poli.setValue(false);
                check6Poli.setValue(false);
                check8Poli.setValue(false);
                break;
            case 4:
                check2Poli.setValue(false);
                check6Poli.setValue(false);
                check8Poli.setValue(false);
                break;
            case 6:
                check2Poli.setValue(false);
                check4Poli.setValue(false);
                check8Poli.setValue(false);
                break;
            case 8:
                check2Poli.setValue(false);
                check4Poli.setValue(false);
                check6Poli.setValue(false);
                break;
        }
        ChangingPoles = false;    
        }
    }    
    
        
    private void loadtabellaMotori() {
        String[] ListaSerieMoro = {"MN", "MB"};
        String query = "";
        if (l_VG.currentUserSaaS.idClienteIndex == 1) {
            double kw = getDefaultKw(l_VG.VentilatoreCurrent.MotoreOriginale);
            int poli = getDefaultPoli(l_VG.VentilatoreCurrent.MotoreOriginale);
            switch (poli) {
                case 2:
                    check2Poli.setEnabled(true);
                    check4Poli.setEnabled(true);
                    check6Poli.setEnabled(true);
                    check8Poli.setEnabled(true);
                    check2Poli.setValue(true);
                    break;
                case 4:
                    check2Poli.setEnabled(false);
                    check4Poli.setEnabled(true);
                    check6Poli.setEnabled(true);
                    check8Poli.setEnabled(true);
                    check4Poli.setValue(true);
                    break;
                case 6:
                    check2Poli.setEnabled(false);
                    check4Poli.setEnabled(false);
                    check6Poli.setEnabled(true);
                    check8Poli.setEnabled(true);
                    check6Poli.setValue(true);
                    break;
                case 8:
                    check2Poli.setEnabled(false);
                    check4Poli.setEnabled(false);
                    check6Poli.setEnabled(false);
                    check8Poli.setEnabled(true);
                    check8Poli.setValue(true);
                    break;
            }
            if (l_VG.ParametriVari.controlloPoliPotenza) {
                check2Poli.setEnabled(true);
                check4Poli.setEnabled(true);
                check6Poli.setEnabled(true);
                check8Poli.setEnabled(true);
            }
            int poli2 = poli + 2;
            if (l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E08") && kw > 0) {
                double pot = PotenzaRichiestaW > (kw * 1000) ? PotenzaRichiestaW : kw * 1000;
                query = this.l_VG.utilityCliente.buildQuerySceltaMotoreForSelezioneManuale(this.l_VG.VentilatoreCurrent, pot, this.NumeroGiri, this.PD2);
                query = query.replace("and Freq", "and Poli IN (" + poli + ", " + poli2 + ") and Freq");
            } else {
                query = this.l_VG.utilityCliente.buildQuerySceltaMotoreForSelezioneManuale(this.l_VG.VentilatoreCurrent, this.PotenzaRichiestaW, this.NumeroGiri, this.PD2);
                query = this.l_VG.utilityCliente.buildQuerySceltaMotoreAdOrderBy(query);
                query = query.replace("and Freq", "and Poli >= (" + poli + ") and Freq");
            }
            if (AtexFan) {
                query = query.replace("and Freq", "and isAtex = 1 and Freq");
            } else {
                query = query.replace("and Freq", "and isAtex = 0 and Freq");
            }
            if (l_VG.VentilatoreCurrent.Trasmissione) {
                query = query.replace("and Freq", "and diretto = 0 and Freq");
            }
        } else {
            final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
            double kw = getDefaultKw(l_VG.VentilatoreCurrent.MotoreOriginale);
            int poli = getDefaultPoli(l_VG.VentilatoreCurrent.MotoreOriginale);
            if (Arrays.asList(ListaSerieMoro).contains(l_VG.VentilatoreCurrent.Serie)) {
                query = creaQueryMotoreMoro(l_VG.VentilatoreCurrent, AtexFan);
            } else if (l_vcc04.Tipo.equals("assiale") && !l_VG.trasmissioneFlag) {
                query = this.l_VG.utilityCliente.buildQuerySceltaMotoreLimitata(kw, poli, PD2);
                if (AtexFan) {
                    query = query + " and isAtex = 1";
                } else {
                    query = query + " and isAtex = 0";
                }
                loadtabellaMotori(query);
            } else {
                String codiceMonoblocco = "";
                if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                    codiceMonoblocco = ((Cliente04VentilatoreCampiCliente) l_VG.VentilatoreCurrent.campiCliente).Monoblocco;
                }
                switch (poli) {
                    case 2:
                        check2Poli.setEnabled(true);
                        check4Poli.setEnabled(true);
                        check6Poli.setEnabled(true);
                        check8Poli.setEnabled(true);
                        check2Poli.setValue(true);
                        break;
                    case 4:
                        check2Poli.setEnabled(true);
                        check4Poli.setEnabled(true);
                        check6Poli.setEnabled(true);
                        check8Poli.setEnabled(true);
                        check4Poli.setValue(true);
                        break;
                    case 6:
                        check2Poli.setEnabled(true);
                        check4Poli.setEnabled(true);
                        check6Poli.setEnabled(true);
                        check8Poli.setEnabled(true);
                        check6Poli.setValue(true);
                        break;
                    case 8:
                        check2Poli.setEnabled(true);
                        check4Poli.setEnabled(true);
                        check6Poli.setEnabled(true);
                        check8Poli.setEnabled(true);
                        check8Poli.setValue(true);
                        break;
                }
                if (l_VG.ParametriVari.controlloPoliPotenza) {
                    check2Poli.setEnabled(true);
                    check4Poli.setEnabled(true);
                    check6Poli.setEnabled(true);
                    check8Poli.setEnabled(true);
                }
                int poli2 = poli + 2;
                query = "";
                if (l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E08") && kw > 0) {
                    double pot = PotenzaRichiestaW > (kw * 1000) ? PotenzaRichiestaW : kw * 1000;
                    query = this.l_VG.utilityCliente.buildQuerySceltaMotoreForSelezioneManuale(this.l_VG.VentilatoreCurrent, pot, this.NumeroGiri, this.PD2);
                    query = query.replace("and Freq", "and Poli IN (" + poli + ", " + poli2 + ") and Freq");
                } else {
                    query = this.l_VG.utilityCliente.buildQuerySceltaMotoreForSelezioneManuale(this.l_VG.VentilatoreCurrent, this.PotenzaRichiestaW, this.NumeroGiri, this.PD2);
                    query = this.l_VG.utilityCliente.buildQuerySceltaMotoreAdOrderBy(query);
                    query = query.replace("and Freq", "and Poli >= (" + poli + ") and Freq");
                }
                if (AtexFan) {
                    query = query.replace("and Freq", "and isAtex = 1 and Freq");
                } else {
                    query = query.replace("and Freq", "and isAtex = 0 and Freq");
                }
                if (l_VG.VentilatoreCurrent.Trasmissione) {
                    query = query.replace("and Freq", "and diretto = 0 and Freq");
                }
                if (codiceMonoblocco.equalsIgnoreCase("MZ62")) {
                    query = query.replace("and Freq", "and PotkW<=7.5 and Freq");
                }
            }
        }
        loadtabellaMotori(query);
    }
    
    private double getDefaultKw(final String nomeMotore) {
        double result = -1;
        String query = "SELECT PotkW from Motori where CodiceCliente = '" + nomeMotore + "'";
        try {
            Statement stmt = this.l_VG.conTecnica.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            result = rs.getDouble("PotkW");
        } catch (Exception e) {
            
        }
        return result;
    }
    
    private int getDefaultPoli(final String nomeMotore) {
        int result = -1;
        String query = "SELECT Poli from Motori where CodiceCliente = '" + nomeMotore + "'";
        try {
            Statement stmt = this.l_VG.conTecnica.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            result = rs.getInt("Poli");
        } catch (Exception e) {
            
        }
        return result;
    }
    
    private void loadtabellaMotori(final String query) {
    	String codiceMotore;
    	int index = 0;
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
            try {
                this.ElencochkSelezionato.clear();
                this.ElencoCodici.clear();
                this.tableMotori.removeAllItems();//resetta anche le selection
                final Statement stmt = this.l_VG.conTecnica.createStatement();
                final ResultSet rs = stmt.executeQuery(query);
                Motore m = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato;
                if (m!= null && m.Codice.equals("Custom")) {
                    Object obj[] = new Object[9];
                    String itemID = Integer.toString(index++);
                    codiceMotore = m.Codice;
                    newCodice = codiceMotore;
                    oldCodice = "";
                    final CheckBox chkSelCus = new CheckBox();
                    chkSelCus.setData(codiceMotore);
                    chkSelCus.setValue(true);
                    chkSelCus.addValueChangeListener(
                        new ValueChangeListener() {
                        @Override
                        public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                            if (!chkSelCus.isEnabled()) { return; }
                            newCodice = chkSelCus.getData().toString();
                            for (int i=0 ; i<ElencochkSelezionato.size() ; i++) {
                                if (!ElencoCodici.get(i).equals(newCodice)) {
                                    ElencochkSelezionato.get(i).setEnabled(false);
                                    ElencochkSelezionato.get(i).setValue(false);
                                    ElencochkSelezionato.get(i).setEnabled(true);
                                } else {
                                    ElencochkSelezionato.get(i).setEnabled(false);
                                    ElencochkSelezionato.get(i).setValue(true);
                                    ElencochkSelezionato.get(i).setEnabled(true);
                                }
                            }
                        }
                    });
                    ElencochkSelezionato.add(chkSelCus);
                    ElencoCodici.add(codiceMotore);
                    String st = m.Descrizione.equals("-") ? codiceMotore : m.Descrizione;
                    String atex = m.isAtex ? "*" : "";
                    String PTC = m.PTCPresente ? "*" : ""; 
                    obj[0] = chkSelCus;
                    obj[1] = codiceMotore;
                    obj[2] = st;
                    obj[3] = m.EfficienzaEnergetica;
                    obj[4] = atex;
                    obj[5] = PTC;
                    obj[6] = this.l_VG.fmtNd.format(m.PotkW);
                    obj[7] = Integer.toString(m.Volt);
                    obj[8] = Integer.toString(m.Poli);
                    this.tableMotori.addItem(obj, itemID);    
                }
                while(rs.next()) {
                    final Object obj[] = new Object[9];
                    final String itemID = Integer.toString(index++);
                    codiceMotore = rs.getString("CodiceCliente");
                    final CheckBox chkSel = new CheckBox();
                    chkSel.setData(codiceMotore);
                    if (codiceMotore.equals(this.oldCodice)) {
                        chkSel.setValue(true);
                    }
                    chkSel.addValueChangeListener(new ValueChangeListener() {
                        @Override
                        public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                            if (!chkSel.isEnabled()) {
                                return;
                            }
                            pannelloSelezioneMotore.this.newCodice = chkSel.getData().toString();
                            //l_VG.MainView.setMemo(newCodice);
                            for (int i=0 ; i<pannelloSelezioneMotore.this.ElencochkSelezionato.size() ; i++) {
                                if (!pannelloSelezioneMotore.this.ElencoCodici.get(i).equals(pannelloSelezioneMotore.this.newCodice)) {
                                    pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(false);
                                    pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setValue(false);
                                    pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(true);
                                } else {
                                    pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(false);
                                    pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setValue(true);
                                    pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(true);
                                }
                            }
                        }
                    });
                    obj[0] = chkSel;
                 this.ElencochkSelezionato.add(chkSel);
                 this.ElencoCodici.add(codiceMotore);
                        obj[1] = codiceMotore;
                        String str = rs.getString("Descrizione");
                        if (str.equals("-")) {
                            str = codiceMotore;
                        }
                        obj[2] = str;
                        obj[3] = rs.getString("EfficienzaEnergetica");
                        int intTemp = -1;
                        intTemp = rs.getInt("isAtex");
                        if (intTemp == 1) {
                            obj[4] = "*";
                        }
                        intTemp = rs.getInt("PTC");
                        if (intTemp == 1) {
                            obj[5] = "*";
                        }
                        obj[6] = this.l_VG.fmtNd.format(rs.getDouble("PotkW"));
                        obj[7] = Integer.toString(rs.getInt("Volt"));
                        obj[8] = Integer.toString(rs.getInt("Poli"));
                        this.tableMotori.addItem(obj, itemID);
                }
            } catch (final Exception e) {
                Notification.show(e.toString());
        }
    }
    
    private void inserisciMotore(Motore m) {
        String codiceMotore;
    	int index = 0;
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
            try {
                    this.ElencochkSelezionato.clear();
                    this.ElencoCodici.clear();
                    this.tableMotori.removeAllItems();//resetta anche le selection
                    final Statement stmt = this.l_VG.conTecnica.createStatement();
                    final ResultSet rs = stmt.executeQuery(query);
                    Object obj[] = new Object[9];
                    String itemID = Integer.toString(index++);
                    codiceMotore = m.Codice;
                    newCodice = codiceMotore;
                    oldCodice = "";
                    final CheckBox chkSelCus = new CheckBox();
                    chkSelCus.setData(codiceMotore);
                    chkSelCus.setValue(true);
                    chkSelCus.addValueChangeListener(
                        new ValueChangeListener() {
                        @Override
                        public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                            if (!chkSelCus.isEnabled()) { return; }
                            newCodice = chkSelCus.getData().toString();
                            for (int i=0 ; i<ElencochkSelezionato.size() ; i++) {
                                if (!ElencoCodici.get(i).equals(newCodice)) {
                                    ElencochkSelezionato.get(i).setEnabled(false);
                                    ElencochkSelezionato.get(i).setValue(false);
                                    ElencochkSelezionato.get(i).setEnabled(true);
                                } else {
                                    ElencochkSelezionato.get(i).setEnabled(false);
                                    ElencochkSelezionato.get(i).setValue(true);
                                    ElencochkSelezionato.get(i).setEnabled(true);
                                }
                            }
                        }
                    });
                    ElencochkSelezionato.add(chkSelCus);
                    ElencoCodici.add(codiceMotore);
                    String st = m.Descrizione.equals("-") ? codiceMotore : m.Descrizione;
                    String atex = m.isAtex ? "*" : "";
                    String PTC = m.PTCPresente ? "*" : ""; 
                    obj[0] = chkSelCus;
                    obj[1] = codiceMotore;
                    obj[2] = st;
                    obj[3] = m.EfficienzaEnergetica;
                    obj[4] = atex;
                    obj[5] = PTC;
                    obj[6] = this.l_VG.fmtNd.format(m.PotkW);
                    obj[7] = Integer.toString(m.Volt);
                    obj[8] = Integer.toString(m.Poli);
                    this.tableMotori.addItem(obj, itemID);
                    
                    while(rs.next()) {
                        obj = new Object[9];
                        itemID = Integer.toString(index++);
                        codiceMotore = rs.getString("CodiceCliente");
                        final CheckBox chkSel = new CheckBox();
                        chkSel.setData(codiceMotore);
                        if (codiceMotore.equals(this.oldCodice)) {
                            chkSel.setValue(true);
                        }
                        chkSel.addValueChangeListener(new ValueChangeListener() {
                            @Override
                            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                                if (!chkSel.isEnabled()) {
                                    return;
                                }
                                pannelloSelezioneMotore.this.newCodice = chkSel.getData().toString();
                                //l_VG.MainView.setMemo(newCodice);
                                for (int i=0 ; i<pannelloSelezioneMotore.this.ElencochkSelezionato.size() ; i++) {
                                    if (!pannelloSelezioneMotore.this.ElencoCodici.get(i).equals(pannelloSelezioneMotore.this.newCodice)) {
                                        pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(false);
                                        pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setValue(false);
                                        pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(true);
                                    } else {
                                        pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(false);
                                        pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setValue(true);
                                        pannelloSelezioneMotore.this.ElencochkSelezionato.get(i).setEnabled(true);
                                    }
                                }
                            }
                        });
                        obj[0] = chkSel;
                        this.ElencochkSelezionato.add(chkSel);
                        this.ElencoCodici.add(codiceMotore);
                        obj[1] = codiceMotore;
                        String str = rs.getString("Descrizione");
                        if (str.equals("-")) {
                            str = codiceMotore;
                        }
                        obj[2] = str;
                        obj[3] = rs.getString("EfficienzaEnergetica");
                        int intTemp = -1;
                        intTemp = rs.getInt("isAtex");
                        if (intTemp == 1) {
                            obj[4] = "*";
                        }
                        intTemp = rs.getInt("PTC");
                        if (intTemp == 1) {
                            obj[5] = "*";
                        }
                        obj[6] = this.l_VG.fmtNd.format(rs.getDouble("PotkW"));
                        obj[7] = Integer.toString(rs.getInt("Volt"));
                        obj[8] = Integer.toString(rs.getInt("Poli"));
                        this.tableMotori.addItem(obj, itemID);
                    }
            } catch (final Exception e) {
                Notification.show(e.toString());
        }
    }
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonFiltra}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonFiltra_buttonClick(final Button.ClickEvent event) {
		filtratabellaMotori();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonHelpPotenza}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonHelpPotenza_buttonClick(final Button.ClickEvent event) {
		//elencoPotenzeMotoriForHelpkw
		final pannelloHelpPotenza pannello = new pannelloHelpPotenza();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.initPannello(this.elencoPotenzeMotoriForHelpkw);
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withAbortButton();
		msgBox.withOkButton(() -> {
			final int index = pannello.getIndexPotenzaSelezionato();
			if (index >= 0) {
				this.textFieldPot.setValue(this.elencoPotenzeMotoriForHelpkw[index]);
			}
			});
		msgBox.open();
	}
        
        private void buttonCustom_buttonClick(final Button.ClickEvent event) {
		//elencoPotenzeMotoriForHelpkw
                String codice = this.newCodice!=""? newCodice : oldCodice;					
		final pannelloMotoreCustom pannello = new pannelloMotoreCustom(codice);
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.initPannello();
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withAbortButton();
		msgBox.withOkButton(() -> {
                    cit.sellfan.classi.Motore mot = pannello.getNuovoMotore();
                    inserisciMotore(mot);
                    custom = mot;
		});
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonFiltra1}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonFiltra1_buttonClick(final Button.ClickEvent event) {
		filtratabellaMotori();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelWarning = new XdevLabel();
		this.labelTitolo = new XdevLabel();
		this.labelDescrizione = new XdevLabel();
		this.labelIE = new XdevLabel();
		this.labelAtex = new XdevLabel();
                this.labelPTC = new XdevLabel();
                this.labelKw = new XdevLabel();
                this.gridLayoutNuova = new XdevGridLayout();
		this.gridLayoutFiltri = new XdevGridLayout();
		this.buttonFiltra = new XdevButton();
		this.textFieldLikeCodice = new XdevTextField();
		this.textFieldLikeDescrizione = new XdevTextField();
		this.comboBoxClasse = new XdevComboBox<>();
		this.comboBoxAtex = new XdevComboBox<>();
		this.comboBoxPTC = new XdevComboBox<>();
		this.textFieldPot = new TextFieldDouble();
		this.buttonHelpPotenza = new XdevButton();
		this.textFieldV = new TextFieldInteger();
		this.textFieldPoli = new TextFieldInteger();
		this.buttonFiltra1 = new XdevButton();
                this.labelPoli = new XdevLabel();
		this.tableMotori = new XdevTable<>();
                this.gridPoli = new XdevGridLayout();
                this.check2Poli = new XdevCheckBox();
                this.check4Poli = new XdevCheckBox();
                this.check6Poli = new XdevCheckBox();
                this.check8Poli = new XdevCheckBox();
                this.buttonCustom = new XdevButton();
	
		this.labelWarning.setValue("Warning");
                this.labelPoli.setValue("Poli");
                this.labelPTC.setValue("PTC");
		this.labelWarning.setContentMode(ContentMode.HTML);
		this.labelTitolo.setValue("Titolo");
		this.labelTitolo.setContentMode(ContentMode.HTML);
		this.gridLayoutFiltri.setMargin(new MarginInfo(false));
		this.buttonFiltra.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Find24.png"));
		this.buttonFiltra.setCaption("");
                this.labelDescrizione.setValue("Descrizione");
                this.labelIE.setValue("IE");
                this.labelAtex.setValue("Atex");
                this.labelKw.setValue("[Kw]");
		this.comboBoxClasse.setIcon(null);
		this.buttonHelpPotenza.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/help24.png"));
		this.buttonHelpPotenza.setCaption("");
		this.buttonFiltra1.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Find24.png"));
		this.buttonFiltra1.setCaption("");
		this.tableMotori.setStyleName("small striped");
                this.gridPoli.setColumns(11);
                this.gridPoli.setRows(2);
                this.buttonCustom.setCaption("Custom");
                
                check2Poli.setCaption("2");
                check4Poli.setCaption("4");
                check6Poli.setCaption("6");
                check8Poli.setCaption("8");
                gridLayoutNuova.setColumns(11);
                gridLayoutNuova.setRows(2);
                buttonFiltra.setSizeFull();
                textFieldLikeDescrizione.setSizeFull();
                comboBoxClasse.setSizeFull();
                comboBoxAtex.setSizeFull();
                comboBoxPTC.setSizeFull();
                textFieldPot.setSizeFull();
                buttonHelpPotenza.setSizeFull();
                gridPoli.addComponent(labelDescrizione, 0, 0);
                gridPoli.addComponent(textFieldLikeDescrizione, 0, 1);
                gridPoli.addComponent(labelIE, 1, 0);
                gridPoli.addComponent(comboBoxClasse, 1, 1);
                gridPoli.addComponent(labelAtex, 2, 0);
                gridPoli.addComponent(comboBoxAtex, 2, 1);
                gridPoli.addComponent(labelPTC, 3, 0);
                gridPoli.addComponent(comboBoxPTC, 3, 1);
                gridPoli.addComponent(labelPoli, 4, 0, 7 ,0, Alignment.MIDDLE_CENTER);
                gridPoli.addComponent(check2Poli, 4, 1);
                gridPoli.addComponent(check4Poli, 5, 1);
                gridPoli.addComponent(check6Poli, 6, 1);
                gridPoli.addComponent(check8Poli, 7, 1);  
                gridPoli.addComponent(labelKw, 8, 0);
                gridPoli.addComponent(textFieldPot, 8, 1);   
                gridPoli.addComponent(buttonHelpPotenza, 9, 0, 9, 1);
                gridPoli.addComponent(buttonFiltra, 10, 0, 10, 1);
                gridPoli.addComponent(buttonCustom, 11, 0, 11, 1);
                gridPoli.setSizeFull();
		gridPoli.setColumnExpandRatios(120F,30F,30F,30F,5F,5F,5F,5F,10F,20F,20F);
//                gridPoli.setAutoFill(XdevGridLayout.AutoFill.BOTH);
                gridPoli.setSizeFull();
		gridLayoutNuova.setColumnExpandRatios(25F,130F,25F,35F,30F,20F,20F,25F,80F);
                final CustomComponent gridLayoutFiltri_vSpacer = new CustomComponent();
		gridLayoutFiltri_vSpacer.setWidth("1000px");
		this.gridLayoutNuova.addComponent(gridLayoutFiltri_vSpacer, 0, 1, 10, 1);
		this.gridLayoutNuova.setRowExpandRatio(1, 1.0F);
		
		this.labelWarning.setWidth(100, Unit.PERCENTAGE);
		this.labelWarning.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.labelWarning);
		this.verticalLayout.setComponentAlignment(this.labelWarning, Alignment.MIDDLE_CENTER);
		this.labelTitolo.setWidth(725, Unit.PIXELS);
		this.labelTitolo.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.labelTitolo);
		this.verticalLayout.setComponentAlignment(this.labelTitolo, Alignment.MIDDLE_CENTER);
		this.gridLayoutNuova.setSizeFull();
		this.verticalLayout.addComponent(this.gridPoli);
		this.tableMotori.setSizeFull();
		this.verticalLayout.addComponent(this.tableMotori);
		this.verticalLayout.setComponentAlignment(this.tableMotori, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableMotori, 100.0F);
		this.verticalLayout.setWidth("1000px");
		this.setContent(this.verticalLayout);
		this.setWidth(800, Unit.PIXELS);
		this.setHeight(100, Unit.PERCENTAGE);
                this.check2Poli.addValueChangeListener(event -> this.checkBoxPoli_valueChange(2));
                this.check4Poli.addValueChangeListener(event -> this.checkBoxPoli_valueChange(4));
                this.check6Poli.addValueChangeListener(event -> this.checkBoxPoli_valueChange(6));
                this.check8Poli.addValueChangeListener(event -> this.checkBoxPoli_valueChange(8));
		this.buttonFiltra.addClickListener(event -> this.buttonFiltra_buttonClick(event));
		this.buttonHelpPotenza.addClickListener(event -> this.buttonHelpPotenza_buttonClick(event));
		this.buttonFiltra1.addClickListener(event -> this.buttonFiltra1_buttonClick(event));
		this.buttonCustom.addClickListener(event -> this.buttonCustom_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
        private TextFieldInteger textFieldV, textFieldPoli;
	private XdevLabel labelWarning, labelTitolo, labelPoli, labelDescrizione, labelIE, labelAtex, labelPTC, labelKw;
	private XdevButton buttonFiltra, buttonHelpPotenza, buttonFiltra1, buttonCustom;
	private XdevTable<CustomComponent> tableMotori;
	private TextFieldDouble textFieldPot;
	private XdevComboBox<?> comboBoxClasse, comboBoxAtex, comboBoxPTC;
	private XdevGridLayout gridLayoutFiltri, gridLayoutNuova, gridPoli;
	private XdevTextField textFieldLikeCodice, textFieldLikeDescrizione;
	private XdevVerticalLayout verticalLayout;
        private XdevCheckBox check2Poli, check4Poli, check6Poli, check8Poli;
	// </generated-code>

    private String creaQueryMotoreMoro(Ventilatore VentilatoreCurrent, boolean AtexFan) {
        String query = "select * from MotoriMoro where PotkW >= " + Double.toString(PotenzaRichiestaW / 1000.);
        query += " and Freq = " + Integer.toString(VentilatoreCurrent.CAProgettazione.frequenza);
        if (AtexFan)
            query += " and isAtex = 1";
        else
            query += " and isAtex = 0";
        if (VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null)
            query += " and Grandezza = " + VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza + " and Poli = " + VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Poli;
        //servirà un else? Come lo scriviamo? TODO
        query += " and PotkW >= " + Double.toString(VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW);
        query += " and Freq = " + VentilatoreCurrent.CAProgettazione.frequenza;
        return query;
    }
}
