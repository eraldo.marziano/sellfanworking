/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.pannelli;

import cit.myjavalib.UtilityFisica.UtilityUnitaMisura;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.utility.UtilityTraduzioni;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import java.text.MessageFormat;
import java.util.ArrayList;

/**
 *
 * @author Eraldo Marziano
 */
public class PannelloCondizioniNuovo extends Panel{
    private VariabiliGlobali vGlobali;
    private XdevLabel TemperaturaLabel, TemperaturaUM, AltezzaLabel, AltezzaUM, UmiditaLabel,
            UmiditaUM, FrequenzaLabel, FrequenzaUM, DensitaStdLabel, DensitaStdUM, Densita00Label,
            Densita00Value, Densita00UM, DensitaReqLabel, DensitaReqValue, DensitaReqUM;
    private TextFieldDouble DensitaStdValue;
    private XdevGridLayout mainGrid;
    private TextFieldInteger TemperaturaValue, AltezzaValue, UmiditaValue;
    private XdevComboBox<CustomComponent> FrequenzaValue;
    public boolean singoloVentilatore = false;
    UtilityUnitaMisura misura;
    UtilityTraduzioni traduci;
    double densitaRichiesta;
    private boolean caricamentoIniziale = true;
    private XdevButton ResetButton;
    
    public PannelloCondizioniNuovo(String caption) {
        super(caption);
        initUI();
        FrequenzaValue.addItem(50);
        FrequenzaValue.addItem(60);
        TemperaturaValue.setConSegno(true);
        AltezzaValue.setConSegno(false);
        UmiditaValue.setEnabled(false);
    }
    
    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        vGlobali = variabiliGlobali;
        misura = vGlobali.utilityUnitaMisura;
        traduci = vGlobali.utilityTraduzioni;
    }
	
    public void Nazionalizza() {
        TemperaturaLabel.setValue(traduci.TraduciStringa("Temperatura"));
        AltezzaLabel.setValue(traduci.TraduciStringa("Altezza"));
        UmiditaLabel.setValue(traduci.TraduciStringa("Umidità"));
        FrequenzaLabel.setValue(traduci.TraduciStringa("Frequenza"));
    }
        
    public void initPannello() {
        Nazionalizza();
        UmiditaValue.setValue(vGlobali.catalogoCondizioniUmidita);
        if (singoloVentilatore) {
            TemperaturaValue.setValue(misura.fromCelsiusToXX(vGlobali.VentilatoreCurrent.UMTemperatura.index, vGlobali.VentilatoreCurrent.CAProgettazione.temperatura));
            AltezzaValue.setValue(misura.frommToXX(vGlobali.VentilatoreCurrent.CAProgettazione.altezza, vGlobali.VentilatoreCurrent.UMAltezza));
            DensitaStdValue.setValue(vGlobali.VentilatoreCurrent.CARiferimento.rho);
            TemperaturaUM.setValue(vGlobali.VentilatoreCurrent.UMTemperatura.simbolo);
            AltezzaUM.setValue(vGlobali.VentilatoreCurrent.UMAltezza.simbolo);
            FrequenzaValue.setValue(vGlobali.VentilatoreCurrent.CAProgettazione.frequenza);
        } else {
            TemperaturaValue.setValue(misura.fromCelsiusToXX(vGlobali.UMTemperaturaCorrente.index, vGlobali.CACatalogo.temperatura));
            AltezzaValue.setValue(misura.frommToXX(vGlobali.CACatalogo.altezza, vGlobali.UMAltezzaCorrente));
            DensitaStdValue.setValue(vGlobali.CA020Catalogo.rho);
            TemperaturaUM.setValue(vGlobali.UMTemperaturaCorrente.simbolo);
            AltezzaUM.setValue(vGlobali.UMAltezzaCorrente.simbolo);
            FrequenzaValue.setValue(vGlobali.CACatalogo.frequenza);
        }
        abilitaUmidita();
        calcolaDensitaCorrente();
//        if (caricamentoIniziale) {
//            FrequenzaValue.setValue(50);
//            caricamentoIniziale = false;
//        }
    }

    private void abilitaUmidita() {
        UmiditaValue.setEnabled(DensitaStdValue.getDoubleValue()==1.204);
    }
    
    public void azioneReset() {
        resetPreferenze();
    }
    
    public void azioneOK() {
        OKPreferenze();
    }
    
    private void OKPreferenze() {
        if (FrequenzaValue.getValue() == null) return;
        vGlobali.catalogoCondizioniAltezza = Double.toString(misura.fromXXTom(AltezzaValue.getDoubleValue(), vGlobali.UMAltezzaCorrente));
        vGlobali.CACatalogo.altezza = Double.parseDouble(vGlobali.catalogoCondizioniAltezza);
        vGlobali.catalogoCondizioniTemperatura = Double.toString(misura.fromXXtoCelsius(vGlobali.UMTemperaturaCorrente.index, TemperaturaValue.getDoubleValue()));
        vGlobali.CACatalogo.temperatura = Double.parseDouble(vGlobali.catalogoCondizioniTemperatura);
        int frequenzaIniziale = 50;
        if (vGlobali.VentilatoreCurrent != null) {
            frequenzaIniziale = vGlobali.VentilatoreCurrent.CAProgettazione.frequenza;
            vGlobali.VentilatoreCurrent.CAProgettazione.frequenza = vGlobali.CACatalogo.frequenza;
        }
        vGlobali.catalogoCondizioniFrequenza = FrequenzaValue.getValue().toString();
        vGlobali.CACatalogo.frequenza = Integer.parseInt(vGlobali.catalogoCondizioniFrequenza);
        vGlobali.catalogoCondizioniUmidita = UmiditaValue.getIntegerValue();
        vGlobali.CACatalogo.umidita = vGlobali.catalogoCondizioniUmidita;
        calcolaDensitaCorrente();
        vGlobali.CACatalogo.rho = densitaRichiesta;
        int frequenzaFinale = Integer.parseInt(FrequenzaValue.getValue().toString());
        if (singoloVentilatore) {
            if (vGlobali.VentilatoreCurrent == null)
                return;
            cambiaSingoloVentilatore(vGlobali.VentilatoreCurrent, frequenzaIniziale, frequenzaFinale);
        } else {
            for (Ventilatore v : vGlobali.ElencoVentilatoriFromCatalogo) {
               cambiaSingoloVentilatore(v, frequenzaIniziale, frequenzaFinale); 
        }
	}
    }
    
    private void resetPreferenze() {
        vGlobali.CACatalogo.temperatura = 20.0;
        vGlobali.CACatalogo.altezza = 0;
        vGlobali.CACatalogo.rho = 1.204;
        TemperaturaValue.setValue(20.0);
        AltezzaValue.setValue(0);
        DensitaStdValue.setValue(1.204);
        UmiditaValue.setValue(0);
        FrequenzaValue.setValue(50);
	calcolaDensitaCorrente();
    }
    
    private void cambiaSingoloVentilatore(final Ventilatore ventilatore, final int frequenzaIniziale, final int frequenzaFinale) {
        ventilatore.CAProgettazione.frequenza = frequenzaFinale;
        ventilatore.CAProgettazione.altezza = misura.fromXXTom(AltezzaValue.getIntegerValue(), vGlobali.UMAltezzaCorrente);
        ventilatore.CAProgettazione.temperatura = misura.fromXXtoCelsius(vGlobali.UMTemperaturaCorrente.index, TemperaturaValue.getIntegerValue());
        ventilatore.CAProgettazione.rho = densitaRichiesta;
        ventilatore.CARiferimento.rho = DensitaStdValue.getDoubleValue();
        vGlobali.utilityCliente.cambiaVentilatoreHz(ventilatore, frequenzaIniziale, frequenzaFinale);
    }
   
    
    private void calcolaDensitaCorrente() {
        String LabelStd = MessageFormat.format("{0} 20 C° {1} 0 m {2}", traduci.TraduciStringa("Densità fluido a"), traduci.TraduciStringa("e"), traduci.TraduciStringa("S.L.M."));
        String Label00 = MessageFormat.format("{0} 0 C° {1} 0 m {2}", traduci.TraduciStringa("Densità fluido a"), traduci.TraduciStringa("e"), traduci.TraduciStringa("S.L.M."));
        String LabelReq = MessageFormat.format("{0} {1} C° {2} {3} m {4}", 
                traduci.TraduciStringa("Densità fluido a"), 
                TemperaturaValue.getIntegerValue(),
                traduci.TraduciStringa("e"), 
                AltezzaValue.getIntegerValue(),
                traduci.TraduciStringa("S.L.M."));
        double temperaturaCelsius = misura.fromXXtoCelsius(vGlobali.UMTemperaturaCorrente.index, TemperaturaValue.getIntegerValue());
        double altezzaMetri = misura.fromXXTom(AltezzaValue.getIntegerValue(), vGlobali.UMAltezzaCorrente);
        double densita00 = vGlobali.utilityFisica.calcolaDensitaFuido(DensitaStdValue.getDoubleValue(), 0, 0, 0., UmiditaValue.getDoubleValue());
        densitaRichiesta = vGlobali.utilityFisica.calcolaDensitaFuido(DensitaStdValue.getDoubleValue(), temperaturaCelsius, altezzaMetri, 0., UmiditaValue.getDoubleValue());
        vGlobali.fmtNd.setMinimumFractionDigits(3);
        vGlobali.fmtNd.setMaximumFractionDigits(3);
        DensitaStdLabel.setValue(LabelStd);
        Densita00Label.setValue(Label00);
        Densita00Value.setValue(vGlobali.fmtNd.format(densita00));
        DensitaReqLabel.setValue(LabelReq);
        DensitaReqValue.setValue(vGlobali.fmtNd.format(densitaRichiesta));
    }
    
    private void initUI() {
        TemperaturaLabel = new XdevLabel("Temperatura");
        TemperaturaValue = new TextFieldInteger();
        TemperaturaUM = new XdevLabel("C");
        AltezzaLabel = new XdevLabel("Altezza");
        AltezzaValue = new TextFieldInteger();
        AltezzaUM = new XdevLabel("m");
        UmiditaLabel = new XdevLabel("Umidità");
        UmiditaValue = new TextFieldInteger();
        UmiditaUM = new XdevLabel("%");
        FrequenzaLabel = new XdevLabel("Frequenza");
        FrequenzaValue = new XdevComboBox<>();
        FrequenzaUM = new XdevLabel("Hz");
        DensitaStdLabel = new XdevLabel("DensitaStandard");
        DensitaStdValue = new TextFieldDouble();
        DensitaStdUM = new XdevLabel("Kg/m<sup>3</sup>", ContentMode.HTML);
        Densita00Label = new XdevLabel("Densita00");
        Densita00Value = new XdevLabel("1250.00");
        Densita00UM = new XdevLabel("Kg/m<sup>3</sup>", ContentMode.HTML);
        DensitaReqLabel = new XdevLabel("DensitaReq");
        DensitaReqValue = new XdevLabel("1250.00");
        DensitaReqUM = new XdevLabel("Kg/m<sup>3</sup>", ContentMode.HTML);
        ResetButton = new XdevButton("Reset");
        
        TemperaturaLabel.setSizeFull();
        TemperaturaValue.setWidth(80, Unit.PIXELS);
        TemperaturaValue.setHeight(100, Unit.PERCENTAGE);
        TemperaturaUM.setSizeFull();
        AltezzaLabel.setSizeFull();
        AltezzaValue.setSizeFull();
        AltezzaUM.setSizeFull();
        UmiditaLabel.setSizeFull();
        UmiditaValue.setSizeFull();
        UmiditaUM.setSizeFull();
        FrequenzaLabel.setSizeFull();
        FrequenzaValue.setSizeFull();
        FrequenzaUM.setSizeFull();
        DensitaStdLabel.setSizeFull();
        DensitaStdValue.setSizeFull();
        DensitaStdUM.setSizeFull();
        Densita00Label.setSizeFull();
        Densita00Value.setSizeFull();
        Densita00UM.setSizeFull();
        DensitaReqLabel.setSizeFull();
        DensitaReqValue.setSizeFull();
        DensitaReqUM.setSizeFull();
        ResetButton.setSizeFull();
        
        mainGrid = new XdevGridLayout(3, 8);
        mainGrid.addComponent(TemperaturaLabel, 0, 0, Alignment.MIDDLE_RIGHT);
        mainGrid.addComponent(TemperaturaValue, 1, 0, Alignment.MIDDLE_CENTER);
        mainGrid.addComponent(TemperaturaUM   , 2, 0, Alignment.MIDDLE_LEFT);
        mainGrid.addComponent(AltezzaLabel    , 0, 1, Alignment.MIDDLE_RIGHT);
        mainGrid.addComponent(AltezzaValue    , 1, 1, Alignment.MIDDLE_CENTER);
        mainGrid.addComponent(AltezzaUM       , 2, 1, Alignment.MIDDLE_LEFT);
        mainGrid.addComponent(UmiditaLabel    , 0, 2, Alignment.MIDDLE_RIGHT);
        mainGrid.addComponent(UmiditaValue    , 1, 2, Alignment.MIDDLE_CENTER);
        mainGrid.addComponent(UmiditaUM       , 2, 2, Alignment.MIDDLE_LEFT);
        mainGrid.addComponent(FrequenzaLabel  , 0, 3, Alignment.MIDDLE_RIGHT);
        mainGrid.addComponent(FrequenzaValue  , 1, 3, Alignment.MIDDLE_CENTER);
        mainGrid.addComponent(FrequenzaUM     , 2, 3, Alignment.MIDDLE_LEFT);
        mainGrid.addComponent(DensitaStdLabel , 0, 4, Alignment.MIDDLE_RIGHT);
        mainGrid.addComponent(DensitaStdValue , 1, 4, Alignment.MIDDLE_CENTER);
        mainGrid.addComponent(DensitaStdUM    , 2, 4, Alignment.MIDDLE_LEFT);
        mainGrid.addComponent(Densita00Label  , 0, 5, Alignment.MIDDLE_RIGHT);
        mainGrid.addComponent(Densita00Value  , 1, 5, Alignment.MIDDLE_CENTER);
        mainGrid.addComponent(Densita00UM     , 2, 5, Alignment.MIDDLE_LEFT);
        mainGrid.addComponent(DensitaReqLabel , 0, 6, Alignment.MIDDLE_RIGHT);
        mainGrid.addComponent(DensitaReqValue , 1, 6, Alignment.MIDDLE_CENTER);
        mainGrid.addComponent(DensitaReqUM    , 2, 6, Alignment.MIDDLE_LEFT);
        mainGrid.addComponent(ResetButton     , 1, 7, 2, 7, Alignment.MIDDLE_CENTER);
        mainGrid.setColumnExpandRatios(4,2,1);
        mainGrid.setStyleName("Elenco");
        this.setContent(mainGrid);
        
        TemperaturaValue.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            calcolaDensitaCorrente();
        });
        AltezzaValue.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            calcolaDensitaCorrente();
        });
        UmiditaValue.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            if (UmiditaValue.getIntegerValue()>100)
                UmiditaValue.setValue(100);
            calcolaDensitaCorrente();
        });
        DensitaStdValue.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            abilitaUmidita();
            calcolaDensitaCorrente();
        });
        ResetButton.addClickListener(event -> resetPreferenze());
    }
}
