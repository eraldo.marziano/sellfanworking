package com.cit.sellfan.ui.pannelli;

import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.sellfan.classi.HelpRpm;

public class pannelloHelpRpm extends Panel {
	private VariabiliGlobali l_VG;
    private int rpmIndexSelezionati = -1;
    private HelpRpm helpRpm;

	/**
	 * 
	 */
	public pannelloHelpRpm() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void Nazionalizza() {
		this.labelRpmConsigliati.setValue("<b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Rpm consigliati") + "</b>");
	}
	
	public void initPannello(final HelpRpm helpRpmIn) {
		Nazionalizza();
	    this.rpmIndexSelezionati = -1;
	    this.helpRpm = helpRpmIn;
	    this.comboBoxRpm.setEnabled(false);
		this.comboBoxRpm.removeAllItems();
        this.l_VG.fmtNd.setMaximumFractionDigits(3);
        this.l_VG.fmtNd.setMinimumFractionDigits(3);
        for (int i=0 ; i<this.helpRpm.elencoRpmPossibili.size() ; i++) {
        	this.comboBoxRpm.addItem(Integer.toString(this.helpRpm.elencoRpmPossibili.get(i)) + " (" + this.l_VG.fmtNd.format(this.helpRpm.elencoRapportiPossibili.get(i)) + ")");
        }
        final Collection<?> coll = this.comboBoxRpm.getItemIds();
        if (coll.size() >= 1) {
			this.comboBoxRpm.setEnabled(true);
		}
	}
	
    public int getIndexRpmSelezionati() {
        return this.rpmIndexSelezionati;
    }
	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxRpm}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxRpm_valueChange(final Property.ValueChangeEvent event) {
		this.rpmIndexSelezionati = -1;
		if (this.comboBoxRpm.getValue() == null) {
			return;
		}
		try {
			final String str = event.getProperty().getValue().toString();
			final String strSpli[] = str.split(" ");
			final int rpm = Integer.parseInt(strSpli[0]);
			for (int i=0 ; i<this.helpRpm.elencoRpmPossibili.size() ; i++) {
				if (rpm == this.helpRpm.elencoRpmPossibili.get(i)) {
					this.rpmIndexSelezionati = i;
					break;
				}
			}
		} catch (final Exception e) {
			//Notification.show(e.toString());
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelRpmConsigliati = new XdevLabel();
		this.comboBoxRpm = new XdevComboBox<>();
	
		this.labelRpmConsigliati.setValue("<b>Rpm consigliati");
		this.labelRpmConsigliati.setContentMode(ContentMode.HTML);
	
		this.labelRpmConsigliati.setSizeUndefined();
		this.verticalLayout.addComponent(this.labelRpmConsigliati);
		this.verticalLayout.setComponentAlignment(this.labelRpmConsigliati, Alignment.MIDDLE_CENTER);
		this.comboBoxRpm.setSizeUndefined();
		this.verticalLayout.addComponent(this.comboBoxRpm);
		this.verticalLayout.setComponentAlignment(this.comboBoxRpm, Alignment.MIDDLE_CENTER);
		final CustomComponent verticalLayout_spacer = new CustomComponent();
		verticalLayout_spacer.setSizeFull();
		this.verticalLayout.addComponent(verticalLayout_spacer);
		this.verticalLayout.setExpandRatio(verticalLayout_spacer, 1.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.comboBoxRpm.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloHelpRpm.this.comboBoxRpm_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelRpmConsigliati;
	private XdevComboBox<?> comboBoxRpm;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>

}
