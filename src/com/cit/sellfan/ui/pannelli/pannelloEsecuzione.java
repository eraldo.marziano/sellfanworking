package com.cit.sellfan.ui.pannelli;

import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.sellfan.Costanti;

public class pannelloEsecuzione extends Panel {
	private VariabiliGlobali l_VG;
	@SuppressWarnings("unused")
	private boolean editEnabled;
    public String esecuzioniGiunto[] = {"E08"};
    public String esecuzioniCinghia[] = {"E09", "E12", "E18", "E19"};
    public String esecuzioniCinghiaPuleggia[] = { };
    public String esecuzioniNoInverter[] = { };
    public boolean puleggeAltoRendimentoVisible = false;
    public boolean inverterRendimentoVisible = false;

	/**
	 * 
	 */
	public pannelloEsecuzione() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void Nazionalizza() {
		this.buttonCambiaOrientamento.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cambia"));
		this.labelEsecuzione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
        this.panelERP.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Regolamento") + " (UE) N. 327/2011");
        this.checkBoxRegolamentoAttivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Regolamento attivato"));
        this.checkBoxTarget2015.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Anno di riferimento") + ": 2015");
        this.checkBoxGiunto.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Giunto"));
        this.checkBoxCinghieBassoRendimento.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cinghie trapezoidali (Standard)"));
        this.checkBoxCinghieAltoRendimento.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cinghie e pulegge dentate"));
        this.checkBoxInverter.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Inverter"));
        this.panelERP.setVisible(this.l_VG.currentUserSaaS.idClienteIndex == 4);
        this.buttonAngoloMeno.setVisible(this.l_VG.utilityCliente.isTabVisible("AccessoriAngolo"));
        this.buttonAngoloPiu.setVisible(this.l_VG.utilityCliente.isTabVisible("AccessoriAngolo"));
        this.labelAngolo.setVisible(this.l_VG.utilityCliente.isTabVisible("AccessoriAngolo"));
	}
	
	public void setVentilatoreCurrent() {
        this.l_VG.VentilatoreCurrent.campiCliente.trasmissioneAltoRendimento = false;
        this.l_VG.VentilatoreCurrent.campiCliente.trasmissioneBassoRendimento = !this.l_VG.VentilatoreCurrent.campiCliente.trasmissioneAltoRendimento;
        this.l_VG.VentilatoreCurrent.campiCliente.inverterPresente = this.l_VG.gestioneAccessori.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "INV");
		final String esecuzione = this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione;
		this.editEnabled = false;
		Nazionalizza();
		this.labelAngolo.setValue("<html><center><b>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
		this.labelOrientamento.setValue("<html><center><b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Orientamento") + " " + this.l_VG.VentilatoreCurrent.selezioneCorrente.Orientamento + "</html>");
		this.comboBoxEsecuzioni.setEnabled(false);
		this.comboBoxEsecuzioni.removeAllItems();
        for (int i=0 ; i<this.l_VG.VentilatoreCurrent.Esecuzioni.length ; i++) {
        	if (!this.l_VG.VentilatoreCurrent.Esecuzioni[i].equals("-")) {
        		this.comboBoxEsecuzioni.addItem(this.l_VG.VentilatoreCurrent.Esecuzioni[i]);
        	}
        }
        this.comboBoxEsecuzioni.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
        @SuppressWarnings("rawtypes")
		final Collection coll = this.comboBoxEsecuzioni.getItemIds();
        this.comboBoxEsecuzioni.setEnabled(coll.size() > 1 && this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo);
        this.labelDescrizione.setValue("<html><center>" + this.l_VG.getDescrizioneEsecuzione(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione) + "</html>");
        if (!this.l_VG.utilityCliente.isERP327disattivabile(this.l_VG.VentilatoreCurrent)) {
        	this.checkBoxRegolamentoAttivo.setEnabled(false);
            this.l_VG.VentilatoreCurrent.campiCliente.ERP327Abilitato = true;
        } else {
        	this.checkBoxRegolamentoAttivo.setEnabled(true);
        }
        this.checkBoxRegolamentoAttivo.setValue(this.l_VG.VentilatoreCurrent.campiCliente.ERP327Abilitato);
        this.checkBoxGiunto.setVisible(false);
        this.checkBoxCinghieBassoRendimento.setVisible(false);
        this.checkBoxCinghieAltoRendimento.setVisible(false);
        this.checkBoxGiunto.setEnabled(false);
        this.l_VG.VentilatoreCurrent.campiCliente.giuntoPresente = false;
        for (int i=0 ; i<this.esecuzioniGiunto.length ; i++) {
            if (esecuzione.equals(this.esecuzioniGiunto[i])) {
                this.l_VG.VentilatoreCurrent.campiCliente.giuntoPresente = true;
                this.checkBoxGiunto.setVisible(true);
                this.checkBoxGiunto.setValue(true);
            }
        }
        for (int i=0 ; i<this.esecuzioniCinghia.length ; i++) {
            if (esecuzione.equals(this.esecuzioniCinghia[i])) {
            	this.checkBoxCinghieBassoRendimento.setVisible(true);
            	this.checkBoxCinghieBassoRendimento.setEnabled(false);
            	this.checkBoxCinghieAltoRendimento.setVisible(this.puleggeAltoRendimentoVisible);
            	this.checkBoxCinghieAltoRendimento.setEnabled(false);
                if (this.l_VG.VentilatoreCurrent.campiCliente.trasmissioneBassoRendimento) {
                	this.checkBoxCinghieBassoRendimento.setValue(true);
                } else {
                	this.checkBoxCinghieBassoRendimento.setValue(true);
                }
                this.checkBoxCinghieBassoRendimento.setEnabled(true);
                this.checkBoxCinghieAltoRendimento.setEnabled(true);
            }
        }
        this.checkBoxInverter.setVisible(false);
        boolean visibile = true;
        for (int i=0 ; i<this.esecuzioniNoInverter.length ; i++) {
            if (esecuzione.equals(this.esecuzioniNoInverter[i])) {
                visibile = false;
            }
        }
        this.checkBoxInverter.setVisible(visibile && this.inverterRendimentoVisible);
        if (!visibile) {
            this.l_VG.VentilatoreCurrent.campiCliente.inverterPresente = false;
        }
        this.checkBoxInverter.setValue(this.l_VG.VentilatoreCurrent.campiCliente.inverterPresente);
        this.editEnabled = true;
	}
	
	public String getEsecuzione() {
		return this.comboBoxEsecuzioni.getValue().toString();
	}
	
	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxEsecuzioni}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxEsecuzioni_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxEsecuzioni.isEnabled()) {
			return;
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.panel = new XdevPanel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.labelEsecuzione = new XdevLabel();
		this.comboBoxEsecuzioni = new XdevComboBox<>();
		this.labelDescrizione = new XdevLabel();
		this.panel2 = new XdevPanel();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.buttonAngoloMeno = new XdevButton();
		this.labelAngolo = new XdevLabel();
		this.buttonAngoloPiu = new XdevButton();
		this.labelOrientamento = new XdevLabel();
		this.buttonCambiaOrientamento = new XdevButton();
		this.panelERP = new XdevPanel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.checkBoxRegolamentoAttivo = new XdevCheckBox();
		this.checkBoxTarget2015 = new XdevCheckBox();
		this.checkBoxGiunto = new XdevCheckBox();
		this.checkBoxCinghieBassoRendimento = new XdevCheckBox();
		this.checkBoxCinghieAltoRendimento = new XdevCheckBox();
		this.checkBoxInverter = new XdevCheckBox();
	
		this.panel.setTabIndex(0);
		this.verticalLayout4.setMargin(new MarginInfo(false));
		this.horizontalLayout3.setMargin(new MarginInfo(false));
		this.labelEsecuzione.setValue("Esecuzione");
		this.labelDescrizione.setValue("Descrizione");
		this.labelDescrizione.setContentMode(ContentMode.HTML);
		this.panel2.setTabIndex(0);
		this.verticalLayout3.setMargin(new MarginInfo(false));
		this.horizontalLayout2.setMargin(new MarginInfo(false));
		this.buttonAngoloMeno.setCaption("<<");
		this.buttonAngoloMeno.setStyleName("small");
		this.labelAngolo.setValue("0");
		this.labelAngolo.setContentMode(ContentMode.HTML);
		this.buttonAngoloPiu.setCaption(">>");
		this.buttonAngoloPiu.setStyleName("ThemeIcons");
		this.labelOrientamento.setValue("RD");
		this.labelOrientamento.setContentMode(ContentMode.HTML);
		this.buttonCambiaOrientamento.setCaption("Cambia");
		this.buttonCambiaOrientamento.setStyleName("small");
		this.panelERP.setCaption("327");
		this.panelERP.setTabIndex(0);
		this.checkBoxRegolamentoAttivo.setCaption("attivo");
		this.checkBoxTarget2015.setCaption("2015");
		this.checkBoxTarget2015.setEnabled(false);
		this.checkBoxTarget2015.setValue(true);
		this.checkBoxGiunto.setCaption("giunto");
		this.checkBoxCinghieBassoRendimento.setCaption("trapeziodali");
		this.checkBoxCinghieAltoRendimento.setCaption("pulegge");
		this.checkBoxInverter.setCaption("inverter");
	
		this.labelEsecuzione.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.labelEsecuzione);
		this.horizontalLayout3.setComponentAlignment(this.labelEsecuzione, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout3.setExpandRatio(this.labelEsecuzione, 10.0F);
		this.comboBoxEsecuzioni.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxEsecuzioni.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.comboBoxEsecuzioni);
		this.horizontalLayout3.setComponentAlignment(this.comboBoxEsecuzioni, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.comboBoxEsecuzioni, 10.0F);
		this.horizontalLayout3.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.horizontalLayout3);
		this.verticalLayout4.setExpandRatio(this.horizontalLayout3, 10.0F);
		this.labelDescrizione.setWidth(100, Unit.PERCENTAGE);
		this.labelDescrizione.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.labelDescrizione);
		this.verticalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout4.setHeight(-1, Unit.PIXELS);
		this.panel.setContent(this.verticalLayout4);
		this.buttonAngoloMeno.setWidth(100, Unit.PERCENTAGE);
		this.buttonAngoloMeno.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.buttonAngoloMeno);
		this.horizontalLayout2.setComponentAlignment(this.buttonAngoloMeno, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.buttonAngoloMeno, 10.0F);
		this.labelAngolo.setWidth(100, Unit.PERCENTAGE);
		this.labelAngolo.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.labelAngolo);
		this.horizontalLayout2.setComponentAlignment(this.labelAngolo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.labelAngolo, 10.0F);
		this.buttonAngoloPiu.setWidth(100, Unit.PERCENTAGE);
		this.buttonAngoloPiu.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.buttonAngoloPiu);
		this.horizontalLayout2.setComponentAlignment(this.buttonAngoloPiu, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.buttonAngoloPiu, 10.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout2);
		this.verticalLayout3.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.labelOrientamento.setWidth(100, Unit.PERCENTAGE);
		this.labelOrientamento.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.labelOrientamento);
		this.verticalLayout3.setComponentAlignment(this.labelOrientamento, Alignment.MIDDLE_CENTER);
		this.buttonCambiaOrientamento.setSizeUndefined();
		this.verticalLayout3.addComponent(this.buttonCambiaOrientamento);
		this.verticalLayout3.setComponentAlignment(this.buttonCambiaOrientamento, Alignment.MIDDLE_CENTER);
		final CustomComponent verticalLayout3_spacer = new CustomComponent();
		verticalLayout3_spacer.setSizeFull();
		this.verticalLayout3.addComponent(verticalLayout3_spacer);
		this.verticalLayout3.setExpandRatio(verticalLayout3_spacer, 1.0F);
		this.verticalLayout3.setSizeFull();
		this.panel2.setContent(this.verticalLayout3);
		this.checkBoxRegolamentoAttivo.setSizeUndefined();
		this.verticalLayout2.addComponent(this.checkBoxRegolamentoAttivo);
		this.verticalLayout2.setComponentAlignment(this.checkBoxRegolamentoAttivo, Alignment.MIDDLE_CENTER);
		this.checkBoxTarget2015.setSizeUndefined();
		this.verticalLayout2.addComponent(this.checkBoxTarget2015);
		this.verticalLayout2.setComponentAlignment(this.checkBoxTarget2015, Alignment.MIDDLE_CENTER);
		this.checkBoxGiunto.setSizeUndefined();
		this.verticalLayout2.addComponent(this.checkBoxGiunto);
		this.verticalLayout2.setComponentAlignment(this.checkBoxGiunto, Alignment.MIDDLE_CENTER);
		this.checkBoxCinghieBassoRendimento.setSizeUndefined();
		this.verticalLayout2.addComponent(this.checkBoxCinghieBassoRendimento);
		this.verticalLayout2.setComponentAlignment(this.checkBoxCinghieBassoRendimento, Alignment.MIDDLE_CENTER);
		this.checkBoxCinghieAltoRendimento.setSizeUndefined();
		this.verticalLayout2.addComponent(this.checkBoxCinghieAltoRendimento);
		this.verticalLayout2.setComponentAlignment(this.checkBoxCinghieAltoRendimento, Alignment.MIDDLE_CENTER);
		this.checkBoxInverter.setSizeUndefined();
		this.verticalLayout2.addComponent(this.checkBoxInverter);
		this.verticalLayout2.setComponentAlignment(this.checkBoxInverter, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout2.setHeight(-1, Unit.PIXELS);
		this.panelERP.setContent(this.verticalLayout2);
		this.panel.setWidth(100, Unit.PERCENTAGE);
		this.panel.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panel);
		this.verticalLayout.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
		this.panel2.setWidth(100, Unit.PERCENTAGE);
		this.panel2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panel2);
		this.verticalLayout.setComponentAlignment(this.panel2, Alignment.MIDDLE_CENTER);
		this.panelERP.setWidth(100, Unit.PERCENTAGE);
		this.panelERP.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panelERP);
		this.verticalLayout.setExpandRatio(this.panelERP, 10.0F);
		this.verticalLayout.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout.setHeight(-1, Unit.PIXELS);
		this.setContent(this.verticalLayout);
		this.setWidth(400, Unit.PIXELS);
		this.setHeight(420, Unit.PIXELS);
	
		this.comboBoxEsecuzioni.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloEsecuzione.this.comboBoxEsecuzioni_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelEsecuzione, labelDescrizione, labelAngolo, labelOrientamento;
	private XdevButton buttonAngoloMeno, buttonAngoloPiu, buttonCambiaOrientamento;
	private XdevHorizontalLayout horizontalLayout3, horizontalLayout2;
	private XdevPanel panel, panel2, panelERP;
	private XdevCheckBox checkBoxRegolamentoAttivo, checkBoxTarget2015, checkBoxGiunto, checkBoxCinghieBassoRendimento,
			checkBoxCinghieAltoRendimento, checkBoxInverter;
	private XdevVerticalLayout verticalLayout, verticalLayout4, verticalLayout3, verticalLayout2;
	private XdevComboBox<CustomComponent> comboBoxEsecuzioni;
	// </generated-code>

}
