package com.cit.sellfan.ui.pannelli;

import java.util.ArrayList;
import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.Serie;
import cit.sellfan.classi.titoli.StringheUNI;

public class pannelloSelezioneSerieDefault extends Panel {
	private VariabiliGlobali l_VG;
	private final Container containerSerie = new IndexedContainer();
	private boolean editEnabled;

	public pannelloSelezioneSerieDefault() {
		super();
		this.initUI();
		this.containerSerie.removeAllItems();
		this.containerSerie.addContainerProperty("Selected", XdevCheckBox.class, true);
		this.containerSerie.addContainerProperty("Serie", String.class, "");
		this.containerSerie.addContainerProperty("QMin", Double.class, "");
		this.containerSerie.addContainerProperty("QMax", Double.class, "");
		this.containerSerie.addContainerProperty("PTMin", Double.class, "");
		this.containerSerie.addContainerProperty("PTMax", Double.class, "");
		this.containerSerie.addContainerProperty("Description", String.class, "");
		this.tableSerie.setContainerDataSource(this.containerSerie);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		Nazionalizza();
	}
	
	public boolean getTRFlag() {
		return this.checkBoxTR.getValue();
	}
	
	public void Nazionalizza() {
		this.labelTitolo.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Selezionare le serie di ventilatori che si desidera includere") + "</html>");
		this.checkBoxDA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Direttamente accoppiati"));
		this.checkBoxTR.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Trasmissione"));
		this.buttonTutte.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Seleziona tutte"));
		this.buttonNessuna.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Deseleziona tutte"));
		this.tableSerie.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableSerie.setColumnHeader("Serie", this.l_VG.utilityTraduzioni.TraduciStringa("Serie"));
		this.tableSerie.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
//		labelMemo.setValue("<htnl><b><font color='blue'>" + l_VG.utilityTraduzioni.TraduciStringa("Serie selezionate"+":") + " 0</html>");
	}
	
	public void init(final boolean TRFlag, final ArrayList<Serie> elenco) {
		this.editEnabled = false;
		this.tableSerie.setColumnHeader("QMin", "<html>" + StringheUNI.PORTATA + " min " + this.l_VG.UMPortataCorrente.simbolo + "</html>");
		this.tableSerie.setColumnHeader("QMax", "<html>" + StringheUNI.PORTATA + " max " + this.l_VG.UMPortataCorrente.simbolo + "</html>");
		this.tableSerie.setColumnHeader("PTMin", "<html>" + StringheUNI.PRESSIONETOTALE + " min " + this.l_VG.UMPressioneCorrente.simbolo + "</html>");
		this.tableSerie.setColumnHeader("PTMax", "<html>" + StringheUNI.PRESSIONETOTALE + " max " + this.l_VG.UMPressioneCorrente.simbolo + "</html>");
		this.checkBoxTR.setValue(TRFlag);
		this.checkBoxDA.setValue(!TRFlag);
		buildTabellaSerie(TRFlag, elenco);
		contaSerieSelezionate();
		this.editEnabled = true;
	}
	
        public void generaTabella(ArrayList<Serie> elenco) {
            editEnabled = false;
            this.tableSerie.setColumnHeader("QMin", "<html>" + StringheUNI.PORTATA + " min " + this.l_VG.UMPortataCorrente.simbolo + "</html>");
            this.tableSerie.setColumnHeader("QMax", "<html>" + StringheUNI.PORTATA + " max " + this.l_VG.UMPortataCorrente.simbolo + "</html>");
            this.tableSerie.setColumnHeader("PTMin", "<html>" + StringheUNI.PRESSIONETOTALE + " min " + this.l_VG.UMPressioneCorrente.simbolo + "</html>");
            this.tableSerie.setColumnHeader("PTMax", "<html>" + StringheUNI.PRESSIONETOTALE + " max " + this.l_VG.UMPressioneCorrente.simbolo + "</html>");
            this.tableSerie.removeAllItems();
            for(Serie serie : elenco) {
                final Object obj[] = new Object[7];
                buldObject(obj, serie);
                tableSerie.addItem(obj);
            }
                    
        contaSerieSelezionate();
                    
            
            editEnabled = true;
        }
        
        
        
	public void init(final boolean TRFlag) {
		this.editEnabled = false;
		this.tableSerie.setColumnHeader("QMin", "<html>" + StringheUNI.PORTATA + " min " + this.l_VG.UMPortataCorrente.simbolo + "</html>");
		this.tableSerie.setColumnHeader("QMax", "<html>" + StringheUNI.PORTATA + " max " + this.l_VG.UMPortataCorrente.simbolo + "</html>");
		this.tableSerie.setColumnHeader("PTMin", "<html>" + StringheUNI.PRESSIONETOTALE + " min " + this.l_VG.UMPressioneCorrente.simbolo + "</html>");
		this.tableSerie.setColumnHeader("PTMax", "<html>" + StringheUNI.PRESSIONETOTALE + " max " + this.l_VG.UMPressioneCorrente.simbolo + "</html>");
		this.checkBoxTR.setValue(TRFlag);
		this.checkBoxDA.setValue(!TRFlag);
		buildTabellaSerie(TRFlag);
		this.editEnabled = true;
		contaSerieSelezionate();
	}
	
	public void aggiornaSerieUtilizzabiliRicerca() {
        for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
            this.l_VG.ElencoSerie.get(i).UtilizzabileRicerca = false;
        }
		final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
		for(final Object id : Ventilatoriids){
			final Item item = this.containerSerie.getItem(id);
			final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
			if (cb.getValue()) {
				final String serie = item.getItemProperty("Serie").getValue().toString();
				for (int j=0 ; j<this.l_VG.ElencoSerie.size() ; j++) {
					if (this.l_VG.ElencoSerie.get(j).SerieTrans.equals(serie)) {
						this.l_VG.ElencoSerie.get(j).UtilizzabileRicerca = true;
						break;
					}
				}
			}
		}
	}
	
	public void hideTRDA() {
		this.checkBoxTR.setVisible(false);
		this.checkBoxDA.setVisible(false);
	}
	
    
    private void buildTabellaSerie(final Boolean TRFlag, final ArrayList<Serie> elenco) {
        this.editEnabled = false;
        try {
            this.tableSerie.removeAllItems();//resetta anche le selection
        } catch (final Exception e) { }
        for (int i=0 ; i<elenco.size() ; i++) {
            final String itemID = Integer.toString(i);
            final Object obj[] = new Object[7];
            obj[1] = "???";
            for (int j=0 ; j<this.l_VG.ElencoSerie.size() ; j++) {
                if (this.l_VG.ElencoSerieDisponibili.get(j) && this.l_VG.ElencoSerie.get(j).Serie.equals(elenco.get(i).Serie)) {
                    final Serie l_s = this.l_VG.ElencoSerie.get(j);
                    if (TRFlag) {
                        if (l_s.Trasmissione < 1) {
                            continue;
                        }
                    } else {
                        if (l_s.DirettamenteAccoppiato < 1) {
                            continue;
                        }
                    }
                    buldObject(obj, l_s);
                }
            }
            if (!obj[1].equals("???")) {
                try {
                this.tableSerie.addItem(obj, itemID);
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                }
            }
        }
        contaSerieSelezionate();
    this.editEnabled = true;
    }
	
	private void buildTabellaSerie(final Boolean TRFlag) {
		this.editEnabled = false;
		try {
			this.tableSerie.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
		}
		for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
			final String itemID = Integer.toString(i);
			final Object obj[] = new Object[7];
			final Serie l_s = this.l_VG.ElencoSerie.get(i);
			if (TRFlag) {
				if (l_s.Trasmissione == 1) {
					buldObject(obj, l_s);
					this.tableSerie.addItem(obj, itemID);
				}
			} else {
				if (l_s.DirettamenteAccoppiato == 1) {
					buldObject(obj, l_s);
					this.tableSerie.addItem(obj, itemID);
				}
			}
		}
		contaSerieSelezionate();
		this.editEnabled = true;
	}
	
	private void buldObject(final Object obj[], final Serie l_s) {
		final XdevCheckBox cb = new XdevCheckBox();
		cb.setValue(l_s.UtilizzabileRicerca);
		cb.addValueChangeListener(new ValueChangeListener() {
			@Override
				public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
					if (!pannelloSelezioneSerieDefault.this.editEnabled) {
						return;
					}
					pannelloSelezioneSerieDefault.this.editEnabled = false;
					contaSerieSelezionate();
					pannelloSelezioneSerieDefault.this.editEnabled = true;
				}
	    });
		obj[0] = cb;
		obj[1] = l_s.SerieTrans;
		obj[2] = Math.floor(this.l_VG.utilityUnitaMisura.fromm3hToXX(l_s.QMinm3h, this.l_VG.UMPortataCorrente) * 100.) / 100.;
		obj[3] = Math.floor(this.l_VG.utilityUnitaMisura.fromm3hToXX(l_s.QMaxm3h, this.l_VG.UMPortataCorrente) * 100.) / 100.;
		obj[4] = Math.floor(this.l_VG.utilityUnitaMisura.fromPaToXX(l_s.PTMinPa, this.l_VG.UMPressioneCorrente) * 100.) / 100.;
		obj[5] = Math.floor(this.l_VG.utilityUnitaMisura.fromPaToXX(l_s.PTMaxPa, this.l_VG.UMPressioneCorrente) * 100.) / 100.;
		obj[6] = this.l_VG.utilityTraduzioni.TraduciStringa(l_s.DescrizioneTrans);
	}
	
	public void contaSerieSelezionate() {
		final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
		int nSerieSelezionate = 0;
		this.tableSerie.setEnabled(false);
		for(final Object id : Ventilatoriids){
			final Item item = this.containerSerie.getItem(id);
			final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
			if (cb.getValue()) {
				nSerieSelezionate++;
			}
		}
		this.tableSerie.setEnabled(true);
		this.labelMemo.setValue("<htnl><b><font color='blue'>" + this.l_VG.utilityTraduzioni.TraduciStringa("Serie selezionate"+":") + " " + Integer.toString(nSerieSelezionate) + "</html>");
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTutte}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTutte_buttonClick(final Button.ClickEvent event) {
		final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
		this.editEnabled = false;
		this.tableSerie.setEnabled(false);
		for(final Object id : Ventilatoriids){
			final Item item = this.containerSerie.getItem(id);
			final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
			cb.setValue(true);
		}
		this.tableSerie.setEnabled(true);
		this.editEnabled = true;
		contaSerieSelezionate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonNessuna}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonNessuna_buttonClick(final Button.ClickEvent event) {
		final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
		this.editEnabled = false;
		this.tableSerie.setEnabled(false);
		for(final Object id : Ventilatoriids){
			final Item item = this.containerSerie.getItem(id);
			final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
			cb.setValue(false);
		}
		this.tableSerie.setEnabled(true);
		this.editEnabled = true;
		contaSerieSelezionate();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelTitolo = new XdevLabel();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.checkBoxDA = new XdevCheckBox();
		this.checkBoxTR = new XdevCheckBox();
		this.tableSerie = new XdevTable<>();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.labelMemo = new XdevLabel();
		this.buttonTutte = new XdevButton();
		this.buttonNessuna = new XdevButton();
	
		this.verticalLayout.setMargin(new MarginInfo(false, true, false, true));
		this.labelTitolo.setValue("titolo");
		this.labelTitolo.setContentMode(ContentMode.HTML);
		this.horizontalLayout.setMargin(new MarginInfo(false, true, false, true));
		this.checkBoxDA.setCaption("DA");
		this.checkBoxDA.setEnabled(false);
		this.checkBoxTR.setCaption("TR");
		this.checkBoxTR.setEnabled(false);
		this.labelMemo.setValue("memo");
		this.labelMemo.setContentMode(ContentMode.HTML);
		this.buttonTutte.setCaption("Tutte");
		this.buttonNessuna.setCaption("nessuna");
	
		this.checkBoxDA.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxDA.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.checkBoxDA);
		this.horizontalLayout.setComponentAlignment(this.checkBoxDA, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.checkBoxDA, 10.0F);
		this.checkBoxTR.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTR.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.checkBoxTR);
		this.horizontalLayout.setComponentAlignment(this.checkBoxTR, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.checkBoxTR, 10.0F);
		this.labelMemo.setWidth(100, Unit.PERCENTAGE);
		this.labelMemo.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.labelMemo);
		this.horizontalLayout2.setComponentAlignment(this.labelMemo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.labelMemo, 10.0F);
		this.buttonTutte.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonTutte);
		this.horizontalLayout2.setComponentAlignment(this.buttonTutte, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout2.setExpandRatio(this.buttonTutte, 10.0F);
		this.buttonNessuna.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonNessuna);
		this.horizontalLayout2.setComponentAlignment(this.buttonNessuna, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout2.setExpandRatio(this.buttonNessuna, 10.0F);
		this.labelTitolo.setWidth(100, Unit.PERCENTAGE);
		this.labelTitolo.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.labelTitolo);
		this.verticalLayout.setComponentAlignment(this.labelTitolo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.tableSerie.setSizeFull();
		this.verticalLayout.addComponent(this.tableSerie);
		this.verticalLayout.setComponentAlignment(this.tableSerie, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableSerie, 100.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setWidth(60, Unit.PERCENTAGE);
		this.setHeight(100, Unit.PERCENTAGE);
	
		this.buttonTutte.addClickListener(event -> this.buttonTutte_buttonClick(event));
		this.buttonNessuna.addClickListener(event -> this.buttonNessuna_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelTitolo, labelMemo;
	private XdevButton buttonTutte, buttonNessuna;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2;
	private XdevTable<?> tableSerie;
	private XdevCheckBox checkBoxDA, checkBoxTR;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>


}
