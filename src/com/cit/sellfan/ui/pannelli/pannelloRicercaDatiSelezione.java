package com.cit.sellfan.ui.pannelli;

import java.util.Iterator;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.ProgressView;
import com.cit.sellfan.ui.view.SearchView;
import com.cit.sellfan.ui.view.cliente04.Cliente04pannelloSelezioneSerie;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.sellfan.classi.CondizioniAmbientali;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.titoli.StringheUNI;
import de.steinwedel.messagebox.MessageBox;

public class pannelloRicercaDatiSelezione extends Panel {
	private VariabiliGlobali l_VG;
	private SearchView searchView;
	private boolean editEnabled;
	private Object pannelloSerie = null;
	private boolean TRFlagOld;
	private boolean AssialiFlagOld;
	private boolean CentrifughiFlagOld;
	private MessageBox msgBox;
	private boolean needLoadElencoSerie = false;
        private boolean TRFlag;
	/**
	 * 
	 */
	public pannelloRicercaDatiSelezione() {
		super();
		this.initUI();
		this.textFieldPortataKgh.setMaximumFractionDigits(2);
		this.textFieldNPortata.setMaximumFractionDigits(2);
		this.textAltezzaSLM.setConSegno(true);
		this.textFieldTemperatura.setConSegno(true);
		this.comboBoxFrequenza.addItem("50");
		this.comboBoxFrequenza.addItem("60");
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		this.searchView = (SearchView)this.l_VG.ElencoView[this.l_VG.SearchViewIndex];
		this.editEnabled = false;
		this.comboBoxAltezzaSLMUM.setEnabled(false);
		for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.size() ; i++) {
			this.comboBoxAltezzaSLMUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(i).simbolo));
		}
		this.comboBoxAltezzaSLMUM.setEnabled(true);
		this.comboBoxTemperaturaUM.setEnabled(false);
		for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.size() ; i++) {
			this.comboBoxTemperaturaUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.get(i).simbolo));
		}
		this.comboBoxTemperaturaUM.setEnabled(true);
		this.comboBoxPortataUM.setEnabled(false);
                this.comboBoxPortataUM.setCaptionAsHtml(true);
                for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.size() ; i++) {
                    this.comboBoxPortataUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.get(i).simbolo));
		}
		this.comboBoxPortataUM.setEnabled(true);
		this.comboBoxPressioneUM.setEnabled(false);
		for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.size() ; i++) {
			this.comboBoxPressioneUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.get(i).simbolo));
		}
		this.comboBoxPressioneUM.setEnabled(true);
	}
	
	public void resetPannello() {
		buttonReset_buttonClick(null);
		//Notification.show("setUMFromDefaultIndex");
	}
	public void init() {
		init(false);
	}
	
	public void init(final boolean returning) {
		this.editEnabled = false;
		this.checkBoxDA.setEnabled(false);
		this.checkBoxTR.setEnabled(false);
		this.checkBoxOutlet.setEnabled(false);
		this.checkBoxInlet.setEnabled(false);
		this.checkBoxAriaSecca.setEnabled(false);
		this.checkBoxAriaUmida.setEnabled(false);
		this.checkBoxEsplicito.setEnabled(false);
		this.checkBoxMiscela.setEnabled(false);
		this.checkBoxAssiali.setEnabled(false);
		this.checkBoxCentrifughi.setEnabled(false);
		this.checkBoxTotale.setEnabled(false);
		this.checkBoxStatica.setEnabled(false);
		this.checkBoxDA.setValue(this.l_VG.searchIsDiretto);
		this.checkBoxTR.setValue(!this.l_VG.searchIsDiretto);
		
		this.checkBoxAriaSecca.setValue(true);
		this.checkBoxAriaUmida.setValue(false);
		this.checkBoxEsplicito.setValue(false);
		this.checkBoxMiscela.setValue(false);
		this.checkBoxAssiali.setValue(!this.l_VG.searchIsCentrifughi);
		this.checkBoxCentrifughi.setValue(this.l_VG.searchIsCentrifughi);
		
		//this.labelPortata.setValue(StringheUNI.PORTATA);
		this.labelNPortata.setValue("NQ");
		if (this.l_VG.ricercaAspirazione) {
			this.checkBoxOutlet.setValue(false);
			this.checkBoxInlet.setValue(true);
		} else {
			this.checkBoxOutlet.setValue(true);
			this.checkBoxInlet.setValue(false);
		}
		if (this.l_VG.ricercaTotale) {
			this.checkBoxTotale.setValue(true);
			this.checkBoxStatica.setValue(false);
			this.labelPressione.setValue(StringheUNI.PRESSIONETOTALE);
			} else {
				this.checkBoxTotale.setValue(false);
				this.checkBoxStatica.setValue(true);
				this.labelPressione.setValue(StringheUNI.PRESSIONESTATICA);
			}
		this.checkBoxDA.setEnabled(true);
		this.checkBoxTR.setEnabled(true);
		this.checkBoxOutlet.setEnabled(true);
		this.checkBoxInlet.setEnabled(true);
		this.checkBoxAriaSecca.setEnabled(true);
		this.checkBoxAriaUmida.setEnabled(true);
		this.checkBoxEsplicito.setEnabled(true);
		this.checkBoxMiscela.setEnabled(true);
		checkBoxAriaSecca_valueChange(null);
		this.checkBoxAssiali.setEnabled(true);
		this.checkBoxCentrifughi.setEnabled(true);
		this.checkBoxTotale.setEnabled(true);
		this.checkBoxStatica.setEnabled(true);
		this.comboBoxPressioneUM.setEnabled(false);
		this.comboBoxPortataUM.setEnabled(false);
		this.comboBoxAltezzaSLMUM.setEnabled(false);
		this.comboBoxTemperaturaUM.setEnabled(false);
		this.comboBoxPressioneUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMPressioneCorrente.simbolo));
        this.comboBoxFrequenza.setValue(Integer.toString(this.l_VG.CARicerca.frequenza));
		
		/*if (returning) {
			this.l_VG.UMPressioneCorrente = this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.get(0);
			}
		if (returning) {
			this.l_VG.UMPortataCorrente = this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.get(0);
			}*/
		this.comboBoxPortataUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMPortataCorrente.simbolo));
		this.comboBoxAltezzaSLMUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMAltezzaCorrente.simbolo));
		this.comboBoxTemperaturaUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMTemperaturaCorrente.simbolo));
		this.comboBoxPressioneUM.setEnabled(true);
		this.comboBoxPortataUM.setEnabled(true);
		this.comboBoxAltezzaSLMUM.setEnabled(true);
		this.comboBoxTemperaturaUM.setEnabled(true);
		
		this.textAltezzaSLM.setMaximumFractionDigits(this.l_VG.UMAltezzaCorrente.nDecimaliStampa);
		this.textAltezzaSLM.setValue(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.CARicerca.altezza, this.l_VG.UMAltezzaCorrente));
		this.textFieldTemperatura.setMaximumFractionDigits(this.l_VG.UMTemperaturaCorrente.nDecimaliStampa);
		this.textFieldTemperatura.setValue(this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, this.l_VG.CARicerca.temperatura));
		this.textFieldUmidita.setValue(0);
		this.textFieldPortata.setMaximumFractionDigits(this.l_VG.UMPortataCorrente.nDecimaliStampa);
		if (this.l_VG.SelezioneDati.PortataRichiestam3h == 0.) {
			this.textFieldPortata.setValue("");
		}
		this.textFieldPressione.setMaximumFractionDigits(this.l_VG.UMPressioneCorrente.nDecimaliStampa);
		if (this.l_VG.SelezioneDati.PressioneRichiestaPa == 0.) {
			this.textFieldPressione.setValue("");
		}
		aggiornaNormalPortataAndKgh(true);
		
        //buildComboEsecuzioni();
        this.panelUmidita.setVisible(false);
        String str = "<html>ρ<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("Fluido") + "</sub> " + this.l_VG.utilityTraduzioni.TraduciStringa("a") + " ";
        str += Integer.toString((int)this.l_VG.CA020Ricerca.temperatura) + " [°C] " + this.l_VG.utilityTraduzioni.TraduciStringa("e") + " " + Integer.toString((int)this.l_VG.CA020Ricerca.altezza);
        str += " [m] " + "</html>";
        //str += " [m] " + l_VG.utilityTraduzioni.TraduciStringa("S.L.M.") + "</html>";
        this.labelDensita200.setValue(str);
        this.labelDensitaUM.setValue(StringheUNI.kgalmcubo);
        this.textFieldDensita200.setEnabled(false);
		this.textFieldDensita200.setValue(this.l_VG.CA020Ricerca.rho);
        buildDensitaCorrente();
        this.buttonMiscela.setEnabled(false);
        
        this.TRFlagOld = this.checkBoxTR.getValue();
        this.AssialiFlagOld = this.checkBoxAssiali.getValue();
        this.CentrifughiFlagOld = this.checkBoxCentrifughi.getValue();
        this.editEnabled = true;
	}
	
	private void comboBoxFrequenza_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}
	
	public void Nazionalizza() {
		this.labelAlezzaSLM.setValue("<html><b>h<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("S.L.M.") + "</sub></html>");
		this.labelTemperatura.setValue("<html><b>T<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("Fluido") + "</sub>:</html>");
		this.checkBoxAriaSecca.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aria secca"));
		this.checkBoxAriaUmida.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aria umida"));
		this.checkBoxEsplicito.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Esplicito"));
		this.checkBoxMiscela.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Miscela"));
		this.buttonMiscela.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Miscela"));
		this.checkBoxAssiali.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Assiali"));
		this.checkBoxCentrifughi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Centrifughi"));
		this.checkBoxTR.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Trasmissione"));
		this.checkBoxDA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatori diretti"));
                this.labelEsec.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
//		labelPortata.setValue(l_VG.utilityTraduzioni.TraduciStringa("Portata"));
//		labelNPortata.setValue(l_VG.utilityTraduzioni.TraduciStringa("Normal portata"));
//		labelPressione.setValue(l_VG.utilityTraduzioni.TraduciStringa("Pressione"));
		this.checkBoxTotale.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("totale"));
		this.checkBoxStatica.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("statica"));
		this.checkBoxInlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("aspirazione"));
		this.checkBoxOutlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("mandata"));
		this.buttonSelezioneSerie.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Seleziona Serie"));
		this.buttonReset.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset"));
		this.buttonCerca.setCaption(System.lineSeparator() + this.l_VG.utilityTraduzioni.TraduciStringa("Cerca!"));
		
		String str = "<html>ρ<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("Fluido") + "</sub> " + this.l_VG.utilityTraduzioni.TraduciStringa("a") + " ";;
		str += Integer.toString((int)this.l_VG.CA020Ricerca.temperatura) + " [°C] " + this.l_VG.utilityTraduzioni.TraduciStringa("e") + " " + Integer.toString((int)this.l_VG.CA020Ricerca.altezza);
		str += " [m] " + "</html>";
		//str += " [m] " + l_VG.utilityTraduzioni.TraduciStringa("S.L.M.") + "</html>";
		this.labelDensita200.setValue(str);
	}
	
	public void getDati() {
                this.l_VG.SelezioneDati.esecuzioneSelezionata = "";
                    if (this.comboBoxEsecuzioni.getValue() != null) {
                            if (!this.comboBoxEsecuzioni.getValue().toString().equals("Default")) {
                                    this.l_VG.SelezioneDati.esecuzioneSelezionata = this.comboBoxEsecuzioni.getValue().toString();
                            } else {
                                    this.l_VG.SelezioneDati.esecuzioneSelezionata = "";
                            }
                    }
                this.l_VG.SelezioneDati.umidita = this.textFieldUmidita.getIntegerValue();
		this.l_VG.SelezioneDati.CategorieSerieIndex = 0;
		this.l_VG.SelezioneDati.MaterialeIndex = 0;
		this.l_VG.SelezioneDati.aperturaSerrandaDapo = 100.;
		this.l_VG.SelezioneDati.UMPortata = (UnitaMisura)this.l_VG.UMPortataCorrente.clone();
		this.l_VG.SelezioneDati.UMPressione = (UnitaMisura)this.l_VG.UMPressioneCorrente.clone();
		this.l_VG.SelezioneDati.UMPotenza = (UnitaMisura)this.l_VG.UMPotenzaCorrente.clone();
		this.l_VG.SelezioneDati.UMAltezza = (UnitaMisura)this.l_VG.UMAltezzaCorrente.clone();
		this.l_VG.SelezioneDati.UMTemperatura = (UnitaMisura)this.l_VG.UMTemperaturaCorrente.clone();
		if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
			final UtilityCliente uc04 = this.l_VG.utilityCliente;
	        uc04.AssialiSelezionatiFlag = this.checkBoxAssiali.getValue();
	        uc04.CentrifughiSelezionatiFlag = this.checkBoxCentrifughi.getValue();
		}
        this.l_VG.SelezioneDati.Trasmissione = this.checkBoxTR.getValue();
		this.l_VG.SelezioneDati.pressioneStaticaFlag = this.checkBoxStatica.getValue();
		this.l_VG.SelezioneDati.PressioneMandataFlag = this.checkBoxOutlet.getValue();
		if (this.comboBoxFrequenza.getValue() != null) {
			this.l_VG.CARicerca.frequenza = Integer.parseInt(this.comboBoxFrequenza.getValue().toString());
		}
        this.l_VG.SelezioneDati.PotSonoraFlag = true;
        this.l_VG.SelezioneDati.PresSonoraMax = this.l_VG.SelezioneDati.PotSonoraMax;
	}

        public void buildComboEsecuzioni(final boolean TR, boolean ass) {
		this.editEnabled = false;
		this.TRFlag = TR;
		this.comboBoxEsecuzioni.removeAllItems();
		this.comboBoxEsecuzioni.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Default"));
                if (TR && ass) {
                    this.comboBoxEsecuzioni.addItem("E01");
                    this.comboBoxEsecuzioni.addItem("E09");
                } else
		if (TR) {
			    if (this.l_VG.pannelloRicercaEsecuzioniTR1 != null) {
                                for (int i=0 ; i<this.l_VG.pannelloRicercaEsecuzioniTR1.length ; i++) {
                                    if (this.l_VG.debugFlag) {
                                        Notification.show(Integer.toString(i)+"  "+this.l_VG.pannelloRicercaEsecuzioniTR1[i]);
                                    }
                                    this.comboBoxEsecuzioni.addItem(this.l_VG.pannelloRicercaEsecuzioniTR1[i]);
                                }
                            } 
                            if (this.l_VG.pannelloRicercaEsecuzioniTR2 != null) {
                                for (int i=0 ; i<this.l_VG.pannelloRicercaEsecuzioniTR2.length ; i++) {
                                    if (this.l_VG.debugFlag) {
                                        Notification.show(Integer.toString(i)+"  "+this.l_VG.pannelloRicercaEsecuzioniTR2[i]);
                                    }
                                    this.comboBoxEsecuzioni.addItem(this.l_VG.pannelloRicercaEsecuzioniTR2[i]);
                                }
                            }
		} else {
            if (this.l_VG.pannelloRicercaEsecuzioniDA != null) {
                for (int i=0 ; i<this.l_VG.pannelloRicercaEsecuzioniDA.length ; i++) {
                	if (this.l_VG.debugFlag) {
						Notification.show(Integer.toString(i)+"  "+this.l_VG.pannelloRicercaEsecuzioniDA[i]);
					}
                	this.comboBoxEsecuzioni.addItem(this.l_VG.pannelloRicercaEsecuzioniDA[i]);
                }
            }
		}
		this.comboBoxEsecuzioni.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Default"));
		this.editEnabled = true;
	}
        
	private void buildDensitaCorrente() {
        String str = "<html>ρ<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("Fluido") + "</sub> " + this.l_VG.utilityTraduzioni.TraduciStringa("a") + " ";
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.UMTemperaturaCorrente.nDecimaliStampa);
        str += this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, this.l_VG.CARicerca.temperatura));
        str += " " + this.l_VG.UMTemperaturaCorrente.simbolo + " " + this.l_VG.utilityTraduzioni.TraduciStringa("e") + " ";
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.UMAltezzaCorrente.nDecimaliStampa);
        str += this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.CARicerca.altezza, this.l_VG.UMAltezzaCorrente)) + " " + this.l_VG.UMAltezzaCorrente.simbolo;
        str += " " + "</html>";
        //str += " " + l_VG.utilityTraduzioni.TraduciStringa("S.L.M.") + "</html>";
        this.labelDensitaXX.setValue(str);
        if (this.textFieldUmidita.getIntegerValue() > 100) {
        	this.textFieldUmidita.setEnabled(false);
        	this.textFieldUmidita.setValue(100);
        	this.textFieldUmidita.setEnabled(true);
        }
        this.l_VG.CARicerca.rho = this.l_VG.utilityFisica.calcolaDensitaFuido(this.textFieldDensita200.getDoubleValue(), this.l_VG.CARicerca.temperatura, this.l_VG.CARicerca.altezza, 0., this.textFieldUmidita.getIntegerValue());
        this.l_VG.fmtNd.setMaximumFractionDigits(3);
        str = "<html><b>" + this.l_VG.fmtNd.format(this.l_VG.CARicerca.rho) + "</html>";
        this.labelDensitaXXVal.setValue(str);
	}
	
	private void aggiornaPortataAndNormalPortata() {
		this.textFieldPortata.setEnabled(false);
		this.textFieldNPortata.setEnabled(false);
		final double portatam3h = this.textFieldPortataKgh.getDoubleValue() / this.l_VG.CARicerca.rho;
        final double fattoreNm3h = this.l_VG.CARicerca.rho / this.l_VG.utilityFisica.getDensitaAria(0);
		if (portatam3h > 0.) {
			this.textFieldPortata.setValue(this.l_VG.utilityUnitaMisura.fromm3hToXX(portatam3h, this.l_VG.UMPortataCorrente));
			this.textFieldNPortata.setValue(portatam3h * fattoreNm3h);
			this.l_VG.SelezioneDati.PortataRichiestam3h = portatam3h;
			this.l_VG.SelezioneDati.NormalPortataRichiesta = portatam3h * fattoreNm3h;
		} else {
			this.textFieldPortata.setValue("");
			this.textFieldNPortata.setValue("");
			this.l_VG.SelezioneDati.PortataRichiestam3h = 0.;
			this.l_VG.SelezioneDati.NormalPortataRichiesta = 0.;
		}
		this.textFieldNPortata.setEnabled(true);
		this.textFieldPortata.setEnabled(true);
	}
	
	private void aggiornaNormalPortataAndKgh() {
		aggiornaNormalPortataAndKgh(false);
	}
	
	private void aggiornaNormalPortataAndKgh(final boolean returning) {
		this.textFieldNPortata.setEnabled(false);
        this.textFieldPortataKgh.setEnabled(false);
        if (!returning) {
        final double fattoreNm3h = this.l_VG.CARicerca.rho / this.l_VG.utilityFisica.getDensitaAria(0);
        final double portata = this.l_VG.utilityUnitaMisura.fromXXTom3h(this.textFieldPortata.getDoubleValue(), this.l_VG.UMPortataCorrente);
        if (portata > 0.) {
        	this.textFieldNPortata.setValue(portata * fattoreNm3h);
            this.l_VG.SelezioneDati.NormalPortataRichiesta = portata * fattoreNm3h;
        	this.textFieldPortataKgh.setValue(portata * this.l_VG.CARicerca.rho);
        } else {
        	this.textFieldNPortata.setValue("");
        	this.textFieldPortataKgh.setValue("");
            this.l_VG.SelezioneDati.NormalPortataRichiesta = 0.;
        }
        }
        this.textFieldNPortata.setEnabled(true);
        this.textFieldPortataKgh.setEnabled(true);
	}
	
	private void aggiornaPortataAndKgh() {
		this.textFieldPortata.setEnabled(false);
        this.textFieldPortataKgh.setEnabled(false);
        final double fattoreNm3h = this.l_VG.CARicerca.rho / this.l_VG.utilityFisica.getDensitaAria(0);
        final double portatam3h = this.textFieldNPortata.getDoubleValue() / fattoreNm3h;
        this.textFieldPortata.setMaximumFractionDigits(this.l_VG.UMPortataCorrente.nDecimaliStampa);
        if (portatam3h > 0.) {
        	this.textFieldPortata.setValue(this.l_VG.utilityUnitaMisura.fromm3hToXX(portatam3h, this.l_VG.UMPortataCorrente));
        	this.textFieldPortataKgh.setValue(portatam3h * this.l_VG.CARicerca.rho);
        	this.l_VG.SelezioneDati.PortataRichiestam3h = portatam3h;
        } else {
        	this.textFieldPortata.setValue("");
        	this.textFieldPortataKgh.setValue("");
        	this.l_VG.SelezioneDati.PortataRichiestam3h = 0.;
        }
        this.textFieldPortata.setEnabled(true);
        this.textFieldPortataKgh.setEnabled(true);
	}
	
	public void buildComboEsecuzioni() {
            
		this.buildComboEsecuzioni(this.checkBoxTR.getValue(), this.checkBoxAssiali.getValue());
	}

	private boolean testFlag() {
		if (this.TRFlagOld != this.checkBoxTR.getValue()) {
			return false;
		}
		if (this.AssialiFlagOld != this.checkBoxAssiali.getValue()) {
			return false;
		}
		if (this.CentrifughiFlagOld != this.checkBoxCentrifughi.getValue()) {
			return false;
		}
		return true;
	}
	
	private void initPannelloSelezioneSerie() {
		if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
			if (this.pannelloSerie == null || !testFlag()) {
				for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
					this.l_VG.ElencoSerie.get(i).UtilizzabileRicerca = this.l_VG.ElencoSerieDisponibili.get(i);
				}
				this.pannelloSerie = new Cliente04pannelloSelezioneSerie();
				final Cliente04pannelloSelezioneSerie pan = (Cliente04pannelloSelezioneSerie)this.pannelloSerie;
				pan.setVariabiliGlobali(this.l_VG);
				pan.init(this.checkBoxTR.getValue(), this.checkBoxAssiali.getValue(), this.checkBoxCentrifughi.getValue());
			}
		} else {
			if (this.pannelloSerie == null || !testFlag()) {
				for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
					this.l_VG.ElencoSerie.get(i).UtilizzabileRicerca = this.l_VG.ElencoSerieDisponibili.get(i);
				}
				this.pannelloSerie = new pannelloSelezioneSerieDefault();
				final pannelloSelezioneSerieDefault pan = (pannelloSelezioneSerieDefault)this.pannelloSerie;
				pan.setVariabiliGlobali(this.l_VG);
				pan.init(this.checkBoxTR.getValue(), this.l_VG.ElencoSerie);
			}
		}
	    this.TRFlagOld = this.checkBoxTR.getValue();
	    this.AssialiFlagOld = this.checkBoxAssiali.getValue();
	    this.CentrifughiFlagOld = this.checkBoxCentrifughi.getValue();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textAltezzaSLM}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textAltezzaSLM_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.CARicerca.altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textAltezzaSLM.getDoubleValue(), this.l_VG.UMAltezzaCorrente);
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxAltezzaSLMUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxAltezzaSLMUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		if (this.comboBoxAltezzaSLMUM.getValue() == null) {
			return;
		}
		this.editEnabled = false;
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxAltezzaSLMUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textAltezzaSLM.getDoubleValue(), this.l_VG.UMAltezzaCorrente);
		this.l_VG.UMAltezzaCorrente = this.l_VG.utilityUnitaMisura.getLunghezzaUM(index);
		this.textAltezzaSLM.setEnabled(false);
		this.textAltezzaSLM.setMaximumFractionDigits(this.l_VG.UMAltezzaCorrente.nDecimaliStampa);
		this.textAltezzaSLM.setValue(this.l_VG.utilityUnitaMisura.frommToXX(altezza, this.l_VG.UMAltezzaCorrente));
		this.textAltezzaSLM.setEnabled(true);
		this.l_VG.CARicerca.altezza = altezza;
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldTemperatura}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldTemperatura_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.CARicerca.temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getDoubleValue());
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxTemperaturaUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxTemperaturaUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		if (this.comboBoxTemperaturaUM.getValue() == null) {
			return;
		}
		this.editEnabled = false;
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxTemperaturaUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getDoubleValue());
		this.l_VG.UMTemperaturaCorrente = this.l_VG.utilityUnitaMisura.getTemperaturaUM(index);
		this.textFieldTemperatura.setEnabled(false);
		this.textFieldTemperatura.setMaximumFractionDigits(this.l_VG.UMTemperaturaCorrente.nDecimaliStampa);
		this.textFieldTemperatura.setValue(this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, temperatura));
		this.textFieldTemperatura.setEnabled(true);
		this.l_VG.CARicerca.temperatura = temperatura;
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAriaSecca}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAriaSecca_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		if (!this.checkBoxAriaSecca.isEnabled()) {
			return;
		}
		this.checkBoxAriaUmida.setValue(!this.checkBoxAriaSecca.getValue());
		this.checkBoxEsplicito.setValue(!this.checkBoxAriaSecca.getValue());
		this.checkBoxMiscela.setValue(!this.checkBoxAriaSecca.getValue());
		this.buttonMiscela.setEnabled(false);
		this.panelUmidita.setVisible(false);
		this.buttonMiscela.setEnabled(false);
		this.textFieldUmidita.setEnabled(false);
		this.textFieldUmidita.setValue(0);
        this.l_VG.CA020Ricerca = new CondizioniAmbientali();
		this.textFieldDensita200.setEnabled(false);
		this.textFieldDensita200.setValue(this.l_VG.CA020Ricerca.rho);
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAriaUmida}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAriaUmida_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		if (!this.checkBoxAriaUmida.isEnabled()) {
			return;
		}
		this.checkBoxAriaSecca.setValue(!this.checkBoxAriaUmida.getValue());
		this.checkBoxEsplicito.setValue(!this.checkBoxAriaUmida.getValue());
		this.checkBoxMiscela.setValue(!this.checkBoxAriaUmida.getValue());
		this.panelUmidita.setVisible(true);
		this.buttonMiscela.setEnabled(false);
		this.textFieldUmidita.setEnabled(false);
		this.textFieldUmidita.setValue(0);
		this.textFieldUmidita.setEnabled(true);
        this.textFieldDensita200.setEnabled(false);
        this.l_VG.CA020Ricerca = new CondizioniAmbientali();
		this.textFieldDensita200.setValue(this.l_VG.CA020Ricerca.rho);
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxEsplicito}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxEsplicito_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		if (!this.checkBoxEsplicito.isEnabled()) {
			return;
		}
		this.checkBoxAriaSecca.setValue(!this.checkBoxEsplicito.getValue());
		this.checkBoxAriaUmida.setValue(!this.checkBoxEsplicito.getValue());
		this.checkBoxMiscela.setEnabled(true);
		this.checkBoxMiscela.setValue(!this.checkBoxEsplicito.getValue());
		this.panelUmidita.setVisible(false);
		this.buttonMiscela.setEnabled(false);
		this.textFieldDensita200.setEnabled(true);
		this.textFieldUmidita.setEnabled(false);
		this.textFieldUmidita.setValue(0);
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxMiscela}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxMiscela_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		if (!this.checkBoxMiscela.isEnabled()) {
			return;
		}
		this.checkBoxAriaSecca.setValue(!this.checkBoxMiscela.getValue());
		this.checkBoxAriaUmida.setValue(!this.checkBoxMiscela.getValue());
		this.checkBoxEsplicito.setValue(!this.checkBoxMiscela.getValue());
		this.checkBoxMiscela.setEnabled(true);
		this.textFieldDensita200.setEnabled(false);
		this.panelUmidita.setVisible(false);
		this.buttonMiscela.setEnabled(true);
        this.l_VG.CA020Ricerca = new CondizioniAmbientali();
		this.textFieldDensita200.setValue(this.l_VG.CA020Ricerca.rho);
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		for (int i=0 ; i<this.l_VG.ElencoGas.size() ; i++) {
			if (i == 0) {
				this.l_VG.ElencoGas.get(i).percentuale = 100.0;
			} else {
				this.l_VG.ElencoGas.get(i).percentuale = 0.0;
			}
		}
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldUmidita}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldUmidita_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonMiscela}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonMiscela_buttonClick(final Button.ClickEvent event) {
		this.searchView.reset();
		final pannelloMiscela pannello = new pannelloMiscela();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.init();
		this.msgBox = MessageBox.create();
		this.msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Calcolo Densità fluido"));
		this.msgBox.withMessage(pannello);
		this.msgBox.withAbortButton();
		this.msgBox.withOkButton(() -> {
			pannello.aggiornaPesiGas();
			this.editEnabled = false;
			this.textFieldDensita200.setValue(pannello.getDensitaCalcolata());
			this.l_VG.CA020Ricerca.rho = this.textFieldDensita200.getDoubleValue();
			buildDensitaCorrente();
			aggiornaNormalPortataAndKgh();
			this.searchView.reset();
			this.editEnabled = true;
			});
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldDensita200}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldDensita200_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.CA020Ricerca.rho = this.textFieldDensita200.getDoubleValue();
		buildDensitaCorrente();
		aggiornaNormalPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAssiali}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAssiali_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
                
		this.needLoadElencoSerie = true;
		this.editEnabled = false;
		this.checkBoxCentrifughi.setValue(!this.checkBoxAssiali.getValue());
		this.searchView.reset();
                buildComboEsecuzioni();
                this.searchView.pannelloFiltriSelezione.tolIsAss(this.checkBoxAssiali.getValue());
                if (this.checkBoxAssiali.getValue()) {
                    this.checkBoxInlet.setEnabled(false);
                    this.checkBoxOutlet.setEnabled(false);
                } else {
                    this.checkBoxInlet.setEnabled(true);
                    this.checkBoxOutlet.setEnabled(true);
                }
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCentrifughi}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCentrifughi_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		boolean flag = !this.checkBoxCentrifughi.getValue();
                this.searchView.pannelloFiltriSelezione.tolIsAss(flag);
                this.needLoadElencoSerie = true;
		this.editEnabled = false;
		this.checkBoxAssiali.setValue(!this.checkBoxCentrifughi.getValue());
		buildComboEsecuzioni();
		this.searchView.reset();
                if (this.checkBoxAssiali.getValue()) {
                    this.checkBoxInlet.setEnabled(false);
                    this.checkBoxOutlet.setEnabled(false);
                } else {
                    this.checkBoxInlet.setEnabled(true);
                    this.checkBoxOutlet.setEnabled(true);
                }
                this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTR}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTR_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxDA.setValue(!this.checkBoxTR.getValue());
		buildComboEsecuzioni();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxDA}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxDA_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxTR.setValue(!this.checkBoxDA.getValue());
		buildComboEsecuzioni();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldPortata}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPortata_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		final double portata = this.l_VG.utilityUnitaMisura.fromXXTom3h(this.textFieldPortata.getDoubleValue(), this.l_VG.UMPortataCorrente);
		this.l_VG.SelezioneDati.PortataRichiestam3h = portata;
		aggiornaNormalPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxPortataUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxPortataUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		if (this.comboBoxPortataUM.getValue() == null) {
			return;
		}
		this.editEnabled = false;
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxPortataUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double portata = this.l_VG.utilityUnitaMisura.fromXXTom3h(this.textFieldPortata.getDoubleValue(), this.l_VG.UMPortataCorrente);
		this.l_VG.UMPortataCorrente = this.l_VG.utilityUnitaMisura.getPortataUM(index);
		if (this.l_VG.debugFlag) {
			Notification.show(this.l_VG.UMPortataCorrente.simbolo);
		}
		this.textFieldPortata.setEnabled(false);
		this.textFieldPortata.setMaximumFractionDigits(this.l_VG.UMPortataCorrente.nDecimaliStampa);
		this.textFieldPortata.setValue(this.l_VG.utilityUnitaMisura.fromm3hToXX(portata, this.l_VG.UMPortataCorrente));
		this.textFieldPortata.setEnabled(true);
		this.l_VG.SelezioneDati.PortataRichiestam3h = portata;
		// this.l_VG.portataAttuale = l_VG.SelezioneDati.PortataRichiestam3h
		aggiornaNormalPortataAndKgh();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldPortataKgh}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPortataKgh_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		aggiornaPortataAndNormalPortata();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldNPortata}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNPortata_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.SelezioneDati.NormalPortataRichiesta = this.textFieldNPortata.getDoubleValue();
		aggiornaPortataAndKgh();
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldPressione}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPressione_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.SelezioneDati.PressioneRichiestaPa = this.l_VG.utilityUnitaMisura.fromXXToPa(this.textFieldPressione.getDoubleValue(), this.l_VG.UMPressioneCorrente);
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxPressioneUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxPressioneUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		if (this.comboBoxPressioneUM.getValue() == null) {
			return;
		}
		this.editEnabled = false;
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxPressioneUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double pressione = this.l_VG.utilityUnitaMisura.fromXXToPa(this.textFieldPressione.getDoubleValue(), this.l_VG.UMPressioneCorrente);
		this.l_VG.UMPressioneCorrente = this.l_VG.utilityUnitaMisura.getPressioneUM(index);
		this.textFieldPressione.setEnabled(false);
		this.textFieldPressione.setMaximumFractionDigits(this.l_VG.UMPressioneCorrente.nDecimaliStampa);
		this.textFieldPressione.setValue(this.l_VG.utilityUnitaMisura.fromPaToXX(pressione, this.l_VG.UMPressioneCorrente));
		this.textFieldPressione.setEnabled(true);
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTotale}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTotale_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaTotale = this.checkBoxTotale.getValue();
		this.checkBoxStatica.setValue(!this.checkBoxTotale.getValue());
		this.labelPressione.setValue(StringheUNI.PRESSIONETOTALE);
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxStatica}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxStatica_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaTotale = !this.checkBoxStatica.getValue();
		this.checkBoxTotale.setValue(!this.checkBoxStatica.getValue());
		this.labelPressione.setValue(StringheUNI.PRESSIONESTATICA);
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxInlet}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxInlet_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaAspirazione = true;
		this.checkBoxOutlet.setValue(!this.checkBoxInlet.getValue());
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxOutlet}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxOutlet_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaAspirazione = false;
		this.checkBoxInlet.setValue(!this.checkBoxOutlet.getValue());
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSelezioneSerie}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonSelezioneSerie_buttonClick(final Button.ClickEvent event) {
		initPannelloSelezioneSerie();
		this.msgBox = MessageBox.create();
		if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
			final Cliente04pannelloSelezioneSerie pan = (Cliente04pannelloSelezioneSerie)this.pannelloSerie;
			pan.Nazionalizza();
			this.msgBox.withMessage(pan);
			this.msgBox.withOkButton(() -> {
				this.editEnabled = false;
				this.searchView.reset();
				this.checkBoxTR.setValue(pan.getTRFlag());
				this.checkBoxDA.setValue(!pan.getTRFlag());
				this.checkBoxAssiali.setValue(pan.getAssialiFlag());
				this.checkBoxCentrifughi.setValue(pan.getCentrifughiFlag());
				pan.aggiornaSerieUtilizzabiliRicerca();
				this.editEnabled = true;
				});
		} else {
			final pannelloSelezioneSerieDefault pan = (pannelloSelezioneSerieDefault)this.pannelloSerie;
			pan.Nazionalizza();
			this.msgBox.withMessage(pan);
			this.msgBox.withOkButton(() -> {
				this.editEnabled = false;
				this.searchView.reset();
				this.checkBoxTR.setValue(pan.getTRFlag());
				this.checkBoxDA.setValue(!pan.getTRFlag());
				pan.aggiornaSerieUtilizzabiliRicerca();
				this.editEnabled = true;
				});
		}
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCerca}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCerca_buttonClick(final Button.ClickEvent event) {
		// set the search settings in VariabiliGlobali
		this.l_VG.searchIsCentrifughi = this.checkBoxCentrifughi.getValue();
		this.l_VG.searchIsDiretto = this.checkBoxDA.getValue();
		
		
		
        if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneSearch)) {
    		this.msgBox = MessageBox.createWarning();
    		this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Ricerche Disponibili Raggiunto"));
    		//msgBox.withAbortButton();
    		this.msgBox.withOkButton();
    		this.msgBox.open();
            return;
        }
        if (this.needLoadElencoSerie) {
        	this.needLoadElencoSerie = false;
        	initPannelloSelezioneSerie();
        	if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
        		final Cliente04pannelloSelezioneSerie pan = (Cliente04pannelloSelezioneSerie)this.pannelloSerie;
        		pan.aggiornaSerieUtilizzabiliRicerca();
        	} else {
        		final pannelloSelezioneSerieDefault pan = (pannelloSelezioneSerieDefault)this.pannelloSerie;
        		pan.aggiornaSerieUtilizzabiliRicerca();
        	}
        	
        }
        final Window popup = PopupWindow.For(new ProgressView(this.l_VG.utilityTraduzioni)).closable(false).draggable(false).resizable(false).modal(true).show();
        UI.getCurrent().push();
		this.searchView.reset();
		getDati();
		this.searchView.pannelloFiltriSelezione.getDati();
		this.l_VG.ElencoVentilatoriFromRicerca.clear();
		//searchView.reset();
		this.l_VG.eseguiRicerca();
		this.searchView.buildTableRisultati();
		popup.close();
		if (this.l_VG.ElencoVentilatoriFromRicerca.size() <= 0) {
	      	this.msgBox = MessageBox.createInfo();
	      	this.msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ricerca"));
	       	this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Nessun ventilatore selezionato"));
	       	this.msgBox.withOkButton();
	       	this.msgBox.open();
		}
	}
	private void comboBoxEsecuzioni_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonReset_buttonClick(final Button.ClickEvent event) {
		this.l_VG.setUMFromDefaultIndex();
        this.l_VG.CARicerca = (CondizioniAmbientali)this.l_VG.CACatalogo.clone();
        this.l_VG.CA020Ricerca = (CondizioniAmbientali)this.l_VG.CA020Catalogo.clone();
        this.l_VG.SelezioneDati.PortataRichiestam3h = 0.;
        this.l_VG.SelezioneDati.PressioneRichiestaPa = 0.;
        this.textFieldNPortata.setValue(0);
    	this.textFieldPortataKgh.setValue(0);
        this.l_VG.SelezioneDati.RendMinimo = 0;
        this.l_VG.CorrettorePotenzaMotoreCorrente = this.l_VG.CorrettorePotenzaMotoreDaConfig;
		this.searchView.reset();
		this.searchView.init();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout2 = new XdevVerticalLayout();
		this.gridLayout = new XdevGridLayout();
		this.labelAlezzaSLM = new XdevLabel();
		this.textAltezzaSLM = new TextFieldDouble();
		this.comboBoxAltezzaSLMUM = new XdevComboBox<>();
		this.labelTemperatura = new XdevLabel();
		this.textFieldTemperatura = new TextFieldDouble();
		this.comboBoxTemperaturaUM = new XdevComboBox<>();
		this.gridLayout8 = new XdevGridLayout();
		this.checkBoxAriaSecca = new XdevCheckBox();
		this.checkBoxAriaUmida = new XdevCheckBox();
		this.checkBoxEsplicito = new XdevCheckBox();
		this.checkBoxMiscela = new XdevCheckBox();
		this.panelUmidita = new XdevPanel();
		this.horizontalLayout3 = new XdevHorizontalLayout();
                this.labelEsec = new XdevLabel();
		this.labelUmidita = new XdevLabel();
		this.textFieldUmidita = new TextFieldInteger();
		this.labelUmiditaUM = new XdevLabel();
		this.buttonMiscela = new XdevButton();
		this.gridLayout2 = new XdevGridLayout();
		this.labelDensita200 = new XdevLabel();
                this.comboBoxEsecuzioni = new XdevComboBox<>();
		this.textFieldDensita200 = new TextFieldDouble();
		this.labelDensitaUM = new XdevLabel();
		this.labelDensitaXX = new XdevLabel();
		this.labelDensitaXXVal = new XdevLabel();
		this.gridLayout3 = new XdevGridLayout();
		this.checkBoxAssiali = new XdevCheckBox();
		this.checkBoxCentrifughi = new XdevCheckBox();
		this.checkBoxTR = new XdevCheckBox();
		this.checkBoxDA = new XdevCheckBox();
		this.gridLayout4 = new XdevGridLayout();
		this.labelPortata = new XdevLabel();
		this.labelPortataM = new XdevLabel();
		this.labelHz = new XdevLabel();
		this.textFieldPortata = new TextFieldDouble();
		this.comboBoxPortataUM = new XdevComboBox<>();
		this.textFieldPortataKgh = new TextFieldDouble();
		this.label = new XdevLabel();
		this.labelNPortata = new XdevLabel();
		this.textFieldNPortata = new TextFieldDouble();
		this.label11 = new XdevLabel();
		this.labelPressione = new XdevLabel();
		this.textFieldPressione = new TextFieldDouble();
		this.comboBoxPressioneUM = new XdevComboBox<>();
		this.comboBoxFrequenza = new XdevComboBox<>();
		this.gridLayout6 = new XdevGridLayout();
		this.checkBoxTotale = new XdevCheckBox();
		this.checkBoxStatica = new XdevCheckBox();
		this.checkBoxInlet = new XdevCheckBox();
		this.checkBoxOutlet = new XdevCheckBox();

		this.gridLayout5 = new XdevGridLayout();
		this.buttonSelezioneSerie = new XdevButton();
		this.buttonCerca = new XdevButton();
		this.buttonReset = new XdevButton();
                this.labelEsec.setValue("Esecuzione");
                this.labelEsec.setWidth("100%");
		this.labelHz.setValue("[Hz]");
		this.verticalLayout2.setMargin(new MarginInfo(true, true, false, true));
		this.gridLayout.setMargin(new MarginInfo(false));
		this.labelAlezzaSLM.setStyleName("align-right");
		this.labelAlezzaSLM.setValue("Altezza");
		this.labelAlezzaSLM.setContentMode(ContentMode.HTML);
		this.labelTemperatura.setStyleName("align-right");
		this.labelTemperatura.setValue("temp");
		this.labelTemperatura.setContentMode(ContentMode.HTML);
		this.gridLayout8.setMargin(new MarginInfo(false));
		this.checkBoxAriaSecca.setCaption("aria secca");
		this.checkBoxAriaUmida.setCaption("aria umida");
		this.checkBoxEsplicito.setCaption("esplicito");
		this.checkBoxMiscela.setCaption("miscela");
		this.panelUmidita.setTabIndex(0);
		this.panelUmidita.setStyleName("borderless");
		this.horizontalLayout3.setMargin(new MarginInfo(false));
		this.labelUmidita.setStyleName("align-right");
		this.labelUmidita.setValue("Umidità");
		this.labelUmiditaUM.setValue("%");
		this.buttonMiscela.setCaption("miscela");
		this.buttonMiscela.setCaptionAsHtml(true);
		this.gridLayout2.setMargin(new MarginInfo(false));
		this.labelDensita200.setStyleName("align-right");
		this.labelDensita200.setValue("den200");
		this.labelDensita200.setContentMode(ContentMode.HTML);
		this.labelDensitaUM.setValue("fissa");
		this.labelDensitaUM.setContentMode(ContentMode.HTML);
		this.labelDensitaXX.setStyleName("align-right");
		this.labelDensitaXX.setValue("denXX");
		this.labelDensitaXX.setContentMode(ContentMode.HTML);
		this.labelDensitaXXVal.setContentMode(ContentMode.HTML);
		this.gridLayout3.setMargin(new MarginInfo(false));
		this.checkBoxAssiali.setCaption("assiali");
		this.checkBoxAssiali.setCaptionAsHtml(true);
		this.checkBoxCentrifughi.setCaption("centrifughi");
		this.checkBoxCentrifughi.setCaptionAsHtml(true);
		this.checkBoxTR.setCaption("TR");
		this.checkBoxTR.setCaptionAsHtml(true);
		this.checkBoxDA.setCaption("DA");
		this.checkBoxDA.setCaptionAsHtml(true);
		this.gridLayout4.setMargin(new MarginInfo(false));
		this.labelPortata.setStyleName("align-right");
		this.labelPortata.setValue("Qv");
		this.labelPortata.setContentMode(ContentMode.HTML);
		this.labelPortataM.setStyleName("align-right");
		this.labelPortataM.setValue("Qm");
		this.labelPortataM.setContentMode(ContentMode.HTML);
		this.label.setValue("[kg/h]");
		this.labelNPortata.setStyleName("align-right");
		this.labelNPortata.setValue("NQ");
		this.labelNPortata.setContentMode(ContentMode.HTML);
		this.textFieldNPortata.setEnabled(false);
		this.label11.setValue("<html>[Nm<sup>3</sup>/h]</html>");
		this.label11.setContentMode(ContentMode.HTML);
		this.labelPressione.setStyleName("align-right");
		this.labelPressione.setValue("P");
		this.labelPressione.setContentMode(ContentMode.HTML);
		this.gridLayout6.setMargin(new MarginInfo(false));
		this.checkBoxTotale.setCaption("totale");
		this.checkBoxTotale.setCaptionAsHtml(true);
		this.checkBoxStatica.setCaption("statica");
		this.checkBoxStatica.setCaptionAsHtml(true);
		this.checkBoxInlet.setCaption("inlet");
		this.checkBoxInlet.setCaptionAsHtml(true);
		this.checkBoxOutlet.setCaption("outlet");
		this.checkBoxOutlet.setCaptionAsHtml(true);
		this.gridLayout5.setMargin(new MarginInfo(false));
		this.buttonSelezioneSerie.setCaption("serie");
		this.buttonSelezioneSerie.setStyleName("small");
		this.buttonCerca.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Find24.png"));
		this.buttonCerca.setCaption("cerca");
		this.buttonReset.setCaption("reset");
		this.buttonReset.setStyleName("small");
	
		this.gridLayout.setColumns(3);
		this.gridLayout.setRows(3);
		this.labelAlezzaSLM.setWidth(100, Unit.PERCENTAGE);
		this.labelAlezzaSLM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelAlezzaSLM, 0, 0);
		this.textAltezzaSLM.setWidth(100, Unit.PERCENTAGE);
		this.textAltezzaSLM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textAltezzaSLM, 1, 0);
		this.comboBoxAltezzaSLMUM.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxAltezzaSLMUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxAltezzaSLMUM, 2, 0);
		this.labelTemperatura.setWidth(100, Unit.PERCENTAGE);
		this.labelTemperatura.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelTemperatura, 0, 1);
		this.textFieldTemperatura.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTemperatura.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldTemperatura, 1, 1);
		this.comboBoxTemperaturaUM.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxTemperaturaUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxTemperaturaUM, 2, 1);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 2, 2, 2);
		this.gridLayout.setRowExpandRatio(2, 1.0F);
		this.labelUmidita.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.labelUmidita);
		this.horizontalLayout3.setComponentAlignment(this.labelUmidita, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.labelUmidita, 10.0F);
		this.textFieldUmidita.setWidth(100, Unit.PERCENTAGE);
		this.textFieldUmidita.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.textFieldUmidita);
		this.horizontalLayout3.setComponentAlignment(this.textFieldUmidita, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.textFieldUmidita, 10.0F);
		this.labelUmiditaUM.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.labelUmiditaUM);
		this.horizontalLayout3.setComponentAlignment(this.labelUmiditaUM, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.labelUmiditaUM, 10.0F);
		this.horizontalLayout3.setSizeFull();
		this.panelUmidita.setContent(this.horizontalLayout3);
		this.gridLayout8.setColumns(2);
		this.gridLayout8.setRows(4);
		this.checkBoxAriaSecca.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxAriaSecca.setHeight(-1, Unit.PIXELS);
                this.gridLayout8.addComponent(this.checkBoxAriaSecca, 0, 0);
		this.checkBoxAriaUmida.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxAriaUmida.setHeight(-1, Unit.PIXELS);
		this.gridLayout8.addComponent(this.checkBoxAriaUmida, 0, 1);
		this.checkBoxEsplicito.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxEsplicito.setHeight(-1, Unit.PIXELS);
		this.gridLayout8.addComponent(this.checkBoxEsplicito, 1, 0);
		this.checkBoxMiscela.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxMiscela.setHeight(-1, Unit.PIXELS);
		this.gridLayout8.addComponent(this.checkBoxMiscela, 1, 1);
		this.panelUmidita.setSizeFull();
		this.gridLayout8.addComponent(this.panelUmidita, 0, 2);
		this.buttonMiscela.setSizeUndefined();
		this.gridLayout8.addComponent(this.buttonMiscela, 1, 2);
		this.gridLayout8.setColumnExpandRatio(0, 40.0F);
		this.gridLayout8.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout8_vSpacer = new CustomComponent();
		gridLayout8_vSpacer.setSizeFull();
		this.gridLayout8.addComponent(gridLayout8_vSpacer, 0, 3, 1, 3);
		this.gridLayout8.setRowExpandRatio(3, 1.0F);
		this.gridLayout2.setColumns(3);
		this.gridLayout2.setRows(4);
		
		this.labelDensita200.setWidth(100, Unit.PERCENTAGE);
		this.labelDensita200.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.labelDensita200, 0, 0);
		this.textFieldDensita200.setWidth(100, Unit.PERCENTAGE);
		this.textFieldDensita200.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldDensita200, 1, 0);
		this.labelDensitaUM.setWidth(100, Unit.PERCENTAGE);
		this.labelDensitaUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.labelDensitaUM, 2, 0);
		this.labelDensitaXX.setWidth(100, Unit.PERCENTAGE);
		this.labelDensitaXX.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.labelDensitaXX, 0, 1);
		this.labelDensitaXXVal.setSizeFull();
		this.gridLayout2.addComponent(this.labelDensitaXXVal, 1, 1);
		this.gridLayout2.setColumnExpandRatio(0, 30.0F);
		this.gridLayout2.setColumnExpandRatio(1, 10.0F);
		this.gridLayout2.setColumnExpandRatio(2, 8.0F);
		final CustomComponent gridLayout2_vSpacer = new CustomComponent();
		gridLayout2_vSpacer.setSizeFull();
		this.gridLayout2.addComponent(gridLayout2_vSpacer, 0, 2, 2, 2);
		this.gridLayout2.setRowExpandRatio(2, 1.0F);
		this.gridLayout3.setColumns(2);
		this.gridLayout3.setRows(3);
		this.checkBoxAssiali.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxAssiali.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.checkBoxAssiali, 0, 0);
		this.checkBoxCentrifughi.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxCentrifughi.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.checkBoxCentrifughi, 0, 1);
		this.checkBoxTR.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTR.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.checkBoxTR, 1, 0);
		this.checkBoxDA.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxDA.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.checkBoxDA, 1, 1);
		this.gridLayout3.setColumnExpandRatio(0, 10.0F);
		this.gridLayout3.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout3_vSpacer = new CustomComponent();
		gridLayout3_vSpacer.setSizeFull();
		this.gridLayout3.addComponent(gridLayout3_vSpacer, 0, 2, 1, 2);
		this.gridLayout3.setRowExpandRatio(2, 1.0F);
		this.gridLayout4.setColumns(4);
		this.gridLayout4.setRows(5);
		this.labelPortata.setWidth(80, Unit.PERCENTAGE);
		this.labelPortata.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.labelPortata, 0, 1);
		this.labelPortataM.setWidth(80, Unit.PERCENTAGE);
		this.labelPortataM.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.labelPortataM, 0, 2);
		this.textFieldPortata.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPortata.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.textFieldPortata, 1, 1);
		this.comboBoxPortataUM.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPortataUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.comboBoxPortataUM, 2, 1);
		this.textFieldPortataKgh.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPortataKgh.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.textFieldPortataKgh, 1, 2);
		this.label.setSizeUndefined();
		this.gridLayout4.addComponent(this.label, 2, 2);
		this.labelNPortata.setWidth(100, Unit.PERCENTAGE);
		this.labelNPortata.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.labelNPortata, 0, 3);
		this.textFieldNPortata.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNPortata.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.textFieldNPortata, 1, 3);
		this.label11.setWidth(100, Unit.PERCENTAGE);
		this.label11.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.label11, 2, 3);
		this.labelPressione.setWidth(100, Unit.PERCENTAGE);
		this.labelPressione.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.labelPressione, 0, 4);
		this.textFieldPressione.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPressione.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.textFieldPressione, 1, 4);
		this.comboBoxPressioneUM.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPressioneUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.comboBoxPressioneUM, 2, 4);
		this.gridLayout4.setColumnExpandRatio(0, 8.0F);
		this.gridLayout4.setColumnExpandRatio(1, 10.0F);
		this.gridLayout4.setColumnExpandRatio(2, 10.0F);
		this.gridLayout4.setColumnExpandRatio(3, 10.0F);
		this.comboBoxFrequenza.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxFrequenza.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.comboBoxFrequenza, 0 ,0);
		this.labelHz.setWidth(100, Unit.PERCENTAGE);
		this.labelHz.setHeight(-1, Unit.PIXELS);
		this.gridLayout4.addComponent(this.labelHz, 1, 0);
                this.comboBoxEsecuzioni.setSizeFull();
                this.gridLayout4.addComponent(this.comboBoxEsecuzioni, 3, 0);
		this.gridLayout4.addComponent(this.labelEsec, 2, 0);
		
		final CustomComponent gridLayout4_vSpacer = new CustomComponent();
		gridLayout4_vSpacer.setSizeFull();
		//this.gridLayout4.addComponent(gridLayout4_vSpacer, 0, 4, 2, 4);
		//this.gridLayout4.setRowExpandRatio(4, 1.0F);
		this.gridLayout6.setColumns(2);
		this.gridLayout6.setRows(3);
		this.checkBoxTotale.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTotale.setHeight(-1, Unit.PIXELS);
		this.gridLayout6.addComponent(this.checkBoxTotale, 0, 0);
		this.checkBoxStatica.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxStatica.setHeight(-1, Unit.PIXELS);
		this.gridLayout6.addComponent(this.checkBoxStatica, 0, 1);
		this.checkBoxInlet.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxInlet.setHeight(-1, Unit.PIXELS);
		this.gridLayout6.addComponent(this.checkBoxInlet, 1, 0);
		this.checkBoxOutlet.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxOutlet.setHeight(-1, Unit.PIXELS);
		this.gridLayout6.addComponent(this.checkBoxOutlet, 1, 1);
		this.gridLayout6.setColumnExpandRatio(0, 10.0F);
		this.gridLayout6.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout6_vSpacer = new CustomComponent();
		gridLayout6_vSpacer.setSizeFull();
		this.gridLayout6.addComponent(gridLayout6_vSpacer, 0, 2, 1, 2);
		this.gridLayout6.setRowExpandRatio(2, 1.0F);
		this.gridLayout5.setColumns(2);
		this.gridLayout5.setRows(2);
		this.buttonSelezioneSerie.setSizeFull();
                this.gridLayout5.addComponent(this.buttonSelezioneSerie, 0, 0);
		this.buttonReset.setSizeFull();
		this.gridLayout5.addComponent(this.buttonReset, 0, 1);
//                this.buttonCerca.setHeight(50, Unit.PIXELS);
                this.buttonCerca.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout5.addComponent(this.buttonCerca, 1, 0, 1, 1, Alignment.MIDDLE_CENTER);
                this.buttonCerca.setStyleName("double");
                this.gridLayout5.setColumnExpandRatio(1, 1F);
		this.gridLayout5.setColumnExpandRatio(0, 1F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout);
		this.gridLayout8.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout8.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout8);
		this.gridLayout2.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout2);
		this.gridLayout3.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout3);
		this.gridLayout4.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout4);
		this.gridLayout6.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout6.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout6);
		this.gridLayout5.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout5);
		final CustomComponent verticalLayout2_spacer = new CustomComponent();
		verticalLayout2_spacer.setSizeFull();
		this.verticalLayout2.addComponent(verticalLayout2_spacer);
		this.verticalLayout2.setExpandRatio(verticalLayout2_spacer, 1.0F);
		this.verticalLayout2.setSizeFull();
		this.setContent(this.verticalLayout2);
		this.setWidth(30, Unit.PERCENTAGE);
		this.setHeight(80, Unit.PERCENTAGE);

		this.textAltezzaSLM.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textAltezzaSLM_textChange(event);
			}
		});
		this.comboBoxAltezzaSLMUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.comboBoxAltezzaSLMUM_valueChange(event);
			}
		});
		this.textFieldTemperatura.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textFieldTemperatura_textChange(event);
			}
		});
		this.comboBoxTemperaturaUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.comboBoxTemperaturaUM_valueChange(event);
			}
		});
		this.comboBoxFrequenza.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.comboBoxFrequenza_valueChange(event);
			}
		});
		this.checkBoxAriaSecca.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxAriaSecca_valueChange(event);
			}
		});
		this.checkBoxAriaUmida.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxAriaUmida_valueChange(event);
			}
		});
		this.checkBoxEsplicito.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxEsplicito_valueChange(event);
			}
		});
		this.checkBoxMiscela.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxMiscela_valueChange(event);
			}
		});
		this.textFieldUmidita.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textFieldUmidita_textChange(event);
			}
		});
		this.buttonMiscela.addClickListener(event -> this.buttonMiscela_buttonClick(event));
		this.textFieldDensita200.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textFieldDensita200_textChange(event);
			}
		});
		this.checkBoxAssiali.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxAssiali_valueChange(event);
			}
		});
		this.checkBoxCentrifughi.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxCentrifughi_valueChange(event);
			}
		});
		this.checkBoxTR.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxTR_valueChange(event);
			}
		});
		this.checkBoxDA.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxDA_valueChange(event);
			}
		});
		this.textFieldPortata.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textFieldPortata_textChange(event);
			}
		});
		this.comboBoxPortataUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.comboBoxPortataUM_valueChange(event);
			}
		});
		this.textFieldPortataKgh.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textFieldPortataKgh_textChange(event);
			}
		});
		this.textFieldNPortata.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textFieldNPortata_textChange(event);
			}
		});
		this.textFieldPressione.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaDatiSelezione.this.textFieldPressione_textChange(event);
			}
		});
		this.comboBoxPressioneUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.comboBoxPressioneUM_valueChange(event);
			}
		});
                this.comboBoxEsecuzioni.addValueChangeListener((final Property.ValueChangeEvent event) -> {
                    pannelloRicercaDatiSelezione.this.comboBoxEsecuzioni_valueChange(event);
                });
		this.checkBoxTotale.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxTotale_valueChange(event);
			}
		});
		this.checkBoxStatica.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxStatica_valueChange(event);
			}
		});
		this.checkBoxInlet.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxInlet_valueChange(event);
			}
		});
		this.checkBoxOutlet.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaDatiSelezione.this.checkBoxOutlet_valueChange(event);
			}
		});
		this.buttonSelezioneSerie.addClickListener(event -> this.buttonSelezioneSerie_buttonClick(event));
		this.buttonCerca.addClickListener(event -> this.buttonCerca_buttonClick(event));
		this.buttonReset.addClickListener(event -> this.buttonReset_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private TextFieldInteger textFieldUmidita;
	private XdevLabel labelAlezzaSLM, labelTemperatura, labelUmidita, labelUmiditaUM, labelDensita200, labelDensitaUM,
			labelDensitaXX, labelDensitaXXVal, labelPortata, labelPortataM, label, labelNPortata, label11, labelPressione, labelHz, labelEsec;
	private XdevButton buttonMiscela, buttonSelezioneSerie, buttonCerca, buttonReset;
	private XdevHorizontalLayout horizontalLayout3;
	private TextFieldDouble textAltezzaSLM, textFieldTemperatura, textFieldDensita200, textFieldPortata,
			textFieldPortataKgh, textFieldNPortata, textFieldPressione;
	private XdevCheckBox checkBoxAriaSecca, checkBoxAriaUmida, checkBoxEsplicito, checkBoxMiscela, checkBoxAssiali,
			checkBoxCentrifughi, checkBoxTR, checkBoxDA, checkBoxTotale, checkBoxStatica, checkBoxInlet, checkBoxOutlet;
	private XdevPanel panelUmidita;
	private XdevGridLayout gridLayout, gridLayout8, gridLayout2, gridLayout3, gridLayout4, gridLayout6, gridLayout5;
	private XdevVerticalLayout verticalLayout2;
	private XdevComboBox<CustomComponent> comboBoxAltezzaSLMUM, comboBoxTemperaturaUM, comboBoxPortataUM,
			comboBoxPressioneUM, comboBoxFrequenza, comboBoxEsecuzioni;
	// </generated-code>


}
