package com.cit.sellfan.ui.pannelli;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.classi.Clientev21;
import cit.sellfan.classi.Preventivo;
import cit.sellfan.classi.UtilityCliente;
import cit.utility.UtilityTraduzioni;
import java.util.HashMap;
import java.util.Map;

class ClientCompare implements Comparator<Clientev21>
{
	@Override
	public int compare(final Clientev21 o1, final Clientev21 o2) {
		return o1.ragionesociale.compareToIgnoreCase(o2.ragionesociale);
	}
}

public class pannelloEditPreventivo extends Panel {
	private VariabiliGlobali l_VG;
	private final DateField textDataConsegna = new DateField();
	private CheckBox chkDataConsegna;
	private final boolean newMode;
	private boolean loading = false;
	private Clientev21 clienteSelezionato;
	Validator noSpecChar = new Validator() {
		private static final long serialVersionUID = 1L;
		@Override
		public void validate(final Object value) throws InvalidValueException {
			final Pattern p = Pattern.compile("^[a-zA-Z0-9 -]*$");
			final Matcher m = p.matcher(value.toString());
			if (!m.find()) {
				throw new InvalidValueException("Only letters and numbers admitted");
			}
			}
	};
	private XdevComboBox comboBoxClienti;
	public pannelloEditPreventivo(final boolean newModehere) {
		super();
		this.newMode = newModehere;
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		Nazionalizza();
	}

	public void Nazionalizza() {
		final UtilityTraduzioni trans = this.l_VG.utilityTraduzioni;
		if (this.newMode) {
			this.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nuovo Preventivo"));
		} else {
			this.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Modifica dettagli Preventivo"));
		}
		this.textFieldCodiceOfferta.setCaption(trans.TraduciStringa("Codice Offerta"));
		this.textFieldCodiceOrdine.setCaption(trans.TraduciStringa("Progetto"));
		this.textFieldCodiceOfferta.setInputPrompt(trans.TraduciStringa("Solo spazi lettere e numeri"));
		this.textDataConsegna.setCaption(trans.TraduciStringa("Data di consegna Richiesta"));
		this.textFieldIstruzioniConsegna.setCaption(trans.TraduciStringa("Consegna"));
		this.textFieldPagamento.setCaption(trans.TraduciStringa("Pagamento Preventivo Corrente"));
		this.textFieldSconto.setCaption(trans.TraduciStringa("Sconto Preventivo Corrente"));
		this.textFieldIndirizzoSpedizione.setCaption(trans.TraduciStringa("Indirizzo di spedizione Preventivo Corrente"));
		this.chkDataConsegna.setCaption(trans.TraduciStringa("Inserisci data Consegna"));
		this.textFieldNote.setCaption(trans.TraduciStringa("Note"));
		this.textFieldNomeContatto.setCaption(trans.TraduciStringa("Contatto Interno"));
		this.textFieldMailContatto.setCaption(trans.TraduciStringa("Mail Interno"));
                this.textFieldPackaging.setCaption("Packaging");
                this.textFieldIncoterms.setCaption("Incoterms");
		this.comboBoxClienti.setCaption(trans.TraduciStringa("Cliente"));
	}
	
	@SuppressWarnings("unchecked")
	public void init() {
		this.loading = true;
		if (this.l_VG.currentCliente != null) {
			this.clienteSelezionato = this.l_VG.currentCliente;
		}
        //Inizializza comboBox Clienti
		this.comboBoxClienti.removeAllItems();
		this.comboBoxClienti.addContainerProperty("RagioneSociale", String.class, null);
                this.comboBoxClienti.addContainerProperty("Cliente", Clientev21.class, null);
                this.comboBoxClienti.setItemCaptionMode(ItemCaptionMode.PROPERTY);
                this.comboBoxClienti.setItemCaptionPropertyId("RagioneSociale");
                final ArrayList<Clientev21> sortedClienti = this.l_VG.ElencoClienti;
                Collections.sort(sortedClienti, new ClientCompare());
                for (int i=0 ; i<sortedClienti.size() ; i++) {
                        this.comboBoxClienti.addItem(sortedClienti.get(i).IDCliente); //add an item to the container, and set it to itemId
                    final Item item = this.comboBoxClienti.getItem(sortedClienti.get(i).IDCliente);
                    item.getItemProperty("Cliente").setValue(sortedClienti.get(i));
                    item.getItemProperty("RagioneSociale").setValue(sortedClienti.get(i).ragionesociale);
                }

        //Nuovo preventivo: Combobox Attivo, Informazioni Offerta non attive
		if (this.newMode) {
			this.textFieldPagamento.setEnabled(false);
			this.textFieldSconto.setEnabled(false);
			this.textFieldIndirizzoSpedizione.setEnabled(false);
			this.textFieldNomeContatto.setEnabled(false);
			this.textFieldMailContatto.setEnabled(false);
		}
		//Preventivo esistente
		if (!this.newMode) {
			final Preventivo prevCur = this.l_VG.preventivoCurrent;
                        Map datiOfferta = generaDatiOfferta(prevCur, this.clienteSelezionato);
                        if (prevCur.IDCliente == -1) {
				this.textFieldPagamento.setEnabled(false);
				this.textFieldSconto.setEnabled(false);
				this.textFieldIndirizzoSpedizione.setEnabled(false);
				this.textFieldNomeContatto.setEnabled(false);
				this.textFieldMailContatto.setEnabled(false);
			} else {
				this.comboBoxClienti.setValue(prevCur.IDCliente);
				this.comboBoxClienti.setEnabled(false);
			}
			this.textFieldCodiceOfferta.setValue(prevCur.NumOfferta);
			this.textFieldCodiceOrdine.setValue(prevCur.NumOrdine);
			this.textDataConsegna.setValue(prevCur.ConsegnaRichiesta);
			this.textDataConsegna.setVisible(prevCur.dataConsegnaInserita);
			this.textFieldIstruzioniConsegna.setValue(datiOfferta.get("NoteSpedizione").toString());
                        this.textFieldPagamento.setValue(datiOfferta.get("Payment").toString());
			this.textFieldSconto.setValue(Double.parseDouble(datiOfferta.get("Sconto").toString()));
			this.textFieldIndirizzoSpedizione.setValue(datiOfferta.get("ShippingAddress").toString());
			this.chkDataConsegna.setValue(prevCur.dataConsegnaInserita);
			this.textFieldNote.setValue(datiOfferta.get("Note").toString());
                        this.textFieldNomeContatto.setValue(datiOfferta.get("InternalReference").toString());
                        this.textFieldMailContatto.setValue(datiOfferta.get("InternalReferenceMail").toString());
                        this.textFieldIncoterms.setValue(datiOfferta.get("Incoterms").toString());
                        this.textFieldPackaging.setValue(datiOfferta.get("Packaging").toString());
			}
		this.loading = false;

		
	}
	
	public void getValues() {
		final Preventivo prevCur = this.l_VG.preventivoCurrent;
		final String replacedString = this.textFieldCodiceOfferta.getValue().replaceAll("[^0-9A-Za-z -]", "");
		if (this.clienteSelezionato != null) {
			this.l_VG.currentCliente = this.clienteSelezionato;
			prevCur.IDCliente = this.clienteSelezionato.IDCliente;
			prevCur.ragioneSocialeCliente = this.clienteSelezionato.ragionesociale;
		}
		prevCur.NumOfferta = replacedString;
		prevCur.NumOrdine = this.textFieldCodiceOrdine.getValue();
		prevCur.dataConsegnaInserita = this.chkDataConsegna.getValue();
		prevCur.ConsegnaRichiesta = this.textDataConsegna.getValue();
		prevCur.NoteConsegna = this.textFieldIstruzioniConsegna.getValue();
		prevCur.Pagamento = this.textFieldPagamento.getValue();
		final Double sconto = this.textFieldSconto.getDoubleValue();
		if (sconto == 0 && !this.newMode) {
			if (this.clienteSelezionato != null) {
				prevCur.Sconto = this.l_VG.currentCliente.scontocliente;
			}
		} else {
			prevCur.Sconto = sconto;
		}
		prevCur.IndirizzoSpedizione = this.textFieldIndirizzoSpedizione.getValue();
		prevCur.Note = this.textFieldNote.getValue();
		if (prevCur.nomeContattoInterno != null) {
			if (this.textFieldNomeContatto.getValue().equals("") && !this.newMode && this.clienteSelezionato!=null) {
				prevCur.nomeContattoInterno	= this.l_VG.currentCliente.riferimentotecnico;
			} else {
				prevCur.nomeContattoInterno = this.textFieldNomeContatto.getValue();
			}
		}
		if (prevCur.mailContattoInterno != null) {
			if (this.textFieldMailContatto.getValue().equals("") && !this.newMode && this.clienteSelezionato!=null) {
				prevCur.mailContattoInterno = this.l_VG.currentCliente.emailtecnica;
			} else {
				prevCur.mailContattoInterno = this.textFieldMailContatto.getValue();
			}
		}
	}
	
	private void consegnaSet(final Boolean value) {
		this.textDataConsegna.setVisible(value);
	}
	private void setCliente() {
		this.textFieldPagamento.setEnabled(true);
		this.textFieldSconto.setEnabled(true);
		this.textFieldIndirizzoSpedizione.setEnabled(true);
		this.textFieldNomeContatto.setEnabled(true);
		this.textFieldMailContatto.setEnabled(true);
		
		this.textFieldPagamento.setValue(this.clienteSelezionato.pagamento);
		this.textFieldSconto.setValue(this.clienteSelezionato.scontocliente);
		this.textFieldIndirizzoSpedizione.setValue(this.clienteSelezionato.indirizzospedizione);
		this.textFieldNomeContatto.setValue(this.clienteSelezionato.riferimentotecnico);
		this.textFieldMailContatto.setValue(this.clienteSelezionato.emailtecnica);
	}
	
	public boolean validate() {
		final Pattern p = Pattern.compile("^[a-zA-Z0-9 -]*$");
		final Matcher m = p.matcher(this.textFieldCodiceOfferta.getValue());
		return m.find();
		}
	
	private void initUI() {
		this.gridLayout = new XdevGridLayout();
		this.textFieldCodiceOfferta = new XdevTextField();
		this.textFieldCodiceOrdine = new XdevTextField();
		this.textFieldIstruzioniConsegna = new XdevTextField();
		this.textFieldPagamento = new XdevTextField();
		this.textFieldSconto = new TextFieldDouble();
		this.textFieldIndirizzoSpedizione = new XdevTextField();
		this.chkDataConsegna = new CheckBox();
		this.textFieldNote = new XdevTextField();
		this.textFieldNomeContatto = new XdevTextField();
		this.textFieldMailContatto = new XdevTextField();
                this.textFieldIncoterms = new XdevTextField();
                this.textFieldPackaging = new XdevTextField();
		this.comboBoxClienti = new XdevComboBox<>();
		this.textFieldCodiceOfferta.setSizeFull();
		this.textFieldCodiceOrdine.setSizeFull();
		this.textFieldIstruzioniConsegna.setSizeFull();
		this.textFieldPagamento.setSizeFull();
		this.textFieldSconto.setSizeFull();
		this.textFieldIndirizzoSpedizione.setSizeFull();
		this.textFieldNote.setSizeFull();
		this.textFieldNomeContatto.setSizeFull();
		this.textFieldMailContatto.setSizeFull();
                this.textFieldIncoterms.setSizeFull();
                this.textFieldPackaging.setSizeFull();
		
		this.chkDataConsegna.addValueChangeListener(new ValueChangeListener() {
		@Override
		public void valueChange(final ValueChangeEvent event) {
			final CheckBox chk = (CheckBox)event.getProperty();
			consegnaSet(chk.getValue());
		}});
		this.chkDataConsegna.setSizeFull();
		
		this.textDataConsegna.setDateFormat("dd/MM/yyyy");
		this.textDataConsegna.setSizeFull();
		this.comboBoxClienti.setSizeFull();
		
		this.gridLayout.setColumns(2);
		this.gridLayout.setRows(9);
		this.gridLayout.addComponent(this.textFieldCodiceOfferta, 0, 0);
		this.gridLayout.addComponent(this.textFieldCodiceOrdine, 1, 0);
		this.gridLayout.addComponent(this.chkDataConsegna, 0, 1);
		this.gridLayout.addComponent(this.textDataConsegna, 1, 1);
		this.gridLayout.addComponent(this.textFieldIstruzioniConsegna, 0, 2, 1, 2);
		this.gridLayout.addComponent(this.comboBoxClienti, 0, 3, 1, 3);
		this.gridLayout.addComponent(this.textFieldPagamento, 0, 4);
		this.gridLayout.addComponent(this.textFieldSconto, 1, 4);
		this.gridLayout.addComponent(this.textFieldIndirizzoSpedizione, 0, 5, 1, 5);
		this.gridLayout.addComponent(this.textFieldNomeContatto, 0, 6);
		this.gridLayout.addComponent(this.textFieldMailContatto, 1, 6);
                this.gridLayout.addComponent(this.textFieldIncoterms, 0, 7);
                this.gridLayout.addComponent(this.textFieldPackaging, 1, 7);
		this.gridLayout.addComponent(this.textFieldNote, 0, 8, 1, 8);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setSizeFull();
		this.setContent(this.gridLayout);
		this.setSizeUndefined();
		
		this.comboBoxClienti.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				this.comboBoxClienti_valueChange(event);
			}

			private void comboBoxClienti_valueChange(final ValueChangeEvent event) {
				if (!pannelloEditPreventivo.this.loading) {
					//selezione cliente
					//l_VG.currentCliente =
					final String ragSoc = ((XdevComboBox) event.getProperty()).getContainerProperty(event.getProperty().getValue(), "RagioneSociale").getValue().toString();
					final int idCl = Integer.parseInt(event.getProperty().getValue().toString());
					for (int i = 0; i < pannelloEditPreventivo.this.l_VG.ElencoClienti.size() -1; i++) {
						if (pannelloEditPreventivo.this.l_VG.ElencoClienti.get(i).ragionesociale.equals(ragSoc)) {
							pannelloEditPreventivo.this.clienteSelezionato = pannelloEditPreventivo.this.l_VG.ElencoClienti.get(i);
							break;
						}
					}
					setCliente();
				final String text = event.getProperty().getValue().toString();
				}
			}
		});
	}
	
	private XdevGridLayout gridLayout;
	private TextFieldDouble textFieldSconto;
	private XdevTextField textFieldNote, textFieldNomeContatto, textFieldMailContatto, textFieldCodiceOfferta, textFieldCodiceOrdine, textFieldIstruzioniConsegna, textFieldPagamento, textFieldIndirizzoSpedizione, textFieldIncoterms, textFieldPackaging; // </generated-code>

    private Map generaDatiOfferta(Preventivo datiPreventivo, Clientev21 datiCliente) {
        final UtilityCliente datiProgramma = this.l_VG.utilityCliente;
        Map datiOfferta=new HashMap();
        // setto dati default
        datiOfferta.put("NoteSpedizione", datiProgramma.consegnaNoteDefault);
        datiOfferta.put("Incoterms", datiProgramma.resaDefault );
        datiOfferta.put("Packaging", datiProgramma.imballoDefault);
        // setto dati cliente
        datiOfferta.put("Payment", datiCliente.pagamento);
        datiOfferta.put("Sconto", datiCliente.scontocliente);
        datiOfferta.put("ShippingAddress", datiCliente.indirizzospedizione);
        datiOfferta.put("InternalReference", datiCliente.riferimentotecnico);
        datiOfferta.put("InternalReferenceMail", datiCliente.emailtecnica);
        if (!datiCliente.resa.equals(""))
            datiOfferta.replace("Incoterms", datiCliente.resa);
        if (!datiCliente.imballo.equals(""))
            datiOfferta.replace("Packaging", datiCliente.imballo);
        // setto dati preventivo
        datiOfferta.put("Note", datiPreventivo.Note);
        if (!datiPreventivo.NoteConsegna.equals(""))
            datiOfferta.replace("NoteSpedizione", datiPreventivo.NoteConsegna);
        if (!datiPreventivo.Resa.equals(""))
            datiOfferta.replace("Incoterms", datiPreventivo.Resa);
        if (!datiPreventivo.Imballo.equals(""))
            datiOfferta.replace("Packaging", datiPreventivo.Imballo);
        if (!datiPreventivo.Pagamento.equals(""))
            datiOfferta.replace("Payment", datiPreventivo.Pagamento);
        if (datiPreventivo.Sconto > 0)
            datiOfferta.replace("Sconto", datiPreventivo.Sconto);
        if (!datiPreventivo.IndirizzoSpedizione.equals(""))
            datiOfferta.replace("ShippingAddress", datiPreventivo.IndirizzoSpedizione);
        if (!datiPreventivo.nomeContattoInterno.equals(""))
            datiOfferta.replace("InternalReference", datiPreventivo.nomeContattoInterno);
        if (!datiPreventivo.mailContattoInterno.equals(""))
            datiOfferta.replace("InternalReferenceMail", datiPreventivo.mailContattoInterno);
        
        return datiOfferta;
    }


}



