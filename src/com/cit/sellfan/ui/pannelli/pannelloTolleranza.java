package com.cit.sellfan.ui.pannelli;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;

/**
 *
 * @author Eraldo Marziano
 */
public class pannelloTolleranza extends Panel{
    private VariabiliGlobali vGlobali;
    private XdevGridLayout gridLayout;
    private XdevLabel labelPiu, labelMeno, labelPiuPercento, labelMenoPercento;
    private TextFieldInteger valuePiu, valueMeno;
    private boolean suPressione = false;

    public pannelloTolleranza(boolean pressione) {
        super();
        suPressione = pressione;
        initUI();
    }
    
    public void setVariabiliGlobali(VariabiliGlobali vGlobali) {
        this.vGlobali = vGlobali;
        if (suPressione) {
            valuePiu.setValue(vGlobali.ParametriVari.pannelloRicercaTolleranzaPressionePiu * 100);
            valueMeno.setValue(vGlobali.ParametriVari.pannelloRicercaTolleranzaPressioneMeno * 100);
        } else {
            valuePiu.setValue(vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezionePIU * 100);
            valueMeno.setValue(vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO * 100);
        }
    }

    
    public void confermaValori() {
        if (suPressione) {
            vGlobali.ParametriVari.pannelloRicercaTolleranzaPressioneMeno = valueMeno.getIntegerValue() / 100.;
            vGlobali.ParametriVari.pannelloRicercaTolleranzaPressionePiu = valuePiu.getIntegerValue() / 100.;
            vGlobali.SelezioneDati.precisionePiuPressione = (double) valuePiu.getDoubleValue() / 100.0;
            vGlobali.SelezioneDati.precisioneMenoPressione = (double) valueMeno.getDoubleValue() / 100.0;
        } else {
            vGlobali.SelezioneDati.precisionePiu = (double) valuePiu.getDoubleValue() / 100.0;
            vGlobali.SelezioneDati.precisioneMeno = (double) valueMeno.getDoubleValue() / 100.0;
            vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO = valueMeno.getIntegerValue() / 100.;
            vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezionePIU = valuePiu.getIntegerValue() / 100.;
        }
    }
    
    private void initUI() {
        gridLayout = new XdevGridLayout();
	labelPiu = new XdevLabel("+");
        labelMeno = new XdevLabel("-");
	labelPiuPercento = new XdevLabel("%");
        labelMenoPercento = new XdevLabel("%");
        valuePiu = new TextFieldInteger();
        valueMeno = new TextFieldInteger();
        labelPiu.setWidth(100, Unit.PERCENTAGE);
        labelMeno.setWidth(100, Unit.PERCENTAGE);
        valuePiu.setWidth(100, Unit.PERCENTAGE);
        valueMeno.setWidth(100, Unit.PERCENTAGE);
        gridLayout.setRows(2);
        gridLayout.setColumns(3);
        gridLayout.addComponent(labelMeno, 0, 0);
        gridLayout.addComponent(valueMeno, 1, 0);
        gridLayout.addComponent(labelMenoPercento, 2, 0);
        gridLayout.addComponent(labelPiu, 0, 1);
        gridLayout.addComponent(valuePiu, 1, 1);
        gridLayout.addComponent(labelPiuPercento, 2, 1);
        gridLayout.setColumnExpandRatios(1, 3, 1);
        gridLayout.setSizeFull();
        this.setContent(gridLayout);
    }
    
    	
}
