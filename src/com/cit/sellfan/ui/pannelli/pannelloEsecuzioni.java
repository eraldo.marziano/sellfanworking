/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.pannelli;

import cit.sellfan.classi.Tramoggia;
import cit.utility.UtilityTraduzioni;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.utility.oggettoSelect;
import com.vaadin.data.Property;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

/**
 *
 * @author Eraldo Marziano
 */
public class pannelloEsecuzioni extends Panel{
    private XdevImage diagramma;
    private XdevVerticalLayout layoutPrincipale;
    private XdevHorizontalLayout layoutComboBox;
    private XdevComboBox<CustomComponent> esecuzione;
    private XdevLabel esecuzioneLabel, descrizioneLabel;
    private VariabiliGlobali vGlobali;
    private String path;
    private boolean centrifughi;
    String esecuzioneSelezionata;
    
    public pannelloEsecuzioni() {
        super();
        initUI();
    }
    
    private void initUI() {
        this.esecuzioneLabel = new XdevLabel("Esecuzione");
        this.esecuzione = new XdevComboBox<>();
        this.layoutComboBox = new XdevHorizontalLayout(esecuzioneLabel, esecuzione);
        this.descrizioneLabel = new XdevLabel("");
        this.diagramma = new XdevImage();
        this.layoutPrincipale = new XdevVerticalLayout(layoutComboBox, descrizioneLabel, diagramma);
        this.descrizioneLabel.setWidth(300, Unit.PIXELS);
        this.layoutPrincipale.addStyleName("centered");
        this.setContent(layoutPrincipale);
        
        
        esecuzione.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            esecuzione_ValueChange();
        });
        
        
    }
    
    public void SetVariabiliGlobali (VariabiliGlobali vg) {
        vGlobali = vg;
        path = vg.Paths.rootResources;
    }
    
    public void Nazionalizza() {
        
    }
    
    public void init(String AssialiCentrifughi, String TrasmissioneDiretti) {
        Nazionalizza();
        buildElencoEsecuzioni(AssialiCentrifughi, TrasmissioneDiretti);
        centrifughi = AssialiCentrifughi.equals("Centrifughi");
    }
    public String aggiornaValori() 
    {
        return esecuzioneSelezionata;
    }
    public void esecuzione_ValueChange() {
        esecuzioneSelezionata = esecuzione.getValue().toString();
        String assiale = (centrifughi) ? "" : "ass";
        descrizioneLabel.setValue(vGlobali.getDescrizioneEsecuzione(esecuzioneSelezionata));
        String fileName = this.path + "mzimg/esecuzioni/" + esecuzioneSelezionata + assiale + ".jpg";
	StreamResource streamImage = vGlobali.getStreamResourceForImageFile(fileName);
        if (streamImage != null) {
            diagramma.setSource(streamImage);
        }
    }
    
    public void setEsecuzioneIniziale(String ex) {
        if (ex.equals("Default"))
            esecuzione.select(esecuzione.getItemIds().toArray()[0]);
        else
            esecuzione.select(ex);
    }
    
    private void buildElencoEsecuzioni(String AssCen, String TraDir) {
        if (TraDir.equals("Ventilatori diretti"))
        {
            if (AssCen.equals("Assiali"))
                esecuzione.addItems("E04", "E05");
            else
                for (String esec : vGlobali.pannelloRicercaEsecuzioniDA)
                    esecuzione.addItem(esec);
        }
        if (TraDir.equals("Trasmissione"))
        {
            if (AssCen.equals("Assiali"))
                esecuzione.addItems("E01", "E09");
            else
            {
                for (String esec : vGlobali.pannelloRicercaEsecuzioniTR1)
                    esecuzione.addItem(esec);
                for (String esec : vGlobali.pannelloRicercaEsecuzioniTR2)
                    esecuzione.addItem(esec);
            }
        }
    }
}
