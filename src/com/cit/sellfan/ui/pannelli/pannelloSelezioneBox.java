package com.cit.sellfan.ui.pannelli;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.Box;
import cit.sellfan.classi.SelezioneCorrente;
import java.util.List;
import java.util.Map;

public class pannelloSelezioneBox extends Panel {
	private final VariabiliGlobali l_VG;
	private final Container containerBox = new IndexedContainer();
	private Box possibleBox;
        int codiceBox;
        ArrayList<Integer> misure = new ArrayList<Integer>();
	public pannelloSelezioneBox(final VariabiliGlobali vg, int codiceBox, ArrayList<Integer> misure) {
		super();
		this.initUI();
		this.l_VG = vg;
                this.codiceBox = codiceBox;
                this.misure = misure;
		this.containerBox.removeAllItems();
		this.containerBox.addContainerProperty("Code", String.class, "");
		this.containerBox.addContainerProperty("Description", String.class, "");
		this.tableBox.setContainerDataSource(this.containerBox);
		loadBox();
	}
	
	public void loadBox() {
            this.tableBox.removeAllItems();
            Box box = this.l_VG.dbTecnico.getBoxSelezionabile(codiceBox, misure);
            l_VG.VentilatoreCurrent.selezioneCorrente.box = box;
            final String  esecuz = this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione;
            SelezioneCorrente selCor = this.l_VG.VentilatoreCurrent.selezioneCorrente;
            String codicePadded = (selCor.box.codiceBox.length()==3)? "0"+selCor.box.codiceBox : selCor.box.codiceBox;
            String Orientamento = (selCor.Orientamento.equals("RD"))? "R" : "L";
            String Angolo;
            switch (selCor.OrientamentoAngolo) {
                case 0:
                    Angolo = "0";
                    break;
                case 90:
                    Angolo = "2";
                    break;
                case 270:
                    Angolo = "6";
                    break;
                default:
                    Angolo = "XX";
                    break;
            }
            String codiceCompletoBox = " BOXL" + codicePadded + "-" + l_VG.VentilatoreCurrent.Modello.replace(" ","") + Orientamento + Angolo + " ";
                if (esecuz.equals("E04")) {
			final String boxTab[] = new String[2];
			boxTab[0] = codiceCompletoBox;
			boxTab[1] = this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione Box");
			this.tableBox.addItem(boxTab, null);
		}
                possibleBox = box;
		
		//ricerca in DB
	}

	public Box getBox() {
		final String modello = this.l_VG.VentilatoreCurrent.Modello;
		final int rotaz   = this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo;
		final int  tagliaMot = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza;
		final String  esecuz = this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione;
                final String serie = this.l_VG.VentilatoreCurrent.Serie;
		Box result = null;
			if (esecuz.equals("E04")) {
			final List<Map<String, Object>> lista = this.l_VG.dbTecnico.getBox(rotaz, tagliaMot, modello, serie);
//			result = new Box(
//					lista.get(0).toString(),
//					lista.get(1).toString(),
//					lista.get(4).toString(),
//					lista.get(2).toString(),
//					(int)lista.get(3),
//					(int)lista.get(5),
//					(int)lista.get(6),
//					(int)lista.get(7),
//					(int)lista.get(8)
//					);
		}
		return result;
			
	}
	
	
	public Box select() {
		return l_VG.VentilatoreCurrent.selezioneCorrente.box;
		}

	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelTitolo = new XdevLabel();
		this.tableBox = new XdevTable<>();
		this.labelTitolo.setContentMode(ContentMode.HTML);
		this.tableBox.setStyleName("small striped");
		this.labelTitolo.setValue("Selezionare il box?");
		
		this.verticalLayout.addComponent(this.labelTitolo);
		this.verticalLayout.setComponentAlignment(this.labelTitolo, Alignment.MIDDLE_CENTER);
		this.tableBox.setSizeFull();
		this.verticalLayout.addComponent(this.tableBox);
		this.verticalLayout.setComponentAlignment(this.tableBox, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableBox, 100.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setWidth(80, Unit.PERCENTAGE);
		this.setHeight(80, Unit.PERCENTAGE);
		}

	private XdevLabel labelTitolo;
	private XdevTable<CustomComponent> tableBox;
	private XdevVerticalLayout verticalLayout;
}

