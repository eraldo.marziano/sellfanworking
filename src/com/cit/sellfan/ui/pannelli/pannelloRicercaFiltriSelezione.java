package com.cit.sellfan.ui.pannelli;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.SearchView;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.sellfan.classi.titoli.StringheUNI;

public class pannelloRicercaFiltriSelezione extends Panel {
	private VariabiliGlobali l_VG;
	private boolean TRFlag;
	private SearchView searchView;
	private boolean editEnabled;
/*
	private myIntegerField textFieldTolleranzaMeno = new myIntegerField();
	private myIntegerField textFieldTolleranzaPiu = new myIntegerField();
	private myIntegerField textFieldrhoMin = new myIntegerField();
	private myIntegerField textFieldPotenzaSonoraMax = new myIntegerField();
	private myIntegerField textFieldCorrettorePotMotore = new myIntegerField();
	private myIntegerField textFieldDiametroGirante = new myIntegerField();
	private myIntegerField textFieldRpmMax = new myIntegerField();
*/

	/**
	 * 
	 */
	public pannelloRicercaFiltriSelezione() {
		super();
		this.initUI();
		this.comboBoxFrequenza.addItem("50");
		this.comboBoxFrequenza.addItem("60");
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		this.searchView = (SearchView)this.l_VG.ElencoView[this.l_VG.SearchViewIndex];
		init();
	}
	
	public void resetPannello() {
		init();
	}
	public void init() {
		this.editEnabled = false;
		this.textFieldTolleranzaMeno.setValue(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO * 100.);
		this.textFieldTolleranzaPiu.setValue(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezionePIU * 100.);
		this.checkBoxTolleranzaPressione.setValue(this.l_VG.pannelloRicercaTolleranzaPressioneFlag);
		this.checkBoxTolleranzaPortata.setValue(!this.l_VG.pannelloRicercaTolleranzaPressioneFlag);
        this.comboBoxFrequenza.setValue(Integer.toString(this.l_VG.CARicerca.frequenza));
        this.checkBoxAspirazioneSingola.setValue(true);
        this.checkBoxAspirazioneMultipla.setValue(false);
        this.textFieldCorrettorePotMotore.setValue(this.l_VG.CorrettorePotenzaMotoreCorrente);
        this.checkBoxDapo.setValue(this.l_VG.SelezioneDati.Dapo);
        this.checkBoxSerranda.setValue(this.l_VG.SelezioneDati.Serranda);
        this.textFieldDiametroGirante.setValue(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.SelezioneDati.DiametroMassimoGirante, this.l_VG.UMDiametroCorrente));
        this.textFieldRpmMax.setValue(this.l_VG.SelezioneDati.giriMassimiGirante);
        this.textFieldPotenzaSonoraMax.setValue(this.l_VG.SelezioneDati.PotSonoraMax);
        this.textFieldrhoMin.setValue(this.l_VG.SelezioneDati.RendMinimo);
        this.editEnabled = true;
	}
        
        public void tolIsAss(boolean isAss) {
            if (isAss) {
                this.textFieldTolleranzaMeno.setValue(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssMENO * 100.);
		this.textFieldTolleranzaPiu.setValue(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssPIU * 100.);
	    } else {
                this.textFieldTolleranzaMeno.setValue(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO * 100.);
		this.textFieldTolleranzaPiu.setValue(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezionePIU * 100.);
            }
        }

	public void Nazionalizza() {
		this.labelTolleranza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tolleranza Ricerca"));
		this.labelTolleranzaSu.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tolleranza su"));
		this.checkBoxTolleranzaPressione.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("pressione"));
		this.checkBoxTolleranzaPortata.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("portata"));
		this.labelEsecuzioni.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzioni"));
		this.labelAspirazione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Aspirazione"));
		this.checkBoxAspirazioneSingola.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("singola"));
		this.checkBoxAspirazioneMultipla.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("multipla"));
		this.labelrhoMin.setValue(StringheUNI.RENDIMENTO);
		this.labelPotenzaSonora.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pot. Son. max"));
		this.labelCorrettorePotMotore.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Correttore potenza motore"));
		this.labelSerranda.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Presenza"));
		this.checkBoxDapo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Dapò"));
		this.checkBoxSerranda.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Serranda"));
		this.labelDiametroGirante.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Diametro massimo girante"));
		this.labelRpmMax.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Giri massimi girante"));
	}
	
	public void getDati() {
		this.l_VG.SelezioneDati.esecuzioneSelezionata = "";
		if (this.comboBoxEsecuzioni.getValue() != null) {
			if (!this.comboBoxEsecuzioni.getValue().toString().equals("Default")) {
				this.l_VG.SelezioneDati.esecuzioneSelezionata = this.comboBoxEsecuzioni.getValue().toString();
			} else {
				this.l_VG.SelezioneDati.esecuzioneSelezionata = "";
			}
		}
		this.l_VG.SelezioneDati.tolleranzaPortataFlag = this.checkBoxTolleranzaPressione.getValue();
		this.l_VG.SelezioneDati.precisioneMeno = this.textFieldTolleranzaMeno.getIntegerValue() / 100.;
		this.l_VG.SelezioneDati.precisionePiu = this.textFieldTolleranzaPiu.getIntegerValue() / 100.;
//		if (this.comboBoxFrequenza.getValue() != null) {
//			this.l_VG.CARicerca.frequenza = Integer.parseInt(this.comboBoxFrequenza.getValue().toString());
//		}
		this.l_VG.SelezioneDati.AspirazioneSingola = this.checkBoxAspirazioneSingola.getValue();
		this.l_VG.SelezioneDati.RendMinimo = this.textFieldrhoMin.getIntegerValue();
		this.l_VG.CorrettorePotenzaMotoreCorrente = this.textFieldCorrettorePotMotore.getIntegerValue();
		this.l_VG.SelezioneDati.Dapo = this.checkBoxDapo.getValue();
		this.l_VG.SelezioneDati.Serranda = this.checkBoxSerranda.getValue();
        this.l_VG.SelezioneDati.DiametroMassimoGirante = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldDiametroGirante.getIntegerValue(), this.l_VG.UMDiametroCorrente);
		this.l_VG.SelezioneDati.giriMassimiGirante = this.textFieldRpmMax.getIntegerValue();
		this.l_VG.SelezioneDati.PotSonoraMax = this.textFieldPotenzaSonoraMax.getIntegerValue();
	}
	
	public void buildComboEsecuzioni(final boolean TR, boolean ass) {
		this.editEnabled = false;
		this.TRFlag = TR;
		this.comboBoxEsecuzioni.removeAllItems();
		this.comboBoxEsecuzioni.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Default"));
                if (TR && ass) {
                    this.comboBoxEsecuzioni.addItem("E01");
                    this.comboBoxEsecuzioni.addItem("E09");
                } else
		if (TR) {
			    if (this.l_VG.pannelloRicercaEsecuzioniTR1 != null) {
                                for (int i=0 ; i<this.l_VG.pannelloRicercaEsecuzioniTR1.length ; i++) {
                                    if (this.l_VG.debugFlag) {
                                        Notification.show(Integer.toString(i)+"  "+this.l_VG.pannelloRicercaEsecuzioniTR1[i]);
                                    }
                                    this.comboBoxEsecuzioni.addItem(this.l_VG.pannelloRicercaEsecuzioniTR1[i]);
                                }
                            } 
                            if (this.l_VG.pannelloRicercaEsecuzioniTR2 != null) {
                                for (int i=0 ; i<this.l_VG.pannelloRicercaEsecuzioniTR2.length ; i++) {
                                    if (this.l_VG.debugFlag) {
                                        Notification.show(Integer.toString(i)+"  "+this.l_VG.pannelloRicercaEsecuzioniTR2[i]);
                                    }
                                    this.comboBoxEsecuzioni.addItem(this.l_VG.pannelloRicercaEsecuzioniTR2[i]);
                                }
                            }
		} else {
            if (this.l_VG.pannelloRicercaEsecuzioniDA != null) {
                for (int i=0 ; i<this.l_VG.pannelloRicercaEsecuzioniDA.length ; i++) {
                	if (this.l_VG.debugFlag) {
						Notification.show(Integer.toString(i)+"  "+this.l_VG.pannelloRicercaEsecuzioniDA[i]);
					}
                	this.comboBoxEsecuzioni.addItem(this.l_VG.pannelloRicercaEsecuzioniDA[i]);
                }
            }
		}
		this.comboBoxEsecuzioni.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Default"));
		this.editEnabled = true;
	}
	
	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTolleranzaPressione}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTolleranzaPressione_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxTolleranzaPortata.setValue(!this.checkBoxTolleranzaPressione.getValue());
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTolleranzaPortata}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTolleranzaPortata_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxTolleranzaPressione.setValue(!this.checkBoxTolleranzaPortata.getValue());
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxDapo}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxDapo_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		if (this.checkBoxDapo.getValue()) {
			this.checkBoxSerranda.setValue(false);
		}
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxSerranda}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxSerranda_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		if (this.checkBoxSerranda.getValue()) {
			this.checkBoxDapo.setValue(false);
		}
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAspirazioneMultipla}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAspirazioneMultipla_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAspirazioneSingola.setEnabled(false);
		this.checkBoxAspirazioneSingola.setValue(!this.checkBoxAspirazioneMultipla.getValue());
		buildComboEsecuzioni(this.l_VG.trasmissioneFlag, false);
		this.checkBoxAspirazioneSingola.setEnabled(true);
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAspirazioneSingola}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAspirazioneSingola_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAspirazioneMultipla.setEnabled(false);
		this.checkBoxAspirazioneMultipla.setValue(!this.checkBoxAspirazioneSingola.getValue());
		buildComboEsecuzioni(this.l_VG.trasmissioneFlag, false);
		this.checkBoxAspirazioneMultipla.setEnabled(true);
		this.searchView.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxFrequenza}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxFrequenza_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxEsecuzioni}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxEsecuzioni_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldTolleranzaMeno}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldTolleranzaMeno_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldTolleranzaPiu}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldTolleranzaPiu_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldrhoMin}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldrhoMin_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldCorrettorePotMotore}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldCorrettorePotMotore_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldDiametroGirante}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldDiametroGirante_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldRpmMax}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldRpmMax_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldPotenzaSonoraMax}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPotenzaSonoraMax_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.searchView.reset();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout3 = new XdevVerticalLayout();
		this.gridLayout7 = new XdevGridLayout();
		this.labelTolleranza = new XdevLabel();
		this.label6 = new XdevLabel();
		this.textFieldTolleranzaMeno = new TextFieldInteger();
		this.label3 = new XdevLabel();
		this.label4 = new XdevLabel();
		this.textFieldTolleranzaPiu = new TextFieldInteger();
		this.label8 = new XdevLabel();
		this.horizontalLayout6 = new XdevHorizontalLayout();
		this.labelTolleranzaSu = new XdevLabel();
		this.checkBoxTolleranzaPressione = new XdevCheckBox();
		this.checkBoxTolleranzaPortata = new XdevCheckBox();
		this.horizontalLayout7 = new XdevHorizontalLayout();
		this.comboBoxFrequenza = new XdevComboBox<>();
		this.label7 = new XdevLabel();
		this.horizontalLayout8 = new XdevHorizontalLayout();
		this.labelEsecuzioni = new XdevLabel();
		this.comboBoxEsecuzioni = new XdevComboBox<>();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.labelAspirazione = new XdevLabel();
		this.checkBoxAspirazioneSingola = new XdevCheckBox();
		this.checkBoxAspirazioneMultipla = new XdevCheckBox();
		this.gridLayout9 = new XdevGridLayout();
		this.labelrhoMin = new XdevLabel();
		this.textFieldrhoMin = new TextFieldInteger();
		this.label12 = new XdevLabel();
		this.gridLayout10 = new XdevGridLayout();
		this.labelCorrettorePotMotore = new XdevLabel();
		this.textFieldCorrettorePotMotore = new TextFieldInteger();
		this.label15 = new XdevLabel();
		this.horizontalLayout5 = new XdevHorizontalLayout();
		this.labelSerranda = new XdevLabel();
		this.checkBoxDapo = new XdevCheckBox();
		this.checkBoxSerranda = new XdevCheckBox();
		this.gridLayout11 = new XdevGridLayout();
		this.labelDiametroGirante = new XdevLabel();
		this.textFieldDiametroGirante = new TextFieldInteger();
		this.label17 = new XdevLabel();
		this.gridLayout12 = new XdevGridLayout();
		this.labelRpmMax = new XdevLabel();
		this.textFieldRpmMax = new TextFieldInteger();
		this.label20 = new XdevLabel();
		this.gridLayout13 = new XdevGridLayout();
		this.labelPotenzaSonora = new XdevLabel();
		this.textFieldPotenzaSonoraMax = new TextFieldInteger();
		this.label13 = new XdevLabel();
	
		this.verticalLayout3.setMargin(new MarginInfo(true, true, false, true));
		this.gridLayout7.setSpacing(false);
		this.gridLayout7.setMargin(new MarginInfo(false));
		this.labelTolleranza.setValue("toller");
		this.labelTolleranza.setContentMode(ContentMode.HTML);
		this.label6.setValue(" -");
		this.label3.setValue("%");
		this.label4.setValue(" +");
		this.label8.setValue("%");
		this.horizontalLayout6.setMargin(new MarginInfo(false));
		this.labelTolleranzaSu.setValue("tollsu");
		this.labelTolleranzaSu.setContentMode(ContentMode.HTML);
		this.checkBoxTolleranzaPressione.setCaption("pressione");
		this.checkBoxTolleranzaPressione.setCaptionAsHtml(true);
		this.checkBoxTolleranzaPortata.setCaption("portata");
		this.checkBoxTolleranzaPortata.setCaptionAsHtml(true);
		this.horizontalLayout7.setMargin(new MarginInfo(false));
		this.label7.setValue("[Hz]");
		this.horizontalLayout8.setMargin(new MarginInfo(false));
		this.labelEsecuzioni.setStyleName("align-right");
		this.labelEsecuzioni.setValue("esec");
		this.labelEsecuzioni.setContentMode(ContentMode.HTML);
		this.horizontalLayout4.setMargin(new MarginInfo(false));
		this.labelAspirazione.setStyleName("align-right");
		this.labelAspirazione.setValue("mult");
		this.labelAspirazione.setContentMode(ContentMode.HTML);
		this.checkBoxAspirazioneSingola.setCaption("multipla");
		this.checkBoxAspirazioneSingola.setCaptionAsHtml(true);
		this.checkBoxAspirazioneMultipla.setCaption("singola");
		this.checkBoxAspirazioneMultipla.setCaptionAsHtml(true);
		this.gridLayout9.setSpacing(false);
		this.gridLayout9.setMargin(new MarginInfo(false));
		this.labelrhoMin.setStyleName("align-right");
		this.labelrhoMin.setValue("rhomin");
		this.labelrhoMin.setContentMode(ContentMode.HTML);
		this.label12.setValue("%");
		this.gridLayout10.setMargin(new MarginInfo(false));
		this.labelCorrettorePotMotore.setStyleName("align-right");
		this.labelCorrettorePotMotore.setValue("correttpot");
		this.labelCorrettorePotMotore.setContentMode(ContentMode.HTML);
		this.label15.setValue("%");
		this.horizontalLayout5.setMargin(new MarginInfo(false));
		this.labelSerranda.setStyleName("align-right");
		this.labelSerranda.setValue("ser");
		this.labelSerranda.setContentMode(ContentMode.HTML);
		this.checkBoxDapo.setCaption("dapo");
		this.checkBoxDapo.setCaptionAsHtml(true);
		this.checkBoxSerranda.setCaption("serran");
		this.checkBoxSerranda.setCaptionAsHtml(true);
		this.gridLayout11.setMargin(new MarginInfo(false));
		this.labelDiametroGirante.setStyleName("align-right");
		this.labelDiametroGirante.setValue("dimg");
		this.labelDiametroGirante.setContentMode(ContentMode.HTML);
		this.label17.setValue("[mm]");
		this.gridLayout12.setMargin(new MarginInfo(false));
		this.labelRpmMax.setStyleName("align-right");
		this.labelRpmMax.setValue("rpmmax");
		this.labelRpmMax.setContentMode(ContentMode.HTML);
		this.label20.setValue("[Rpm]");
		this.gridLayout13.setMargin(new MarginInfo(false));
		this.labelPotenzaSonora.setStyleName("align-right");
		this.labelPotenzaSonora.setValue("potson");
		this.labelPotenzaSonora.setContentMode(ContentMode.HTML);
		this.label13.setValue("[dB/dB(A)]");
	
		this.gridLayout7.setColumns(7);
		this.gridLayout7.setRows(2);
		this.labelTolleranza.setWidth(100, Unit.PERCENTAGE);
		this.labelTolleranza.setHeight(-1, Unit.PIXELS);
		this.gridLayout7.addComponent(this.labelTolleranza, 0, 0);
		this.gridLayout7.setComponentAlignment(this.labelTolleranza, Alignment.TOP_RIGHT);
		this.label6.setWidth(100, Unit.PERCENTAGE);
		this.label6.setHeight(-1, Unit.PIXELS);
		this.gridLayout7.addComponent(this.label6, 1, 0);
		this.textFieldTolleranzaMeno.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTolleranzaMeno.setHeight(-1, Unit.PIXELS);
		this.gridLayout7.addComponent(this.textFieldTolleranzaMeno, 2, 0);
		this.label3.setWidth(100, Unit.PERCENTAGE);
		this.label3.setHeight(-1, Unit.PIXELS);
		this.gridLayout7.addComponent(this.label3, 3, 0);
		this.label4.setWidth(100, Unit.PERCENTAGE);
		this.label4.setHeight(-1, Unit.PIXELS);
		this.gridLayout7.addComponent(this.label4, 4, 0);
		this.textFieldTolleranzaPiu.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTolleranzaPiu.setHeight(-1, Unit.PIXELS);
		this.gridLayout7.addComponent(this.textFieldTolleranzaPiu, 5, 0);
		this.label8.setWidth(100, Unit.PERCENTAGE);
		this.label8.setHeight(-1, Unit.PIXELS);
		this.gridLayout7.addComponent(this.label8, 6, 0);
		this.gridLayout7.setColumnExpandRatio(0, 100.0F);
		this.gridLayout7.setColumnExpandRatio(1, 5.0F);
		this.gridLayout7.setColumnExpandRatio(2, 30.0F);
		this.gridLayout7.setColumnExpandRatio(3, 10.0F);
		this.gridLayout7.setColumnExpandRatio(4, 5.0F);
		this.gridLayout7.setColumnExpandRatio(5, 30.0F);
		this.gridLayout7.setColumnExpandRatio(6, 10.0F);
		final CustomComponent gridLayout7_vSpacer = new CustomComponent();
		gridLayout7_vSpacer.setSizeFull();
		this.gridLayout7.addComponent(gridLayout7_vSpacer, 0, 1, 6, 1);
		this.gridLayout7.setRowExpandRatio(1, 1.0F);
		this.labelTolleranzaSu.setWidth(100, Unit.PERCENTAGE);
		this.labelTolleranzaSu.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.labelTolleranzaSu);
		this.horizontalLayout6.setComponentAlignment(this.labelTolleranzaSu, Alignment.TOP_RIGHT);
		this.horizontalLayout6.setExpandRatio(this.labelTolleranzaSu, 18.0F);
		this.checkBoxTolleranzaPressione.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTolleranzaPressione.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.checkBoxTolleranzaPressione);
		this.horizontalLayout6.setComponentAlignment(this.checkBoxTolleranzaPressione, Alignment.TOP_CENTER);
		this.horizontalLayout6.setExpandRatio(this.checkBoxTolleranzaPressione, 20.0F);
		this.checkBoxTolleranzaPortata.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTolleranzaPortata.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.checkBoxTolleranzaPortata);
		this.horizontalLayout6.setComponentAlignment(this.checkBoxTolleranzaPortata, Alignment.TOP_CENTER);
		this.horizontalLayout6.setExpandRatio(this.checkBoxTolleranzaPortata, 10.0F);
		/*this.comboBoxFrequenza.setWidth(70, Unit.PIXELS);
		this.comboBoxFrequenza.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout7.addComponent(this.comboBoxFrequenza);
		this.label7.setWidth(100, Unit.PERCENTAGE);
		this.label7.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout7.addComponent(this.label7);
		this.horizontalLayout7.setExpandRatio(this.label7, 100.0F);*/
		this.labelEsecuzioni.setSizeUndefined();
		this.horizontalLayout8.addComponent(this.labelEsecuzioni);
		this.horizontalLayout8.setComponentAlignment(this.labelEsecuzioni, Alignment.TOP_RIGHT);
		this.horizontalLayout8.setExpandRatio(this.labelEsecuzioni, 10.0F);
		this.comboBoxEsecuzioni.setSizeUndefined();
		this.horizontalLayout8.addComponent(this.comboBoxEsecuzioni);
		this.horizontalLayout8.setComponentAlignment(this.comboBoxEsecuzioni, Alignment.TOP_CENTER);
		this.labelAspirazione.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.labelAspirazione);
		this.horizontalLayout4.setExpandRatio(this.labelAspirazione, 10.0F);
		this.checkBoxAspirazioneSingola.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.checkBoxAspirazioneSingola);
		this.horizontalLayout4.setComponentAlignment(this.checkBoxAspirazioneSingola, Alignment.TOP_CENTER);
		this.horizontalLayout4.setExpandRatio(this.checkBoxAspirazioneSingola, 10.0F);
		this.checkBoxAspirazioneMultipla.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxAspirazioneMultipla.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.checkBoxAspirazioneMultipla);
		this.horizontalLayout4.setComponentAlignment(this.checkBoxAspirazioneMultipla, Alignment.TOP_CENTER);
		this.horizontalLayout4.setExpandRatio(this.checkBoxAspirazioneMultipla, 10.0F);
		this.gridLayout9.setColumns(3);
		this.gridLayout9.setRows(2);
		this.labelrhoMin.setWidth(100, Unit.PERCENTAGE);
		this.labelrhoMin.setHeight(-1, Unit.PIXELS);
		this.gridLayout9.addComponent(this.labelrhoMin, 0, 0);
		this.gridLayout9.setComponentAlignment(this.labelrhoMin, Alignment.TOP_RIGHT);
		this.textFieldrhoMin.setWidth(100, Unit.PERCENTAGE);
		this.textFieldrhoMin.setHeight(-1, Unit.PIXELS);
		this.gridLayout9.addComponent(this.textFieldrhoMin, 1, 0);
		this.label12.setWidth(100, Unit.PERCENTAGE);
		this.label12.setHeight(-1, Unit.PIXELS);
		this.gridLayout9.addComponent(this.label12, 2, 0);
		this.gridLayout9.setColumnExpandRatio(0, 30.0F);
		this.gridLayout9.setColumnExpandRatio(1, 10.0F);
		this.gridLayout9.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayout9_vSpacer = new CustomComponent();
		gridLayout9_vSpacer.setSizeFull();
		this.gridLayout9.addComponent(gridLayout9_vSpacer, 0, 1, 2, 1);
		this.gridLayout9.setRowExpandRatio(1, 1.0F);
		this.gridLayout10.setColumns(3);
		this.gridLayout10.setRows(2);
		this.labelCorrettorePotMotore.setWidth(100, Unit.PERCENTAGE);
		this.labelCorrettorePotMotore.setHeight(-1, Unit.PIXELS);
		this.gridLayout10.addComponent(this.labelCorrettorePotMotore, 0, 0);
		this.textFieldCorrettorePotMotore.setWidth(100, Unit.PERCENTAGE);
		this.textFieldCorrettorePotMotore.setHeight(-1, Unit.PIXELS);
		this.gridLayout10.addComponent(this.textFieldCorrettorePotMotore, 1, 0);
		this.label15.setSizeUndefined();
		this.gridLayout10.addComponent(this.label15, 2, 0);
		this.gridLayout10.setColumnExpandRatio(0, 40.0F);
		this.gridLayout10.setColumnExpandRatio(1, 10.0F);
		this.gridLayout10.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayout10_vSpacer = new CustomComponent();
		gridLayout10_vSpacer.setSizeFull();
		this.gridLayout10.addComponent(gridLayout10_vSpacer, 0, 1, 2, 1);
		this.gridLayout10.setRowExpandRatio(1, 1.0F);
		this.labelSerranda.setWidth(100, Unit.PERCENTAGE);
		this.labelSerranda.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout5.addComponent(this.labelSerranda);
		this.horizontalLayout5.setExpandRatio(this.labelSerranda, 10.0F);
		this.checkBoxDapo.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxDapo.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout5.addComponent(this.checkBoxDapo);
		this.horizontalLayout5.setComponentAlignment(this.checkBoxDapo, Alignment.TOP_CENTER);
		this.horizontalLayout5.setExpandRatio(this.checkBoxDapo, 10.0F);
		this.checkBoxSerranda.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxSerranda.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout5.addComponent(this.checkBoxSerranda);
		this.horizontalLayout5.setComponentAlignment(this.checkBoxSerranda, Alignment.TOP_CENTER);
		this.horizontalLayout5.setExpandRatio(this.checkBoxSerranda, 10.0F);
		this.gridLayout11.setColumns(3);
		this.gridLayout11.setRows(2);
		this.labelDiametroGirante.setWidth(100, Unit.PERCENTAGE);
		this.labelDiametroGirante.setHeight(-1, Unit.PIXELS);
		this.gridLayout11.addComponent(this.labelDiametroGirante, 0, 0);
		this.textFieldDiametroGirante.setWidth(100, Unit.PERCENTAGE);
		this.textFieldDiametroGirante.setHeight(-1, Unit.PIXELS);
		this.gridLayout11.addComponent(this.textFieldDiametroGirante, 1, 0);
		this.gridLayout11.setComponentAlignment(this.textFieldDiametroGirante, Alignment.TOP_CENTER);
		this.label17.setWidth(100, Unit.PERCENTAGE);
		this.label17.setHeight(-1, Unit.PIXELS);
		this.gridLayout11.addComponent(this.label17, 2, 0);
		this.gridLayout11.setComponentAlignment(this.label17, Alignment.TOP_CENTER);
		this.gridLayout11.setColumnExpandRatio(0, 40.0F);
		this.gridLayout11.setColumnExpandRatio(1, 10.0F);
		this.gridLayout11.setColumnExpandRatio(2, 5.0F);
		final CustomComponent gridLayout11_vSpacer = new CustomComponent();
		gridLayout11_vSpacer.setSizeFull();
		this.gridLayout11.addComponent(gridLayout11_vSpacer, 0, 1, 2, 1);
		this.gridLayout11.setRowExpandRatio(1, 1.0F);
		this.gridLayout12.setColumns(3);
		this.gridLayout12.setRows(2);
		this.labelRpmMax.setWidth(100, Unit.PERCENTAGE);
		this.labelRpmMax.setHeight(-1, Unit.PIXELS);
		this.gridLayout12.addComponent(this.labelRpmMax, 0, 0);
		this.textFieldRpmMax.setWidth(100, Unit.PERCENTAGE);
		this.textFieldRpmMax.setHeight(-1, Unit.PIXELS);
		this.gridLayout12.addComponent(this.textFieldRpmMax, 1, 0);
		this.label20.setWidth(100, Unit.PERCENTAGE);
		this.label20.setHeight(-1, Unit.PIXELS);
		this.gridLayout12.addComponent(this.label20, 2, 0);
		this.gridLayout12.setColumnExpandRatio(0, 20.0F);
		this.gridLayout12.setColumnExpandRatio(1, 10.0F);
		this.gridLayout12.setColumnExpandRatio(2, 5.0F);
		final CustomComponent gridLayout12_vSpacer = new CustomComponent();
		gridLayout12_vSpacer.setSizeFull();
		this.gridLayout12.addComponent(gridLayout12_vSpacer, 0, 1, 2, 1);
		this.gridLayout12.setRowExpandRatio(1, 1.0F);
		this.gridLayout13.setColumns(3);
		this.gridLayout13.setRows(2);
		this.labelPotenzaSonora.setWidth(100, Unit.PERCENTAGE);
		this.labelPotenzaSonora.setHeight(-1, Unit.PIXELS);
		this.gridLayout13.addComponent(this.labelPotenzaSonora, 0, 0);
		this.textFieldPotenzaSonoraMax.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPotenzaSonoraMax.setHeight(-1, Unit.PIXELS);
		this.gridLayout13.addComponent(this.textFieldPotenzaSonoraMax, 1, 0);
		this.label13.setWidth(100, Unit.PERCENTAGE);
		this.label13.setHeight(-1, Unit.PIXELS);
		this.gridLayout13.addComponent(this.label13, 2, 0);
		this.gridLayout13.setColumnExpandRatio(0, 20.0F);
		this.gridLayout13.setColumnExpandRatio(1, 10.0F);
		this.gridLayout13.setColumnExpandRatio(2, 20.0F);
		final CustomComponent gridLayout13_vSpacer = new CustomComponent();
		gridLayout13_vSpacer.setSizeFull();
		this.gridLayout13.addComponent(gridLayout13_vSpacer, 0, 1, 2, 1);
		this.gridLayout13.setRowExpandRatio(1, 1.0F);
		this.gridLayout7.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.gridLayout7);
		this.horizontalLayout6.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout6.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout6);
		this.horizontalLayout7.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout7);
		this.horizontalLayout8.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout8.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout8);
		this.horizontalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout4);
		this.gridLayout9.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout9.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.gridLayout9);
		this.gridLayout10.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout10.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.gridLayout10);
		this.verticalLayout3.setComponentAlignment(this.gridLayout10, Alignment.MIDDLE_CENTER);
		this.horizontalLayout5.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout5);
		this.gridLayout11.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout11.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.gridLayout11);
		this.gridLayout12.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout12.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.gridLayout12);
		this.gridLayout13.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout13.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.gridLayout13);
		final CustomComponent verticalLayout3_spacer = new CustomComponent();
		verticalLayout3_spacer.setSizeFull();
		this.verticalLayout3.addComponent(verticalLayout3_spacer);
		this.verticalLayout3.setExpandRatio(verticalLayout3_spacer, 1.0F);
		this.verticalLayout3.setSizeFull();
		this.setContent(this.verticalLayout3);
//		this.setWidth(30, Unit.PERCENTAGE);
		this.setHeight(80, Unit.PERCENTAGE);
	
		this.textFieldTolleranzaMeno.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.textFieldTolleranzaMeno_textChange(event);
			}
		});
		this.textFieldTolleranzaPiu.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.textFieldTolleranzaPiu_textChange(event);
			}
		});
		this.checkBoxTolleranzaPressione.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.checkBoxTolleranzaPressione_valueChange(event);
			}
		});
		this.checkBoxTolleranzaPortata.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.checkBoxTolleranzaPortata_valueChange(event);
			}
		});
		this.comboBoxFrequenza.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.comboBoxFrequenza_valueChange(event);
			}
		});
		this.comboBoxEsecuzioni.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.comboBoxEsecuzioni_valueChange(event);
			}
		});
		this.checkBoxAspirazioneSingola.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.checkBoxAspirazioneSingola_valueChange(event);
			}
		});
		this.checkBoxAspirazioneMultipla.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.checkBoxAspirazioneMultipla_valueChange(event);
			}
		});
		this.textFieldrhoMin.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.textFieldrhoMin_textChange(event);
			}
		});
		this.textFieldCorrettorePotMotore.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.textFieldCorrettorePotMotore_textChange(event);
			}
		});
		this.checkBoxDapo.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.checkBoxDapo_valueChange(event);
			}
		});
		this.checkBoxSerranda.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.checkBoxSerranda_valueChange(event);
			}
		});
		this.textFieldDiametroGirante.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.textFieldDiametroGirante_textChange(event);
			}
		});
		this.textFieldRpmMax.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.textFieldRpmMax_textChange(event);
			}
		});
		this.textFieldPotenzaSonoraMax.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloRicercaFiltriSelezione.this.textFieldPotenzaSonoraMax_textChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelTolleranza, label6, label3, label4, label8, labelTolleranzaSu, label7, labelEsecuzioni,
			labelAspirazione, labelrhoMin, label12, labelCorrettorePotMotore, label15, labelSerranda, labelDiametroGirante,
			label17, labelRpmMax, label20, labelPotenzaSonora, label13;
	private XdevHorizontalLayout horizontalLayout6, horizontalLayout7, horizontalLayout8, horizontalLayout4,
			horizontalLayout5;
	private XdevCheckBox checkBoxTolleranzaPressione, checkBoxTolleranzaPortata, checkBoxAspirazioneSingola,
			checkBoxAspirazioneMultipla, checkBoxDapo, checkBoxSerranda;
	private XdevGridLayout gridLayout7, gridLayout9, gridLayout10, gridLayout11, gridLayout12, gridLayout13;
	private XdevVerticalLayout verticalLayout3;
	private TextFieldInteger textFieldTolleranzaMeno, textFieldTolleranzaPiu, textFieldrhoMin, textFieldCorrettorePotMotore,
			textFieldDiametroGirante, textFieldRpmMax, textFieldPotenzaSonoraMax;
	private XdevComboBox<CustomComponent> comboBoxFrequenza, comboBoxEsecuzioni;
	// </generated-code>


}
