package com.cit.sellfan.ui.pannelli;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.SelezioneCorrente;
import cit.sellfan.classi.Silenziatore;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;

public class pannelloInternoSilenziatore extends Panel {
	private final VariabiliGlobali l_VG;
	private final Container containerSilenziatori = new IndexedContainer();
	private ArrayList<Silenziatore> listaInterna;
	private final ArrayList<Coppia> listaSelezione = new ArrayList<>();
	final CheckBox chkSilenziatore = new CheckBox();
	final CheckBox chkTramoggia = new CheckBox();
	final CheckBox chkSupporto = new CheckBox();
	private boolean Mandata;
	
	public void Nazionalizza(final boolean isMandata) {
		this.labelTitolo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Selezione Silenziatore e Accessori Silenziatore"));
		}
	
	//elenco dei silenziatori

	public pannelloInternoSilenziatore(final VariabiliGlobali vg) {
		super();
		this.initUI();
		this.l_VG = vg;
		this.containerSilenziatori.removeAllItems();
		this.containerSilenziatori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerSilenziatori.addContainerProperty("Code", String.class, "");
		this.containerSilenziatori.addContainerProperty("Description", String.class, "");
		this.tableInternaAccessori.setContainerDataSource(this.containerSilenziatori);
	}

	public void loadTabellaInterna(final Silenziatore selected, final boolean isMandata) {
		final SelezioneCorrente selCor = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		this.tableInternaAccessori.removeAllItems();
		final Object silenziatore[] = new Object[3];
		final Object tramoggia[] = new Object[3];
		final Object supporto[] = new Object[3];
		
		this.chkSilenziatore.setImmediate(true);
		this.chkTramoggia.setImmediate(true);
		this.chkSupporto.setImmediate(true);
		
		if (isMandata) {
			this.chkSilenziatore.setValue(selCor.vuoleSilMandata);
			this.chkTramoggia.setValue(selCor.vuoleTramoggiaMandata);
		} else {
			this.chkSilenziatore.setValue(selCor.vuoleSilAspirazione);
			this.chkTramoggia.setValue(selCor.vuoleTramoggiaAspirazione);
			this.chkSupporto.setValue(selCor.vuoleSostegno);
		}
		final String dimBoccaMandata = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).AxBMandata;
		final String dimBoccaAspirazione = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).FiAspirazione;
		silenziatore[0] = this.chkSilenziatore;
		silenziatore[1] = selected.codiceSilenziatore;
		silenziatore[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Cilindrico ") + selected.descrizioneSilenziatore;
		tramoggia[0] = this.chkTramoggia;
		tramoggia[1] = "";
		if (isMandata) {
			tramoggia[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Tramoggia") + ": " + dimBoccaMandata + selCor.descTramMandata;
		} else {
			tramoggia[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Riduzione Circolare") + ": " + dimBoccaAspirazione + selCor.descTramAspirazione;
		}
		if (!isMandata) {
		supporto[0] = this.chkSupporto;
		supporto[1] = selected.sostegnoSilenziatore.codiceSostegno;
		supporto[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Supporto anteriore per silenziatore in Aspirazione") + ": " + selected.sostegnoSilenziatore.descrizioneSostegno; }
		
		this.tableInternaAccessori.addItem(silenziatore, 1);
		this.tableInternaAccessori.addItem(tramoggia, 2);
		if (!isMandata) {
		this.tableInternaAccessori.addItem(supporto, 3);
		}
		
		
	}
	
	public void returnSelected(final boolean isMandata) {
		if (isMandata) {
			this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSilMandata = this.chkSilenziatore.getValue();
			this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleTramoggiaMandata = this.chkTramoggia.getValue();
		} else {
			this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSilAspirazione = this.chkSilenziatore.getValue();
			this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleTramoggiaAspirazione = this.chkTramoggia.getValue();
			this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSostegno = this.chkSupporto.getValue();
		}
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
		//this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
	}
	
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelTitolo = new XdevLabel();
		this.labelSostegno = new XdevLabel();
		this.tableInternaAccessori = new XdevTable<>();
		this.checkBoxSostegno = new XdevCheckBox();
		this.labelTitolo.setContentMode(ContentMode.HTML);
		this.tableInternaAccessori.setStyleName("small striped");
		this.checkBoxSostegno.setCaption("Aggiungere supporto anteriore");

		
		this.verticalLayout.addComponent(this.labelTitolo);
		this.verticalLayout.setComponentAlignment(this.labelTitolo, Alignment.MIDDLE_CENTER);
		this.tableInternaAccessori.setSizeFull();
		this.verticalLayout.addComponent(this.tableInternaAccessori);
		this.verticalLayout.setComponentAlignment(this.tableInternaAccessori, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableInternaAccessori, 100.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setWidth(80, Unit.PERCENTAGE);
		this.setHeight(80, Unit.PERCENTAGE);
		}

	private XdevLabel labelTitolo, labelSostegno;
	private XdevTable<CustomComponent> tableInternaAccessori;
	private XdevCheckBox checkBoxSostegno;
	private XdevVerticalLayout verticalLayout;
}

