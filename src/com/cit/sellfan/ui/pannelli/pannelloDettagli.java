package com.cit.sellfan.ui.pannelli;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;

import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.Box;
import cit.sellfan.classi.Silenziatore;

public class pannelloDettagli extends Panel {
	private final VariabiliGlobali l_VG;
	private final ArrayList<XdevLabel> elencoParametri = new ArrayList<>();
	String path;
	
	public void Nazionalizza() {
		this.labelTitolo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Dettagli Tecnici"));
		}
	
	//elenco dei silenziatori

	public pannelloDettagli(final VariabiliGlobali vg) {
		super();
		this.initUI();
		this.l_VG = vg;
		this.path = this.l_VG.Paths.rootResources;
		
		this.elencoParametri.add(this.dim0);
		this.dim0.setVisible(false);
		this.elencoParametri.add(this.dim1);
		this.dim1.setVisible(false);
		this.elencoParametri.add(this.dim2);
		this.dim2.setVisible(false);
		this.elencoParametri.add(this.dim3);
		this.dim3.setVisible(false);
		this.elencoParametri.add(this.dim4);
		this.dim4.setVisible(false);
		this.elencoParametri.add(this.dim5);
		this.dim5.setVisible(false);
		this.elencoParametri.add(this.dim6);
		this.dim6.setVisible(false);
		this.elencoParametri.add(this.dim7);
		this.dim7.setVisible(false);
		this.elencoParametri.add(this.dim8);
		this.dim8.setVisible(false);
		this.elencoParametri.add(this.dim9);
		this.dim9.setVisible(false);
	}
	
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.verticalLayout = new XdevVerticalLayout();
		this.image = new XdevImage();
		this.image.setWidth("100%");
		this.horizontalLayout.addComponent(this.image);
		this.horizontalLayout.addComponent(this.verticalLayout);
		this.horizontalLayout.setWidth("100%");
		this.horizontalLayout.setHeight("100%");
		this.horizontalLayout.setExpandRatio(this.image, 6.0F);
		this.horizontalLayout.setExpandRatio(this.verticalLayout, 4.0F);
		
		this.dim0 = new XdevLabel();
		this.dim1 = new XdevLabel();
		this.dim2 = new XdevLabel();
		this.dim3 = new XdevLabel();
		this.dim4 = new XdevLabel();
		this.dim5 = new XdevLabel();
		this.dim6 = new XdevLabel();
		this.dim7 = new XdevLabel();
		this.dim8 = new XdevLabel();
		this.dim9 = new XdevLabel();
		
		this.dim0.setValue("Label");
		this.dim1.setValue("Label");
		this.dim2.setValue("Label");
		this.dim3.setValue("Label");
		this.dim4.setValue("Label");
		this.dim5.setValue("Label");
		this.dim6.setValue("Label");
		this.dim7.setValue("Label");
		this.dim8.setValue("Label");
		this.dim9.setValue("Label");
		
		this.dim0.setContentMode(ContentMode.HTML);
		this.dim1.setContentMode(ContentMode.HTML);
		this.dim2.setContentMode(ContentMode.HTML);
		this.dim3.setContentMode(ContentMode.HTML);
		this.dim4.setContentMode(ContentMode.HTML);
		this.dim5.setContentMode(ContentMode.HTML);
		this.dim6.setContentMode(ContentMode.HTML);
		this.dim7.setContentMode(ContentMode.HTML);
		this.dim8.setContentMode(ContentMode.HTML);
		this.dim9.setContentMode(ContentMode.HTML);
		
		this.dim0.setWidth(100, Unit.PERCENTAGE);
		this.dim0.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim0);
		this.verticalLayout.setComponentAlignment(this.dim0, Alignment.MIDDLE_CENTER);
		this.dim1.setWidth(100, Unit.PERCENTAGE);
		this.dim1.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim1);
		this.verticalLayout.setComponentAlignment(this.dim1, Alignment.MIDDLE_CENTER);
		this.dim2.setWidth(100, Unit.PERCENTAGE);
		this.dim2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim2);
		this.verticalLayout.setComponentAlignment(this.dim2, Alignment.MIDDLE_CENTER);
		this.dim3.setWidth(100, Unit.PERCENTAGE);
		this.dim3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim3);
		this.verticalLayout.setComponentAlignment(this.dim3, Alignment.MIDDLE_CENTER);
		this.dim4.setWidth(100, Unit.PERCENTAGE);
		this.dim4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim4);
		this.verticalLayout.setComponentAlignment(this.dim4, Alignment.MIDDLE_CENTER);
		this.dim5.setWidth(100, Unit.PERCENTAGE);
		this.dim5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim5);
		this.verticalLayout.setComponentAlignment(this.dim5, Alignment.MIDDLE_CENTER);
		this.dim6.setWidth(100, Unit.PERCENTAGE);
		this.dim6.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim6);
		this.verticalLayout.setComponentAlignment(this.dim6, Alignment.MIDDLE_CENTER);
		this.dim7.setWidth(100, Unit.PERCENTAGE);
		this.dim7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim7);
		this.verticalLayout.setComponentAlignment(this.dim7, Alignment.MIDDLE_CENTER);
		this.dim8.setWidth(100, Unit.PERCENTAGE);
		this.dim8.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim8);
		this.verticalLayout.setComponentAlignment(this.dim8, Alignment.MIDDLE_CENTER);
		this.dim9.setWidth(100, Unit.PERCENTAGE);
		this.dim9.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.dim9);
		this.verticalLayout.setComponentAlignment(this.dim9, Alignment.MIDDLE_CENTER);
		
		
		this.labelTitolo = new XdevLabel();
		this.labelTitolo.setContentMode(ContentMode.HTML);

		this.setContent(this.horizontalLayout);
		}

	private XdevLabel labelTitolo;
	private XdevHorizontalLayout horizontalLayout;
	private XdevImage image;
	private XdevVerticalLayout verticalLayout;
	private XdevLabel dim0, dim1, dim2, dim3, dim4, dim5, dim6, dim7, dim8, dim9;

	public void loadSilenziatore(final Silenziatore actualSil) {
		final String fileName = this.path + "mzimg/silenziatori/sil.png";
		final StreamResource streamImage = this.l_VG.getStreamResourceForImageFile(fileName);
		if (streamImage != null) {
			this.image.setSource(streamImage);
		}
		this.elencoParametri.get(0).setValue("<b><center>D:<font color='blue'>" + actualSil.diametroCircolare);
		this.elencoParametri.get(0).setVisible(true);
		this.elencoParametri.get(1).setValue("<b><center>D1:<font color='blue'>" + actualSil.D1);
		this.elencoParametri.get(1).setVisible(true);
		this.elencoParametri.get(2).setValue("<b><center>D2:<font color='blue'>" + actualSil.D2);
		this.elencoParametri.get(2).setVisible(true);
		this.elencoParametri.get(3).setValue("<b><center>E:<font color='blue'>" + actualSil.E);
		this.elencoParametri.get(3).setVisible(true);
		this.elencoParametri.get(4).setValue("<b><center>F:<font color='blue'>" + actualSil.F);
		this.elencoParametri.get(4).setVisible(true);
		this.elencoParametri.get(5).setValue("<b><center>L:<font color='blue'>" + actualSil.L);
		this.elencoParametri.get(5).setVisible(true);
		}
	
	public void loadTramoggia(final Silenziatore actualSil) {
		final String fileName = this.path + "mzimg/silenziatori/tramoggia.png";
		final StreamResource streamImage = this.l_VG.getStreamResourceForImageFile(fileName);
		if (streamImage != null) {
			this.image.setSource(streamImage);
		}
		this.elencoParametri.get(0).setValue("<b><center>d: <font color='blue'>" + actualSil.tramoggiaConnessa.diametroCircolare);
		this.elencoParametri.get(0).setVisible(true);
		this.elencoParametri.get(1).setValue("<b><center>l: <font color='blue'>" + actualSil.tramoggiaConnessa.lunghezza);
		this.elencoParametri.get(1).setVisible(true);
	}
	
	public void loadAccessorio(final Accessorio acc) {
		final String fileName = this.path + "mzimg/accessori/" + acc.Codice + ".png";
		final StreamResource streamImage = this.l_VG.getStreamResourceForImageFile(fileName);
		if (streamImage != null) {
			this.image.setSource(streamImage);
		}
		for (int i = 1; i < acc.parametriLabel.length; i++) {
			this.elencoParametri.get(i).setValue("<b><center>" + this.l_VG.utility.rimpiazzaLettereGreche(acc.parametriLabel[i]) + ": <font color='blue'>" + acc.parametriVal[i]);
			this.elencoParametri.get(i).setVisible(true);
		}
	}

	public void loadBox(final Box box) {
		final String fileName = this.path + "mzimg/silenziatori/box.png";
		final StreamResource streamImage = this.l_VG.getStreamResourceForImageFile(fileName);
		if (streamImage != null) {
			this.image.setSource(streamImage);
		}
		this.elencoParametri.get(0).setValue("<b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Dimensioni Base") + ": <font color='blue'> " + 
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.box.baseMis);
		this.elencoParametri.get(1).setValue("<b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Ingombro Base") + ": <font color='blue'> " + 
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.box.baseIng);
		this.elencoParametri.get(2).setValue("<b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Altezza") + ": <font color='blue'> " + 
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.box.altezzaMis);
		this.elencoParametri.get(3).setValue("<b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Ingombro Altezza") + ": <font color='blue'> " + 
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.box.altezzaIng);
		this.elencoParametri.get(0).setVisible(true);
		this.elencoParametri.get(1).setVisible(true);
		this.elencoParametri.get(2).setVisible(true);
		this.elencoParametri.get(3).setVisible(true);
	}
}

