package com.cit.sellfan.ui.pannelli;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.event.FieldEvents;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.sellfan.classi.ventilatore.Ventilatore;

public class pannelloCondizioni extends Panel {
	private VariabiliGlobali l_VG;
	public boolean onlyForVentilatore = false;
	private double densitaFluido;

	/**
	 * 
	 */
	public pannelloCondizioni() {
		super();
		this.initUI();
		this.comboBoxFrequenza.addItem("50");
		this.comboBoxFrequenza.addItem("60");
		this.textFieldTemperatura.setConSegno(true);
		this.textFieldAltezza.setConSegno(true);
		this.textFieldUmidita.setEnabled(false);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void Nazionalizza() {
		if (this.onlyForVentilatore) {
			this.labelTitoloPreferenze.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatore") + " " + this.l_VG.utilityTraduzioni.TraduciStringa("Condizioni Ambientali") + "</html>");
		} else {
			this.labelTitoloPreferenze.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Condizioni Ambientali") + "</html>");
		}
		this.labelTemperatura.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Temperatura"));
		this.labelAltezza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Altezza"));
		this.labelFrequenza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza"));
		this.labelUmidita.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Umidità"));
	}

	public void initPannello() {
		Nazionalizza();
		this.textFieldUmidita.setValue(this.l_VG.catalogoCondizioniUmidita);
		if (this.onlyForVentilatore) {
			this.textFieldTemperatura.setValue((int)this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.VentilatoreCurrent.UMTemperatura.index, this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura));
			this.textFieldAltezza.setValue((int)(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.VentilatoreCurrent.CAProgettazione.altezza, this.l_VG.VentilatoreCurrent.UMAltezza)));
			this.textFieldDensita020.setValue(this.l_VG.VentilatoreCurrent.CARiferimento.rho);
			this.labelTemperaturaUM.setValue(this.l_VG.VentilatoreCurrent.UMTemperatura.simbolo);
			this.labelAltezzaUM.setValue(this.l_VG.VentilatoreCurrent.UMAltezza.simbolo);
	        this.comboBoxFrequenza.setValue(Integer.toString(this.l_VG.VentilatoreCurrent.CAProgettazione.frequenza));
		} else {
			this.textFieldTemperatura.setValue((int)this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, this.l_VG.CACatalogo.temperatura));
			this.textFieldAltezza.setValue((int)(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.CACatalogo.altezza, this.l_VG.UMAltezzaCorrente)));
			this.textFieldDensita020.setValue(this.l_VG.CA020Catalogo.rho);
			this.labelTemperaturaUM.setValue(this.l_VG.UMTemperaturaCorrente.simbolo);
			this.labelAltezzaUM.setValue(this.l_VG.UMAltezzaCorrente.simbolo);
			if (this.l_VG.catalogoCondizioniFrequenza.equals("60 Hz")) {
				this.comboBoxFrequenza.setValue("60");
			} else {
				this.comboBoxFrequenza.setValue("50");
			}
		}
		enableUmidita();
		buildDenditaCorrente();
	}

	public void enableUmidita() {
		if (this.textFieldDensita020.getDoubleValue() == 1.204) {
			this.textFieldUmidita.setEnabled(true);
		} else {
			this.textFieldUmidita.setEnabled(false);
		}
	}
	
	public void azioneReset() {
		resetPreferenze();
		azioneOK();
	}

	public void azioneOK() {
		OKPreferenze();
	}

	private void resetPreferenze() {
		this.l_VG.CACatalogo.temperatura = 20;
		this.textFieldTemperatura.setValue(this.l_VG.CACatalogo.temperatura);
		this.l_VG.CACatalogo.altezza = 0;
		this.textFieldAltezza.setValue(this.l_VG.CACatalogo.altezza);
		this.l_VG.CACatalogo.rho = 1.204;
		this.textFieldDensita020.setValue(this.l_VG.CACatalogo.rho);
		this.textFieldUmidita.setValue(0);
		buildDenditaCorrente();
		this.comboBoxFrequenza.setValue("50");
	}

	private void OKPreferenze() {
		if (this.comboBoxFrequenza.getValue() == null) {
			return;
		}
		this.l_VG.catalogoCondizioniAltezza = Double.toString(this.l_VG.utilityUnitaMisura.fromXXTom(Double.parseDouble(this.textFieldAltezza.getValue()), this.l_VG.UMAltezzaCorrente));
		this.l_VG.CACatalogo.altezza = Double.parseDouble(this.l_VG.catalogoCondizioniAltezza);
                this.l_VG.catalogoCondizioniTemperatura = Double.toString(this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, Double.parseDouble(this.textFieldTemperatura.getValue())));
		this.l_VG.CACatalogo.temperatura = Double.parseDouble(this.l_VG.catalogoCondizioniTemperatura);
                int vecchiaFrequenza = 50;
                if (this.l_VG.VentilatoreCurrent!=null) {
                    vecchiaFrequenza = this.l_VG.VentilatoreCurrent.CAProgettazione.frequenza; 
                    this.l_VG.VentilatoreCurrent.CAProgettazione.frequenza = this.l_VG.CACatalogo.frequenza;
                } 
                this.l_VG.catalogoCondizioniFrequenza = this.comboBoxFrequenza.getValue().toString(); 
		this.l_VG.CACatalogo.frequenza = Integer.parseInt(this.l_VG.catalogoCondizioniFrequenza);
                this.l_VG.catalogoCondizioniUmidita = this.textFieldUmidita.getIntegerValue();
                this.l_VG.CACatalogo.umidita = this.l_VG.catalogoCondizioniUmidita;
		buildDenditaCorrente();
                this.l_VG.CACatalogo.rho = this.densitaFluido;
		final int newFreq = Integer.parseInt(this.comboBoxFrequenza.getValue().toString());
                this.l_VG.VentilatoreCurrent.CAProgettazione = l_VG.CACatalogo;
                if (this.onlyForVentilatore) {
			if (this.l_VG.VentilatoreCurrent == null) {
				return;
			}
			cambiaSingoloVentilatore(this.l_VG.VentilatoreCurrent, newFreq, vecchiaFrequenza);
/*
			if (this.comboBoxFrequenza.getValue() != null) {
				this.l_VG.VentilatoreCurrent.CAProgettazione.frequenza = Integer.parseInt(this.comboBoxFrequenza.getValue().toString());
			}
			this.l_VG.VentilatoreCurrent.CAProgettazione.altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldAltezza.getIntegerValue(), this.l_VG.UMAltezzaCorrente);
			this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getIntegerValue());
			this.l_VG.VentilatoreCurrent.CAProgettazione.rho = this.densitaFluido;
			this.l_VG.VentilatoreCurrent.CARiferimento.rho = this.textFieldDensita020.getDoubleValue();
			if (oldFreq != this.l_VG.VentilatoreCurrent.CAProgettazione.frequenza) {
				this.l_VG.utilityCliente.cambiaVentilatoreHz(this.l_VG.VentilatoreCurrent, oldFreq, this.l_VG.VentilatoreCurrent.CAProgettazione.frequenza);
			}
*/
		} else {
			for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromCatalogo.size() ; i++) {
				final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromCatalogo.get(i);
				cambiaSingoloVentilatore(l_v, newFreq, vecchiaFrequenza);
			}
/*
			if (this.comboBoxFrequenza.getValue() != null) {
				this.l_VG.CACatalogo.frequenza = Integer.parseInt(this.comboBoxFrequenza.getValue().toString());
			}
	        this.l_VG.CACatalogo.altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldAltezza.getIntegerValue(), this.l_VG.UMAltezzaCorrente);
	        this.l_VG.CACatalogo.temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getIntegerValue());
	        this.l_VG.CACatalogo.rho = this.densitaFluido;
	        this.l_VG.CA020Catalogo.rho = this.textFieldDensita020.getDoubleValue();
	        this.l_VG.CARicerca = (CondizioniAmbientali)this.l_VG.CACatalogo.clone();
	        this.l_VG.CA020Ricerca = (CondizioniAmbientali)this.l_VG.CA020Catalogo.clone();
	        this.l_VG.CAFree = (CondizioniAmbientali)this.l_VG.CACatalogo.clone();
	        this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CARicerca.temperatura, this.l_VG.CARicerca.altezza);
	        this.l_VG.dbConfig.storeCACatalogo(this.l_VG.CACatalogo);
	        this.l_VG.dbConfig.storeCA020Catalogo(this.l_VG.CA020Catalogo);
*/
		}
	}
	
	private void cambiaSingoloVentilatore(final Ventilatore l_v, final int newFreq, final int oldFreq) {
		//final int oldFreq = l_v.CAProgettazione.frequenza;
		l_v.CAProgettazione.frequenza = newFreq;
		l_v.CAProgettazione.altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldAltezza.getIntegerValue(), this.l_VG.UMAltezzaCorrente);
		l_v.CAProgettazione.temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getIntegerValue());
		l_v.CAProgettazione.rho = this.densitaFluido;  
		l_v.CARiferimento.rho = this.textFieldDensita020.getDoubleValue();
		this.l_VG.utilityCliente.cambiaVentilatoreHz(l_v, oldFreq, newFreq);
	}

	private void buildDenditaCorrente() {
		//l_VG.MainView.setMemo(Integer.toString(l_VG.utilityFisica.getTemperaturaRiferimantoC()));
        String l_str = this.l_VG.utilityTraduzioni.TraduciStringa("Densità fluido a") + " ";
        this.labelDensita20C.setValue(l_str + "20 [C°]");
        this.labelDensitaXXC.setValue(l_str + Integer.toString(this.textFieldTemperatura.getIntegerValue()) + " " + this.l_VG.UMTemperaturaCorrente.simbolo);
        l_str = this.l_VG.utilityTraduzioni.TraduciStringa("e") + " 0 [m] " + this.l_VG.utilityTraduzioni.TraduciStringa("sul livello del mare");
        this.labelDensita0m.setValue(l_str);
        l_str = this.l_VG.utilityTraduzioni.TraduciStringa("e") + " " + Integer.toString(this.textFieldAltezza.getIntegerValue()) + " " + this.l_VG.UMAltezzaCorrente.simbolo + " " + this.l_VG.utilityTraduzioni.TraduciStringa("sul livello del mare");
        this.labelDensitaXXm.setValue(l_str);
        final double tCelsius = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getIntegerValue());
        final double altezzam = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldAltezza.getIntegerValue(), this.l_VG.UMAltezzaCorrente);
        this.densitaFluido = this.l_VG.utilityFisica.calcolaDensitaFuido(this.textFieldDensita020.getDoubleValue(), tCelsius, altezzam, 0., this.textFieldUmidita.getDoubleValue());
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(4);
        this.labelDensitaXXCXXm.setValue(this.l_VG.fmtNd.format(this.densitaFluido));
	}
	
	private void textFieldUmidita_textChange(final FieldEvents.TextChangeEvent event) {
		if (this.textFieldUmidita.getIntegerValue() > 100) {
			this.textFieldUmidita.setValue(100);
		}
		buildDenditaCorrente();
	}
	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldTemperatura}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldTemperatura_textChange(final FieldEvents.TextChangeEvent event) {
		buildDenditaCorrente();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldAltezza}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldAltezza_textChange(final FieldEvents.TextChangeEvent event) {
		buildDenditaCorrente();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldDensita020}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldDensita020_textChange(final FieldEvents.TextChangeEvent event) {
		enableUmidita();
		buildDenditaCorrente();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.gridLayoutSetupPreferenze = new XdevGridLayout();
		this.labelTitoloPreferenze = new XdevLabel();
		this.labelTemperatura = new XdevLabel();
		this.textFieldTemperatura = new TextFieldInteger();
		this.labelTemperaturaUM = new XdevLabel();
		this.labelAltezza = new XdevLabel();
		this.textFieldAltezza = new TextFieldInteger();
		this.labelAltezzaUM = new XdevLabel();
		this.labelFrequenza = new XdevLabel();
		this.comboBoxFrequenza = new XdevComboBox<>();
		this.label3 = new XdevLabel();
		this.labelDensita20C = new XdevLabel();
		this.textFieldDensita020 = new TextFieldDouble();
		this.label4 = new XdevLabel();
		this.labelDensita0m = new XdevLabel();
		this.labelDensitaXXC = new XdevLabel();
		this.labelDensitaXXCXXm = new XdevLabel();
		this.label6 = new XdevLabel();
		this.labelDensitaXXm = new XdevLabel();
		this.labelUmidita = new XdevLabel();
		this.labelUmiditaUM = new XdevLabel();
		this.textFieldUmidita = new TextFieldInteger();
		
	
		this.gridLayoutSetupPreferenze.setCaption("");
		this.gridLayoutSetupPreferenze.setStyleName("rgb-gradient");
		this.gridLayoutSetupPreferenze.setCaptionAsHtml(true);
		this.labelTitoloPreferenze.setValue("<html><b><center><center>Preferenze</html>");
		this.labelTitoloPreferenze.setContentMode(ContentMode.HTML);
		this.labelTemperatura.setValue("Temperatura");
		this.labelTemperaturaUM.setValue("UM");
		this.labelTemperaturaUM.setContentMode(ContentMode.HTML);
		this.labelAltezza.setValue("Altezza");
		this.labelAltezzaUM.setValue("UM");
		this.labelAltezzaUM.setContentMode(ContentMode.HTML);
		this.labelUmidita.setValue("Umidità");
		this.labelUmidita.setContentMode(ContentMode.HTML);
		this.labelUmiditaUM.setValue("%");
		this.labelUmiditaUM.setContentMode(ContentMode.HTML);
		this.labelFrequenza.setCaption("");
		this.labelFrequenza.setValue("Frequenza");
		this.label3.setValue("[Hz]");
		this.labelDensita20C.setValue("Desnsita 20C");
		this.label4.setValue("<html>[Kg/m<sup>3</sup>]</html>");
		this.label4.setContentMode(ContentMode.HTML);
		this.labelDensita0m.setValue("0m");
		this.labelDensitaXXC.setValue("Densita XXC");
		this.labelDensitaXXCXXm.setValue("???");
		this.label6.setValue("<html>[Kg/m<sup>3</sup>]</html>");
		this.label6.setContentMode(ContentMode.HTML);
		this.labelDensitaXXm.setValue("XXm");
	
		this.gridLayoutSetupPreferenze.setColumns(3);
		this.gridLayoutSetupPreferenze.setRows(10);
		
		this.labelTitoloPreferenze.setWidth(100, Unit.PERCENTAGE);
		this.labelTitoloPreferenze.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.labelTitoloPreferenze, 0, 0, 2, 0);
		
		this.labelTemperatura.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelTemperatura, 0, 1);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelTemperatura, Alignment.MIDDLE_RIGHT);
		this.textFieldTemperatura.setWidth(70, Unit.PIXELS);
		this.textFieldTemperatura.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.textFieldTemperatura, 1, 1);
		this.labelTemperaturaUM.setWidth(80, Unit.PIXELS);
		this.labelTemperaturaUM.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.labelTemperaturaUM, 2, 1);
		
		this.labelAltezza.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelAltezza, 0, 2);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelAltezza, Alignment.MIDDLE_RIGHT);
		this.textFieldAltezza.setWidth(70, Unit.PIXELS);
		this.textFieldAltezza.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.textFieldAltezza, 1, 2);
		this.labelAltezzaUM.setWidth(80, Unit.PIXELS);
		this.labelAltezzaUM.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.labelAltezzaUM, 2, 2);
		
		this.labelUmidita.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelUmidita, 0, 3);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelUmidita, Alignment.MIDDLE_RIGHT);
		this.textFieldUmidita.setWidth(70, Unit.PIXELS);
		this.textFieldUmidita.setHeight(-1, Unit.PIXELS);

		this.gridLayoutSetupPreferenze.addComponent(this.textFieldUmidita, 1, 3);
		this.gridLayoutSetupPreferenze.addComponent(this.labelUmiditaUM, 2, 3);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelUmiditaUM, Alignment.MIDDLE_LEFT);
		
		
		this.labelFrequenza.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelFrequenza, 0, 4);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelFrequenza, Alignment.TOP_RIGHT);
		this.comboBoxFrequenza.setWidth(70, Unit.PIXELS);
		this.comboBoxFrequenza.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.comboBoxFrequenza, 1, 4);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.comboBoxFrequenza, Alignment.BOTTOM_LEFT);
		this.label3.setWidth(80, Unit.PIXELS);
		this.label3.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.label3, 2, 4);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.label3, Alignment.MIDDLE_LEFT);
		
		this.labelDensita20C.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelDensita20C, 0, 5);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelDensita20C, Alignment.MIDDLE_RIGHT);
		this.textFieldDensita020.setWidth(70, Unit.PIXELS);
		this.textFieldDensita020.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.textFieldDensita020, 1, 5);
		this.label4.setWidth(80, Unit.PIXELS);
		this.label4.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.label4, 2, 5);
		
		this.labelDensita0m.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelDensita0m, 0, 6);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelDensita0m, Alignment.MIDDLE_RIGHT);
		
		this.labelDensitaXXC.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelDensitaXXC, 0, 7);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelDensitaXXC, Alignment.MIDDLE_RIGHT);
		this.labelDensitaXXCXXm.setWidth(70, Unit.PIXELS);
		this.labelDensitaXXCXXm.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.labelDensitaXXCXXm, 1, 7);
		this.label6.setWidth(80, Unit.PIXELS);
		this.label6.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.addComponent(this.label6, 2, 7);
		
		this.labelDensitaXXm.setSizeUndefined();
		this.gridLayoutSetupPreferenze.addComponent(this.labelDensitaXXm, 0, 8);
		this.gridLayoutSetupPreferenze.setComponentAlignment(this.labelDensitaXXm, Alignment.MIDDLE_RIGHT);
		this.gridLayoutSetupPreferenze.setColumnExpandRatio(0, 100.0F);
		this.gridLayoutSetupPreferenze.setColumnExpandRatio(1, 10.0F);
		this.gridLayoutSetupPreferenze.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayoutSetupPreferenze_vSpacer = new CustomComponent();
		gridLayoutSetupPreferenze_vSpacer.setSizeFull();
		//this.gridLayoutSetupPreferenze.addComponent(gridLayoutSetupPreferenze_vSpacer, 0, 8, 2, 8);
		this.gridLayoutSetupPreferenze.setRowExpandRatio(8, 1.0F);
		this.gridLayoutSetupPreferenze.setWidth(600, Unit.PIXELS);
		this.gridLayoutSetupPreferenze.setHeight(300, Unit.PIXELS);

		this.setContent(this.gridLayoutSetupPreferenze);
//		this.setWidth(410, Unit.PIXELS);
//		this.setHeight(300, Unit.PIXELS);
	
		this.textFieldUmidita.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloCondizioni.this.textFieldUmidita_textChange(event);
			}
		});
		this.textFieldTemperatura.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloCondizioni.this.textFieldTemperatura_textChange(event);
			}
		});
		this.textFieldAltezza.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloCondizioni.this.textFieldAltezza_textChange(event);
			}
		});
		this.textFieldDensita020.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				pannelloCondizioni.this.textFieldDensita020_textChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelTitoloPreferenze, labelTemperatura, labelTemperaturaUM, labelAltezza, labelAltezzaUM,
			labelFrequenza, labelUmidita, labelUmiditaUM, label3, labelDensita20C, label4, labelDensita0m, labelDensitaXXC, labelDensitaXXCXXm, label6,
			labelDensitaXXm, labelDensitaA00, labelDensitaARichiesta;
	private TextFieldDouble textFieldDensita020;
	private XdevGridLayout gridLayoutSetupPreferenze;
	private TextFieldInteger textFieldTemperatura, textFieldAltezza, textFieldUmidita;
	private XdevComboBox<CustomComponent> comboBoxFrequenza;
	// </generated-code>

}
