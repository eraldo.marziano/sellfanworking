package com.cit.sellfan.ui.pannelli;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;

import cit.classi.Clientev21;

public class pannelloCliente extends Panel {
	private VariabiliGlobali l_VG;
	private final pannelloIndirizzoItalia indirizzoItalia = new pannelloIndirizzoItalia();
	private final pannelloIndirizzoEstero indirizzoEstero = new pannelloIndirizzoEstero();
	private boolean ItaliaVisible = true;

	/**
	 * 
	 */
	public pannelloCliente() {
		super();
		this.initUI();
		this.indirizzoItalia.setWidth(100, Unit.PERCENTAGE);
		this.indirizzoItalia.setHeight(-1, Unit.PIXELS);
		this.indirizzoEstero.setWidth(100, Unit.PERCENTAGE);
		this.indirizzoEstero.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.replaceComponent(this.panelIndirizzoGosth, this.indirizzoItalia);
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		this.indirizzoItalia.setVariabiliGlobali(this.l_VG);
		this.indirizzoEstero.setVariabiliGlobali(this.l_VG);
	}
	
	public void Nazionalizza() {
		this.indirizzoItalia.Nazionalizza();
		this.indirizzoEstero.Nazionalizza();
		this.labelSociale.setValue("<html>" + this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale") + "<sup>*</sup></html>");
		this.checkBoxItalia.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Italia"));
		this.labelIVA.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Partita IVA"));
		this.labelCF.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Codice Fiscale"));
		this.labelTel.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Telefono"));
		this.labelFax.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Fax"));
		this.labelSconto.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Sconto") + " %");
		this.labelSettore.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Settore Commerciale"));
		this.labelRif.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Riferimento"));
		this.labelTec.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Riferimento Interno"));
		//this.labelAcq.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Riferimento Acquisti"));
		this.checkBoxAgente.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Agente"));
		this.checkBoxConcessionario.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Concessionario"));
		this.labelPagamento.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pagamento"));
		this.labelImballo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Imballo"));
		this.checkBoxImballoCompreso.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Compreso"));
		this.labelResa.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Resa"));
		this.labelSpedizione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Indirizzo di spedizione"));
	}
	
	public void setClienteInUse(final Clientev21 clienteInUse) {
		if (clienteInUse == null) {
			return;
		}
		enableFields(false);
		this.indirizzoItalia.setClienteInUse(clienteInUse);
		this.indirizzoEstero.setClienteInUse(clienteInUse);
		if (clienteInUse.indirizzo.italia) {
			if (!this.ItaliaVisible) {
				this.verticalLayout.replaceComponent(this.indirizzoEstero, this.indirizzoItalia);
				this.ItaliaVisible = true;
			}
		} else {
			if (this.ItaliaVisible) {
				this.verticalLayout.replaceComponent(this.indirizzoItalia, this.indirizzoEstero);
				this.ItaliaVisible = false;
			}
		}
		this.textFieldRagioneSociale.setValue(clienteInUse.ragionesociale);
		this.checkBoxItalia.setValue(clienteInUse.indirizzo.italia);
		this.textFieldIVA.setValue(clienteInUse.partitaiva);
		this.textFieldCF.setValue(clienteInUse.codicefistale);
		this.textFieldTel1.setValue(clienteInUse.telefono1);
		this.textFieldTel2.setValue(clienteInUse.telefono2);
		this.textFieldFax.setValue(clienteInUse.fax);
		this.textFieldSconto.setValue(clienteInUse.scontocliente);
		this.textFieldSettore.setValue(clienteInUse.settorecommerciale);
		this.textFieldRif.setValue(clienteInUse.riferimento);
		this.textFieldmailRif.setValue(clienteInUse.email);
		this.textFieldTec.setValue(clienteInUse.riferimentotecnico);
		this.textFieldmailTec.setValue(clienteInUse.emailtecnica);
		//this.textFieldAcq.setValue(clienteInUse.riferimentoacquisti);
		//this.textFieldmailAcq.setValue(clienteInUse.emailacquisti);
		this.checkBoxAgente.setValue(clienteInUse.agente);
		this.checkBoxConcessionario.setValue(clienteInUse.concessionario);
		this.textFieldPagamento.setValue(clienteInUse.pagamento);
		this.textFieldImballo.setValue(clienteInUse.imballo);
		this.checkBoxImballoCompreso.setValue(clienteInUse.imballocompreso);
		this.textFieldResa.setValue(clienteInUse.resa);
		this.textFieldSpedizione.setValue(clienteInUse.indirizzospedizione);
		enableFields(false);
	}
/*
	public void setCurrentCliente() {
		enableFields(false);
		indirizzoItalia.setClienteInUse(l_VG.currentCliente);
		indirizzoEstero.setClienteInUse(l_VG.currentCliente);
		if (l_VG.currentCliente.indirizzo.italia) {
			if (!ItaliaVisible) {
				verticalLayout.replaceComponent(indirizzoEstero, indirizzoItalia);
				ItaliaVisible = true;
			}
		} else {
			if (ItaliaVisible) {
				verticalLayout.replaceComponent(indirizzoItalia, indirizzoEstero);
				ItaliaVisible = false;
			}
		}
		textFieldRagioneSociale.setValue(l_VG.currentCliente.ragionesociale);
		checkBoxItalia.setValue(l_VG.currentCliente.indirizzo.italia);
		textFieldIVA.setValue(l_VG.currentCliente.partitaiva);
		textFieldCF.setValue(l_VG.currentCliente.codicefistale);
		textFieldTel1.setValue(l_VG.currentCliente.telefono1);
		textFieldTel2.setValue(l_VG.currentCliente.telefono2);
		textFieldFax.setValue(l_VG.currentCliente.fax);
		textFieldSconto.setValue(l_VG.currentCliente.scontocliente);
		textFieldSettore.setValue(l_VG.currentCliente.settorecommerciale);
		textFieldRif.setValue(l_VG.currentCliente.riferimento);
		textFieldmailRif.setValue(l_VG.currentCliente.email);
		textFieldTec.setValue(l_VG.currentCliente.riferimentotecnico);
		textFieldmailTec.setValue(l_VG.currentCliente.emailtecnica);
		textFieldAcq.setValue(l_VG.currentCliente.riferimentoacquisti);
		textFieldmailAcq.setValue(l_VG.currentCliente.emailacquisti);
		checkBoxAgente.setValue(l_VG.currentCliente.agente);
		checkBoxConcessionario.setValue(l_VG.currentCliente.concessionario);
		textFieldPagamento.setValue(l_VG.currentCliente.pagamento);
		textFieldImballo.setValue(l_VG.currentCliente.imballo);
		checkBoxImballoCompreso.setValue(l_VG.currentCliente.imballocompreso);
		textFieldResa.setValue(l_VG.currentCliente.resa);
		textFieldSpedizione.setValue(l_VG.currentCliente.indirizzospedizione);
		enableFields(false);
	}
*/
	public void enableFields(final boolean editEnabled) {
		this.indirizzoItalia.enableFields(editEnabled);
		this.indirizzoEstero.enableFields(editEnabled);
		this.textFieldRagioneSociale.setEnabled(editEnabled);
		this.checkBoxItalia.setEnabled(editEnabled);
		this.textFieldIVA.setEnabled(editEnabled);
		this.textFieldCF.setEnabled(editEnabled);
		this.textFieldTel1.setEnabled(editEnabled);
		this.textFieldTel2.setEnabled(editEnabled);
		this.textFieldFax.setEnabled(editEnabled);
		this.textFieldSconto.setEnabled(editEnabled);
		this.textFieldSettore.setEnabled(editEnabled);
		this.textFieldRif.setEnabled(editEnabled);
		this.textFieldmailRif.setEnabled(editEnabled);
		this.textFieldTec.setEnabled(editEnabled);
		this.textFieldmailTec.setEnabled(editEnabled);
		//this.textFieldAcq.setEnabled(editEnabled);
		//this.textFieldmailAcq.setEnabled(editEnabled);
		this.checkBoxAgente.setEnabled(editEnabled);
		this.checkBoxConcessionario.setEnabled(editEnabled);
		this.textFieldPagamento.setEnabled(editEnabled);
		this.textFieldImballo.setEnabled(editEnabled);
		this.checkBoxImballoCompreso.setEnabled(editEnabled);
		this.textFieldResa.setEnabled(editEnabled);
		this.textFieldSpedizione.setEnabled(editEnabled);
	}
	
	public String getRagioneSociale() {
		return this.textFieldRagioneSociale.getValue();
	}
/*
	public void getCurrentCliente() {
		l_VG.currentCliente.ragionesociale = textFieldRagioneSociale.getValue();
		l_VG.currentCliente.indirizzo.italia = checkBoxItalia.getValue();
		l_VG.currentCliente.partitaiva = textFieldIVA.getValue();
		l_VG.currentCliente.codicefistale = textFieldCF.getValue();
		l_VG.currentCliente.telefono1 = textFieldTel1.getValue();
		l_VG.currentCliente.telefono2 = textFieldTel2.getValue();
		l_VG.currentCliente.fax = textFieldFax.getValue();
		l_VG.currentCliente.scontocliente = textFieldSconto.getDoubleValue();
		l_VG.currentCliente.settorecommerciale = textFieldSettore.getValue();
		l_VG.currentCliente.riferimento = textFieldRif.getValue();
		l_VG.currentCliente.email = textFieldmailRif.getValue();
		l_VG.currentCliente.riferimentotecnico = textFieldTec.getValue();
		l_VG.currentCliente.emailtecnica = textFieldmailTec.getValue();
		l_VG.currentCliente.riferimentoacquisti = textFieldAcq.getValue();
		l_VG.currentCliente.emailacquisti = textFieldmailAcq.getValue();
		l_VG.currentCliente.agente = checkBoxAgente.getValue();
		l_VG.currentCliente.concessionario = checkBoxConcessionario.getValue();
		l_VG.currentCliente.pagamento = textFieldPagamento.getValue();
		l_VG.currentCliente.imballo = textFieldImballo.getValue();
		l_VG.currentCliente.imballocompreso = checkBoxImballoCompreso.getValue();
		l_VG.currentCliente.resa = textFieldResa.getValue();
		l_VG.currentCliente.indirizzospedizione = textFieldSpedizione.getValue();
		if (ItaliaVisible) {
			indirizzoItalia.getClienteInUse(l_VG.currentCliente);
		} else {
			indirizzoEstero.getClienteInUse(l_VG.currentCliente);
		}
	}
*/
	public void getClienteInUse(final Clientev21 clienteInUse) {
		if (clienteInUse == null) {
			return;
		}
		clienteInUse.ragionesociale = this.textFieldRagioneSociale.getValue();
		clienteInUse.indirizzo.italia = this.checkBoxItalia.getValue();
		clienteInUse.partitaiva = this.textFieldIVA.getValue();
		clienteInUse.codicefistale = this.textFieldCF.getValue();
		clienteInUse.telefono1 = this.textFieldTel1.getValue();
		clienteInUse.telefono2 = this.textFieldTel2.getValue();
		clienteInUse.fax = this.textFieldFax.getValue();
		clienteInUse.scontocliente = this.textFieldSconto.getDoubleValue();
		clienteInUse.settorecommerciale = this.textFieldSettore.getValue();
		clienteInUse.riferimento = this.textFieldRif.getValue();
		clienteInUse.email = this.textFieldmailRif.getValue();
		clienteInUse.riferimentotecnico = this.textFieldTec.getValue();
		clienteInUse.emailtecnica = this.textFieldmailTec.getValue();
		//clienteInUse.riferimentoacquisti = this.textFieldAcq.getValue();
		//clienteInUse.emailacquisti = this.textFieldmailAcq.getValue();
		clienteInUse.agente = this.checkBoxAgente.getValue();
		clienteInUse.concessionario = this.checkBoxConcessionario.getValue();
		clienteInUse.pagamento = this.textFieldPagamento.getValue();
		clienteInUse.imballo = this.textFieldImballo.getValue();
		clienteInUse.imballocompreso = this.checkBoxImballoCompreso.getValue();
		clienteInUse.resa = this.textFieldResa.getValue();
		clienteInUse.indirizzospedizione = this.textFieldSpedizione.getValue();
		if (this.ItaliaVisible) {
			this.indirizzoItalia.getClienteInUse(clienteInUse);
		} else {
			this.indirizzoEstero.getClienteInUse(clienteInUse);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxItalia}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxItalia_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxItalia.isEnabled()) {
			return;
		}
		if (this.checkBoxItalia.getValue()) {
			if (!this.ItaliaVisible) {
				this.verticalLayout.replaceComponent(this.indirizzoEstero, this.indirizzoItalia);
				this.ItaliaVisible = true;
			}
		} else {
			if (this.ItaliaVisible) {
				this.verticalLayout.replaceComponent(this.indirizzoItalia, this.indirizzoEstero);
				this.ItaliaVisible = false;
			}
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.labelSociale = new XdevLabel();
		this.textFieldRagioneSociale = new XdevTextField();
		this.checkBoxItalia = new XdevCheckBox();
		this.panelIndirizzoGosth = new XdevPanel();
		this.gridLayout = new XdevGridLayout();
		this.labelIVA = new XdevLabel();
		this.textFieldIVA = new XdevTextField();
		this.labelCF = new XdevLabel();
		this.textFieldCF = new XdevTextField();
		this.labelTel = new XdevLabel();
		this.textFieldTel1 = new XdevTextField();
		this.textFieldTel2 = new XdevTextField();
		this.labelFax = new XdevLabel();
		this.textFieldFax = new XdevTextField();
		this.labelSconto = new XdevLabel();
		this.textFieldSconto = new TextFieldDouble();
		this.labelSettore = new XdevLabel();
		this.textFieldSettore = new XdevTextField();
		this.labelRif = new XdevLabel();
		this.textFieldRif = new XdevTextField();
		this.labelmailRif = new XdevLabel();
		this.textFieldmailRif = new XdevTextField();
		this.labelTec = new XdevLabel();
		this.textFieldTec = new XdevTextField();
		this.labelmailTec = new XdevLabel();
		this.textFieldmailTec = new XdevTextField();
		//this.labelAcq = new XdevLabel();
		//this.textFieldAcq = new XdevTextField();
		//this.labelmailAcq = new XdevLabel();
		//this.textFieldmailAcq = new XdevTextField();
		this.checkBoxAgente = new XdevCheckBox();
		this.checkBoxConcessionario = new XdevCheckBox();
		this.labelPagamento = new XdevLabel();
		this.textFieldPagamento = new XdevTextField();
		this.labelImballo = new XdevLabel();
		this.textFieldImballo = new XdevTextField();
		this.checkBoxImballoCompreso = new XdevCheckBox();
		this.labelResa = new XdevLabel();
		this.textFieldResa = new XdevTextField();
		this.labelSpedizione = new XdevLabel();
		this.textFieldSpedizione = new XdevTextField();
	
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.labelSociale.setValue("Ragione Sociale");
		this.labelSociale.setContentMode(ContentMode.HTML);
		this.checkBoxItalia.setCaption("Italia");
		this.gridLayout.setMargin(new MarginInfo(false));
		this.labelIVA.setValue("Partita IVA");
		this.labelCF.setValue("Codice Fiscale");
		this.labelTel.setValue("Telefono");
		this.labelFax.setValue("Fax");
		this.labelSconto.setValue("Sconto %");
		this.labelSettore.setValue("Settore Commerciale");
		this.labelRif.setValue("Riferimento");
		this.labelmailRif.setValue("e-mail");
		this.labelTec.setValue("Riferimento Interno");
		this.labelmailTec.setValue("e-mail");
		//this.labelAcq.setValue("Riferimento Acquisti");
		//this.labelmailAcq.setValue("e-mail");
		this.checkBoxAgente.setCaption("Agente");
		this.checkBoxConcessionario.setCaption("Concessionario");
		this.labelPagamento.setValue("Pagamento");
		this.labelImballo.setValue("Imballo");
		this.checkBoxImballoCompreso.setCaption("Compreso");
		this.labelResa.setValue("Resa");
		this.labelSpedizione.setValue("Indirizzo Spedizione");
	
		this.labelSociale.setSizeUndefined();
		this.horizontalLayout.addComponent(this.labelSociale);
		this.textFieldRagioneSociale.setWidth(100, Unit.PERCENTAGE);
		this.textFieldRagioneSociale.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.textFieldRagioneSociale);
		this.horizontalLayout.setComponentAlignment(this.textFieldRagioneSociale, Alignment.TOP_CENTER);
		this.horizontalLayout.setExpandRatio(this.textFieldRagioneSociale, 10.0F);
		this.gridLayout.setColumns(4);
		this.gridLayout.setRows(14);
		this.labelIVA.setSizeUndefined();
		this.gridLayout.addComponent(this.labelIVA, 0, 0);
		this.gridLayout.setComponentAlignment(this.labelIVA, Alignment.TOP_RIGHT);
		this.textFieldIVA.setWidth(100, Unit.PERCENTAGE);
		this.textFieldIVA.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldIVA, 1, 0);
		this.labelCF.setSizeUndefined();
		this.gridLayout.addComponent(this.labelCF, 2, 0);
		this.gridLayout.setComponentAlignment(this.labelCF, Alignment.TOP_RIGHT);
		this.textFieldCF.setWidth(100, Unit.PERCENTAGE);
		this.textFieldCF.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldCF, 3, 0);
		this.labelTel.setSizeUndefined();
		this.gridLayout.addComponent(this.labelTel, 0, 1);
		this.gridLayout.setComponentAlignment(this.labelTel, Alignment.TOP_RIGHT);
		this.textFieldTel1.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTel1.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldTel1, 1, 1);
		this.textFieldTel2.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTel2.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldTel2, 2, 1, 3, 1);
		this.labelFax.setSizeUndefined();
		this.gridLayout.addComponent(this.labelFax, 0, 2);
		this.gridLayout.setComponentAlignment(this.labelFax, Alignment.TOP_RIGHT);
		this.textFieldFax.setWidth(100, Unit.PERCENTAGE);
		this.textFieldFax.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldFax, 1, 2);
		this.labelSconto.setSizeUndefined();
		this.gridLayout.addComponent(this.labelSconto, 0, 3);
		this.gridLayout.setComponentAlignment(this.labelSconto, Alignment.TOP_RIGHT);
		this.textFieldSconto.setSizeUndefined();
		this.gridLayout.addComponent(this.textFieldSconto, 1, 3);
		this.labelSettore.setSizeUndefined();
		this.gridLayout.addComponent(this.labelSettore, 0, 4);
		this.gridLayout.setComponentAlignment(this.labelSettore, Alignment.TOP_RIGHT);
		this.textFieldSettore.setWidth(100, Unit.PERCENTAGE);
		this.textFieldSettore.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldSettore, 1, 4, 3, 4);
		this.labelRif.setSizeUndefined();
		this.gridLayout.addComponent(this.labelRif, 0, 5);
		this.gridLayout.setComponentAlignment(this.labelRif, Alignment.TOP_RIGHT);
		this.textFieldRif.setWidth(100, Unit.PERCENTAGE);
		this.textFieldRif.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldRif, 1, 5);
		this.labelmailRif.setSizeUndefined();
		this.gridLayout.addComponent(this.labelmailRif, 2, 5);
		this.gridLayout.setComponentAlignment(this.labelmailRif, Alignment.TOP_RIGHT);
		this.textFieldmailRif.setWidth(100, Unit.PERCENTAGE);
		this.textFieldmailRif.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldmailRif, 3, 5);
		this.labelTec.setSizeUndefined();
		this.gridLayout.addComponent(this.labelTec, 0, 6);
		this.gridLayout.setComponentAlignment(this.labelTec, Alignment.TOP_RIGHT);
		this.textFieldTec.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTec.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldTec, 1, 6);
		this.labelmailTec.setSizeUndefined();
		this.gridLayout.addComponent(this.labelmailTec, 2, 6);
		this.gridLayout.setComponentAlignment(this.labelmailTec, Alignment.TOP_RIGHT);
		this.textFieldmailTec.setWidth(100, Unit.PERCENTAGE);
		this.textFieldmailTec.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldmailTec, 3, 6);
		//this.labelAcq.setSizeUndefined();
		//this.gridLayout.addComponent(this.labelAcq, 0, 7);
		//this.gridLayout.setComponentAlignment(this.labelAcq, Alignment.TOP_RIGHT);
		//this.textFieldAcq.setWidth(100, Unit.PERCENTAGE);
		//this.textFieldAcq.setHeight(-1, Unit.PIXELS);
		//this.gridLayout.addComponent(this.textFieldAcq, 1, 7);
		//this.labelmailAcq.setSizeUndefined();
		//this.gridLayout.addComponent(this.labelmailAcq, 2, 7);
		//this.gridLayout.setComponentAlignment(this.labelmailAcq, Alignment.TOP_RIGHT);
		//this.textFieldmailAcq.setWidth(100, Unit.PERCENTAGE);
		//this.textFieldmailAcq.setHeight(-1, Unit.PIXELS);
		//this.gridLayout.addComponent(this.textFieldmailAcq, 3, 7);
		this.checkBoxAgente.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxAgente, 1, 7);
		this.checkBoxConcessionario.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxConcessionario, 2, 7);
		this.labelPagamento.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPagamento, 0, 8);
		this.gridLayout.setComponentAlignment(this.labelPagamento, Alignment.TOP_RIGHT);
		this.textFieldPagamento.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPagamento.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldPagamento, 1, 8);
		this.labelImballo.setSizeUndefined();
		this.gridLayout.addComponent(this.labelImballo, 0, 9);
		this.gridLayout.setComponentAlignment(this.labelImballo, Alignment.TOP_RIGHT);
		this.textFieldImballo.setWidth(100, Unit.PERCENTAGE);
		this.textFieldImballo.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldImballo, 1, 9);
		this.checkBoxImballoCompreso.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxImballoCompreso, 2, 9);
		this.labelResa.setSizeUndefined();
		this.gridLayout.addComponent(this.labelResa, 0, 10);
		this.gridLayout.setComponentAlignment(this.labelResa, Alignment.TOP_RIGHT);
		this.textFieldResa.setWidth(100, Unit.PERCENTAGE);
		this.textFieldResa.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldResa, 1, 10);
		this.labelSpedizione.setSizeUndefined();
		this.gridLayout.addComponent(this.labelSpedizione, 0, 11);
		this.gridLayout.setComponentAlignment(this.labelSpedizione, Alignment.TOP_RIGHT);
		this.textFieldSpedizione.setWidth(100, Unit.PERCENTAGE);
		this.textFieldSpedizione.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldSpedizione, 1, 11, 3, 11);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setColumnExpandRatio(3, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 12, 3, 12);
		this.gridLayout.setRowExpandRatio(13, 1.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.checkBoxItalia.setSizeUndefined();
		this.verticalLayout.addComponent(this.checkBoxItalia);
		this.panelIndirizzoGosth.setWidth(100, Unit.PERCENTAGE);
		this.panelIndirizzoGosth.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panelIndirizzoGosth);
		this.gridLayout.setSizeFull();
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setExpandRatio(this.gridLayout, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.checkBoxItalia.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				pannelloCliente.this.checkBoxItalia_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelSociale, labelIVA, labelCF, labelTel, labelFax, labelSconto, labelSettore, labelRif,
			labelmailRif, labelTec, labelmailTec, labelPagamento, labelImballo, labelResa,
			labelSpedizione;
	private XdevHorizontalLayout horizontalLayout;
	private TextFieldDouble textFieldSconto;
	private XdevCheckBox checkBoxItalia, checkBoxAgente, checkBoxConcessionario, checkBoxImballoCompreso;
	private XdevPanel panelIndirizzoGosth;
	private XdevTextField textFieldRagioneSociale, textFieldIVA, textFieldCF, textFieldTel1, textFieldTel2, textFieldFax,
			textFieldSettore, textFieldRif, textFieldmailRif, textFieldTec, textFieldmailTec, textFieldPagamento, textFieldImballo, textFieldResa, textFieldSpedizione;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>


}
