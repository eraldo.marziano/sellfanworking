/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.pannelli;

import java.text.MessageFormat;
import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.myjavalib.UtilityFisica.UtilityUnitaMisura;
import cit.sellfan.classi.CondizioniAmbientali;
import cit.sellfan.classi.SelezioneDati;
import cit.sellfan.classi.UtilityCliente;
import cit.utility.UtilityTraduzioni;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.utility.Logger;
import com.cit.sellfan.ui.utility.oggettoInfoRicerca;
import com.cit.sellfan.ui.utility.oggettoSelect;
import com.cit.sellfan.ui.view.ProgressView;
import com.cit.sellfan.ui.view.SearchView;
import com.vaadin.data.Property;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.listselect.XdevOptionGroup;
import de.steinwedel.messagebox.MessageBox;
import java.io.Console;
import java.util.ArrayList;
import java.util.regex.*;

/**
 *
 * @author Eraldo Marziano
 */
public class pannelloRicerca extends Panel{
    private static final long serialVersionUID = 5052024568480785764L;
    private VariabiliGlobali vGlobali;
    private SearchView searchView;
    private UtilityTraduzioni traduci;
    private UtilityUnitaMisura misure;
    private MessageBox msgBox;
    private boolean modificabile;
    private boolean serieDaRicaricare = false;
    private boolean tolleranzaSuPressione = false;
    private oggettoInfoRicerca risultatoFiltro;
    private oggettoSelect Assiali, Centrifughi, Trasmissione, Diretto, Aspirazione, Mandata, Totale, Statica, Secca, Umida, Esplicita, Miscela;
    Logger logger = new Logger();
    
    public pannelloRicerca() {
            super();
            initUI();
    }
    public void setVariabiliGlobali (final VariabiliGlobali variabiliGlobali) {
        modificabile = false;
        vGlobali = variabiliGlobali;
        traduci = vGlobali.utilityTraduzioni;
        misure = vGlobali.utilityUnitaMisura;
	this.searchView = (SearchView)vGlobali.ElencoView[vGlobali.SearchViewIndex];
        CA_AltezzaUM.setEnabled(false);
            for (UnitaMisura UdM : vGlobali.utilityUnitaMisura.ElencoUnitaMisuraLunghezza) {
                CA_AltezzaUM.addItem(removeHTML(UdM.simbolo));
            }
        CA_AltezzaUM.setEnabled(true);
	CA_TemperaturaUM.setEnabled(false);
            for (UnitaMisura UdM : vGlobali.utilityUnitaMisura.ElencoUnitaMisuraTemperatura) {
                CA_TemperaturaUM.addItem(removeHTML(UdM.simbolo));
            }
        CA_TemperaturaUM.setEnabled(true);
	DV_HUM.setEnabled(false);
            for (UnitaMisura UdM : vGlobali.utilityUnitaMisura.ElencoUnitaMisuraPressione) {
                DV_HUM.addItem(removeHTML(UdM.simbolo));
            }
        DV_HUM.setEnabled(true);
	DV_QvUM.setEnabled(false);
            for (UnitaMisura UdM : vGlobali.utilityUnitaMisura.ElencoUnitaMisuraPortata) {
                DV_QvUM.addItem(removeHTML(UdM.simbolo));
            }
        DV_QvUM.setEnabled(true);
        DV_QmUM.setEnabled(false);
        DV_QmUM.addItem("[kg/h]");
        DV_NqUM.setEnabled(false);
        DV_NqUM.addItem("[Nm³/h]");
        String defaultT = (vGlobali.ParametriVari.defaultTolleranza == null)? "Pressione" : vGlobali.ParametriVari.defaultTolleranza;
        tolleranzaSuPressione = !defaultT.equals("Pressione");
        if (vGlobali.isDonaldson()) {
            DV_UMQDon = new XdevLabel("m<sup>3</sup>/h", ContentMode.HTML);
            DV_UMQDon.setWidth(100, Unit.PERCENTAGE);
            DV_UMPDon = new XdevLabel("Pa");
            DV_UMPDon.setWidth(100, Unit.PERCENTAGE);
            CA_AltezzaLabel.setVisible(false);
            CA_Altezza.setVisible(false);
            CA_AltezzaUM.setVisible(false);
            HLtipoFluido.setVisible(false);
            DV_AssialiCentrifughi.setVisible(false);
            DV_TrasmissioneDiretti.setVisible(false);
            DV_HLabel.setContentMode(ContentMode.HTML);
            DV_HLabel.setValue("P<sub>St</sub>");
            DV_AspirazioneMandata.setVisible(false);
            DV_TotaleStatica.setVisible(false);
            EN_BottoniGrid.removeAllComponents();
            EN_BottoniGrid.addComponent(EN_Ricerca, 0, 0, 1, 1);
            DV_PortataGrid.removeAllComponents();
            DV_PortataGrid.addComponent(DV_QvLabel, 0, 0);
            DV_PortataGrid.addComponent(DV_Qv, 1, 0, 2, 0);
            DV_PortataGrid.addComponent(DV_UMQDon, 3, 0, 5, 0);
            DV_PortataGrid.addComponent(DV_QmLabel, 0, 1);
            DV_PortataGrid.addComponent(DV_Qm, 1, 1, 2, 1);
            DV_PortataGrid.addComponent(DV_QmUM, 3, 1, 5, 1);
            DV_PortataGrid.addComponent(DV_NqLabel, 0, 2);
            DV_PortataGrid.addComponent(DV_Nq, 1, 2, 2, 2);
            DV_PortataGrid.addComponent(DV_NqUM, 3, 2, 5, 2);
            DV_PortataGrid.addStyleName("NoBottomPadding");
            DV_PressioneGrid.removeAllComponents();
            DV_PressioneGrid.addComponent(DV_HLabel, 0, 0);
            DV_PressioneGrid.addComponent(DV_H, 1, 0, 2, 0);
            DV_PressioneGrid.addComponent(DV_UMPDon, 3, 0, 5, 0);
            DV_PressioneGrid.addComponent(DV_TotaleStatica, 0, 1, 2, 1);
            DV_PressioneGrid.addComponent(DV_AspirazioneMandata, 3, 1, 5, 1);
            DV_PressioneGrid.addStyleName("NoBottomPadding");
            DV_Qm.setVisible(false);
            DV_QmLabel.setVisible(false);
            DV_QmUM.setVisible(false);
            DV_Nq.setVisible(false);
            DV_NqLabel.setVisible(false);
            DV_NqUM.setVisible(false);
        }
        buildComboEsecuzioni();
        setDefaultValues();
        buildDensitaCorrente();
        modificabile = true;
        CA_ElencoFluidi_valueChange();
        
    }
    public void getDati() {
        SelezioneDati sDati = vGlobali.SelezioneDati;
        String defaultEsec = traduci.TraduciStringa("Default");
        String Esecuzione = DV_EsecuzioneCombo.getValue().toString();
        UtilityCliente uCliente = vGlobali.utilityCliente;
        if (vGlobali.isDonaldson()) {
            sDati.esecuzioneSelezionata = Esecuzione.equals("Standing N4") ? "E04" : "E05";
        } else {
            sDati.esecuzioneSelezionata = (Esecuzione.equals("") || Esecuzione.equals(defaultEsec)) ? "" : Esecuzione;
        }
        sDati.umidita = CA_Umidita.getIntegerValue();
        sDati.CategorieSerieIndex = 0;
        sDati.MaterialeIndex = 0;
        sDati.aperturaSerrandaDapo = 100;
	sDati.UMPortata = (UnitaMisura) vGlobali.UMPortataCorrente.clone();
        sDati.UMPressione = (UnitaMisura) vGlobali.UMPressioneCorrente.clone();
        sDati.UMPotenza = (UnitaMisura) vGlobali.UMPotenzaCorrente.clone();
        sDati.UMAltezza = (UnitaMisura) vGlobali.UMAltezzaCorrente.clone();
        sDati.UMTemperatura = (UnitaMisura) vGlobali.UMTemperaturaCorrente.clone();
        uCliente.AssialiSelezionatiFlag = ((oggettoSelect) DV_AssialiCentrifughi.getValue()).theSame("Assiali");
        sDati.Assiali = uCliente.AssialiSelezionatiFlag;
        uCliente.CentrifughiSelezionatiFlag = ((oggettoSelect) DV_AssialiCentrifughi.getValue()).theSame("Centrifughi");
        sDati.PotSonoraFlag = true;
        sDati.PresSonoraMax = sDati.PotSonoraMax;
        sDati.Trasmissione = !vGlobali.searchIsDiretto;
        sDati.precisioneMeno = vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO;
        sDati.precisionePiu = vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezionePIU;
        sDati.precisioneMenoPressione = vGlobali.ParametriVari.pannelloRicercaTolleranzaPressioneMeno;
        sDati.precisionePiuPressione = vGlobali.ParametriVari.pannelloRicercaTolleranzaPressionePiu;
        boolean statica = ((oggettoSelect)(DV_TotaleStatica.getValue())).theSame("Statica");
        boolean mandata = ((oggettoSelect)(DV_AspirazioneMandata.getValue())).theSame("Mandata");
        sDati.pressioneStaticaFlag = statica;
        sDati.PressioneMandataFlag = mandata;
        if (DV_FrequenzaCombo.getValue() != null)
            vGlobali.CARicerca.frequenza = Integer.parseInt(DV_FrequenzaCombo.getValue().toString());
        if (risultatoFiltro != null) {
//        sDati.DiametroMassimoGirante = risultatoFiltro.DiametroMassimoGirante;
//        sDati.giriMassimiGirante = risultatoFiltro.GiriMassimiGirante;
//        vGlobali.CorrettorePotenzaMotoreCorrente = risultatoFiltro.CorrettorePotenzaMotore;
        }
        String tipoFluido = ((oggettoSelect)(CA_ElencoFluidi.getValue())).nome;
        switch (tipoFluido) {
            case "Aria Secca":
                sDati.tipoFluidoRicerca = vGlobali.utilityTraduzioni.TraduciStringa("Aria Secca");
                break;
            case "Aria Umida":
                sDati.tipoFluidoRicerca = vGlobali.utilityTraduzioni.TraduciStringa("Aria Umida") + " " + CA_Umidita.getValue();
                break;
            case "Esplicito":
                sDati.tipoFluidoRicerca = vGlobali.utilityTraduzioni.TraduciStringa("Esplicito");
                break;
            case "Miscela":
                sDati.tipoFluidoRicerca = sDati.tipoFluido;
                break;
            default:
        }
        
        //logger.log(sDati);
    }
    public void Nazionalizza() {
        CA_AltezzaLabel.setValue(MessageFormat.format("h<sub>{0}</sub>", traduci.TraduciStringa("S.L.M.")));
        CA_TemperaturaLabel.setValue(MessageFormat.format("T<sub>{0}</sub>", traduci.TraduciStringa("Fluido")));
        CA_SeccoLabel.setValue(MessageFormat.format("kg/m<sup>3</sup>&nbsp({0} 20°C {1} 0 m)", traduci.TraduciStringa("a"), traduci.TraduciStringa("e")));
        CA_EsplicitoLabel.setValue(MessageFormat.format("kg/m<sup>3</sup>&nbsp({0} 20°C {1} 0 m)", traduci.TraduciStringa("a"), traduci.TraduciStringa("e")));
        DV_FrequenzaLabel.setValue(MessageFormat.format("{0} (Hz)", traduci.TraduciStringa("Frequenza")));
        DV_EsecuzioneLabel.setValue(traduci.TraduciStringa("Esecuzione"));
        CA_MiscelaButton.setCaption(traduci.TraduciStringa("Definisci"));
        EN_SelezioneAvanzata.setCaption(traduci.TraduciStringa("Selezione Avanzata"));
        EN_Ricerca.setCaption(traduci.TraduciStringa("Cerca!"));
        Secca.valore = traduci.TraduciStringa(Secca.nome);
        Umida.valore = traduci.TraduciStringa(Umida.nome);
        Esplicita.valore = traduci.TraduciStringa(Esplicita.nome);
        Miscela.valore = traduci.TraduciStringa(Miscela.nome);
        Assiali.valore = traduci.TraduciStringa(Assiali.nome);
        Centrifughi.valore = traduci.TraduciStringa(Centrifughi.nome);
        Trasmissione.valore = traduci.TraduciStringa(Trasmissione.nome);
        Diretto.valore = traduci.TraduciStringa(Diretto.nome);
        Totale.valore = traduci.TraduciStringa(Totale.nome);
        Statica.valore = traduci.TraduciStringa(Statica.nome);
        Aspirazione.valore = traduci.TraduciStringa(Aspirazione.nome);
        Mandata.valore = traduci.TraduciStringa(Mandata.nome);
        pannelloCondizioni.setCaption(traduci.TraduciStringa("Condizioni Ambientali"));
        pannelloVentilator.setCaption(traduci.TraduciStringa("Dati Ventilatore"));
        
        
        
        DV_TolleranzaSuPressione.setCaption(MessageFormat.format("{0} -{1}% +{2}%", 
                traduci.TraduciStringa("Tolleranza"),
                vGlobali.ParametriVari.pannelloRicercaTolleranzaPressioneMeno * 100,
                vGlobali.ParametriVari.pannelloRicercaTolleranzaPressionePiu * 100));
        DV_TolleranzaSuPortata.setCaption(MessageFormat.format("{0} -{1}% +{2}%", 
                traduci.TraduciStringa("Tolleranza"),
                vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO * 100,
                vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezionePIU * 100));
    }
    
    private void buildDensitaCorrente() {
        vGlobali.fmtNd.setMaximumFractionDigits(vGlobali.UMTemperaturaCorrente.nDecimaliStampa);
        String temperaturaRicerca = vGlobali.fmtNd.format(misure.fromCelsiusToXX(vGlobali.UMTemperaturaCorrente.index, vGlobali.CARicerca.temperatura));
        vGlobali.fmtNd.setMaximumFractionDigits(vGlobali.UMAltezzaCorrente.nDecimaliStampa);
        String altezzaRicerca = vGlobali.fmtNd.format(misure.frommToXX(vGlobali.CARicerca.altezza, vGlobali.UMAltezzaCorrente));
        String fluido = MessageFormat.format("&#961 {0} {1} ({2}{3} {4} {5} {6} {7})",
                traduci.TraduciStringa("Fluido"),
                traduci.TraduciStringa("a"),
                temperaturaRicerca,
                removePar(vGlobali.UMTemperaturaCorrente.simbolo),
                traduci.TraduciStringa("e"),
                altezzaRicerca,
                removePar(vGlobali.UMAltezzaCorrente.simbolo),
                traduci.TraduciStringa("S.L.M."));
        if (CA_Umidita.getIntegerValue() > 100) {
            CA_Umidita.setEnabled(false);
            CA_Umidita.setValue(100);
            CA_Umidita.setEnabled(true);
        }
        vGlobali.CARicerca.rho = vGlobali.utilityFisica.calcolaDensitaFuido(getDensitaCorrente(), vGlobali.CARicerca.temperatura, vGlobali.CARicerca.altezza,0.0,CA_Umidita.getIntegerValue()); 
        vGlobali.fmtNd.setMaximumFractionDigits(3);
        CA_DensitaFinale.setValue(fluido);
        CA_DensitaFinaleValore.setValue(vGlobali.fmtNd.format(vGlobali.CARicerca.rho) + "&nbspkg/m<sup>3</sup>");
    }
    private void aggiornaQvAndQm() {
        DV_Qv.setEnabled(false);
        DV_Qm.setEnabled(false);
        double fattoreNm3H = vGlobali.CARicerca.rho / vGlobali.utilityFisica.getDensitaAria(0);
        double portataM3h = DV_Nq.getDoubleValue() / fattoreNm3H;
        DV_Qv.setMaximumFractionDigits(vGlobali.UMPortataCorrente.nDecimaliStampa);
        if (portataM3h > 0) {
            DV_Qv.setValue(misure.fromm3hToXX(portataM3h, vGlobali.UMPortataCorrente));
            DV_Qm.setValue(portataM3h * vGlobali.CARicerca.rho);
            vGlobali.SelezioneDati.PortataRichiestam3h = portataM3h;
        } else {
            DV_Qv.setValue("");
            DV_Qm.setValue("");
            vGlobali.SelezioneDati.PortataRichiestam3h = 0.0;
        }
        DV_Qv.setEnabled(true);
        DV_Qm.setEnabled(true);
    }
    private void aggiornaQvAndNq() {
        DV_Qv.setEnabled(false);
        DV_Nq.setEnabled(false);
        double portataM3h = DV_Qm.getDoubleValue() / vGlobali.CARicerca.rho;
        double fattoreNm3h = vGlobali.CARicerca.rho / vGlobali.utilityFisica.getDensitaAria(0);
        if (portataM3h > 0) {
            DV_Qv.setValue(vGlobali.utilityUnitaMisura.fromm3hToXX(portataM3h, vGlobali.UMPortataCorrente));
            DV_Nq.setValue(portataM3h * fattoreNm3h);
            vGlobali.SelezioneDati.PortataRichiestam3h = portataM3h;
            vGlobali.SelezioneDati.NormalPortataRichiesta = portataM3h * fattoreNm3h;
        } else {
            DV_Qv.setValue("");
            DV_Nq.setValue("");
            vGlobali.SelezioneDati.PortataRichiestam3h = 0.0;
            vGlobali.SelezioneDati.NormalPortataRichiesta = 0.0;
        }
        DV_Qv.setEnabled(true);
        DV_Nq.setEnabled(true);
	}
    private void aggiornaNqAndQm() {
        aggiornaNqAndQm(false);
    }
    private void aggiornaNqAndQm(final boolean returning) {
	DV_Nq.setEnabled(false);
        DV_Qm.setEnabled(false);
        if (returning) {
            DV_Nq.setValue("");
            DV_Qm.setValue("");
            vGlobali.SelezioneDati.NormalPortataRichiesta = 0.;
        } else {
            double fattoreNm3H = vGlobali.CARicerca.rho / vGlobali.utilityFisica.getDensitaAria(0);
            double portata = misure.fromXXTom3h(DV_Qv.getDoubleValue(), vGlobali.UMPortataCorrente);
            if (portata > 0) {
                DV_Nq.setValue(portata * fattoreNm3H);
                vGlobali.SelezioneDati.NormalPortataRichiesta = portata * fattoreNm3H;
                DV_Qm.setValue(portata * vGlobali.CARicerca.rho);
            }
        }
        DV_Nq.setEnabled(true);
        DV_Qm.setEnabled(true);
    }
    private double getDensitaCorrente() {
        String tipoFluido = ((oggettoSelect)(CA_ElencoFluidi.getValue())).nome;
        switch (tipoFluido) {
            case "Aria Secca":
                return CA_Secco.getDoubleValue();
            case "Aria Umida":
                return CA_Secco.getDoubleValue();
            case "Esplicito":
                return CA_Esplicito.getDoubleValue();
            case "Miscela":
                return CA_Miscela.getDoubleValue();
            default:
                return 0;
        }
    }
    public void buildComboEsecuzioni() {
        String tipo = ((oggettoSelect)(DV_AssialiCentrifughi.getValue())).nome;
        String trasmissione = ((oggettoSelect)(DV_TrasmissioneDiretti.getValue())).nome;
        DV_EsecuzioneCombo.removeAllItems();
        DV_EsecuzioneCombo.addItem(traduci.TraduciStringa("Default"));
        if (trasmissione.equals("Trasmissione") && tipo.equals("Assiali")) {
            DV_EsecuzioneCombo.addItem("E01");
            DV_EsecuzioneCombo.addItem("E09");
        } else if (trasmissione.equals("Trasmissione")) {
            if (vGlobali.pannelloRicercaEsecuzioniTR1 != null)
                for (String esec : vGlobali.pannelloRicercaEsecuzioniTR1) {
                    DV_EsecuzioneCombo.addItem(esec);
                }
            if (vGlobali.pannelloRicercaEsecuzioniTR2 != null)
                for (String esec : vGlobali.pannelloRicercaEsecuzioniTR2) {
                    DV_EsecuzioneCombo.addItem(esec);
                }            
        } else {
            if (vGlobali.pannelloRicercaEsecuzioniDA != null)
                for (String esec: vGlobali.pannelloRicercaEsecuzioniDA) {
                    DV_EsecuzioneCombo.addItem(esec);
                }
        }
        DV_EsecuzioneCombo.setValue(traduci.TraduciStringa("Default"));
        if (vGlobali.isDonaldson()) {
            DV_EsecuzioneCombo.removeAllItems();
            DV_EsecuzioneCombo.addItem("Standing N4");
            DV_EsecuzioneCombo.addItem("Laying N5");
            DV_EsecuzioneCombo.setValue("Standing N4");
        }
    }
    public void update() {
        String defaultT = (vGlobali.ParametriVari.defaultTolleranza == null)? "Pressione" : vGlobali.ParametriVari.defaultTolleranza;
        tolleranzaSuPressione = !defaultT.equals("Pressione");
        if (tolleranzaSuPressione) {
            DV_TolleranzaSuPortata.addStyleName("evidenziato");
            DV_TolleranzaSuPortata.removeStyleName("schiarito");
            DV_TolleranzaSuPressione.removeStyleName("evidenziato");
            DV_TolleranzaSuPressione.addStyleName("schiarito");
        }
        else {
            DV_TolleranzaSuPressione.addStyleName("evidenziato");
            DV_TolleranzaSuPressione.removeStyleName("schiarito");
            DV_TolleranzaSuPortata.removeStyleName("evidenziato");
            DV_TolleranzaSuPortata.addStyleName("schiarito");
        }
    }
    private void setDefaultValues() {
        CA_Altezza.setValue(0);
        CA_AltezzaUM.setValue(removeHTML(vGlobali.UMAltezzaCorrente.simbolo));
        CA_Temperatura.setConSegno(true);
        CA_Temperatura.setValue(20);
        CA_TemperaturaUM.setValue(removeHTML(vGlobali.UMTemperaturaCorrente.simbolo));
        DV_HUM.setValue(removeHTML(vGlobali.UMPressioneCorrente.simbolo));
        DV_QvUM.setValue(removeHTML(vGlobali.UMPortataCorrente.simbolo));
        DV_FrequenzaCombo.select(vGlobali.CARicerca.frequenza);
        DV_QmUM.setValue("[kg/h]");
        DV_NqUM.setValue("[Nm³/h]");
        DV_TolleranzaSuPressione.setCaption(MessageFormat.format("{0} -{1}% +{2}%", 
            traduci.TraduciStringa("Tolleranza"),
            vGlobali.ParametriVari.pannelloRicercaTolleranzaPressioneMeno * 100,
            vGlobali.ParametriVari.pannelloRicercaTolleranzaPressionePiu * 100));
        DV_TolleranzaSuPortata.setCaption(MessageFormat.format("{0} -{1}% +{2}%", 
            traduci.TraduciStringa("Tolleranza"),
            vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO * 100,
            vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezionePIU * 100));
        if (tolleranzaSuPressione) {
            DV_TolleranzaSuPortata.addStyleName("evidenziato");
            DV_TolleranzaSuPortata.removeStyleName("schiarito");
            DV_TolleranzaSuPressione.removeStyleName("evidenziato");
            DV_TolleranzaSuPressione.addStyleName("schiarito");
            vGlobali.SelezioneDati.tolleranzaPortataFlag = false;
        }
        else {
            DV_TolleranzaSuPressione.addStyleName("evidenziato");
            DV_TolleranzaSuPressione.removeStyleName("schiarito");
            DV_TolleranzaSuPortata.removeStyleName("evidenziato");
            DV_TolleranzaSuPortata.addStyleName("schiarito");
            vGlobali.SelezioneDati.tolleranzaPortataFlag = true;        
        }
        oggettoSelect temp = vGlobali.utilityCliente.TotStaDefault.equals("Totale") ? Totale : Statica;
        DV_TotaleStatica.setValue(temp);
        temp = vGlobali.utilityCliente.AspManDefault.equals("Aspirazione") ? Aspirazione : Mandata;
        DV_AspirazioneMandata.setValue(temp);
        if (vGlobali.isDonaldson()) {
            DV_EsecuzioneCombo.setValue(Totale);
            DV_AssialiCentrifughi.setValue(Centrifughi);
            DV_TrasmissioneDiretti.setValue(Diretto);
            DV_AspirazioneMandata.setValue(Aspirazione);
            DV_TotaleStatica.setValue(Statica);
        }
        if (vGlobali.presetRicerca != null) {
            modificabile = true;
            DV_Qv.setValue(vGlobali.presetRicerca.Portata);
            DV_HUM.setValue(removeHTML("<html>[mmH<sub>2</sub>O]</html>"));
            DV_HUM_valueChange();
            DV_H.setValue(vGlobali.presetRicerca.Prevalenza);
            DV_Qv_valueChange();
            DV_H_valueChange();
        }
    }
    private String removePar(String s) {
        s = s.replace("[", "");
        s = s.replace("]", "");
        return s;
    }
    private String removeHTML(String s) {
        s = s.replace("<html>", "");
        s = s.replace("</html>", "");
        Pattern pattern = Pattern.compile("<.*?>(.*?)<(/.*?)>");
        Matcher matcher = pattern.matcher(s);
        if (matcher.find()) {
            String value = matcher.group(1);
            String type = matcher.group(2);
            if (value.equals("2") && type.equals("/sub")) value = "₂";
            if (value.equals("2") && type.equals("/sup")) value = "²";
            if (value.equals("3") && type.equals("/sub")) value = "₃";
            if (value.equals("3") && type.equals("/sup")) value = "³";
            s = matcher.replaceAll(value);
        }
        return s;
    }
        
    protected void initUI() {
        modificabile = false;
        this.layoutPrincipale = new XdevVerticalLayout();
        this.layoutCondizioni = new XdevVerticalLayout();
        this.layoutVentilator = new XdevVerticalLayout();
        this.CA_CondizioniGrid = new XdevGridLayout(3, 2);
        this.CA_FluidoGrid = new XdevGridLayout(2, 4);
        this.pannelloCondizioni = new Panel();
        this.pannelloVentilator = new Panel();
        this.CA_Altezza = new TextFieldDouble();
        this.CA_Temperatura = new TextFieldDouble();
        this.CA_Esplicito = new TextFieldDouble();
        this.CA_Secco = new TextFieldDouble();
        this.CA_Umidita = new TextFieldInteger();
        this.CA_Miscela = new TextFieldDouble();
        this.CA_AltezzaUM = new XdevComboBox<>();
        this.CA_TemperaturaUM = new XdevComboBox<>();
        CA_MiscelaButton = new Button("Definisci");
        CA_ElencoFluidi = new XdevOptionGroup<>();
        DV_AssialiCentrifughi = new XdevOptionGroup<>();
        DV_TrasmissioneDiretti = new XdevOptionGroup<>();
        
        
        CA_AltezzaLabel = new XdevLabel("h<sub>s.l.m</sub>", ContentMode.HTML);
        CA_TemperaturaLabel = new XdevLabel("T<sub>fluido</sub>", ContentMode.HTML);
        CA_SeccoLabel = new XdevLabel("kg/m<sup>3</sup>&nbsp(a 20°C e 0 m)", ContentMode.HTML);
        CA_EsplicitoLabel = new XdevLabel("kg/m<sup>3</sup>&nbsp(a 20°C e 0 m)", ContentMode.HTML);
        CA_UmiditaLabel = new XdevLabel("  %");
        CA_DensitaFinale = new XdevLabel("&#961&nbspFluido (a 20°C e 0 m)", ContentMode.HTML);
        CA_DensitaFinaleValore = new XdevLabel("1.204" + "&nbspkg/m<sup>3</sup>", ContentMode.HTML);
        
        CA_AltezzaLabel.setWidth(100, Unit.PERCENTAGE);
        CA_Altezza.setWidth(100, Unit.PERCENTAGE);
        CA_AltezzaUM.setWidth(100, Unit.PERCENTAGE);
        CA_TemperaturaLabel.setWidth(100, Unit.PERCENTAGE);
        CA_Temperatura.setWidth(100, Unit.PERCENTAGE);
        CA_TemperaturaUM.setWidth(100, Unit.PERCENTAGE);
        CA_Secco.setWidth(100, Unit.PERCENTAGE);
        CA_SeccoLabel.setWidth(100, Unit.PERCENTAGE);
        CA_Umidita.setWidth(100, Unit.PERCENTAGE);
        CA_UmiditaLabel.setWidth(100, Unit.PERCENTAGE);
        CA_Esplicito.setWidth(100, Unit.PERCENTAGE);
        CA_EsplicitoLabel.setWidth(100, Unit.PERCENTAGE);
        CA_Miscela.setWidth(100, Unit.PERCENTAGE);
        CA_DensitaFinale.setWidth(100, Unit.PERCENTAGE);
        CA_DensitaFinaleValore.setWidth(100, Unit.PERCENTAGE);
        
        Secca = new oggettoSelect("Aria Secca");
        Umida = new oggettoSelect("Aria Umida");
        Esplicita = new oggettoSelect("Esplicito");
        Miscela = new oggettoSelect("Miscela");
        
        
        CA_Secco.setValue(1.204);
        CA_Secco.setReadOnly(true);
        CA_Umidita.setValue(0);
        CA_Miscela.setValue(1.204);
        CA_Miscela.setReadOnly(true);
        
        CA_Esplicito.setValue(1.204);
        
        CA_Secco.setStyleName("TextFieldElenco");
        CA_SeccoLabel.setStyleName("EtichettaElenco");
        CA_Umidita.setStyleName("TextFieldElenco");
        CA_UmiditaLabel.setStyleName("EtichettaElenco");
        CA_Esplicito.setStyleName("TextFieldElenco");
        CA_EsplicitoLabel.setStyleName("EtichettaElenco");
        CA_Miscela.setStyleName("TextFieldElenco");
        CA_MiscelaButton.setStyleName("BottoneElenco");
        //CA_DensitaFinale.setStyleName("BigLabel");
        CA_DensitaFinaleValore.setStyleName("VeryBigLabel");
        
        CA_ElencoFluidi.addItems(Secca, Umida, Esplicita, Miscela);
        CA_ElencoFluidi.setStyleName("Elenco");
        CA_ElencoFluidi.setValue(Secca);
        CA_ElencoFluidi.setWidth(100, Unit.PERCENTAGE);
        CA_ElencoFluidi.setHeightUndefined();
        this.CA_CondizioniGrid.addComponent(CA_AltezzaLabel, 0, 0, Alignment.MIDDLE_LEFT);
        this.CA_CondizioniGrid.addComponent(CA_Altezza, 1, 0, Alignment.MIDDLE_LEFT);
        this.CA_CondizioniGrid.addComponent(CA_AltezzaUM, 2, 0, Alignment.MIDDLE_LEFT);
        this.CA_CondizioniGrid.addComponent(CA_TemperaturaLabel, 0, 1, Alignment.MIDDLE_LEFT);
        this.CA_CondizioniGrid.addComponent(CA_Temperatura, 1, 1, Alignment.MIDDLE_LEFT);
        this.CA_CondizioniGrid.addComponent(CA_TemperaturaUM, 2, 1, Alignment.MIDDLE_LEFT);
        this.CA_CondizioniGrid.addStyleName("NoBottomPadding");

        
        //this.CA_MiscelaButton.setHeight(26, Unit.PIXELS);
        this.CA_FluidoGrid.addComponent(CA_Secco, 0, 0);
        this.CA_FluidoGrid.addComponent(CA_SeccoLabel, 1, 0);
        this.CA_FluidoGrid.addComponent(CA_Umidita, 0, 1);
        this.CA_FluidoGrid.addComponent(CA_UmiditaLabel, 1, 1);
        this.CA_FluidoGrid.addComponent(CA_Esplicito, 0, 2);
        this.CA_FluidoGrid.addComponent(CA_EsplicitoLabel, 1, 2);
        this.CA_FluidoGrid.addComponent(CA_Miscela, 0, 3);
        this.CA_FluidoGrid.addComponent(CA_MiscelaButton, 1, 3);
        this.CA_FluidoGrid.setRowExpandRatios(1.0F, 1.0F, 1.0F, 1.0F);
        this.CA_FluidoGrid.setColumnExpandRatios(1.0F, 1.5F);
        this.CA_FluidoGrid.setWidth("100%");
        this.CA_FluidoGrid.setStyleName("Elenco");
        
        //Layout orizzontale che contiene RadioGroup tipi Fluido e controlli specifici
        HLtipoFluido = new HorizontalLayout();
        HLtipoFluido.setWidth(100, Unit.PERCENTAGE);
        HLtipoFluido.addComponents(CA_ElencoFluidi, CA_FluidoGrid);
        HLtipoFluido.setExpandRatio(CA_ElencoFluidi, 1.0F);
        HLtipoFluido.setExpandRatio(CA_FluidoGrid, 2.0F);
        //
        CA_DensitaFinaleLayout = new HorizontalLayout();
        CA_DensitaFinaleLayout.setWidth(100, Unit.PERCENTAGE);
        CA_DensitaFinaleLayout.addComponents(CA_DensitaFinale, CA_DensitaFinaleValore);
        CA_DensitaFinaleLayout.setExpandRatio(CA_DensitaFinale, 2.0F);
        CA_DensitaFinaleLayout.setExpandRatio(CA_DensitaFinaleValore, 1.0F);
        
        this.CA_CondizioniGrid.setWidth(100, Unit.PERCENTAGE);
        this.CA_CondizioniGrid.setColumnExpandRatios(1, 1, 1);
        this.CA_CondizioniGrid.setRowExpandRatios(1, 1);
        layoutCondizioni.addComponents(CA_CondizioniGrid, HLtipoFluido, CA_DensitaFinaleLayout);
        this.pannelloCondizioni.setStyleName("pannelliRicerca");
        this.pannelloCondizioni.setContent(layoutCondizioni);
        this.pannelloCondizioni.setCaption("Condizioni Ambientali");
        
        
        Assiali = new oggettoSelect("Assiali");
        Centrifughi = new oggettoSelect("Centrifughi");
        Trasmissione = new oggettoSelect("Trasmissione");
        Diretto = new oggettoSelect("Ventilatori diretti");
        Aspirazione = new oggettoSelect("Aspirazione");
        Mandata = new oggettoSelect("Mandata");
        Totale = new oggettoSelect("Totale");
        Statica = new oggettoSelect("Statica");
        DV_AssialiCentrifughi = new OptionGroup();
        DV_AssialiCentrifughi.addItems(Assiali, Centrifughi);
        DV_AssialiCentrifughi.setStyleName("ElencoStretto");
        //DV_AssialiCentrifughi.setMultiSelect(true);
        DV_AssialiCentrifughi.setValue(Centrifughi);
        DV_AssialiCentrifughi.setWidth(100, Unit.PERCENTAGE);
        DV_AssialiCentrifughi.setHeightUndefined();
        DV_AssialiCentrifughi.addValueChangeListener((Property.ValueChangeEvent event) -> {
            buildComboEsecuzioni();
        });
        DV_TrasmissioneDiretti.addItems(Trasmissione, Diretto);
        DV_TrasmissioneDiretti.setStyleName("ElencoStretto");
        DV_TrasmissioneDiretti.setValue(Diretto);
        DV_TrasmissioneDiretti.setWidth(100, Unit.PERCENTAGE);
        DV_TrasmissioneDiretti.setHeightUndefined();
        DV_TrasmissioneDiretti.addValueChangeListener((Property.ValueChangeEvent event) -> {
            buildComboEsecuzioni();
        });
        
        DV_PrimoBlocco = new HorizontalLayout(DV_AssialiCentrifughi, DV_TrasmissioneDiretti);
        DV_PrimoBlocco.setWidth(100, Unit.PERCENTAGE);
        
        DV_FrequenzaLabel = new XdevLabel("Frequenza (Hz)");
        DV_FrequenzaCombo = new XdevComboBox<>();
        DV_EsecuzioneLabel = new XdevLabel("Esecuzione");
        DV_EsecuzioneCombo = new XdevComboBox<>();
        DV_InfoEsecuzione = new Button("i");
        DV_HLabel = new XdevLabel("H");
        DV_H = new TextFieldDouble();
        DV_HUM = new XdevComboBox<>();
        DV_TotaleStatica = new XdevOptionGroup();
        DV_AspirazioneMandata = new XdevOptionGroup();
        
        DV_FrequenzaLabel.setWidth(100, Unit.PERCENTAGE);
        DV_FrequenzaCombo.setWidth(100, Unit.PERCENTAGE);
        DV_EsecuzioneLabel.setWidth(100, Unit.PERCENTAGE);
        DV_EsecuzioneCombo.setWidth(100, Unit.PERCENTAGE);
        DV_InfoEsecuzione.setWidth(26, Unit.PIXELS);
        DV_HLabel.setWidth(100, Unit.PERCENTAGE);
        DV_H.setWidth(100, Unit.PERCENTAGE);
        DV_HUM.setSizeFull();
        DV_TotaleStatica.setSizeFull();
        DV_AspirazioneMandata.setSizeFull();
        
        
        DV_FrequenzaLabel.setStyleName("EtichettaElenco");
        DV_EsecuzioneLabel.setStyleName("EtichettaElenco");
        DV_FrequenzaCombo.setStyleName("Elenco");
        DV_EsecuzioneCombo.setStyleName("Elenco");
        DV_InfoEsecuzione.addStyleName("BottoneInfo");
        DV_HLabel.setStyleName("EtichettaElenco");
        DV_H.setStyleName("TextFieldElenco");
        DV_HUM.setStyleName("Elenco");
        DV_TotaleStatica.setStyleName("ElencoStretto");
        DV_AspirazioneMandata.setStyleName("ElencoStretto");
        
        DV_FrequenzaCombo.addItems(50, 60);
        DV_FrequenzaCombo.select(50);
        DV_TotaleStatica.addItems(Totale, Statica);
        DV_TotaleStatica.setValue(Totale);
        DV_AspirazioneMandata.addItems(Aspirazione, Mandata);
        DV_AspirazioneMandata.setValue(Mandata);
        
        
        DV_FrequenzaEsecuzioneGrid = new XdevGridLayout(3, 2);
        DV_FrequenzaEsecuzioneGrid.setColumnExpandRatios(3, 3, 1);
        DV_FrequenzaEsecuzioneGrid.setWidth(100, Unit.PERCENTAGE);
        DV_FrequenzaEsecuzioneGrid.addComponent(DV_FrequenzaLabel, 0, 0);
        DV_FrequenzaEsecuzioneGrid.addComponent(DV_FrequenzaCombo, 1, 0);
        DV_FrequenzaEsecuzioneGrid.addComponent(DV_EsecuzioneLabel, 0, 1);
        DV_FrequenzaEsecuzioneGrid.addComponent(DV_EsecuzioneCombo, 1, 1);
        DV_FrequenzaEsecuzioneGrid.addComponent(DV_InfoEsecuzione, 2, 1);
        DV_FrequenzaEsecuzioneGrid.addStyleName("NoTopPadding");
        
        DV_PressioneGrid = new XdevGridLayout(6, 2);
        DV_PressioneGrid.setWidth(100, Unit.PERCENTAGE);
        DV_PressioneGrid.addComponent(DV_HLabel, 0, 0);
        DV_PressioneGrid.addComponent(DV_H, 1, 0, 2, 0);
        DV_PressioneGrid.addComponent(DV_HUM, 3, 0, 5, 0);
        DV_PressioneGrid.addComponent(DV_TotaleStatica, 0, 1, 2, 1);
        DV_PressioneGrid.addComponent(DV_AspirazioneMandata, 3, 1, 5, 1);
        DV_PressioneGrid.addStyleName("NoBottomPadding");
        
        DV_TolleranzaSuPressione = new Button("Tolleranza -10%+20%");
        DV_TolleranzaSuPressione.setStyleName("double");
        DV_TolleranzaSuPressione.addStyleName("bigger");
        DV_TolleranzaSuPressione.setCaptionAsHtml(true);
        DV_TolleranzaSuPressione.setSizeFull();
        
        DV_PressioneHL = new HorizontalLayout(DV_PressioneGrid, DV_TolleranzaSuPressione);
        DV_PressioneHL.setWidth(100, Unit.PERCENTAGE);
        DV_PressioneHL.setExpandRatio(DV_PressioneGrid, 3);
        DV_PressioneHL.setExpandRatio(DV_TolleranzaSuPressione, 1);
        DV_PressioneHL.setStyleName("bordoGrigio");
        
        DV_QvLabel = new XdevLabel("Qv");
        DV_Qv = new TextFieldDouble();
        DV_QvUM = new XdevComboBox<>();
        DV_QmLabel = new XdevLabel("Qm");
        DV_Qm = new TextFieldDouble();
        DV_QmUM = new XdevComboBox<>();
        DV_NqLabel = new XdevLabel("Nq");
        DV_Nq = new TextFieldDouble();
        DV_NqUM = new XdevComboBox<>();
        
        DV_QvLabel.setSizeFull();
        DV_Qv.setSizeFull();
        DV_QvUM.setSizeFull();
        DV_QmLabel.setSizeFull();
        DV_Qm.setSizeFull();
        DV_QmUM.setSizeFull();
        DV_NqLabel.setSizeFull();
        DV_Nq.setSizeFull();
        DV_NqUM.setSizeFull();
        
        DV_QvLabel.setStyleName("EtichettaElenco");
        DV_Qv.setStyleName("TextFieldElenco");
        DV_QvUM.setStyleName("Elenco");
        DV_QmLabel.setStyleName("EtichettaElenco");
        DV_Qm.setStyleName("TextFieldElenco");
        DV_QmUM.setStyleName("Elenco");
        DV_NqLabel.setStyleName("EtichettaElenco");
        DV_Nq.setStyleName("TextFieldElenco");
        DV_NqUM.setStyleName("Elenco");
        
        DV_PortataGrid = new XdevGridLayout(6, 3);
        DV_PortataGrid.setWidth(100, Unit.PERCENTAGE);
        DV_PortataGrid.addComponent(DV_QvLabel, 0, 0);
        DV_PortataGrid.addComponent(DV_Qv, 1, 0, 2, 0);
        DV_PortataGrid.addComponent(DV_QvUM, 3, 0, 5, 0);
        DV_PortataGrid.addComponent(DV_QmLabel, 0, 1);
        DV_PortataGrid.addComponent(DV_Qm, 1, 1, 2, 1);
        DV_PortataGrid.addComponent(DV_QmUM, 3, 1, 5, 1);
        DV_PortataGrid.addComponent(DV_NqLabel, 0, 2);
        DV_PortataGrid.addComponent(DV_Nq, 1, 2, 2, 2);
        DV_PortataGrid.addComponent(DV_NqUM, 3, 2, 5, 2);
        DV_PortataGrid.addStyleName("NoBottomPadding");
        
        DV_TolleranzaSuPortata = new Button("Tolleranza -10%+20%");
        DV_TolleranzaSuPortata.setStyleName("double");
        DV_TolleranzaSuPortata.setCaptionAsHtml(true);
        DV_TolleranzaSuPortata.setSizeFull();
        
        DV_PortataHL = new HorizontalLayout(DV_PortataGrid, DV_TolleranzaSuPortata);
        DV_PortataHL.setWidth(100, Unit.PERCENTAGE);
        DV_PortataHL.setExpandRatio(DV_PortataGrid, 3);
        DV_PortataHL.setExpandRatio(DV_TolleranzaSuPortata, 1);
        DV_PortataHL.setStyleName("bordoGrigio");
        
        
        layoutVentilator.addComponents(DV_PrimoBlocco, DV_FrequenzaEsecuzioneGrid, DV_PortataHL, DV_PressioneHL);
        
        EN_SelezioneAvanzata = new Button("Selezione Avanzata");
//        EN_SelezioneAvanzata.setStyleName("double");
        EN_SelezioneAvanzata.setSizeFull();
        EN_Reset = new Button("Reset");
//        EN_Reset.setStyleName("double");
        EN_Reset.setSizeFull();
        
        EN_Ricerca = new Button("Cerca");
        EN_Ricerca.setStyleName("doubleFull");
        EN_Ricerca.setSizeFull();
        
        EN_BottoniGrid = new XdevGridLayout(2, 2);
        EN_BottoniGrid.setWidth(100, Unit.PERCENTAGE);
        EN_BottoniGrid.addComponent(EN_SelezioneAvanzata, 0, 0);
        EN_BottoniGrid.addComponent(EN_Reset, 0, 1);
        EN_BottoniGrid.addComponent(EN_Ricerca, 1, 0, 1, 1);
        EN_BottoniGrid.addStyleName("NoBottomPadding");
        
        
        
        
        this.pannelloVentilator.setCaption("Dati Ventilatore");
        this.pannelloVentilator.setContent(layoutVentilator);
        this.pannelloVentilator.setStyleName("pannelliRicerca");
        this.layoutPrincipale.addComponents(pannelloCondizioni, pannelloVentilator, EN_BottoniGrid);
        this.layoutPrincipale.addStyleName("ZeroBottomPadding");
        this.setSizeFull();
        this.setContent(layoutPrincipale);
        
        CA_Altezza.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            CA_Altezza_valueChange(event);
        });
        CA_AltezzaUM.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            CA_AltezzaUM_valueChange(event);
        });
        CA_Temperatura.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            CA_Temperatura_valueChange(event);
        });
        CA_TemperaturaUM.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            CA_TemperaturaUM_valueChange(event);
        });
        CA_ElencoFluidi.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            CA_ElencoFluidi_valueChange();
        });
        CA_Umidita.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            CA_Umidita_valueChange();
        });
        CA_Esplicito.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            CA_Esplicito_valueChange();
        });
        CA_MiscelaButton.addClickListener(event -> CA_MiscelaButton_buttonClick());
        DV_AspirazioneMandata.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            DV_AspirazioneMandata_valueChange();
        });
        DV_AssialiCentrifughi.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            DV_AssialiCentrifughi_valueChange();
        });
        DV_TrasmissioneDiretti.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_TrasmissioneDiretti_valueChange();
        });
        DV_FrequenzaCombo.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_FrequenzaCombo_valueChange();
        });
        DV_EsecuzioneCombo.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_EsecuzioneCombo_valueChange();
        });
        DV_H.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_H_valueChange();
        });
        DV_HUM.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_HUM_valueChange();
        });
        DV_TotaleStatica.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_TotaleStatica_valueChange();
        });
        DV_Qv.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_Qv_valueChange();
        });
        DV_QvUM.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_QvUM_valueChange();
        });
        DV_Qm.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_Qm_valueChange();
        });
        DV_Nq.addValueChangeListener((final Property.ValueChangeEvent event) ->{
            DV_Nq_valueChange();
        });
        DV_InfoEsecuzione.addClickListener(event -> DV_InfoEsecuzione_buttonClick());
        DV_TolleranzaSuPortata.addClickListener(event -> DV_TolleranzaSuPortata_buttonClick());
        DV_TolleranzaSuPressione.addClickListener(event -> DV_TolleranzaSuPressione_buttonClick());
        EN_SelezioneAvanzata.addClickListener(event -> EN_SelezioneAvanzata_buttonClick());
        EN_Ricerca.addClickListener(event -> EN_Ricerca_buttonClick());
        EN_Reset.addClickListener(event -> EN_Reset_buttonClick());
        layoutPrincipale.setSpacing(false);
        modificabile = true;
        
    }
    
    private Button CA_MiscelaButton, DV_InfoEsecuzione, DV_TolleranzaSuPressione, DV_TolleranzaSuPortata, EN_SelezioneAvanzata, EN_Reset, EN_Ricerca;
    private XdevLabel CA_AltezzaLabel, CA_TemperaturaLabel, CA_SeccoLabel, CA_EsplicitoLabel, CA_UmiditaLabel, CA_DensitaFinale, CA_DensitaFinaleValore;
    private XdevLabel DV_FrequenzaLabel, DV_EsecuzioneLabel, DV_HLabel, DV_QvLabel, DV_QmLabel, DV_NqLabel, DV_UMQDon, DV_UMPDon;
    private XdevVerticalLayout layoutPrincipale, layoutCondizioni, layoutVentilator;
    private HorizontalLayout CA_DensitaFinaleLayout, HLtipoFluido, DV_PrimoBlocco, DV_PressioneHL, DV_PortataHL;
    private Panel pannelloCondizioni, pannelloVentilator;
    private TextFieldInteger CA_Umidita;
    private TextFieldDouble CA_Altezza, CA_Temperatura, CA_Esplicito, CA_Secco, CA_Miscela;
    private TextFieldDouble DV_H, DV_Qv, DV_Qm, DV_Nq;
    private XdevGridLayout CA_CondizioniGrid, CA_FluidoGrid, DV_FrequenzaEsecuzioneGrid, DV_PressioneGrid, DV_PortataGrid, EN_BottoniGrid;
    private XdevComboBox<CustomComponent> CA_AltezzaUM, CA_TemperaturaUM, DV_FrequenzaCombo, DV_EsecuzioneCombo, DV_HUM, DV_QvUM, DV_QmUM, DV_NqUM;
    private OptionGroup CA_ElencoFluidi, DV_AssialiCentrifughi, DV_TrasmissioneDiretti, DV_TotaleStatica, DV_AspirazioneMandata;

    private void CA_Altezza_valueChange(Property.ValueChangeEvent event) {
        if (!modificabile) return;
        modificabile = false;
        vGlobali.CARicerca.altezza = misure.fromXXTom(CA_Altezza.getDoubleValue(), vGlobali.UMAltezzaCorrente);
        buildDensitaCorrente();
        aggiornaNqAndQm();
        this.searchView.reset();
        modificabile = true;
    }
    private void CA_AltezzaUM_valueChange(Property.ValueChangeEvent event) {
        if (!modificabile || CA_AltezzaUM.getValue() == null) return;
        modificabile = false;
        UnitaMisura UMSelezionata = null;
        String valoreScelto = CA_AltezzaUM.getValue().toString();
        for (UnitaMisura unit : misure.ElencoUnitaMisuraLunghezza) {
            if (removeHTML(unit.simbolo).equals(valoreScelto)) {
                UMSelezionata = unit;
            }
        }
        double altezza = misure.fromXXTom(CA_Altezza.getDoubleValue(), vGlobali.UMAltezzaCorrente);
        vGlobali.UMAltezzaCorrente = UMSelezionata;
        CA_Altezza.setEnabled(false);
        CA_Altezza.setMaximumFractionDigits(vGlobali.UMAltezzaCorrente.nDecimaliStampa);
        CA_Altezza.setValue(misure.frommToXX(altezza, UMSelezionata));
        CA_Altezza.setEnabled(true);
        vGlobali.CARicerca.altezza = altezza;
        buildDensitaCorrente();
        aggiornaNqAndQm();
        this.searchView.reset();
        modificabile = true;
    }
    private void CA_Temperatura_valueChange(Property.ValueChangeEvent event) {
        if (!modificabile) return;
        modificabile = false;
        vGlobali.CARicerca.temperatura = misure.fromXXtoCelsius(vGlobali.UMTemperaturaCorrente.index, CA_Temperatura.getDoubleValue());
        if (vGlobali.isDonaldson()) {
            if (vGlobali.CARicerca.temperatura>100) {
                showPopupMZ("Temperature too high");
                vGlobali.CARicerca.temperatura=20;
                CA_Temperatura.setValue(misure.fromCelsiusToXX(vGlobali.UMTemperaturaCorrente.index, 20));
            }
        }
        buildDensitaCorrente();
        aggiornaNqAndQm();
        this.searchView.reset();
        modificabile = true;
    }
    private void CA_TemperaturaUM_valueChange(Property.ValueChangeEvent event) {
        if (!modificabile || CA_TemperaturaUM.getValue() == null) return;
        modificabile = false;
        UnitaMisura UMSelezionata = null;
        String valoreScelto = CA_TemperaturaUM.getValue().toString();
        for (UnitaMisura unit : misure.ElencoUnitaMisuraTemperatura) {
            if (removeHTML(unit.simbolo).equals(valoreScelto)) {
                UMSelezionata = unit;
            }
        }
        double temperatura = misure.fromXXtoCelsius(vGlobali.UMTemperaturaCorrente.index, CA_Temperatura.getDoubleValue());
        vGlobali.UMTemperaturaCorrente = UMSelezionata;
        CA_Temperatura.setEnabled(false);
        CA_Temperatura.setMaximumFractionDigits(vGlobali.UMTemperaturaCorrente.nDecimaliStampa);
        CA_Temperatura.setValue(misure.fromCelsiusToXX(UMSelezionata.index, temperatura));
        CA_Temperatura.setEnabled(true);
        vGlobali.CARicerca.temperatura = temperatura;
        buildDensitaCorrente();
        aggiornaNqAndQm();
        this.searchView.reset();
        modificabile = true;
    }
    private void CA_ElencoFluidi_valueChange(){
        if (!modificabile) return;
        modificabile = false;
        String tipoFluido = ((oggettoSelect)(CA_ElencoFluidi.getValue())).nome;
        vGlobali.CA020Ricerca = new CondizioniAmbientali();
	switch (tipoFluido) {
            case "Aria Secca":
                CA_Umidita.setEnabled(false);
                CA_Umidita.setValue(0);
                CA_Esplicito.setEnabled(false);
                CA_Esplicito.setValue(0);
                CA_MiscelaButton.setEnabled(false);
                break;
            case "Aria Umida":
                CA_Secco.setValue(vGlobali.CA020Ricerca.rho);
                CA_Umidita.setEnabled(true);
                CA_Esplicito.setEnabled(false);
                CA_Esplicito.setValue(0);
                CA_MiscelaButton.setEnabled(false);
                break;
            case "Esplicito":
                CA_Secco.setValue(vGlobali.CA020Ricerca.rho);
                CA_Umidita.setEnabled(false);
                CA_Umidita.setValue(0);
                CA_Esplicito.setEnabled(true);
                CA_Esplicito.setValue(vGlobali.CA020Ricerca.rho);
                CA_MiscelaButton.setEnabled(false);
                break;
            case "Miscela":
                CA_Secco.setValue(vGlobali.CA020Ricerca.rho);
                CA_Umidita.setEnabled(false);
                CA_Umidita.setValue(0);
                CA_Esplicito.setEnabled(false);
                CA_Esplicito.setValue(vGlobali.CA020Ricerca.rho);
                CA_MiscelaButton.setEnabled(true);
                break;
            default:
        }
        buildDensitaCorrente();
        aggiornaNqAndQm();
	this.searchView.reset();
	modificabile = true;
    }
    private void CA_Umidita_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        if (CA_Umidita.getDoubleValue()>100)
            CA_Umidita.setValue(100);
        buildDensitaCorrente();
        aggiornaNqAndQm();
        searchView.reset();
        modificabile = true;
    }
    private void CA_Esplicito_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        buildDensitaCorrente();
        aggiornaNqAndQm();
        searchView.reset();
        modificabile = true;
    }
    private void CA_MiscelaButton_buttonClick() {
        searchView.reset();
        pannelloMiscela pannelloMiscela = new pannelloMiscela();
        pannelloMiscela.setVariabiliGlobali(vGlobali);
        pannelloMiscela.init();
        MessageBox popup = MessageBox.create();
        popup.withCaption(traduci.TraduciStringa("Calcolo Densità fluido"));
        popup.withMessage(pannelloMiscela);
        popup.withWidth("40%");
        popup.withAbortButton();
        popup.withOkButton(() -> {
            pannelloMiscela.aggiornaPesiGas();
            modificabile = false;
            CA_Miscela.setReadOnly(false);
            CA_Miscela.setValue(pannelloMiscela.getDensitaCalcolata());
            CA_Miscela.setReadOnly(true);
            buildDensitaCorrente();
            aggiornaNqAndQm();
            searchView.reset();
            modificabile = true;
        });
        popup.open();
    }
    private void DV_AssialiCentrifughi_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        serieDaRicaricare = true;
        DV_AspirazioneMandata.setEnabled(((oggettoSelect)(DV_AssialiCentrifughi.getValue())).theSame("Centrifughi"));
        searchView.reset();
        modificabile = true;
    }
    private void DV_AspirazioneMandata_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        vGlobali.ricercaAspirazione = ((oggettoSelect)(DV_AspirazioneMandata.getValue())).theSame("Aspirazione");
        searchView.reset();
        modificabile = true;
    }
    private void DV_TrasmissioneDiretti_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        searchView.reset();
        modificabile = true;
    }
    private void DV_FrequenzaCombo_valueChange() {
        if (!modificabile) return;
        searchView.reset();
    }
    private void DV_EsecuzioneCombo_valueChange() {
        if (!modificabile) return;
        searchView.reset();
        }
    private void DV_H_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        vGlobali.SelezioneDati.PressioneRichiestaPa = misure.fromXXToPa(DV_H.getDoubleValue(), vGlobali.UMPressioneCorrente);
        if (vGlobali.isDonaldson()) {
            if (vGlobali.SelezioneDati.PressioneRichiestaPa>6100) {
                showPopupMZ("Pressure too high");
                vGlobali.SelezioneDati.PressioneRichiestaPa=0;
                DV_H.setValue(misure.fromPaToXX(0, vGlobali.UMPressioneCorrente));
            }
        }
        searchView.reset();
        modificabile = true;
    }
    private void DV_HUM_valueChange() {
        if (!modificabile || DV_HUM.getValue() == null) return;
        modificabile = false;
        UnitaMisura UMSelezionata = null;
        String valoreScelto = DV_HUM.getValue().toString();
        for (UnitaMisura unit : misure.ElencoUnitaMisuraPressione) {
            if (removeHTML(unit.simbolo).equals(valoreScelto)) {
                UMSelezionata = unit;
            }
        }
        double pressione = misure.fromXXToPa(DV_H.getDoubleValue(), vGlobali.UMPressioneCorrente);
        vGlobali.UMPressioneCorrente = UMSelezionata;
        DV_H.setEnabled(false);
        DV_H.setMaximumFractionDigits(vGlobali.UMPressioneCorrente.nDecimaliStampa);
        DV_H.setValue(misure.fromPaToXX(pressione, UMSelezionata));
        DV_H.setEnabled(true);
        modificabile = true;
    }
    private void DV_TotaleStatica_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        vGlobali.ricercaTotale = ((oggettoSelect)(DV_TotaleStatica.getValue())).theSame("Totale");
        searchView.reset();
        modificabile = true;
    }
    private void DV_Qv_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        vGlobali.SelezioneDati.PortataRichiestam3h = misure.fromXXTom3h(DV_Qv.getDoubleValue(), vGlobali.UMPortataCorrente);
        if (vGlobali.isDonaldson()) {
            if (vGlobali.SelezioneDati.PortataRichiestam3h>40100) {
                showPopupMZ("Flow rate too high");
                vGlobali.SelezioneDati.PortataRichiestam3h=0;
                DV_Qv.setValue(misure.fromm3hToXX(0, vGlobali.UMPortataCorrente));
            }
        }
        aggiornaNqAndQm();
        searchView.reset();
        modificabile = true;
    }
    private void DV_QvUM_valueChange() {
        if (!modificabile || DV_QvUM.getValue() == null) return;
        modificabile = false;
        UnitaMisura UMSelezionata = null;
        String valoreScelto = DV_QvUM.getValue().toString();
        for (UnitaMisura unit : misure.ElencoUnitaMisuraPortata) {
            if (removeHTML(unit.simbolo).equals(valoreScelto)) {
                UMSelezionata = unit;
            }
        }
        double portata = misure.fromXXTom3h(DV_Qv.getDoubleValue(), vGlobali.UMPortataCorrente);
        vGlobali.UMPortataCorrente = UMSelezionata;
        DV_Qv.setEnabled(false);
        DV_Qv.setMaximumFractionDigits(vGlobali.UMPortataCorrente.nDecimaliStampa);
        DV_Qv.setValue(misure.fromm3hToXX(portata, UMSelezionata));
        DV_Qv.setEnabled(true);
        vGlobali.SelezioneDati.PortataRichiestam3h = portata;
        aggiornaNqAndQm();
        modificabile = true;
    }
    private void DV_Nq_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        vGlobali.SelezioneDati.NormalPortataRichiesta = DV_Nq.getDoubleValue();
        aggiornaQvAndQm();
        searchView.reset();
        modificabile = true;
    }
    private void DV_Qm_valueChange() {
        if (!modificabile) return;
        modificabile = false;
        aggiornaQvAndNq();
        searchView.reset();
        modificabile = true;
    }
    private void DV_TolleranzaSuPortata_buttonClick(){
        DV_TolleranzaSuPortata.addStyleName("evidenziato");
        DV_TolleranzaSuPortata.removeStyleName("schiarito");
        DV_TolleranzaSuPressione.removeStyleName("evidenziato");
        DV_TolleranzaSuPressione.addStyleName("schiarito");
        pannelloTolleranza pannelloTolleranza = new pannelloTolleranza(false);
        pannelloTolleranza.setVariabiliGlobali(vGlobali);
        MessageBox popup = MessageBox.create();
        popup.withCaption(traduci.TraduciStringa("Tolleranza su Portata"));
        popup.withMessage(pannelloTolleranza);
        popup.withAbortButton();
        popup.withOkButton(() ->{
            searchView.reset();
            pannelloTolleranza.confermaValori();
            DV_TolleranzaSuPortata.setCaption(MessageFormat.format("{0} -{1}% +{2}%", 
                traduci.TraduciStringa("Tolleranza"),
                vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO * 100,
                vGlobali.ParametriVari.pannelloRicercaTolleranzaSelezionePIU * 100));
            vGlobali.SelezioneDati.tolleranzaPortataFlag = false;
        });
        popup.open();
    }
    private void DV_TolleranzaSuPressione_buttonClick(){
        DV_TolleranzaSuPressione.addStyleName("evidenziato");
        DV_TolleranzaSuPressione.removeStyleName("schiarito");
        DV_TolleranzaSuPortata.removeStyleName("evidenziato");
        DV_TolleranzaSuPortata.addStyleName("schiarito");
        pannelloTolleranza pannelloTolleranza = new pannelloTolleranza(true);
        pannelloTolleranza.setVariabiliGlobali(vGlobali);
        MessageBox popup = MessageBox.create();
        popup.withCaption(traduci.TraduciStringa("Tolleranza su Pressione"));
        popup.withMessage(pannelloTolleranza);
        popup.withAbortButton();
        popup.withOkButton(() ->{
            searchView.reset();
            pannelloTolleranza.confermaValori();
            DV_TolleranzaSuPressione.setCaption(MessageFormat.format("{0} -{1}% +{2}%", 
                traduci.TraduciStringa("Tolleranza"),
                vGlobali.ParametriVari.pannelloRicercaTolleranzaPressioneMeno * 100,
                vGlobali.ParametriVari.pannelloRicercaTolleranzaPressionePiu * 100));
            vGlobali.SelezioneDati.tolleranzaPortataFlag = true;
        
        });
        popup.open();
    }
    private void DV_InfoEsecuzione_buttonClick() {
        pannelloEsecuzioni pannelloEsecuzione = new pannelloEsecuzioni();
        pannelloEsecuzione.setWidth(40, Unit.PERCENTAGE);
        pannelloEsecuzione.SetVariabiliGlobali(vGlobali);
        oggettoSelect AssCen = (oggettoSelect) DV_AssialiCentrifughi.getValue();
        oggettoSelect TraDir = (oggettoSelect) DV_TrasmissioneDiretti.getValue();
        pannelloEsecuzione.init(AssCen.nome, TraDir.nome);
        pannelloEsecuzione.setEsecuzioneIniziale(DV_EsecuzioneCombo.getValue().toString());
        msgBox = MessageBox.createInfo();
        msgBox.withOkButton(() -> {
            modificabile = false;
            searchView.reset();
            String esecuzionescelta = pannelloEsecuzione.aggiornaValori();
            DV_EsecuzioneCombo.setValue(esecuzionescelta);
            modificabile = true;
	});
        msgBox.withCloseButton();
        msgBox.withMessage(pannelloEsecuzione);
        msgBox.open();
    }
    private void EN_SelezioneAvanzata_buttonClick() {
        PannelloSerie pannelloSerie = new PannelloSerie();
        pannelloSerie.setVariabiliGlobali(vGlobali);
        boolean trasmissione = ((oggettoSelect) DV_TrasmissioneDiretti.getValue()).theSame("Trasmissione");
        boolean assiali = ((oggettoSelect) DV_AssialiCentrifughi.getValue()).theSame("Assiali");
        pannelloSerie.init(trasmissione, assiali);
        pannelloSerie.setWidth(60, Unit.PERCENTAGE);
        msgBox = MessageBox.create();
        msgBox.withOkButton(() -> {
            modificabile = false;
            searchView.reset();
            risultatoFiltro = pannelloSerie.aggiornaValori();
            aggiornaFiltro(risultatoFiltro);
            modificabile = true;
	});
        msgBox.withMessage(pannelloSerie);
        msgBox.open();
}
    private void EN_Reset_buttonClick() {
        vGlobali.setUMFromDefaultIndex();
        vGlobali.CARicerca = (CondizioniAmbientali) vGlobali.CACatalogo.clone();
        vGlobali.CA020Ricerca = (CondizioniAmbientali) vGlobali.CA020Catalogo.clone();
        vGlobali.SelezioneDati.PortataRichiestam3h = 0.;
        vGlobali.SelezioneDati.PressioneRichiestaPa = 0.;
        DV_H.setValue(0);
        DV_Qv.setValue(0);
        DV_Nq.setValue(0);
        vGlobali.SelezioneDati.RendMinimo = 0;
        vGlobali.CorrettorePotenzaMotoreCorrente = vGlobali.CorrettorePotenzaMotoreDaConfig;
        CA_ElencoFluidi.setValue(Secca);
        DV_AssialiCentrifughi.setValue(Centrifughi);
        DV_TrasmissioneDiretti.setValue(Diretto);
        setDefaultValues();
	searchView.reset();
	searchView.init();
    }
    
    private void EN_Ricerca_buttonClick() {
        vGlobali.searchIsCentrifughi = (((oggettoSelect)(DV_AssialiCentrifughi.getValue())).theSame("Centrifughi"));
        vGlobali.searchIsDiretto = (((oggettoSelect)(DV_TrasmissioneDiretti.getValue())).theSame("Ventilatori diretti"));
        if (!vGlobali.traceUser.isActionEnabled(vGlobali.traceUser.AzioneSearch)) {
            msgBox = MessageBox.createWarning();
            msgBox.withMessage(traduci.TraduciStringa("Numero Masssimo Ricerche Disponibili Raggiunto"));
            msgBox.withOkButton();
            msgBox.open();
            return;
        }
        if (serieDaRicaricare) {
            serieDaRicaricare = false;
            //ricarica serie da pannello
        }
        Window loader = PopupWindow.For(new ProgressView(traduci)).closable(false).draggable(false).resizable(false).modal(true).show();
        UI.getCurrent().push();
        //logger.log("Prova");
        searchView.reset();
        getDati();
        
        vGlobali.ElencoVentilatoriFromRicerca.clear();
        vGlobali.eseguiRicerca();
        searchView.buildTableRisultati();
        loader.close();
        if (vGlobali.ElencoVentilatoriFromRicerca.size() < 1) {
            msgBox = MessageBox.createInfo();
            msgBox.withCaption(traduci.TraduciStringa("Ricerca"));
            msgBox.withMessage(traduci.TraduciStringa("Nessun risultato trovato"));
            msgBox.withOkButton();
            msgBox.open();
        }
    }

    public void showPopupMZ(String message) {
        final MessageBox msgBox = MessageBox.createInfo();
        msgBox.withCaption("Warning: " + message);
        msgBox.withMessage("Please ask MZ for this special request.");
        msgBox.withOkButton();
        msgBox.open();
    }
    private void aggiornaFiltro(oggettoInfoRicerca risultatoFiltro) {
        modificabile = false;
        String TipoTrasmissione = risultatoFiltro.TipoTrasmissione;
        String TipoVentilatore = risultatoFiltro.TipoVentilatore;
        if (TipoTrasmissione.equals("Direttamente Accoppiato"))
            DV_TrasmissioneDiretti.setValue(Diretto);
        else
            DV_TrasmissioneDiretti.setValue(Trasmissione);
        if (TipoVentilatore.equals("Assiale")){
            DV_AssialiCentrifughi.setMultiSelect(false);
            DV_AssialiCentrifughi.setValue(Assiali);
        }
        else if (TipoVentilatore.equals("Centrifugo")) {
            DV_AssialiCentrifughi.setMultiSelect(false);
            DV_AssialiCentrifughi.setValue(Centrifughi);
        }
        vGlobali.CorrettorePotenzaMotoreCorrente = risultatoFiltro.CorrettorePotenzaMotore;//this.l_VG.CorrettorePotenzaMotoreCorrente = this.textFieldCorrettorePotMotore.getIntegerValue();
        vGlobali.SelezioneDati.DiametroMassimoGirante = risultatoFiltro.DiametroMassimoGirante; //this.l_VG.SelezioneDati.DiametroMassimoGirante = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldDiametroGirante.getIntegerValue(), this.l_VG.UMDiametroCorrente);
        vGlobali.SelezioneDati.giriMassimiGirante = risultatoFiltro.GiriMassimiGirante;
        modificabile = true;
    }
}
