package com.cit.sellfan.ui.pannelli;

import cit.sellfan.classi.Motore;
import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.PreventiviView;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import de.steinwedel.messagebox.MessageBox;


public class pannelloMotoreCustom extends Panel {
    private VariabiliGlobali l_VG;
    private Motore MotoreDiPartenza, MotoreCustom; 
    private final String codice;
    public pannelloMotoreCustom(String _codice) {
        super();
        codice = _codice;
        this.initUI();
    }

    public Motore getNuovoMotore() {
        MotoreCustom = (Motore)MotoreDiPartenza.clone();
        MotoreCustom.Codice = "Custom";
        MotoreCustom.CodiceCliente = tfCodice.getValue();
        MotoreCustom.PotkW = Double.parseDouble(tfKw.getValue());
        MotoreCustom.Poli = Integer.parseInt(cbPoli.getValue().toString());
        MotoreCustom.Rpm = Integer.parseInt(tfRpm.getValue());
        MotoreCustom.Freq = Integer.parseInt(cbFreq.getValue().toString());
        MotoreCustom.Descrizione = tfDescrizione.getValue();
        MotoreCustom.Prezzo = Double.parseDouble(tfPrezzo.getValue());
        return MotoreCustom;
    }
    
    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        if (variabiliGlobali.VentilatoreCurrent.selezioneCorrente.MotoreInstallato == null || !variabiliGlobali.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Codice.equals("Custom"))
        {
            MotoreDiPartenza = variabiliGlobali.utilityCliente.getMotore(codice);
            MotoreDiPartenza.Prezzo = variabiliGlobali.utilityCliente.getPrezzoMotore(codice, variabiliGlobali.VentilatoreCurrent.selezioneCorrente.Esecuzione);
        } else if (variabiliGlobali.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Codice.equals("Custom"))
            MotoreDiPartenza = variabiliGlobali.VentilatoreCurrent.selezioneCorrente.MotoreInstallato;
    }
	
    public void Nazionalizza() {
        labelTitolo.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Impostare motore speciale") + "</html>");
        tfCodice.setCaption("Codice");
        tfKw.setCaption("Kw");
        cbPoli.setCaption("Poli");
        tfRpm.setCaption("Rpm");
        cbFreq.setCaption("Frequenza");
        tfDescrizione.setCaption("Descrizione");
        tfPrezzo.setCaption("Prezzo");
    }
	
    public void initPannello() {
        Nazionalizza();
        String[] codiceSplit = MotoreDiPartenza.CodiceCliente.split("/");
        tfCodice.setValue(codiceSplit[0] + "/custom");
        tfKw.setValue(MotoreDiPartenza.PotkW);
        cbPoli.addItems(2,4,6,8);
        cbPoli.setValue(MotoreDiPartenza.Poli);
        cbPoli.setEnabled(false);
        tfRpm.setValue(MotoreDiPartenza.Rpm);
        tfRpm.setEnabled(false);
        cbFreq.addItems(50,60);
        cbFreq.setValue(MotoreDiPartenza.Freq);
        cbFreq.setEnabled(false);
        tfDescrizione.setValue(MotoreDiPartenza.Descrizione);
        tfDescrizione.setMaxLength(255);
        tfPrezzo.setValue(MotoreDiPartenza.Prezzo);
	}

    private void initUI() {
        
        lyPrincipale = new XdevHorizontalLayout();
        lyBottoni = new XdevHorizontalLayout();
        verticalLayout = new XdevVerticalLayout();
        tfCodice = new XdevTextField();
        tfKw = new TextFieldDouble();
        cbPoli = new XdevComboBox();
        tfRpm = new TextFieldInteger();
        cbFreq = new XdevComboBox();
        tfDescrizione = new XdevTextField();
        tfPrezzo = new TextFieldDouble();
        labelTitolo = new XdevLabel("Titolo", ContentMode.HTML);
        tfCodice.setWidth("100%");
        tfKw.setWidth("50px");
        cbPoli.setWidth("50px");
        tfRpm.setWidth("80px");
        cbFreq.setWidth("80px");
        tfDescrizione.setWidth("300px");
        tfPrezzo.setWidth("80px");
        lyPrincipale.addComponents(tfCodice, tfKw, cbPoli, tfRpm, cbFreq, tfDescrizione, tfPrezzo);
        verticalLayout.setSizeFull();
        verticalLayout.addComponent(labelTitolo, Alignment.MIDDLE_CENTER);
        verticalLayout.addComponent(lyPrincipale);
        verticalLayout.addComponent(lyBottoni);
        this.setContent(this.verticalLayout);
        this.setSizeFull();
    }
    
    private XdevLabel labelTitolo;
    private XdevComboBox<?> cbFreq, cbPoli;
    private XdevVerticalLayout verticalLayout;
    private XdevHorizontalLayout lyPrincipale, lyBottoni;
    private TextFieldInteger tfRpm;
    private XdevTextField tfCodice, tfDescrizione;
    private TextFieldDouble tfKw, tfPrezzo;
    
}
