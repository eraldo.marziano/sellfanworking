package com.cit.sellfan.ui.view.mobile;

import java.util.Locale;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.Costanti;
import cit.sellfan.classi.Serie;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.utility.NumeriRomani;

public class MobileCatalogoView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
	private final Container containerSerie = new IndexedContainer();
	private final Container containerVentilatori = new IndexedContainer();
	private String currentVentilatoreID = null;
	@SuppressWarnings("unused")
	private String currentSerieCode = null;

	/**
	 * 
	 */
	public MobileCatalogoView() {
		super();
		this.initUI();
		this.tableSerie.setEnabled(false);
		this.tableVentilatori.setEnabled(false);
		this.containerSerie.removeAllItems();
		this.containerSerie.addContainerProperty("Code", String.class, "");
		this.containerSerie.addContainerProperty("Description", String.class, "");
		this.tableSerie.setContainerDataSource(this.containerSerie);
		this.containerVentilatori.removeAllItems();
		this.containerVentilatori.addContainerProperty("Check", XdevImage.class, "");
		this.containerVentilatori.addContainerProperty("Code", String.class, "");
		this.containerVentilatori.addContainerProperty("Motor", String.class, "");
		this.containerVentilatori.addContainerProperty("Power", Double.class, 0.0);
		this.containerVentilatori.addContainerProperty("Rpm", Double.class, 0.0);
		this.containerVentilatori.addContainerProperty("Impeller", Double.class, 0.0);
		this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
		this.tableSerie.setEnabled(true);
		this.tableVentilatori.setEnabled(true);
	}

	private void setConverterForTable() {
		final myStringToDoubleConverter converter = new myStringToDoubleConverter();
		this.tableVentilatori.setConverter("Power", converter);
		this.tableVentilatori.setConverter("Rpm", converter);
		this.tableVentilatori.setConverter("Impeller", converter);
	}
	
	private class myStringToDoubleConverter extends StringToDoubleConverter {
		public myStringToDoubleConverter() {
			super();
		}
		
		@Override
		public java.text.NumberFormat getFormat(final Locale locale) {
			return MobileCatalogoView.this.l_VG.fmtNdTable;
		}
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoMobileView[this.l_VG.CatalogoViewIndex] = this;
		this.checkBoxDA.setValue(!this.l_VG.trasmissioneFlag);
		this.checkBoxTR.setValue(this.l_VG.trasmissioneFlag);
	    try {
	    	buildTableSerie();
	    } catch (final Exception e) {
	    	//Notification.show(e.toString());
	    }
		setButtonVisible(false);
		setConverterForTable();
	}
	
	public void resetView() {
	    try {
	    	buildTableSerie();
	    } catch (final Exception e) {
	    	//Notification.show(e.toString());
	    }
	}
	
	public void Nazionalizza() {
		//buttonAddToOffer.setVisible(l_VG.currentLivelloUtente == Costanti.utenteDefault);
		this.checkBoxTR.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Trasmissione"));
		this.checkBoxDA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatori diretti"));
		this.tableSerie.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableSerie.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableVentilatori.setColumnHeader("Check", this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo"));
		this.tableVentilatori.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableVentilatori.setColumnHeader("Motor", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.tableVentilatori.setColumnHeader("Power", this.l_VG.utilityTraduzioni.TraduciStringa("P. inst.") + " [kW]");
		this.tableVentilatori.setColumnHeader("Rpm", this.l_VG.utilityTraduzioni.TraduciStringa("N. giri") + " [Rpm]");
		this.tableVentilatori.setColumnHeader("Impeller", this.l_VG.utilityTraduzioni.TraduciStringa("D. girante") + " [mm]");
		if (!this.l_VG.utilityCliente.isTabVisible("Offerte")) {
			this.tableVentilatori.setColumnWidth("Check", 0);
		}
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		return this.isPannelloInizializzato;
	}
	
	@SuppressWarnings("unused")
	public void buildTableSerie() {
		if (!this.l_VG.utilityCliente.isTabVisible("Catalogo")) {
			return;
		}
		this.tableSerie.setEnabled(false);
		this.currentVentilatoreID = null;
		this.currentSerieCode = null;
		final Item item;
		this.l_VG.ElencoVentilatoriFromCatalogo.clear();
		buildTableVentilatori(null);
//		containerSerie.removeAllItems();
		try {
			this.tableSerie.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
		}
		for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
			final Object obj[] = new Object[2];
			final Serie l_s = this.l_VG.ElencoSerie.get(i);
			if (!this.l_VG.ElencoSerieDisponibili.get(i)) {
				continue;
			}
			final String itemID = l_s.Serie;
			if (this.l_VG.trasmissioneFlag) {
				if (l_s.Trasmissione == 1) {
					obj[0] = l_s.SerieTrans;
					obj[1] = this.l_VG.utilityTraduzioni.TraduciStringa(l_s.DescrizioneTrans);
					this.tableSerie.addItem(obj, itemID);
				}
			} else {
				if (l_s.DirettamenteAccoppiato == 1) {
					obj[0] = l_s.SerieTrans;
					obj[1] = this.l_VG.utilityTraduzioni.TraduciStringa(l_s.DescrizioneTrans);
					this.tableSerie.addItem(obj, itemID);
				}
			}
		}
		this.isPannelloInizializzato = true;
		this.tableSerie.setEnabled(true);
	}
	
	@SuppressWarnings("unchecked")
	public void aggiornaTableVentilatori() {
		try {
			if (this.currentVentilatoreID != null) {
				final Item selectedItem = this.containerVentilatori.getItem(this.currentVentilatoreID);
				selectedItem.getItemProperty("Code").setValue(this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent));
				selectedItem.getItemProperty("Motor").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
				selectedItem.getItemProperty("Power").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW);
				selectedItem.getItemProperty("Rpm").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
				selectedItem.getItemProperty("Impeller").setValue(this.l_VG.VentilatoreCurrent.DiametroGirante * 1000.);
			}
		} catch (final Exception e) {
			//l_VG.MainView.setMemo(e.toString());
		}
	}

    private String getSerieFromTranscodifica(final String serieTrans) {
        for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
            if (serieTrans.equals(this.l_VG.ElencoSerie.get(i).SerieTrans)) {
				return this.l_VG.ElencoSerie.get(i).Serie;
			}
        }
        return null;
    }

    private void buildTableVentilatori(final String serie) {
    	this.tableVentilatori.setEnabled(false);
		this.currentSerieCode = serie;
		this.currentVentilatoreID = null;
		@SuppressWarnings("unused")
		final Item item;
		try {
			this.tableVentilatori.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
		}
		//this.l_VG.ElencoVentilatoriFromCatalogo.clear();
    	this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
		setButtonVisible(false);
		if (serie == null) {
			return;
		}
		this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
		try {
			//this.l_VG.dbTecnico.loadCatalogo(this.l_VG.ElencoVentilatoriFromCatalogo, serie, this.l_VG.nTemperatureLimite + 1, this.l_VG.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, this.l_VG.trasmissioneFlag, this.l_VG.CACatalogo, this.l_VG.CA020Catalogo, this.l_VG.idTranscodificaIndex);
	        for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromCatalogo.size() ; i++) {
				final String itemID = Integer.toString(i);
				final Object obj[] = new Object[6];
				final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromCatalogo.get(i);
				if (l_v.Classe.equals("-") && l_v.elencoRpmClassi.size() > 0) {
					l_v.Classe = NumeriRomani.daAraboARomano(Integer.toString(l_v.elencoClassi.get(0)));
				}
				if (this.l_VG.debugFlag) {
					Notification.show(l_v.Classe+"\t"+Integer.toString(l_v.elencoClassi.get(0))+"\t"+Integer.toString(l_v.elencoRpmClassi.get(0))+"\t"+Integer.toString(l_v.elencoRpmClassi.size()));
				}
				final XdevImage chk = new XdevImage();
				boolean onPreventivo = false;
				if (this.l_VG.ElencoVentilatoriFromPreventivo != null) {
					for (int j=0 ; j<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; j++) {
						final Ventilatore l_vv = this.l_VG.ElencoVentilatoriFromPreventivo.get(j);
						if (l_vv.Serie.equals(l_v.Serie) && l_vv.Modello.equals(l_v.Modello) && l_vv.Versione.equals(l_v.Versione)) {
							onPreventivo = true;
							break;
						}
					}
				}
				if (onPreventivo) {
					chk.setSource(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
				}
				chk.setWidth(20, Unit.PIXELS);
				chk.setHeight(20, Unit.PIXELS);
				obj[0] = chk;
				obj[1] = this.l_VG.buildModelloCompleto(l_v);
				obj[2] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
				obj[3] = l_v.selezioneCorrente.MotoreInstallato.PotkW;
				obj[4] = l_v.selezioneCorrente.NumeroGiri;
				obj[5] = l_v.DiametroGirante * 1000.;
				this.tableVentilatori.addItem(obj, itemID);
			}
		} catch (final Exception e) {
			Notification.show(e.toString());
		}
		this.tableVentilatori.setEnabled(true);
	}
	
	private void setButtonVisible(final boolean value) {
		this.buttonAddToOffer.setVisible(this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
	}
	
	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTR}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTR_valueChange(final Property.ValueChangeEvent event) {
		this.checkBoxDA.setValue(!this.checkBoxTR.getValue());
		this.l_VG.trasmissioneFlag = this.checkBoxTR.getValue();
		buildTableSerie();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxDA}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxDA_valueChange(final Property.ValueChangeEvent event) {
		this.checkBoxTR.setValue(!this.checkBoxDA.getValue());
		this.l_VG.trasmissioneFlag = this.checkBoxTR.getValue();
		buildTableSerie();
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableVentilatori}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableVentilatori_valueChange(final Property.ValueChangeEvent event) {
		try {
			this.currentVentilatoreID = event.getProperty().getValue().toString();
			this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(event.getProperty().getValue().toString());
			setButtonVisible(true);
		} catch (final Exception e) {
			this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAddToOffer}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAddToOffer_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToPreventivo();
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableSerie}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableSerie_valueChange(final Property.ValueChangeEvent event) {
		if (!this.tableSerie.isEnabled()) {
			return;
		}
		try {
			final Item item = this.containerSerie.getItem(event.getProperty().getValue().toString());
			if (this.l_VG.debugFlag) {
				Notification.show(item.getItemProperty("Code").getValue().toString());
			}
			final String serie = getSerieFromSerieTrans(item.getItemProperty("Code").getValue().toString());
			int freq = -1;
			if (!this.l_VG.catalogoCondizioniFrequenza.equals("")) {
				freq = Integer.parseInt(this.l_VG.catalogoCondizioniFrequenza.split(" ")[0]);
			}
			this.l_VG.dbTecnico.loadCatalogo(this.l_VG.ElencoVentilatoriFromCatalogo, serie, this.l_VG.nTemperatureLimite + 1, this.l_VG.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, this.l_VG.trasmissioneFlag, this.l_VG.CACatalogo, this.l_VG.CA020Catalogo, this.l_VG.idTranscodificaIndex, freq);
	        buildTableVentilatori(getSerieFromTranscodifica(item.getItemProperty("Code").getValue().toString()));
	    	this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
		} catch (final Exception e) {
	        buildTableVentilatori(null);
	    	this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
		}
	}

	private String getSerieFromSerieTrans(final String serieTrans) {
		for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
			final Serie l_s = this.l_VG.ElencoSerie.get(i);
			if (l_s.Serie.equals(serieTrans)) {
				return l_s.Serie;
			}
			if (l_s.SerieTrans.equals(serieTrans)) {
				return l_s.Serie;
			}
		}
		return "";
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.gridLayout = new XdevGridLayout();
		this.checkBoxTR = new XdevCheckBox();
		this.checkBoxDA = new XdevCheckBox();
		this.tableSerie = new XdevTable<>();
		this.tableVentilatori = new XdevTable<>();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.buttonAddToOffer = new XdevButton();
	
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.gridLayout.setMargin(new MarginInfo(false));
		this.checkBoxTR.setCaption("Direct driven fans");
		this.checkBoxDA.setCaption("Belt driven");
		this.tableSerie.setStyleName("mystriped");
		this.tableVentilatori.setStyleName("mystriped");
		this.horizontalLayout.setMargin(new MarginInfo(false, true, false, true));
		this.buttonAddToOffer.setCaption("Add to Offer");
		this.buttonAddToOffer.setStyleName("big giallo");
	
		this.gridLayout.setColumns(2);
		this.gridLayout.setRows(2);
		this.checkBoxTR.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTR.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxTR, 0, 0);
		this.checkBoxDA.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxDA.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxDA, 1, 0);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 1, 1, 1);
		this.gridLayout.setRowExpandRatio(1, 1.0F);
		this.buttonAddToOffer.setWidth(100, Unit.PERCENTAGE);
		this.buttonAddToOffer.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.buttonAddToOffer);
		this.horizontalLayout.setComponentAlignment(this.buttonAddToOffer, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.buttonAddToOffer, 10.0F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.tableSerie.setSizeFull();
		this.verticalLayout.addComponent(this.tableSerie);
		this.verticalLayout.setExpandRatio(this.tableSerie, 10.0F);
		this.tableVentilatori.setSizeFull();
		this.verticalLayout.addComponent(this.tableVentilatori);
		this.verticalLayout.setExpandRatio(this.tableVentilatori, 20.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.checkBoxTR.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileCatalogoView.this.checkBoxTR_valueChange(event);
			}
		});
		this.checkBoxDA.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileCatalogoView.this.checkBoxDA_valueChange(event);
			}
		});
		this.tableSerie.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileCatalogoView.this.tableSerie_valueChange(event);
			}
		});
		this.tableVentilatori.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileCatalogoView.this.tableVentilatori_valueChange(event);
			}
		});
		this.buttonAddToOffer.addClickListener(event -> this.buttonAddToOffer_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonAddToOffer;
	private XdevVerticalLayout verticalLayout;
	private XdevCheckBox checkBoxTR, checkBoxDA;
	private XdevGridLayout gridLayout;
	private XdevTable<CustomComponent> tableSerie, tableVentilatori;
	private XdevHorizontalLayout horizontalLayout; // </generated-code>


}