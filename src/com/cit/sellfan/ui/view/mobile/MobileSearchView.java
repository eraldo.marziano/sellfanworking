package com.cit.sellfan.ui.view.mobile;

import java.util.Iterator;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.view.ProgressView;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.sellfan.Costanti;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.utility.NumeriRomani;
import de.steinwedel.messagebox.MessageBox;

public class MobileSearchView extends XdevView {
	private VariabiliGlobali l_VG;
	private final Container containerRisultati = new IndexedContainer();
	private String currentItemID = null;
	private MessageBox msgBox;
	private boolean editEnabled;

	/**
	 * 
	 */
	public MobileSearchView() {
		super();
		this.initUI();
		this.containerRisultati.removeAllItems();
		this.containerRisultati.addContainerProperty("Prezzo", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("Modello", String.class, "");
		this.containerRisultati.addContainerProperty("Motore", String.class, "");
		this.containerRisultati.addContainerProperty("Q", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("Pt", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("Ps", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("W", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("rpm", Integer.class, 0);
		this.containerRisultati.addContainerProperty("eta", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("LW", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("L", Double.class, 0.0);
		this.tableRisultati.setContainerDataSource(this.containerRisultati);
		this.textAltezzaSLM.setConSegno(true);
		this.textFieldTemperatura.setConSegno(true);
		abilitaBottoni(false);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.editEnabled = false;
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoMobileView[this.l_VG.SearchViewIndex] = this;
		this.comboBoxAltezzaSLMUM.setEnabled(false);
		for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.size() ; i++) {
			this.comboBoxAltezzaSLMUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(i).simbolo));
		}
		this.comboBoxAltezzaSLMUM.setEnabled(true);
		this.comboBoxTemperaturaUM.setEnabled(false);
		for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.size() ; i++) {
			this.comboBoxTemperaturaUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.get(i).simbolo));
		}
		this.comboBoxTemperaturaUM.setEnabled(true);
		this.comboBoxPortataUM.setEnabled(false);
		for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.size() ; i++) {
			this.comboBoxPortataUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.get(i).simbolo));
		}
		this.comboBoxPortataUM.setEnabled(true);
		this.comboBoxPressioneUM.setEnabled(false);
		for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.size() ; i++) {
			this.comboBoxPressioneUM.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.get(i).simbolo));
		}
		this.comboBoxPressioneUM.setEnabled(true);
		init();
	}
	
	public void resetView() {
		try {
			init();
		} catch (final Exception e) {
			//Notification.show(e.toString());
		}
	}
	
	public void init() {
		if (!this.l_VG.utilityCliente.isTabVisible("Ricerca")) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxDA.setEnabled(false);
		this.checkBoxTR.setEnabled(false);
		this.checkBoxOutlet.setEnabled(false);
		this.checkBoxInlet.setEnabled(false);
		this.checkBoxAssiali.setEnabled(false);
		this.checkBoxCentrifughi.setEnabled(false);
		this.checkBoxTotale.setEnabled(false);
		this.checkBoxStatica.setEnabled(false);
		this.checkBoxDA.setValue(!this.l_VG.trasmissioneFlag);
		this.checkBoxTR.setValue(this.l_VG.trasmissioneFlag);
		this.checkBoxAssiali.setValue(false);
		this.checkBoxCentrifughi.setValue(true);
		this.labelPortata.setValue(StringheUNI.PORTATA);
		this.labelPressione.setValue(StringheUNI.PRESSIONETOTALE);
		this.checkBoxDA.setEnabled(true);
		this.checkBoxTR.setEnabled(true);
		this.checkBoxOutlet.setEnabled(true);
		this.checkBoxInlet.setEnabled(true);
		this.comboBoxAltezzaSLMUM.setEnabled(false);
		this.comboBoxTemperaturaUM.setEnabled(false);
		this.comboBoxAltezzaSLMUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMAltezzaCorrente.simbolo));
		this.comboBoxTemperaturaUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMTemperaturaCorrente.simbolo));
		this.comboBoxAltezzaSLMUM.setEnabled(true);
		this.comboBoxTemperaturaUM.setEnabled(true);
		this.comboBoxPressioneUM.setEnabled(true);
		this.comboBoxPortataUM.setEnabled(true);
		
		if (this.l_VG.ricercaAspirazione) {
			this.checkBoxOutlet.setValue(false);
			this.checkBoxInlet.setValue(true);
		} else {
			this.checkBoxOutlet.setValue(true);
			this.checkBoxInlet.setValue(false);
		}
		if (this.l_VG.ricercaTotale) {
			this.checkBoxTotale.setValue(true);
			this.checkBoxStatica.setValue(false);
			this.labelPressione.setValue(StringheUNI.PRESSIONETOTALE);
			} else {
				this.checkBoxTotale.setValue(false);
				this.checkBoxStatica.setValue(true);
				this.labelPressione.setValue(StringheUNI.PRESSIONESTATICA);
			}
		
		this.textFieldPortata.setMaximumFractionDigits(this.l_VG.UMPortataCorrente.nDecimaliStampa);
		if (this.l_VG.SelezioneDati.PortataRichiestam3h > 0.) {
			this.textFieldPortata.setValue(this.l_VG.SelezioneDati.PortataRichiestam3h);
		} else {
			this.textFieldPortata.setValue("");
		}
		this.textFieldPressione.setMaximumFractionDigits(this.l_VG.UMPressioneCorrente.nDecimaliStampa);
		if (this.l_VG.SelezioneDati.PressioneRichiestaPa > 0.) {
			this.textFieldPressione.setValue(this.l_VG.SelezioneDati.PressioneRichiestaPa);
		} else {
			this.textFieldPressione.setValue("");
		}
		
		this.textAltezzaSLM.setMaximumFractionDigits(this.l_VG.UMAltezzaCorrente.nDecimaliStampa);
		this.textAltezzaSLM.setValue(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.CARicerca.altezza, this.l_VG.UMAltezzaCorrente));
		this.textFieldTemperatura.setMaximumFractionDigits(this.l_VG.UMTemperaturaCorrente.nDecimaliStampa);
		this.textFieldTemperatura.setValue(this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, this.l_VG.CARicerca.temperatura));
		this.comboBoxPressioneUM.setEnabled(false);
		this.comboBoxPortataUM.setEnabled(false);
		this.comboBoxPressioneUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMPressioneCorrente.simbolo));
		this.comboBoxPortataUM.setValue(this.l_VG.utility.filtraHTMLTags(this.l_VG.UMPortataCorrente.simbolo));
		this.comboBoxPressioneUM.setEnabled(true);
		this.comboBoxPortataUM.setEnabled(true);
		this.checkBoxTotale.setEnabled(true);
		this.checkBoxStatica.setEnabled(true);
		this.editEnabled = true;
	}
	
	public void Nazionalizza() {
		this.labelAlezzaSLM.setValue("<html><b>h<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("S.L.M.") + "</sub></html>");
		this.labelTemperatura.setValue("<html><b>T<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("Fluido") + "</sub>:</html>");
		this.buttonAddToOffer.setVisible(this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
		this.checkBoxAssiali.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Assiali"));
		this.checkBoxCentrifughi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Centrifughi"));
		this.checkBoxTR.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Trasmissione"));
		this.checkBoxDA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatori diretti"));
		this.checkBoxTotale.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("totale"));
		this.checkBoxStatica.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("statica"));
		this.checkBoxInlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("aspirazione"));
		this.checkBoxOutlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("mandata"));
		this.tableRisultati.setColumnHeader("Prezzo", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo"));
		this.tableRisultati.setColumnHeader("Modello", this.l_VG.utilityTraduzioni.TraduciStringa("Modello"));
		this.tableRisultati.setColumnHeader("Motore", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.tableRisultati.setColumnHeader("Q", StringheUNI.PORTATA);
		this.tableRisultati.setColumnHeader("Pt", StringheUNI.PRESSIONETOTALE);
		this.tableRisultati.setColumnHeader("Ps", StringheUNI.PRESSIONESTATICA);
		this.tableRisultati.setColumnHeader("W", StringheUNI.POTENZAASSE);
		this.tableRisultati.setColumnHeader("rpm", StringheUNI.RPM);
		this.tableRisultati.setColumnHeader("eta", StringheUNI.RENDIMENTO);
		this.tableRisultati.setColumnHeader("LW", StringheUNI.POTENZASONORA);
		this.tableRisultati.setColumnHeader("L", StringheUNI.PRESSIONESONORA);
		this.buttonAddToOffer.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
	}
	
	public void reset() {
		this.currentItemID = null;
		abilitaBottoni(false);
		try {
			this.tableRisultati.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
			
		}
		this.l_VG.MainView.setVentilatoreCorrenteFromRicerca(null);
	}
	
	private void abilitaBottoni(final Boolean value) {
		//buttonSelRistretta.setEnabled(value);
		this.buttonAddToOffer.setEnabled(value);
	}
	
	private void initSelezioneDati() {
		this.l_VG.SelezioneDati.CategorieSerieIndex = 0;
		this.l_VG.SelezioneDati.MaterialeIndex = 0;
		this.l_VG.SelezioneDati.aperturaSerrandaDapo = 100.;
		this.l_VG.SelezioneDati.UMPortata = (UnitaMisura)this.l_VG.UMPortataCorrente.clone();
		this.l_VG.SelezioneDati.UMPressione = (UnitaMisura)this.l_VG.UMPressioneCorrente.clone();
		this.l_VG.SelezioneDati.UMPotenza = (UnitaMisura)this.l_VG.UMPotenzaCorrente.clone();
		this.l_VG.SelezioneDati.UMAltezza = (UnitaMisura)this.l_VG.UMAltezzaCorrente.clone();
		this.l_VG.SelezioneDati.UMTemperatura = (UnitaMisura)this.l_VG.UMTemperaturaCorrente.clone();
		if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
			final UtilityCliente uc04 = this.l_VG.utilityCliente;
	        uc04.AssialiSelezionatiFlag = this.checkBoxAssiali.getValue();
	        uc04.CentrifughiSelezionatiFlag = this.checkBoxCentrifughi.getValue();
		}
        this.l_VG.SelezioneDati.Trasmissione = this.checkBoxTR.getValue();
		this.l_VG.SelezioneDati.pressioneStaticaFlag = this.checkBoxStatica.getValue();
		this.l_VG.SelezioneDati.PressioneMandataFlag = this.checkBoxOutlet.getValue();
        
        this.l_VG.SelezioneDati.PotSonoraFlag = true;
        //l_VG.SelezioneDati.PresSonoraMax = l_VG.SelezioneDati.PotSonoraMax;

        this.l_VG.SelezioneDati.esecuzioneSelezionata = "";
		this.l_VG.SelezioneDati.tolleranzaPortataFlag = this.l_VG.pannelloRicercaTolleranzaPressioneFlag;
		this.l_VG.SelezioneDati.precisioneMeno = this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO;
		this.l_VG.SelezioneDati.precisionePiu = this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezionePIU;
		this.l_VG.CARicerca.frequenza = this.l_VG.CARicerca.frequenza;
		this.l_VG.SelezioneDati.AspirazioneSingola = true;
		this.l_VG.SelezioneDati.RendMinimo = 0;//textFieldrhoMin.getIntegerValue();
		this.l_VG.CorrettorePotenzaMotoreCorrente = this.l_VG.CorrettorePotenzaMotoreCorrente;
		//l_VG.SelezioneDati.Dapo = l_VG.SelezioneDati.Dapo;
		//l_VG.SelezioneDati.Serranda = l_VG.SelezioneDati.Serranda;
        //l_VG.SelezioneDati.DiametroMassimoGirante = l_VG.SelezioneDati.DiametroMassimoGirante;
		//l_VG.SelezioneDati.giriMassimiGirante = l_VG.SelezioneDati.giriMassimiGirante;
		//l_VG.SelezioneDati.PotSonoraMax = l_VG.SelezioneDati.PotSonoraMax;
		// prova
		final double portata = this.l_VG.utilityUnitaMisura.fromXXTom3h(this.textFieldPortata.getDoubleValue(), this.l_VG.UMPortataCorrente);
		final double pressione = this.l_VG.utilityUnitaMisura.fromXXToPa(this.textFieldPressione.getDoubleValue(), this.l_VG.UMPressioneCorrente);
		final double altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textAltezzaSLM.getDoubleValue(), this.l_VG.UMAltezzaCorrente);
		final double temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getDoubleValue());
		this.l_VG.SelezioneDati.PortataRichiestam3h = portata;
		this.l_VG.SelezioneDati.PressioneRichiestaPa = pressione;
		this.l_VG.CARicerca.altezza = altezza;
		this.l_VG.CARicerca.temperatura = temperatura;
        this.l_VG.CARicerca.rho = this.l_VG.utilityFisica.calcolaDensitaFuido(this.l_VG.CA020Ricerca.rho, this.l_VG.CARicerca.temperatura, this.l_VG.CARicerca.altezza, 0., 0);
		
		
		
		
	}

	public void buildTableRisultati() {
		abilitaBottoni(false);
		reset();
/*
		StreamFileResource streamFileResource = new StreamFileResource(l_VG.resourcesRoot + l_VG.currentUserSaaS.Ambiente + "img/accessori/" + "vuota.png");
		StreamResource source = new StreamResource(streamFileResource, "myimage" + Integer.toString(l_VG.progressivoGenerico++) + ".png");
		source.setCacheTime(100);
		image.setSource(source);
		try {
			tableRisultati.removeAllItems();//resetta anche le selection
		} catch (Exception e) {
			
			
		}
*/
		for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromRicerca.size() ; i++) {
			final String itemID = Integer.toString(i);
			final Object obj[] = new Object[11];
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromRicerca.get(i);
			if (l_v.Classe.equals("-") && l_v.elencoRpmClassi.size() > 0) {
				l_v.Classe = NumeriRomani.daAraboARomano(Integer.toString(l_v.elencoClassi.get(0)));
			}
			obj[0] = l_v.PrezzoVentilatoreForSort;
			obj[1] = this.l_VG.buildModelloCompleto(l_v);
			obj[2] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
			obj[3] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, this.l_VG.UMPortataCorrente));
			
			obj[4] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, this.l_VG.UMPressioneCorrente));
			obj[5] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneStatica, this.l_VG.UMPressioneCorrente));
			obj[6] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.Potenza, this.l_VG.UMPotenzaCorrente));
			obj[7] = (int)l_v.selezioneCorrente.NumeroGiri;
			obj[8] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.Rendimento, 2);
			final double contributoXRumore = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(l_v.NBoccheCanalizzateBase, l_v.selezioneCorrente.nBoccheCanalizzate);
			obj[9] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PotenzaSonora+contributoXRumore, this.l_VG.NDecimaliRumore);
			obj[10] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PotenzaSonora+contributoXRumore, this.l_VG.NDecimaliRumore);
			this.tableRisultati.addItem(obj, itemID);
		}
	}

	@SuppressWarnings("unchecked")
	public void aggiornaTableRisultati() {
		try {
			if (this.currentItemID != null) {
				final Item selectedItem = this.containerRisultati.getItem(this.currentItemID);
				selectedItem.getItemProperty("Prezzo").setValue(this.l_VG.VentilatoreCurrent.PrezzoVentilatoreForSort);
				selectedItem.getItemProperty("Modello").setValue(this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent));
				selectedItem.getItemProperty("Motore").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
				selectedItem.getItemProperty("Q").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromm3hToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, this.l_VG.UMPortataCorrente)));
				selectedItem.getItemProperty("Pt").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale, this.l_VG.UMPressioneCorrente)));
				selectedItem.getItemProperty("Ps").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica, this.l_VG.UMPressioneCorrente)));
				selectedItem.getItemProperty("W").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza, this.l_VG.UMPotenzaCorrente)));
				selectedItem.getItemProperty("rpm").setValue((int)this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
				selectedItem.getItemProperty("eta").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.VentilatoreCurrent.selezioneCorrente.Rendimento, 2));
				final double contributoXRumore = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(this.l_VG.VentilatoreCurrent.NBoccheCanalizzateBase, this.l_VG.VentilatoreCurrent.selezioneCorrente.nBoccheCanalizzate);
				selectedItem.getItemProperty("LW").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaSonora+contributoXRumore, this.l_VG.NDecimaliRumore));
				selectedItem.getItemProperty("L").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaSonora+contributoXRumore, this.l_VG.NDecimaliRumore));
			}
		} catch (final Exception e) {
			//l_VG.MainView.setMemo(e.toString());
		}
	}
	
	private void checkBoxTotale_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaTotale = this.checkBoxTotale.getValue();
		this.checkBoxStatica.setValue(!this.checkBoxTotale.getValue());
		this.labelPressione.setValue(StringheUNI.PRESSIONETOTALE);
		this.reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxStatica}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxStatica_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaTotale = !this.checkBoxStatica.getValue();
		this.checkBoxTotale.setValue(!this.checkBoxStatica.getValue());
		this.labelPressione.setValue(StringheUNI.PRESSIONESTATICA);
		this.reset();
		this.editEnabled = true;
	}
	
	private void checkBoxInlet_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaAspirazione = this.checkBoxInlet.getValue();
		this.checkBoxOutlet.setValue(!this.checkBoxInlet.getValue());
		reset();
		this.editEnabled = true;
	}
	
	private void checkBoxOutlet_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.l_VG.ricercaAspirazione = !this.checkBoxInlet.getValue();
		this.checkBoxInlet.setValue(!this.checkBoxOutlet.getValue());
		reset();
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxPortataUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxPortataUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxPortataUM.isEnabled()) {
			return;
		}
		if (this.comboBoxPortataUM.getValue() == null) {
			return;
		}
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxPortataUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double portata = this.l_VG.utilityUnitaMisura.fromXXTom3h(this.textFieldPortata.getDoubleValue(), this.l_VG.UMPortataCorrente);
		this.l_VG.UMPortataCorrente = this.l_VG.utilityUnitaMisura.getPortataUM(index);
		if (this.l_VG.debugFlag) {
			Notification.show(this.l_VG.UMPortataCorrente.simbolo);
		}
		this.textFieldPortata.setEnabled(false);
		this.textFieldPortata.setMaximumFractionDigits(this.l_VG.UMPortataCorrente.nDecimaliStampa);
		this.textFieldPortata.setValue(this.l_VG.utilityUnitaMisura.fromm3hToXX(portata, this.l_VG.UMPortataCorrente));
		this.textFieldPortata.setEnabled(true);
		this.l_VG.SelezioneDati.PortataRichiestam3h = portata;
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxPressioneUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxPressioneUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxPressioneUM.isEnabled()) {
			return;
		}
		if (this.comboBoxPressioneUM.getValue() == null) {
			return;
		}
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxPressioneUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double pressione = this.l_VG.utilityUnitaMisura.fromXXToPa(this.textFieldPressione.getDoubleValue(), this.l_VG.UMPressioneCorrente);
		this.l_VG.UMPressioneCorrente = this.l_VG.utilityUnitaMisura.getPressioneUM(index);
		this.textFieldPressione.setEnabled(false);
		this.textFieldPressione.setMaximumFractionDigits(this.l_VG.UMPressioneCorrente.nDecimaliStampa);
		this.textFieldPressione.setValue(this.l_VG.utilityUnitaMisura.fromPaToXX(pressione, this.l_VG.UMPressioneCorrente));
		this.textFieldPressione.setEnabled(true);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSearch}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonSearch_buttonClick(final Button.ClickEvent event) {
        final Window popup = PopupWindow.For(new ProgressView(this.l_VG.utilityTraduzioni)).closable(false).draggable(false).resizable(false).modal(true).show();
        UI.getCurrent().push();
		reset();
		initSelezioneDati();
		this.l_VG.ElencoVentilatoriFromRicerca.clear();
		this.l_VG.eseguiRicerca();
		//l_VG.MainView.setMemo(Integer.toString(l_VG.ElencoVentilatoriFromRicerca.size()));
		if (this.l_VG.debugFlag) {
			Notification.show("Trovati: "+Integer.toString(this.l_VG.ElencoVentilatoriFromRicerca.size()));
		}
		buildTableRisultati();
		popup.close();
		if (this.l_VG.ElencoVentilatoriFromRicerca.size() <= 0) {
	      	this.msgBox = MessageBox.createInfo();
	      	this.msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ricerca"));
	       	this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Nessun ventilatore selezionato"));
	       	this.msgBox.withOkButton();
	       	this.msgBox.open();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableRisultati}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableRisultati_valueChange(final Property.ValueChangeEvent event) {
		try {
			if (this.l_VG.debugFlag) {
				Notification.show(event.getProperty().getValue().toString());
			}
			this.currentItemID = event.getProperty().getValue().toString();
			this.l_VG.MainView.setVentilatoreCorrenteFromRicerca(event.getProperty().getValue().toString());
			abilitaBottoni(true);
		} catch (final Exception e) {
			abilitaBottoni(false);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAddToOffer}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAddToOffer_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToPreventivo();
	}

	
	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxAltezzaSLMUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxAltezzaSLMUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxAltezzaSLMUM.isEnabled()) {
			return;
		}
		if (this.comboBoxAltezzaSLMUM.getValue() == null) {
			return;
		}
		this.comboBoxAltezzaSLMUM.setEnabled(false);
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxAltezzaSLMUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textAltezzaSLM.getDoubleValue(), this.l_VG.UMAltezzaCorrente);
		this.l_VG.UMAltezzaCorrente = this.l_VG.utilityUnitaMisura.getLunghezzaUM(index);
		this.textAltezzaSLM.setEnabled(false);
		this.textAltezzaSLM.setMaximumFractionDigits(this.l_VG.UMAltezzaCorrente.nDecimaliStampa);
		this.textAltezzaSLM.setValue(this.l_VG.utilityUnitaMisura.frommToXX(altezza, this.l_VG.UMAltezzaCorrente));
		this.textAltezzaSLM.setEnabled(true);
		this.l_VG.CARicerca.altezza = altezza;
        this.l_VG.CARicerca.rho = this.l_VG.utilityFisica.calcolaDensitaFuido(this.l_VG.CA020Ricerca.rho, this.l_VG.CARicerca.temperatura, this.l_VG.CARicerca.altezza, 0., 0);
		this.comboBoxAltezzaSLMUM.setEnabled(true);
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxTemperaturaUM}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxTemperaturaUM_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxTemperaturaUM.isEnabled()) {
			return;
		}
		if (this.comboBoxTemperaturaUM.getValue() == null) {
			return;
		}
		this.comboBoxTemperaturaUM.setEnabled(false);
		final String newValue = event.getProperty().getValue().toString();
		final Iterator<?> item = this.comboBoxTemperaturaUM.getItemIds().iterator();
		int index = -1;
		while(item.hasNext()) {
			index++;
			if (item.next().toString().equals(newValue)) {
				break;
			}
		}
		final double temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getDoubleValue());
		this.l_VG.UMTemperaturaCorrente = this.l_VG.utilityUnitaMisura.getTemperaturaUM(index);
		this.textFieldTemperatura.setEnabled(false);
		this.textFieldTemperatura.setMaximumFractionDigits(this.l_VG.UMTemperaturaCorrente.nDecimaliStampa);
		this.textFieldTemperatura.setValue(this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, temperatura));
		this.textFieldTemperatura.setEnabled(true);
		this.l_VG.CARicerca.temperatura = temperatura;
        this.l_VG.CARicerca.rho = this.l_VG.utilityFisica.calcolaDensitaFuido(this.l_VG.CA020Ricerca.rho, this.l_VG.CARicerca.temperatura, this.l_VG.CARicerca.altezza, 0., 0);
		this.comboBoxTemperaturaUM.setEnabled(true);
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.gridLayout = new XdevGridLayout();
		this.checkBoxAssiali = new XdevCheckBox();
		this.checkBoxCentrifughi = new XdevCheckBox();
		this.checkBoxTR = new XdevCheckBox();
		this.checkBoxDA = new XdevCheckBox();
		this.checkBoxInlet = new XdevCheckBox();
		this.checkBoxOutlet = new XdevCheckBox();
		this.checkBoxTotale = new XdevCheckBox();
		this.checkBoxStatica = new XdevCheckBox();
		this.labelPortata = new XdevLabel();
		this.textFieldPortata = new TextFieldDouble();
		this.comboBoxPortataUM = new XdevComboBox<>();
		this.labelAlezzaSLM = new XdevLabel();
		this.textAltezzaSLM = new TextFieldDouble();
		this.comboBoxAltezzaSLMUM = new XdevComboBox<>();
		this.labelPressione = new XdevLabel();
		this.textFieldPressione = new TextFieldDouble();
		this.comboBoxPressioneUM = new XdevComboBox<>();
		this.labelTemperatura = new XdevLabel();
		this.textFieldTemperatura = new TextFieldDouble();
		this.comboBoxTemperaturaUM = new XdevComboBox<>();
		this.tableRisultati = new XdevTable<>();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.buttonSearch = new XdevButton();
		this.buttonAddToOffer = new XdevButton();
	
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.gridLayout.setMargin(new MarginInfo(false, false, false, true));
		this.checkBoxAssiali.setCaption("assiali");
		this.checkBoxCentrifughi.setCaption("centrifughi");
		this.checkBoxTR.setCaption("TR");
		this.checkBoxDA.setCaption("DA");
		this.checkBoxInlet.setCaption("inlet");
		this.checkBoxOutlet.setCaption("outlet");
		this.checkBoxTotale.setCaption("totale");
		this.checkBoxStatica.setCaption("statica");
		this.labelPortata.setValue("Q");
		this.labelPortata.setContentMode(ContentMode.HTML);
		this.labelAlezzaSLM.setValue("Altezza");
		this.labelAlezzaSLM.setContentMode(ContentMode.HTML);
		this.labelPressione.setValue("P");
		this.labelPressione.setContentMode(ContentMode.HTML);
		this.labelTemperatura.setValue("temp");
		this.labelTemperatura.setContentMode(ContentMode.HTML);
		this.tableRisultati.setStyleName("mystriped");
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.buttonSearch.setCaption("Search");
		this.buttonSearch.setStyleName("big azzurro");
		this.buttonAddToOffer.setCaption("Add to Offer");
		this.buttonAddToOffer.setStyleName("big giallo");
	
		this.gridLayout.setColumns(6);
		this.gridLayout.setRows(7);
		this.checkBoxAssiali.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxAssiali.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxAssiali, 0, 0, 1, 0);
		this.checkBoxCentrifughi.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxCentrifughi.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxCentrifughi, 2, 0, 3, 0);
		this.checkBoxTR.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTR.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxTR, 0, 1, 1, 1);
		this.checkBoxDA.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxDA.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxDA, 2, 1, 3, 1);
		this.checkBoxInlet.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxInlet.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxInlet, 0, 2, 1, 2);
		this.checkBoxOutlet.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxOutlet.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxOutlet, 2, 2, 3, 2);
		this.checkBoxTotale.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTotale.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxTotale, 0, 3, 1, 3);
		this.checkBoxStatica.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxStatica.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.checkBoxStatica, 2, 3, 3, 3);
		this.labelPortata.setWidth(100, Unit.PERCENTAGE);
		this.labelPortata.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelPortata, 0, 4);
		this.textFieldPortata.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPortata.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldPortata, 1, 4);
		this.comboBoxPortataUM.setWidth(80, Unit.PERCENTAGE);
		this.comboBoxPortataUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxPortataUM, 2, 4);
		this.labelAlezzaSLM.setWidth(100, Unit.PERCENTAGE);
		this.labelAlezzaSLM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelAlezzaSLM, 3, 4);
		this.textAltezzaSLM.setWidth(100, Unit.PERCENTAGE);
		this.textAltezzaSLM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textAltezzaSLM, 4, 4);
		this.comboBoxAltezzaSLMUM.setWidth(80, Unit.PERCENTAGE);
		this.comboBoxAltezzaSLMUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxAltezzaSLMUM, 5, 4);
		this.labelPressione.setWidth(100, Unit.PERCENTAGE);
		this.labelPressione.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelPressione, 0, 5);
		this.textFieldPressione.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPressione.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldPressione, 1, 5);
		this.comboBoxPressioneUM.setWidth(80, Unit.PERCENTAGE);
		this.comboBoxPressioneUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxPressioneUM, 2, 5);
		this.labelTemperatura.setWidth(100, Unit.PERCENTAGE);
		this.labelTemperatura.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelTemperatura, 3, 5);
		this.textFieldTemperatura.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTemperatura.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldTemperatura, 4, 5);
		this.comboBoxTemperaturaUM.setWidth(80, Unit.PERCENTAGE);
		this.comboBoxTemperaturaUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxTemperaturaUM, 5, 5);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setColumnExpandRatio(2, 10.0F);
		this.gridLayout.setColumnExpandRatio(3, 10.0F);
		this.gridLayout.setColumnExpandRatio(4, 10.0F);
		this.gridLayout.setColumnExpandRatio(5, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 6, 5, 6);
		this.gridLayout.setRowExpandRatio(6, 1.0F);
		this.buttonSearch.setWidth(100, Unit.PERCENTAGE);
		this.buttonSearch.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.buttonSearch);
		this.horizontalLayout.setComponentAlignment(this.buttonSearch, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.buttonSearch, 10.0F);
		this.buttonAddToOffer.setWidth(100, Unit.PERCENTAGE);
		this.buttonAddToOffer.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.buttonAddToOffer);
		this.horizontalLayout.setComponentAlignment(this.buttonAddToOffer, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.buttonAddToOffer, 10.0F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.tableRisultati.setSizeFull();
		this.verticalLayout.addComponent(this.tableRisultati);
		this.verticalLayout.setExpandRatio(this.tableRisultati, 20.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.comboBoxPortataUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.comboBoxPortataUM_valueChange(event);
			}
		});
		this.comboBoxAltezzaSLMUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.comboBoxAltezzaSLMUM_valueChange(event);
			}
		});
		this.comboBoxPressioneUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.comboBoxPressioneUM_valueChange(event);
			}
		});
		this.comboBoxTemperaturaUM.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.comboBoxTemperaturaUM_valueChange(event);
			}
		});
		this.tableRisultati.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.tableRisultati_valueChange(event);
			}
		});
		this.checkBoxInlet.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.checkBoxInlet_valueChange(event);
			}
		});
		this.checkBoxOutlet.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.checkBoxOutlet_valueChange(event);
			}
		});
		this.checkBoxTotale.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.checkBoxTotale_valueChange(event);
			}
		});
		this.checkBoxStatica.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobileSearchView.this.checkBoxStatica_valueChange(event);
			}
		});
		this.buttonSearch.addClickListener(event -> this.buttonSearch_buttonClick(event));
		this.buttonAddToOffer.addClickListener(event -> this.buttonAddToOffer_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelPortata, labelAlezzaSLM, labelPressione, labelTemperatura;
	private XdevButton buttonSearch, buttonAddToOffer;
	private XdevTable<CustomComponent> tableRisultati;
	private XdevHorizontalLayout horizontalLayout;
	private TextFieldDouble textFieldPortata, textAltezzaSLM, textFieldPressione, textFieldTemperatura;
	private XdevComboBox<?> comboBoxAltezzaSLMUM, comboBoxTemperaturaUM;
	private XdevCheckBox checkBoxAssiali, checkBoxCentrifughi, checkBoxTR, checkBoxDA, checkBoxInlet, checkBoxOutlet,
			checkBoxTotale, checkBoxStatica;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout;
	private XdevComboBox<CustomComponent> comboBoxPortataUM, comboBoxPressioneUM;
	// </generated-code>


}