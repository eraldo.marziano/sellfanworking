package com.cit.sellfan.ui.view.mobile;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.template.jGraficoXYWeb;
import com.cit.sellfan.ui.view.PreventiviView;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevSlider;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

import cit.sellfan.Costanti;
import cit.sellfan.classi.titoli.StringheUNI;
import de.steinwedel.messagebox.MessageBox;

public class MobilePrestazioniView extends XdevView {
	private VariabiliGlobali l_VG;
	public jGraficoXYWeb grafico = new jGraficoXYWeb();
	private boolean editEnabled;

	/**
	 * 
	 */
	public MobilePrestazioniView() {
		super();
		this.initUI();
		addShortcutListener(new ShortcutListener("Shortcut Enter", ShortcutAction.KeyCode.ENTER, null) {
		    @Override
		    public void handleAction(final Object sender, final Object target) {
		    	//l_VG.PrestazioniView.this.l_VG.MainView.addToMemo(Boolean.toString(target.equals(PrestazioniView.this.textFieldRpmRequest))+" handleAction rpm");
		    	if (target.equals(MobilePrestazioniView.this.textFieldRpmRequest)) {
		    		cambiaRpm();
		    	} else if (target.equals(MobilePrestazioniView.this.textFieldQRichiesta)) {
		    		cambiaPortata();
		    	}
		    }
		});
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoMobileView[this.l_VG.PrestazioniViewIndex] = this;
	}
	
	public void Nazionalizza() {
		this.buttonAddOffer.setVisible(this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
		this.buttonAddOffer.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
		this.buttonCangheMotor.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cambia Motore"));
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		try {
			if (this.l_VG == null || !this.l_VG.utilityCliente.isTabVisible("Prestazioni") || this.l_VG.VentilatoreCurrent == null) {
				return false;
			}
			if (!this.l_VG.VentilatoreCurrent.CurveTrovate) {
				return false;
			}
			return true;
		} catch (final Exception e) {
			return false;
		}
	}
	
	public void setVentilatoreCurrent() {
		this.editEnabled = false;
/*
		String memo = "FromPreventivo:" + Boolean.toString(l_VG.VentilatoreCurrent.FromPreventivo) + "  FromRicerca:" + Boolean.toString(l_VG.VentilatoreCurrent.FromRicerca);
		l_VG.MainView.setMemo(memo);
*/
        final String l_tit1 = this.l_VG.utilityCliente.buildTitoloGraficoPannelloPrestazioniPrimaRiga(this.l_VG.VentilatoreCurrent, this.l_VG.UMDiametroCorrente, this.l_VG.VentilatoreCurrent.UMAltezza, this.l_VG.VentilatoreCurrent.UMTemperatura);
        final String l_tit2 = this.l_VG.utilityCliente.buildTitoloGraficoPannelloPrestazioniSecondaRiga(this.l_VG.VentilatoreCurrent);
        if (this.l_VG.currentGraficoPrestazioniCurveWeb == null && this.l_VG.debugFlag) {
			Notification.show("setVentilatoreCurrent currentGraficoPrestazioniCurve");
		}
        this.l_VG.currentGraficoPrestazioniCurveWeb.reset();
        this.l_VG.currentGraficoPrestazioniCurveWeb.setParametriForPannelloPrestazioni(l_tit1, l_tit2, this.l_VG.ParametriVari.pannelloPrestazioniHieghtLightLimitiRicercaFlag);
        final boolean forzaGiri = this.l_VG.pannelloCatalogoForzaGiriMassimiFlag && this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaCatalogo && this.l_VG.VentilatoreCurrent.PrimaVolta;
        final boolean inizializzaMarker = this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaCatalogo && this.l_VG.VentilatoreCurrent.PrimaVolta;
        final boolean aggiornaAltriPannelli = true;
        final boolean displayCurvaCaratteristica = this.l_VG.VentilatoreCurrent.FromRicerca && this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag;
        boolean displayOrizzontalMarker = false;
        if (this.l_VG.pannelloPrestazioniMarkerOrizontalSpecialFlag) {
            displayOrizzontalMarker = true;
        } else {
            displayOrizzontalMarker = this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaRicerca || this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo;
        }
        this.l_VG.currentGraficoPrestazioniCurveWeb.setFlag(false, 2, forzaGiri, inizializzaMarker, aggiornaAltriPannelli, displayCurvaCaratteristica, this.l_VG.ParametriVari.pannelloPrestazionimarkerFlag, this.l_VG.ParametriVari.pannelloPrestazionimarkerFlag, displayOrizzontalMarker, this.l_VG.pannelloPrestazioniMarkerOrizontalSpecialFlag);
        //l_VG.currentGraficoPrestazioniCurve.setFlag(false, 2, false, false, false, false, true, true, true, true);
        this.l_VG.currentGraficoPrestazioniCurveWeb.buildGrafico(this.grafico);
        
//        grafico.buildSample();
		final StreamResource graficosource = new StreamResource(this.grafico, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		graficosource.setCacheTime(100);
		this.image.setSource(graficosource);
		showVentilatoreCurrent();
		this.buttonAddOffer.setEnabled(this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo);
        this.textFieldQRichiesta.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
        try {
            final double portata = this.l_VG.ventilatoreFisicaCurrent.getPortatam3h();
            this.textFieldQRichiesta.setValue(this.l_VG.utilityUnitaMisura.fromm3hToXX(portata, this.l_VG.VentilatoreCurrent.UMPortata));
        } catch (final Exception e) {
        	
        }
        this.textFieldRpmRequest.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
		this.editEnabled = true;
	}

	private void cambiaRpm() {
		if (!this.editEnabled) {
			return;
		}
		int newValue = this.textFieldRpmRequest.getIntegerValue();
		if (newValue < this.l_VG.VentilatoreCurrent.RpmMin) {
			newValue = this.l_VG.VentilatoreCurrent.RpmMin;
		}
		if (newValue > this.l_VG.VentilatoreCurrent.RpmMaxTeorici * this.l_VG.ParametriVari.pannelloCondizioniOverRpm) {
			newValue = (int)(this.l_VG.VentilatoreCurrent.RpmMaxTeorici * this.l_VG.ParametriVari.pannelloCondizioniOverRpm);
		}
		this.textFieldRpmRequest.setValue(newValue);
		if (this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri != newValue) {
			this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri = newValue;
	   		setVentilatoreCurrent();
	   		this.l_VG.ventilatoreCambiato = true;
		}
	}

	private void cambiaPortata() {
		if (!this.editEnabled) {
			return;
		}
		double newValue = this.textFieldQRichiesta.getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione;
		if (newValue < this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin) {
			newValue = this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin;
		}
		if (newValue > this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax) {
			newValue = this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax;
		}
		if (newValue / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione != this.sliderQ.getValue()) {
			this.sliderQ.setValue(newValue / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
		}
	}

	@SuppressWarnings("unused")
	private void showVentilatoreCurrent() {
		try {
			this.editEnabled = false;
			this.l_VG.ERP327Build.setERP327(this.l_VG.VentilatoreCurrent, this.l_VG.regolamento327Algoritmo, this.l_VG.ventilatoreFisicaParameter);
            this.l_VG.ventilatoreFisicaCurrent.setNumeroGiriPortatam3h(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri, this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker);
            double v = this.l_VG.ventilatoreFisicaCurrent.getPortatam3h();
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
            //l_VG.MainView.setMemo(l_VG.VentilatoreCurrent.UMPortata.simbolo+"  "+Integer.toString(l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa)+"  "+l_VG.UMPortataCorrente.simbolo+"  "+Integer.toString(l_VG.UMPortataCorrente.nDecimaliStampa));
            String str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromm3hToXX(v, this.l_VG.VentilatoreCurrent.UMPortata));
            this.labelQ.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PORTATA, " ", this.l_VG.VentilatoreCurrent.UMPortata.simbolo, "<font color='blue'> ", str));
            this.labelQ1.setValue(this.l_VG.utility.giuntaStringHtml(StringheUNI.PORTATA, " ", this.l_VG.VentilatoreCurrent.UMPortata.simbolo));
            this.sliderQ.setResolution(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
            this.sliderQ.setMin(this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
            this.sliderQ.setMax(this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
            final double portata = this.l_VG.ventilatoreFisicaCurrent.getPortatam3h();
            this.sliderQ.setValue(portata / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
            final double fattoreNm3h = this.l_VG.VentilatoreCurrent.CAProgettazione.rho / this.l_VG.utilityFisica.getDensitaAria(0);
            v *= fattoreNm3h;
            this.l_VG.fmtNd.setMaximumFractionDigits(0);
            v = this.l_VG.ventilatoreFisicaCurrent.getPressioneTotalePa();
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPressione.nDecimaliStampa);
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale = v;
            if (v > 0.) {
                str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(v, this.l_VG.VentilatoreCurrent.UMPressione));
            } else {
                str = "";
            }
            this.labelHT.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PRESSIONETOTALE, " ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, "<font color='blue'> ", str));
            v = this.l_VG.ventilatoreFisicaCurrent.getPressioneTotaleFreePa();
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotaleFree = v;
            v = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaPa();
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica = v;
            if (v > 0.) {
                str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(v, this.l_VG.VentilatoreCurrent.UMPressione));
            } else {
                str = "";
            }
            this.labelHSt.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PRESSIONESTATICA, " ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, "<font color='blue'> ", str));
            v = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaFreePa();
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStaticaFree = v;
            v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaW();
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.UMPotenzaCorrente.nDecimaliStampa);
            this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza = v;
            if (v > 0.) {
                str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(v, this.l_VG.UMPotenzaCorrente));
            } else {
                str = "";
            }
            this.labelNa.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZAASSE, " ", this.l_VG.UMPotenzaCorrente.simbolo, "<font color='blue'> ", str));
            v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaFreeW();
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.UMPotenzaCorrente.nDecimaliStampa);
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaFree = v;
            if (v > 0. && this.l_VG.VentilatoreCurrent.datiERP327 != null) {
            	v = (v + this.l_VG.VentilatoreCurrent.datiERP327.potenzaPersaCuscinettiW) / this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione;
            	this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsterna = v;
                str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(v, this.l_VG.UMPotenzaCorrente));
            } else {
            	this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsterna = -1.;
                str = "";
            }
            this.labelNe.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZAESTERNA, " ", this.l_VG.UMPotenzaCorrente.simbolo, "<font color='blue'> ", str));
            this.l_VG.fmtNd.setMaximumFractionDigits(2);
            v = this.l_VG.ventilatoreFisicaCurrent.getRendimento();
            if (v > 0.) {
                str = this.l_VG.fmtNd.format(v);
             } else {
            	str = "";
            }
            this.labeletaT.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.RENDIMENTO, " ", "[%]", "<font color='blue'> ", str));
            final double contributoXRumore = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(this.l_VG.VentilatoreCurrent.NBoccheCanalizzateBase, this.l_VG.VentilatoreCurrent.selezioneCorrente.nBoccheCanalizzate);
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.NDecimaliRumore);
            v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaSonora() + contributoXRumore;
            if (v > 0.) {
                str = this.l_VG.fmtNd.format(v);
             } else {
            	str = "";
            }
            this.labelLW.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZASONORA, " ", "[dB(A)]", "<font color='blue'> ", str));
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.NDecimaliRumore);
            if (this.l_VG.ventilatoreFisicaCurrent.isPtCurvaDisponibile() && this.l_VG.ventilatoreFisicaCurrent.isPotCurvaDisponibile()) {      //rendimento
            	this.labeletaT.setVisible(true);
            } else {
            	this.labeletaT.setVisible(false);
            }
            if (this.l_VG.ventilatoreFisicaCurrent.havePotenzaSonora()) {
            	this.labelLW.setVisible(true);
             } else {
            	this.labelLW.setVisible(false);
            }
            final boolean displayCurvaCaratteristica = this.l_VG.VentilatoreCurrent.FromRicerca && this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag;
            this.labelModelloCompleto.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent)));
            final int giri[] = new int[this.l_VG.VentilatoreCurrent.RpmLimite.length];
            for (int i=0 ; i<this.l_VG.VentilatoreCurrent.RpmLimite.length ; i++) {
            	giri[i] = this.l_VG.VentilatoreCurrent.RpmLimite[i];
            }
            if (giri[0] < 0) {
                giri[0] = (int)this.l_VG.VentilatoreCurrent.RpmInit;
            }
            int index = 0;
            for (index=0 ; index<this.l_VG.VentilatoreCurrent.RpmLimite.length-1 ; index++) {
                if (this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura <= this.l_VG.temperatureLimite[index]) {
					break;
				}
            }
            if (index == 0) {
            	str = "<=" + Integer.toString(this.l_VG.temperatureLimite[0]) + " °C:";
            } else if (index == this.l_VG.VentilatoreCurrent.RpmLimite.length - 1) {
            	str = ">" + Integer.toString(this.l_VG.temperatureLimite[index - 1]) + " °C:";
            } else {
            	str = Integer.toString((int)this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura) + " °C:";
            }
            str += "  " + Integer.toString(giri[index]);
            this.sliderRpm.setMin(this.l_VG.VentilatoreCurrent.RpmMin);
            this.sliderRpm.setMax(this.l_VG.VentilatoreCurrent.RpmMaxTeorici * this.l_VG.ParametriVari.pannelloCondizioniOverRpm);
            this.sliderRpm.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
            this.editEnabled = true;
        } catch (final Exception e) {
			if (this.l_VG.debugFlag) {
				Notification.show(e.toString());
			}
		}
	}
	/**
	 * Event handler delegate method for the {@link XdevSlider} {@link #sliderQ}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void sliderQ_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker = this.sliderQ.getValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione;
		setVentilatoreCurrent();
		this.l_VG.ventilatoreCambiato = true;
		if (this.l_VG.VentilatoreCurrent.FromPreventivo) {
			final PreventiviView view = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
			view.aggiornaVentilatoreCorrente();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevSlider} {@link #sliderRpm}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void sliderRpm_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri = this.sliderRpm.getValue();
		setVentilatoreCurrent();
		this.l_VG.ventilatoreCambiato = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAddOffer}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAddOffer_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToPreventivo();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCangheMotor}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCangheMotor_buttonClick(final Button.ClickEvent event) {
		final String warningStr = this.l_VG.utilityCliente.getWarning(this.l_VG.VentilatoreCurrent, 0);
		if (warningStr != null) {
			final MessageBox msgBox = MessageBox.create();
			msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(warningStr));
			msgBox.withOkButton(() -> {
				this.l_VG.cambiaManualmenteMotore();
				});
			msgBox.open();
		} else {
			this.l_VG.cambiaManualmenteMotore();
		}
	}

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldRpmRequest}.
	 *
	 * @see FieldEvents.BlurListener#blur(FieldEvents.BlurEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldRpmRequest_blur(final FieldEvents.BlurEvent event) {
		cambiaRpm();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldQRichiesta}.
	 *
	 * @see FieldEvents.BlurListener#blur(FieldEvents.BlurEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQRichiesta_blur(final FieldEvents.BlurEvent event) {
		cambiaPortata();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelModelloCompleto = new XdevLabel();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.image = new XdevImage();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.panel5 = new XdevPanel();
		this.gridLayout1 = new XdevGridLayout();
		this.labelQ = new XdevLabel();
		this.labelHT = new XdevLabel();
		this.labelHSt = new XdevLabel();
		this.labelNa = new XdevLabel();
		this.labelNe = new XdevLabel();
		this.labeletaT = new XdevLabel();
		this.labelLW = new XdevLabel();
		this.labelRiserva3 = new XdevLabel();
		this.labelRiserva4 = new XdevLabel();
		this.labelRiserva5 = new XdevLabel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.labelQ1 = new XdevLabel();
		this.textFieldQRichiesta = new TextFieldDouble();
		this.sliderQ = new XdevSlider();
		this.labelRPM = new XdevLabel();
		this.textFieldRpmRequest = new TextFieldInteger();
		this.sliderRpm = new XdevSlider();
		this.horizontalLayout6 = new XdevHorizontalLayout();
		this.buttonAddOffer = new XdevButton();
		this.buttonCangheMotor = new XdevButton();
	
		this.verticalLayout.setSpacing(false);
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.labelModelloCompleto.setValue("ModelloCompleto");
		this.labelModelloCompleto.setContentMode(ContentMode.HTML);
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.verticalLayout3.setSpacing(false);
		this.verticalLayout3.setMargin(new MarginInfo(false));
		this.horizontalLayout2.setSpacing(false);
		this.horizontalLayout2.setMargin(new MarginInfo(false));
		this.panel5.setTabIndex(0);
		this.panel5.setStyleName("dark");
		this.gridLayout1.setSpacing(false);
		this.gridLayout1.setMargin(new MarginInfo(false, true, false, true));
		this.labelQ.setValue("<html><b>H<sub>D</sub><font color='blue'>pippo</html>");
		this.labelQ.setContentMode(ContentMode.HTML);
		this.labelHT.setValue("Label");
		this.labelHT.setContentMode(ContentMode.HTML);
		this.labelHSt.setValue("Label");
		this.labelHSt.setContentMode(ContentMode.HTML);
		this.labelNa.setValue("Label");
		this.labelNa.setContentMode(ContentMode.HTML);
		this.labelNe.setValue("Label");
		this.labelNe.setContentMode(ContentMode.HTML);
		this.labeletaT.setValue("Label");
		this.labeletaT.setContentMode(ContentMode.HTML);
		this.labelLW.setValue("Label");
		this.labelLW.setContentMode(ContentMode.HTML);
		this.labelRiserva3.setContentMode(ContentMode.HTML);
		this.labelRiserva4.setContentMode(ContentMode.HTML);
		this.labelRiserva5.setContentMode(ContentMode.HTML);
		this.verticalLayout4.setSpacing(false);
		this.verticalLayout4.setMargin(new MarginInfo(false));
		this.horizontalLayout3.setMargin(new MarginInfo(false, true, false, true));
		this.labelQ1.setValue("Label");
		this.labelQ1.setContentMode(ContentMode.HTML);
		this.textFieldQRichiesta.setColumns(5);
		this.labelRPM.setValue("Rpm:");
		this.labelRPM.setContentMode(ContentMode.HTML);
		this.textFieldRpmRequest.setColumns(5);
		this.horizontalLayout6.setMargin(new MarginInfo(false, true, false, true));
		this.buttonAddOffer.setCaption("Add to Offer");
		this.buttonAddOffer.setStyleName("big giallo");
		this.buttonCangheMotor.setCaption("Change motor");
		this.buttonCangheMotor.setStyleName("big");
	
		this.image.setWidth(100, Unit.PERCENTAGE);
		this.image.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.image);
		this.horizontalLayout.setComponentAlignment(this.image, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.image, 10.0F);
		this.gridLayout1.setColumns(7);
		this.gridLayout1.setRows(4);
		this.labelQ.setWidth(100, Unit.PERCENTAGE);
		this.labelQ.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelQ, 0, 0);
		this.labelHT.setWidth(100, Unit.PERCENTAGE);
		this.labelHT.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelHT, 1, 0);
		this.labelHSt.setWidth(100, Unit.PERCENTAGE);
		this.labelHSt.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelHSt, 2, 0);
		this.labelNa.setWidth(100, Unit.PERCENTAGE);
		this.labelNa.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelNa, 3, 0);
		this.labelNe.setWidth(100, Unit.PERCENTAGE);
		this.labelNe.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelNe, 4, 0);
		this.labeletaT.setWidth(100, Unit.PERCENTAGE);
		this.labeletaT.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labeletaT, 5, 0);
		this.labelLW.setWidth(100, Unit.PERCENTAGE);
		this.labelLW.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelLW, 6, 0);
		this.labelRiserva3.setWidth(100, Unit.PERCENTAGE);
		this.labelRiserva3.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelRiserva3, 4, 1);
		this.labelRiserva4.setWidth(100, Unit.PERCENTAGE);
		this.labelRiserva4.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelRiserva4, 5, 1);
		this.labelRiserva5.setWidth(100, Unit.PERCENTAGE);
		this.labelRiserva5.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelRiserva5, 5, 2);
		this.gridLayout1.setColumnExpandRatio(0, 10.0F);
		this.gridLayout1.setColumnExpandRatio(1, 10.0F);
		this.gridLayout1.setColumnExpandRatio(2, 10.0F);
		this.gridLayout1.setColumnExpandRatio(3, 10.0F);
		this.gridLayout1.setColumnExpandRatio(4, 10.0F);
		this.gridLayout1.setColumnExpandRatio(5, 10.0F);
		this.gridLayout1.setColumnExpandRatio(6, 10.0F);
		final CustomComponent gridLayout1_vSpacer = new CustomComponent();
		gridLayout1_vSpacer.setSizeFull();
		this.gridLayout1.addComponent(gridLayout1_vSpacer, 0, 3, 6, 3);
		this.gridLayout1.setRowExpandRatio(3, 1.0F);
		this.gridLayout1.setSizeFull();
		this.panel5.setContent(this.gridLayout1);
		this.panel5.setWidth(100, Unit.PERCENTAGE);
		this.panel5.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.panel5);
		this.horizontalLayout2.setExpandRatio(this.panel5, 100.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout2);
		this.verticalLayout3.setComponentAlignment(this.horizontalLayout2, Alignment.BOTTOM_RIGHT);
		this.verticalLayout3.setExpandRatio(this.horizontalLayout2, 10.0F);
		this.labelQ1.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.labelQ1);
		this.horizontalLayout3.setComponentAlignment(this.labelQ1, Alignment.MIDDLE_CENTER);
		this.textFieldQRichiesta.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.textFieldQRichiesta);
		this.horizontalLayout3.setComponentAlignment(this.textFieldQRichiesta, Alignment.MIDDLE_CENTER);
		this.sliderQ.setWidth(100, Unit.PERCENTAGE);
		this.sliderQ.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.sliderQ);
		this.horizontalLayout3.setComponentAlignment(this.sliderQ, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.sliderQ, 10.0F);
		this.labelRPM.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.labelRPM);
		this.horizontalLayout3.setComponentAlignment(this.labelRPM, Alignment.MIDDLE_CENTER);
		this.textFieldRpmRequest.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.textFieldRpmRequest);
		this.horizontalLayout3.setComponentAlignment(this.textFieldRpmRequest, Alignment.MIDDLE_CENTER);
		this.sliderRpm.setWidth(100, Unit.PERCENTAGE);
		this.sliderRpm.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.sliderRpm);
		this.horizontalLayout3.setComponentAlignment(this.sliderRpm, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.sliderRpm, 10.0F);
		this.buttonAddOffer.setWidth(100, Unit.PERCENTAGE);
		this.buttonAddOffer.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.buttonAddOffer);
		this.horizontalLayout6.setComponentAlignment(this.buttonAddOffer, Alignment.MIDDLE_LEFT);
		this.horizontalLayout6.setExpandRatio(this.buttonAddOffer, 10.0F);
		this.buttonCangheMotor.setWidth(100, Unit.PERCENTAGE);
		this.buttonCangheMotor.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.buttonCangheMotor);
		this.horizontalLayout6.setComponentAlignment(this.buttonCangheMotor, Alignment.MIDDLE_CENTER);
		this.horizontalLayout6.setExpandRatio(this.buttonCangheMotor, 10.0F);
		this.horizontalLayout3.setSizeFull();
		this.verticalLayout4.addComponent(this.horizontalLayout3);
		this.verticalLayout4.setComponentAlignment(this.horizontalLayout3, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.horizontalLayout3, 10.0F);
		this.horizontalLayout6.setSizeFull();
		this.verticalLayout4.addComponent(this.horizontalLayout6);
		this.verticalLayout4.setComponentAlignment(this.horizontalLayout6, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.horizontalLayout6, 10.0F);
		this.labelModelloCompleto.setWidth(100, Unit.PERCENTAGE);
		this.labelModelloCompleto.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.labelModelloCompleto);
		this.verticalLayout.setComponentAlignment(this.labelModelloCompleto, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.horizontalLayout, 10.0F);
		this.verticalLayout3.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.verticalLayout3);
		this.verticalLayout.setComponentAlignment(this.verticalLayout3, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.verticalLayout4);
		this.verticalLayout.setComponentAlignment(this.verticalLayout4, Alignment.BOTTOM_RIGHT);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.textFieldQRichiesta.addBlurListener(event -> this.textFieldQRichiesta_blur(event));
		this.sliderQ.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobilePrestazioniView.this.sliderQ_valueChange(event);
			}
		});
		this.textFieldRpmRequest.addBlurListener(event -> this.textFieldRpmRequest_blur(event));
		this.sliderRpm.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobilePrestazioniView.this.sliderRpm_valueChange(event);
			}
		});
		this.buttonAddOffer.addClickListener(event -> this.buttonAddOffer_buttonClick(event));
		this.buttonCangheMotor.addClickListener(event -> this.buttonCangheMotor_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelModelloCompleto, labelQ, labelHT, labelHSt, labelNa, labelNe, labeletaT, labelLW, labelRiserva3,
			labelRiserva4, labelRiserva5, labelQ1, labelRPM;
	private XdevButton buttonAddOffer, buttonCangheMotor;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2, horizontalLayout3, horizontalLayout6;
	private XdevImage image;
	private TextFieldDouble textFieldQRichiesta;
	private XdevSlider sliderQ, sliderRpm;
	private XdevPanel panel5;
	private XdevGridLayout gridLayout1;
	private XdevVerticalLayout verticalLayout, verticalLayout3, verticalLayout4;
	private TextFieldInteger textFieldRpmRequest;
	// </generated-code>

}
