package com.cit.sellfan.ui.view.mobile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.pannelli.pannelloPreventivoEditNew;
import com.cit.sellfan.ui.view.PreventiviView;
import com.cit.sellfan.ui.view.ProgressView;
import com.cit.sellfan.ui.view.cliente04.Cliente04pannelloPagineOfferta;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.classi.Clientev21;
import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.sellfan.Costanti;
import cit.sellfan.classi.Preventivo;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatorePreventivo;
import cit.sellfan.personalizzazioni.cliente04.Cliente04StampaOffertaPoiXLSX;
import de.steinwedel.messagebox.MessageBox;

public class MobilePreventiviVentilatori extends XdevView {
	private VariabiliGlobali l_VG;
	private Container containerPreventivi;
	private Container containerVentilatori;
	private final ArrayList<XdevTextField> elencoNoteVentilatori = new ArrayList<>();
	private FileDownloader fileDownloader;
    private final ArrayList<String> ElencoFileXls = new ArrayList<>();
	private MessageBox msgBox;
	private String memoDownload;

	/**
	 * 
	 */
	public MobilePreventiviVentilatori() {
		super();
		this.initUI();
		initContainerPreventivi();
/*
		this.containerPreventivi.removeAllItems();
		this.containerPreventivi.addContainerProperty("Offerta", String.class, "");
		this.containerPreventivi.addContainerProperty("Cliente", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("RSoc", String.class, "");
		this.containerPreventivi.addContainerProperty("Data", Date.class, null);
		this.containerPreventivi.addContainerProperty("Ordine", String.class, "");
		this.containerPreventivi.addContainerProperty("Price", Double.class, "");
		this.containerPreventivi.addContainerProperty("PriceUser", Double.class, "");
		this.containerPreventivi.addContainerProperty("N", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("Note", XdevTextField.class, "");
		this.containerPreventivi.addContainerProperty("Codice", Integer.class, 0);
		this.tablePreventivi.setContainerDataSource(this.containerPreventivi);
*/
		initContainerVentilatori();
/*
		this.containerVentilatori.removeAllItems();
		this.containerVentilatori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerVentilatori.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerVentilatori.addContainerProperty("Ventilatore", String.class, "");
		this.containerVentilatori.addContainerProperty("Q", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Pt", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Motore", String.class, "");
		this.containerVentilatori.addContainerProperty("Note", XdevTextField.class, "");
		this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
*/
	}
	
	public void setPreventivoSelected(final String idPreventivo) {
		this.tablePreventivi.select(idPreventivo);
	}
	
	private void initContainerPreventivi() {
		this.containerPreventivi = new IndexedContainer();
		this.containerPreventivi.addContainerProperty("Offerta", String.class, "");
		this.containerPreventivi.addContainerProperty("Cliente", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("RSoc", String.class, "");
		this.containerPreventivi.addContainerProperty("Data", Date.class, null);
		this.containerPreventivi.addContainerProperty("Ordine", String.class, "");
		this.containerPreventivi.addContainerProperty("Price", Double.class, "");
		this.containerPreventivi.addContainerProperty("PriceUser", Double.class, "");
		this.containerPreventivi.addContainerProperty("N", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("Note", XdevTextField.class, "");
		this.containerPreventivi.addContainerProperty("Codice", Integer.class, 0);
		this.tablePreventivi.setContainerDataSource(this.containerPreventivi);
	}
	
	private void initContainerVentilatori() {
		this.containerVentilatori = new IndexedContainer();
		this.containerVentilatori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerVentilatori.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerVentilatori.addContainerProperty("Ventilatore", String.class, "");
		this.containerVentilatori.addContainerProperty("Q", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Pt", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Motore", String.class, "");
		this.containerVentilatori.addContainerProperty("Note", XdevTextField.class, "");
		this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
	}

	private void setConverterForTable() {
		final myStringToDoubleConverter converter = new myStringToDoubleConverter();
		this.tablePreventivi.setConverter("Price", converter);
		this.tablePreventivi.setConverter("N", converter);
	}
	
	private class myStringToDoubleConverter extends StringToDoubleConverter {
		public myStringToDoubleConverter() {
			super();
		}
		
		@Override
		public java.text.NumberFormat getFormat(final Locale locale) {
			return MobilePreventiviVentilatori.this.l_VG.fmtNdTable;
		}
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		setConverterForTable();
	}

	public void Nazionalizza() {
		this.buttonCancellaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella Preventivo"));
		this.buttonSalvaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Salva Preventivo"));
		this.buttonChiudiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Chiudi Preventivo"));
		this.buttonNuovoPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nuovo Preventivo"));
		
		this.buttonResetPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset Preventivo"));
		this.buttonCancellaVentilatore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella"));
		
		this.buttonCambiaMotore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cambia Motore"));
		this.buttonGeneraPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Genera Preventivo"));
		this.buttonDownloadPreventivo.setCaption("Download " + this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo"));

		this.tablePreventivi.setColumnHeader("Codice", "");
		this.tablePreventivi.setColumnHeader("Data", this.l_VG.utilityTraduzioni.TraduciStringa("Data"));
		this.tablePreventivi.setColumnHeader("Cliente", this.l_VG.utilityTraduzioni.TraduciStringa("Cliente"));
		this.tablePreventivi.setColumnHeader("RSoc", this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale"));
		this.tablePreventivi.setColumnHeader("Offerta", this.l_VG.utilityTraduzioni.TraduciStringa("Offerta"));
		this.tablePreventivi.setColumnHeader("Ordine", this.l_VG.utilityTraduzioni.TraduciStringa("Ordine"));
		this.tablePreventivi.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo") + " " + Costanti.EURO);
		this.tablePreventivi.setColumnHeader("N", this.l_VG.utilityTraduzioni.TraduciStringa("N. Vent."));
		this.tablePreventivi.setColumnHeader("Note", this.l_VG.utilityTraduzioni.TraduciStringa("Note"));
		this.tablePreventivi.setColumnWidth("Codice", 0);

		this.tableVentilatori.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableVentilatori.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
		this.tableVentilatori.setColumnHeader("Ventilatore", this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatore"));
		this.tableVentilatori.setColumnHeader("Motore", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.tableVentilatori.setColumnHeader("Note", this.l_VG.utilityTraduzioni.TraduciStringa("Note"));
	}
	
	public void storePreventivo() {
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
	}
	
	@SuppressWarnings("unchecked")
	public void aggiornaVentilatoreCorrente() {
		//Notification.show("aggiornaVentilatoreCorrente");
		final Collection<?> Ventilatoriids = this.containerVentilatori.getItemIds();
		int i = 0;
		this.tableVentilatori.setEnabled(false);
		for(final Object id : Ventilatoriids) {
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i++);
			final Item item = this.containerVentilatori.getItem(id);
			item.getItemProperty("Ventilatore").setValue(this.l_VG.buildModelloCompleto(l_v));
			item.getItemProperty("Motore").setValue(l_v.selezioneCorrente.MotoreInstallato.CodiceCliente);
			double v = this.l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, l_v.UMPortata);
			this.l_VG.fmtNd.setMaximumFractionDigits(l_v.UMPortata.nDecimaliStampa);
			XdevLabel lb = (XdevLabel)item.getItemProperty("Q").getValue();
			lb.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPortata.simbolo);
			v = this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, l_v.UMPressione);
			lb = (XdevLabel)item.getItemProperty("Pt").getValue();
			lb.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPressione.simbolo);
			try {
				this.elencoNoteVentilatori.get(this.l_VG.VentilatoreCurrentIndex).setValue(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).Note);
			} catch (final Exception e) {
				
			}
		}
		storePreventivo();
		this.tableVentilatori.setEnabled(true);
	}
	
	@SuppressWarnings("unchecked")
	public void aggiornaRigaPreventivoCorrente() {
		if (this.l_VG.preventivoCurrent == null) {
			return;
		}
//		this.tablePreventivi.setEnabled(false);
		final Item item = this.containerPreventivi.getItem(Integer.toString(this.l_VG.preventivoCurrent.IDPreventivo));
		if (item == null) {
			return;
		}
		item.getItemProperty("Offerta").setValue(this.l_VG.preventivoCurrent.NumOfferta);
		item.getItemProperty("Ordine").setValue(this.l_VG.preventivoCurrent.NumOrdine);
		final Date d = new Date();
		d.setTime(this.l_VG.preventivoCurrent.Data.getTime());
		item.getItemProperty("Data").setValue(d);
		item.getItemProperty("Cliente").setValue(this.l_VG.preventivoCurrent.IDCliente);
		item.getItemProperty("RSoc").setValue(this.l_VG.preventivoCurrent.ragioneSocialeCliente);
		item.getItemProperty("N").setValue(this.l_VG.preventivoCurrent.Ventilatori.size());
        this.l_VG.fmtNd.setMinimumFractionDigits(2);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
		//item.getItemProperty("Price").setValue(this.l_VG.fmtNd.format(this.l_VG.preventivoCurrent.CostoTotale));
		item.getItemProperty("Price").setValue(this.l_VG.preventivoCurrent.CostoTotale);
		final double prezzoVendita = this.l_VG.preventivoCurrent.CostoTotale * (100. - this.l_VG.preventivoCurrent.Sconto) / 100.;
		//item.getItemProperty("PriceUser").setValue(this.l_VG.fmtNd.format(prezzoVendita));
		item.getItemProperty("PriceUser").setValue(prezzoVendita);
		storePreventivo();
//		this.tablePreventivi.setEnabled(true);
	}

	@SuppressWarnings("unchecked")
	public void aggiornaCostoPreventivoCorrente() {
//		this.tablePreventivi.setEnabled(false);
		final Item item = this.containerPreventivi.getItem(Integer.toString(this.l_VG.preventivoCurrent.IDPreventivo));
		if (item == null) {
			return;
		}
        this.l_VG.fmtNd.setMinimumFractionDigits(2);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
		//item.getItemProperty("Price").setValue(this.l_VG.fmtNd.format(this.l_VG.preventivoCurrent.CostoTotale));
		item.getItemProperty("Price").setValue(this.l_VG.preventivoCurrent.CostoTotale);
		final double prezzoVendita = this.l_VG.preventivoCurrent.CostoTotale * (100. - this.l_VG.preventivoCurrent.Sconto) / 100.;
		//item.getItemProperty("PriceUser").setValue(this.l_VG.fmtNd.format(prezzoVendita));
		item.getItemProperty("PriceUser").setValue(prezzoVendita);
//		this.tablePreventivi.setEnabled(true);
	}
	
	public void aggiornaPreventivoCorrente() {
		aggiornaRigaPreventivoCorrente();
		this.tableVentilatori.setEnabled(false);
		initTabellaVentilatori();
		abilitaBottoni();
		this.tableVentilatori.setEnabled(true);
	}
	
	public void initTabellaPreventivi() {
		initTabellaPreventivi(false);
	}
	
	public void initTabellaPreventivi(final boolean preventivoNew) {
		if (!preventivoNew && this.l_VG.preventivoCurrent != null && this.l_VG.preventivoCurrentIndex != -1) {
			aggiornaPreventivoCorrente();
			return;
		}
		this.horizontalLayout2.removeAllComponents();
		@SuppressWarnings("unused")
		final Item item;
		this.l_VG.preventivoCurrent = null;
		this.l_VG.preventivoCurrentIndex = -1;
		this.tablePreventivi.setEnabled(false);
		initContainerPreventivi();
		try {
			for (int i=0 ; i<this.l_VG.ElencoPreventivi.size() ; i++) {
				final Object obj[] = new Object[10];
				final Preventivo l_p = this.l_VG.ElencoPreventivi.get(i);
				final String itemID = Integer.toString(l_p.IDPreventivo);
				obj[0] = l_p.NumOfferta;
				obj[1] = l_p.IDCliente;
				obj[2] = l_p.ragioneSocialeCliente;
				final Date d = new Date();
				d.setTime(l_p.Data.getTime());
				obj[3] = d;
				obj[4] = l_p.NumOrdine;
	            this.l_VG.fmtNd.setMinimumFractionDigits(2);
	            this.l_VG.fmtNd.setMaximumFractionDigits(2);
	            //obj[5] = this.l_VG.fmtNd.format(l_p.CostoTotale);
	            obj[5] = l_p.CostoTotale;
	    		final double prezzoVendita = l_p.CostoTotale * (100. - l_p.Sconto) / 100.;
	    		//obj[6] = this.l_VG.fmtNd.format(prezzoVendita);
	    		obj[6] = prezzoVendita;
				obj[7] = l_p.Ventilatori.size();
				final XdevTextField tf = new XdevTextField();
				tf.setValue(l_p.Note);
				tf.setData(itemID);
				tf.setEnabled(true);
				tf.addValueChangeListener(new ValueChangeListener(){
					   @Override
					public void valueChange(final Property.ValueChangeEvent event){
						   l_p.Note = tf.getValue();
						   l_p.PreventivoCambiato = true;
					   }
				});
				obj[8] = tf;
				obj[9] = l_p.IDPreventivo;
				this.tablePreventivi.addItem(obj, itemID);
			}
		} catch (final Exception e) {
			this.l_VG.MainView.setMemo(e.toString());
		}
		initTabellaVentilatori();
		abilitaBottoni();
		this.tablePreventivi.setEnabled(true);
		this.tablePreventivi.setSizeFull();
		this.horizontalLayout2.addComponent(this.tablePreventivi);
		this.horizontalLayout2.setComponentAlignment(this.tablePreventivi, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.tablePreventivi, 10.0F);
//		final Collection IDcollection = this.tablePreventivi.getItemIds();
//		Notification.show("initTabellaPreventivi "+Integer.toString(IDcollection.size()));
	}
	
	public void reinitTabellaPreventivi() {
        initTabellaPreventivi(true);
		this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
	}

	public void NewModifyPrenentivo(final boolean newMode) {
		if (newMode) {
	        if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneNuovaOfferta, this.l_VG.ElencoPreventivi.size())) {
	        	this.msgBox = MessageBox.createWarning();
	        	this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
	        	this.msgBox.withOkButton();
	        	this.msgBox.open();
	            return;
	        }
		}
		final pannelloPreventivoEditNew pannello = new pannelloPreventivoEditNew();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.newMode = newMode;
		pannello.init();
		this.msgBox = MessageBox.create();
		this.msgBox.withMessage(pannello);
		this.msgBox.withAbortButton();
		this.msgBox.withOkButton(() -> {
			if (newMode) {
				this.l_VG.preventivoCurrent = new Preventivo();
				this.l_VG.ElencoVentilatoriFromPreventivo.clear();
			}
			pannello.getValues();
			storePreventivo();
	        final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
	        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
	        if (newMode) {
	        	this.l_VG.preventivoCurrentIndex = this.l_VG.ElencoPreventivi.size() - 1;
	        }
	        this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex);
			PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
	        preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
	        if (newMode) {
	        	initTabellaPreventivi(true);
//	        	this.preventiviView.initPreventivi();
	        	setPreventivoSelected(idPreventivo.toString());
	            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneNuovaOfferta, "ID: " + Integer.toString(idPreventivo));
	        } else {
	        	preventiviView.aggiornaPreventivo();
	        }
		});
		this.msgBox.open();
	}

	private void abilitaBottoni() {
		boolean value = false;
		if (this.l_VG.preventivoCurrent != null) {
			value = true;
		}
		this.buttonSalvaPreventivo.setEnabled(value);
		this.buttonChiudiPreventivo.setEnabled(value);
		this.buttonCancellaPreventivo.setEnabled(value);
		this.buttonResetPreventivo.setEnabled(value);
		this.buttonGeneraPreventivo.setEnabled(value);
		this.buttonDownloadPreventivo.setEnabled(false);
		value = false;
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		if (preventiviView.ventilatoreIndex >= 0) {
			value = true;
		}
		this.buttonCambiaMotore.setEnabled(value);
		this.buttonCancellaVentilatore.setEnabled(value);
		preventiviView.abilitaTab();
		
		this.buttonCancellaPreventivo.setVisible(false);
	}

	private void initTabellaVentilatori() {
		double v;
		this.tableVentilatori.setEnabled(false);
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		preventiviView.ventilatoreIndex = -1;
		initContainerVentilatori();
		if (this.l_VG.preventivoCurrent == null) {
			return;
		}
		this.elencoNoteVentilatori.clear();
		for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
			final Object obj[] = new Object[7];
			final String itemID = Integer.toString(i);
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
			final VentilatorePreventivo l_vp = this.l_VG.preventivoCurrent.Ventilatori.get(i);
			final CheckBox cb = new CheckBox();
			cb.setData(itemID);
			cb.setValue(l_v.Selezionato);
			cb.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						l_v.Selezionato = cb.getValue().booleanValue();
						l_vp.HaPreventivo = l_v.Selezionato;
						l_v.VentilatoreCambiato = true;
						final PreventiviView preventivoTemp = (PreventiviView)MobilePreventiviVentilatori.this.l_VG.ElencoView[MobilePreventiviVentilatori.this.l_VG.PreventiviViewIndex];
						MobilePreventiviVentilatori.this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();;
						final PreventiviView preventiviView = (PreventiviView)MobilePreventiviVentilatori.this.l_VG.ElencoView[MobilePreventiviVentilatori.this.l_VG.PreventiviViewIndex];
						preventiviView.aggiornaPreventivo();
						preventiviView.aggiornaPreventivo();
						preventiviView.abilitaTab();
					}
		    });
			obj[0] = cb;
			final TextFieldInteger tfi = new TextFieldInteger();
//			VentilatorePreventivo l_vp = l_VG.preventivoCurrent.Ventilatori.get(i);
			tfi.setValue(Integer.toString(l_vp.Qta));
			tfi.setData(itemID);
			tfi.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						l_vp.Qta = tfi.getIntegerValue();
						l_v.VentilatoreCambiato = true;
						final PreventiviView preventivoTemp = (PreventiviView)MobilePreventiviVentilatori.this.l_VG.ElencoView[MobilePreventiviVentilatori.this.l_VG.PreventiviViewIndex];
						MobilePreventiviVentilatori.this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();;
						final PreventiviView preventiviView = (PreventiviView)MobilePreventiviVentilatori.this.l_VG.ElencoView[MobilePreventiviVentilatori.this.l_VG.PreventiviViewIndex];
						preventiviView.aggiornaPreventivo();
					}
			});
			obj[1] = tfi;
			obj[2] = this.l_VG.buildModelloCompleto(l_v);
			final XdevLabel lblQ = new XdevLabel();
			lblQ.setContentMode(ContentMode.HTML);
			v = this.l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, l_v.UMPortata);
			this.l_VG.fmtNd.setMaximumFractionDigits(l_v.UMPortata.nDecimaliStampa);
			lblQ.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPortata.simbolo);
			obj[3] = lblQ;
			final XdevLabel lblPt = new XdevLabel();
			lblPt.setContentMode(ContentMode.HTML);
			v = this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, l_v.UMPressione);
			this.l_VG.fmtNd.setMaximumFractionDigits(l_v.UMPressione.nDecimaliStampa);
			lblPt.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPressione.simbolo);
			obj[4] = lblPt;
			obj[5] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
			final XdevTextField tf = new XdevTextField();
			tf.setValue(l_v.Note);
			tf.setData(itemID);
			tf.setEnabled(true);
			tf.addValueChangeListener(new ValueChangeListener() {
				   @Override
				public void valueChange(final Property.ValueChangeEvent event){
					   l_v.Note = tf.getValue();
					   l_v.VentilatoreCambiato = true;
				   }
			});
			this.elencoNoteVentilatori.add(tf);
			obj[6] = tf;
			this.tableVentilatori.addItem(obj, itemID);
		}
		this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
		this.tableVentilatori.setEnabled(true);
	}
/*
	private double calcolaCostoPreventivo() {
		double costoPreventivo = 0.0;
		for (int i=0 ; i<l_VG.preventivoCurrent.Ventilatori.size() ; i++) {
			if (l_VG.preventivoCurrent.Ventilatori.get(i).HaPreventivo) {
				costoPreventivo += l_VG.preventivoCurrent.Ventilatori.get(i).Qta * l_VG.preventivoCurrent.Ventilatori.get(i).CostoVentilatore;
			}
		}
		return costoPreventivo;
	}
*/
	private void generaPreventivo() {
		try {
	        final ArrayList<String> ElencoModelloCompleto = new ArrayList<>();
	        for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
	            ElencoModelloCompleto.add(this.l_VG.buildModelloCompleto(this.l_VG.ElencoVentilatoriFromPreventivo.get(i), this.l_VG.parametriModelloCompletoForPrint));
	        }
	        for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
	            final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
	    		this.l_VG.dbTecnico.caricaDimensioniEsecuzione(l_v);
	            if (!l_v.DatiCompleti) {
	                this.l_VG.completaCaricamentoVentilatore(l_v);
	            }
	        }
	        InputStream inwb = null;
            inwb = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + this.l_VG.utilityCliente.idCliente + "_template_offerta.xlsx");
            this.l_VG.stampaOfferta.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.ventilatoreFisicaParameter, inwb);
            this.l_VG.stampaOfferta.initUtility(this.l_VG.currentCitFont, this.l_VG.utility, this.l_VG.utilityTraduzioni, this.l_VG.utilityUnitaMisura, null, null);
	        final InputStream isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoofferta.png");
	        final InputStream isCurva[] = new InputStream[14];
	        if (this.l_VG.sfondoGraficiPrestazioni != null && this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
	        	for (int i=0 ; i<14 ; i++) {
					isCurva[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
				}
	        }
	        final InputStream isRumore[] = new InputStream[14];
	        final InputStream isRumore1[] = new InputStream[14];
	        if (this.l_VG.sfondoGraficiSpettro != null && this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag) {
	        	for (int i=0 ; i<14 ; i++) {
					isRumore[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
				}
	        	for (int i=0 ; i<14 ; i++) {
					isRumore1[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
				}
	        	
	        }
	        this.l_VG.stampaOfferta.initStampaApachePOI(ElencoModelloCompleto, this.l_VG.utilityCliente, this.l_VG.gestioneAccessori, this.l_VG.currentUtilizzatore, isLogo, this.l_VG.Paths, this.l_VG.assiGraficiDefault, isCurva, isRumore, isRumore1);
	        this.l_VG.stampaOfferta.eseguiStampa(this.l_VG.Paths.rootResources + this.l_VG.preventivoCurrent.NumOfferta, this.ElencoFileXls, this.l_VG.currentCliente, this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo, null, this.l_VG.ParametriVari);
	        if (this.ElencoFileXls.size() > 0) {
	        	this.l_VG.dbCRMv2.storePreventivoFanStampato(this.l_VG.preventivoCurrent, this.ElencoFileXls.get(0));
	        	final String memo = this.l_VG.utilityTraduzioni.TraduciStringa("Generato") + " " + this.ElencoFileXls.get(0);
	            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneStampaOfferta, memo);
	            this.buttonDownloadPreventivo.setEnabled(true);
		        final Resource res = new FileResource(new File(this.ElencoFileXls.get(0)));
		        this.memoDownload = this.ElencoFileXls.get(0);
		        if (this.fileDownloader == null) {
		        	this.fileDownloader = new FileDownloader(res);
		    		this.fileDownloader.extend(this.buttonDownloadPreventivo);
		        } else {
		        	this.fileDownloader.setFileDownloadResource(res);
		        }
				this.msgBox = MessageBox.createInfo();
				this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo Correttamente Generato"));
				this.msgBox.withOkButton();
				this.msgBox.open();
	        } else {
	        	this.buttonDownloadPreventivo.setEnabled(false);
	        }
	        
	        //l_VG.MainView.setMemo(ElencoFileXls.get(0));
		} catch (final Exception e) {
	        //l_VG.MainView.setMemo(e.toString());
			if (this.l_VG.debugFlag) {
				Notification.show(e.toString());
			}
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tablePreventivi}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tablePreventivi_valueChange(final Property.ValueChangeEvent event) {
		if (!this.tablePreventivi.isEnabled()) {
			return;
		}
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		try {
			if (this.l_VG.preventivoCurrent != null) {
				//l_VG.MainView.setMemo("store preventivo "+l_VG.preventivoCurrent.NumOfferta);
				storePreventivo();
			}
			final Item item = this.containerPreventivi.getItem(event.getProperty().getValue().toString());
			final int codicePreventivo = (int)item.getItemProperty("Codice").getValue();
			for (int i=0 ; i<this.l_VG.ElencoPreventivi.size() ; i++) {
				if (this.l_VG.ElencoPreventivi.get(i).IDPreventivo == codicePreventivo) {
					this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(i);
					this.l_VG.preventivoCurrentIndex = i;
					break;
				}
			}
			if (this.l_VG.preventivoCurrent.IDCliente == -1) {
				this.l_VG.currentCliente = new Clientev21();
			} else {
				this.l_VG.currentCliente = this.l_VG.dbCRMv2.setCurrentClienteFromCodice(this.l_VG.preventivoCurrent.IDCliente, this.l_VG.ElencoClienti);
			}
			this.l_VG.dbCRMv2.loadElencoVentilatoriPreventivo(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
			for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
				final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
				l_v.ERPCaricato = false;
				this.l_VG.buildDatiERP327ForSellFan(l_v);
				if (l_v.UMAltezza == null) {
					l_v.UMAltezza = (UnitaMisura)this.l_VG.UMAltezzaCorrente.clone();
				}
				if (l_v.UMPortata == null) {
					l_v.UMPortata = (UnitaMisura)this.l_VG.UMPortataCorrente.clone();
				}
				if (l_v.UMPotenza == null) {
					l_v.UMPotenza = (UnitaMisura)this.l_VG.UMPotenzaCorrente.clone();
				}
				if (l_v.UMPressione == null) {
					l_v.UMPressione = (UnitaMisura)this.l_VG.UMPressioneCorrente.clone();
				}
				if (l_v.UMTemperatura == null) {
					l_v.UMTemperatura = (UnitaMisura)this.l_VG.UMTemperaturaCorrente.clone();
				}
			}
			initTabellaVentilatori();
			abilitaBottoni();
			preventiviView.aggiornaPreventivo();
			preventiviView.abilitaTab();
		} catch (final Exception e) {
			this.l_VG.preventivoCurrent = null;
			this.l_VG.currentCliente = null;
			this.l_VG.ElencoVentilatoriFromPreventivo.clear();
			initTabellaVentilatori();
			this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
			abilitaBottoni();
			preventiviView.abilitaTab();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSalvaPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonSalvaPreventivo_buttonClick(final Button.ClickEvent event) {
		if (this.l_VG.preventivoCurrent != null) {
			//l_VG.MainView.setMemo("store preventivo "+l_VG.preventivoCurrent.NumOfferta);
			storePreventivo();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCancellaPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCancellaPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.dbCRMv2.deletePreventivoFan(this.l_VG.preventivoCurrent);
        this.l_VG.ElencoPreventivi.remove(this.l_VG.preventivoCurrentIndex);
        reinitTabellaPreventivi();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonNuovoPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonNuovoPreventivo_buttonClick(final Button.ClickEvent event) {
        if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneNuovaOfferta, this.l_VG.ElencoPreventivi.size())) {
        	this.msgBox = MessageBox.createWarning();
        	this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
        	this.msgBox.withOkButton();
        	this.msgBox.open();
            return;
        }
		final pannelloPreventivoEditNew pannello = new pannelloPreventivoEditNew();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.newMode = true;
		pannello.init();
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withAbortButton();
		msgBox.withOkButton(() -> {
			if (pannello.newMode) {
				this.l_VG.preventivoCurrent = new Preventivo();
				this.l_VG.ElencoVentilatoriFromPreventivo.clear();
			}
			pannello.getValues();
			storePreventivo();
	        final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
	        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
	        final PreventiviView preventivi = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
	        if (pannello.newMode) {
	        	preventivi.reinitPreventivi();
	        	preventivi.setPreventivoSelected(idPreventivo.toString());
	            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneNuovaOfferta, "ID:" + Integer.toString(idPreventivo));
	        } else {
	        	preventivi.aggiornaPreventivo();
	        }
			
			});
		msgBox.open();
	}
	
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonResetPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonResetPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.ElencoVentilatoriFromPreventivo.clear();
        this.l_VG.preventivoCurrent.Ventilatori.clear();
        final PreventiviView preventivoTemp = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();;
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		preventiviView.aggiornaPreventivo();
		storePreventivo();
		initTabellaVentilatori();
		aggiornaPreventivoCorrente();
		abilitaBottoni();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCancellaVentilatore}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCancellaVentilatore_buttonClick(final Button.ClickEvent event) {
        this.l_VG.ElencoVentilatoriFromPreventivo.remove(this.l_VG.VentilatoreCurrentIndex);
        this.l_VG.preventivoCurrent.Ventilatori.remove(this.l_VG.VentilatoreCurrentIndex);
        final PreventiviView preventivoTemp = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();;
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		preventiviView.aggiornaPreventivo();
		storePreventivo();
		initTabellaVentilatori();
		aggiornaPreventivoCorrente();
		abilitaBottoni();
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableVentilatori}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableVentilatori_valueChange(final Property.ValueChangeEvent event) {
		if (!this.tableVentilatori.isEnabled()) {
			return;
		}
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		try {
			storePreventivo();
			this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(event.getProperty().getValue().toString());
			preventiviView.ventilatoreIndex = Integer.parseInt(event.getProperty().getValue().toString());
			preventiviView.aggiornaCosto();
			abilitaBottoni();
		} catch (final Exception e) {
			this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
			abilitaBottoni();
			preventiviView.ventilatoreIndex = -1;
			preventiviView.abilitaTab();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCambiaMotore}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCambiaMotore_buttonClick(final Button.ClickEvent event) {
		final String warningStr = this.l_VG.utilityCliente.getWarning(this.l_VG.VentilatoreCurrent, 0);
		if (warningStr != null) {
			this.l_VG.MainView.setAccessoriVentilatoreCurrent();
			final MessageBox msgBox = MessageBox.create();
			msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(warningStr));
			msgBox.withOkButton(() -> {
				this.l_VG.cambiaManualmenteMotore();
				if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null) {
			        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
			        ventilatorePreventivo.HaMotore = true;
			        ventilatorePreventivo.MotoreSelezionato = true;
			        ventilatorePreventivo.VentilatoreCambiato = true;
					final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
					preventiviView.aggiornaCosto();
			        aggiornaVentilatoreCorrente();
				}
			});
			msgBox.open();
		} else {
			this.l_VG.cambiaManualmenteMotore();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonGeneraPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonGeneraPreventivo_buttonClick(final Button.ClickEvent event) {
		int nv = 0;
		for (int i=0 ; i<this.l_VG.preventivoCurrent.Ventilatori.size() ; i++) {
			if (this.l_VG.preventivoCurrent.Ventilatori.get(i).HaPreventivo) {
				nv++;
			}
		}
		if (nv <= 0) {
			this.msgBox = MessageBox.createInfo();
			this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Nessun ventilatore selezionato"));
			this.msgBox.withOkButton();
			this.msgBox.open();
			return;
		}
        if (this.l_VG.stampaOfferta == null) {
       		this.l_VG.stampaOfferta = new Cliente04StampaOffertaPoiXLSX();
        }
		final Cliente04pannelloPagineOfferta pannello = new Cliente04pannelloPagineOfferta();
		pannello.setVariabiliGlobali(this.l_VG);
		this.msgBox = MessageBox.create();
		this.msgBox.withMessage(pannello);
		this.msgBox.withAbortButton();
		this.msgBox.withOkButton(() -> {
			pannello.getFlag();
	        final Window popup = PopupWindow.For(new ProgressView(this.l_VG.utilityTraduzioni)).closable(false).draggable(false).resizable(false).modal(true).show();
	        UI.getCurrent().push();
			generaPreventivo();
			popup.close();
			});
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonDownloadPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonDownloadPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneDownloadOfferta, "Download Preventivo " + this.memoDownload);
        this.msgBox = MessageBox.create();
		this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Continuare con il preventivo corrente") + "?");
		this.msgBox.withYesButton();
		this.msgBox.withNoButton(() -> {
			buttonNuovoPreventivo_buttonClick(null);
		});
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonChiudiPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonChiudiPreventivo_buttonClick(final Button.ClickEvent event) {
		if (this.l_VG.preventivoCurrent != null) {
			//l_VG.MainView.setMemo("store preventivo "+l_VG.preventivoCurrent.NumOfferta);
			storePreventivo();
			this.l_VG.MainView.resetMainView();
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.panel2 = new XdevPanel();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.tablePreventivi = new XdevTable<>();
		this.panel3 = new XdevPanel();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.buttonChiudiPreventivo = new XdevButton();
		this.buttonSalvaPreventivo = new XdevButton();
		this.buttonCancellaPreventivo = new XdevButton();
		this.buttonNuovoPreventivo = new XdevButton();
		this.panel4 = new XdevPanel();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.tableVentilatori = new XdevTable<>();
		this.panel5 = new XdevPanel();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.buttonResetPreventivo = new XdevButton();
		this.buttonCancellaVentilatore = new XdevButton();
		this.panel7 = new XdevPanel();
		this.horizontalLayout8 = new XdevHorizontalLayout();
		this.buttonCambiaMotore = new XdevButton();
		this.buttonGeneraPreventivo = new XdevButton();
		this.buttonDownloadPreventivo = new XdevButton();
	
		this.verticalLayout.setSpacing(false);
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.panel2.setTabIndex(0);
		this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.tablePreventivi.setStyleName("mystriped");
		this.panel3.setTabIndex(0);
		this.horizontalLayout3.setMargin(new MarginInfo(false, true, false, true));
		this.buttonChiudiPreventivo.setCaption("Chiudi Preventivo");
		this.buttonChiudiPreventivo.setStyleName("big");
		this.buttonSalvaPreventivo.setCaption("Salva Preventivo");
		this.buttonSalvaPreventivo.setStyleName("big");
		this.buttonCancellaPreventivo.setCaption("Cancella Preventivo");
		this.buttonCancellaPreventivo.setStyleName("big");
		this.buttonNuovoPreventivo
				.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/newspaper_add.png"));
		this.buttonNuovoPreventivo.setCaption("Nuovo Preventivo");
		this.buttonNuovoPreventivo.setStyleName("big giallo");
		this.panel4.setTabIndex(0);
		this.horizontalLayout.setMargin(new MarginInfo(false, true, false, false));
		this.tableVentilatori.setStyleName("mystriped");
		this.panel5.setTabIndex(0);
		this.horizontalLayout4.setMargin(new MarginInfo(false, true, false, true));
		this.buttonResetPreventivo.setCaption("Reset Preventivo");
		this.buttonResetPreventivo.setStyleName("small");
		this.buttonCancellaVentilatore
				.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Delete16.png"));
		this.buttonCancellaVentilatore.setCaption("Cancella");
		this.buttonCancellaVentilatore.setStyleName("small");
		this.panel7.setTabIndex(0);
		this.horizontalLayout8.setMargin(new MarginInfo(false, true, false, true));
		this.buttonCambiaMotore
				.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/documents_exchange.png"));
		this.buttonCambiaMotore.setCaption("Cambia Motore");
		this.buttonCambiaMotore.setStyleName("big");
		this.buttonGeneraPreventivo.setCaption("Genera Preventivo");
		this.buttonGeneraPreventivo.setStyleName("big azzurro");
		this.buttonDownloadPreventivo.setCaption("Download Preventivo");
		this.buttonDownloadPreventivo.setStyleName("big");
	
		this.tablePreventivi.setSizeFull();
		this.horizontalLayout2.addComponent(this.tablePreventivi);
		this.horizontalLayout2.setComponentAlignment(this.tablePreventivi, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.tablePreventivi, 100.0F);
		this.horizontalLayout2.setSizeFull();
		this.panel2.setContent(this.horizontalLayout2);
		this.buttonChiudiPreventivo.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.buttonChiudiPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonChiudiPreventivo, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.buttonChiudiPreventivo, 10.0F);
		this.buttonSalvaPreventivo.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.buttonSalvaPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonSalvaPreventivo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.buttonSalvaPreventivo, 10.0F);
		this.buttonCancellaPreventivo.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.buttonCancellaPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonCancellaPreventivo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.buttonCancellaPreventivo, 10.0F);
		this.buttonNuovoPreventivo.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.buttonNuovoPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonNuovoPreventivo, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout3.setExpandRatio(this.buttonNuovoPreventivo, 10.0F);
		this.horizontalLayout3.setSizeFull();
		this.panel3.setContent(this.horizontalLayout3);
		this.tableVentilatori.setSizeFull();
		this.horizontalLayout.addComponent(this.tableVentilatori);
		this.horizontalLayout.setComponentAlignment(this.tableVentilatori, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.tableVentilatori, 100.0F);
		this.horizontalLayout.setSizeFull();
		this.panel4.setContent(this.horizontalLayout);
		this.buttonResetPreventivo.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.buttonResetPreventivo);
		this.horizontalLayout4.setComponentAlignment(this.buttonResetPreventivo, Alignment.MIDDLE_LEFT);
		this.horizontalLayout4.setExpandRatio(this.buttonResetPreventivo, 10.0F);
		this.buttonCancellaVentilatore.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.buttonCancellaVentilatore);
		this.horizontalLayout4.setComponentAlignment(this.buttonCancellaVentilatore, Alignment.MIDDLE_LEFT);
		this.horizontalLayout4.setExpandRatio(this.buttonCancellaVentilatore, 10.0F);
		this.horizontalLayout4.setSizeFull();
		this.panel5.setContent(this.horizontalLayout4);
		this.buttonCambiaMotore.setSizeUndefined();
		this.horizontalLayout8.addComponent(this.buttonCambiaMotore);
		this.horizontalLayout8.setComponentAlignment(this.buttonCambiaMotore, Alignment.MIDDLE_LEFT);
		this.horizontalLayout8.setExpandRatio(this.buttonCambiaMotore, 10.0F);
		this.buttonGeneraPreventivo.setSizeUndefined();
		this.horizontalLayout8.addComponent(this.buttonGeneraPreventivo);
		this.horizontalLayout8.setComponentAlignment(this.buttonGeneraPreventivo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout8.setExpandRatio(this.buttonGeneraPreventivo, 10.0F);
		this.buttonDownloadPreventivo.setSizeUndefined();
		this.horizontalLayout8.addComponent(this.buttonDownloadPreventivo);
		this.horizontalLayout8.setComponentAlignment(this.buttonDownloadPreventivo, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout8.setExpandRatio(this.buttonDownloadPreventivo, 10.0F);
		this.horizontalLayout8.setSizeFull();
		this.panel7.setContent(this.horizontalLayout8);
		this.panel2.setSizeFull();
		this.verticalLayout.addComponent(this.panel2);
		this.verticalLayout.setExpandRatio(this.panel2, 30.0F);
		this.panel3.setSizeFull();
		this.verticalLayout.addComponent(this.panel3);
		this.verticalLayout.setExpandRatio(this.panel3, 10.0F);
		this.panel4.setSizeFull();
		this.verticalLayout.addComponent(this.panel4);
		this.verticalLayout.setExpandRatio(this.panel4, 30.0F);
		this.panel5.setSizeFull();
		this.verticalLayout.addComponent(this.panel5);
		this.verticalLayout.setExpandRatio(this.panel5, 10.0F);
		this.panel7.setSizeFull();
		this.verticalLayout.addComponent(this.panel7);
		this.verticalLayout.setExpandRatio(this.panel7, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.tablePreventivi.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobilePreventiviVentilatori.this.tablePreventivi_valueChange(event);
			}
		});
		this.buttonChiudiPreventivo.addClickListener(event -> this.buttonChiudiPreventivo_buttonClick(event));
		this.buttonSalvaPreventivo.addClickListener(event -> this.buttonSalvaPreventivo_buttonClick(event));
		this.buttonCancellaPreventivo.addClickListener(event -> this.buttonCancellaPreventivo_buttonClick(event));
		this.buttonNuovoPreventivo.addClickListener(event -> this.buttonNuovoPreventivo_buttonClick(event));
		this.tableVentilatori.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MobilePreventiviVentilatori.this.tableVentilatori_valueChange(event);
			}
		});
		this.buttonResetPreventivo.addClickListener(event -> this.buttonResetPreventivo_buttonClick(event));
		this.buttonCancellaVentilatore.addClickListener(event -> this.buttonCancellaVentilatore_buttonClick(event));
		this.buttonCambiaMotore.addClickListener(event -> this.buttonCambiaMotore_buttonClick(event));
		this.buttonGeneraPreventivo.addClickListener(event -> this.buttonGeneraPreventivo_buttonClick(event));
		this.buttonDownloadPreventivo.addClickListener(event -> this.buttonDownloadPreventivo_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonChiudiPreventivo, buttonSalvaPreventivo, buttonCancellaPreventivo, buttonNuovoPreventivo,
			buttonResetPreventivo, buttonCancellaVentilatore, buttonCambiaMotore, buttonGeneraPreventivo,
			buttonDownloadPreventivo;
	private XdevHorizontalLayout horizontalLayout2, horizontalLayout3, horizontalLayout, horizontalLayout4,
			horizontalLayout8;
	private XdevTable<CustomComponent> tablePreventivi, tableVentilatori;
	private XdevPanel panel2, panel3, panel4, panel5, panel7;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>

}
