package com.cit.sellfan.ui.view;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.template.jGraficoXYWeb;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevSlider;
import com.xdev.ui.XdevTextArea;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.Rumore;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.personalizzazioni.cliente04.Cliente04Rumore;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;

public class SpettroSonoroView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
	private Cliente04Rumore rumore = null;
    private VentilatoreFisica l_vf;
	private Cliente04VentilatoreCampiCliente l_vcc;
	private UtilityCliente l_uc;
	private boolean editEnabled = false;
	public jGraficoXYWeb graficoPressione = new jGraficoXYWeb();
	public jGraficoXYWeb graficoPotenza = new jGraficoXYWeb();
	public jGraficoXYWeb graficoCondotto = new jGraficoXYWeb();
	
	/**
	 * 
	 */
	public SpettroSonoroView() {
		super();
		this.initUI();
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoView[this.l_VG.SpettroSonoroViewIndex] = this;
        this.l_uc = this.l_VG.utilityCliente;
        this.rumore = new Cliente04Rumore(this.l_VG.utilityCliente.ConnCliente, this.l_VG.dbTableQualifier, "mz");
	}
	
	public void Nazionalizza() {
        this.labelHT.setValue(StringheUNI.PRESSIONETOTALE);
        this.checkBoxNessunaA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_uc.installazioneDesc[0]));
        this.checkBoxMandataB.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_uc.installazioneDesc[1]));
        this.checkBoxAspirazioneC.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_uc.installazioneDesc[2]));
        this.checkBoxEntrambeD.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_uc.installazioneDesc[3]));
        this.panelPotenza.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Sonora"));
        this.checkBoxTenuta.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Tenuta"));
        this.checkBoxGasCaldi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Gas Caldi"));
        this.panelPressione.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora"));
        this.labelPropagazione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Propagazione"));
        this.labelDistanza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Distanza"));
        this.panelInstallazione.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora"));
        this.labelPressioneInstallazione.setValue("<b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora in condizioni di installazione"));
        
        this.checkBoxCanalizzazioneIsolate.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.utilityCliente.getDescrizioneCanalizzazioni(0)));
        this.checkBoxCanalizzazioneAlta.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.utilityCliente.getDescrizioneCanalizzazioni(1)));
        this.checkBoxCanalizzazioneMedia.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.utilityCliente.getDescrizioneCanalizzazioni(2)));
        this.checkBoxCanalizzazioneBassa.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.utilityCliente.getDescrizioneCanalizzazioni(3)));
        
        this.labelAmbiente.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Ambiente"));
        this.checkBoxCampoLibero.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Campo libero"));
        this.checkBoxRiverberante.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ambiente"));
        this.labelRumoreFondo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Rumore di fondo"));
        this.textAreaNota2.setReadOnly(false);
        this.textAreaNota.setReadOnly(false);
        this.textAreaNota2.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Valore medio puramente indicativo, ottenuto tramite coefficienti sperimentali."));
        String str = this.l_VG.utilityTraduzioni.TraduciStringa("Dati di pressione sonora calcolati. Utilizzare solo come riferimento.");
        str += "\n\n" + this.l_VG.utilityTraduzioni.TraduciStringa("Eventuali accessori possono modificare i valori di rumorosità.");
        this.textAreaNota.setValue(str);
        this.textAreaNota2.setReadOnly(true);
        this.textAreaNota.setReadOnly(true);
        if (this.l_VG.VentilatoreCurrent != null) {
			this.labelEsecuzioneDesc.setValue(this.l_VG.getDescrizioneEsecuzione(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione));
			final DecimalFormat df = new DecimalFormat("#.##");
			df.setRoundingMode(RoundingMode.HALF_DOWN);
			this.labelDiamGirante.setValue("<b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Diametro Girante") + ": </b>" + df.format(this.l_VG.VentilatoreCurrent.DiametroGirante) + " [m]");
		}
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		if (this.l_VG == null) {
			return false;
		}
		if (this.l_VG.VentilatoreCurrent == null || this.l_VG.utilityCliente == null || !this.l_VG.utilityCliente.isTabVisible("SpettroSonoro")) {
			return false;
		}
		return true;
	}
	
	public void setVentilatoreCurrent() {
		this.isPannelloInizializzato = false;
		this.editEnabled = false;
		this.l_VG.MainView.aggiornaGrafico();
        this.l_vcc = (Cliente04VentilatoreCampiCliente)this.l_VG.VentilatoreCurrent.campiCliente;
        this.labelModello.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent)));
        this.labelEsecuzione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione") + " " + this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
        this.labelEsecuzioneDesc.setValue(this.l_VG.getDescrizioneEsecuzione(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione));
        final DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_DOWN);
		this.labelDiamGirante.setValue("<b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Diametro Girante") + ": </b>" + df.format(this.l_VG.VentilatoreCurrent.DiametroGirante) + " [m]");
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
        this.labelQVal.setValue("<b>Q: </b>" + this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromm3hToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, this.l_VG.VentilatoreCurrent.UMPortata)) + "   " + this.l_VG.VentilatoreCurrent.UMPortata.simbolo);
        //this.labelQUM.setValue(this.l_VG.VentilatoreCurrent.UMPortata.simbolo);
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPressione.nDecimaliStampa);
        this.labelHTVal.setValue("<b>" + StringheUNI.PRESSIONETOTALE + ": </b>" + this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale, this.l_VG.VentilatoreCurrent.UMPressione)) + "   " + this.l_VG.VentilatoreCurrent.UMPressione.simbolo);
        //this.labelHTUM.setValue(this.l_VG.VentilatoreCurrent.UMPressione.simbolo);
        this.labelRpmVal.setValue("<b>RPM: </b>" + Integer.toString((int)this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri) + "    [RPM]");
        this.l_VG.fmtNd.setMaximumFractionDigits(3);
        this.labelDensita.setValue("<b>ρ<sub>Fluid</sub>: </b>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.CAProgettazione.rho) + "   [kg/m<sup>3</sup>]");
        fillFlagFromCampiCliente();
        drawIstogrammi();
        resizeView();
        this.editEnabled = true;
        this.isPannelloInizializzato = true;
/*
		StreamResource graficosource = new StreamResource(l_VG.PrestazioniView.grafico, "myimage" + Integer.toString(l_VG.progressivoGenerico++) + ".png");
		graficosource.setCacheTime(100);
		imagePressione.setSource(graficosource);
		imagePotenza.setSource(graficosource);
		imageCondotto.setSource(graficosource);
*/
	}
	
	public void resizeView() {
		if (this.l_VG.browserAspectRatio < 1.43) {			//default
			this.imagePotenzaAlternativa.setVisible(false);
			this.imagePressioneAlternativa.setVisible(false);
			this.imagePotenzaDefault.setVisible(true);
			this.imagePressioneDefault.setVisible(true);
		} else {
			this.imagePotenzaDefault.setVisible(false);
			this.imagePressioneDefault.setVisible(false);
			this.imagePotenzaAlternativa.setVisible(true);
			this.imagePressioneAlternativa.setVisible(true);
		}
	}
	
	public void drawIstogrammi() {
		this.graficoPressione.ResetGrafico();
		this.graficoPotenza.ResetGrafico();
		this.graficoCondotto.ResetGrafico();
        try {
            if (this.l_VG.sfondoGraficiSpettro != null && this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag) {
                final InputStream insfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                this.graficoPotenza.setImgSfondo(insfondo);
                final InputStream insfondo1 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                this.graficoPressione.setImgSfondo(insfondo1);
                final InputStream insfondo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                this.graficoCondotto.setImgSfondo(insfondo2);
            } else {
            	this.graficoPressione.setImgSfondo(null);
            	this.graficoPotenza.setImgSfondo(null);
            	this.graficoCondotto.setImgSfondo(null);
            }
        } catch (final Exception e) {
        	
        }
		this.graficoPressione.setTipoIstogramma();
		this.graficoPotenza.setTipoIstogramma();
		this.graficoCondotto.setTipoIstogramma();
//inizializzazione
        if (this.l_VG.VentilatoreCurrent.selezioneCorrente.nBoccheCanalizzate < 0) {
            this.l_VG.VentilatoreCurrent.selezioneCorrente.nBoccheCanalizzate = this.l_VG.VentilatoreCurrent.NBoccheCanalizzateBase;
        }
        if (this.l_VG.VentilatoreCurrent.selezioneCorrente.distanzaSpettroSonorom < 0.) {
            this.l_VG.VentilatoreCurrent.selezioneCorrente.distanzaSpettroSonorom = this.l_VG.VentilatoreCurrent.DefautDistanzaSpettro;
        }
        this.l_vf = new VentilatoreFisica();
        this.l_vf.setVentilatoreFisica(this.l_VG.conTecnica, this.l_VG.dbTableQualifier.dbTableQualifierPreDBTecnico, this.l_VG.dbTableQualifier.dbTableQualifierPostDBTecnico, this.l_VG.VentilatoreCurrent, this.l_VG.ventilatoreFisicaParameter);
        try {
            this.l_vf.setNumeroGiriPortatam3h(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri, this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker);
            if (!this.l_vf.havePotenzaSonora()) {
				return;
			}
            if (!setSpettroSonoroBase(this.l_VG.VentilatoreCurrent, this.rumore)) {
				return;
			}
        } catch (final Exception e) {
        	return;
        }
        if ((this.rumore.RumoreBase <= 0.) || (this.rumore.RumoreBaseA <= 0.)) {
            return;
        }
        loadCampiCliente();
		if (!this.rumore.calcolaSpettroCorrenteRumore(this.l_VG.VentilatoreCurrent, this.l_vf, this.l_VG.ventilatoreFisicaParameter)) {
			return;
		}
        this.rumore.titoliBarreSpettroDefault[10] = this.l_VG.utilityTraduzioni.TraduciStringa("Totale");
		try {
            final String titoliBarre[] = this.l_uc.paccaTitoliSpettroSonoro(this.rumore.titoliBarreSpettroDefault);
            final String titoloX = this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza");
            String titoloY = this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Sonora");
            if (this.checkBoxdBA.getValue()) {
                displayIstogrammaSpettroSonoro(this.graficoPotenza, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorrettoA), "[dB(A)]", titoloX, titoloY);
            } else {
                displayIstogrammaSpettroSonoro(this.graficoPotenza, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorretto), "[dB]", titoloX, titoloY);
            }
            this.l_vcc.potenzadB = this.rumore.SpettroCorrente[10];
            this.l_vcc.potenzadBA = this.rumore.SpettroCorrenteA[10];
            titoloY = this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora");
            if (this.checkBoxdBA.getValue()) {
                displayIstogrammaSpettroSonoro(this.graficoPressione, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorrentePressioneA), "[dB(A)]", titoloX, titoloY);
            } else {
                displayIstogrammaSpettroSonoro(this.graficoPressione, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorrentePressione), "[dB]", titoloX, titoloY);
            }
            this.l_vcc.pressionedB = this.rumore.SpettroCorrentePressione[10];
            this.l_vcc.pressionedBA = this.rumore.SpettroCorrentePressioneA[10];
            titoloY = this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Sonora Immessa in un Condotto");
            if (this.checkBoxdBA.getValue()) {
                displayIstogrammaSpettroSonoro(this.graficoCondotto, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.spettroPotenzaInCanaliA), "[dB(A)]", titoloX, titoloY);
            } else {
                displayIstogrammaSpettroSonoro(this.graficoCondotto, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.spettroPotenzaInCanali), "[dB]", titoloX, titoloY);
            }
            displayPressioneCorretta();
    		final StreamResource graficopotenzasource = new StreamResource(this.graficoPotenza, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
    		graficopotenzasource.setCacheTime(100);
    		this.imagePotenzaDefault.setSource(graficopotenzasource);
    		this.imagePotenzaAlternativa.setSource(graficopotenzasource);
    		final StreamResource graficopressionesource = new StreamResource(this.graficoPressione, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
    		graficopressionesource.setCacheTime(100);
    		this.imagePressioneDefault.setSource(graficopressionesource);
    		this.imagePressioneAlternativa.setSource(graficopressionesource);
    		final StreamResource graficocondottosource = new StreamResource(this.graficoCondotto, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
    		graficocondottosource.setCacheTime(100);
    		this.imageCondotto.setSource(graficocondottosource);
		} catch (final Exception e) {
			
		}
        this.textFieldDistanza.setEnabled(false);
        this.textFieldDistanza.setValue(this.sliderDistanza.getValue());
        this.textFieldDistanza.setEnabled(true);
	}
	
	private void loadCampiCliente() {
        this.l_vcc.dBA = this.checkBoxdBA.getValue();
        this.l_vcc.tenutaFlag = this.checkBoxTenuta.isVisible() && this.checkBoxTenuta.getValue();
        this.l_vcc.gasCaldiFlag = this.checkBoxGasCaldi.isVisible() && this.checkBoxGasCaldi.getValue();
        this.l_vcc.aspirazioneCanalizzataFlag = false;
        this.l_vcc.mandataCanalizzataFlag = false;
        if (this.checkBoxAspirazioneC.getValue()) {
            this.l_vcc.aspirazioneCanalizzataFlag = true;
        } else if (this.checkBoxMandataB.getValue()) {
            this.l_vcc.mandataCanalizzataFlag = true;
        } else if (this.checkBoxEntrambeD.getValue()) {
            this.l_vcc.aspirazioneCanalizzataFlag = true;
            this.l_vcc.mandataCanalizzataFlag = true;
        }
        this.l_vcc.propagazione12sfFlag = this.checkBox12sf.getValue();
        this.l_vcc.propagazione14sfFlag = this.checkBox14sf.getValue();
        this.l_vcc.propagazione18sfFlag = this.checkBox18sf.getValue();
        this.l_vcc.distanzam = this.l_VG.VentilatoreCurrent.selezioneCorrente.distanzaSpettroSonorom;
        this.l_vcc.canalizzazioneIsolateFlag = this.checkBoxCanalizzazioneIsolate.getValue();
        this.l_vcc.canalizzazioneAltaFlag = this.checkBoxCanalizzazioneAlta.getValue() && this.checkBoxCanalizzazioneAlta.isVisible();
        this.l_vcc.canalizzazioneMediaFlag = this.checkBoxCanalizzazioneMedia.getValue() && this.checkBoxCanalizzazioneMedia.isVisible();
        this.l_vcc.canalizzazioneBassaFlag = this.checkBoxCanalizzazioneBassa.getValue() && this.checkBoxCanalizzazioneBassa.isVisible();
        this.l_vcc.campoLiberoFlag = this.checkBoxCampoLibero.getValue();
        this.l_vcc.riverberoFlag = this.checkBoxRiverberante.getValue();
        this.l_vcc.riverbero = this.textFieldRiverbero.getIntegerValue();
        this.l_vcc.rumoreFondo = this.textFieldRumoreFondo.getIntegerValue();
	}
	
	private void displayPressioneCorretta() {
        loadCampiCliente();
		double pressioneCorretta = 0.;
		String um;
		if (this.checkBoxdBA.getValue()) {
            pressioneCorretta = Math.pow(10., 0.1 * this.rumore.pressioneSonoraInstallazioneA);
		} else {
            pressioneCorretta = Math.pow(10., 0.1 * this.rumore.pressioneSonoraInstallazione);
		}
        pressioneCorretta += Math.pow(10., 0.1 * this.textFieldRumoreFondo.getIntegerValue());
        pressioneCorretta = 10. * Math.log10(pressioneCorretta);
        if (this.checkBoxRiverberante.getValue()) {
        	pressioneCorretta += this.textFieldRiverbero.getIntegerValue();
        }
        this.l_VG.fmtNd.setMaximumFractionDigits(1);
        this.labelPressioneCorretta.setValue("<html><b><font color='blue'>" + this.l_VG.fmtNd.format(pressioneCorretta) + "</html>");
        if (this.checkBoxdBA.getValue()) {
            um = " ±3 [dB(A)]";
            this.labelCanalizzazioneIsolate.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneAIsolate) + um);
            this.labelCanalizzazioneAlta.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneAAlta) + um);
            this.labelCanalizzazioneMedia.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneAMedia) + um);
            this.labelCanalizzazioneBassa.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneABassa) + um);
        } else {
            um = " ±3 [dB]";
            this.labelCanalizzazioneIsolate.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneIsolate) + um);
            this.labelCanalizzazioneAlta.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneAlta) + um);
            this.labelCanalizzazioneMedia.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneMedia) + um);
            this.labelCanalizzazioneBassa.setValue(this.l_VG.fmtNd.format(this.rumore.pressioneSonoraInstallazioneBassa) + um);
        }
	}
	
	private void fillFlagFromCampiCliente() {
		Accessorio l_a;
        this.checkBoxdB.setEnabled(false);
        this.checkBoxdBA.setEnabled(false);
        if (this.l_vcc.dBA) {
        	this.checkBoxdBA.setValue(true);
        	this.checkBoxdB.setValue(false);
        	this.labelRiverberoUM.setValue("[dB(A)]");
        	this.labelFondoUM.setValue("[dB(A)]");
        	this.labelPressioneCorrettaUM.setValue("±3 [dB(A)]");
        } else {
        	this.checkBoxdBA.setValue(false);
        	this.checkBoxdB.setValue(true);
        	this.labelRiverberoUM.setValue("[dB]");
        	this.labelFondoUM.setValue("[dB]");
        	this.labelPressioneCorrettaUM.setValue("±3 [dB]");
        }
        this.checkBoxdB.setEnabled(true);
        this.checkBoxdBA.setEnabled(true);
        this.checkBox12sf.setEnabled(false);
        this.checkBox14sf.setEnabled(false);
        this.checkBox18sf.setEnabled(false);
        this.checkBox12sf.setValue(this.l_vcc.propagazione12sfFlag);
        this.checkBox14sf.setValue(this.l_vcc.propagazione14sfFlag);
        this.checkBox18sf.setValue(this.l_vcc.propagazione18sfFlag);
        this.checkBox12sf.setEnabled(true);
        this.checkBox14sf.setEnabled(true);
        this.checkBox18sf.setEnabled(true);
        this.labelDistanzaUM.setValue("[m]");
        this.sliderDistanza.setEnabled(false);
        this.sliderDistanza.setResolution(1);
        this.sliderDistanza.setMin(1.);
        this.sliderDistanza.setMax(50.);
        this.sliderDistanza.setValue(this.l_vcc.distanzam);
        this.sliderDistanza.setEnabled(true);
        this.textFieldDistanza.setEnabled(false);
        this.textFieldDistanza.setValue(this.l_vcc.distanzam);
        this.textFieldDistanza.setEnabled(true);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
        //labelDistanzaVal.setValue(l_VG.fmtNd.format(l_vcc.distanzam));
        
        this.checkBoxCampoLibero.setEnabled(false);
        this.checkBoxCampoLibero.setValue(this.l_vcc.campoLiberoFlag);
        this.checkBoxCampoLibero.setEnabled(true);
        this.checkBoxRiverberante.setEnabled(true);
        this.checkBoxRiverberante.setValue(this.l_vcc.riverberoFlag);
        this.checkBoxRiverberante.setEnabled(true);
        this.textFieldRiverbero.setEnabled(false);
        if (this.l_vcc.riverbero > 0.) {
        	this.textFieldRiverbero.setValue(this.l_vcc.riverbero);
        } else {
        	this.textFieldRiverbero.setValue("");
        }
        this.textFieldRiverbero.setEnabled(true);
        this.textFieldRiverbero.setVisible(this.l_vcc.riverberoFlag);
        this.labelRiverberoUM.setVisible(this.l_vcc.riverberoFlag);
        this.textFieldRumoreFondo.setEnabled(false);
        if (this.l_vcc.rumoreFondo > 0.) {
        	this.textFieldRumoreFondo.setValue(this.l_vcc.rumoreFondo);
        } else {
        	this.textFieldRumoreFondo.setValue("");
        }
        this.textFieldRumoreFondo.setEnabled(true);
        this.checkBoxTenuta.setVisible(false);
        this.checkBoxTenuta.setValue(false);
        for (int i=0 ; i<this.l_uc.gestioneVentilatori.elencoTenute.length ; i++) {
            l_a = this.l_VG.gestioneAccessori.getAccessorio(this.l_VG.VentilatoreCurrent, this.l_uc.gestioneVentilatori.elencoTenute[i]);
            if (l_a != null) {
            	this.checkBoxTenuta.setVisible(true);
                if (this.l_VG.gestioneAccessori.isAccessorioSelected(this.l_VG.VentilatoreCurrent, this.l_uc.gestioneVentilatori.elencoTenute[i])) {
                	this.checkBoxTenuta.setValue(true);
                    break;
                }
            }
        }
        this.checkBoxGasCaldi.setVisible(false);
        this.checkBoxGasCaldi.setValue(false);
        l_a = this.l_VG.gestioneAccessori.getAccessorio(this.l_VG.VentilatoreCurrent, "GC");
        if (l_a != null) {
        	this.checkBoxGasCaldi.setVisible(true);
        	this.checkBoxGasCaldi.setValue(this.l_VG.gestioneAccessori.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "GC"));
        }
        
        
        this.checkBoxCanalizzazioneIsolate.setValue(this.l_vcc.canalizzazioneIsolateFlag);
        this.checkBoxCanalizzazioneAlta.setValue(this.l_vcc.canalizzazioneAltaFlag);
        this.checkBoxCanalizzazioneMedia.setValue(this.l_vcc.canalizzazioneMediaFlag);
        this.checkBoxCanalizzazioneBassa.setValue(this.l_vcc.canalizzazioneBassaFlag);
        this.checkBoxNessunaA.setValue(false);
        this.checkBoxMandataB.setValue(false);
        this.checkBoxAspirazioneC.setValue(false);
        this.checkBoxEntrambeD.setValue(false);
        if (this.l_vcc.TipoInstallazione.equals("A")) {
        	this.checkBoxCanalizzazioneIsolate.setVisible(false);
        	this.checkBoxCanalizzazioneAlta.setVisible(false);
        	this.checkBoxCanalizzazioneMedia.setVisible(false);
        	this.checkBoxCanalizzazioneBassa.setVisible(false);
        	this.labelCanalizzazioneIsolate.setVisible(false);
        	this.labelCanalizzazioneAlta.setVisible(false);
        	this.labelCanalizzazioneMedia.setVisible(false);
        	this.labelCanalizzazioneBassa.setVisible(false);
        	this.checkBoxNessunaA.setValue(true);
        } else {
        	this.checkBoxCanalizzazioneIsolate.setVisible(true);
        	this.checkBoxCanalizzazioneAlta.setVisible(true);
        	this.checkBoxCanalizzazioneMedia.setVisible(true);
        	this.checkBoxCanalizzazioneBassa.setVisible(true);
        	this.labelCanalizzazioneIsolate.setVisible(true);
        	this.labelCanalizzazioneAlta.setVisible(true);
        	this.labelCanalizzazioneMedia.setVisible(true);
        	this.labelCanalizzazioneBassa.setVisible(true);
        	if (this.l_vcc.TipoInstallazione.equals("B")) {
				this.checkBoxMandataB.setValue(true);
			} else if (this.l_vcc.TipoInstallazione.equals("C")) {
				this.checkBoxAspirazioneC.setValue(true);
			} else if (this.l_vcc.TipoInstallazione.equals("D")) {
				this.checkBoxEntrambeD.setValue(true);
			}
        }
/*
        labelCanalizzazioneAlta.setVisible(!l_vcc.canalizzazioneIsolateFlag);
        labelCanalizzazioneMedia.setVisible(!l_vcc.canalizzazioneIsolateFlag);
        labelCanalizzazioneBassa.setVisible(!l_vcc.canalizzazioneIsolateFlag);
        
        checkBoxCanalizzazioneIsolate.setEnabled(false);
        checkBoxCanalizzazioneAlta.setEnabled(false);
        checkBoxCanalizzazioneMedia.setEnabled(false);
        checkBoxCanalizzazioneBassa.setEnabled(false);
        labelCanalizzazioneIsolate.setVisible(true);
        checkBoxCanalizzazioneIsolate.setVisible(true);
    	checkBoxNessunaA.setValue(false);
    	checkBoxMandataB.setValue(false);
    	checkBoxAspirazioneC.setValue(false);
    	checkBoxEntrambeD.setValue(false);
        if (l_vcc.TipoInstallazione.equals("A")) {
        	checkBoxNessunaA.setValue(true);
        	labelCanalizzazioneIsolate.setVisible(false);
        	checkBoxCanalizzazioneIsolate.setVisible(false);
        } else if (l_vcc.TipoInstallazione.equals("B")) checkBoxMandataB.setValue(true);
        else if (l_vcc.TipoInstallazione.equals("C")) checkBoxAspirazioneC.setValue(true);
        else if (l_vcc.TipoInstallazione.equals("D")) checkBoxEntrambeD.setValue(true);
        checkBoxCanalizzazioneIsolate.setEnabled(true);
        checkBoxCanalizzazioneAlta.setEnabled(true);
        checkBoxCanalizzazioneMedia.setEnabled(true);
        checkBoxCanalizzazioneBassa.setEnabled(true);
*/
	}
	
    private boolean setSpettroSonoroBase(final Ventilatore l_v, final Rumore rumore) {
        rumore.RumoreBase = 0.;
        rumore.RumoreBaseA = 0.;
        rumore.distanzaPressioneBase = l_v.DefautDistanzaSpettro;
        rumore.deltaPotenzaPressioneSonora = l_v.DeltaPotPressSonora;
        final int index = this.l_VG.ventilatoreFisicaParameter.getIndexSpettroSonoroSemplificato(l_v.CategoriaSpettro);
        if (index < 0) {
			return false;
		}
        System.arraycopy(this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).Valore, 0, rumore.SpettroBase, 0, rumore.SpettroBase.length);
        rumore.BFIm1 = this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).BFIm1;
        rumore.BFI = this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).BFI;
        rumore.BFIp1 = this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).BFIp1;

        for (int i=0 ; i<rumore.SpettroBase.length ; i++) {
            rumore.SpettroBaseA[i] = rumore.SpettroBase[i] + this.l_VG.ventilatoreFisicaParameter.AScale[i];
        }
        rumore.RumoreBase = this.l_VG.utilityMath.RisultanteDaIstrogramma(rumore.SpettroBase);
        rumore.RumoreBaseA = this.l_VG.utilityMath.RisultanteDaIstrogramma(rumore.SpettroBaseA);
        return true;
    }

    private void displayIstogrammaSpettroSonoro(final jGraficoXYWeb grafico, final String titoliBarre[], final double spettro[], final String UM, final String titoloX, final String titoloY) {
        displayIstogrammaSpettroSonoro(grafico, titoliBarre, spettro, -1, UM, titoloX, titoloY);
    }

    private boolean displayIstogrammaSpettroSonoro(final jGraficoXYWeb grafico,final String titoliBarre[], final double spettro[], final int lenSpettro, final String UM, final String titoloX, String titoloY) {
    	try {
            grafico.addAsseXIstogramma(titoliBarre, lenSpettro, titoloX + " [Hz]");
            titoloY += " ±3";
            int istoMax = -100000;
            for (int i=0 ; i<spettro.length ; i++) {
                if (istoMax < (int)(spettro[i] + 20.) / 10) {
                    istoMax = (int)(spettro[i] + 20.) / 10;
                }
            }
            if (istoMax <= 0) {
				return false;
			}
            grafico.addAsseYLineare(0., istoMax * 10., UM);
            titoloY += " " + UM;
            final int index = grafico.addIstogrammaRumore(spettro, lenSpettro, Color.YELLOW, Color.GREEN, true);
            grafico.setIstogrammaDecimali(index, 1);
            grafico.setTitoloGrafico(titoloY);
            grafico.repaint();
    		return true;
    	} catch (final Exception e) {
    		return false;
    	}
    }
/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxdB}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxdB_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.checkBoxdBA.setEnabled(false);
		this.checkBoxdBA.setValue(!this.checkBoxdB.getValue());
		this.checkBoxdBA.setEnabled(true);
    	this.labelRiverberoUM.setValue("[dB]");
    	this.labelFondoUM.setValue("[dB]");
    	this.labelPressioneCorrettaUM.setValue("±3 [dB]");
    	drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxdBA}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxdBA_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.checkBoxdB.setEnabled(false);
		this.checkBoxdB.setValue(!this.checkBoxdBA.getValue());
		this.checkBoxdB.setEnabled(true);
    	this.labelRiverberoUM.setValue("[dB(A)]");
    	this.labelFondoUM.setValue("[dB(A)]");
    	this.labelPressioneCorrettaUM.setValue("±3 [dB(A)]");
    	drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBox12sf}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBox12sf_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		final boolean value = this.checkBox12sf.getValue();
		if (value) {
			this.editEnabled = false;
			this.checkBox14sf.setValue(false);
			this.checkBox18sf.setValue(false);
			this.editEnabled = true;
		}
		drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBox14sf}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBox14sf_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		final boolean value = this.checkBox14sf.getValue();
		if (value) {
			this.editEnabled = false;
			this.checkBox12sf.setValue(false);
			this.checkBox18sf.setValue(false);
			this.editEnabled = true;
		}
		drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBox18sf}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBox18sf_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		final boolean value = this.checkBox18sf.getValue();
		if (value) {
			this.editEnabled = false;
			this.checkBox12sf.setValue(false);
			this.checkBox14sf.setValue(false);
			this.editEnabled = true;
		}
		drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevSlider}
	 * {@link #sliderDistanza}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void sliderDistanza_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
        this.l_VG.VentilatoreCurrent.selezioneCorrente.distanzaSpettroSonorom = this.sliderDistanza.getValue();
        //l_VG.fmtNd.setMaximumFractionDigits(2);
        //labelDistanzaVal.setValue(l_VG.fmtNd.format(l_VG.VentilatoreCurrent.selezioneCorrente.distanzaSpettroSonorom));
        drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCampoLibero}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCampoLibero_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.checkBoxRiverberante.setEnabled(false);
		this.textFieldRiverbero.setEnabled(false);
		if (this.checkBoxCampoLibero.getValue()) {
			this.textFieldRiverbero.setVisible(false);
			this.labelRiverberoUM.setVisible(false);
			this.textFieldRiverbero.setValue("");
			this.checkBoxRiverberante.setValue(false);
		} else {
			this.textFieldRiverbero.setVisible(true);
			this.labelRiverberoUM.setVisible(true);
			this.checkBoxRiverberante.setValue(true);
		}
		this.checkBoxRiverberante.setEnabled(true);
		this.textFieldRiverbero.setEnabled(true);
		displayPressioneCorretta();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxRiverberante}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxRiverberante_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.checkBoxCampoLibero.setEnabled(false);
		if (this.checkBoxRiverberante.getValue()) {
			this.textFieldRiverbero.setVisible(true);
			this.labelRiverberoUM.setVisible(true);
			this.checkBoxCampoLibero.setValue(false);
		} else {
			this.textFieldRiverbero.setVisible(false);
			this.labelRiverberoUM.setVisible(false);
		}
		this.checkBoxCampoLibero.setEnabled(true);
		displayPressioneCorretta();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCanalizzazioneIsolate}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCanalizzazioneIsolate_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		final boolean value = this.checkBoxCanalizzazioneIsolate.getValue();
		if (value) {
			this.editEnabled = false;
			this.checkBoxCanalizzazioneAlta.setValue(false);
			this.checkBoxCanalizzazioneMedia.setValue(false);
			this.checkBoxCanalizzazioneBassa.setValue(false);
			this.editEnabled = true;
		}
		drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCanalizzazioneAlta}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCanalizzazioneAlta_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		final boolean value = this.checkBoxCanalizzazioneAlta.getValue();
		if (value) {
			this.editEnabled = false;
			this.checkBoxCanalizzazioneIsolate.setValue(false);
			this.checkBoxCanalizzazioneMedia.setValue(false);
			this.checkBoxCanalizzazioneBassa.setValue(false);
			this.editEnabled = true;
		}
		this.editEnabled = true;
		drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCanalizzazioneMedia}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCanalizzazioneMedia_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		final boolean value = this.checkBoxCanalizzazioneMedia.getValue();
		if (value) {
			this.editEnabled = false;
			this.checkBoxCanalizzazioneIsolate.setValue(false);
			this.checkBoxCanalizzazioneAlta.setValue(false);
			this.checkBoxCanalizzazioneBassa.setValue(false);
			this.editEnabled = true;
		}
		this.editEnabled = true;
		drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCanalizzazioneBassa}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCanalizzazioneBassa_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		final boolean value = this.checkBoxCanalizzazioneBassa.getValue();
		if (value) {
			this.editEnabled = false;
			this.checkBoxCanalizzazioneIsolate.setValue(false);
			this.checkBoxCanalizzazioneMedia.setValue(false);
			this.checkBoxCanalizzazioneAlta.setValue(false);
			this.editEnabled = true;
		}
		this.editEnabled = true;
		drawIstogrammi();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldDistanza}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldDistanza_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		final double newValue = this.textFieldDistanza.getDoubleValue();
		if (newValue < 1. || newValue > 50.) {
			return;
		}
		this.sliderDistanza.setValue(newValue);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonStepDistanzaMeno}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonStepDistanzaMeno_buttonClick(final Button.ClickEvent event) {
		if (!this.editEnabled) {
			return;
		}
		final double newValue = this.sliderDistanza.getValue() - this.l_VG.ParametriVari.stepDistanzaRumorem;
		if (newValue < 1.) {
			return;
		}
		this.sliderDistanza.setValue(newValue);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonStepDistanzaPiu}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonStepDistanzaPiu_buttonClick(final Button.ClickEvent event) {
		if (!this.editEnabled) {
			return;
		}
		final double newValue = this.sliderDistanza.getValue() + this.l_VG.ParametriVari.stepDistanzaRumorem;
		if (newValue > 50.) {
			return;
		}
		this.sliderDistanza.setValue(newValue);
	}

/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldRiverbero}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldRiverbero_textChange(final FieldEvents.TextChangeEvent event) {
		displayPressioneCorretta();
	}

/**
 * Event handler delegate method for the {@link TextFieldDouble}
 * {@link #textFieldRumoreFondo}.
 *
 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
 * @eventHandlerDelegate Do NOT delete, used by UI designer!
 */
private void textFieldRumoreFondo_textChange(final FieldEvents.TextChangeEvent event) {
	displayPressioneCorretta();
}

/*
 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
 * the UI designer.
 */
// <generated-code name="initUI">
private void initUI() {
	this.horizontalLayout = new XdevHorizontalLayout();
	this.verticalLayout = new XdevVerticalLayout();
	this.verticalLayout4 = new XdevVerticalLayout();
	this.labelModello = new XdevLabel();
	this.labelEsecuzione = new XdevLabel();
	this.labelEsecuzioneDesc = new XdevLabel();
	this.labelDiamGirante = new XdevLabel();
	this.labelDiamGiranteVal = new XdevLabel();
	this.labelQ = new XdevLabel();
	this.labelQVal = new XdevLabel();
	this.labelQUM = new XdevLabel();
	this.labelHT = new XdevLabel();
	this.labelHTVal = new XdevLabel();
	this.labelHTUM = new XdevLabel();
	this.label8 = new XdevLabel();
	this.labelRpmVal = new XdevLabel();
	this.label14 = new XdevLabel();
	this.labelDensita = new XdevLabel();
	this.labelrhoVal = new XdevLabel();
	this.label15 = new XdevLabel();
	this.panelPotenza = new XdevPanel();
	this.verticalLayout5 = new XdevVerticalLayout();
	this.horizontalLayout2 = new XdevHorizontalLayout();
	this.checkBoxdB = new XdevCheckBox();
	this.checkBoxdBA = new XdevCheckBox();
	this.checkBoxTenuta = new XdevCheckBox();
	this.checkBoxGasCaldi = new XdevCheckBox();
	this.checkBoxNessunaA = new XdevCheckBox();
	this.checkBoxMandataB = new XdevCheckBox();
	this.checkBoxAspirazioneC = new XdevCheckBox();
	this.checkBoxEntrambeD = new XdevCheckBox();
	this.panelPressione = new XdevPanel();
	this.verticalLayout6 = new XdevVerticalLayout();
	this.labelPropagazione = new XdevLabel();
	this.horizontalLayout3 = new XdevHorizontalLayout();
	this.checkBox12sf = new XdevCheckBox();
	this.checkBox14sf = new XdevCheckBox();
	this.checkBox18sf = new XdevCheckBox();
	this.horizontalLayout4 = new XdevHorizontalLayout();
	this.labelDistanza = new XdevLabel();
	this.labelDistanzaUM = new XdevLabel();
	this.horizontalLayout5 = new XdevHorizontalLayout();
	this.buttonStepDistanzaMeno = new XdevButton();
	this.textFieldDistanza = new TextFieldDouble();
	this.buttonStepDistanzaPiu = new XdevButton();
	this.sliderDistanza = new XdevSlider();
	this.verticalLayout2 = new XdevVerticalLayout();
	this.imagePotenzaDefault = new XdevImage();
	this.imagePressioneDefault = new XdevImage();
	this.imagePotenzaAlternativa = new XdevImage();
	this.imagePressioneAlternativa = new XdevImage();
	this.verticalLayout3 = new XdevVerticalLayout();
	this.imageCondotto = new XdevImage();
	this.panelInstallazione = new XdevPanel();
	this.verticalLayout7 = new XdevVerticalLayout();
	this.labelPressioneInstallazione = new XdevLabel();
	this.gridLayout3 = new XdevGridLayout();
	this.checkBoxCanalizzazioneIsolate = new XdevCheckBox();
	this.labelCanalizzazioneIsolate = new XdevLabel();
	this.checkBoxCanalizzazioneAlta = new XdevCheckBox();
	this.labelCanalizzazioneAlta = new XdevLabel();
	this.checkBoxCanalizzazioneMedia = new XdevCheckBox();
	this.labelCanalizzazioneMedia = new XdevLabel();
	this.checkBoxCanalizzazioneBassa = new XdevCheckBox();
	this.labelCanalizzazioneBassa = new XdevLabel();
	this.gridLayout2 = new XdevGridLayout();
	this.labelAmbiente = new XdevLabel();
	this.checkBoxCampoLibero = new XdevCheckBox();
	this.checkBoxRiverberante = new XdevCheckBox();
	this.textFieldRiverbero = new TextFieldDouble();
	this.labelRiverberoUM = new XdevLabel();
	this.labelRumoreFondo = new XdevLabel();
	this.textFieldRumoreFondo = new TextFieldDouble();
	this.labelFondoUM = new XdevLabel();
	this.labelPressioneCorretta = new XdevLabel();
	this.labelPressioneCorrettaUM = new XdevLabel();
	this.panel = new XdevPanel();
	this.verticalLayout8 = new XdevVerticalLayout();
	this.textAreaNota2 = new XdevTextArea();
	this.textAreaNota = new XdevTextArea();

	this.horizontalLayout.setMargin(new MarginInfo(false));
	this.verticalLayout.setMargin(new MarginInfo(false));
	this.verticalLayout4.setMargin(new MarginInfo(false));
	this.labelModello.setValue("Modello");
	this.labelModello.setContentMode(ContentMode.HTML);
	this.labelEsecuzione.setValue("Esecuzione");
	this.labelEsecuzioneDesc.setValue("Esecuzione Desc");
	this.labelDiamGirante.setValue("Diametro Girante");
	this.labelDiamGiranteVal.setValue("Diam");
	this.labelDiamGirante.setContentMode(ContentMode.HTML);
	this.labelQVal.setContentMode(ContentMode.HTML);
	this.labelHTVal.setContentMode(ContentMode.HTML);
	this.labelRpmVal.setContentMode(ContentMode.HTML);
	this.labelDensita.setContentMode(ContentMode.HTML);
	
//	this.labelHTUM.setValue("HTUM");
//	this.labelHTUM.setContentMode(ContentMode.HTML);
	this.label8.setValue("Rpm");
	this.labelRpmVal.setValue("val");
	this.label14.setValue("[Rpm]");
	this.labelDensita.setValue("<html>ρ<sub>Fluid</sub>:</html>");
	this.labelDensita.setContentMode(ContentMode.HTML);
	this.labelrhoVal.setValue("val");
	
	this.label15.setValue("<html>[kg/m<sup>3</sup>]</html>");
	this.label15.setContentMode(ContentMode.HTML);
	this.panelPotenza.setCaption("Potenza Sonora");
	this.panelPotenza.setTabIndex(0);
	this.verticalLayout5.setMargin(new MarginInfo(false, false, false, true));
	this.horizontalLayout2.setMargin(new MarginInfo(false));
	this.checkBoxdB.setCaption("dB");
	this.checkBoxdBA.setCaption("dB(A)");
	this.checkBoxTenuta.setCaption("Tenuta");
	this.checkBoxTenuta.setEnabled(false);
	this.checkBoxGasCaldi.setCaption("Gas Caldi");
	this.checkBoxGasCaldi.setEnabled(false);
	this.checkBoxNessunaA.setCaption("A");
	this.checkBoxNessunaA.setEnabled(false);
	this.checkBoxMandataB.setCaption("B");
	this.checkBoxMandataB.setEnabled(false);
	this.checkBoxAspirazioneC.setCaption("C");
	this.checkBoxAspirazioneC.setEnabled(false);
	this.checkBoxEntrambeD.setCaption("D");
	this.checkBoxEntrambeD.setEnabled(false);
	this.panelPressione.setCaption("Pressione Sonora");
	this.panelPressione.setTabIndex(0);
	this.verticalLayout6.setMargin(new MarginInfo(false));
	this.labelPropagazione.setValue("Propagazione");
	this.horizontalLayout3.setMargin(new MarginInfo(false, false, false, true));
	this.checkBox12sf.setCaption("1/2 sf");
	this.checkBox14sf.setCaption("1/4 sf");
	this.checkBox18sf.setCaption("1/8 sf");
	this.horizontalLayout4.setMargin(new MarginInfo(false, false, false, true));
	this.labelDistanza.setValue("Distanza");
	this.labelDistanzaUM.setValue("[m]");
	this.horizontalLayout5.setMargin(new MarginInfo(false, true, false, true));
	this.buttonStepDistanzaMeno.setCaption("<<");
	this.buttonStepDistanzaMeno.setStyleName("small");
	this.buttonStepDistanzaPiu.setCaption(">>");
	this.buttonStepDistanzaPiu.setStyleName("small");
	this.verticalLayout2.setMargin(new MarginInfo(false));
	this.verticalLayout3.setMargin(new MarginInfo(false));
	this.panelInstallazione.setCaption("Pressione Sonora");
	this.verticalLayout7.setMargin(new MarginInfo(false, false, false, true));
	this.labelPressioneInstallazione.setValue("PInstallazione");
	this.labelPressioneInstallazione.setContentMode(ContentMode.HTML);
	this.gridLayout3.setMargin(new MarginInfo(false));
	this.checkBoxCanalizzazioneIsolate.setCaption("CanalizzazioneIsolate");
	this.labelCanalizzazioneIsolate.setValue("100.0 ±3 [dB(A)]");
	this.checkBoxCanalizzazioneAlta.setCaption("CanalizzazioneAlta");
	this.labelCanalizzazioneAlta.setValue("100.0 ±3 [dB(A)]");
	this.checkBoxCanalizzazioneMedia.setCaption("CanalizzazioneMedia");
	this.labelCanalizzazioneMedia.setValue("100.0 ±3 [dB(A)]");
	this.checkBoxCanalizzazioneBassa.setCaption("CanalizzazioneBassa");
	this.labelCanalizzazioneBassa.setValue("100.0 ±3 [dB(A)]");
	this.gridLayout2.setMargin(new MarginInfo(false));
	this.labelAmbiente.setValue("Ambiente");
	this.checkBoxCampoLibero.setCaption("Campo libero");
	this.checkBoxRiverberante.setCaption("Riverberante");
	this.labelRiverberoUM.setValue("UM");
	this.labelRumoreFondo.setValue("Rumore di fondo");
	this.labelFondoUM.setValue("UM");
	this.labelPressioneCorretta.setValue("?");
	this.labelPressioneCorretta.setContentMode(ContentMode.HTML);
	this.labelPressioneCorrettaUM.setValue("±3");
	this.panel.setCaption("");
	this.verticalLayout8.setMargin(new MarginInfo(false, true, false, true));
	this.textAreaNota2.setRows(3);

//	this.gridLayout.setColumns(3);
//	this.gridLayout.setRows(5);
//	this.labelQ.setSizeUndefined();
//	this.gridLayout.addComponent(this.labelQ, 0, 0);
//	this.gridLayout.setComponentAlignment(this.labelQ, Alignment.TOP_RIGHT);
//	this.labelQVal.setWidth(100, Unit.PERCENTAGE);
//	this.labelQVal.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.labelQVal, 1, 0);
//	this.labelQUM.setWidth(100, Unit.PERCENTAGE);
//	this.labelQUM.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.labelQUM, 2, 0);
//	this.labelHT.setSizeUndefined();
//	this.gridLayout.addComponent(this.labelHT, 0, 1);
//	this.gridLayout.setComponentAlignment(this.labelHT, Alignment.TOP_RIGHT);
//	this.labelHTVal.setWidth(100, Unit.PERCENTAGE);
//	this.labelHTVal.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.labelHTVal, 1, 1);
//	this.labelHTUM.setWidth(100, Unit.PERCENTAGE);
//	this.labelHTUM.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.labelHTUM, 2, 1);
//	this.label8.setSizeUndefined();
//	this.gridLayout.addComponent(this.label8, 0, 2);
//	this.gridLayout.setComponentAlignment(this.label8, Alignment.TOP_RIGHT);
//	this.labelRpmVal.setWidth(100, Unit.PERCENTAGE);
//	this.labelRpmVal.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.labelRpmVal, 1, 2);
//	this.label14.setWidth(100, Unit.PERCENTAGE);
//	this.label14.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.label14, 2, 2);
//	//this.labelDensita.setSizeUndefined();
//	this.gridLayout.addComponent(this.labelDensita, 0, 3);
//	this.gridLayout.setComponentAlignment(this.labelDensita, Alignment.TOP_RIGHT);
//	this.labelrhoVal.setWidth(100, Unit.PERCENTAGE);
//	this.labelrhoVal.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.labelrhoVal, 1, 3);
//	this.gridLayout.setComponentAlignment(this.labelrhoVal, Alignment.TOP_RIGHT);
//	this.label15.setWidth(100, Unit.PERCENTAGE);
//	this.label15.setHeight(-1, Unit.PIXELS);
//	this.gridLayout.addComponent(this.label15, 2, 3);
//	this.gridLayout.setComponentAlignment(this.label15, Alignment.TOP_RIGHT);
//	this.gridLayout.setColumnExpandRatio(0, 10.0F);
//	this.gridLayout.setColumnExpandRatio(1, 10.0F);
//	this.gridLayout.setColumnExpandRatio(2, 10.0F);
//	final CustomComponent gridLayout_vSpacer = new CustomComponent();
//	gridLayout_vSpacer.setSizeFull();
//	this.gridLayout.addComponent(gridLayout_vSpacer, 0, 4, 2, 4);
//	this.gridLayout.setRowExpandRatio(4, 1.0F);
	this.labelModello.setSizeUndefined();
	this.verticalLayout4.addComponent(this.labelModello);
	this.verticalLayout4.setComponentAlignment(this.labelModello, Alignment.TOP_CENTER);
	this.labelEsecuzione.setSizeUndefined();
	this.verticalLayout4.addComponent(this.labelEsecuzione);
	this.verticalLayout4.setComponentAlignment(this.labelEsecuzione, Alignment.TOP_CENTER);
	this.labelEsecuzioneDesc.setSizeUndefined();
	this.verticalLayout4.addComponent(this.labelEsecuzioneDesc);
	this.verticalLayout4.setComponentAlignment(this.labelEsecuzioneDesc, Alignment.TOP_CENTER);
	this.labelDiamGirante.setSizeUndefined();
	this.verticalLayout4.addComponent(this.labelDiamGirante);
	this.verticalLayout4.setComponentAlignment(this.labelDiamGirante, Alignment.TOP_CENTER);
//	this.gridLayout.setWidth(100, Unit.PERCENTAGE);
//	this.gridLayout.setHeight(-1, Unit.PIXELS);
	this.verticalLayout4.addComponent(this.labelQVal);
	this.verticalLayout4.setComponentAlignment(this.labelQVal, Alignment.TOP_CENTER);
	this.verticalLayout4.addComponent(this.labelHTVal);
	this.verticalLayout4.setComponentAlignment(this.labelHTVal, Alignment.TOP_CENTER);
	this.verticalLayout4.addComponent(this.labelRpmVal);
	this.verticalLayout4.setComponentAlignment(this.labelRpmVal, Alignment.TOP_CENTER);
	this.verticalLayout4.addComponent(this.labelDensita);
	this.verticalLayout4.setComponentAlignment(this.labelDensita, Alignment.TOP_CENTER);
	this.checkBoxdB.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxdB.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout2.addComponent(this.checkBoxdB);
	this.horizontalLayout2.setComponentAlignment(this.checkBoxdB, Alignment.MIDDLE_CENTER);
	this.horizontalLayout2.setExpandRatio(this.checkBoxdB, 10.0F);
	this.checkBoxdBA.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxdBA.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout2.addComponent(this.checkBoxdBA);
	this.horizontalLayout2.setComponentAlignment(this.checkBoxdBA, Alignment.MIDDLE_CENTER);
	this.horizontalLayout2.setExpandRatio(this.checkBoxdBA, 10.0F);
	this.horizontalLayout2.setSizeFull();
	this.verticalLayout5.addComponent(this.horizontalLayout2);
	this.verticalLayout5.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
	this.verticalLayout5.setExpandRatio(this.horizontalLayout2, 10.0F);
	this.checkBoxTenuta.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxTenuta.setHeight(-1, Unit.PIXELS);
	this.verticalLayout5.addComponent(this.checkBoxTenuta);
	this.verticalLayout5.setComponentAlignment(this.checkBoxTenuta, Alignment.MIDDLE_CENTER);
	this.checkBoxGasCaldi.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxGasCaldi.setHeight(-1, Unit.PIXELS);
	this.verticalLayout5.addComponent(this.checkBoxGasCaldi);
	this.verticalLayout5.setComponentAlignment(this.checkBoxGasCaldi, Alignment.MIDDLE_CENTER);
	this.checkBoxNessunaA.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxNessunaA.setHeight(-1, Unit.PIXELS);
	this.verticalLayout5.addComponent(this.checkBoxNessunaA);
	this.verticalLayout5.setComponentAlignment(this.checkBoxNessunaA, Alignment.MIDDLE_CENTER);
	this.checkBoxMandataB.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxMandataB.setHeight(-1, Unit.PIXELS);
	this.verticalLayout5.addComponent(this.checkBoxMandataB);
	this.verticalLayout5.setComponentAlignment(this.checkBoxMandataB, Alignment.MIDDLE_CENTER);
	this.checkBoxAspirazioneC.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxAspirazioneC.setHeight(-1, Unit.PIXELS);
	this.verticalLayout5.addComponent(this.checkBoxAspirazioneC);
	this.verticalLayout5.setComponentAlignment(this.checkBoxAspirazioneC, Alignment.MIDDLE_CENTER);
	this.checkBoxEntrambeD.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxEntrambeD.setHeight(-1, Unit.PIXELS);
	this.verticalLayout5.addComponent(this.checkBoxEntrambeD);
	this.verticalLayout5.setComponentAlignment(this.checkBoxEntrambeD, Alignment.MIDDLE_CENTER);
	this.verticalLayout5.setSizeFull();
	this.panelPotenza.setContent(this.verticalLayout5);
	this.checkBox12sf.setWidth(100, Unit.PERCENTAGE);
	this.checkBox12sf.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout3.addComponent(this.checkBox12sf);
	this.horizontalLayout3.setComponentAlignment(this.checkBox12sf, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.checkBox12sf, 10.0F);
	this.checkBox14sf.setWidth(100, Unit.PERCENTAGE);
	this.checkBox14sf.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout3.addComponent(this.checkBox14sf);
	this.horizontalLayout3.setComponentAlignment(this.checkBox14sf, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.checkBox14sf, 10.0F);
	this.checkBox18sf.setWidth(100, Unit.PERCENTAGE);
	this.checkBox18sf.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout3.addComponent(this.checkBox18sf);
	this.horizontalLayout3.setComponentAlignment(this.checkBox18sf, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.checkBox18sf, 10.0F);
	this.labelDistanza.setSizeUndefined();
	this.horizontalLayout4.addComponent(this.labelDistanza);
	this.horizontalLayout4.setComponentAlignment(this.labelDistanza, Alignment.TOP_RIGHT);
	this.horizontalLayout4.setExpandRatio(this.labelDistanza, 10.0F);
	this.labelDistanzaUM.setWidth(100, Unit.PERCENTAGE);
	this.labelDistanzaUM.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout4.addComponent(this.labelDistanzaUM);
	this.horizontalLayout4.setComponentAlignment(this.labelDistanzaUM, Alignment.MIDDLE_CENTER);
	this.horizontalLayout4.setExpandRatio(this.labelDistanzaUM, 10.0F);
	this.buttonStepDistanzaMeno.setWidth(100, Unit.PERCENTAGE);
	this.buttonStepDistanzaMeno.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout5.addComponent(this.buttonStepDistanzaMeno);
	this.horizontalLayout5.setComponentAlignment(this.buttonStepDistanzaMeno, Alignment.MIDDLE_CENTER);
	this.horizontalLayout5.setExpandRatio(this.buttonStepDistanzaMeno, 10.0F);
	this.textFieldDistanza.setWidth(100, Unit.PERCENTAGE);
	this.textFieldDistanza.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout5.addComponent(this.textFieldDistanza);
	this.horizontalLayout5.setComponentAlignment(this.textFieldDistanza, Alignment.MIDDLE_CENTER);
	this.horizontalLayout5.setExpandRatio(this.textFieldDistanza, 10.0F);
	this.buttonStepDistanzaPiu.setWidth(100, Unit.PERCENTAGE);
	this.buttonStepDistanzaPiu.setHeight(-1, Unit.PIXELS);
	this.horizontalLayout5.addComponent(this.buttonStepDistanzaPiu);
	this.horizontalLayout5.setComponentAlignment(this.buttonStepDistanzaPiu, Alignment.MIDDLE_CENTER);
	this.horizontalLayout5.setExpandRatio(this.buttonStepDistanzaPiu, 10.0F);
	this.labelPropagazione.setSizeUndefined();
	this.verticalLayout6.addComponent(this.labelPropagazione);
	this.verticalLayout6.setComponentAlignment(this.labelPropagazione, Alignment.TOP_CENTER);
	this.verticalLayout6.setExpandRatio(this.labelPropagazione, 10.0F);
	this.horizontalLayout3.setSizeFull();
	this.verticalLayout6.addComponent(this.horizontalLayout3);
	this.horizontalLayout4.setWidth(100, Unit.PERCENTAGE);
	this.horizontalLayout4.setHeight(-1, Unit.PIXELS);
	this.verticalLayout6.addComponent(this.horizontalLayout4);
	this.horizontalLayout5.setWidth(100, Unit.PERCENTAGE);
	this.horizontalLayout5.setHeight(-1, Unit.PIXELS);
	this.verticalLayout6.addComponent(this.horizontalLayout5);
	this.verticalLayout6.setComponentAlignment(this.horizontalLayout5, Alignment.MIDDLE_CENTER);
	this.sliderDistanza.setWidth(100, Unit.PERCENTAGE);
	this.sliderDistanza.setHeight(-1, Unit.PIXELS);
	this.verticalLayout6.addComponent(this.sliderDistanza);
	this.verticalLayout6.setExpandRatio(this.sliderDistanza, 10.0F);
	this.verticalLayout6.setSizeFull();
	this.panelPressione.setContent(this.verticalLayout6);
	this.verticalLayout4.setWidth(100, Unit.PERCENTAGE);
	this.verticalLayout4.setHeight(-1, Unit.PIXELS);
	this.verticalLayout.addComponent(this.verticalLayout4);
	this.panelPotenza.setWidth(100, Unit.PERCENTAGE);
	this.panelPotenza.setHeight(-1, Unit.PIXELS);
	this.verticalLayout.addComponent(this.panelPotenza);
	this.panelPressione.setWidth(100, Unit.PERCENTAGE);
	this.panelPressione.setHeight(-1, Unit.PIXELS);
	this.verticalLayout.addComponent(this.panelPressione);
	final CustomComponent verticalLayout_spacer = new CustomComponent();
	verticalLayout_spacer.setSizeFull();
	this.verticalLayout.addComponent(verticalLayout_spacer);
	this.verticalLayout.setExpandRatio(verticalLayout_spacer, 1.0F);
	this.imagePotenzaDefault.setWidth(100, Unit.PERCENTAGE);
	this.imagePotenzaDefault.setHeight(-1, Unit.PIXELS);
	this.verticalLayout2.addComponent(this.imagePotenzaDefault);
	this.verticalLayout2.setComponentAlignment(this.imagePotenzaDefault, Alignment.TOP_CENTER);
	this.imagePressioneDefault.setWidth(100, Unit.PERCENTAGE);
	this.imagePressioneDefault.setHeight(-1, Unit.PIXELS);
	this.verticalLayout2.addComponent(this.imagePressioneDefault);
	this.verticalLayout2.setComponentAlignment(this.imagePressioneDefault, Alignment.TOP_CENTER);
	this.imagePotenzaAlternativa.setWidth(-1, Unit.PIXELS);
	this.imagePotenzaAlternativa.setHeight(100, Unit.PERCENTAGE);
	this.verticalLayout2.addComponent(this.imagePotenzaAlternativa);
	this.verticalLayout2.setComponentAlignment(this.imagePotenzaAlternativa, Alignment.TOP_CENTER);
	this.verticalLayout2.setExpandRatio(this.imagePotenzaAlternativa, 10.0F);
	this.imagePressioneAlternativa.setWidth(-1, Unit.PIXELS);
	this.imagePressioneAlternativa.setHeight(100, Unit.PERCENTAGE);
	this.verticalLayout2.addComponent(this.imagePressioneAlternativa);
	this.verticalLayout2.setComponentAlignment(this.imagePressioneAlternativa, Alignment.TOP_CENTER);
	this.verticalLayout2.setExpandRatio(this.imagePressioneAlternativa, 10.0F);
	this.gridLayout3.setColumns(3);
	this.gridLayout3.setRows(5);
	this.checkBoxCanalizzazioneIsolate.setSizeUndefined();
	this.gridLayout3.addComponent(this.checkBoxCanalizzazioneIsolate, 0, 0);
	this.labelCanalizzazioneIsolate.setSizeUndefined();
	this.gridLayout3.addComponent(this.labelCanalizzazioneIsolate, 1, 0);
	this.checkBoxCanalizzazioneAlta.setSizeUndefined();
	this.gridLayout3.addComponent(this.checkBoxCanalizzazioneAlta, 0, 1);
	this.labelCanalizzazioneAlta.setSizeUndefined();
	this.gridLayout3.addComponent(this.labelCanalizzazioneAlta, 1, 1);
	this.checkBoxCanalizzazioneMedia.setSizeUndefined();
	this.gridLayout3.addComponent(this.checkBoxCanalizzazioneMedia, 0, 2);
	this.labelCanalizzazioneMedia.setSizeUndefined();
	this.gridLayout3.addComponent(this.labelCanalizzazioneMedia, 1, 2);
	this.checkBoxCanalizzazioneBassa.setSizeUndefined();
	this.gridLayout3.addComponent(this.checkBoxCanalizzazioneBassa, 0, 3);
	this.labelCanalizzazioneBassa.setSizeUndefined();
	this.gridLayout3.addComponent(this.labelCanalizzazioneBassa, 1, 3);
	final CustomComponent gridLayout3_hSpacer = new CustomComponent();
	gridLayout3_hSpacer.setSizeFull();
	this.gridLayout3.addComponent(gridLayout3_hSpacer, 2, 0, 2, 3);
	this.gridLayout3.setColumnExpandRatio(2, 1.0F);
	final CustomComponent gridLayout3_vSpacer = new CustomComponent();
	gridLayout3_vSpacer.setSizeFull();
	this.gridLayout3.addComponent(gridLayout3_vSpacer, 0, 4, 1, 4);
	this.gridLayout3.setRowExpandRatio(4, 1.0F);
	this.gridLayout2.setColumns(4);
	this.gridLayout2.setRows(5);
	this.labelAmbiente.setSizeUndefined();
	this.gridLayout2.addComponent(this.labelAmbiente, 0, 0);
	this.gridLayout2.setComponentAlignment(this.labelAmbiente, Alignment.TOP_RIGHT);
	this.checkBoxCampoLibero.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxCampoLibero.setHeight(-1, Unit.PIXELS);
	this.gridLayout2.addComponent(this.checkBoxCampoLibero, 1, 0);
	this.checkBoxRiverberante.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxRiverberante.setHeight(-1, Unit.PIXELS);
	this.gridLayout2.addComponent(this.checkBoxRiverberante, 1, 1);
	this.textFieldRiverbero.setWidth(40, Unit.PIXELS);
	this.textFieldRiverbero.setHeight(-1, Unit.PIXELS);
	this.gridLayout2.addComponent(this.textFieldRiverbero, 2, 1);
	this.labelRiverberoUM.setWidth(100, Unit.PERCENTAGE);
	this.labelRiverberoUM.setHeight(-1, Unit.PIXELS);
	this.gridLayout2.addComponent(this.labelRiverberoUM, 3, 1);
	this.labelRumoreFondo.setSizeUndefined();
	this.gridLayout2.addComponent(this.labelRumoreFondo, 1, 2);
	this.gridLayout2.setComponentAlignment(this.labelRumoreFondo, Alignment.TOP_RIGHT);
	this.textFieldRumoreFondo.setWidth(40, Unit.PIXELS);
	this.textFieldRumoreFondo.setHeight(-1, Unit.PIXELS);
	this.gridLayout2.addComponent(this.textFieldRumoreFondo, 2, 2);
	this.labelFondoUM.setSizeUndefined();
	this.gridLayout2.addComponent(this.labelFondoUM, 3, 2);
	this.labelPressioneCorretta.setSizeUndefined();
	this.gridLayout2.addComponent(this.labelPressioneCorretta, 1, 3);
	this.gridLayout2.setComponentAlignment(this.labelPressioneCorretta, Alignment.TOP_RIGHT);
	this.labelPressioneCorrettaUM.setSizeUndefined();
	this.gridLayout2.addComponent(this.labelPressioneCorrettaUM, 2, 3);
	this.gridLayout2.setColumnExpandRatio(0, 10.0F);
	this.gridLayout2.setColumnExpandRatio(1, 50.0F);
	this.gridLayout2.setColumnExpandRatio(2, 5.0F);
	this.gridLayout2.setColumnExpandRatio(3, 10.0F);
	final CustomComponent gridLayout2_vSpacer = new CustomComponent();
	gridLayout2_vSpacer.setSizeFull();
	this.gridLayout2.addComponent(gridLayout2_vSpacer, 0, 4, 3, 4);
	this.gridLayout2.setRowExpandRatio(4, 1.0F);
	this.labelPressioneInstallazione.setWidth(100, Unit.PERCENTAGE);
	this.labelPressioneInstallazione.setHeight(-1, Unit.PIXELS);
	this.verticalLayout7.addComponent(this.labelPressioneInstallazione);
	this.verticalLayout7.setComponentAlignment(this.labelPressioneInstallazione, Alignment.TOP_CENTER);
	this.verticalLayout7.setExpandRatio(this.labelPressioneInstallazione, 10.0F);
	this.gridLayout3.setWidth(100, Unit.PERCENTAGE);
	this.gridLayout3.setHeight(-1, Unit.PIXELS);
	this.verticalLayout7.addComponent(this.gridLayout3);
	this.verticalLayout7.setComponentAlignment(this.gridLayout3, Alignment.MIDDLE_CENTER);
	this.gridLayout2.setWidth(100, Unit.PERCENTAGE);
	this.gridLayout2.setHeight(-1, Unit.PIXELS);
	this.verticalLayout7.addComponent(this.gridLayout2);
	this.verticalLayout7.setExpandRatio(this.gridLayout2, 10.0F);
	this.verticalLayout7.setSizeFull();
	this.panelInstallazione.setContent(this.verticalLayout7);
	this.textAreaNota2.setWidth(100, Unit.PERCENTAGE);
	this.textAreaNota2.setHeight(-1, Unit.PIXELS);
	this.verticalLayout8.addComponent(this.textAreaNota2);
	this.textAreaNota.setSizeFull();
	this.verticalLayout8.addComponent(this.textAreaNota);
	this.verticalLayout8.setExpandRatio(this.textAreaNota, 10.0F);
	this.verticalLayout8.setSizeFull();
	this.panel.setContent(this.verticalLayout8);
	this.imageCondotto.setWidth(100, Unit.PERCENTAGE);
	this.imageCondotto.setHeight(-1, Unit.PIXELS);
	this.verticalLayout3.addComponent(this.imageCondotto);
	this.panelInstallazione.setWidth(100, Unit.PERCENTAGE);
	this.panelInstallazione.setHeight(-1, Unit.PIXELS);
	this.verticalLayout3.addComponent(this.panelInstallazione);
	this.panel.setWidth(100, Unit.PERCENTAGE);
	this.panel.setHeight(-1, Unit.PIXELS);
	this.verticalLayout3.addComponent(this.panel);
	final CustomComponent verticalLayout3_spacer = new CustomComponent();
	verticalLayout3_spacer.setSizeFull();
	this.verticalLayout3.addComponent(verticalLayout3_spacer);
	this.verticalLayout3.setExpandRatio(verticalLayout3_spacer, 1.0F);
	this.verticalLayout.setSizeFull();
	this.horizontalLayout.addComponent(this.verticalLayout);
	this.horizontalLayout.setComponentAlignment(this.verticalLayout, Alignment.MIDDLE_CENTER);
	this.horizontalLayout.setExpandRatio(this.verticalLayout, 10.0F);
	this.verticalLayout2.setSizeFull();
	this.horizontalLayout.addComponent(this.verticalLayout2);
	this.horizontalLayout.setComponentAlignment(this.verticalLayout2, Alignment.MIDDLE_CENTER);
	this.horizontalLayout.setExpandRatio(this.verticalLayout2, 20.0F);
	this.verticalLayout3.setSizeFull();
	this.horizontalLayout.addComponent(this.verticalLayout3);
	this.horizontalLayout.setComponentAlignment(this.verticalLayout3, Alignment.MIDDLE_CENTER);
	this.horizontalLayout.setExpandRatio(this.verticalLayout3, 14.0F);
	this.horizontalLayout.setSizeFull();
	this.setContent(this.horizontalLayout);
	this.setSizeFull();

	this.checkBoxdB.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxdB_valueChange(event);
		}
	});
	this.checkBoxdBA.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxdBA_valueChange(event);
		}
	});
	this.checkBox12sf.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBox12sf_valueChange(event);
		}
	});
	this.checkBox14sf.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBox14sf_valueChange(event);
		}
	});
	this.checkBox18sf.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBox18sf_valueChange(event);
		}
	});
	this.buttonStepDistanzaMeno.addClickListener(event -> this.buttonStepDistanzaMeno_buttonClick(event));
	this.textFieldDistanza.addTextChangeListener(new FieldEvents.TextChangeListener() {
		@Override
		public void textChange(final FieldEvents.TextChangeEvent event) {
			SpettroSonoroView.this.textFieldDistanza_textChange(event);
		}
	});
	this.buttonStepDistanzaPiu.addClickListener(event -> this.buttonStepDistanzaPiu_buttonClick(event));
	this.sliderDistanza.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.sliderDistanza_valueChange(event);
		}
	});
	this.checkBoxCanalizzazioneIsolate.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxCanalizzazioneIsolate_valueChange(event);
		}
	});
	this.checkBoxCanalizzazioneAlta.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxCanalizzazioneAlta_valueChange(event);
		}
	});
	this.checkBoxCanalizzazioneMedia.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxCanalizzazioneMedia_valueChange(event);
		}
	});
	this.checkBoxCanalizzazioneBassa.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxCanalizzazioneBassa_valueChange(event);
		}
	});
	this.checkBoxCampoLibero.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxCampoLibero_valueChange(event);
		}
	});
	this.checkBoxRiverberante.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			SpettroSonoroView.this.checkBoxRiverberante_valueChange(event);
		}
	});
	this.textFieldRiverbero.addTextChangeListener(new FieldEvents.TextChangeListener() {
		@Override
		public void textChange(final FieldEvents.TextChangeEvent event) {
			SpettroSonoroView.this.textFieldRiverbero_textChange(event);
		}
	});
	this.textFieldRumoreFondo.addTextChangeListener(new FieldEvents.TextChangeListener() {
		@Override
		public void textChange(final FieldEvents.TextChangeEvent event) {
			SpettroSonoroView.this.textFieldRumoreFondo_textChange(event);
		}
	});
} // </generated-code>

// <generated-code name="variables">
private XdevLabel labelModello, labelEsecuzione, labelEsecuzioneDesc, labelQ, labelQVal, labelQUM, labelHT, labelHTVal,
		labelHTUM, label8, labelRpmVal, label14, labelDensita, labelrhoVal, label15, labelPropagazione, labelDistanza,
		labelDistanzaUM, labelPressioneInstallazione, labelCanalizzazioneIsolate, labelCanalizzazioneAlta,
		labelCanalizzazioneMedia, labelCanalizzazioneBassa, labelAmbiente, labelRiverberoUM, labelRumoreFondo,
		labelFondoUM, labelPressioneCorretta, labelPressioneCorrettaUM, labelDiamGirante, labelDiamGiranteVal;
private XdevButton buttonStepDistanzaMeno, buttonStepDistanzaPiu;
private XdevHorizontalLayout horizontalLayout, horizontalLayout2, horizontalLayout3, horizontalLayout4,
		horizontalLayout5;
private XdevImage imagePotenzaDefault, imagePressioneDefault, imagePotenzaAlternativa, imagePressioneAlternativa,
		imageCondotto;
private XdevTextArea textAreaNota2, textAreaNota;
private com.cit.sellfan.ui.TextFieldDouble textFieldDistanza, textFieldRiverbero, textFieldRumoreFondo;
private XdevSlider sliderDistanza;
private XdevPanel panelPotenza, panelPressione, panelInstallazione, panel;
private XdevCheckBox checkBoxdB, checkBoxdBA, checkBoxTenuta, checkBoxGasCaldi, checkBoxNessunaA, checkBoxMandataB,
		checkBoxAspirazioneC, checkBoxEntrambeD, checkBox12sf, checkBox14sf, checkBox18sf,
		checkBoxCanalizzazioneIsolate, checkBoxCanalizzazioneAlta, checkBoxCanalizzazioneMedia,
		checkBoxCanalizzazioneBassa, checkBoxCampoLibero, checkBoxRiverberante;
private XdevGridLayout gridLayout3, gridLayout2;
private XdevVerticalLayout verticalLayout, verticalLayout4, verticalLayout5, verticalLayout6, verticalLayout2,
		verticalLayout3, verticalLayout7, verticalLayout8;
// </generated-code>
}