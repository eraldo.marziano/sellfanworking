package com.cit.sellfan.ui.view;

import java.io.FileInputStream;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.template.jDisplayImageWeb;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

public class RiepilogoView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
	private final jDisplayImageWeb imageweb = new jDisplayImageWeb();

	/**
	 * 
	 */
	public RiepilogoView() {
		super();
		this.initUI();
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void Nazionalizza() {
		this.panelVentilatore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatore"));
		this.labelPt.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Totale"));
		this.labelQ.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Portata Volumetrica"));
		this.labelRpm.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Giri/min"));
		this.labelPs.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Statica"));
		this.labelNe.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Assorbita"));
		this.labelCorrettore.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Correttore Potenza Motore"));
		this.labelPotInstallata.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Installata"));
		this.labeleta.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Rendimento"));
		this.labelPotSonora.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pot. sonora funzionamento"));
		this.labelPresSonora.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Press. sonora funzionamento"));
		this.panelMotore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.labelMotCodice.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.labelMotFrequenza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza"));
		this.labelMotNPoli.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("N. Poli"));
		this.labelMotPotenzaKw.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza"));
		this.panelFoto.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Foto"));
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		if (this.l_VG == null) {
			return false;
		}
		if (!this.l_VG.utilityCliente.isTabVisible("Riepilogo")  || this.l_VG.VentilatoreCurrent == null) {
			return false;
		}
		return true;
	}

    public void setVentilatoreCurrent() {
        this.isPannelloInizializzato = false;
        if (this.l_VG.VentilatoreCurrent == null) {
            return;
        }
        try {
            final FileInputStream fileIn = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "ventilatori/" + this.l_VG.VentilatoreCurrent.FotoRiepilogo);
            this.imageweb.DisplayfromInputStream(fileIn);
            final StreamResource logosource = new StreamResource(this.imageweb, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
            logosource.setCacheTime(100);
            this.image.setSource(logosource);
        } catch (final Exception e) {
            this.image.setSource(null);
        }
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPressione.nDecimaliStampa);
        if (this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale > 0.) {
            this.labelPtVal.setValue(this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale, this.l_VG.VentilatoreCurrent.UMPressione)));
        } else {
            this.labelPtVal.setValue("");
        }
        this.labelPtUM.setValue(this.l_VG.VentilatoreCurrent.UMPressione.simbolo);
        if (this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica > 0.) {
            this.labelPsVal.setValue(this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica, this.l_VG.VentilatoreCurrent.UMPressione)));
        } else {
            this.labelPsVal.setValue("");
        }
        this.labelPsUM.setValue(this.l_VG.VentilatoreCurrent.UMPressione.simbolo);
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
        this.labelQVal.setValue(this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromm3hToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, this.l_VG.VentilatoreCurrent.UMPortata)));
        this.labelQUM.setValue(this.l_VG.VentilatoreCurrent.UMPortata.simbolo);
        this.labelRpmVal.setValue(Integer.toString((int) this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri));
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPotenza.nDecimaliStampa);
        this.labelNeVal.setValue(this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza, this.l_VG.VentilatoreCurrent.UMPotenza)));
        this.labelNeUM.setValue(this.l_VG.VentilatoreCurrent.UMPotenza.simbolo);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
        this.labeletaVal.setValue(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.selezioneCorrente.Rendimento));
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.NDecimaliRumore);
        this.labelPotSonoraVal.setValue(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaSonora));
        this.labelPresSonoraVal.setValue(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneSonora));
        if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null && !this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Codice.equals("-")) {
            this.labelMotCodiceVal.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Codice);
            if (l_VG.idClienteIndexVG == 1 && l_VG.CACatalogo.frequenza == 60) {
                this.labelMotFrequenzaVal.setValue("60");
            } else
                this.labelMotFrequenzaVal.setValue(Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Freq));
            this.labelMotNPoliVal.setValue(Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Poli));
            this.l_VG.fmtNd.setMaximumFractionDigits(3);
            this.labelMotPotenzaKwVal.setValue(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW));
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPotenza.nDecimaliStampa);
            this.labelMotPotenzaVal.setValue(this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW * 1000., this.l_VG.VentilatoreCurrent.UMPotenza)));
            this.labelMotPotenzaUM.setValue(this.l_VG.VentilatoreCurrent.UMPotenza.simbolo);
            this.labelPotInstallataUM.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("vedi Motore Predefinito"));
        } else {
            this.labelMotCodiceVal.setValue("");
            this.labelMotFrequenzaVal.setValue("");
            this.labelMotNPoliVal.setValue("");
            this.labelMotPotenzaKwVal.setValue("");
            this.labelMotPotenzaVal.setValue("");
            this.labelMotPotenzaUM.setValue("");
            this.labelPotInstallataUM.setValue("");
        }
        this.labelCorrettoreVal.setValue(Integer.toString((int) (this.l_VG.VentilatoreCurrent.CorrettorePotenzaMotore * 100.)));
        this.isPannelloInizializzato = true;
    }

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.panelVentilatore = new XdevPanel();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.labelPt = new XdevLabel();
		this.labelQ = new XdevLabel();
		this.labelRpm = new XdevLabel();
		this.labelPs = new XdevLabel();
		this.labelNe = new XdevLabel();
		this.labelCorrettore = new XdevLabel();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.labelPtVal = new XdevLabel();
		this.labelQVal = new XdevLabel();
		this.labelRpmVal = new XdevLabel();
		this.labelPsVal = new XdevLabel();
		this.labelNeVal = new XdevLabel();
		this.labelCorrettoreVal = new XdevLabel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.labelPtUM = new XdevLabel();
		this.labelQUM = new XdevLabel();
		this.labelRpmUM = new XdevLabel();
		this.labelPsUM = new XdevLabel();
		this.labelNeUM = new XdevLabel();
		this.label18 = new XdevLabel();
		this.verticalLayout5 = new XdevVerticalLayout();
		this.labelPotInstallata = new XdevLabel();
		this.labeleta = new XdevLabel();
		this.labelPotSonora = new XdevLabel();
		this.labelPresSonora = new XdevLabel();
		this.verticalLayout6 = new XdevVerticalLayout();
		this.label24 = new XdevLabel();
		this.labeletaVal = new XdevLabel();
		this.labelPotSonoraVal = new XdevLabel();
		this.labelPresSonoraVal = new XdevLabel();
		this.verticalLayout7 = new XdevVerticalLayout();
		this.labelPotInstallataUM = new XdevLabel();
		this.label31 = new XdevLabel();
		this.label32 = new XdevLabel();
		this.label33 = new XdevLabel();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.panelMotore = new XdevPanel();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.verticalLayout8 = new XdevVerticalLayout();
		this.labelMotCodice = new XdevLabel();
		this.labelMotFrequenza = new XdevLabel();
		this.labelMotNPoli = new XdevLabel();
		this.verticalLayout9 = new XdevVerticalLayout();
		this.labelMotCodiceVal = new XdevLabel();
		this.labelMotFrequenzaVal = new XdevLabel();
		this.labelMotNPoliVal = new XdevLabel();
		this.verticalLayout10 = new XdevVerticalLayout();
		this.label7 = new XdevLabel();
		this.label8 = new XdevLabel();
		this.verticalLayout11 = new XdevVerticalLayout();
		this.labelMotPotenzaKw = new XdevLabel();
		this.verticalLayout12 = new XdevVerticalLayout();
		this.labelMotPotenzaKwVal = new XdevLabel();
		this.labelMotPotenzaVal = new XdevLabel();
		this.verticalLayout13 = new XdevVerticalLayout();
		this.label14 = new XdevLabel();
		this.labelMotPotenzaUM = new XdevLabel();
		this.panelFoto = new XdevPanel();
		this.image = new XdevImage();
	
		this.panelVentilatore.setCaption("Ventilatore");
		this.labelPt.setValue("Pressione Totale");
		this.labelQ.setValue("Portata Volumetrica");
		this.labelRpm.setValue("Numero Giri/min");
		this.labelPs.setValue("Pressione Statica");
		this.labelNe.setValue("Potenza Assorbita");
		this.labelCorrettore.setValue("Correttore Potenza Motore");
		this.labelPtVal.setValue("???");
		this.labelQVal.setValue("???");
		this.labelRpmVal.setValue("???");
		this.labelPsVal.setValue("???");
		this.labelNeVal.setValue("???");
		this.labelCorrettoreVal.setValue("???");
		this.labelPtUM.setValue("UM");
		this.labelPtUM.setContentMode(ContentMode.HTML);
		this.labelQUM.setValue("UM");
		this.labelQUM.setContentMode(ContentMode.HTML);
		this.labelRpmUM.setContentMode(ContentMode.HTML);
		this.labelPsUM.setValue("UM");
		this.labelPsUM.setContentMode(ContentMode.HTML);
		this.labelNeUM.setValue("UM");
		this.labelNeUM.setContentMode(ContentMode.HTML);
		this.label18.setValue("%");
		this.labelPotInstallata.setValue("Potenza Installata");
		this.labeleta.setValue("Rendimento");
		this.labelPotSonora.setValue("Pot. sonora funzionamento");
		this.labelPresSonora.setValue("Press. sonora funzionamento");
		this.labeletaVal.setValue("???");
		this.labelPotSonoraVal.setValue("???");
		this.labelPresSonoraVal.setValue("???");
		this.labelPotInstallataUM.setValue("vedi Motore Predefinito");
		this.label31.setValue("%");
		this.label32.setValue("[dB(A)]");
		this.label33.setValue("[dB(A)]");
		this.panelMotore.setCaption("Motore");
		this.labelMotCodice.setValue("Codice");
		this.labelMotFrequenza.setValue("Frequenza");
		this.labelMotNPoli.setValue("N. Poli");
		this.labelMotCodiceVal.setValue("280S04-50/3GD2PS");
		this.labelMotFrequenzaVal.setValue("??");
		this.labelMotNPoliVal.setValue("?");
		this.label8.setValue("Hz");
		this.labelMotPotenzaKw.setValue("Potenza");
		this.labelMotPotenzaKwVal.setValue("???");
		this.labelMotPotenzaVal.setValue("???");
		this.label14.setValue("[kW]");
		this.labelMotPotenzaUM.setValue("UM");
		this.labelMotPotenzaUM.setContentMode(ContentMode.HTML);
		this.panelFoto.setCaption("Foto");
	
		this.labelPt.setSizeUndefined();
		this.verticalLayout2.addComponent(this.labelPt);
		this.labelQ.setSizeUndefined();
		this.verticalLayout2.addComponent(this.labelQ);
		this.labelRpm.setSizeUndefined();
		this.verticalLayout2.addComponent(this.labelRpm);
		this.labelPs.setSizeUndefined();
		this.verticalLayout2.addComponent(this.labelPs);
		this.labelNe.setSizeUndefined();
		this.verticalLayout2.addComponent(this.labelNe);
		this.labelCorrettore.setSizeUndefined();
		this.verticalLayout2.addComponent(this.labelCorrettore);
		this.labelPtVal.setSizeUndefined();
		this.verticalLayout3.addComponent(this.labelPtVal);
		this.verticalLayout3.setComponentAlignment(this.labelPtVal, Alignment.TOP_RIGHT);
		this.labelQVal.setSizeUndefined();
		this.verticalLayout3.addComponent(this.labelQVal);
		this.verticalLayout3.setComponentAlignment(this.labelQVal, Alignment.TOP_RIGHT);
		this.labelRpmVal.setSizeUndefined();
		this.verticalLayout3.addComponent(this.labelRpmVal);
		this.verticalLayout3.setComponentAlignment(this.labelRpmVal, Alignment.TOP_RIGHT);
		this.labelPsVal.setSizeUndefined();
		this.verticalLayout3.addComponent(this.labelPsVal);
		this.verticalLayout3.setComponentAlignment(this.labelPsVal, Alignment.TOP_RIGHT);
		this.labelNeVal.setSizeUndefined();
		this.verticalLayout3.addComponent(this.labelNeVal);
		this.verticalLayout3.setComponentAlignment(this.labelNeVal, Alignment.TOP_RIGHT);
		this.labelCorrettoreVal.setSizeUndefined();
		this.verticalLayout3.addComponent(this.labelCorrettoreVal);
		this.verticalLayout3.setComponentAlignment(this.labelCorrettoreVal, Alignment.TOP_RIGHT);
		this.labelPtUM.setSizeUndefined();
		this.verticalLayout4.addComponent(this.labelPtUM);
		this.labelQUM.setSizeUndefined();
		this.verticalLayout4.addComponent(this.labelQUM);
		this.labelRpmUM.setWidth(-1, Unit.PIXELS);
		this.labelRpmUM.setHeight(18, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.labelRpmUM);
		this.labelPsUM.setSizeUndefined();
		this.verticalLayout4.addComponent(this.labelPsUM);
		this.labelNeUM.setSizeUndefined();
		this.verticalLayout4.addComponent(this.labelNeUM);
		this.label18.setSizeUndefined();
		this.verticalLayout4.addComponent(this.label18);
		this.labelPotInstallata.setSizeUndefined();
		this.verticalLayout5.addComponent(this.labelPotInstallata);
		this.labeleta.setSizeUndefined();
		this.verticalLayout5.addComponent(this.labeleta);
		this.labelPotSonora.setSizeUndefined();
		this.verticalLayout5.addComponent(this.labelPotSonora);
		this.labelPresSonora.setSizeUndefined();
		this.verticalLayout5.addComponent(this.labelPresSonora);
		this.label24.setWidth(-1, Unit.PIXELS);
		this.label24.setHeight(18, Unit.PIXELS);
		this.verticalLayout6.addComponent(this.label24);
		this.verticalLayout6.setComponentAlignment(this.label24, Alignment.TOP_CENTER);
		this.labeletaVal.setSizeUndefined();
		this.verticalLayout6.addComponent(this.labeletaVal);
		this.verticalLayout6.setComponentAlignment(this.labeletaVal, Alignment.TOP_RIGHT);
		this.labelPotSonoraVal.setSizeUndefined();
		this.verticalLayout6.addComponent(this.labelPotSonoraVal);
		this.verticalLayout6.setComponentAlignment(this.labelPotSonoraVal, Alignment.TOP_RIGHT);
		this.labelPresSonoraVal.setSizeUndefined();
		this.verticalLayout6.addComponent(this.labelPresSonoraVal);
		this.verticalLayout6.setComponentAlignment(this.labelPresSonoraVal, Alignment.TOP_RIGHT);
		this.labelPotInstallataUM.setSizeUndefined();
		this.verticalLayout7.addComponent(this.labelPotInstallataUM);
		this.label31.setSizeUndefined();
		this.verticalLayout7.addComponent(this.label31);
		this.label32.setSizeUndefined();
		this.verticalLayout7.addComponent(this.label32);
		this.label33.setSizeUndefined();
		this.verticalLayout7.addComponent(this.label33);
		this.verticalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout2.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.verticalLayout2);
		this.horizontalLayout3.setExpandRatio(this.verticalLayout2, 20.0F);
		this.verticalLayout3.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout3.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.verticalLayout3);
		this.horizontalLayout3.setExpandRatio(this.verticalLayout3, 10.0F);
		this.verticalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout4.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.verticalLayout4);
		this.horizontalLayout3.setExpandRatio(this.verticalLayout4, 10.0F);
		this.verticalLayout5.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout5.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.verticalLayout5);
		this.horizontalLayout3.setExpandRatio(this.verticalLayout5, 20.0F);
		this.verticalLayout6.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout6.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.verticalLayout6);
		this.horizontalLayout3.setExpandRatio(this.verticalLayout6, 10.0F);
		this.verticalLayout7.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout7.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.verticalLayout7);
		this.horizontalLayout3.setExpandRatio(this.verticalLayout7, 17.0F);
		this.horizontalLayout3.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout3.setHeight(-1, Unit.PIXELS);
		this.panelVentilatore.setContent(this.horizontalLayout3);
		this.panelVentilatore.setWidth(100, Unit.PERCENTAGE);
		this.panelVentilatore.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.panelVentilatore);
		this.horizontalLayout.setExpandRatio(this.panelVentilatore, 10.0F);
		this.labelMotCodice.setSizeUndefined();
		this.verticalLayout8.addComponent(this.labelMotCodice);
		this.labelMotFrequenza.setSizeUndefined();
		this.verticalLayout8.addComponent(this.labelMotFrequenza);
		this.labelMotNPoli.setSizeUndefined();
		this.verticalLayout8.addComponent(this.labelMotNPoli);
		this.labelMotCodiceVal.setSizeUndefined();
		this.verticalLayout9.addComponent(this.labelMotCodiceVal);
		this.verticalLayout9.setComponentAlignment(this.labelMotCodiceVal, Alignment.TOP_RIGHT);
		this.labelMotFrequenzaVal.setSizeUndefined();
		this.verticalLayout9.addComponent(this.labelMotFrequenzaVal);
		this.verticalLayout9.setComponentAlignment(this.labelMotFrequenzaVal, Alignment.TOP_RIGHT);
		this.labelMotNPoliVal.setSizeUndefined();
		this.verticalLayout9.addComponent(this.labelMotNPoliVal);
		this.verticalLayout9.setComponentAlignment(this.labelMotNPoliVal, Alignment.TOP_RIGHT);
		this.label7.setWidth(-1, Unit.PIXELS);
		this.label7.setHeight(18, Unit.PIXELS);
		this.verticalLayout10.addComponent(this.label7);
		this.verticalLayout10.setComponentAlignment(this.label7, Alignment.MIDDLE_CENTER);
		this.label8.setSizeUndefined();
		this.verticalLayout10.addComponent(this.label8);
		this.labelMotPotenzaKw.setSizeUndefined();
		this.verticalLayout11.addComponent(this.labelMotPotenzaKw);
		this.labelMotPotenzaKwVal.setSizeUndefined();
		this.verticalLayout12.addComponent(this.labelMotPotenzaKwVal);
		this.verticalLayout12.setComponentAlignment(this.labelMotPotenzaKwVal, Alignment.TOP_RIGHT);
		this.labelMotPotenzaVal.setSizeUndefined();
		this.verticalLayout12.addComponent(this.labelMotPotenzaVal);
		this.verticalLayout12.setComponentAlignment(this.labelMotPotenzaVal, Alignment.TOP_RIGHT);
		this.label14.setSizeUndefined();
		this.verticalLayout13.addComponent(this.label14);
		this.labelMotPotenzaUM.setSizeUndefined();
		this.verticalLayout13.addComponent(this.labelMotPotenzaUM);
		this.verticalLayout8.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout8.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.verticalLayout8);
		this.horizontalLayout4.setExpandRatio(this.verticalLayout8, 20.0F);
		this.verticalLayout9.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout9.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.verticalLayout9);
		this.horizontalLayout4.setExpandRatio(this.verticalLayout9, 20.0F);
		this.verticalLayout10.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout10.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.verticalLayout10);
		this.horizontalLayout4.setExpandRatio(this.verticalLayout10, 10.0F);
		this.verticalLayout11.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout11.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.verticalLayout11);
		this.horizontalLayout4.setExpandRatio(this.verticalLayout11, 20.0F);
		this.verticalLayout12.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout12.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.verticalLayout12);
		this.horizontalLayout4.setExpandRatio(this.verticalLayout12, 25.0F);
		this.verticalLayout13.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout13.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.verticalLayout13);
		this.horizontalLayout4.setExpandRatio(this.verticalLayout13, 10.0F);
		this.horizontalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout4.setHeight(-1, Unit.PIXELS);
		this.panelMotore.setContent(this.horizontalLayout4);
		this.image.setSizeFull();
		this.panelFoto.setContent(this.image);
		this.panelMotore.setWidth(100, Unit.PERCENTAGE);
		this.panelMotore.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.panelMotore);
		this.horizontalLayout2.setExpandRatio(this.panelMotore, 30.0F);
		this.panelFoto.setWidth(160, Unit.PIXELS);
		this.panelFoto.setHeight(160, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.panelFoto);
		this.horizontalLayout2.setExpandRatio(this.panelFoto, 10.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setExpandRatio(this.horizontalLayout2, 10.0F);
		this.verticalLayout.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout.setHeight(-1, Unit.PIXELS);
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelPt, labelQ, labelRpm, labelPs, labelNe, labelCorrettore, labelPtVal, labelQVal, labelRpmVal,
			labelPsVal, labelNeVal, labelCorrettoreVal, labelPtUM, labelQUM, labelRpmUM, labelPsUM, labelNeUM, label18,
			labelPotInstallata, labeleta, labelPotSonora, labelPresSonora, label24, labeletaVal, labelPotSonoraVal,
			labelPresSonoraVal, labelPotInstallataUM, label31, label32, label33, labelMotCodice, labelMotFrequenza,
			labelMotNPoli, labelMotCodiceVal, labelMotFrequenzaVal, labelMotNPoliVal, label7, label8, labelMotPotenzaKw,
			labelMotPotenzaKwVal, labelMotPotenzaVal, label14, labelMotPotenzaUM;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout3, horizontalLayout2, horizontalLayout4;
	private XdevImage image;
	private XdevPanel panelVentilatore, panelMotore, panelFoto;
	private XdevVerticalLayout verticalLayout, verticalLayout2, verticalLayout3, verticalLayout4, verticalLayout5,
			verticalLayout6, verticalLayout7, verticalLayout8, verticalLayout9, verticalLayout10, verticalLayout11,
			verticalLayout12, verticalLayout13;
	// </generated-code>

}
