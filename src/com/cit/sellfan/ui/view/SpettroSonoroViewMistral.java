package com.cit.sellfan.ui.view;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.template.jGraficoXYWeb;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevSlider;
import com.xdev.ui.XdevTextArea;
import com.xdev.ui.XdevVerticalLayout;
import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.Rumore;
import cit.sellfan.classi.Serie;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.personalizzazioni.cliente01.Cliente01Rumore;
import cit.sellfan.personalizzazioni.cliente01.Cliente01VentilatoreCampiCliente;
import com.vaadin.server.Sizeable;

public class SpettroSonoroViewMistral extends SpettroSonoroView {
    public boolean isPannelloInizializzato = false;
    private VariabiliGlobali l_VG;
    private Cliente01Rumore rumore = null;
    private VentilatoreFisica l_vf;
    private Cliente01VentilatoreCampiCliente l_vcc = new Cliente01VentilatoreCampiCliente();
    private UtilityCliente l_uc;
    private Ventilatore vCurr;
    private boolean editEnabled = false;
    public jGraficoXYWeb graficoPressione = new jGraficoXYWeb();
    public jGraficoXYWeb graficoPotenza = new jGraficoXYWeb();
    public jGraficoXYWeb graficoCondotto = new jGraficoXYWeb();
    private String lbl_finalPressure_start = "", lbl_finalPressure_middle = "", lbl_finalPressure_end = "";
	
    public SpettroSonoroViewMistral() {
        super();
        this.initUI();
    }
	
    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        this.l_VG.ElencoView[this.l_VG.SpettroSonoroViewIndex] = this;
        this.l_uc = this.l_VG.utilityCliente;
        this.rumore = new Cliente01Rumore(this.l_VG.utilityCliente.ConnTecnica, this.l_VG.dbTableQualifier, "mistral");
    }
	
    public void Nazionalizza() {
        this.pn_soundPower.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Sonora"));
        this.pn_pressure.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora"));
        this.lb_Pressure_Propagation.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Propagazione"));
        this.lb_Pressure_Dist.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Distanza"));
        this.pn_soundPowerInstallation.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora"));
        this.lb_right_r1_pressure.setValue("<b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora in condizioni di installazione"));
        this.lb_soundPowerInstallation_1_Environment.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Ambiente"));
        this.cb_env_OpF.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Campo libero"));
        this.cb_env_Rev.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Riverbero"));
        this.lb_env_Bck.setValue("  " +this.l_VG.utilityTraduzioni.TraduciStringa("Rumore di fondo"));
        lbl_finalPressure_start = "<html>"+this.l_VG.utilityTraduzioni.TraduciStringa("Pressione sonora finale");
        lb_final_pressure.setValue(lbl_finalPressure_start + " : " + lbl_finalPressure_middle + lbl_finalPressure_end);
        this.ta_Note.setReadOnly(false);
        String str = this.l_VG.utilityTraduzioni.TraduciStringa("Valore medio puramente indicativo, ottenuto tramite coefficienti sperimentali.");
        str += "\n" + this.l_VG.utilityTraduzioni.TraduciStringa("Dati di pressione sonora calcolati. Utilizzare solo come riferimento.");
        str += "\n" + this.l_VG.utilityTraduzioni.TraduciStringa("Eventuali accessori possono modificare i valori di rumorosità.");
        this.ta_Note.setValue(str);
        this.ta_Note.setReadOnly(true);
        if (vCurr != null) {
            final DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.HALF_DOWN);
            this.lb_r3_Impeller.setValue("<b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Diametro Girante") + ": </b>" + df.format(vCurr.DiametroGirante) + " [m]");
        }
        this.cb_d2_SPH.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Libera"));
        this.cb_d2_HEM.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Emisfera"));
        this.cb_d2_DIH.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Diedro"));
        this.cb_d2_TRI.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Triedro"));
    }
	
    public boolean isPreSetVentilatoreCurrentOK() {
        if (this.l_VG == null)  return false;
        if (vCurr == null || this.l_VG.utilityCliente == null || !this.l_VG.utilityCliente.isTabVisible("SpettroSonoro")) return false;
        return true;
    }
	
    public void setVentilatoreCurrent() {
        vCurr = l_VG.VentilatoreCurrent;
        isPannelloInizializzato = false;
        editEnabled = false;
        l_VG.MainView.aggiornaGraficoMistral();
        lb_r1_Model.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(vCurr)));
        lb_r2_Arrangement.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione") + " " + vCurr.selezioneCorrente.Esecuzione);
        final DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.HALF_DOWN);
        lb_r3_Impeller.setValue("<b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Diametro Girante") + ": </b>" + df.format(vCurr.DiametroGirante) + " [m]");
        l_VG.fmtNd.setMinimumFractionDigits(0);
        l_VG.fmtNd.setMaximumFractionDigits(vCurr.UMPortata.nDecimaliStampa);
        lb_r4_Q.setValue("<b>Q: </b>" + this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromm3hToXX(vCurr.selezioneCorrente.Marker, vCurr.UMPortata)) + "   " + vCurr.UMPortata.simbolo);
        l_VG.fmtNd.setMaximumFractionDigits(vCurr.UMPressione.nDecimaliStampa);
        lb_r5_Ht.setValue("<b>" + StringheUNI.PRESSIONETOTALE + ": </b>" + this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(vCurr.selezioneCorrente.PressioneTotale, vCurr.UMPressione)) + "   " + vCurr.UMPressione.simbolo);
        lb_r6_RPM.setValue("<b>RPM: </b>" + Integer.toString((int) vCurr.selezioneCorrente.NumeroGiri) + " [RPM]");
        l_VG.fmtNd.setMaximumFractionDigits(3);
        lb_r7_rho.setValue("<b>ρ<sub>Fluid</sub>: </b>" + this.l_VG.fmtNd.format(vCurr.CAProgettazione.rho) + " [kg/m<sup>3</sup>]");
        fillFlagFromCampiCliente();
        Nazionalizza();
        drawIstogrammi();
        resizeView();
        editEnabled = true;
        isPannelloInizializzato = true;
    }
	
    public void resizeView() {
        if (this.l_VG.browserAspectRatio < 1.43) {			//default
            this.imagePotenzaAlternativa.setVisible(false);
            this.imagePressioneAlternativa.setVisible(false);
            this.imagePotenzaDefault.setVisible(true);
            this.imagePressioneDefault.setVisible(true);
        } else {
            this.imagePotenzaDefault.setVisible(false);
            this.imagePressioneDefault.setVisible(false);
            this.imagePotenzaAlternativa.setVisible(true);
            this.imagePressioneAlternativa.setVisible(true);
        }
    }
	
    public void drawIstogrammi() {
        this.graficoPressione.ResetGrafico();
        this.graficoPotenza.ResetGrafico();
        this.graficoCondotto.ResetGrafico();
        try {
            if (this.l_VG.sfondoGraficiSpettro != null && this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag) {
                final InputStream insfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                this.graficoPotenza.setImgSfondo(insfondo);
                final InputStream insfondo1 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                this.graficoPressione.setImgSfondo(insfondo1);
                final InputStream insfondo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                this.graficoCondotto.setImgSfondo(insfondo2);
            } else {
                this.graficoPressione.setImgSfondo(null);
                this.graficoPotenza.setImgSfondo(null);
                this.graficoCondotto.setImgSfondo(null);
            }
        } catch (final Exception e) {}
        this.graficoPressione.setTipoIstogramma();
        this.graficoPotenza.setTipoIstogramma();
        this.graficoCondotto.setTipoIstogramma();
        if (vCurr.selezioneCorrente.nBoccheCanalizzate < 0) {
            vCurr.selezioneCorrente.nBoccheCanalizzate = vCurr.NBoccheCanalizzateBase;
        }
        if (vCurr.selezioneCorrente.distanzaSpettroSonorom < 0.) {
            vCurr.selezioneCorrente.distanzaSpettroSonorom = vCurr.DefautDistanzaSpettro;
        }
        this.l_vf = new VentilatoreFisica();
        this.l_vf.setVentilatoreFisica(this.l_VG.conTecnica, this.l_VG.dbTableQualifier.dbTableQualifierPreDBTecnico, this.l_VG.dbTableQualifier.dbTableQualifierPostDBTecnico, vCurr, this.l_VG.ventilatoreFisicaParameter);
        try {
            this.l_vf.setNumeroGiriPortatam3h(vCurr.selezioneCorrente.NumeroGiri, vCurr.selezioneCorrente.Marker);
            if (!this.l_vf.havePotenzaSonora()) return;
            if (!setSpettroSonoroBase(vCurr, this.rumore)) return;
        } catch (final Exception e) { return; }
        if ((this.rumore.RumoreBase <= 0.) || (this.rumore.RumoreBaseA <= 0.)) { return; }
        loadCampiCliente();
        vCurr.campiCliente = l_vcc;
        if (!this.rumore.calcolaSpettroCorrenteRumore(vCurr, this.l_vf, this.l_VG.ventilatoreFisicaParameter)) { return; }
        this.rumore.titoliBarreSpettroDefault[10] = this.l_VG.utilityTraduzioni.TraduciStringa("Totale");
        try {
            final String titoliBarre[] = this.l_uc.paccaTitoliSpettroSonoro(this.rumore.titoliBarreSpettroDefault);
            final String titoloX = this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza");
            String titoloY = this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Sonora");
            if (this.cb_unit_dBA.getValue()) {
                displayIstogrammaSpettroSonoro(this.graficoPotenza, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorrettoA), "[dB(A)]", titoloX, titoloY);
            } else {
                displayIstogrammaSpettroSonoro(this.graficoPotenza, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorretto), "[dB]", titoloX, titoloY);
            }
            this.l_vcc.potenzadB = this.rumore.SpettroCorrente[10];
            this.l_vcc.potenzadBA = this.rumore.SpettroCorrenteA[10];
            titoloY = this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora");
            if (this.cb_unit_dBA.getValue()) {
                displayIstogrammaSpettroSonoro(this.graficoPressione, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorrentePressioneA), "[dB(A)]", titoloX, titoloY);
            } else {
                displayIstogrammaSpettroSonoro(this.graficoPressione, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.SpettroCorrentePressione), "[dB]", titoloX, titoloY);
            }
            this.l_vcc.pressionedB = this.rumore.SpettroCorrentePressione[10];
            this.l_vcc.pressionedBA = this.rumore.SpettroCorrentePressioneA[10];
            titoloY = this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Sonora Immessa in un Condotto");
            if (this.cb_unit_dBA.getValue()) {
                displayIstogrammaSpettroSonoro(this.graficoCondotto, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.spettroPotenzaInCanaliA), "[dB(A)]", titoloX, titoloY);
            } else {
                displayIstogrammaSpettroSonoro(this.graficoCondotto, titoliBarre, this.l_uc.paccaSpettroSonoro(this.rumore.spettroPotenzaInCanali), "[dB]", titoloX, titoloY);
            }
            displayPressioneCorretta();
            final StreamResource graficopotenzasource = new StreamResource(this.graficoPotenza, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
            graficopotenzasource.setCacheTime(100);
            this.imagePotenzaDefault.setSource(graficopotenzasource);
            this.imagePotenzaAlternativa.setSource(graficopotenzasource);
            final StreamResource graficopressionesource = new StreamResource(this.graficoPressione, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
            graficopressionesource.setCacheTime(100);
            this.imagePressioneDefault.setSource(graficopressionesource);
            this.imagePressioneAlternativa.setSource(graficopressionesource);
            final StreamResource graficocondottosource = new StreamResource(this.graficoCondotto, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
            graficocondottosource.setCacheTime(100);
            this.imageCondotto.setSource(graficocondottosource);
        } catch (final Exception e) {}
        this.tf_dis.setEnabled(false);
        this.tf_dis.setValue(this.sl_dis.getValue());
        this.tf_dis.setEnabled(true);
    }
	
    private void loadCampiCliente() {
        //cerca di capire tipo Serie
        Serie s = l_VG.ElencoSerie.stream().filter(serie -> l_VG.VentilatoreCurrent.Serie.equals(serie.Serie)).findAny().orElse(null);
        if (s!= null)
            l_vcc.Tipo = s.Categoria == 1 ? "assiale" : "centrifugo";
        l_vcc.dBA = cb_unit_dBA.getValue();
        l_vcc.propagazioneFreesfFlag = cb_d2_SPH.getValue();
        l_vcc.propagazione12sfFlag = cb_d2_HEM.getValue();
        l_vcc.propagazione14sfFlag = cb_d2_DIH.getValue();
        l_vcc.propagazione18sfFlag = cb_d2_TRI.getValue();
        l_vcc.distanzam = l_VG.VentilatoreCurrent.selezioneCorrente.distanzaSpettroSonorom;
        l_vcc.campoLiberoFlag = cb_env_OpF.getValue();
        l_vcc.riverberoFlag = cb_env_Rev.getValue();
        l_vcc.riverbero = tf_env_Rev.getIntegerValue();
        l_vcc.rumoreFondo = tf_env_Bck.getIntegerValue();
        l_vcc.Tipo = ((Cliente01VentilatoreCampiCliente) vCurr.campiCliente).Tipo;
    }
	
    private void displayPressioneCorretta() {
        loadCampiCliente();
        double pressioneCorretta = 0.;
        if (this.cb_unit_dBA.getValue()) {
            pressioneCorretta = Math.pow(10., 0.1 * this.rumore.pressioneSonoraInstallazioneA);
        } else {
            pressioneCorretta = Math.pow(10., 0.1 * this.rumore.pressioneSonoraInstallazione);
        }
        pressioneCorretta += Math.pow(10., 0.1 * this.tf_env_Bck.getIntegerValue());
        pressioneCorretta = 10. * Math.log10(pressioneCorretta);
        if (this.cb_env_Rev.getValue()) {
            pressioneCorretta += this.tf_env_Rev.getIntegerValue();
        }
        this.l_VG.fmtNd.setMaximumFractionDigits(1);
        lbl_finalPressure_middle = "<b><font color='blue' size=5>" + this.l_VG.fmtNd.format(pressioneCorretta) + "</b></font>  ";
        lb_final_pressure.setValue(lbl_finalPressure_start + " : " + lbl_finalPressure_middle + lbl_finalPressure_end);
    }
	
    private void fillFlagFromCampiCliente() {
        this.cb_unit_DB.setEnabled(false);
        this.cb_unit_dBA.setEnabled(false);
        if (this.l_vcc.dBA) {
            cb_unit_dBA.setValue(true);
            cb_unit_DB.setValue(false);
            lb_env_Rev_UM.setValue("[dB(A)]");
            lb_env_Bck_UM.setValue("[dB(A)]");
            lbl_finalPressure_end = "±3 [dB(A)] </html>";
        } else {
            cb_unit_dBA.setValue(false);
            cb_unit_DB.setValue(true);
            lb_env_Rev_UM.setValue("[dB]");
            lb_env_Bck_UM.setValue("[dB]");
            lbl_finalPressure_end = "±3 [dB] </html>";
        }
        this.cb_unit_DB.setEnabled(true);
        this.cb_unit_dBA.setEnabled(true);
        this.cb_d2_HEM.setEnabled(false);
        this.cb_d2_DIH.setEnabled(false);
        this.cb_d2_TRI.setEnabled(false);
        this.cb_d2_HEM.setValue(this.l_vcc.propagazione12sfFlag);
        this.cb_d2_DIH.setValue(this.l_vcc.propagazione14sfFlag);
        this.cb_d2_TRI.setValue(this.l_vcc.propagazione18sfFlag);
        this.cb_d2_HEM.setEnabled(true);
        this.cb_d2_DIH.setEnabled(true);
        this.cb_d2_TRI.setEnabled(true);
        this.sl_dis.setEnabled(false);
        this.sl_dis.setResolution(1);
        this.sl_dis.setMin(1.);
        this.sl_dis.setMax(50.);
        this.sl_dis.setValue(this.l_vcc.distanzam);
        this.sl_dis.setEnabled(true);
        this.tf_dis.setEnabled(false);
        this.tf_dis.setValue(this.l_vcc.distanzam);
        this.tf_dis.setEnabled(true);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
        this.cb_env_OpF.setEnabled(false);
        this.cb_env_OpF.setValue(this.l_vcc.campoLiberoFlag);
        this.cb_env_OpF.setEnabled(true);
        this.cb_env_Rev.setEnabled(true);
        this.cb_env_Rev.setValue(this.l_vcc.riverberoFlag);
        this.cb_env_Rev.setEnabled(true);
        this.tf_env_Rev.setEnabled(false);
        if (this.l_vcc.riverbero > 0.) {
            this.tf_env_Rev.setValue(this.l_vcc.riverbero);
        } else {
            this.tf_env_Rev.setValue("");
        }
        this.tf_env_Rev.setEnabled(this.l_vcc.riverberoFlag);
        this.tf_env_Bck.setEnabled(false);
        if (this.l_vcc.rumoreFondo > 0.) {
            this.tf_env_Bck.setValue(this.l_vcc.rumoreFondo);
        } else {
            this.tf_env_Bck.setValue("");
        }
        this.tf_env_Bck.setEnabled(true);
    }
	
    private boolean setSpettroSonoroBase(final Ventilatore l_v, final Rumore rumore) {
        rumore.RumoreBase = 0.;
        rumore.RumoreBaseA = 0.;
        rumore.distanzaPressioneBase = l_v.DefautDistanzaSpettro;
        rumore.deltaPotenzaPressioneSonora = l_v.DeltaPotPressSonora;
        final int index = this.l_VG.ventilatoreFisicaParameter.getIndexSpettroSonoroSemplificato(l_v.CategoriaSpettro);
        if (index < 0) {
            return false;
        }
        System.arraycopy(this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).Valore, 0, rumore.SpettroBase, 0, rumore.SpettroBase.length);
        rumore.BFIm1 = this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).BFIm1;
        rumore.BFI = this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).BFI;
        rumore.BFIp1 = this.l_VG.ventilatoreFisicaParameter.elencoCategorieSpettroSonoroSemplificato.get(index).BFIp1;
        for (int i = 0; i < rumore.SpettroBase.length; i++) {
            rumore.SpettroBaseA[i] = rumore.SpettroBase[i] + this.l_VG.ventilatoreFisicaParameter.AScale[i];
        }
        rumore.RumoreBase = this.l_VG.utilityMath.RisultanteDaIstrogramma(rumore.SpettroBase);
        rumore.RumoreBaseA = this.l_VG.utilityMath.RisultanteDaIstrogramma(rumore.SpettroBaseA);
        return true;
    }

    private void displayIstogrammaSpettroSonoro(final jGraficoXYWeb grafico, final String titoliBarre[], final double spettro[], final String UM, final String titoloX, final String titoloY) {
        displayIstogrammaSpettroSonoro(grafico, titoliBarre, spettro, -1, UM, titoloX, titoloY);
    }

    private boolean displayIstogrammaSpettroSonoro(final jGraficoXYWeb grafico, final String titoliBarre[], final double spettro[], final int lenSpettro, final String UM, final String titoloX, String titoloY) {
        try {
            grafico.addAsseXIstogramma(titoliBarre, lenSpettro, titoloX + " [Hz]");
            titoloY += " ±3";
            int istoMax = -100000;
            for (int i = 0; i < spettro.length; i++) {
                if (istoMax < (int) (spettro[i] + 20.) / 10) {
                    istoMax = (int) (spettro[i] + 20.) / 10;
                }
            }
            if (istoMax <= 0) { return false; }
            grafico.addAsseYLineare(0., istoMax * 10., UM);
            titoloY += " " + UM;
            final int index = grafico.addIstogrammaRumore(spettro, lenSpettro, Color.YELLOW, Color.GREEN, true);
            grafico.setIstogrammaDecimali(index, 1);
            grafico.setTitoloGrafico(titoloY);
            grafico.repaint();
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    private void changedB_dA(boolean checkBoxValue, dB_Unit_Type source) {
        if (editEnabled) {
            if ((checkBoxValue && source == dB_Unit_Type.DB)||(!checkBoxValue && source == dB_Unit_Type.DBA)) {
                cb_unit_dBA.setEnabled(false);
                cb_unit_dBA.setValue(false);
                cb_unit_dBA.setEnabled(true);
                cb_unit_DB.setEnabled(false);
                cb_unit_DB.setValue(true);
                cb_unit_DB.setEnabled(true);
                lb_env_Rev_UM.setValue("[dB]");
                lb_env_Bck_UM.setValue("[dB]");
                lbl_finalPressure_end = "±3 [dB] </html>";
            } else {
                cb_unit_DB.setEnabled(false);
                cb_unit_DB.setValue(false);
                cb_unit_DB.setEnabled(true);
                cb_unit_dBA.setEnabled(false);
                cb_unit_dBA.setValue(true);
                cb_unit_dBA.setEnabled(true);
                lb_env_Rev_UM.setValue("[dB(A)]");
                lb_env_Bck_UM.setValue("[dB(A)]");
                lbl_finalPressure_end = "±3 [dB(A)] </html>";
            }
            lb_final_pressure.setValue(lbl_finalPressure_start + " : " + lbl_finalPressure_middle + lbl_finalPressure_end);
            drawIstogrammi();
        }
    }
    
    private void changePropagation(d2_Type source) {
        if (editEnabled) {
            editEnabled = false;
            switch (source) {
                case SPHERE:
                    cb_d2_SPH.setValue(true);
                    cb_d2_HEM.setValue(false);
                    cb_d2_DIH.setValue(false);
                    cb_d2_TRI.setValue(false);
                    break;
                case HEMISPHERE:
                    cb_d2_SPH.setValue(false);
                    cb_d2_HEM.setValue(true);
                    cb_d2_DIH.setValue(false);
                    cb_d2_TRI.setValue(false);
                    break;
                case DIHEDRON:
                    cb_d2_SPH.setValue(false);
                    cb_d2_HEM.setValue(false);
                    cb_d2_DIH.setValue(true);
                    cb_d2_TRI.setValue(false);
                    break;
                case TRIHEDRON:
                    cb_d2_SPH.setValue(false);
                    cb_d2_HEM.setValue(false);
                    cb_d2_DIH.setValue(false);
                    cb_d2_TRI.setValue(true);
                    break;
            }
            editEnabled = true;
            drawIstogrammi();
        }
    }
    
    private void changeDistance(distance_Type_Change source) {
        if (editEnabled) {
            switch (source) {
                case SLIDER:
                    l_VG.VentilatoreCurrent.selezioneCorrente.distanzaSpettroSonorom = sl_dis.getValue();
                    break;
                case BUTTON_MINUS:
                    if (sl_dis.getValue()-l_VG.ParametriVari.stepDistanzaRumorem >= 1.)
                        sl_dis.setValue(sl_dis.getValue()-l_VG.ParametriVari.stepDistanzaRumorem);
                    break;
                case BUTTON_PLUS:
                    if (sl_dis.getValue()+l_VG.ParametriVari.stepDistanzaRumorem <= 50.)
                        sl_dis.setValue(sl_dis.getValue()+l_VG.ParametriVari.stepDistanzaRumorem);
                    break;
                case TEXTFIELD:
                    if (tf_dis.getDoubleValue() < 1.)
                        tf_dis.setValue(1.);
                    else if (tf_dis.getDoubleValue() > 50.)
                        tf_dis.setValue(50.);
                    sl_dis.setValue(tf_dis.getDoubleValue());
                    break;
            }
            drawIstogrammi();
        }
    }
    
    private void changeEnvironment(environment_Change source) {
        if (editEnabled) {
            editEnabled = false;
            switch (source) {
                case TF:
                    break;
                case CB_OPEN_FIELD:
                    cb_env_OpF.setEnabled(false);
                    cb_env_Rev.setEnabled(false);
                    tf_env_Rev.setEnabled(false);
                    cb_env_OpF.setValue(true);
                    cb_env_Rev.setValue(false);
                    tf_env_Rev.setEnabled(false);
                    lb_env_Rev_UM.setEnabled(false);
                    tf_env_Rev.setValue(0);
                    cb_env_Rev.setEnabled(true);
                    cb_env_OpF.setEnabled(true);
                    tf_env_Rev.setEnabled(true);
                    break;
                case CB_REVERB:
                    cb_env_OpF.setEnabled(false);
                    cb_env_Rev.setEnabled(false);
                    tf_env_Rev.setEnabled(false);
                    cb_env_OpF.setValue(false);
                    cb_env_Rev.setValue(true);
                    tf_env_Rev.setEnabled(true);
                    lb_env_Rev_UM.setEnabled(true);
                    cb_env_Rev.setEnabled(true);
                    cb_env_OpF.setEnabled(true);
                    tf_env_Rev.setEnabled(true);
                    break;
            }
            editEnabled = true;
        }
        displayPressioneCorretta();
    }

    private void initUI() {
        this.hl_main = new XdevHorizontalLayout();
        this.vl_mainLeft = new XdevVerticalLayout();
        this.vl_info = new XdevVerticalLayout();
        this.lb_r1_Model = new XdevLabel();
        this.lb_r2_Arrangement = new XdevLabel();
        this.lb_r3_Impeller = new XdevLabel();
        this.lb_r4_Q = new XdevLabel();
        this.lb_r5_Ht = new XdevLabel();
        this.lb_r6_RPM = new XdevLabel();
        this.lb_r7_rho = new XdevLabel();
        this.pn_soundPower = new XdevPanel();
        this.hl_soundPower = new XdevHorizontalLayout();
        this.cb_unit_DB = new XdevCheckBox();
        this.cb_unit_dBA = new XdevCheckBox();
        this.pn_pressure = new XdevPanel();
        this.vl_Pressure = new XdevVerticalLayout();
        this.lb_Pressure_Propagation = new XdevLabel();
        this.cb_d2_SPH = new XdevCheckBox();
        this.cb_d2_HEM = new XdevCheckBox();
        this.cb_d2_DIH = new XdevCheckBox();
        this.cb_d2_TRI = new XdevCheckBox();
        this.lb_Pressure_Dist = new XdevLabel();
        this.hl_Pressure_Dist = new XdevHorizontalLayout();
        this.bt_dis_min = new XdevButton();
        this.tf_dis = new TextFieldDouble();
        this.bt_dis_plu = new XdevButton();
        this.sl_dis = new XdevSlider();
        gl_d2 = new XdevGridLayout(2,2);
        this.vl_grafici = new XdevVerticalLayout();
        this.imagePotenzaDefault = new XdevImage();
        this.imagePressioneDefault = new XdevImage();
        this.imagePotenzaAlternativa = new XdevImage();
        this.imagePressioneAlternativa = new XdevImage();
        this.vl_mainRight = new XdevVerticalLayout();
        this.imageCondotto = new XdevImage();
        this.pn_soundPowerInstallation = new XdevPanel();
        this.verticalLayout7 = new XdevVerticalLayout();
        lb_env_Rev_UM = new XdevLabel();
        this.lb_right_r1_pressure = new XdevLabel();
        this.gridLayout3 = new XdevGridLayout();
        this.gl_soundPowerInstallation = new XdevGridLayout(4, 5);
        this.lb_soundPowerInstallation_1_Environment = new XdevLabel();
        this.cb_env_OpF = new XdevCheckBox();
        this.cb_env_Rev = new XdevCheckBox();
        this.tf_env_Rev = new TextFieldDouble();
        this.lb_env_Bck = new XdevLabel();
        this.tf_env_Bck = new TextFieldDouble();
        this.lb_env_Bck_UM = new XdevLabel();
        this.lb_final_pressure = new XdevLabel();
        this.panel = new XdevPanel();
        this.verticalLayout8 = new XdevVerticalLayout();
        this.ta_Note = new XdevTextArea();
        this.hl_main.setMargin(new MarginInfo(false));
        this.vl_mainLeft.setMargin(new MarginInfo(false));
        this.vl_info.setMargin(new MarginInfo(false));
        this.lb_r1_Model.setValue("Modello");
        this.lb_r1_Model.setContentMode(ContentMode.HTML);
        this.lb_r2_Arrangement.setValue("Esecuzione");
        this.lb_r3_Impeller.setValue("Diametro Girante");
        this.lb_r3_Impeller.setContentMode(ContentMode.HTML);
        this.lb_r4_Q.setContentMode(ContentMode.HTML);
        this.lb_r5_Ht.setContentMode(ContentMode.HTML);
        this.lb_r6_RPM.setContentMode(ContentMode.HTML);
        this.lb_r7_rho.setContentMode(ContentMode.HTML);
        this.lb_r6_RPM.setValue("val");
        this.lb_r7_rho.setValue("<html>ρ<sub>Fluid</sub>:</html>");
        this.lb_r7_rho.setContentMode(ContentMode.HTML);
        this.pn_soundPower.setCaption("Potenza Sonora");
        this.pn_soundPower.setTabIndex(0);
        this.hl_soundPower.setMargin(new MarginInfo(false));
        this.cb_unit_DB.setCaption("dB");
        this.cb_unit_dBA.setCaption("dB(A)");
        this.pn_pressure.setCaption("Pressione Sonora");
        this.pn_pressure.setTabIndex(0);
        this.vl_Pressure.setMargin(new MarginInfo(false));
        this.lb_Pressure_Propagation.setValue("Propagazione");
        this.cb_d2_SPH.setCaption("Libera");
        this.cb_d2_HEM.setCaption("Emisfera");
        this.cb_d2_DIH.setCaption("Diedro");
        this.cb_d2_TRI.setCaption("Triedro");
        this.lb_Pressure_Dist.setValue("Distanza [m]");
        this.hl_Pressure_Dist.setMargin(new MarginInfo(false, true, false, true));
        this.bt_dis_min.setCaption("<<");
        this.bt_dis_min.setStyleName("small");
        this.bt_dis_plu.setCaption(">>");
        this.bt_dis_plu.setStyleName("small");
        this.vl_grafici.setMargin(new MarginInfo(false));
        this.vl_mainRight.setMargin(new MarginInfo(false));
        this.pn_soundPowerInstallation.setCaption("Pressione Sonora");
        this.verticalLayout7.setMargin(new MarginInfo(false, false, false, true));
        this.lb_right_r1_pressure.setValue("PInstallazione");
        this.lb_right_r1_pressure.setContentMode(ContentMode.HTML);
        this.gl_soundPowerInstallation.setMargin(new MarginInfo(false));
        this.lb_soundPowerInstallation_1_Environment.setValue("Ambiente");
        this.cb_env_OpF.setCaption("Campo libero");
        this.cb_env_Rev.setCaption("Riverberante");
        this.lb_env_Bck.setValue("Rumore di fondo");
        this.lb_env_Bck_UM.setValue("UM");
        this.lb_final_pressure.setValue("?");
        this.lb_final_pressure.setContentMode(ContentMode.HTML);
        ta_Note.setRows(3);
        this.panel.setCaption("");
        this.verticalLayout8.setMargin(new MarginInfo(false, true, false, true));
        //info
        lb_r1_Model.setSizeUndefined();
        lb_r2_Arrangement.setSizeUndefined();
        lb_r3_Impeller.setSizeUndefined();
        lb_r4_Q.setSizeUndefined();
        lb_r5_Ht.setSizeUndefined();
        lb_r6_RPM.setSizeUndefined();
        lb_r7_rho.setSizeUndefined();
        Alignment infoAlign = Alignment.TOP_CENTER;
        vl_info.addComponent(lb_r1_Model, infoAlign);
        vl_info.addComponent(lb_r2_Arrangement, infoAlign);
        vl_info.addComponent(lb_r3_Impeller, infoAlign);
        vl_info.addComponent(lb_r4_Q, infoAlign);
        vl_info.addComponent(lb_r5_Ht, infoAlign);
        vl_info.addComponent(lb_r6_RPM, infoAlign);
        vl_info.addComponent(lb_r7_rho, infoAlign);
        vl_info.setWidth(100, Sizeable.Unit.PERCENTAGE);
        vl_info.setHeight(-1, Sizeable.Unit.PIXELS);
        //sound Power
        cb_unit_DB.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_unit_DB.setHeight(-1, Sizeable.Unit.PIXELS);
        cb_unit_dBA.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_unit_dBA.setHeight(-1, Sizeable.Unit.PIXELS);
        hl_soundPower.addComponent(cb_unit_DB, Alignment.MIDDLE_CENTER);
        hl_soundPower.addComponent(cb_unit_dBA, Alignment.MIDDLE_CENTER);
        hl_soundPower.setExpandRatios(10.0F, 10.0F);
        hl_soundPower.setSizeFull();
        pn_soundPower.setContent(hl_soundPower);
        pn_soundPower.setWidth(100, Sizeable.Unit.PERCENTAGE);
        pn_soundPower.setHeight(-1, Sizeable.Unit.PIXELS);
        //sound Pressure
        lb_Pressure_Propagation.setSizeUndefined();
        cb_d2_SPH.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_d2_SPH.setHeight(-1, Sizeable.Unit.PIXELS);
        cb_d2_HEM.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_d2_HEM.setHeight(-1, Sizeable.Unit.PIXELS);
        cb_d2_DIH.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_d2_DIH.setHeight(-1, Sizeable.Unit.PIXELS);
        cb_d2_TRI.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_d2_TRI.setHeight(-1, Sizeable.Unit.PIXELS);
        gl_d2.setWidth("100%");
        gl_d2.addComponents(cb_d2_SPH, cb_d2_HEM, cb_d2_DIH, cb_d2_TRI);
        lb_Pressure_Dist.setSizeUndefined();
        bt_dis_min.setWidth(100, Sizeable.Unit.PERCENTAGE);
        bt_dis_min.setHeight(-1, Sizeable.Unit.PIXELS);
        tf_dis.setWidth(100, Sizeable.Unit.PERCENTAGE);
        tf_dis.setHeight(-1, Sizeable.Unit.PIXELS);
        bt_dis_plu.setWidth(100, Sizeable.Unit.PERCENTAGE);
        bt_dis_plu.setHeight(-1, Sizeable.Unit.PIXELS);
        hl_Pressure_Dist.addComponent(bt_dis_min, Alignment.MIDDLE_CENTER);
        hl_Pressure_Dist.addComponent(tf_dis, Alignment.MIDDLE_CENTER);
        hl_Pressure_Dist.addComponent(bt_dis_plu, Alignment.MIDDLE_CENTER);
        hl_Pressure_Dist.setExpandRatios(10F, 10F, 10F);
        hl_Pressure_Dist.setWidth(100, Sizeable.Unit.PERCENTAGE);
        hl_Pressure_Dist.setHeight(-1, Sizeable.Unit.PIXELS);
        sl_dis.setWidth(100, Sizeable.Unit.PERCENTAGE);
        sl_dis.setHeight(-1, Sizeable.Unit.PIXELS);
        vl_Pressure.addComponent(lb_Pressure_Propagation, Alignment.TOP_CENTER);
        vl_Pressure.addComponent(gl_d2);
        vl_Pressure.addComponent(lb_Pressure_Dist, Alignment.TOP_CENTER);
        vl_Pressure.addComponent(hl_Pressure_Dist, Alignment.MIDDLE_CENTER);
        vl_Pressure.addComponent(sl_dis);
        vl_Pressure.setSizeFull();
        pn_pressure.setContent(vl_Pressure);
        pn_pressure.setWidth(100, Sizeable.Unit.PERCENTAGE);
        pn_pressure.setHeight(-1, Sizeable.Unit.PIXELS);
        vl_mainLeft.addComponent(vl_info);
        vl_mainLeft.addComponent(pn_soundPower);
        vl_mainLeft.addComponent(pn_pressure);
        CustomComponent vl_Mainleft_spacer = new CustomComponent();
	vl_Mainleft_spacer.setSizeFull();
	vl_mainLeft.addComponent(vl_Mainleft_spacer);
	vl_mainLeft.setExpandRatio(vl_Mainleft_spacer, 1.0F);
        imagePotenzaDefault.setWidth(100, Sizeable.Unit.PERCENTAGE);
        imagePotenzaDefault.setHeight(-1, Sizeable.Unit.PIXELS);
        imagePressioneDefault.setWidth(100, Sizeable.Unit.PERCENTAGE);
        imagePressioneDefault.setHeight(-1, Sizeable.Unit.PIXELS);
        imagePotenzaAlternativa.setWidth(-1, Sizeable.Unit.PIXELS);
        imagePotenzaAlternativa.setHeight(100, Sizeable.Unit.PERCENTAGE);
        imagePressioneAlternativa.setWidth(-1, Sizeable.Unit.PIXELS);
        imagePressioneAlternativa.setHeight(100, Sizeable.Unit.PERCENTAGE);
        vl_grafici.addComponent(imagePotenzaDefault, Alignment.TOP_CENTER);
        vl_grafici.addComponent(imagePressioneDefault, Alignment.TOP_CENTER);
        vl_grafici.addComponent(imagePotenzaAlternativa, Alignment.TOP_CENTER);
        vl_grafici.addComponent(imagePressioneAlternativa, Alignment.TOP_CENTER);
        lb_soundPowerInstallation_1_Environment.setSizeUndefined();
        lb_env_Bck.setSizeUndefined();
        lb_env_Bck_UM.setSizeUndefined();
        lb_final_pressure.setSizeUndefined();
        cb_env_OpF.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_env_OpF.setHeight(-1, Sizeable.Unit.PIXELS);
        cb_env_Rev.setWidth(100, Sizeable.Unit.PERCENTAGE);
        cb_env_Rev.setHeight(-1, Sizeable.Unit.PIXELS);
        tf_env_Rev.setWidth(40, Sizeable.Unit.PIXELS);
        tf_env_Rev.setHeight(-1, Sizeable.Unit.PIXELS);
        tf_env_Bck.setWidth(40, Sizeable.Unit.PIXELS);
        tf_env_Bck.setHeight(-1, Sizeable.Unit.PIXELS);
        gl_soundPowerInstallation.addComponent(lb_soundPowerInstallation_1_Environment, 0, 0, 3, 0, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(cb_env_OpF, 0, 1, 3, 1, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(cb_env_Rev, 0, 2, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(tf_env_Rev, 1, 2, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(lb_env_Rev_UM, 2, 2, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(lb_env_Bck, 0, 3, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(tf_env_Bck, 1, 3, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(lb_env_Bck_UM, 2, 3, Alignment.MIDDLE_LEFT);
        gl_soundPowerInstallation.addComponent(lb_final_pressure, 0, 4, 4, 4, Alignment.MIDDLE_LEFT);
        
        gl_soundPowerInstallation.setColumnExpandRatios(4F, 0F, 0F, 15F);
         //this.gl_soundPowerInstallation.addComponent(this.labelPressioneCorrettaUM, 2, 3);
        final CustomComponent gridLayout2_vSpacer = new CustomComponent();
        gridLayout2_vSpacer.setSizeFull();
        //this.gl_soundPowerInstallation.addComponent(gridLayout2_vSpacer, 0, 4, 3, 4);
        this.lb_right_r1_pressure.setWidth(100, Sizeable.Unit.PERCENTAGE);
        this.lb_right_r1_pressure.setHeight(-1, Sizeable.Unit.PIXELS);
        this.verticalLayout7.addComponent(this.lb_right_r1_pressure);
        this.verticalLayout7.setComponentAlignment(this.lb_right_r1_pressure, Alignment.TOP_CENTER);
        this.verticalLayout7.setExpandRatio(this.lb_right_r1_pressure, 10.0F);
        this.gridLayout3.setWidth(100, Sizeable.Unit.PERCENTAGE);
        this.gridLayout3.setHeight(-1, Sizeable.Unit.PIXELS);
        this.verticalLayout7.addComponent(this.gridLayout3);
        this.verticalLayout7.setComponentAlignment(this.gridLayout3, Alignment.MIDDLE_CENTER);
        this.gl_soundPowerInstallation.setWidth(100, Sizeable.Unit.PERCENTAGE);
        this.gl_soundPowerInstallation.setHeight(-1, Sizeable.Unit.PIXELS);
        this.verticalLayout7.addComponent(gl_soundPowerInstallation);
        this.verticalLayout7.setSizeFull();
        this.pn_soundPowerInstallation.setContent(this.verticalLayout7);
        this.ta_Note.setSizeFull();
        this.verticalLayout8.addComponent(this.ta_Note);
        this.verticalLayout8.setExpandRatio(this.ta_Note, 10.0F);
        this.verticalLayout8.setSizeFull();
        this.panel.setContent(this.verticalLayout8);
        this.imageCondotto.setWidth(100, Sizeable.Unit.PERCENTAGE);
        this.imageCondotto.setHeight(-1, Sizeable.Unit.PIXELS);
        this.vl_mainRight.addComponent(this.imageCondotto);
        this.pn_soundPowerInstallation.setWidth(100, Sizeable.Unit.PERCENTAGE);
        this.pn_soundPowerInstallation.setHeight(-1, Sizeable.Unit.PIXELS);
        this.vl_mainRight.addComponent(this.pn_soundPowerInstallation);
        this.panel.setWidth(100, Sizeable.Unit.PERCENTAGE);
        this.panel.setHeight(-1, Sizeable.Unit.PIXELS);
        this.vl_mainRight.addComponent(this.panel);
        final CustomComponent verticalLayout3_spacer = new CustomComponent();
        verticalLayout3_spacer.setSizeFull();
        this.vl_mainRight.addComponent(verticalLayout3_spacer);
        this.vl_mainRight.setExpandRatio(verticalLayout3_spacer, 1.0F);
        this.vl_mainLeft.setSizeFull();
        this.hl_main.addComponent(this.vl_mainLeft);
        this.hl_main.setComponentAlignment(this.vl_mainLeft, Alignment.MIDDLE_CENTER);
        this.hl_main.setExpandRatio(this.vl_mainLeft, 10.0F);
        this.vl_grafici.setSizeFull();
        this.hl_main.addComponent(this.vl_grafici);
        this.hl_main.setComponentAlignment(this.vl_grafici, Alignment.MIDDLE_CENTER);
        this.hl_main.setExpandRatio(this.vl_grafici, 20.0F);
        this.vl_mainRight.setSizeFull();
        this.hl_main.addComponent(this.vl_mainRight);
        this.hl_main.setComponentAlignment(this.vl_mainRight, Alignment.MIDDLE_CENTER);
        this.hl_main.setExpandRatio(this.vl_mainRight, 14.0F);
        this.hl_main.setSizeFull();
        this.setContent(this.hl_main);
        this.setSizeFull();

        cb_unit_DB.addValueChangeListener(event -> changedB_dA((boolean)event.getProperty().getValue(), dB_Unit_Type.DB));
        cb_unit_dBA.addValueChangeListener(event -> changedB_dA((boolean)event.getProperty().getValue(), dB_Unit_Type.DBA));
        cb_d2_SPH.addValueChangeListener(event -> changePropagation(d2_Type.SPHERE));
        cb_d2_HEM.addValueChangeListener(event -> changePropagation(d2_Type.HEMISPHERE));
        cb_d2_DIH.addValueChangeListener(event -> changePropagation(d2_Type.DIHEDRON));
        cb_d2_TRI.addValueChangeListener(event -> changePropagation(d2_Type.TRIHEDRON));
        cb_env_OpF.addValueChangeListener(event -> changeEnvironment(environment_Change.CB_OPEN_FIELD));
        cb_env_Rev.addValueChangeListener(event -> changeEnvironment(environment_Change.CB_REVERB));
        tf_dis.addTextChangeListener(event -> changeDistance(distance_Type_Change.TEXTFIELD));
        tf_env_Rev.addTextChangeListener(event -> changeEnvironment(environment_Change.TF));
        tf_env_Bck.addTextChangeListener(event -> changeEnvironment(environment_Change.TF));
        bt_dis_min.addClickListener(event -> changeDistance(distance_Type_Change.BUTTON_MINUS));
        bt_dis_plu.addClickListener (event -> changeDistance(distance_Type_Change.BUTTON_PLUS));
        sl_dis.addValueChangeListener  (event -> changeDistance(distance_Type_Change.SLIDER));
    } 

    private XdevLabel lb_r1_Model, lb_r2_Arrangement, lb_r3_Impeller, lb_r4_Q, lb_r5_Ht, lb_r6_RPM, lb_r7_rho, lb_Pressure_Propagation, lb_Pressure_Dist, lb_right_r1_pressure,
            lb_soundPowerInstallation_1_Environment, lb_env_Rev_UM, lb_env_Bck, lb_env_Bck_UM, lb_final_pressure;
    private XdevButton bt_dis_min, bt_dis_plu;
    private XdevHorizontalLayout hl_main, hl_soundPower, hl_Pressure_Dist;
    private XdevImage imagePotenzaDefault, imagePressioneDefault, imagePotenzaAlternativa, imagePressioneAlternativa, imageCondotto;
    private XdevTextArea ta_Note;
    private com.cit.sellfan.ui.TextFieldDouble tf_dis, tf_env_Rev, tf_env_Bck;
    private XdevSlider sl_dis;
    private XdevPanel pn_soundPower, pn_pressure, pn_soundPowerInstallation, panel;
    private XdevCheckBox cb_unit_DB, cb_unit_dBA, cb_d2_SPH, cb_d2_HEM, cb_d2_DIH, cb_d2_TRI, cb_env_OpF, cb_env_Rev;
    private XdevGridLayout gl_d2, gridLayout3, gl_soundPowerInstallation;
    private XdevVerticalLayout vl_mainLeft, vl_info, vl_Pressure, vl_grafici, vl_mainRight, verticalLayout7, verticalLayout8;
    
    private enum dB_Unit_Type {
        DB,
        DBA
    }
    private enum d2_Type {
        SPHERE,
        HEMISPHERE,
        DIHEDRON,
        TRIHEDRON
    }
    private enum distance_Type_Change {
        BUTTON_PLUS,
        BUTTON_MINUS,
        SLIDER,
        TEXTFIELD
    }
    private enum environment_Change {
        TF,
        CB_OPEN_FIELD,
        CB_REVERB
    }
}