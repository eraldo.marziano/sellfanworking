package com.cit.sellfan.ui.view;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.pannelli.pannelloPasswordXUsiSpeciali;
import com.cit.sellfan.ui.view.cliente01.Cliente01SetupProtettiView;
import com.cit.sellfan.ui.view.cliente04.Cliente04SetupProtettiView;
import com.cit.sellfan.ui.view.setup.SetupCurveView;
import com.cit.sellfan.ui.view.setup.SetupGruppiSelezioneView;
import com.cit.sellfan.ui.view.setup.SetupUtilizzatoreView;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

import cit.sellfan.Costanti;
import com.cit.sellfan.ui.view.setup.SetupKitAccessori;
import com.vaadin.ui.Component;
import de.steinwedel.messagebox.MessageBox;

public class SetupView extends XdevView {
	private VariabiliGlobali l_VG;
	private SetupUtilizzatoreView setuputilizzatore = new SetupUtilizzatoreView();
	private SetupCurveView setupcurve = new SetupCurveView();
	private Cliente04SetupProtettiView setupprotetti04 = null;
	private Cliente01SetupProtettiView setupprotetti01 = null;
	private SetupGruppiSelezioneView setupgruppi = new SetupGruppiSelezioneView();
        private SetupKitAccessori setupKit = new SetupKitAccessori();
	private String coloreBottone = "verde";
	private String coloreBottoneSelezionato = "verdeselezionato";
	private MessageBox msgBox;
	private int indexTabCorrente = -1;
	private int indexTabUtilizzatore = 0;
	private int indexTabCurve = 1;
	private int indexTabGruppi = 2;
	private int indexTabProtetti = 3;
        private int indexTabKit = 4;
        boolean isProduction = false;

	/**
	 * 
	 */
	public SetupView() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(VariabiliGlobali variabiliGlobali) {
            l_VG = variabiliGlobali;
            l_VG.ElencoView[l_VG.SetupViewIndex] = this;
            setuputilizzatore.setVariabiliGlobali(l_VG);
            setupcurve.setVariabiliGlobali(l_VG);
            setupgruppi.setVariabiliGlobali(l_VG);
            setupKit.setVariabiliGlobali(l_VG);
            if (l_VG.currentUserSaaS.idClienteIndex == 4) {
                    setupprotetti04 = new Cliente04SetupProtettiView();
                    setupprotetti04.setVariabiliGlobali(l_VG);
            } else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
                    setupprotetti01 = new Cliente01SetupProtettiView();
                    setupprotetti01.setVariabiliGlobali(l_VG);
            }
            buttonKit.setVisible(!l_VG.isDonaldson() && l_VG.utilityCliente.isTabVisible("SetupKit"));
            buttonGruppi.setVisible(!l_VG.isDonaldson() && l_VG.utilityCliente.isTabVisible("SetupGruppi"));
            buttonProtetti.setVisible(l_VG.utilityCliente.isTabVisible("SetupProtettiVari"));
            if (l_VG.isDonaldson()) {
                buttonGruppi.setVisible(false);
                buttonKit.setVisible(false);
            }
	}
        
	public void Nazionalizza(boolean Produzione) {
            buttonGruppi.setVisible(l_VG.currentLivelloUtente == Costanti.utenteDefault);
            buttonUtilizzatore.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Utilizzatore"));
            buttonCurve.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Curve"));
            buttonGruppi.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Gruppi Serie"));
            buttonProtetti.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Protetti"));
            buttonReset.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Reset"));
            buttonOK.setCaption(l_VG.utilityTraduzioni.TraduciStringa("OK"));
            setuputilizzatore.Nazionalizza();
            setupcurve.Nazionalizza();
            if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) {
                    if (l_VG.currentUserSaaS.idClienteIndex == 4) {
                            setupprotetti04.Nazionalizza();
                    } else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
                            setupprotetti01.Nazionalizza();
                    }
            }
            if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) setupgruppi.Nazionalizza();
        }
	public void Nazionalizza() {
		buttonGruppi.setVisible(l_VG.currentLivelloUtente == Costanti.utenteDefault);
		buttonUtilizzatore.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Utilizzatore"));
		buttonCurve.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Curve"));
		buttonGruppi.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Gruppi Serie"));
		buttonProtetti.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Protetti"));
		buttonReset.setCaption(l_VG.utilityTraduzioni.TraduciStringa("Reset"));
		buttonOK.setCaption(l_VG.utilityTraduzioni.TraduciStringa("OK"));
		setuputilizzatore.Nazionalizza();
		setupcurve.Nazionalizza();
                if (l_VG.utilityCliente.isTabVisible("SetupKit")) setupKit.Nazionalizza();
		if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) {
			if (l_VG.currentUserSaaS.idClienteIndex == 4) {
				setupprotetti04.Nazionalizza();
			} else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
				setupprotetti01.Nazionalizza();
			}
		}
		if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) setupgruppi.Nazionalizza();
	}
	
        
        public void initSetup(boolean Produzione) {
        	indexTabCorrente = -1;
		setuputilizzatore.initSetup();
		setupcurve.initSetup();
                if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) {
			if (l_VG.currentUserSaaS.idClienteIndex == 4) {
				setupprotetti04.initSetup();
			} else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
				setupprotetti01.initSetup();
			}
		}
                buttonKit.setVisible(false);
		if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) setupgruppi.initSetup();
		buttonUtilizzatore_buttonClick(null);
	    
        }
        
	public void initSetup() {
		indexTabCorrente = -1;
		setuputilizzatore.initSetup();
		setupcurve.initSetup();
                if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) {
			if (l_VG.currentUserSaaS.idClienteIndex == 4) {
				setupprotetti04.initSetup();
			} else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
				setupprotetti01.initSetup();
			}
		}
                try {
                    l_VG.getClass().getField("ElencoKitAccessori");
                    isProduction = false;
                } catch (Exception e) {
                    isProduction = true;
                }
                if (!isProduction && l_VG.utilityCliente.isTabVisible("SetupKit")) {
                    setupKit.setVariabiliGlobali(l_VG);
                    setupKit.initSetup();
		} 
                buttonKit.setVisible(!isProduction);
		if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) setupgruppi.initSetup();
                if (l_VG.isDonaldson()) {
                    buttonCurve.setVisible(false);
                    buttonKit.setVisible(false);
                }
		buttonUtilizzatore_buttonClick(null);
	}
	
	private void resetColoreSelezionato() {
		resetColoreSelezionato(buttonUtilizzatore);
		resetColoreSelezionato(buttonCurve);
		resetColoreSelezionato(buttonProtetti);
		resetColoreSelezionato(buttonGruppi);
		resetColoreSelezionato(buttonKit);
	}
	
	private void resetColoreSelezionato(XdevButton bottone) {
		if (bottone.getStyleName().contains(coloreBottoneSelezionato)) {
			bottone.removeStyleName(coloreBottoneSelezionato);
			bottone.addStyleName(coloreBottone);
		}
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonUtilizzatore}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonUtilizzatore_buttonClick(Button.ClickEvent event) {
		if (indexTabCorrente == indexTabUtilizzatore) return;
		indexTabCorrente = indexTabUtilizzatore;
		resetColoreSelezionato();
		buttonUtilizzatore.removeStyleName(coloreBottone);
		buttonUtilizzatore.addStyleName(coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		setuputilizzatore.setSizeFull();
		this.gridLayout.addComponent(setuputilizzatore, 0, 0);
                buttonReset.setEnabled(true);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCurve}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCurve_buttonClick(Button.ClickEvent event) {
		if (indexTabCorrente == indexTabCurve) return;
		indexTabCorrente = indexTabCurve;
		resetColoreSelezionato();
		buttonCurve.removeStyleName(coloreBottone);
		buttonCurve.addStyleName(coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		setupcurve.setSizeFull();
		this.gridLayout.addComponent(setupcurve, 0, 0);
                buttonReset.setEnabled(true);
        }
        
        	private void buttonKit_buttonClick(Button.ClickEvent event) {
		if (indexTabCorrente == indexTabKit) return;
		indexTabCorrente = indexTabKit;
		resetColoreSelezionato();
		buttonKit.removeStyleName(coloreBottone);
		buttonKit.addStyleName(coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		setupKit.setSizeFull();
		this.gridLayout.addComponent(setupKit, 0, 0);
                buttonReset.setEnabled(false);
                this.setSizeFull();
	}


	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonReset_buttonClick(Button.ClickEvent event) {
            Component componenteAttivo = this.gridLayout.getComponent(0, 0);
            if (componenteAttivo == setuputilizzatore) 
                setuputilizzatore.azioneReset();
            if (componenteAttivo == setupcurve)
                setupcurve.azioneReset();
            if (componenteAttivo == setupgruppi) setupgruppi.azioneReset();
            if (componenteAttivo == setupKit) setupKit.azioneReset();
            if (componenteAttivo == setupprotetti04) setupprotetti04.azioneReset();
//		if (l_VG.utilityCliente.isTabVisible("SetupUtilizzatore")) setuputilizzatore.azioneReset();
//		if (l_VG.utilityCliente.isTabVisible("SetupCurve")) setupcurve.azioneReset();
//		if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) setupgruppi.azioneReset();
//		if (l_VG.utilityCliente.isTabVisible("SetupKit")) setupKit.azioneReset();
//		if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) {
//			if (l_VG.currentUserSaaS.idClienteIndex == 4) {
//				setupprotetti04.azioneReset();
//			} else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
//				setupprotetti01.azioneReset();
//			}
//		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonOK}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonOK_buttonClick(Button.ClickEvent event) {
            Component componenteAttivo = this.gridLayout.getComponent(0, 0);
            if (componenteAttivo == setuputilizzatore) 
                setuputilizzatore.azioneOK();
            if (componenteAttivo == setupcurve)
                setupcurve.azioneOK();
            if (componenteAttivo == setupgruppi)
                setupgruppi.azioneOK();
            if (componenteAttivo == setupKit)
                setupKit.azioneOK();
            if (componenteAttivo == setupprotetti04)
                setupprotetti04.azioneOK();
//
//            setuputilizzatore.azioneOK();
//        setupcurve.azioneOK();
//        if (l_VG.utilityCliente.isTabVisible("SetupKit")) setupKit.azioneOK();
//        if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) setupgruppi.azioneOK();
//        if (l_VG.utilityCliente.isTabVisible("SetupProtetti")) {
//            if (l_VG.currentUserSaaS.idClienteIndex == 4) {
//                setupprotetti04.azioneOK();
//            } else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
//                setupprotetti01.azioneOK();
//            }
//        }
      	msgBox = MessageBox.createInfo();
      	msgBox.withCaption(l_VG.utilityTraduzioni.TraduciStringa("Opzioni"));
       	msgBox.withMessage(l_VG.utilityTraduzioni.TraduciStringa("Fatto"));
       	msgBox.withOkButton();
       	msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonProtetti}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonProtetti_buttonClick(Button.ClickEvent event) {
		if (indexTabCorrente == indexTabProtetti) return;
		indexTabCorrente = indexTabProtetti;
		pannelloPasswordXUsiSpeciali pannelloPsw = new pannelloPasswordXUsiSpeciali();
		pannelloPsw.setVariabiliGlobali(l_VG);
		msgBox = MessageBox.create();
		msgBox.withMessage(pannelloPsw);
		msgBox.withAbortButton();
                msgBox.withOkButton(() -> {
			if (!pannelloPsw.isPasswordOK()) return;
			resetColoreSelezionato();
			buttonProtetti.removeStyleName(coloreBottone);
			buttonProtetti.addStyleName(coloreBottoneSelezionato);
			this.gridLayout.removeComponent(0, 0);
			setupcurve.setSizeFull();
                        buttonReset.setEnabled(true);
			if (l_VG.currentUserSaaS.idClienteIndex == 4) {
				this.gridLayout.addComponent(setupprotetti04, 0, 0);
			} else if (l_VG.currentUserSaaS.idClienteIndex == 1) {
				this.gridLayout.addComponent(setupprotetti01, 0, 0);
			}
			});
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonGruppi}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonGruppi_buttonClick(Button.ClickEvent event) {
		if (indexTabCorrente == indexTabGruppi) return;
		indexTabCorrente = indexTabGruppi;
		resetColoreSelezionato();
		buttonGruppi.removeStyleName(coloreBottone);
		buttonGruppi.addStyleName(coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		setupgruppi.setSizeFull();
                buttonReset.setEnabled(true);
		this.gridLayout.addComponent(setupgruppi, 0, 0);
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.buttonUtilizzatore = new XdevButton();
		this.buttonCurve = new XdevButton();
		this.buttonGruppi = new XdevButton();
		this.buttonProtetti = new XdevButton();
		this.gridLayout = new XdevGridLayout();
		this.panelToRemove = new XdevPanel();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.buttonReset = new XdevButton();
		this.buttonOK = new XdevButton();
                this.buttonKit = new XdevButton();
	
		this.verticalLayout.setStyleName("active");
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false, false, false, true));
		this.buttonUtilizzatore.setCaption("Utilizzatore");
		this.buttonUtilizzatore.setStyleName("verde");
		this.buttonCurve.setCaption("Curve");
		this.buttonCurve.setStyleName("verde");
		this.buttonGruppi.setCaption("Gruppi");
		this.buttonGruppi.setStyleName("verde");
		this.buttonProtetti.setCaption("Protetti");
                this.buttonKit.setCaption("Kit");
                this.buttonKit.setStyleName("verde");
		this.buttonProtetti.setStyleName("verde");
		this.gridLayout.setMargin(new MarginInfo(false));
		this.panelToRemove.setTabIndex(0);
		this.horizontalLayout2.setSpacing(false);
		this.horizontalLayout2.setMargin(new MarginInfo(false, false, false, true));
		this.buttonReset.setCaption("Reset");
		this.buttonOK.setCaption("OK");
	
		this.buttonUtilizzatore.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonUtilizzatore);
		this.horizontalLayout.setComponentAlignment(this.buttonUtilizzatore, Alignment.MIDDLE_CENTER);
		this.buttonCurve.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonCurve);
		this.horizontalLayout.setComponentAlignment(this.buttonCurve, Alignment.MIDDLE_CENTER);
		this.buttonGruppi.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonGruppi);
		this.horizontalLayout.setComponentAlignment(this.buttonGruppi, Alignment.MIDDLE_CENTER);
                this.buttonKit.setSizeUndefined();
                this.horizontalLayout.addComponent(this.buttonKit, Alignment.MIDDLE_CENTER);
		this.buttonProtetti.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonProtetti);
		this.horizontalLayout.setComponentAlignment(this.buttonProtetti, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout_spacer = new CustomComponent();
		horizontalLayout_spacer.setSizeFull();
		this.horizontalLayout.addComponent(horizontalLayout_spacer);
		this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
		this.gridLayout.setColumns(1);
		this.gridLayout.setRows(1);
		this.panelToRemove.setSizeFull();
		this.gridLayout.addComponent(this.panelToRemove, 0, 0);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setRowExpandRatio(0, 10.0F);
		this.buttonReset.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonReset);
		this.horizontalLayout2.setComponentAlignment(this.buttonReset, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.buttonReset, 10.0F);
		this.buttonOK.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonOK);
		this.horizontalLayout2.setComponentAlignment(this.buttonOK, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.buttonOK, 10.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(35, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.gridLayout.setSizeFull();
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.gridLayout, 10.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(35, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.buttonUtilizzatore.addClickListener(event -> this.buttonUtilizzatore_buttonClick(event));
		this.buttonCurve.addClickListener(event -> this.buttonCurve_buttonClick(event));
		this.buttonGruppi.addClickListener(event -> this.buttonGruppi_buttonClick(event));
		this.buttonProtetti.addClickListener(event -> this.buttonProtetti_buttonClick(event));
		this.buttonReset.addClickListener(event -> this.buttonReset_buttonClick(event));
		this.buttonOK.addClickListener(event -> this.buttonOK_buttonClick(event));
		this.buttonKit.addClickListener(event -> this.buttonKit_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonUtilizzatore, buttonCurve, buttonGruppi, buttonProtetti, buttonReset, buttonOK, buttonKit;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2;
	private XdevPanel panelToRemove;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>


}
