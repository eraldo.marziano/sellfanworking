package com.cit.sellfan.ui.view;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.template.jGraficoXYWeb;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.components.colorpicker.ColorChangeEvent;
import com.vaadin.ui.components.colorpicker.ColorChangeListener;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevColorPicker;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.Costanti;
import cit.sellfan.classi.grafici.GraficoPrestazioniConfronto;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import de.steinwedel.messagebox.MessageBox;

public class CompareView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
	private final Container containerVentilatori = new IndexedContainer();
	private final jGraficoXYWeb grafico = new jGraficoXYWeb();
    @SuppressWarnings("unused")
	private final double XPortata[] = null;
    @SuppressWarnings("unused")
	private final double YPressioneStatica[] = null;
    @SuppressWarnings("unused")
	private final double YPressioneTotale[] = null;
    @SuppressWarnings("unused")
    private final double YPotenzaAssorbita[] = null;
    @SuppressWarnings("unused")
    private final double YRendimento[] = null;
    @SuppressWarnings("unused")
    private final double YRumore[] = null;
    @SuppressWarnings("unused")
    private final int IndexAssePortata = -1;
    @SuppressWarnings("unused")
    private final int IndexAssePressione = -1;
    @SuppressWarnings("unused")
    private final int IndexAssePotenza = -1;
    @SuppressWarnings("unused")
    private final int IndexAsseRendimento = -1;
    @SuppressWarnings("unused")
    private final int IndexAssePotenzaSonora = -1;
    @SuppressWarnings("unused")
    private final int NPuntiQuartiche = 200;
    @SuppressWarnings("unused")
    private double contributoXRumore;
    @SuppressWarnings("unused")
    private final VentilatoreFisica ventilatoreFisica = new VentilatoreFisica();
    @SuppressWarnings("unused")
    private final ArrayList<Integer> indexPressioneStatica = new ArrayList<>();
    @SuppressWarnings("unused")
    private final ArrayList<Integer> indexPressioneTotale = new ArrayList<>();
    @SuppressWarnings("unused")
    private final ArrayList<Integer> indexPotenza = new ArrayList<>();
    @SuppressWarnings("unused")
    private final ArrayList<Integer> indexRendimento = new ArrayList<>();
    @SuppressWarnings("unused")
    private final ArrayList<Integer> indexCurvaCaratteristica = new ArrayList<>();
    @SuppressWarnings("unused")
    private final ArrayList<Integer> indexRumore = new ArrayList<>();
    private final XdevColorPicker ventilatoreColor[] = new XdevColorPicker[8];
    private String IndexVentilatoreSelezionatoStr = null;
    private GraficoPrestazioniConfronto graficoConfronto;
	/**
	 * 
	 */
	public CompareView() {
		super();
		this.initUI();
		this.containerVentilatori.removeAllItems();
		this.containerVentilatori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerVentilatori.addContainerProperty("Number", Integer.class, 0);
		this.containerVentilatori.addContainerProperty("Prezzo", Double.class, 0.);
		this.containerVentilatori.addContainerProperty("Ventilatore", String.class, "");
		this.containerVentilatori.addContainerProperty("Motore", String.class, "");
		this.containerVentilatori.addContainerProperty("Portata", Double.class, 0.);
		this.containerVentilatori.addContainerProperty("PressTot", Double.class, 0.);
		this.containerVentilatori.addContainerProperty("PressStat", Double.class, 0.);
		this.containerVentilatori.addContainerProperty("Potenza", Double.class, 0.);
		this.containerVentilatori.addContainerProperty("Rpm", Integer.class, 0);
		this.containerVentilatori.addContainerProperty("Rendimento", Double.class, 0.);
		this.containerVentilatori.addContainerProperty("PotenzaSon", Double.class, 0.);
		this.containerVentilatori.addContainerProperty("PressioneSon", Double.class, 0.);
		this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
		this.ventilatoreColor[0] = this.colorPicker1;
		this.ventilatoreColor[1] = this.colorPicker2;
		this.ventilatoreColor[2] = this.colorPicker3;
		this.ventilatoreColor[3] = this.colorPicker4;
		this.ventilatoreColor[4] = this.colorPicker5;
		this.ventilatoreColor[5] = this.colorPicker6;
		this.ventilatoreColor[6] = this.colorPicker7;
		this.ventilatoreColor[7] = this.colorPicker8;
	}

	private void setConverterForTable() {
		final myStringToDoubleConverter converter = new myStringToDoubleConverter();
		this.tableVentilatori.setConverter("Prezzo", converter);
		this.tableVentilatori.setConverter("Portata", converter);
		this.tableVentilatori.setConverter("PressTot", converter);
		this.tableVentilatori.setConverter("PressStat", converter);
		this.tableVentilatori.setConverter("Potenza", converter);
		this.tableVentilatori.setConverter("Rpm", converter);
		this.tableVentilatori.setConverter("Rendimento", converter);
		this.tableVentilatori.setConverter("PotenzaSon", converter);
		this.tableVentilatori.setConverter("PressioneSon", converter);
	}
	
	private class myStringToDoubleConverter extends StringToDoubleConverter {
		public myStringToDoubleConverter() {
			super();
		}
		
		@Override
		public java.text.NumberFormat getFormat(final Locale locale) {
			return CompareView.this.l_VG.fmtNdTable;
		}
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoView[this.l_VG.CompareViewIndex] = this;
		this.checkBoxPStatica.setCaption(StringheUNI.PRESSIONESTATICA);
		this.checkBoxPTotale.setCaption(StringheUNI.PRESSIONETOTALE);
		this.checkBoxPotenza.setCaption(StringheUNI.POTENZAASSE);
		this.checkBoxRendimento.setCaption(StringheUNI.RENDIMENTO);
		setConverterForTable();
	}
	
	public void resetView() {
		try {
			this.l_VG.ElencoVentilatoriFromCompare.clear();
			initTabellaVentilatori();
		} catch (final Exception e) {
			
		}
	}
        
        public void togglePreventivoButton(boolean value) {
            buttonAggiungiPreventivo.setEnabled(value);
        }
	
	public boolean isPreSetVentilatoreCurrentOK() {
		return this.isPannelloInizializzato;
	}
	
	public void Nazionalizza() {
		this.buttonAggiungiPreventivo.setVisible(this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
		this.tableVentilatori.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableVentilatori.setColumnHeader("Number", "#");
		this.tableVentilatori.setColumnHeader("Prezzo", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo") + " " + Costanti.EURO);
		this.tableVentilatori.setColumnHeader("Ventilatore", this.l_VG.utilityTraduzioni.TraduciStringa("Modello"));
		this.tableVentilatori.setColumnHeader("Motore", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.tableVentilatori.setColumnHeader("Portata", this.l_VG.utilityTraduzioni.TraduciStringa("Portata"));
		this.tableVentilatori.setColumnHeader("PressTot", this.l_VG.utilityTraduzioni.TraduciStringa("P. Tot."));
		this.tableVentilatori.setColumnHeader("PressStat", this.l_VG.utilityTraduzioni.TraduciStringa("P. Stat."));
		this.tableVentilatori.setColumnHeader("Potenza", this.l_VG.utilityTraduzioni.TraduciStringa("Pt. ass."));
		this.tableVentilatori.setColumnHeader("Rpm", this.l_VG.utilityTraduzioni.TraduciStringa("N. giri"));
		this.tableVentilatori.setColumnHeader("Rendimento", this.l_VG.utilityTraduzioni.TraduciStringa("Rendimento"));
		this.tableVentilatori.setColumnHeader("PotenzaSon", this.l_VG.utilityTraduzioni.TraduciStringa("Pt. son."));
		this.tableVentilatori.setColumnHeader("PressioneSon", this.l_VG.utilityTraduzioni.TraduciStringa("Ps. son."));
		this.buttonAggiungiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
		this.buttonReset.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset"));
		this.buttonCancellaVentilatore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella Ventilatore"));
		if (this.l_VG.ParametriVari.pannelloPrestazioniPressioneSonoraVisibileFlag) {
			this.checkBoxPressioneSonora.setCaption(StringheUNI.PRESSIONESONORA);
		} else {
			this.checkBoxPressioneSonora.setCaption(StringheUNI.POTENZASONORA);
		}
		this.checkBoxCurvaCaratteristica.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Curva Caratteristica"));
		buildGrafico();
	}
	
	public void initCompare () {
		initTabellaVentilatori();
	}

	class RemindTask extends TimerTask {
		@Override
		public void run() {
			CompareView.this.buttonAggiungiPreventivo.setIcon(null);
		}
	}

	private void initTabellaVentilatori() {
		this.isPannelloInizializzato = false;
		if (!this.l_VG.utilityCliente.isTabVisible("Confronto")) {
			return;
		}
		@SuppressWarnings("unused")
		final Item item;
		this.tableVentilatori.setEnabled(false);
		try {
			this.tableVentilatori.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
		}
		double prezzo = 0.;
		for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromCompare.size() ; i++) {
			prezzo = 0.;
			final Object obj[] = new Object[13];
			final String itemID = Integer.toString(i);
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromCompare.get(i);
			final CheckBox cb = new CheckBox();
			cb.setData(itemID);
			cb.setValue(l_v.Selezionato);
			cb.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						l_v.Selezionato = cb.getValue().booleanValue();
						buildGrafico();
					}
		    });
			obj[0] = cb;
			obj[1] = i + 1;
            for (int j=0 ; j<l_v.Esecuzioni.length ; j++) {
                if (l_v.Esecuzioni[j].equals(l_v.selezioneCorrente.Esecuzione)) {
                    prezzo = l_v.PrezzoEsecuzioni[j];
                    break;
                }
            }
            obj[2] = prezzo;
			obj[3] = this.l_VG.buildModelloCompleto(l_v);
			obj[4] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
			obj[5] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.Marker);
			obj[6] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PressioneTotale);
			obj[7] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PressioneStatica);
			obj[8] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.Potenza);
			obj[9] = (int)l_v.selezioneCorrente.NumeroGiri;
			obj[10] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.Rendimento, 2);
			obj[11] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PotenzaSonora, this.l_VG.NDecimaliRumore);
			if (l_v.selezioneCorrente.PressioneSonora > 0.) {
				obj[12] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PressioneSonora, this.l_VG.NDecimaliRumore);
			} else {
				obj[12] = null;
			}
			this.tableVentilatori.addItem(obj, itemID);
		}
		this.tableVentilatori.setEnabled(true);
		this.buttonCancellaVentilatore.setEnabled(false);
		buildGrafico();
		this.isPannelloInizializzato = true;
	}
	
	public java.awt.Color[] getColori() {
		final java.awt.Color color[] = new java.awt.Color[8];
		for (int i=0 ; i<8 ; i++) {
        	final Color colore = this.ventilatoreColor[i].getColor();
        	color[i] = new java.awt.Color(colore.getRed(), colore.getGreen(), colore.getBlue());
		}
		return color;
	}
	
	private void buildGrafico() {
		final java.awt.Color color[] = getColori();
        this.graficoConfronto = new GraficoPrestazioniConfronto();
        this.graficoConfronto.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.utilityCliente, this.l_VG.utilityTraduzioni, this.l_VG.currentCitFont.linguaDisplayFont);
        //graficoConfronto.initUM(l_VG.UMPortataCorrente, l_VG.UMPressioneCorrente, l_VG.UMPotenzaCorrente, "[dB]");
        this.graficoConfronto.initParametriVari(this.l_VG.ventilatoreFisicaParameter, this.l_VG.ParametriVari.pannelloPrestazioniPressioneSonoraVisibileFlag, color);
        this.graficoConfronto.buildGrafico(this.l_VG.assiGraficiDefault, this.grafico, this.l_VG.ElencoVentilatoriFromCompare);
        hideCurve();
		int nV;
		for (nV=0 ; nV<this.graficoConfronto.getNVentilatoriVisualizzati() ; nV++) {
			this.ventilatoreColor[nV].setVisible(true);
			this.ventilatoreColor[nV].setCaption(this.graficoConfronto.getNomeVentilatoriVisualizzati(nV));
		}
		for ( ; nV<8 ; nV++) {
			this.ventilatoreColor[nV].setVisible(false);
		}
	}
	
	public boolean[] getCirve() {
		final boolean curve[] = new boolean[6];
		curve[0] = this.checkBoxPStatica.getValue();
		curve[1] = this.checkBoxPTotale.getValue();
		curve[2] = this.checkBoxPotenza.getValue();
		curve[3] = this.checkBoxRendimento.getValue();
		curve[4] = this.checkBoxCurvaCaratteristica.getValue();
		curve[5] = this.checkBoxPressioneSonora.getValue();
		return curve;
	}
	
	private void hideCurve() {
        this.graficoConfronto.showhideCurve(this.checkBoxPStatica.getValue(), this.checkBoxPTotale.getValue(), this.checkBoxPotenza.getValue(), this.checkBoxRendimento.getValue(), this.checkBoxCurvaCaratteristica.getValue(), this.checkBoxPressioneSonora.getValue());
		final StreamResource graficosource = new StreamResource(this.grafico, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		graficosource.setCacheTime(100);
		this.imageGrafico.setSource(graficosource);
	}
	
	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableVentilatori}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableVentilatori_valueChange(final Property.ValueChangeEvent event) {
		try {
			if (this.l_VG.debugFlag) {
				Notification.show(event.getProperty().getValue().toString());
			}
			this.IndexVentilatoreSelezionatoStr = event.getProperty().getValue().toString();
			this.buttonCancellaVentilatore.setEnabled(true);
		} catch (final Exception e) {
			this.IndexVentilatoreSelezionatoStr = null;
			this.buttonCancellaVentilatore.setEnabled(false);
			//l_VG.MainView.setMemo(e.toString());
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxPStatica}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxPStatica_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxPTotale}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxPTotale_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxPotenza}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxPotenza_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxRendimento}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxRendimento_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCurvaCaratteristica}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCurvaCaratteristica_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxPressioneSonora}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxPressioneSonora_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAggiungiPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAggiungiPreventivo_buttonClick(final Button.ClickEvent event) {
		for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromCompare.size() ; i++) {
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromCompare.get(i);
			if (l_v.Selezionato) {
				this.l_VG.addVentilatoreToPreventivo(l_v);
				final MessageBox msgBox = MessageBox.createInfo();
				msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Fatto"));
				msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatore aggiunto al Preventivo corrente"));
				msgBox.withOkButton();
				msgBox.open();
			}
		}
		this.buttonAggiungiPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
		final Timer timer = new Timer();
	    timer.schedule(new RemindTask(), 1000);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCancellaVentilatore}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCancellaVentilatore_buttonClick(final Button.ClickEvent event) {
		this.l_VG.ElencoVentilatoriFromCompare.remove(Integer.parseInt(this.IndexVentilatoreSelezionatoStr));
		initTabellaVentilatori();
		this.IndexVentilatoreSelezionatoStr = null;
		this.buttonCancellaVentilatore.setEnabled(false);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonReset_buttonClick(final Button.ClickEvent event) {
		this.l_VG.ElencoVentilatoriFromCompare.clear();
		initTabellaVentilatori();
		this.IndexVentilatoreSelezionatoStr = null;
		this.buttonCancellaVentilatore.setEnabled(false);
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker1}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker1_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker2}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker2_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker3}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker3_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker4}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker4_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker5}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker5_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker6}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker6_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker7}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker7_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	
	}

	/**
	 * Event handler delegate method for the {@link XdevColorPicker}
	 * {@link #colorPicker8}.
	 *
	 * @see ColorChangeListener#colorChanged(ColorChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void colorPicker8_colorChanged(final ColorChangeEvent event) {
		buildGrafico();
	
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.tableVentilatori = new XdevTable<>();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.buttonAggiungiPreventivo = new XdevButton();
		this.buttonReset = new XdevButton();
		this.buttonCancellaVentilatore = new XdevButton();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.imageGrafico = new XdevImage();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.checkBoxPStatica = new XdevCheckBox();
		this.checkBoxPTotale = new XdevCheckBox();
		this.checkBoxPotenza = new XdevCheckBox();
		this.checkBoxRendimento = new XdevCheckBox();
		this.checkBoxCurvaCaratteristica = new XdevCheckBox();
		this.checkBoxPressioneSonora = new XdevCheckBox();
		this.labelSpazio = new XdevLabel();
		this.colorPicker1 = new XdevColorPicker();
		this.colorPicker2 = new XdevColorPicker();
		this.colorPicker3 = new XdevColorPicker();
		this.colorPicker4 = new XdevColorPicker();
		this.colorPicker5 = new XdevColorPicker();
		this.colorPicker6 = new XdevColorPicker();
		this.colorPicker7 = new XdevColorPicker();
		this.colorPicker8 = new XdevColorPicker();
	
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.tableVentilatori.setStyleName("small mystriped");
		this.horizontalLayout4.setMargin(new MarginInfo(false));
		this.buttonAggiungiPreventivo.setCaption("Aggiungi al Preventivo");
		this.buttonAggiungiPreventivo.setStyleName("big giallo");
		this.buttonAggiungiPreventivo.setId("buttonAggiungiPreventivo");
		this.buttonReset.setCaption("Reset");
		this.buttonReset.setStyleName("small");
		this.buttonCancellaVentilatore.setCaption("Cancella Ventilatore");
		this.buttonCancellaVentilatore.setStyleName("small");
		this.horizontalLayout2.setMargin(new MarginInfo(false));
		this.horizontalLayout3.setSpacing(false);
		this.horizontalLayout3.setMargin(new MarginInfo(false));
		this.verticalLayout2.setSpacing(false);
		this.verticalLayout2.setMargin(new MarginInfo(false, false, false, true));
		this.checkBoxPStatica.setCaption("P. Statica");
		this.checkBoxPStatica.setCaptionAsHtml(true);
		this.checkBoxPStatica.setValue(true);
		this.checkBoxPTotale.setCaption("P. Totale");
		this.checkBoxPTotale.setCaptionAsHtml(true);
		this.checkBoxPTotale.setValue(true);
		this.checkBoxPotenza.setCaption("Potenza");
		this.checkBoxPotenza.setCaptionAsHtml(true);
		this.checkBoxPotenza.setValue(true);
		this.checkBoxRendimento.setCaption("Rendimento");
		this.checkBoxRendimento.setCaptionAsHtml(true);
		this.checkBoxRendimento.setValue(true);
		this.checkBoxCurvaCaratteristica.setCaption("Curva Caratteristica");
		this.checkBoxCurvaCaratteristica.setCaptionAsHtml(true);
		this.checkBoxCurvaCaratteristica.setValue(true);
		this.checkBoxPressioneSonora.setCaption("Pressione Sonora");
		this.checkBoxPressioneSonora.setCaptionAsHtml(true);
		this.checkBoxPressioneSonora.setValue(true);
		this.labelSpazio.setValue(" ");
		this.colorPicker1.setColor(Color.RED);
		this.colorPicker1.setCaption("n 1");
		this.colorPicker1.setStyleName("small");
		this.colorPicker2.setColor(Color.CYAN);
		this.colorPicker2.setCaption("n 2");
		this.colorPicker2.setStyleName("small");
		this.colorPicker3.setColor(new Color(255, 165, 0));
		this.colorPicker3.setCaption("n 3");
		this.colorPicker3.setStyleName("small");
		this.colorPicker4.setColor(new Color(46, 139, 87));
		this.colorPicker4.setCaption("n 4");
		this.colorPicker4.setStyleName("small");
		this.colorPicker5.setColor(Color.BLUE);
		this.colorPicker5.setCaption("n 5");
		this.colorPicker5.setStyleName("small");
		this.colorPicker6.setColor(Color.YELLOW);
		this.colorPicker6.setCaption("n 6");
		this.colorPicker6.setStyleName("small");
		this.colorPicker7.setColor(Color.BLACK);
		this.colorPicker7.setCaption("n 7");
		this.colorPicker7.setStyleName("small");
		this.colorPicker8.setColor(Color.MAGENTA);
		this.colorPicker8.setCaption("n 8");
		this.colorPicker8.setStyleName("small");
	
		this.tableVentilatori.setSizeFull();
		this.horizontalLayout.addComponent(this.tableVentilatori);
		this.horizontalLayout.setExpandRatio(this.tableVentilatori, 10.0F);
		this.buttonAggiungiPreventivo.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.buttonAggiungiPreventivo);
		this.horizontalLayout4.setComponentAlignment(this.buttonAggiungiPreventivo, Alignment.MIDDLE_CENTER);
		this.buttonReset.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.buttonReset);
		this.horizontalLayout4.setComponentAlignment(this.buttonReset, Alignment.MIDDLE_CENTER);
		this.buttonCancellaVentilatore.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.buttonCancellaVentilatore);
		this.horizontalLayout4.setComponentAlignment(this.buttonCancellaVentilatore, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout4_spacer = new CustomComponent();
		horizontalLayout4_spacer.setSizeFull();
		this.horizontalLayout4.addComponent(horizontalLayout4_spacer);
		this.horizontalLayout4.setExpandRatio(horizontalLayout4_spacer, 1.0F);
		this.checkBoxPStatica.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxPStatica.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.checkBoxPStatica);
		this.verticalLayout2.setExpandRatio(this.checkBoxPStatica, 10.0F);
		this.checkBoxPTotale.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxPTotale.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.checkBoxPTotale);
		this.checkBoxPotenza.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxPotenza.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.checkBoxPotenza);
		this.checkBoxRendimento.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxRendimento.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.checkBoxRendimento);
		this.checkBoxCurvaCaratteristica.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxCurvaCaratteristica.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.checkBoxCurvaCaratteristica);
		this.checkBoxPressioneSonora.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxPressioneSonora.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.checkBoxPressioneSonora);
		this.labelSpazio.setSizeUndefined();
		this.verticalLayout2.addComponent(this.labelSpazio);
		this.verticalLayout2.setComponentAlignment(this.labelSpazio, Alignment.MIDDLE_CENTER);
		this.colorPicker1.setWidth(161, Unit.PIXELS);
		this.colorPicker1.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker1);
		this.colorPicker2.setWidth(161, Unit.PIXELS);
		this.colorPicker2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker2);
		this.colorPicker3.setWidth(161, Unit.PIXELS);
		this.colorPicker3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker3);
		this.colorPicker4.setWidth(161, Unit.PIXELS);
		this.colorPicker4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker4);
		this.colorPicker5.setWidth(161, Unit.PIXELS);
		this.colorPicker5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker5);
		this.colorPicker6.setWidth(161, Unit.PIXELS);
		this.colorPicker6.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker6);
		this.colorPicker7.setWidth(161, Unit.PIXELS);
		this.colorPicker7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker7);
		this.colorPicker8.setWidth(161, Unit.PIXELS);
		this.colorPicker8.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.colorPicker8);
		this.imageGrafico.setSizeFull();
		this.horizontalLayout3.addComponent(this.imageGrafico);
		this.horizontalLayout3.setExpandRatio(this.imageGrafico, 40.0F);
		this.verticalLayout2.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.verticalLayout2);
		this.horizontalLayout3.setExpandRatio(this.verticalLayout2, 10.0F);
		this.horizontalLayout3.setSizeFull();
		this.horizontalLayout2.addComponent(this.horizontalLayout3);
		this.horizontalLayout2.setComponentAlignment(this.horizontalLayout3, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.horizontalLayout3, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setExpandRatio(this.horizontalLayout, 10.0F);
		this.horizontalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout4);
		this.horizontalLayout2.setSizeFull();
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.horizontalLayout2, 20.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.tableVentilatori.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				CompareView.this.tableVentilatori_valueChange(event);
			}
		});
		this.buttonAggiungiPreventivo.addClickListener(event -> this.buttonAggiungiPreventivo_buttonClick(event));
		this.buttonReset.addClickListener(event -> this.buttonReset_buttonClick(event));
		this.buttonCancellaVentilatore.addClickListener(event -> this.buttonCancellaVentilatore_buttonClick(event));
		this.checkBoxPStatica.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				CompareView.this.checkBoxPStatica_valueChange(event);
			}
		});
		this.checkBoxPTotale.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				CompareView.this.checkBoxPTotale_valueChange(event);
			}
		});
		this.checkBoxPotenza.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				CompareView.this.checkBoxPotenza_valueChange(event);
			}
		});
		this.checkBoxRendimento.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				CompareView.this.checkBoxRendimento_valueChange(event);
			}
		});
		this.checkBoxCurvaCaratteristica.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				CompareView.this.checkBoxCurvaCaratteristica_valueChange(event);
			}
		});
		this.checkBoxPressioneSonora.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				CompareView.this.checkBoxPressioneSonora_valueChange(event);
			}
		});
		this.colorPicker1.addColorChangeListener(event -> this.colorPicker1_colorChanged(event));
		this.colorPicker2.addColorChangeListener(event -> this.colorPicker2_colorChanged(event));
		this.colorPicker3.addColorChangeListener(event -> this.colorPicker3_colorChanged(event));
		this.colorPicker4.addColorChangeListener(event -> this.colorPicker4_colorChanged(event));
		this.colorPicker5.addColorChangeListener(event -> this.colorPicker5_colorChanged(event));
		this.colorPicker6.addColorChangeListener(event -> this.colorPicker6_colorChanged(event));
		this.colorPicker7.addColorChangeListener(event -> this.colorPicker7_colorChanged(event));
		this.colorPicker8.addColorChangeListener(event -> this.colorPicker8_colorChanged(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevColorPicker colorPicker1, colorPicker2, colorPicker3, colorPicker4, colorPicker5, colorPicker6,
			colorPicker7, colorPicker8;
	private XdevButton buttonAggiungiPreventivo, buttonReset, buttonCancellaVentilatore;
	private XdevLabel labelSpazio;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout4, horizontalLayout2, horizontalLayout3;
	private XdevTable<CustomComponent> tableVentilatori;
	private XdevImage imageGrafico;
	private XdevCheckBox checkBoxPStatica, checkBoxPTotale, checkBoxPotenza, checkBoxRendimento,
			checkBoxCurvaCaratteristica, checkBoxPressioneSonora;
	private XdevVerticalLayout verticalLayout, verticalLayout2;
	// </generated-code>


}
