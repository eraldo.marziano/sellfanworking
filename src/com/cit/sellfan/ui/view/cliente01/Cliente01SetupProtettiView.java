package com.cit.sellfan.ui.view.cliente01;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

public class Cliente01SetupProtettiView extends XdevView {
	private VariabiliGlobali l_VG;

	/**
	 * 
	 */
	public Cliente01SetupProtettiView() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void Nazionalizza() {
		this.label1.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza da visualizzare"));
		this.label2.setValue(this.l_VG.utilityTraduzioni.TraduciStringa(">0 visualizza questo valore"));
		this.label3.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("<=0 visualizza valore reale"));
	}
	
	public void initSetup() {
		this.textFieldFrequenza.setValue(this.l_VG.utilityCliente.taroccoHz);
	}
	
	public void azioneReset() {
		this.l_VG.utilityCliente.taroccoHz = -1;
		initSetup();
	}
	
	public void azioneOK() {
		this.l_VG.utilityCliente.taroccoHz = this.textFieldFrequenza.getIntegerValue();
	}
	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.verticalLayout = new XdevVerticalLayout();
		this.label1 = new XdevLabel();
		this.label2 = new XdevLabel();
		this.label3 = new XdevLabel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.textFieldFrequenza = new TextFieldInteger();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.label = new XdevLabel();
	
		this.label1.setValue("Frequenza da visualizzare");
		this.label2.setValue(">0 visualizza questo valore");
		this.label3.setValue("<=0 visualizza valore reale");
		this.textFieldFrequenza.setColumns(5);
		this.label.setValue("Hz");
	
		this.label1.setSizeUndefined();
		this.verticalLayout.addComponent(this.label1);
		this.label2.setSizeUndefined();
		this.verticalLayout.addComponent(this.label2);
		this.label3.setSizeUndefined();
		this.verticalLayout.addComponent(this.label3);
		this.textFieldFrequenza.setSizeUndefined();
		this.verticalLayout2.addComponent(this.textFieldFrequenza);
		this.label.setSizeUndefined();
		this.verticalLayout3.addComponent(this.label);
		this.verticalLayout.setSizeUndefined();
		this.horizontalLayout.addComponent(this.verticalLayout);
		this.verticalLayout2.setSizeUndefined();
		this.horizontalLayout.addComponent(this.verticalLayout2);
		this.verticalLayout3.setSizeUndefined();
		this.horizontalLayout.addComponent(this.verticalLayout3);
		final CustomComponent horizontalLayout_spacer = new CustomComponent();
		horizontalLayout_spacer.setSizeFull();
		this.horizontalLayout.addComponent(horizontalLayout_spacer);
		this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel label1, label2, label3, label;
	private XdevHorizontalLayout horizontalLayout;
	private XdevVerticalLayout verticalLayout, verticalLayout2, verticalLayout3;
	private TextFieldInteger textFieldFrequenza;
	// </generated-code>

}
