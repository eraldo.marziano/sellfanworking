package com.cit.sellfan.ui.view;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.template.jDisplayImageWeb;
import com.vaadin.data.Property;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

public class Ingombro2ImgView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
    private final jDisplayImageWeb displayImage1 = new jDisplayImageWeb();
    private final jDisplayImageWeb displayImage2 = new jDisplayImageWeb();

	/**
	 * 
	 */
	public Ingombro2ImgView() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void Nazionalizza() {
		this.buttonCambiaOrientamento.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cambia"));
		this.labelEsecuzione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		if (this.l_VG == null) {
			return false;
		}
		if (!this.l_VG.utilityCliente.isTabVisible("Ingombro2Img")  || this.l_VG.VentilatoreCurrent == null) {
			return false;
		}
		InputStream isImg1 = null;
		try {
			//Notification.show("dimensioni/" + l_VG.utilityCliente.buildIngombro2ImgFronte(l_VG.VentilatoreCurrent));
			isImg1 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.utilityCliente.buildIngombro2ImgFronte(this.l_VG.VentilatoreCurrent));
		} catch (final Exception e) {
			//Notification.show("fronte "+e.toString());
		}
		InputStream isImg2 = null;
		try {
			//Notification.show("dimensioni/" + l_VG.utilityCliente.buildIngombro2ImgLato(l_VG.VentilatoreCurrent));
			isImg2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.utilityCliente.buildIngombro2ImgLato(this.l_VG.VentilatoreCurrent));
		} catch (final Exception e) {
			//Notification.show("lato "+e.toString());
		}
		if (isImg1 == null || isImg2 == null) {
			return false;
		}
		return true;
	}
	
	public void setVentilatoreCurrent() {
		this.isPannelloInizializzato = false;
		if (this.l_VG.VentilatoreCurrent == null) {
			return;
		}
		this.labelTitolo.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent)));
		this.comboBoxEsecuzioni.setEnabled(false);
		this.comboBoxEsecuzioni.removeAllItems();
        for (int i=0 ; i<this.l_VG.VentilatoreCurrent.Esecuzioni.length ; i++) {
        	if (!this.l_VG.VentilatoreCurrent.Esecuzioni[i].equals("-")) {
        		this.comboBoxEsecuzioni.addItem(this.l_VG.VentilatoreCurrent.Esecuzioni[i]);
        	}
        }
        this.comboBoxEsecuzioni.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
        final Collection<?> coll = this.comboBoxEsecuzioni.getItemIds();
        if (coll.size() > 1) {
			this.comboBoxEsecuzioni.setEnabled(true);
		}
		InputStream isImg1 = null;
		try {
			//Notification.show("dimensioni/" + l_VG.utilityCliente.buildIngombro2ImgFronte(l_VG.VentilatoreCurrent));
			isImg1 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.utilityCliente.buildIngombro2ImgFronte(this.l_VG.VentilatoreCurrent));
		} catch (final Exception e) {
			//Notification.show("fronte "+e.toString());
		}
		this.displayImage1.DisplayfromInputStream(isImg1);
		this.displayImage1.buildImage();
		final StreamResource imagesource1 = new StreamResource(this.displayImage1, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		imagesource1.setCacheTime(0);
		this.image1.setSource(imagesource1);
		InputStream isImg2 = null;
		try {
			//Notification.show("dimensioni/" + l_VG.utilityCliente.buildIngombro2ImgLato(l_VG.VentilatoreCurrent));
			isImg2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.utilityCliente.buildIngombro2ImgLato(this.l_VG.VentilatoreCurrent));
		} catch (final Exception e) {
			//Notification.show("lato "+e.toString());
		}
		this.displayImage2.DisplayfromInputStream(isImg2);
		this.displayImage2.buildImage();
		final StreamResource imagesource2 = new StreamResource(this.displayImage2, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		imagesource2.setCacheTime(0);
		this.image2.setSource(imagesource2);
		caricaLSZ();
		this.labelAngolo.setValue("<html><center><b>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
		this.labelOrientamento.setValue("<html><center><b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Orientamento") + " " + this.l_VG.VentilatoreCurrent.selezioneCorrente.Orientamento + "</html>");
		this.isPannelloInizializzato = true;
	}
	
	private void caricaLSZ() {
		String IngombroL = "???";
		String IngombroS = "???";
		String IngombroZ = "???";
        Statement stmt = null;
        ResultSet rsg = null;
        String query;
        try {
            stmt = this.l_VG.conTecnica.createStatement();
            query = "select * from " + this.l_VG.dbTableQualifier.dbTableQualifierPreDBTecnico + "Orientamento" + this.l_VG.dbTableQualifier.dbTableQualifierPostDBTecnico;
            query += " where Modello = '" + this.l_VG.VentilatoreCurrent.Modello + "' ";
            if (this.l_VG.VentilatoreCurrent.Trasmissione == true) {
                query += "and Trasmissione = 1 ";
            } else {
                query += "and Trasmissione = 0 ";
            }
            query += " and (Versione = '-' or Versione = '" + this.l_VG.VentilatoreCurrent.Versione + "')";
            query += " and (Classe = '" + this.l_VG.VentilatoreCurrent.Classe + "' or Classe = '-')";
            query += " and Angolo = " + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo);
            query += " order by Versione desc";
            rsg = stmt.executeQuery(query);
            Integer vi;
            rsg.next();
            vi = rsg.getInt("S");
            if (vi > 0) {
                IngombroS = vi.toString();
            }
            vi = rsg.getInt("Z");
            if (vi > 0) {
                IngombroZ = vi.toString();
            }
            vi = rsg.getInt("L");
            if (vi > 0) {
                IngombroL = vi.toString();
            }
            rsg.close();
            stmt.close();
        } catch (final Exception e) {
            IngombroS = "";
            IngombroZ = "";
            IngombroL = "";
        }
        if (this.l_VG.VentilatoreCurrent.DIM_L != null) {
            IngombroL = this.l_VG.VentilatoreCurrent.DIM_L.toString();
        }
        this.labelL.setValue("<html><center><b>" + IngombroL + "</html>");
        this.labelS.setValue("<html><center><b>" + IngombroS + "</html>");
        this.labelZ.setValue(IngombroZ);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAngoloMeno}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAngoloMeno_buttonClick(final Button.ClickEvent event) {
		this.l_VG.cambiaAngolo(-1);
		setVentilatoreCurrent();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAngoloPiu}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAngoloPiu_buttonClick(final Button.ClickEvent event) {
		this.l_VG.cambiaAngolo(1);
		setVentilatoreCurrent();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCambiaOrientamento}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCambiaOrientamento_buttonClick(final Button.ClickEvent event) {
		this.l_VG.cambiaOrientamento();
		setVentilatoreCurrent();
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxEsecuzioni}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxEsecuzioni_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxEsecuzioni.isEnabled()) {
			return;
		}
		if (this.comboBoxEsecuzioni.getValue() == null) {
			return;
		}
		this.l_VG.cambiaEsecuzione(this.comboBoxEsecuzioni.getValue().toString());
		setVentilatoreCurrent();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.labelTitolo = new XdevLabel();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.labelL = new XdevLabel();
		this.labelS = new XdevLabel();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.panelForImage1 = new XdevPanel();
		this.verticalLayout5 = new XdevVerticalLayout();
		this.image1 = new XdevImage();
		this.panelForImage2 = new XdevPanel();
		this.verticalLayout6 = new XdevVerticalLayout();
		this.image2 = new XdevImage();
		this.labelZ = new XdevLabel();
		this.horizontalLayout5 = new XdevHorizontalLayout();
		this.horizontalLayout6 = new XdevHorizontalLayout();
		this.buttonAngoloMeno = new XdevButton();
		this.labelAngolo = new XdevLabel();
		this.buttonAngoloPiu = new XdevButton();
		this.horizontalLayout7 = new XdevHorizontalLayout();
		this.labelOrientamento = new XdevLabel();
		this.buttonCambiaOrientamento = new XdevButton();
		this.horizontalLayout8 = new XdevHorizontalLayout();
		this.labelEsecuzione = new XdevLabel();
		this.comboBoxEsecuzioni = new XdevComboBox<>();
	
		this.verticalLayout.setMargin(new MarginInfo(true, true, false, true));
		this.horizontalLayout.setMargin(new MarginInfo(false, true, true, true));
		this.labelTitolo.setValue("titolo");
		this.labelTitolo.setContentMode(ContentMode.HTML);
		this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.labelL.setValue("L");
		this.labelL.setContentMode(ContentMode.HTML);
		this.labelS.setValue("S");
		this.labelS.setContentMode(ContentMode.HTML);
		this.horizontalLayout4.setMargin(new MarginInfo(false, true, false, true));
		this.panelForImage1.setTabIndex(0);
		this.panelForImage2.setTabIndex(0);
		this.labelZ.setValue("Z");
		this.horizontalLayout5.setMargin(new MarginInfo(false, true, false, true));
		this.buttonAngoloMeno.setCaption("<<");
		this.buttonAngoloMeno.setStyleName("small");
		this.labelAngolo.setValue("0");
		this.labelAngolo.setContentMode(ContentMode.HTML);
		this.buttonAngoloPiu.setCaption(">>");
		this.buttonAngoloPiu.setStyleName("small");
		this.labelOrientamento.setValue("RD");
		this.labelOrientamento.setContentMode(ContentMode.HTML);
		this.buttonCambiaOrientamento.setCaption("Cambia");
		this.buttonCambiaOrientamento.setStyleName("small");
		this.labelEsecuzione.setValue("Esecuzione");
	
		this.labelTitolo.setWidth(100, Unit.PERCENTAGE);
		this.labelTitolo.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.labelTitolo);
		this.horizontalLayout.setComponentAlignment(this.labelTitolo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.labelTitolo, 10.0F);
		this.labelL.setWidth(100, Unit.PERCENTAGE);
		this.labelL.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.labelL);
		this.horizontalLayout2.setComponentAlignment(this.labelL, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.labelL, 10.0F);
		this.labelS.setWidth(100, Unit.PERCENTAGE);
		this.labelS.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.labelS);
		this.horizontalLayout2.setComponentAlignment(this.labelS, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.labelS, 10.0F);
		this.image1.setSizeUndefined();
		this.verticalLayout5.addComponent(this.image1);
		this.verticalLayout5.setComponentAlignment(this.image1, Alignment.MIDDLE_CENTER);
		this.verticalLayout5.setExpandRatio(this.image1, 10.0F);
		this.verticalLayout5.setSizeFull();
		this.panelForImage1.setContent(this.verticalLayout5);
		this.image2.setSizeUndefined();
		this.verticalLayout6.addComponent(this.image2);
		this.verticalLayout6.setComponentAlignment(this.image2, Alignment.MIDDLE_CENTER);
		this.verticalLayout6.setExpandRatio(this.image2, 10.0F);
		this.verticalLayout6.setSizeFull();
		this.panelForImage2.setContent(this.verticalLayout6);
		this.panelForImage1.setSizeFull();
		this.horizontalLayout4.addComponent(this.panelForImage1);
		this.horizontalLayout4.setComponentAlignment(this.panelForImage1, Alignment.MIDDLE_CENTER);
		this.horizontalLayout4.setExpandRatio(this.panelForImage1, 10.0F);
		this.panelForImage2.setSizeFull();
		this.horizontalLayout4.addComponent(this.panelForImage2);
		this.horizontalLayout4.setComponentAlignment(this.panelForImage2, Alignment.MIDDLE_CENTER);
		this.horizontalLayout4.setExpandRatio(this.panelForImage2, 10.0F);
		this.labelZ.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.labelZ);
		this.horizontalLayout4.setComponentAlignment(this.labelZ, Alignment.MIDDLE_RIGHT);
		this.buttonAngoloMeno.setSizeUndefined();
		this.horizontalLayout6.addComponent(this.buttonAngoloMeno);
		this.horizontalLayout6.setComponentAlignment(this.buttonAngoloMeno, Alignment.MIDDLE_CENTER);
		this.labelAngolo.setWidth(60, Unit.PIXELS);
		this.labelAngolo.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.labelAngolo);
		this.horizontalLayout6.setComponentAlignment(this.labelAngolo, Alignment.MIDDLE_CENTER);
		this.buttonAngoloPiu.setSizeUndefined();
		this.horizontalLayout6.addComponent(this.buttonAngoloPiu);
		this.horizontalLayout6.setComponentAlignment(this.buttonAngoloPiu, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout6_spacer = new CustomComponent();
		horizontalLayout6_spacer.setSizeFull();
		this.horizontalLayout6.addComponent(horizontalLayout6_spacer);
		this.horizontalLayout6.setExpandRatio(horizontalLayout6_spacer, 1.0F);
		this.labelOrientamento.setSizeUndefined();
		this.horizontalLayout7.addComponent(this.labelOrientamento);
		this.horizontalLayout7.setComponentAlignment(this.labelOrientamento, Alignment.MIDDLE_CENTER);
		this.buttonCambiaOrientamento.setSizeUndefined();
		this.horizontalLayout7.addComponent(this.buttonCambiaOrientamento);
		this.horizontalLayout7.setComponentAlignment(this.buttonCambiaOrientamento, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout7_spacer = new CustomComponent();
		horizontalLayout7_spacer.setSizeFull();
		this.horizontalLayout7.addComponent(horizontalLayout7_spacer);
		this.horizontalLayout7.setExpandRatio(horizontalLayout7_spacer, 1.0F);
		this.labelEsecuzione.setSizeUndefined();
		this.horizontalLayout8.addComponent(this.labelEsecuzione);
		this.horizontalLayout8.setComponentAlignment(this.labelEsecuzione, Alignment.MIDDLE_RIGHT);
		this.comboBoxEsecuzioni.setSizeUndefined();
		this.horizontalLayout8.addComponent(this.comboBoxEsecuzioni);
		this.horizontalLayout8.setComponentAlignment(this.comboBoxEsecuzioni, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout8_spacer = new CustomComponent();
		horizontalLayout8_spacer.setSizeFull();
		this.horizontalLayout8.addComponent(horizontalLayout8_spacer);
		this.horizontalLayout8.setExpandRatio(horizontalLayout8_spacer, 1.0F);
		this.horizontalLayout6.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout6.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout5.addComponent(this.horizontalLayout6);
		this.horizontalLayout5.setComponentAlignment(this.horizontalLayout6, Alignment.MIDDLE_LEFT);
		this.horizontalLayout5.setExpandRatio(this.horizontalLayout6, 10.0F);
		this.horizontalLayout7.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout7.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout5.addComponent(this.horizontalLayout7);
		this.horizontalLayout5.setComponentAlignment(this.horizontalLayout7, Alignment.MIDDLE_CENTER);
		this.horizontalLayout5.setExpandRatio(this.horizontalLayout7, 10.0F);
		this.horizontalLayout8.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout8.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout5.addComponent(this.horizontalLayout8);
		this.horizontalLayout5.setComponentAlignment(this.horizontalLayout8, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout5.setExpandRatio(this.horizontalLayout8, 10.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.horizontalLayout4.setSizeFull();
		this.verticalLayout.addComponent(this.horizontalLayout4);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout4, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.horizontalLayout4, 10.0F);
		this.horizontalLayout5.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout5);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout5, Alignment.BOTTOM_RIGHT);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.buttonAngoloMeno.addClickListener(event -> this.buttonAngoloMeno_buttonClick(event));
		this.buttonAngoloPiu.addClickListener(event -> this.buttonAngoloPiu_buttonClick(event));
		this.buttonCambiaOrientamento.addClickListener(event -> this.buttonCambiaOrientamento_buttonClick(event));
		this.comboBoxEsecuzioni.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Ingombro2ImgView.this.comboBoxEsecuzioni_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelTitolo, labelL, labelS, labelZ, labelAngolo, labelOrientamento, labelEsecuzione;
	private XdevButton buttonAngoloMeno, buttonAngoloPiu, buttonCambiaOrientamento;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2, horizontalLayout4, horizontalLayout5,
			horizontalLayout6, horizontalLayout7, horizontalLayout8;
	private XdevImage image1, image2;
	private XdevPanel panelForImage1, panelForImage2;
	private XdevVerticalLayout verticalLayout, verticalLayout5, verticalLayout6;
	private XdevComboBox<CustomComponent> comboBoxEsecuzioni;
	// </generated-code>

}
