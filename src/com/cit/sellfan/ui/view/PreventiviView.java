package com.cit.sellfan.ui.view;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.view.mobile.MobilePreventiviVentilatori;
import com.cit.sellfan.ui.view.preventivi.PreventiviCliente;
import com.cit.sellfan.ui.view.preventivi.PreventiviCosto;
import com.cit.sellfan.ui.view.preventivi.PreventiviMacchina;
import com.cit.sellfan.ui.view.preventivi.PreventiviVarie;
import com.cit.sellfan.ui.view.preventivi.PreventiviVentilatori;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

import cit.sellfan.Costanti;
import cit.sellfan.classi.ventilatore.Ventilatore;

public class PreventiviView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
	public final PreventiviVentilatori ventilatoriView = new PreventiviVentilatori();
	private final MobilePreventiviVentilatori mobileVentilatoriView = new MobilePreventiviVentilatori();
	private final PreventiviCliente clienteView = new PreventiviCliente();
	private final PreventiviVarie varieView = new PreventiviVarie();
	private final PreventiviCosto costoView = new PreventiviCosto();
	private final PreventiviMacchina macchinaView = new PreventiviMacchina();
	public int ventilatoreIndex = -1;
	private final String coloreBottone = "verde";
	private final String coloreBottoneSelezionato = "verdeselezionato";
	//private MessageBox msgBox;
	/**
	 * 
	 */
	public PreventiviView() {
		super();
		this.initUI();
	}
	
	public PreventiviVentilatori getVentilatori() {
		return this.ventilatoriView;
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex] = this;
		//l_VG.PreventiviView = this;
		this.ventilatoriView.setVariabiliGlobali(this.l_VG);
		this.mobileVentilatoriView.setVariabiliGlobali(this.l_VG);
		this.clienteView.setVariabiliGlobali(this.l_VG);
		this.varieView.setVariabiliGlobali(this.l_VG);
		this.costoView.setVariabiliGlobali(this.l_VG);
		this.macchinaView.setVariabiliGlobali(this.l_VG);
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		return this.isPannelloInizializzato;
	}
	
	public void Nazionalizza() {
		this.buttonPreventivi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Preventivi"));
		this.buttonCliente.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cliente"));
		this.buttonVarie.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Varie"));
		this.buttonCosto.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Offerta Economica"));
		this.buttonMacchina.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Caratteristiche Macchina"));
		this.buttonNuovoPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nuovo Preventivo"));
		this.ventilatoriView.Nazionalizza();
		this.mobileVentilatoriView.Nazionalizza();
		this.clienteView.Nazionalizza();
		this.varieView.Nazionalizza();
		this.costoView.Nazionalizza();
		this.macchinaView.Nazionalizza();
	}
	
	public void initPreventivi() {
		//Notification.show("preventivi "+Integer.toString(l_VG.currentLivelloUtente)+" "+Integer.toString(Costanti.utenteDefault));
		if (!this.l_VG.utilityCliente.isTabVisible("Offerte") || this.l_VG.currentLivelloUtente != Costanti.utenteDefault) {
			return;
		}
		this.l_VG.ventilatoreCambiato = false;
		this.buttonCliente.setVisible(false);
		this.buttonVarie.setVisible(false);
		this.buttonCosto.setVisible(false);
		this.buttonMacchina.setVisible(false);
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.initTabellaPreventivi();
		} else {
			this.ventilatoriView.initTabellaPreventivi();
		}
		buttonPreventivi_buttonClick(null);
		resetColoreSelezionato();
		this.buttonPreventivi.removeStyleName(this.coloreBottone);
		this.buttonPreventivi.addStyleName(this.coloreBottoneSelezionato);
		this.isPannelloInizializzato = true;
		//Notification.show("preventivi                           init ok");
	}
	
	public void refreshAccessori() {
		this.macchinaView.setVentilatoreCurrent();
		this.costoView.refreshAccessori();
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.storePreventivo();
		} else {
			this.ventilatoriView.storePreventivo();
		}
	}
	
	public void abilitaTab() {
		boolean value = false;
		if (this.l_VG.preventivoCurrent != null) {
			value = true;
		}
		this.buttonCliente.setVisible(value);
		this.buttonVarie.setVisible(false);
		if (this.ventilatoreIndex >= 0) {
			this.buttonCosto.setVisible(this.l_VG.preventivoCurrent.Ventilatori.get(this.ventilatoreIndex).HaPreventivo);
			this.buttonMacchina.setVisible(!this.l_VG.MobileMode);
		} else {
			this.buttonCosto.setVisible(false);
			this.buttonMacchina.setVisible(false);
			//this.l_VG.MainView.setMemo("Nessuna offerta selezionata - ");
		}
	}
	
	public void setPreventivoSelected(final String idPreventivo) {
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.setPreventivoSelected(idPreventivo);
		} else {
			this.ventilatoriView.setPreventivoSelected(idPreventivo);
			
		}
	}
	
	public void aggiornaVentilatore() {
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.aggiornaVentilatoreCorrente();
		} else {
			this.ventilatoriView.aggiornaVentilatoreCorrente();
		}
		this.costoView.setCostoVentilatore();
	}
	
	public void aggiornaVentilatoreCorrente() {
		this.costoView.setCostoVentilatore();
		this.ventilatoriView.aggiornaVentilatoreCorrente();
		this.macchinaView.setVentilatoreCurrent();
	}
	
	public double calcolaCostoPreventivo() {
		double costoPreventivo = 0.0;
		for (int i=0 ; i<this.l_VG.preventivoCurrent.Ventilatori.size() ; i++) {
			if (this.l_VG.preventivoCurrent.Ventilatori.get(i).HaPreventivo) {
				costoPreventivo += this.l_VG.preventivoCurrent.Ventilatori.get(i).Qta * this.l_VG.preventivoCurrent.Ventilatori.get(i).CostoVentilatore;
			}
		}
		return costoPreventivo;
	}
	
	public void aggiornaPreventivo() {
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.aggiornaRigaPreventivoCorrente();
		} else {
			this.ventilatoriView.aggiornaRigaPreventivoCorrente();
		}
		this.clienteView.setCliente();
		this.varieView.setPreventivo();
	}
	
	public void reinitPreventivi() {
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.reinitTabellaPreventivi();
		} else {
			this.ventilatoriView.reinitTabellaPreventivi();
		}
	}
	
	public boolean isOnEdit() {
		return this.clienteView.isOnEdit();
	}
	
	public void aggiornaCosto() {
		this.costoView.setCostoVentilatore();
	}
	
	private void resetColoreSelezionato() {
		resetColoreSelezionato(this.buttonPreventivi);
		resetColoreSelezionato(this.buttonCliente);
		resetColoreSelezionato(this.buttonVarie);
		resetColoreSelezionato(this.buttonCosto);
		resetColoreSelezionato(this.buttonMacchina);
	}
	
	private void resetColoreSelezionato(final XdevButton bottone) {
		if (bottone.getStyleName().contains(this.coloreBottoneSelezionato)) {
			bottone.removeStyleName(this.coloreBottoneSelezionato);
			bottone.addStyleName(this.coloreBottone);
		}
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonPreventivi}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonPreventivi_buttonClick(final Button.ClickEvent event) {
		if (this.clienteView.isOnEdit()) {
			return;
		}
		resetColoreSelezionato();
		this.buttonPreventivi.removeStyleName(this.coloreBottone);
		this.buttonPreventivi.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.costoView.calcolaPrezzoTotale();
		if (this.l_VG.preventivoCurrent != null && this.l_VG.preventivoCurrent.PreventivoCambiato) {
			if (this.l_VG.MobileMode) {
				this.mobileVentilatoriView.aggiornaPreventivoCorrente();
			} else {
				this.ventilatoriView.aggiornaPreventivoCorrente();
			}
		}
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.setSizeFull();
			this.gridLayout.addComponent(this.mobileVentilatoriView, 0, 0);
		} else {
			this.ventilatoriView.setSizeFull();
			this.gridLayout.addComponent(this.ventilatoriView, 0, 0);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCosto}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCosto_buttonClick(final Button.ClickEvent event) {
		if (this.clienteView.isOnEdit()) {
			return;
		}
		resetColoreSelezionato();
		this.buttonCosto.removeStyleName(this.coloreBottone);
		this.buttonCosto.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.costoView.setSizeFull();
		this.costoView.calcolaPrezzoTotale();
		this.gridLayout.addComponent(this.costoView, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCliente}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCliente_buttonClick(final Button.ClickEvent event) {
		if (this.clienteView.isOnEdit()) {
			return;
		}
		resetColoreSelezionato();
		this.buttonCliente.removeStyleName(this.coloreBottone);
		this.buttonCliente.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.clienteView.initTableClienti();
		this.clienteView.setSizeFull();
		this.gridLayout.addComponent(this.clienteView, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonVarie}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonVarie_buttonClick(final Button.ClickEvent event) {
		if (this.clienteView.isOnEdit()) {
			return;
		}
		resetColoreSelezionato();
		this.buttonVarie.removeStyleName(this.coloreBottone);
		this.buttonVarie.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.varieView.setSizeFull();
		this.costoView.calcolaPrezzoTotale();
		this.gridLayout.addComponent(this.varieView, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonMacchina}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonMacchina_buttonClick(final Button.ClickEvent event) {
		if (this.clienteView.isOnEdit()) {
			return;
		}
		resetColoreSelezionato();
		this.buttonMacchina.removeStyleName(this.coloreBottone);
		this.buttonMacchina.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.macchinaView.setSizeFull();
		this.macchinaView.setVentilatoreCurrent();
		this.gridLayout.addComponent(this.macchinaView, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonNuovoPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonNuovoPreventivo_buttonClick(final Button.ClickEvent event) {
		if (this.l_VG.MobileMode) {
			this.mobileVentilatoriView.NewModifyPrenentivo(true);
		} else {
			this.ventilatoriView.editPreventivo(true);
		}
/*
        if (!l_VG.traceUser.isActionEnabled(l_VG.traceUser.AzioneNuovaOfferta, l_VG.ElencoPreventivi.size())) {
        	msgBox = MessageBox.createWarning();
        	msgBox.withMessage(l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
        	msgBox.withOkButton();
        	msgBox.open();
            return;
        }
		pannelloPreventivoEditNew pannello = new pannelloPreventivoEditNew();
		pannello.setVariabiliGlobali(l_VG);
		pannello.newMode = true;
		pannello.init();
		msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withOkButton(() -> {
			if (pannello.newMode) {
				l_VG.preventivoCurrent = new Preventivo();
				l_VG.ElencoVentilatoriFromPreventivo.clear();
			}
			pannello.getValues();
			storePreventivo();
	        Integer idPreventivo = l_VG.preventivoCurrent.IDPreventivo;
	        l_VG.dbCRMv2.loadFullElencoPreventiviFan(l_VG.ElencoPreventivi, 0 , l_VG.currentUserSaaS._ID);
	        PreventiviView preventivoTemp = (PreventiviView)l_VG.ElencoView[l_VG.PreventiviViewIndex];
	        if (pannello.newMode) {
	        	preventivoTemp.reinitPreventivi();
	        	preventivoTemp.setPreventivoSelected(idPreventivo.toString());
	            l_VG.traceUser.traceAzioneUser(l_VG.traceUser.AzioneNuovaOfferta, "ID: " + Integer.toString(idPreventivo));
	        } else {
	        	preventivoTemp.aggiornaPreventivo();
	        }
		});
		msgBox.open();
*/
	}

	public void storePreventivo() {
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
	}
	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.buttonPreventivi = new XdevButton();
		this.buttonCliente = new XdevButton();
		this.buttonVarie = new XdevButton();
		this.buttonMacchina = new XdevButton();
		this.buttonCosto = new XdevButton();
		this.label = new XdevLabel();
		this.buttonNuovoPreventivo = new XdevButton();
		this.gridLayout = new XdevGridLayout();
		this.panelToRemove = new XdevPanel();
	
		this.verticalLayout.setStyleName("active");
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false, false, false, true));
		this.buttonPreventivi.setCaption("Preventivi");
		this.buttonPreventivi.setStyleName("verde");
		this.buttonCliente.setCaption("Cliente");
		this.buttonCliente.setStyleName("verde");
		this.buttonVarie.setCaption("Varie");
		this.buttonVarie.setStyleName("verde");
		this.buttonMacchina.setCaption("Macchina");
		this.buttonMacchina.setStyleName("verde");
		this.buttonCosto.setCaption("Costo");
		this.buttonCosto.setStyleName("verde");
		this.buttonNuovoPreventivo
				.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/newspaper_add.png"));
		this.buttonNuovoPreventivo.setCaption("Nuovo Preventivo");
		this.buttonNuovoPreventivo.setStyleName("giallo");
		this.gridLayout.setMargin(new MarginInfo(false));
		this.panelToRemove.setTabIndex(0);
	
		this.buttonPreventivi.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonPreventivi);
		this.horizontalLayout.setComponentAlignment(this.buttonPreventivi, Alignment.MIDDLE_CENTER);
		this.buttonCliente.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonCliente);
		this.horizontalLayout.setComponentAlignment(this.buttonCliente, Alignment.MIDDLE_CENTER);
		this.buttonVarie.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonVarie);
		this.horizontalLayout.setComponentAlignment(this.buttonVarie, Alignment.MIDDLE_CENTER);
		this.buttonMacchina.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonMacchina);
		this.horizontalLayout.setComponentAlignment(this.buttonMacchina, Alignment.MIDDLE_CENTER);
		this.buttonCosto.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonCosto);
		this.horizontalLayout.setComponentAlignment(this.buttonCosto, Alignment.MIDDLE_CENTER);
		this.label.setWidth(27, Unit.PIXELS);
		this.label.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.label);
		this.horizontalLayout.setComponentAlignment(this.label, Alignment.MIDDLE_CENTER);
		this.buttonNuovoPreventivo.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonNuovoPreventivo);
		this.horizontalLayout.setComponentAlignment(this.buttonNuovoPreventivo, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout_spacer = new CustomComponent();
		horizontalLayout_spacer.setSizeFull();
		this.horizontalLayout.addComponent(horizontalLayout_spacer);
		this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
		this.gridLayout.setColumns(1);
		this.gridLayout.setRows(1);
		this.panelToRemove.setSizeFull();
		this.gridLayout.addComponent(this.panelToRemove, 0, 0);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setRowExpandRatio(0, 10.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(40, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.gridLayout.setSizeFull();
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.gridLayout, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.buttonPreventivi.addClickListener(event -> this.buttonPreventivi_buttonClick(event));
		this.buttonCliente.addClickListener(event -> this.buttonCliente_buttonClick(event));
		this.buttonVarie.addClickListener(event -> this.buttonVarie_buttonClick(event));
		this.buttonMacchina.addClickListener(event -> this.buttonMacchina_buttonClick(event));
		this.buttonCosto.addClickListener(event -> this.buttonCosto_buttonClick(event));
		this.buttonNuovoPreventivo.addClickListener(event -> this.buttonNuovoPreventivo_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonPreventivi, buttonCliente, buttonVarie, buttonMacchina, buttonCosto, buttonNuovoPreventivo;
	private XdevLabel label;
	private XdevHorizontalLayout horizontalLayout;
	private XdevPanel panelToRemove;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>
	public void updateAllCosts() {
		for (final Ventilatore v : this.l_VG.ElencoVentilatoriFromPreventivo) {
			final int i = v.CodiceVentilatoreForPreventivo;
			this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(Integer.toString(i));
			this.ventilatoreIndex = i;
			this.aggiornaCosto();
	}
		
	}


}
