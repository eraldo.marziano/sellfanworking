package com.cit.sellfan.ui.view;

import java.io.FileInputStream;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import com.cit.MyMouseEvents.MyMouseEvent;
import com.cit.MyMouseEvents.MyMouseEvent.MouseDownListener;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.pannelli.pannelloCondizioni;
import com.cit.sellfan.ui.pannelli.pannelloCondizioniFree;
import com.cit.sellfan.ui.pannelli.pannelloEsecuzione;
import com.cit.sellfan.ui.pannelli.pannelloHelpRpm;
import com.cit.sellfan.ui.template.jDisplayImageWeb;
import com.cit.sellfan.ui.template.jGraficoXYWeb;
import com.cit.sellfan.ui.view.cliente04.Cliente04pannelloERP327Info;
import com.cit.sellfan.ui.view.setup.SetupCurveView;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevSlider;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.sellfan.Costanti;
import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.HelpRpm;
import cit.sellfan.classi.Motore;
import cit.sellfan.classi.Warning;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import cit.utility.LettereGreche;
import cit.utility.NumeriRomani;
import com.vaadin.server.Page;
import com.vaadin.ui.Label;
import de.steinwedel.messagebox.ButtonType;
import de.steinwedel.messagebox.MessageBox;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@Theme("my-chameleon")
public class PrestazioniView extends XdevView {
	private VariabiliGlobali l_VG;
	//private final StreamFileResource streamFileResource = null;
	public jGraficoXYWeb grafico = new jGraficoXYWeb();
	private final jDisplayImageWeb imagelogoweb = new jDisplayImageWeb();
	private boolean editEnabled;
        private boolean showChangeEngine= true;
	public PrestazioniView() {
		super();
		this.initUI();
		final MyMouseEvent imageMouseEvent = MyMouseEvent.enableFor(this.image);
		imageMouseEvent.addMouseDownListener(new imageMouseDownListener());
		addShortcutListener(new ShortcutListener("Shortcut Enter", ShortcutAction.KeyCode.ENTER, null) {
		    @Override
		    public void handleAction(final Object sender, final Object target) {
		    	//l_VG.PrestazioniView.this.l_VG.MainView.addToMemo(Boolean.toString(target.equals(PrestazioniView.this.textFieldRpmRequest))+" handleAction rpm");
		    	if (target.equals(PrestazioniView.this.textFieldRpmRequest)) {
		    		cambiaRpm();
		    	} else if (target.equals(PrestazioniView.this.textFieldQRichiesta)) {
		    		cambiaPortata();
		    	}
		    }
		});
	}
	
	private void cambiaRpm() {
		if (!this.editEnabled) {
			return;
		}
		int newValue = this.textFieldRpmRequest.getIntegerValue();
		if (newValue < this.l_VG.VentilatoreCurrent.RpmMin) {
			newValue = this.l_VG.VentilatoreCurrent.RpmMin;
		}
		if (newValue > this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiriMax * this.l_VG.ParametriVari.pannelloCondizioniOverRpm) {
			newValue = (int)(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiriMax * this.l_VG.ParametriVari.pannelloCondizioniOverRpm);
		}
		this.textFieldRpmRequest.setValue(newValue);
		if (this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri != newValue) {
			this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri = newValue;
	   		setVentilatoreCurrent();
	   		this.l_VG.ventilatoreCambiato = true;
		}
                checkWarning();
	}
	
	private void cambiaPortata() {
		if (!this.editEnabled) {
			return;
		}
		double newValue = this.textFieldQRichiesta.getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione;
		if (newValue < this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin) {
			newValue = this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin;
		}
		if (newValue > this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax) {
			newValue = this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax;
		}
		if (newValue / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione != this.sliderQ.getValue()) {
			this.sliderQ.setValue(newValue / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
		}
                checkWarning();
	}
	
	private class imageMouseDownListener implements MouseDownListener {
		@Override
		public void mouseDown(final MouseEventDetails mouseDetails, final int width, final int height) {
			try {
				final double portata = PrestazioniView.this.grafico.getFisicalXfromMouseX(0, mouseDetails.getRelativeX(), width);
				final double newValue = portata * PrestazioniView.this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione;
				if (newValue < PrestazioniView.this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin || newValue > PrestazioniView.this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax) {
					return;
				}
				PrestazioniView.this.sliderQ.setValue(newValue / PrestazioniView.this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
			} catch (final Exception e) {
				
			}
		}
		
	}
        
        public void checkWarning() {
            if (l_VG.utilityCliente.idSottoClienteIndex != 0) {
                try {
                    boolean warning = false;
                    //controlli
                    //errore1
                    boolean warning1;
                    double PotenzaRichiestaW = 0.;
                    PotenzaRichiestaW = this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza;
                    PotenzaRichiestaW += this.l_VG.VentilatoreCurrent.datiERP327.potenzaPersaCuscinettiW;
                    PotenzaRichiestaW /= this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione;
                    if (this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
                        PotenzaRichiestaW *= (1. + this.l_VG.VentilatoreCurrent.CorrettorePotenzaMotore / 100.);
                    } else {
                        PotenzaRichiestaW *= (1. + this.l_VG.CorrettorePotenzaMotoreCorrente / 100.);
                    }
                    double NumeroGiri = this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri;
                    double PD2 = this.l_VG.VentilatoreCurrent.PD2;
                    String query = this.l_VG.utilityCliente.buildQuerySceltaMotoreForSelezioneManualeGrandezza(this.l_VG.VentilatoreCurrent, PotenzaRichiestaW, NumeroGiri, PD2);
                    query = this.l_VG.utilityCliente.buildQuerySceltaMotoreAdOrderBy(query);
                    final Statement stmt = this.l_VG.conTecnica.createStatement();
                    final ResultSet rs = stmt.executeQuery(query);
                    rs.next();
                    if (!rs.next()) {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(0).warningAttivo = true;
                        warning1 = true;
                    }
                    else {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(0).warningAttivo = false;
                        warning1 = false;
                    }
                    if (l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null) {
                        double tagliaMassima;
                        Cliente04VentilatoreCampiCliente cc = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
                        switch (l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione) {
                            case "E01":
                                tagliaMassima = cc.TMME01;
                                break;
                            case "E09":
                                tagliaMassima = cc.TMME09;
                                break;
                            case "E12":
                                tagliaMassima = cc.TMME12;
                                break;
                            case "E08":
                                tagliaMassima = cc.TMME08;
                                break;
                            case "E06":
                                tagliaMassima = cc.TMME06;
                                break;
                            case "E19":
                                tagliaMassima = cc.TMME19;
                                break;
                            case "E18":
                                tagliaMassima = cc.TMME18;
                                break;
                            case "E04":
                                tagliaMassima = cc.TMME04;
                                break;
                            case "E05":
                                tagliaMassima = cc.TMME05;
                                break;
                            default:
                                tagliaMassima = 10000;
                        }
                        if (tagliaMassima > 0 && l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza> tagliaMassima) {
                            this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(0).warningAttivo = true;
                            warning1 = true;
                        }
                    }

                    //warning 2
                    boolean warning2 = false;
                    if (l_VG.VentilatoreCurrent.Trasmissione) {
                    Double potenzaMotore = l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsterna/1000;
                    Cliente04VentilatoreCampiCliente vcc = (Cliente04VentilatoreCampiCliente) l_VG.VentilatoreCurrent.campiCliente;
                    String codiceMonoblocco = vcc.Monoblocco;
                    int maxKw = vcc.MaxKw;
                    double maxKw2 = (vcc.MaxKw2 > 0) ? vcc.MaxKw2 : 10000000;
                    if (maxKw> 0 && potenzaMotore > maxKw2) {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(2).warningAttivo = false;
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(3).warningAttivo = true;
                        warning2 = true;
                    } else if (maxKw > 0 && potenzaMotore > maxKw) {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(2).warningAttivo = true;
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(3).warningAttivo = false;
                        warning2 = true;
                    } else {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(2).warningAttivo = false;
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(3).warningAttivo = false;
                        warning2 = false;
                    }  
                    }
                    //warning 3
                    boolean warning3 = false;
                    String Serie = l_VG.VentilatoreCurrent.Serie;
                    if (Serie.startsWith("VA")||Serie.startsWith("VC")||Serie.startsWith("VP")) {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(4).warningAttivo = true;
                        warning3 = true;
                    }
                    //warning 4
                    boolean warning4 = false;
                    Double Hst = l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica;
                    if (Hst>18000.0) {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(5).warningAttivo = true;
                        warning4 = true;
                    } else {
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(5).warningAttivo = false;
                        warning4 = false;
                    }
                    //warning basamento maggiorato
                    boolean warningDimensione = false;
                    if (this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E12")) {
                        int grandezzaMax = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).TMME12;
                        if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato == null || this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente.equals("-")) {
                            double grandezzaMin = this.l_VG.utilityCliente.getTagliaMinimaPossibile(
                                    this.l_VG.VentilatoreCurrent,
                                    l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsterna,
                                    this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri,
                                    this.l_VG.VentilatoreCurrent.PD2);
                            if (grandezzaMin > grandezzaMax) {
                                this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(1).warningAttivo = true;
                                warningDimensione = true;
                            } else {
                                this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(1).warningAttivo = false;
                                warningDimensione = false;
                            }
                        } else {
                            if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza > grandezzaMax)  {
                                this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(1).warningAttivo = true;
                                warningDimensione = true;
                            } else {
                                this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(1).warningAttivo = false;
                                warningDimensione = false;
                            }
                        }
                    }
                    boolean warningFrequenza = false;
                        ArrayList<Integer> copiato = (ArrayList<Integer>) this.l_VG.VentilatoreCurrent.elencoClassi.clone();
                        if (!this.l_VG.VentilatoreCurrent.elencoClassi.isEmpty()) {
                            if (!this.l_VG.VentilatoreCurrent.Trasmissione) {
                                for (int i=copiato.size() - 1 ; i>=0 ; i--) {
                                    this.l_VG.VentilatoreCurrent.RpmLimite[0] = this.l_VG.VentilatoreCurrent.elencoRpmClassi.get(i);
                                    this.l_VG.utilityCliente.fillRpmLimite(this.l_VG.VentilatoreCurrent);
                                    this.l_VG.VentilatoreCurrent.RpmMaxTeorici = this.l_VG.dbTecnico.RpmMaxTemperatura(this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura, this.l_VG.VentilatoreCurrent);
                                    if (this.l_VG.VentilatoreCurrent.RpmInit > this.l_VG.VentilatoreCurrent.RpmMaxTeorici) {
                                        copiato.remove(i);                                        
                                    }
                                }
                            }   
                        }
                        if (copiato.isEmpty()) {
                            final MessageBox msgBox2 = MessageBox.create();
                            msgBox2.withCaption(l_VG.utilityTraduzioni.TraduciStringa("Attenzione"));
                            msgBox2.withMessage(l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.utilityCliente.getDialogString("ErroreFrequenza")));
                            msgBox2.withOkButton();
                            if (!l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(6).warningApprovato && !warningFrequenza) {
                                msgBox2.open();
                            }
                            warningFrequenza = true;
                            this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(6).warningAttivo = true;
                            
                        } else {
                            warningFrequenza = false;
                            this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(6).warningAttivo = false;
                        }
                    //controllo pulsante
                    boolean pulsanteRosso = false, pulsanteVerde = false;
                    if (warning1 || warning2 || warning3 || warning4 || warningDimensione || warningFrequenza)
                    {
                        for (Warning w : l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning) {
                            if (w.warningAttivo) {
                                if (w.warningApprovato) 
                                    pulsanteVerde = true;
                                else
                                    pulsanteRosso = true;
                            }
                        }
                    }
                    if (pulsanteRosso)
                    {
                        this.buttonWarning.setVisible(true);
                        this.buttonWarning.setStyleName("rosso");
                        this.buttonWarning.addStyleName("blink");  
                        this.l_VG.MainView.activateDatasheet(false);
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.Warning = true;
                    } else if (pulsanteVerde) {
                        this.buttonWarning.setVisible(true);
                        this.buttonWarning.setStyleName("verdewarn");  
                        this.l_VG.MainView.activateDatasheet(true);
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.Warning = true;
                    } else {
                        this.buttonWarning.setVisible(false);  
                        this.l_VG.MainView.activateDatasheet(true);
                        this.l_VG.VentilatoreCurrent.selezioneCorrente.Warning = false;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(PrestazioniView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
            this.l_VG = variabiliGlobali;
            if (l_VG.utilityCliente.idSottoClienteIndex == 2 || l_VG.utilityCliente.idSottoClienteIndex == 6 || l_VG.utilityCliente.idSottoClienteIndex == 9) {
                this.buttonAggiungiPreventivo.setVisible(false);
            }
            this.l_VG.ElencoView[this.l_VG.PrestazioniViewIndex] = this;
		try {
	        final FileInputStream fileIn = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logo.png");
	        this.imagelogoweb.DisplayfromInputStream(fileIn);
                    final StreamResource logosource = new StreamResource(this.imagelogoweb, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
                    logosource.setCacheTime(100);
                    this.imageLogo.setSource(logosource);
		} catch (final Exception e) {
			
		}
        }
	
	public void Nazionalizza() {
		this.buttonAggiungiPreventivo.setVisible(this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
		this.panelRpmLimits.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Limiti Giri/min"));
		this.panelClass.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Classe"));
		this.panelCondictions.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Condizioni"));
		this.labelSLM.setCaption("<html><b>h<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("S.L.M.") + "</sub></html>");
		this.labelTFluido.setCaption("<html><b>T<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("Fluido") + "</sub>:</html>");
		this.labelrhoFluido.setCaption("<html><b>ρ<sub>" + this.l_VG.utilityTraduzioni.TraduciStringa("Fluido") + "</sub>:</html>");
		this.labelUmidName.setCaption("<html><b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Umidità") + ":</html>");
		this.buttonModifyConditions.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Modifica"));
		this.buttonExcecution.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
		this.buttonAggiungiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
		this.buttonCangheMotor.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Seleziona Motore"));
		this.buttonCompare.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Confronta"));
		this.buttonPreferencies.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Preferenze"));
		this.panelEsecuzione.setVisible(this.l_VG.currentUserSaaS.idClienteIndex  == 4);
		this.panelClass.setVisible(this.l_VG.currentUserSaaS.idClienteIndex  == 4);
		this.buttonCompare.setVisible(this.l_VG.utilityCliente.isTabVisible("Confronto"));
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		try {
/*
			if (l_VG == null) {
				Notification.show("l_VG == null");
			} else {
				if (l_VG.utilityCliente == null) {
					Notification.show("l_VG.utilityCliente == null");
				}
				if (l_VG.VentilatoreCurrent == null) {
					Notification.show("l_VG.VentilatoreCurrent == null");
				}
			}
*/
			if (this.l_VG == null || !this.l_VG.utilityCliente.isTabVisible("Prestazioni") || this.l_VG.VentilatoreCurrent == null) {
				return false;
			}
			if (!this.l_VG.VentilatoreCurrent.CurveTrovate) {
				return false;
			}
			return true;
		} catch (final Exception e) {
			return false;
		}
	}
        
	public void setVentilatoreCurrent() {
        buttonWarning.setVisible(false);
        this.editEnabled = false;
        try {
            if (this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.isEmpty()) {
                this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning = new ArrayList<Warning>();
                for (Warning w : this.l_VG.listaW) {
                    this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.add(w.clone());
                }
            }
            for (Warning w : this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning) {
                if (w.warningAttivo) 
                    buttonWarning.setVisible(true);
            }    
        } catch (Exception E) {
            
        }
        if (this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaCatalogo) this.l_VG.VentilatoreCurrent.CAProgettazione = this.l_VG.CACatalogo;
        final String l_tit1 = this.l_VG.utilityCliente.buildTitoloGraficoPannelloPrestazioniPrimaRiga(this.l_VG.VentilatoreCurrent, this.l_VG.UMDiametroCorrente, this.l_VG.VentilatoreCurrent.UMAltezza, this.l_VG.VentilatoreCurrent.UMTemperatura);
        final String l_tit2 = this.l_VG.utilityCliente.buildTitoloGraficoPannelloPrestazioniSecondaRiga(this.l_VG.VentilatoreCurrent);
        if (this.l_VG.currentGraficoPrestazioniCurveWeb == null && this.l_VG.debugFlag) {
			Notification.show("setVentilatoreCurrent currentGraficoPrestazioniCurve");
		}
        this.l_VG.currentGraficoPrestazioniCurveWeb.reset();
        this.l_VG.currentGraficoPrestazioniCurveWeb.setParametriForPannelloPrestazioni(l_tit1, l_tit2, this.l_VG.ParametriVari.pannelloPrestazioniHieghtLightLimitiRicercaFlag);
        final boolean forzaGiri = this.l_VG.pannelloCatalogoForzaGiriMassimiFlag && this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaCatalogo && this.l_VG.VentilatoreCurrent.PrimaVolta;
        final boolean inizializzaMarker = this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaCatalogo && this.l_VG.VentilatoreCurrent.PrimaVolta;
        final boolean aggiornaAltriPannelli = true;
        final boolean displayCurvaCaratteristica = this.l_VG.VentilatoreCurrent.FromRicerca && this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag;
        boolean displayOrizzontalMarker = false;
        if (this.l_VG.pannelloPrestazioniMarkerOrizontalSpecialFlag) {
            displayOrizzontalMarker = true;
        } else {
            displayOrizzontalMarker = this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaRicerca || this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo;
        }
        this.l_VG.currentGraficoPrestazioniCurveWeb.setFlag(false, 2, forzaGiri, inizializzaMarker, aggiornaAltriPannelli, displayCurvaCaratteristica, this.l_VG.ParametriVari.pannelloPrestazionimarkerFlag, this.l_VG.ParametriVari.pannelloPrestazionimarkerFlag, displayOrizzontalMarker, this.l_VG.pannelloPrestazioniMarkerOrizontalSpecialFlag);
        if (this.l_VG.currentUserSaaS.idClienteIndex == 1)
            this.l_VG.currentGraficoPrestazioniCurveWeb.graficoPrestazioniCurveParameter.mistralFlag = true;
        this.l_VG.currentGraficoPrestazioniCurveWeb.buildGrafico(this.grafico);
        
//        grafico.buildSample();
		final StreamResource graficosource = new StreamResource(this.grafico, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		graficosource.setCacheTime(100);
		this.image.setSource(graficosource);
		showVentilatoreCurrent();
		//l_VG.setComponentBackground(buttonAddOffer.getId(), HColor.HGiallo);
		this.buttonAggiungiPreventivo.setEnabled(this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo);
		this.buttonHelpRpm.setVisible(this.l_VG.VentilatoreCurrent.Trasmissione);
		this.buttonHelpRpm.setEnabled(this.l_VG.utilityCliente.isHelpNumeroGiriEnabled(this.l_VG.VentilatoreCurrent.selezioneCorrente));
        this.buttonCangheMotor.setEnabled(!this.l_VG.utilityCliente.isAlberoNudo(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione) && showChangeEngine);
        checkWarning();
        boolean warningOk = l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.isEmpty() ? true : !l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(6).warningAttivo;
        this.buttonAggiungiPreventivo.setEnabled(warningOk);
        this.l_VG.MainView.togglePreventivoButton(warningOk);
        this.l_VG.MainView.confronto.togglePreventivoButton(warningOk);
        this.l_VG.MainView.dimensioni.togglePreventivoButton(warningOk);
        this.l_VG.MainView.ricerca.togglePreventivoButton(warningOk);
        if (l_VG.utilityCliente.idClienteIndex == 4) {
            this.l_VG.MainView.accessori04Assiali.togglePreventivoButton(warningOk);
            this.l_VG.MainView.accessori04Centrifughi.togglePreventivoButton(warningOk);
            this.l_VG.MainView.accessori04Fusione.togglePreventivoButton(warningOk);            
        }
        this.editEnabled = true;
	}
	
	class RemindTask extends TimerTask {
		@Override
		public void run() {
			PrestazioniView.this.buttonAggiungiPreventivo.setIcon(null);
		}
	}
    private void showVentilatoreCurrent() {
        try {
            if (l_VG.currentUserSaaS.idSottoClienteIndex == 6)
                this.l_VG.ERP327Build.setERP327(this.l_VG.VentilatoreCurrent, this.l_VG.regolamento327Algoritmo, this.l_VG.ventilatoreFisicaParameter, "Moro");
            else
                this.l_VG.ERP327Build.setERP327(this.l_VG.VentilatoreCurrent, this.l_VG.regolamento327Algoritmo, this.l_VG.ventilatoreFisicaParameter);
            this.buttonFree.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("a") + " " + Integer.toString((int)this.l_VG.VentilatoreCurrent.CAFree.temperatura)+" [°C], "+Integer.toString((int)this.l_VG.VentilatoreCurrent.CAFree.altezza)+" [m]");
            this.l_VG.ventilatoreFisicaCurrent.setNumeroGiriPortatam3h(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri, this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker);
            final double portata = this.l_VG.ventilatoreFisicaCurrent.getPortatam3h();
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
            String str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromm3hToXX(portata, this.l_VG.VentilatoreCurrent.UMPortata));
            this.labelQ.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PORTATA, " ", this.l_VG.VentilatoreCurrent.UMPortata.simbolo, ": <font color='blue'>", str));
            this.labelQ1.setValue(this.l_VG.utility.giuntaStringHtml(StringheUNI.PORTATA, " ", this.l_VG.VentilatoreCurrent.UMPortata.simbolo));
            this.editEnabled = false;
            this.sliderQ.setResolution(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
            try {
                this.sliderQ.setMax(this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
                this.sliderQ.setMin(this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
                } catch (Exception e) {}
            this.sliderQ.setValue(portata / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
            final double fattoreNm3h = this.l_VG.VentilatoreCurrent.CAProgettazione.rho / this.l_VG.utilityFisica.getDensitaAria(0);
            double v = portata * fattoreNm3h;
            if (this.l_VG.utilityUnitaMisura.isPortataInSecondi(this.l_VG.VentilatoreCurrent.UMPortata)) {
                this.l_VG.fmtNd.setMaximumFractionDigits(3);
                this.labelNQ.setValue(this.l_VG.utility.giuntaStringHtml("<b>", "NQ: " + this.l_VG.fmtNd.format(v / 3600.0), " [", StringheUNI.Nmcubialls, "]"));
                v = portata * this.l_VG.VentilatoreCurrent.CAProgettazione.rho;
                this.labelPesoh.setValue(this.l_VG.utility.giuntaStringHtml("<b>", this.l_VG.fmtNd.format(v / 3600.0), " [", StringheUNI.kgs, "]"));
            } else {
                this.l_VG.fmtNd.setMaximumFractionDigits(0);
                this.labelNQ.setValue(this.l_VG.utility.giuntaStringHtml("<b>", "NQ: " + this.l_VG.fmtNd.format(v), " [", StringheUNI.Nmcubiallh, "]"));
                v = portata * this.l_VG.VentilatoreCurrent.CAProgettazione.rho;
                this.labelPesoh.setValue(this.l_VG.utility.giuntaStringHtml("<b>", this.l_VG.fmtNd.format(v), " [", StringheUNI.kgh, "]"));
            }
            v = this.l_VG.ventilatoreFisicaCurrent.getPressioneTotalePa();
            this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPressione.nDecimaliStampa);
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale = v;
            if (v > 0.)
                str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(v, this.l_VG.VentilatoreCurrent.UMPressione));
            else 
                str = "";
            
            if (this.l_VG.utilityCliente.idClienteIndex == 1)
                this.labelHT.setValue(this.l_VG.utility.giuntaStringHtml("<b>Pt ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
            else
                this.labelHT.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PRESSIONETOTALE, " ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
            
            v = this.l_VG.ventilatoreFisicaCurrent.getPressioneTotaleFreePa();
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotaleFree = v;
            if (v > 0.)
                str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(v, this.l_VG.VentilatoreCurrent.UMPressione));
            else 
                str = "";
            if (this.l_VG.utilityCliente.idClienteIndex == 1)
                this.labelHTFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>Pt ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
            else
               this.labelHTFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PRESSIONETOTALE, " ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
            v = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaPa();
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica = v;
            str = (v>0) ? this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(v, this.l_VG.VentilatoreCurrent.UMPressione)) : "";
            if (this.l_VG.utilityCliente.idClienteIndex == 1)
                this.labelHSt.setValue(this.l_VG.utility.giuntaStringHtml("<b>Ps ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
            else
                this.labelHSt.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PRESSIONESTATICA, " ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
        v = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaFreePa();
        this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStaticaFree = v;
        if (v > 0.) {
            str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromPaToXX(v, this.l_VG.VentilatoreCurrent.UMPressione));
        } else {
            str = "";
        }
        if (this.l_VG.utilityCliente.idClienteIndex == 1) {
             this.labelHStFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>Ps ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
          } else {
                            this.labelHStFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PRESSIONESTATICA, " ", this.l_VG.VentilatoreCurrent.UMPressione.simbolo, ": <font color='blue'>", str));
                    }
        v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaW();
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.UMPotenzaCorrente.nDecimaliStampa);
        this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza = v;
        if (v > 0.) {
            str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(v, this.l_VG.UMPotenzaCorrente));
            if (this.l_VG.utilityCliente.idClienteIndex == 1) {
                     this.labelNa.setValue(this.l_VG.utility.giuntaStringHtml("<b>Pv ", this.l_VG.UMPotenzaCorrente.simbolo, ": <font color='blue'>", str));
              } else {
                                    this.labelNa.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZAASSE, " ", this.l_VG.UMPotenzaCorrente.simbolo, ": <font color='blue'>", str));
                            }
            v = (v + this.l_VG.VentilatoreCurrent.datiERP327.potenzaPersaCuscinettiW) / this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione;
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsterna = v;
            str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(v, this.l_VG.UMPotenzaCorrente));
            this.labelNe.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZAESTERNA, " ", this.l_VG.UMPotenzaCorrente.simbolo, ": <font color='blue'>", str));
        } else {
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsterna = -1.;
            this.labelNa.setValue("");
            this.labelNe.setValue("");
        }
        v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaXXCW();
        this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaXXC = v;
        v = (v + this.l_VG.VentilatoreCurrent.datiERP327.potenzaPersaCuscinettiW) / this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione;
        this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsternaXXC = v;
        v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaFreeW();
        this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaFree = v;
        if (v > 0.) {
            str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(v, this.l_VG.UMPotenzaCorrente));
            if (this.l_VG.utilityCliente.idClienteIndex == 1) {
                     this.labelNaFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>Pv ", this.l_VG.UMPotenzaCorrente.simbolo, ": <font color='blue'>", str));
             } else {
                                    this.labelNaFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZAASSE, " ", this.l_VG.UMPotenzaCorrente.simbolo, ": <font color='blue'>", str));
                            }
            v = (v + this.l_VG.VentilatoreCurrent.datiERP327.potenzaPersaCuscinettiW) / this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione;
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsternaFree = v;
            str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromWToXX(v, this.l_VG.UMPotenzaCorrente));
            this.labelNeFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZAESTERNA, " ", this.l_VG.UMPotenzaCorrente.simbolo, ": <font color='blue'>", str));
        } else {
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsternaXXC = -1;
            this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaEsternaFree = -1;
            this.labelNaFree.setValue("");
            this.labelNeFree.setValue("");
        }
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
        v = this.l_VG.ventilatoreFisicaCurrent.getRendimento();
        if (v > 0.) {
            str = this.l_VG.fmtNd.format(v);
         } else {
            str = "";
        }
        this.labeletaT.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.RENDIMENTO, " ", "[%]", ": <font color='blue'>", str));
        final double contributoXRumore = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(this.l_VG.VentilatoreCurrent.NBoccheCanalizzateBase, this.l_VG.VentilatoreCurrent.selezioneCorrente.nBoccheCanalizzate);
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.NDecimaliRumore);
        v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaSonora() + contributoXRumore;
        if (v > 0.) {
            str = this.l_VG.fmtNd.format(v);
         } else {
            str = "";
        }
        this.labelLW.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZASONORA, " ", "[dB(A)]", ": <font color='blue'>", str));
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.NDecimaliRumore);
        v = this.l_VG.ventilatoreFisicaCurrent.getPotenzaSonoraFree() + contributoXRumore;
        if (v > 0.) {
            str = this.l_VG.fmtNd.format(v);
         } else {
            str = "";
        }
        this.labelLWFree.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.POTENZASONORA, " ", "[dB(A)]", ": <font color='blue'>", str));
        v = this.l_VG.ventilatoreFisicaCurrent.getPressioneSonora() + contributoXRumore;
        if (v > 0.) {
            str = this.l_VG.fmtNd.format(v);
         } else {
            str = "";
        }
        this.labelLA.setValue(this.l_VG.utility.giuntaStringHtml("<b>", StringheUNI.PRESSIONESONORA, " ", "[dB(A)]", ": <font color='blue'>", str));

            this.checkBoxHT.setCaption(StringheUNI.PRESSIONETOTALE);
            this.checkBoxHSt.setCaption(StringheUNI.PRESSIONESTATICA);
            this.checkBoxNa.setCaption(StringheUNI.POTENZAASSE);
            this.checkBoxetaT.setCaption(StringheUNI.RENDIMENTO);
            if (this.l_VG.ParametriVari.pannelloPrestazioniPressioneSonoraVisibileFlag) {
                    this.checkBoxL.setCaption(StringheUNI.PRESSIONESONORA);
            } else {
                    this.checkBoxL.setCaption(StringheUNI.POTENZASONORA);
            }
            this.checkBoxCC.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Curva Carattaristica"));
            this.checkBoxHT.setVisible(this.l_VG.ventilatoreFisicaCurrent.isPtCurvaDisponibile());
            this.checkBoxHSt.setVisible(this.l_VG.ventilatoreFisicaCurrent.isPsCurvaDisponibile());
            this.checkBoxNa.setVisible(this.l_VG.ventilatoreFisicaCurrent.isPotCurvaDisponibile());
        if (this.l_VG.ventilatoreFisicaCurrent.isPtCurvaDisponibile() && this.l_VG.ventilatoreFisicaCurrent.isPotCurvaDisponibile()) {      //rendimento
            this.checkBoxetaT.setVisible(true);
            this.labeletaT.setVisible(true);
        } else {
            this.checkBoxetaT.setVisible(false);
            this.labeletaT.setVisible(false);
        }
        if (this.l_VG.ventilatoreFisicaCurrent.havePotenzaSonora()) {
            this.checkBoxL.setVisible(true);
            this.labelLW.setVisible(true);
            this.labelLA.setVisible(this.l_VG.ParametriVari.pannelloPrestazioniPressioneSonoraVisibileFlag);
        } else {
            this.checkBoxL.setVisible(false);
            this.labelLW.setVisible(false);
            this.labelLA.setVisible(false);
        }
        final boolean displayCurvaCaratteristica = this.l_VG.VentilatoreCurrent.FromRicerca && this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag;
        this.checkBoxCC.setVisible(displayCurvaCaratteristica);
                    this.checkBoxHT.setValue(true);
                    this.checkBoxHSt.setValue(true);
                    this.checkBoxNa.setValue(true);
                    this.checkBoxetaT.setValue(true);
                    this.checkBoxCC.setValue(this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag);
                    this.checkBoxL.setValue(true);
                    hideCurve();

                    this.labelHT.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPt.equals("no"));
                    this.labelHTFree.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPt.equals("no"));
                    this.labelHSt.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPs.equals("no"));
                    this.labelHStFree.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPs.equals("no"));
                    this.labelNa.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPot.equals("no"));
                    this.labelNaFree.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPot.equals("no"));
                    this.labelNe.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPot.equals("no"));
                    this.labelNeFree.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPot.equals("no"));
                    this.labelLW.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPotSon.equals("no"));
                    this.labelLWFree.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPotSon.equals("no"));
                    this.labelLA.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPotSon.equals("no"));
                    this.labeletaT.setVisible(!this.l_VG.VentilatoreCurrent.CurvaPt.equals("no") && !this.l_VG.VentilatoreCurrent.CurvaPot.equals("no"));
    //pannello destra
        int rpmMaxTemp = -1;
        this.buttonModifyConditions.setVisible(this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaCatalogo);
        this.labelModelloCompleto.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent)));
        checkLabel();
        //aggiunta per assiali
        Ventilatore vCurr = l_VG.VentilatoreCurrent;
        if (vCurr.Serie.contains("E")) {
            if (vCurr.Trasmissione) {
                if (vCurr.Serie.equals("ET")) {
                    if (vCurr.CAProgettazione.temperatura > 80 || vCurr.CAProgettazione.temperatura < -20) {
                        labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><p style='background-color:rgb(255, 102, 102)'>", str));
                        showChangeEngine = false;
                    } else {
                        labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
                        showChangeEngine = true;
                    }
                }
                if (vCurr.Serie.equals("EG")) {
                    if (vCurr.CAProgettazione.temperatura > 150 || vCurr.CAProgettazione.temperatura < -20) {
                        labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><p style='background-color:rgb(255, 102, 102)'>", str));
                        showChangeEngine = false;
                    } else {
                        labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
                        showChangeEngine = true;
                    }
                }
            } else {
                if (vCurr.CAProgettazione.temperatura > 40 || vCurr.CAProgettazione.temperatura < -20) {
                    labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><p style='background-color:rgb(255, 102, 102)'>", str));
                    showChangeEngine = false;
                } else {
                    labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
                    showChangeEngine = true;
                }
            }
        }
        this.comboBoxClassi.setEnabled(false);
        this.comboBoxClassi.removeAllItems();
        for (int i=0 ; i<this.l_VG.VentilatoreCurrent.elencoClassi.size() ; i++) {
            this.comboBoxClassi.addItem("Class " + NumeriRomani.daAraboARomano(this.l_VG.VentilatoreCurrent.elencoClassi.get(i)));
        }
        this.comboBoxClassi.setValue("Class " + this.l_VG.VentilatoreCurrent.Classe);
        final Collection<?> coll = this.comboBoxClassi.getItemIds();
        if (coll.size() > 1) {
                            this.comboBoxClassi.setEnabled(true);
                    }
        if (this.l_VG.debugFlag) {
                            Notification.show("Class " + NumeriRomani.daAraboARomano(this.l_VG.VentilatoreCurrent.Classe));
                    }
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMAltezza.nDecimaliStampa);
        str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.VentilatoreCurrent.CAProgettazione.altezza, this.l_VG.VentilatoreCurrent.UMAltezza)) + " " + this.l_VG.VentilatoreCurrent.UMAltezza.simbolo;
        this.labelSLM.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
        this.l_VG.fmtNd.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMTemperatura.nDecimaliStampa);
        str = this.l_VG.fmtNd.format(this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.VentilatoreCurrent.UMTemperatura.index, this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura)) + " " + this.l_VG.VentilatoreCurrent.UMTemperatura.simbolo;
        this.labelTFluido.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
        this.l_VG.fmtNd.setMaximumFractionDigits(3);
        str = this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.CAProgettazione.rho) + " " + StringheUNI.kgalmcubo;
        this.labelrhoFluido.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
        Integer um = -1;
        if (this.l_VG.VentilatoreCurrent.FromRicerca) um = this.l_VG.SelezioneDati.umidita;
        else um = this.l_VG.catalogoCondizioniUmidita;
        this.labelUmidName.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", um.toString()));

        final boolean visibile = this.l_VG.VentilatoreCurrent.datiERP327 != null && this.l_VG.VentilatoreCurrent.Trasmissione &&
                (this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E09") ||
                 this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E12"));
        if (visibile) {
            str = LettereGreche.etaMinuscolo + "<sub>trans</sub>: " + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione * 100.) + " [%]";
            this.labeletaTrasm.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
            this.labeletaTrasm.setVisible(true);
        } else {
            this.labeletaTrasm.setVisible(false);
        }
        this.sliderRpm.setMin(this.l_VG.VentilatoreCurrent.RpmMin);
        double setMax = this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri > this.l_VG.VentilatoreCurrent.RpmMaxTeorici* this.l_VG.ParametriVari.pannelloCondizioniOverRpm ? this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri : this.l_VG.VentilatoreCurrent.RpmMaxTeorici * this.l_VG.ParametriVari.pannelloCondizioniOverRpm;  
        this.sliderRpm.setMax(setMax);
        this.sliderRpm.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
        this.l_VG.fmtNd.setMaximumFractionDigits(0);
        this.textFieldRpmRequest.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
        //sliderRpm.setCaption("<b>Rpm: " + l_VG.fmtNd.format(l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri));
        if (this.l_VG.utilityCliente.taroccoHz > 0) {
            this.labelFrequenza.setValue("<b><center>(" + this.l_VG.utilityCliente.taroccoHz + " Hz)");
        } else {
            if (!this.l_VG.catalogoCondizioniFrequenza.equals("")) {
                                    this.labelFrequenza.setValue("<b><center>(" + this.l_VG.catalogoCondizioniFrequenza +")");
                            } else {
                                    this.labelFrequenza.setValue("<b><center>(" + this.l_VG.VentilatoreCurrent.CAProgettazione.frequenza + " Hz)");
                            }
        }
        this.textFieldQRichiesta.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
        this.textFieldQRichiesta.setValue(this.l_VG.utilityUnitaMisura.fromm3hToXX(portata, this.l_VG.VentilatoreCurrent.UMPortata));
        this.editEnabled = true;
            } catch (final Exception e) {
                    if (this.l_VG.debugFlag) {
                            Notification.show(e.toString());
                    }
            }
        if (l_VG.isDonaldson()) {
            this.checkBoxHT.setValue(true);
            this.checkBoxHSt.setValue(false);
            this.checkBoxNa.setValue(true);
            this.checkBoxetaT.setValue(true);
            this.checkBoxL.setValue(false);
            hideCurve();
            this.sliderQ.setVisible(false);
            this.buttonStepPorataPiu.setVisible(false);
            this.buttonStepPortataMeno.setVisible(false);
            this.labelQ1.setVisible(false);
            this.textFieldQRichiesta.setVisible(false);
            verticalLayout5.setVisible(false);
            horizontalLayout6.setVisible(false);
        }
    }
    
    private void checkLabel() {
        int rpmMaxTemp = -1;
        String str = "";
        //if (comboBoxClassi.isAttached())
        
        this.l_VG.cambiaClasse(" " + l_VG.VentilatoreCurrent.Classe);
        final int giri[] = new int[this.l_VG.VentilatoreCurrent.RpmLimite.length];
        for (int i=0 ; i<this.l_VG.VentilatoreCurrent.RpmLimite.length ; i++) {
            giri[i] = this.l_VG.VentilatoreCurrent.RpmLimite[i];
        }
        if (giri[0] < 0) {
            giri[0] = (int)this.l_VG.VentilatoreCurrent.RpmInit;
        }
        int index = 0;
        for (index=0 ; index<this.l_VG.VentilatoreCurrent.RpmLimite.length-1 ; index++) {
            if (this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura <= this.l_VG.temperatureLimite[index]) {
                    rpmMaxTemp = index;
                                    break;
                            }
        }
        if (index == 0) {
            str = "<=" + Integer.toString(this.l_VG.temperatureLimite[0]) + " °C:";
        } else if (index == this.l_VG.VentilatoreCurrent.RpmLimite.length - 1) {
            str = ">" + Integer.toString(this.l_VG.temperatureLimite[index - 1]) + " °C:";
        } else {
            str = Integer.toString((int)this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura) + " °C:";
        }
        str += "  " + Integer.toString(giri[index]);
        this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiriMax = giri[index];
        if ((this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri > this.l_VG.VentilatoreCurrent.RpmMaxTeorici) || (this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri > giri[rpmMaxTemp])){
            this.labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><p style='background-color:rgb(255, 102, 102)'>", str));
        } else {
            if (this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura > this.l_VG.VentilatoreCurrent.TMax || this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura < this.l_VG.VentilatoreCurrent.TMin) {
                this.labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><p style='background-color:rgb(255, 200, 102)'>", str));
            } else {
                    this.labelRpmLimits.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", str));
            }
        }
        this.l_VG.VentilatoreCurrent.RpmLimitiStr = str;
    }

	private void hideCurve() {
		final boolean showHT = this.checkBoxHT.isVisible() && this.checkBoxHT.getValue();
		final boolean showSt = this.checkBoxHSt.isVisible() && this.checkBoxHSt.getValue();
		final boolean showNa = this.checkBoxNa.isVisible() && this.checkBoxNa.getValue();
		final boolean showetaT = this.checkBoxetaT.isVisible() && this.checkBoxetaT.getValue();
		final boolean showC = this.checkBoxCC.isVisible() && this.checkBoxCC.getValue();
		final boolean showL = this.checkBoxL.isVisible() && this.checkBoxL.getValue();
	    //public void showhideCurve(boolean psVisible, boolean ptVisible, boolean potVisible, boolean etaVisible, boolean caratteristicaVisible, boolean rumoreVisible) {
		this.l_VG.currentGraficoPrestazioniCurveWeb.showhideCurve(showSt, showHT, showNa, showetaT, showC, showL);
		final StreamResource graficosource = new StreamResource(this.grafico, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		graficosource.setCacheTime(100);
		this.image.setSource(graficosource);
	}
/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxHT}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxHT_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevSlider}
	 * {@link #sliderQ}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void sliderQ_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
                checkWarning();
		this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker = this.sliderQ.getValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione;
		setVentilatoreCurrent();
		this.l_VG.ventilatoreCambiato = true;
		if (this.l_VG.VentilatoreCurrent.FromPreventivo) {
			final PreventiviView view = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
			view.aggiornaVentilatoreCorrente();
			if ((this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreAspirazione != null) || (this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreMandata != null)) {
				final MessageBox msgBox = MessageBox.createWarning();
				msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Attenzione"));
	    		msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Cambiato punto di esercizio. La selezione dei silenziatori sarà azzerata."));
	    		msgBox.withOkButton();
	    		msgBox.open();
			}
			this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreAspirazione = null;
			this.l_VG.VentilatoreCurrent.selezioneCorrente.silenziatoreMandata = null;
			
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxHSt}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxHSt_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxNa}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxNa_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxetaT}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxetaT_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxL}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxL_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCC}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCC_valueChange(final Property.ValueChangeEvent event) {
		hideCurve();
	}


	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonFree}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonFree_buttonClick(final Button.ClickEvent event) {
		final pannelloCondizioniFree pannello = new pannelloCondizioniFree();
		pannello.setVariabiliGlobali(this.l_VG);
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withOkButton(() -> {
			double temperatura = pannello.getTemperatura();
			temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, temperatura);
			double altezza = pannello.getAltezza();
			altezza = this.l_VG.utilityUnitaMisura.fromXXTom(altezza, this.l_VG.UMAltezzaCorrente);
			this.l_VG.VentilatoreCurrent.CAFree.temperatura = temperatura;
			this.l_VG.VentilatoreCurrent.CAFree.altezza = altezza;
            this.l_VG.VentilatoreCurrent.CAFree.rho = this.l_VG.utilityFisica.calcolaDensitaFuido(this.l_VG.VentilatoreCurrent.CARiferimento.rho, this.l_VG.VentilatoreCurrent.CAFree.temperatura, this.l_VG.VentilatoreCurrent.CAFree.altezza, 0., 0.);
            setVentilatoreCurrent();
			});
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxClassi}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxClassi_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxClassi.isEnabled()) {
			return;
		}
		if (this.comboBoxClassi.getValue() == null) {
			return;
		}
		if (this.l_VG.cambiaClasse(event.getProperty().getValue().toString())) {
			if (this.l_VG.VentilatoreCurrent.FromPreventivo) {
				final PreventiviView view = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
				view.aggiornaCosto();
			}
			setVentilatoreCurrent();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevSlider}
	 * {@link #sliderRpm}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void sliderRpm_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri = this.sliderRpm.getValue();
		setVentilatoreCurrent();
		this.l_VG.ventilatoreCambiato = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAggiungiPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAggiungiPreventivo_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addToPreventivo();
		this.buttonAggiungiPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
		final Timer timer = new Timer();
	    timer.schedule(new RemindTask(), 1000);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCangheMotor}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCangheMotor_buttonClick(final Button.ClickEvent event) {
            if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                final String warningStr = this.l_VG.utilityCliente.getWarning(this.l_VG.VentilatoreCurrent, 0);
                if (warningStr != null) {
                    final MessageBox msgBox = MessageBox.create();
                    msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(warningStr));
                    msgBox.withOkButton(() -> { this.l_VG.cambiaManualmenteMotore(); });
                    msgBox.open();
                } else {
                    this.l_VG.cambiaManualmenteMotore();
                }
            } else {
                this.l_VG.cambiaManualmenteMotore();
            }
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCompare}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCompare_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToCompare();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonPreferencies}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonPreferencies_buttonClick(final Button.ClickEvent event) {
		this.l_VG.ventilatoreCambiato = true;
		final SetupCurveView view = new SetupCurveView();
		view.setWidth(600);
		view.setVariabiliGlobali(this.l_VG);
		view.Nazionalizza();
		view.initSetup();
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(view);
		msgBox.withOkButton(() -> {
			view.azioneOK();
			this.l_VG.VentilatoreCurrent.UMAltezza = (UnitaMisura)this.l_VG.UMAltezzaCorrente.clone();
			this.l_VG.VentilatoreCurrent.UMPortata = (UnitaMisura)this.l_VG.UMPortataCorrente.clone();
			this.l_VG.VentilatoreCurrent.UMPotenza = (UnitaMisura)this.l_VG.UMPotenzaCorrente.clone();
			this.l_VG.VentilatoreCurrent.UMPressione = (UnitaMisura)this.l_VG.UMPressioneCorrente.clone();
			this.l_VG.VentilatoreCurrent.UMTemperatura = (UnitaMisura)this.l_VG.UMTemperaturaCorrente.clone();
			setVentilatoreCurrent();
			});
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonExcecution}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonExcecution_buttonClick(final Button.ClickEvent event) {
		this.l_VG.ventilatoreCambiato = true;
		final pannelloEsecuzione pannello = new pannelloEsecuzione();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.Nazionalizza();
		pannello.setVentilatoreCurrent();
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withOkButton(() -> {
			this.l_VG.cambiaEsecuzione(pannello.getEsecuzione());
			this.l_VG.utilityCliente.gestioneAccessori.setAccessorioSelezionato(this.l_VG.VentilatoreCurrent, "TRNX", this.l_VG.VentilatoreCurrent.campiCliente.trasmissioneAltoRendimento);
			this.l_VG.utilityCliente.gestioneAccessori.setAccessorioSelezionato(this.l_VG.VentilatoreCurrent, "INV", this.l_VG.VentilatoreCurrent.campiCliente.inverterPresente);
			this.l_VG.VentilatoreCurrent.VentilatoreCambiato = true;
			setVentilatoreCurrent();
			});
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonInfoErp}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonInfoErp_buttonClick(final Button.ClickEvent event) {
		final Cliente04pannelloERP327Info pannello = new Cliente04pannelloERP327Info();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.Nazionalizza();
		pannello.setVentilatoreCurrent();
		final MessageBox msgBox = MessageBox.create();
		msgBox.withCaption("Info ErP 2009/125/CE " + this.l_VG.utilityTraduzioni.TraduciStringa("Regolamento") + " (UE) N. 327/2011");
		msgBox.withMessage(pannello);
		msgBox.withOkButton();
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonModifyConditions}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonModifyConditions_buttonClick(final Button.ClickEvent event) {
		this.l_VG.ventilatoreCambiato = true;
		final pannelloCondizioni pannello = new pannelloCondizioni();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.Nazionalizza();
		pannello.onlyForVentilatore = true;
		pannello.initPannello();
		final MessageBox msgBox = MessageBox.create();
                msgBox.withCaption("Condizioni Ambientali");
		msgBox.withMessage(pannello);
                msgBox.withNoButton(() -> { 		//reset
                    pannello.azioneReset();
                    setVentilatoreCurrent();
                    this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori);
                    this.l_VG.MainView.refreshAccessori();
                    });
		msgBox.withAbortButton();
		//this.l_VG.MainView.setMemo(Double.toString(this.l_VG.VentilatoreCurrent.RpmInit)+" "+Double.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri));
		msgBox.withOkButton(() -> {
                    pannello.azioneOK();
                    int freq = -1;
                    if (!this.l_VG.catalogoCondizioniFrequenza.equals("")) {
                        freq = Integer.parseInt(this.l_VG.catalogoCondizioniFrequenza.split(" ")[0]);
                    }
                    this.l_VG.dbTecnico.setVentilatoreClasseMinima(this.l_VG.VentilatoreCurrent, freq);
                    setVentilatoreCurrent();
                    this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori);
                    this.l_VG.MainView.refreshAccessori();
                });
		msgBox.getButton(ButtonType.NO).setIcon(null);
		msgBox.getButton(ButtonType.NO).setCaption("Reset");
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonStepPortataMeno}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonStepPortataMeno_buttonClick(final Button.ClickEvent event) {
		double newValue = this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker - this.l_VG.ParametriVari.stepPortatam3h;
		if (newValue < this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin) {
			newValue = this.l_VG.currentGraficoPrestazioniCurveWeb.portataMin;
		}
		this.sliderQ.setValue(newValue / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonStepPorataPiu}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonStepPorataPiu_buttonClick(final Button.ClickEvent event) {
		double newValue = this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker + this.l_VG.ParametriVari.stepPortatam3h;
		if (newValue > this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax) {
			newValue = this.l_VG.currentGraficoPrestazioniCurveWeb.portataMax;
		}
		this.sliderQ.setValue(newValue / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonStepRpmMeno}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonStepRpmMeno_buttonClick(final Button.ClickEvent event) {
		this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri = this.sliderRpm.getValue() - this.l_VG.ParametriVari.stepRpm;
		setVentilatoreCurrent();
		this.l_VG.ventilatoreCambiato = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonStepRpmPiu}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonStepRpmPiu_buttonClick(final Button.ClickEvent event) {
		this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri = this.sliderRpm.getValue() + this.l_VG.ParametriVari.stepRpm;
		setVentilatoreCurrent();
		this.l_VG.ventilatoreCambiato = true;
	}
        
        private void buttonWarning_buttonClick(final Button.ClickEvent event) {
            //apri popup
            StringBuilder sb = new StringBuilder("<ul>");
            for (Warning message : this.l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning) {
                if (message.warningAttivo) {
                    sb.append("<li>");
                    sb.append(this.l_VG.utilityTraduzioni.TraduciStringa(message.getWarning()));
                    sb.append("</li>");
                    message.warningApprovato = true;
                }
            }
            sb.append("</ul>");
            sb.append("<p>");
            sb.append(this.l_VG.utilityTraduzioni.TraduciStringa("Clicca per chiudere"));
            Notification note = new Notification("Warning", sb.toString(), Notification.Type.WARNING_MESSAGE, true);
            note.setDelayMsec(-1);
            note.show(Page.getCurrent());
            checkWarning();
        }

	/**
	 * Event handler delegate method for the {@link TextFieldInteger}
	 * {@link #textFieldRpmRequest}.
	 *
	 * @see FieldEvents.BlurListener#blur(FieldEvents.BlurEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldRpmRequest_blur(final FieldEvents.BlurEvent event) {
		//this.l_VG.MainView.addToMemo("blur ");
		cambiaRpm();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonHelpRpm}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonHelpRpm_buttonClick(final Button.ClickEvent event) {
		final HelpRpm helpRpm = new HelpRpm();
		if (!this.l_VG.utilityCliente.initHelpNumeroGiri(this.l_VG.VentilatoreCurrent, helpRpm)) {
			return;
		}
		final pannelloHelpRpm pannello = new pannelloHelpRpm();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.initPannello(helpRpm);
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withAbortButton();
		msgBox.withOkButton(() -> {
			final int index = pannello.getIndexRpmSelezionati();
			if (index >= 0) {
				this.l_VG.VentilatoreCurrent.rapportoTrasmissione = helpRpm.elencoRapportiPossibili.get(index);
				this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri = helpRpm.elencoRpmPossibili.get(index);
				setVentilatoreCurrent();
				this.l_VG.ventilatoreCambiato = true;
			}
			});
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link TextFieldDouble}
	 * {@link #textFieldQRichiesta}.
	 *
	 * @see FieldEvents.BlurListener#blur(FieldEvents.BlurEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQRichiesta_blur(final FieldEvents.BlurEvent event) {
		//this.l_VG.MainView.addToMemo("blur ");
		cambiaPortata();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
                this.buttonWarning =  new XdevButton();
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.image = new XdevImage();
		this.verticalLayout5 = new XdevVerticalLayout();
		this.panel = new XdevPanel();
		this.verticalLayout6 = new XdevVerticalLayout();
		this.panel3 = new XdevPanel();
		this.horizontalLayout5 = new XdevHorizontalLayout();
		this.labelModelloCompleto = new XdevLabel();
		this.panelForLogo = new XdevPanel();
		this.verticalLayout7 = new XdevVerticalLayout();
		this.imageLogo = new XdevImage();
		this.panelRpmLimits = new XdevPanel();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.labelRpmLimits = new XdevLabel();
		this.panelClass = new XdevPanel();
		this.horizontalLayout7 = new XdevHorizontalLayout();
		this.comboBoxClassi = new XdevComboBox<>();
		this.panelCondictions = new XdevPanel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.labelSLM = new XdevLabel();
		this.labelTFluido = new XdevLabel();
		this.labelrhoFluido = new XdevLabel();
		this.buttonModifyConditions = new XdevButton();
		this.labeletaTrasm = new XdevLabel();
		this.gridLayout = new XdevGridLayout();
		this.label = new XdevLabel();
		this.textFieldRpmRequest = new TextFieldInteger();
		this.buttonHelpRpm = new XdevButton();
		this.buttonStepRpmMeno = new XdevButton();
		this.buttonStepRpmPiu = new XdevButton();
		this.sliderRpm = new XdevSlider();
		this.labelFrequenza = new XdevLabel();
		this.panelEsecuzione = new XdevPanel();
		this.verticalLayout8 = new XdevVerticalLayout();
		this.buttonExcecution = new XdevButton();
		this.buttonInfoErp = new XdevButton();
		this.horizontalLayout8 = new XdevHorizontalLayout();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.panel5 = new XdevPanel();
		this.gridLayout1 = new XdevGridLayout();
		this.labelQ = new XdevLabel();
		this.labelHT = new XdevLabel();
		this.labelHSt = new XdevLabel();
		this.labelNa = new XdevLabel();
		this.labelNe = new XdevLabel();
		this.labeletaT = new XdevLabel();
		this.labelLW = new XdevLabel();
		this.labelNQ = new XdevLabel();
		this.labelPesoh = new XdevLabel();
		this.labelLA = new XdevLabel();
		this.buttonFree = new XdevButton();
		this.labelHTFree = new XdevLabel();
		this.labelHStFree = new XdevLabel();
		this.labelNaFree = new XdevLabel();
		this.labelNeFree = new XdevLabel();
		this.labelRiserva5 = new XdevLabel();
		this.labelLWFree = new XdevLabel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.labelQ1 = new XdevLabel();
		this.textFieldQRichiesta = new TextFieldDouble();
		this.buttonStepPortataMeno = new XdevButton();
		this.sliderQ = new XdevSlider();
		this.buttonStepPorataPiu = new XdevButton();
		this.checkBoxHT = new XdevCheckBox();
		this.checkBoxHSt = new XdevCheckBox();
		this.checkBoxNa = new XdevCheckBox();
		this.checkBoxetaT = new XdevCheckBox();
		this.checkBoxL = new XdevCheckBox();
		this.checkBoxCC = new XdevCheckBox();
		this.horizontalLayout6 = new XdevHorizontalLayout();
		this.labelMemo = new XdevLabel();
		this.buttonAggiungiPreventivo = new XdevButton();
		this.buttonCangheMotor = new XdevButton();
		this.buttonCompare = new XdevButton();
		this.buttonPreferencies = new XdevButton();
		this.labelUmid = new XdevLabel();
		this.labelUmidName = new XdevLabel();
                this.horizontalLayoutRPM = new XdevHorizontalLayout();
                this.horizontalLayoutRPM.setSpacing(false);
		this.horizontalLayoutRPM.setMargin(new MarginInfo(true, false));
                this.horizontalLayoutPulsantiRPM = new XdevHorizontalLayout();
                this.horizontalLayoutPulsantiRPM.setSpacing(false);
		this.horizontalLayoutPulsantiRPM.setMargin(new MarginInfo(true, false));
		
		this.verticalLayout.setSpacing(false);
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.verticalLayout5.setSpacing(false);
		this.verticalLayout5.setMargin(new MarginInfo(false));
		this.panel.setTabIndex(0);
		this.verticalLayout6.setMargin(new MarginInfo(false, true, false, true));
		this.verticalLayout6.setImmediate(true);
		this.panel3.setTabIndex(0);
		this.horizontalLayout5.setMargin(new MarginInfo(false));
		this.labelModelloCompleto.setValue("labelModelloCompleto");
		this.labelModelloCompleto.setContentMode(ContentMode.HTML);
		this.verticalLayout7.setMargin(new MarginInfo(false));
		this.panelRpmLimits.setCaption("<html><b>Rpm limits</html>");
		this.panelRpmLimits.setTabIndex(0);
		this.panelRpmLimits.setCaptionAsHtml(true);
		this.horizontalLayout4.setMargin(new MarginInfo(false, true, false, true));
		this.labelRpmLimits.setValue("labelRpmLimits");
		this.labelRpmLimits.setContentMode(ContentMode.HTML);
		this.panelClass.setCaption("<html><b>Class</html>");
		this.panelClass.setTabIndex(0);
		this.panelClass.setCaptionAsHtml(true);
		this.horizontalLayout7.setMargin(new MarginInfo(false, true, false, true));
		this.panelCondictions.setCaption("<html><b>Conditions</html>");
		this.panelCondictions.setTabIndex(0);
		this.panelCondictions.setCaptionAsHtml(true);
		this.verticalLayout2.setSpacing(false);
		this.verticalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.labelSLM.setCaption("<html><b>h<sub>A.S.L.</sub></html>");
		this.labelSLM.setCaptionAsHtml(true);
		this.labelSLM.setValue("Label");
		this.labelSLM.setContentMode(ContentMode.HTML);
		this.labelTFluido.setCaption("<html><b>T<sub>Fluid</sub>:</html>");
		this.labelTFluido.setCaptionAsHtml(true);
		this.labelTFluido.setValue("Label");
		this.labelTFluido.setContentMode(ContentMode.HTML);
		this.labelrhoFluido.setCaption("<html><b>ρ<sub>Fluid</sub>:</html>");
		this.labelrhoFluido.setCaptionAsHtml(true);
		this.labelrhoFluido.setValue("Label");
		this.labelrhoFluido.setContentMode(ContentMode.HTML);
		this.buttonModifyConditions.setCaption("Modify");
		this.buttonModifyConditions.setStyleName("small");
		this.labeletaTrasm.setValue("labeletaTrasm");
		this.labeletaTrasm.setContentMode(ContentMode.HTML);
		this.gridLayout.setSpacing(false);
		this.gridLayout.setMargin(new MarginInfo(false));
		this.label.setValue("<b>Rpm");
		this.label.setContentMode(ContentMode.HTML);
		this.buttonHelpRpm.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/help16.png"));
		this.buttonHelpRpm.setCaption("");
		this.buttonHelpRpm.setStyleName("small");
		this.buttonStepRpmMeno.setCaption("<<");
		this.buttonStepRpmMeno.setStyleName("small");
		this.buttonStepRpmPiu.setCaption(">>");
		this.buttonStepRpmPiu.setStyleName("small");
		this.sliderRpm.setCaptionAsHtml(true);
		this.labelFrequenza.setValue("HZ");
		this.labelFrequenza.setContentMode(ContentMode.HTML);
		this.buttonExcecution.setCaption("Excecution");
		this.buttonExcecution.setStyleName("small");
		this.buttonInfoErp.setCaption("Info ErP 327");
		this.buttonInfoErp.setStyleName("small");
		this.horizontalLayout8.setMargin(new MarginInfo(false));
		this.verticalLayout3.setSpacing(false);
		this.verticalLayout3.setMargin(new MarginInfo(false));
		this.horizontalLayout2.setSpacing(false);
		this.horizontalLayout2.setMargin(new MarginInfo(false));
		this.panel5.setTabIndex(0);
		this.panel5.setStyleName("dark");
		this.gridLayout1.setSpacing(false);
		this.gridLayout1.setMargin(new MarginInfo(false, true, false, true));
		this.labelQ.setValue("<html><b>H<sub>D</sub><font color='blue'>pippo</html>");
		this.labelQ.setContentMode(ContentMode.HTML);
		this.labelHT.setValue("Label");
		this.labelHT.setContentMode(ContentMode.HTML);
		this.labelHSt.setValue("Label");
		this.labelHSt.setContentMode(ContentMode.HTML);
		this.labelNa.setValue("Label");
		this.labelNa.setContentMode(ContentMode.HTML);
		this.labelNe.setValue("Label");
		this.labelNe.setContentMode(ContentMode.HTML);
		this.labeletaT.setValue("Label");
		this.labeletaT.setContentMode(ContentMode.HTML);
		this.labelLW.setValue("Label");
		this.labelLW.setContentMode(ContentMode.HTML);
		this.labelNQ.setStyleName("allign-right");
		this.labelNQ.setPrimaryStyleName("v-label-align-right");
		this.labelNQ.setValue("labelNQ");
		this.labelNQ.setContentMode(ContentMode.HTML);
		this.labelPesoh.setStyleName("allign-right");
		this.labelPesoh.setValue("labelPesoh");
		this.labelPesoh.setContentMode(ContentMode.HTML);
		this.labelLA.setValue("Label");
		this.labelLA.setContentMode(ContentMode.HTML);
		this.buttonFree.setCaption("Free");
		this.buttonFree.setStyleName("small");
		this.buttonFree.setCaptionAsHtml(true);
		this.labelHTFree.setValue("Label");
		this.labelHTFree.setContentMode(ContentMode.HTML);
		this.labelHStFree.setValue("Label");
		this.labelHStFree.setContentMode(ContentMode.HTML);
		this.labelNaFree.setValue("Label");
		this.labelNaFree.setContentMode(ContentMode.HTML);
		this.labelNeFree.setValue("Label");
		this.labelNeFree.setContentMode(ContentMode.HTML);
		this.labelRiserva5.setContentMode(ContentMode.HTML);
		this.labelLWFree.setValue("Label");
		this.labelLWFree.setContentMode(ContentMode.HTML);
		this.verticalLayout4.setSpacing(false);
		this.verticalLayout4.setMargin(new MarginInfo(false));
		this.horizontalLayout3.setMargin(new MarginInfo(false, true, false, true));
		this.labelQ1.setValue("Label");
		this.labelQ1.setContentMode(ContentMode.HTML);
		this.textFieldQRichiesta.setColumns(5);
		this.buttonStepPortataMeno.setCaption("<<");
		this.buttonStepPortataMeno.setStyleName("small");
		this.buttonStepPorataPiu.setCaption(">>");
		this.buttonStepPorataPiu.setStyleName("small");
		this.checkBoxHT.setCaption("<html><b>H<sub>D</sub><font color=\"red\">pippo</html>");
		this.checkBoxHT.setCaptionAsHtml(true);
		this.checkBoxHSt.setCaption("CheckBox");
		this.checkBoxHSt.setCaptionAsHtml(true);
		this.checkBoxNa.setCaption("CheckBox");
		this.checkBoxNa.setCaptionAsHtml(true);
		this.checkBoxetaT.setCaption("CheckBox");
		this.checkBoxetaT.setCaptionAsHtml(true);
		this.checkBoxL.setCaption("CheckBox");
		this.checkBoxL.setCaptionAsHtml(true);
		this.checkBoxCC.setCaption("CheckBox");
		this.checkBoxCC.setCaptionAsHtml(true);
		this.horizontalLayout6.setMargin(new MarginInfo(false, true, false, true));
		this.buttonAggiungiPreventivo.setCaption("Aggiungi Preventivo");
		this.buttonAggiungiPreventivo.setStyleName("big giallo");
		this.buttonAggiungiPreventivo.setId("buttonAddOffer");
		this.buttonCangheMotor
				.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/documents_exchange.png"));
		this.buttonCangheMotor.setCaption("Change motor");
		this.buttonCangheMotor.setStyleName("small");
		this.buttonCompare.setCaption("Compare");
		this.buttonCompare.setStyleName("small");
		this.buttonPreferencies.setCaption("Preferencies");
		this.buttonPreferencies.setStyleName("small");
		
		this.labelUmidName.setCaption("<html><b>Umidità:</html>");
		this.labelUmidName.setCaptionAsHtml(true);
		this.labelUmidName.setValue("Label");
		this.labelUmidName.setContentMode(ContentMode.HTML);
		this.labelUmid.setCaption("");
		
		 
	
		this.labelModelloCompleto.setSizeFull();
		this.horizontalLayout5.addComponent(this.labelModelloCompleto);
		this.horizontalLayout5.setComponentAlignment(this.labelModelloCompleto, Alignment.MIDDLE_CENTER);
		this.horizontalLayout5.setExpandRatio(this.labelModelloCompleto, 10.0F);
		this.horizontalLayout5.setSizeFull();
		this.panel3.setContent(this.horizontalLayout5);
		this.imageLogo.setWidth(100, Unit.PERCENTAGE);
		this.imageLogo.setHeight(-1, Unit.PIXELS);
		this.verticalLayout7.addComponent(this.imageLogo);
		this.verticalLayout7.setComponentAlignment(this.imageLogo, Alignment.MIDDLE_CENTER);
		this.verticalLayout7.setExpandRatio(this.imageLogo, 10.0F);
		this.verticalLayout7.setSizeFull();
		this.panelForLogo.setContent(this.verticalLayout7);
		this.labelRpmLimits.setSizeFull();
		this.horizontalLayout4.addComponent(this.labelRpmLimits);
		this.horizontalLayout4.setComponentAlignment(this.labelRpmLimits, Alignment.MIDDLE_CENTER);
		this.horizontalLayout4.setExpandRatio(this.labelRpmLimits, 10.0F);
		this.horizontalLayout4.setSizeFull();
		this.panelRpmLimits.setContent(this.horizontalLayout4);
		this.comboBoxClassi.setSizeFull();
		this.horizontalLayout7.addComponent(this.comboBoxClassi);
		this.horizontalLayout7.setComponentAlignment(this.comboBoxClassi, Alignment.MIDDLE_CENTER);
		this.horizontalLayout7.setExpandRatio(this.comboBoxClassi, 10.0F);
		this.horizontalLayout7.setSizeFull();
		this.panelClass.setContent(this.horizontalLayout7);
		this.gridLayout.setColumns(4);
		this.gridLayout.setRows(3);
		this.label.setSizeUndefined();
                label.setWidth(40, Unit.PIXELS);
                //this.gridLayout.addComponent(this.label, 0, 0);
		this.textFieldRpmRequest.setWidth(45, Unit.PIXELS);
		this.textFieldRpmRequest.setHeight(-1, Unit.PIXELS);
		//this.gridLayout.addComponent(this.textFieldRpmRequest, 1, 0);
		this.buttonHelpRpm.setWidth(30, Unit.PIXELS);
                this.buttonHelpRpm.setHeight(-1, Unit.PIXELS);
		//this.gridLayout.addComponent(this.buttonHelpRpm, 2, 0);
		this.buttonStepRpmMeno.setSizeUndefined();
		//this.gridLayout.addComponent(this.buttonStepRpmMeno, 0, 1);
                
		//this.gridLayout.setComponentAlignment(this.buttonStepRpmMeno, Alignment.MIDDLE_CENTER);
		this.buttonStepRpmPiu.setSizeUndefined();
		//this.gridLayout.addComponent(this.buttonStepRpmPiu, 1, 1);
                horizontalLayoutPulsantiRPM.addComponents(buttonStepRpmMeno, buttonStepRpmPiu);
		final CustomComponent gridLayout_hSpacer = new CustomComponent();
		gridLayout_hSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_hSpacer, 3, 0, 3, 1);
		this.gridLayout.setColumnExpandRatio(3, 1.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 2, 2, 2);
		this.gridLayout.setRowExpandRatio(2, 2.0F);
		this.horizontalLayoutRPM.addComponents(this.label, this.textFieldRpmRequest, this.buttonHelpRpm);
		
                
                this.labelSLM.setWidth(100, Unit.PERCENTAGE);
		this.labelSLM.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelSLM);
		this.verticalLayout2.setComponentAlignment(this.labelSLM, Alignment.MIDDLE_CENTER);
		this.labelTFluido.setWidth(100, Unit.PERCENTAGE);
		this.labelTFluido.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelTFluido);
		this.verticalLayout2.setComponentAlignment(this.labelTFluido, Alignment.MIDDLE_CENTER);
		this.labelrhoFluido.setWidth(100, Unit.PERCENTAGE);
		this.labelrhoFluido.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelrhoFluido);
		this.verticalLayout2.setComponentAlignment(this.labelrhoFluido, Alignment.MIDDLE_CENTER);
		this.labelUmidName.setWidth(100, Unit.PERCENTAGE);
		this.labelUmidName.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelUmidName);
		this.verticalLayout2.setComponentAlignment(this.labelUmidName, Alignment.MIDDLE_CENTER);
		
		
		
		
		
		
		this.buttonModifyConditions.setSizeUndefined();
		this.verticalLayout2.addComponent(this.buttonModifyConditions);
		this.verticalLayout2.setComponentAlignment(this.buttonModifyConditions, Alignment.MIDDLE_CENTER);
		this.labeletaTrasm.setWidth(100, Unit.PERCENTAGE);
		this.labeletaTrasm.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labeletaTrasm);
		this.verticalLayout2.setComponentAlignment(this.labeletaTrasm, Alignment.MIDDLE_CENTER);
		this.gridLayout.setSizeUndefined();
                this.verticalLayout2.addComponent(this.horizontalLayoutRPM, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.addComponent(this.horizontalLayoutPulsantiRPM, Alignment.MIDDLE_CENTER);
		//this.verticalLayout2.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.sliderRpm.setWidth(100, Unit.PERCENTAGE);
		this.sliderRpm.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.sliderRpm);
		this.verticalLayout2.setComponentAlignment(this.sliderRpm, Alignment.MIDDLE_CENTER);
		this.labelFrequenza.setWidth(100, Unit.PERCENTAGE);
		this.labelFrequenza.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelFrequenza);
		this.verticalLayout2.setComponentAlignment(this.labelFrequenza, Alignment.MIDDLE_CENTER);
		final CustomComponent verticalLayout2_spacer = new CustomComponent();
		verticalLayout2_spacer.setSizeFull();
		this.verticalLayout2.addComponent(verticalLayout2_spacer);
		this.verticalLayout2.setExpandRatio(verticalLayout2_spacer, 1.0F);
		this.verticalLayout2.setSizeFull();
		this.panelCondictions.setContent(this.verticalLayout2);
		this.buttonExcecution.setSizeUndefined();
		this.verticalLayout8.addComponent(this.buttonExcecution);
		this.verticalLayout8.setComponentAlignment(this.buttonExcecution, Alignment.MIDDLE_CENTER);
		this.buttonInfoErp.setSizeUndefined();
		this.verticalLayout8.addComponent(this.buttonInfoErp);
		this.verticalLayout8.setComponentAlignment(this.buttonInfoErp, Alignment.MIDDLE_CENTER);
		this.verticalLayout8.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout8.setHeight(-1, Unit.PIXELS);
		this.panelEsecuzione.setContent(this.verticalLayout8);
		this.panel3.setWidth(100, Unit.PERCENTAGE);
		this.panel3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout6.addComponent(this.panel3);
		this.verticalLayout6.setExpandRatio(this.panel3, 1.0F);
		this.panelForLogo.setSizeFull();
		this.verticalLayout6.addComponent(this.panelForLogo);
		this.verticalLayout6.setExpandRatio(this.panelForLogo, 3.0F);
		this.panelRpmLimits.setWidth(100, Unit.PERCENTAGE);
		this.panelRpmLimits.setHeight(-1, Unit.PIXELS);
		this.verticalLayout6.addComponent(this.panelRpmLimits);
		this.panelClass.setWidth(100, Unit.PERCENTAGE);
		this.panelClass.setHeight(-1, Unit.PIXELS);
		this.verticalLayout6.addComponent(this.panelClass);
		this.panelCondictions.setWidth(100, Unit.PERCENTAGE);
		this.panelCondictions.setHeight(-1, Unit.PIXELS);
		this.verticalLayout6.addComponent(this.panelCondictions);
		this.panelEsecuzione.setWidth(100, Unit.PERCENTAGE);
		this.panelEsecuzione.setHeight(-1, Unit.PIXELS);
		this.verticalLayout6.addComponent(this.panelEsecuzione);
		this.verticalLayout6.setExpandRatio(this.panelEsecuzione, 10.0F);
		this.horizontalLayout8.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout8.setHeight(-1, Unit.PIXELS);
		this.verticalLayout6.addComponent(this.horizontalLayout8);
		this.verticalLayout6.setSizeFull();
		this.panel.setContent(this.verticalLayout6);
		this.panel.setSizeFull();
		this.verticalLayout5.addComponent(this.panel);
		this.verticalLayout5.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
		this.verticalLayout5.setExpandRatio(this.panel, 10.0F);
		this.image.setWidth(-1, Unit.PIXELS);
		this.image.setHeight(100, Unit.PERCENTAGE);
		this.horizontalLayout.addComponent(this.image);
		this.horizontalLayout.setComponentAlignment(this.image, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.image, 60.0F);
		this.verticalLayout5.setSizeFull();
		this.horizontalLayout.addComponent(this.verticalLayout5);
		this.horizontalLayout.setComponentAlignment(this.verticalLayout5, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.verticalLayout5, 10.0F);
		this.gridLayout1.setColumns(7);
		this.gridLayout1.setRows(4);
		this.labelQ.setWidth(100, Unit.PERCENTAGE);
		this.labelQ.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelQ, 0, 0);
		this.labelHT.setWidth(100, Unit.PERCENTAGE);
		this.labelHT.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelHT, 1, 0);
		this.labelHSt.setWidth(100, Unit.PERCENTAGE);
		this.labelHSt.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelHSt, 2, 0);
		this.labelNa.setWidth(100, Unit.PERCENTAGE);
		this.labelNa.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelNa, 3, 0);
		this.labelNe.setWidth(100, Unit.PERCENTAGE);
		this.labelNe.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelNe, 4, 0);
		this.labeletaT.setWidth(100, Unit.PERCENTAGE);
		this.labeletaT.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labeletaT, 5, 0);
		this.labelLW.setWidth(100, Unit.PERCENTAGE);
		this.labelLW.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelLW, 6, 0);
		this.labelNQ.setWidth(100, Unit.PERCENTAGE);
		this.labelNQ.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelNQ, 0, 1, 1, 1);
		this.labelPesoh.setWidth(100, Unit.PERCENTAGE);
		this.labelPesoh.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelPesoh, 2, 1, 3, 1);
		this.labelLA.setWidth(100, Unit.PERCENTAGE);
		this.labelLA.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelLA, 6, 1);
		this.buttonFree.setSizeUndefined();
		this.gridLayout1.addComponent(this.buttonFree, 0, 2);
		this.labelHTFree.setWidth(100, Unit.PERCENTAGE);
		this.labelHTFree.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelHTFree, 1, 2);
		this.labelHStFree.setWidth(100, Unit.PERCENTAGE);
		this.labelHStFree.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelHStFree, 2, 2);
		this.labelNaFree.setWidth(100, Unit.PERCENTAGE);
		this.labelNaFree.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelNaFree, 3, 2);
		this.labelNeFree.setWidth(100, Unit.PERCENTAGE);
		this.labelNeFree.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelNeFree, 4, 2);
                this.buttonWarning.setWidth(90, Unit.PERCENTAGE);
                this.buttonWarning.setHeight(100, Unit.PERCENTAGE);
                this.buttonWarning.setCaptionAsHtml(true);
                this.buttonWarning.setStyleName("rosso");
                this.buttonWarning.addStyleName("blink");
                this.buttonWarning.setCaption("Warning");
                this.buttonWarning.setVisible(false);
                
                this.gridLayout1.addComponent(this.buttonWarning, 5, 1, 5, 2);
		this.labelLWFree.setWidth(100, Unit.PERCENTAGE);
		this.labelLWFree.setHeight(-1, Unit.PIXELS);
		this.gridLayout1.addComponent(this.labelLWFree, 6, 2);
		this.gridLayout1.setColumnExpandRatio(0, 10.0F);
		this.gridLayout1.setColumnExpandRatio(1, 10.0F);
		this.gridLayout1.setColumnExpandRatio(2, 10.0F);
		this.gridLayout1.setColumnExpandRatio(3, 10.0F);
		this.gridLayout1.setColumnExpandRatio(4, 10.0F);
		this.gridLayout1.setColumnExpandRatio(5, 10.0F);
		this.gridLayout1.setColumnExpandRatio(6, 10.0F);
		final CustomComponent gridLayout1_vSpacer = new CustomComponent();
		gridLayout1_vSpacer.setSizeFull();
		this.gridLayout1.addComponent(gridLayout1_vSpacer, 0, 3, 6, 3);
		this.gridLayout1.setRowExpandRatio(3, 1.0F);
		this.gridLayout1.setSizeFull();
		this.panel5.setContent(this.gridLayout1);
		this.panel5.setSizeFull();
		this.horizontalLayout2.addComponent(this.panel5);
		this.horizontalLayout2.setComponentAlignment(this.panel5, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.panel5, 100.0F);
		this.horizontalLayout2.setSizeFull();
		this.verticalLayout3.addComponent(this.horizontalLayout2);
		this.verticalLayout3.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout3.setExpandRatio(this.horizontalLayout2, 10.0F);
		this.labelQ1.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.labelQ1);
		this.horizontalLayout3.setComponentAlignment(this.labelQ1, Alignment.MIDDLE_CENTER);
		this.textFieldQRichiesta.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.textFieldQRichiesta);
		this.horizontalLayout3.setComponentAlignment(this.textFieldQRichiesta, Alignment.MIDDLE_CENTER);
		this.buttonStepPortataMeno.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.buttonStepPortataMeno);
		this.horizontalLayout3.setComponentAlignment(this.buttonStepPortataMeno, Alignment.MIDDLE_CENTER);
		this.sliderQ.setWidth(100, Unit.PERCENTAGE);
		this.sliderQ.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.sliderQ);
		this.horizontalLayout3.setComponentAlignment(this.sliderQ, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.sliderQ, 50.0F);
		this.buttonStepPorataPiu.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.buttonStepPorataPiu);
		this.horizontalLayout3.setComponentAlignment(this.buttonStepPorataPiu, Alignment.MIDDLE_CENTER);
		this.checkBoxHT.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxHT.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxHT);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxHT, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxHT, 10.0F);
		this.checkBoxHSt.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxHSt.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxHSt);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxHSt, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxHSt, 10.0F);
		this.checkBoxNa.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxNa.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxNa);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxNa, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxNa, 10.0F);
		this.checkBoxetaT.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxetaT.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxetaT);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxetaT, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxetaT, 10.0F);
		this.checkBoxL.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxL.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxL);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxL, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxL, 10.0F);
		this.checkBoxCC.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxCC.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxCC);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxCC, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxCC, 10.0F);
		this.labelMemo.setWidth(100, Unit.PERCENTAGE);
		this.labelMemo.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.labelMemo);
		this.horizontalLayout6.setComponentAlignment(this.labelMemo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout6.setExpandRatio(this.labelMemo, 30.0F);
		this.buttonAggiungiPreventivo.setSizeUndefined();
		this.horizontalLayout6.addComponent(this.buttonAggiungiPreventivo);
		this.buttonCangheMotor.setSizeUndefined();
		this.horizontalLayout6.addComponent(this.buttonCangheMotor);
		this.buttonCompare.setSizeUndefined();
		this.horizontalLayout6.addComponent(this.buttonCompare);
		this.buttonPreferencies.setSizeUndefined();
		this.horizontalLayout6.addComponent(this.buttonPreferencies);
		this.horizontalLayout3.setSizeFull();
		this.verticalLayout4.addComponent(this.horizontalLayout3);
		this.verticalLayout4.setComponentAlignment(this.horizontalLayout3, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.horizontalLayout3, 10.0F);
		this.horizontalLayout6.setSizeFull();
		this.verticalLayout4.addComponent(this.horizontalLayout6);
		this.verticalLayout4.setComponentAlignment(this.horizontalLayout6, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.horizontalLayout6, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.horizontalLayout, 100.0F);
		this.verticalLayout3.setSizeFull();
		this.verticalLayout.addComponent(this.verticalLayout3);
		this.verticalLayout.setComponentAlignment(this.verticalLayout3, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.verticalLayout3, 15.0F);
		this.verticalLayout4.setSizeFull();
		this.verticalLayout.addComponent(this.verticalLayout4);
		this.verticalLayout.setComponentAlignment(this.verticalLayout4, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.verticalLayout4, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.comboBoxClassi.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.comboBoxClassi_valueChange(event);
			}
		});
		this.buttonModifyConditions.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
                            PrestazioniView.this.buttonModifyConditions_buttonClick(event);
                        }
		});
		this.textFieldRpmRequest.addBlurListener(event -> this.textFieldRpmRequest_blur(event));
		this.buttonHelpRpm.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonHelpRpm_buttonClick(event);
			}
		});
		this.buttonStepRpmMeno.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonStepRpmMeno_buttonClick(event);
			}
		});
		this.buttonStepRpmPiu.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonStepRpmPiu_buttonClick(event);
			}
		});
                
                this.buttonWarning.addClickListener((final Button.ClickEvent event) -> {
                    PrestazioniView.this.buttonWarning_buttonClick(event);
                });

		this.sliderRpm.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.sliderRpm_valueChange(event);
			}
		});
		this.buttonExcecution.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonExcecution_buttonClick(event);
			}
		});
		this.buttonInfoErp.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonInfoErp_buttonClick(event);
			}
		});
		this.buttonFree.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonFree_buttonClick(event);
			}
		});
		this.textFieldQRichiesta.addBlurListener(event -> this.textFieldQRichiesta_blur(event));
		this.buttonStepPortataMeno.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonStepPortataMeno_buttonClick(event);
			}
		});
		this.sliderQ.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.sliderQ_valueChange(event);
			}
		});
		this.buttonStepPorataPiu.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonStepPorataPiu_buttonClick(event);
			}
		});
		this.checkBoxHT.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.checkBoxHT_valueChange(event);
			}
		});
		this.checkBoxHSt.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.checkBoxHSt_valueChange(event);
			}
		});
		this.checkBoxNa.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.checkBoxNa_valueChange(event);
			}
		});
		this.checkBoxetaT.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.checkBoxetaT_valueChange(event);
			}
		});
		this.checkBoxL.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.checkBoxL_valueChange(event);
			}
		});
		this.checkBoxCC.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PrestazioniView.this.checkBoxCC_valueChange(event);
			}
		});
		this.buttonAggiungiPreventivo.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonAggiungiPreventivo_buttonClick(event);
			}
		});
		this.buttonCangheMotor.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonCangheMotor_buttonClick(event);
			}
		});
		this.buttonCompare.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonCompare_buttonClick(event);
			}
		});
		this.buttonPreferencies.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(final Button.ClickEvent event) {
				PrestazioniView.this.buttonPreferencies_buttonClick(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelModelloCompleto, labelRpmLimits, labelSLM, labelTFluido, labelrhoFluido, labeletaTrasm, label,
			labelFrequenza, labelQ, labelHT, labelHSt, labelNa, labelNe, labeletaT, labelLW, labelNQ, labelPesoh, labelLA,
			labelHTFree, labelHStFree, labelNaFree, labelNeFree, labelRiserva5, labelLWFree, labelQ1, labelMemo, labelUmid, labelUmidName;
	private XdevButton buttonModifyConditions, buttonHelpRpm, buttonStepRpmMeno, buttonStepRpmPiu, buttonExcecution,
			buttonInfoErp, buttonFree, buttonStepPortataMeno, buttonStepPorataPiu, buttonAggiungiPreventivo, buttonCangheMotor,
			buttonCompare, buttonPreferencies, buttonWarning;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout5, horizontalLayout4, horizontalLayout7,
			horizontalLayout8, horizontalLayout2, horizontalLayout3, horizontalLayout6, horizontalLayoutRPM, horizontalLayoutPulsantiRPM;
	private XdevImage image, imageLogo;
	private TextFieldDouble textFieldQRichiesta;
	private XdevSlider sliderRpm, sliderQ;
	private XdevPanel panel, panel3, panelForLogo, panelRpmLimits, panelClass, panelCondictions, panelEsecuzione, panel5;
	private XdevCheckBox checkBoxHT, checkBoxHSt, checkBoxNa, checkBoxetaT, checkBoxL, checkBoxCC;
	private XdevGridLayout gridLayout, gridLayout1;
	private XdevVerticalLayout verticalLayout, verticalLayout5, verticalLayout6, verticalLayout7, verticalLayout2,
			verticalLayout8, verticalLayout3, verticalLayout4;
	private TextFieldInteger textFieldRpmRequest;
	private XdevComboBox<CustomComponent> comboBoxClassi;
	// </generated-code>


}
