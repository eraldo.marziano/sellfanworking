package com.cit.sellfan.ui.view.setup;

import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.IntegerStepper;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.event.FieldEvents;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.sellfan.classi.CondizioniAmbientali;
import cit.sellfan.classi.UtilityCliente;
import com.vaadin.ui.Notification;

public class SetupUtilizzatoreView extends XdevView {
	private VariabiliGlobali l_VG;
	private double densitaFluido;
	/**
	 * 
	 */
	public SetupUtilizzatoreView() {
		super();
		this.initUI();
		this.comboBoxFrequenza.addItem("50");
		this.comboBoxFrequenza.addItem("60");
		this.textFieldTemperatura.setConSegno(true);
		this.textFieldAltezza.setConSegno(true);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
                if (!l_VG.currentUserSaaS.Ambiente.equals("MZ")) {
                    labelConsegna.setVisible(false);
                    labelResa.setVisible(false);
                    labelImballo.setVisible(false);
                    textFieldConsegna.setVisible(false);
                    textFieldImballo.setVisible(false);
                    textFieldResa.setVisible(false);
                }
	}
	
	@SuppressWarnings("rawtypes")
	public void Nazionalizza() {
		final UtilityCliente l_uc04 = this.l_VG.utilityCliente;
		this.labelTitoloSetupUtilizzatore.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Setup Utilizzatore") + "</html>");
		this.labelUser.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("User"));
		this.labelIndirizzo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Indirizzo"));
		this.labelRiferimento.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Riferimento"));
		this.labelRiferimentoInterno.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Riferimento interno"));
		this.labelTelefono.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Fax"));
		this.labelTelefono.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Telefono"));
		this.labelmail1.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("e-mail") + " 1");
		this.labelmail2.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("e-mail") + " 2");
		
		this.labelTitoloPreferenze.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Preferenze Utilizzatore") + "</html>");
		this.labelTemperatura.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Temperatura"));
		this.labelAltezza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Altezza"));
		this.labelFrequenza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza"));
		this.labelTestMot.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("test mot max"));
		this.labelInstallazione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Installazione"));
		this.labelConsegna.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Consegna"));
                this.labelResa.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Resa"));
                this.labelImballo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Imballo"));
                this.comboBoxAspMan.removeAllItems();
                this.comboBoxAspMan.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Aspirazione"));
                this.comboBoxAspMan.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Mandata"));
                this.comboBoxStaTot.removeAllItems();
                this.comboBoxStaTot.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Totale"));
                this.comboBoxStaTot.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Statica"));
                
		int installazioneIndex = -1;
		Collection itemIDs;
		Object obj[];
		try {
			final Object selected = this.comboBoxInstallazione.getValue();
	        itemIDs = this.comboBoxInstallazione.getItemIds();
	        obj = itemIDs.toArray();
	        for (int i=0 ; i<obj.length ; i++) {
	        	if (obj[i].equals(selected)) {
	        		installazioneIndex = i;
	        		break;
	        	}
	        }
			//Notification.show(Integer.toString(installazioneIndex)+"   "+selected.toString());
		} catch (final Exception e) {
			Notification.show(Integer.toString(installazioneIndex)+"   "+"null");
		}
		this.comboBoxInstallazione.removeAllItems();
        if (l_uc04 == null) {
			return;
		}
        for (int i=0 ; i<l_uc04.installazioneDesc.length ; i++) {
			this.comboBoxInstallazione.addItem(this.l_VG.utilityTraduzioni.TraduciStringa(l_uc04.installazioneDesc[i]));
		}
        if (installazioneIndex >= 0) {
        	itemIDs = this.comboBoxInstallazione.getItemIds();
        	obj = itemIDs.toArray();
        	this.comboBoxInstallazione.setValue(obj[installazioneIndex]);
        }
        //Notification.show("init!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}
	
	public void initSetup() {
		final UtilityCliente l_uc04 = this.l_VG.utilityCliente;
		Nazionalizza();
		this.textFieldUser.setValue(this.l_VG.currentUtilizzatore.user);
		this.textFieldIndirizzo.setValue(this.l_VG.currentUtilizzatore.indirizzo);
		this.textFieldRiferimento.setValue(this.l_VG.currentUtilizzatore.riferimento);
		this.textFieldRiferimentoInterno.setValue(this.l_VG.currentUtilizzatore.riferimentoInterno);
		this.textFieldTelefono.setValue(this.l_VG.currentUtilizzatore.telefono);
		this.textFieldFax.setValue(this.l_VG.currentUtilizzatore.fax);
		this.textFieldmail1.setValue(this.l_VG.currentUtilizzatore.email1);
		this.textFieldmail2.setValue(this.l_VG.currentUtilizzatore.email2);
		this.textFieldTemperatura.setValue((int)this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, this.l_VG.CACatalogo.temperatura));
		this.textFieldAltezza.setValue((int)(this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.CACatalogo.altezza, this.l_VG.UMAltezzaCorrente)));
		this.textFieldDensita020.setValue(this.l_VG.CA020Catalogo.rho);
		
		this.labelTemperaturaUM.setValue(this.l_VG.UMTemperaturaCorrente.simbolo);
		this.labelAltezzaUM.setValue(this.l_VG.UMAltezzaCorrente.simbolo);
		String l_str = this.l_VG.utilityTraduzioni.TraduciStringa("Correttore potenza motore [min ??%]");
		l_str = l_str.replace("??", Integer.toString(this.l_VG.CorrettorePotenzaMotoreMinimo));
		this.labelCorrettoreMotore.setValue(l_str);
        this.comboBoxFrequenza.setValue(Integer.toString(this.l_VG.CACatalogo.frequenza));
        this.integerStepperCorrettorePotenzaMotore.setMin(this.l_VG.CorrettorePotenzaMotoreMinimo);
        this.integerStepperCorrettorePotenzaMotore.setValue(this.l_VG.CorrettorePotenzaMotoreDaConfig);
        this.checkBoxTestMot.setValue(l_uc04.testGrandezzaMotoreMax);
        final Collection<?> itemIDs = this.comboBoxInstallazione.getItemIds();
        final Object obj[] = itemIDs.toArray();
        if (l_uc04.tipoInstallazioneDefault.equals("A")) {
			this.comboBoxInstallazione.setValue(obj[0]);
		} else if (l_uc04.tipoInstallazioneDefault.equals("B")) {
			this.comboBoxInstallazione.setValue(obj[1]);
		} else if (l_uc04.tipoInstallazioneDefault.equals("C")) {
			this.comboBoxInstallazione.setValue(obj[2]);
		} else if (l_uc04.tipoInstallazioneDefault.equals("D")) {
			this.comboBoxInstallazione.setValue(obj[3]);
		} else {
			this.comboBoxInstallazione.setValue(obj[0]);
		}
        if (l_uc04.TotStaDefault.equals("Totale"))
            this.comboBoxStaTot.setValue(comboBoxStaTot.getItemIds().toArray()[0]);
        else
            this.comboBoxStaTot.setValue(comboBoxStaTot.getItemIds().toArray()[1]);
        if (l_uc04.AspManDefault.equals("Aspirazione"))
            this.comboBoxAspMan.setValue(comboBoxAspMan.getItemIds().toArray()[0]);
        else
            this.comboBoxAspMan.setValue(comboBoxAspMan.getItemIds().toArray()[1]);
            
        //Notification.show(l_uc04.tipoInstallazioneDefault+"  "+obj[0].toString());
        this.textFieldConsegna.setValue(l_uc04.consegnaNoteDefault);
	this.textFieldImballo.setValue(l_uc04.imballoDefault);
        this.textFieldResa.setValue(l_uc04.resaDefault);
		buildDenditaCorrente();
	}
	
	public void azioneReset() {
		resetUtilizzatore();
		resetPreferenzeUtilizzatore();
		azioneOK();
	}
	
	public void azioneOK() {
		OKUtilizzatore();
		OKPreferenzeUtilizzatore();
	}
	
	private void resetUtilizzatore() {
		this.textFieldUser.setValue("");
		this.textFieldIndirizzo.setValue("");
		this.textFieldRiferimento.setValue("");
		this.textFieldRiferimentoInterno.setValue("");
		this.textFieldTelefono.setValue("");
		this.textFieldFax.setValue("");
		this.textFieldmail1.setValue("");
		this.textFieldmail2.setValue("");
	}
	
	private void resetPreferenzeUtilizzatore() {
		this.textFieldTemperatura.setValue(20);
		this.textFieldAltezza.setValue(0);
		this.textFieldDensita020.setValue(1.204);
		buildDenditaCorrente();
		this.comboBoxFrequenza.setValue("50");
		this.integerStepperCorrettorePotenzaMotore.setValue(this.l_VG.CorrettorePotenzaMotoreMinimo);
		this.checkBoxTestMot.setValue(true);
        final Collection<?> itemIDs = this.comboBoxInstallazione.getItemIds();
        final Object obj[] = itemIDs.toArray();
        this.comboBoxInstallazione.setValue(obj[0].toString());
        //Notification.show(obj[0].toString());
        this.textFieldConsegna.setValue("");
        this.textFieldImballo.setValue("Standard MZ 7€/Pallet");
        this.textFieldResa.setValue("Ex Works");
	}
	
	private void OKUtilizzatore() {
		this.l_VG.currentUtilizzatore.user = this.textFieldUser.getValue();
		this.l_VG.currentUtilizzatore.indirizzo = this.textFieldIndirizzo.getValue();
		this.l_VG.currentUtilizzatore.riferimento = this.textFieldRiferimento.getValue();
		this.l_VG.currentUtilizzatore.riferimentoInterno = this.textFieldRiferimentoInterno.getValue();
		this.l_VG.currentUtilizzatore.telefono = this.textFieldTelefono.getValue();
		this.l_VG.currentUtilizzatore.fax = this.textFieldFax.getValue();
		this.l_VG.currentUtilizzatore.email1 = this.textFieldmail1.getValue();
		this.l_VG.currentUtilizzatore.email2 = this.textFieldmail2.getValue();
		this.l_VG.dbConfig.storeUtilizzatore(this.l_VG.currentUtilizzatore);
	}
	
	private void OKPreferenzeUtilizzatore() {
		final UtilityCliente l_uc04 = this.l_VG.utilityCliente;
		if (this.comboBoxFrequenza.getValue() != null) {
			this.l_VG.CACatalogo.frequenza = Integer.parseInt(this.comboBoxFrequenza.getValue().toString());
		}
        this.l_VG.CACatalogo.altezza = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldAltezza.getIntegerValue(), this.l_VG.UMAltezzaCorrente);
        this.l_VG.CACatalogo.temperatura = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getIntegerValue());
        this.l_VG.CACatalogo.rho = this.densitaFluido;
        this.l_VG.CA020Catalogo.rho = this.textFieldDensita020.getDoubleValue();
        this.l_VG.CARicerca = (CondizioniAmbientali)this.l_VG.CACatalogo.clone();
        this.l_VG.CA020Ricerca = (CondizioniAmbientali)this.l_VG.CA020Catalogo.clone();
        this.l_VG.CAFree = (CondizioniAmbientali)this.l_VG.CACatalogo.clone();
        this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CARicerca.temperatura, this.l_VG.CARicerca.altezza);
        this.l_VG.dbConfig.storeCACatalogo(this.l_VG.CACatalogo);
        this.l_VG.dbConfig.storeCA020Catalogo(this.l_VG.CA020Catalogo);
        this.l_VG.CorrettorePotenzaMotoreDaConfig = this.integerStepperCorrettorePotenzaMotore.getValue();
        if (this.l_VG.CorrettorePotenzaMotoreDaConfig < this.l_VG.CorrettorePotenzaMotoreMinimo) {
			this.l_VG.CorrettorePotenzaMotoreDaConfig = this.l_VG.CorrettorePotenzaMotoreMinimo;
		}
        if (this.comboBoxInstallazione.getValue() != null) {
			l_uc04.tipoInstallazioneDefault = this.comboBoxInstallazione.getValue().toString().split(" ")[0];
		}
        l_uc04.consegnaNoteDefault = this.textFieldConsegna.getValue();
        l_uc04.testGrandezzaMotoreMax = this.checkBoxTestMot.getValue();
        l_uc04.resaDefault = this.textFieldResa.getValue();
        l_uc04.imballoDefault = this.textFieldImballo.getValue();
        l_uc04.AspManDefault = comboBoxAspMan.getValue().toString().equals(comboBoxAspMan.getItemIds().toArray()[0]) ? "Aspirazione" : "Mandata";
        l_uc04.TotStaDefault = comboBoxStaTot.getValue().toString().equals(comboBoxStaTot.getItemIds().toArray()[0]) ? "Totale" : "Statica";
        this.l_VG.utilityCliente.storePreferenzeCliente(this.l_VG.conConfig, this.l_VG.currentUserSaaS._ID, this.l_VG.CorrettorePotenzaMotoreDaConfig);
        this.l_VG.CorrettorePotenzaMotoreCorrente = this.l_VG.CorrettorePotenzaMotoreDaConfig;
	}
	
	private void buildDenditaCorrente() {
        String l_str = this.l_VG.utilityTraduzioni.TraduciStringa("Densità fluido a") + " ";
        this.labelDensita20C.setValue(l_str + "20 [C°]");
        this.labelDensitaXXC.setValue(l_str + Integer.toString(this.textFieldTemperatura.getIntegerValue()) + " " + this.l_VG.UMTemperaturaCorrente.simbolo);
        l_str = this.l_VG.utilityTraduzioni.TraduciStringa("e") + " 0 [m] " + this.l_VG.utilityTraduzioni.TraduciStringa("sul livello del mare");
        this.labelDensita0m.setValue(l_str);
        l_str = this.l_VG.utilityTraduzioni.TraduciStringa("e") + " " + Integer.toString(this.textFieldAltezza.getIntegerValue()) + " " + this.l_VG.UMAltezzaCorrente.simbolo + " " + this.l_VG.utilityTraduzioni.TraduciStringa("sul livello del mare");
        this.labelDensitaXXm.setValue(l_str);
        final double tCelsius = this.l_VG.utilityUnitaMisura.fromXXtoCelsius(this.l_VG.UMTemperaturaCorrente.index, this.textFieldTemperatura.getIntegerValue());
        final double altezzam = this.l_VG.utilityUnitaMisura.fromXXTom(this.textFieldAltezza.getIntegerValue(), this.l_VG.UMAltezzaCorrente);
        this.densitaFluido = this.l_VG.utilityFisica.calcolaDensitaFuido(this.textFieldDensita020.getDoubleValue(), tCelsius, altezzam, 0., 0.);
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(4);
        this.labelDensitaXXCXXm.setValue(this.l_VG.fmtNd.format(this.densitaFluido));
	}
	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldTemperatura}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldTemperatura_textChange(final FieldEvents.TextChangeEvent event) {
		buildDenditaCorrente();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldAltezza}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldAltezza_textChange(final FieldEvents.TextChangeEvent event) {
		buildDenditaCorrente();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldDensita020}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldDensita020_textChange(final FieldEvents.TextChangeEvent event) {
		buildDenditaCorrente();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.panel = new XdevPanel();
		this.verticalLayout = new XdevVerticalLayout();
		this.gridLayoutSetupUtilizzatore = new XdevGridLayout();
		this.labelTitoloSetupUtilizzatore = new XdevLabel();
		this.labelUser = new XdevLabel();
		this.textFieldUser = new XdevTextField();
		this.labelIndirizzo = new XdevLabel();
		this.textFieldIndirizzo = new XdevTextField();
		this.labelRiferimento = new XdevLabel();
		this.textFieldRiferimento = new XdevTextField();
		this.labelRiferimentoInterno = new XdevLabel();
		this.textFieldRiferimentoInterno = new XdevTextField();
		this.labelTelefono = new XdevLabel();
		this.textFieldTelefono = new XdevTextField();
		this.labelFax = new XdevLabel();
		this.textFieldFax = new XdevTextField();
		this.labelmail1 = new XdevLabel();
		this.textFieldmail1 = new XdevTextField();
		this.labelmail2 = new XdevLabel();
		this.textFieldmail2 = new XdevTextField();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.panel2 = new XdevPanel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.gridLayoutSetupUtilizzatore2 = new XdevGridLayout();
		this.labelTitoloPreferenze = new XdevLabel();
		this.labelTemperatura = new XdevLabel();
		this.textFieldTemperatura = new TextFieldInteger();
		this.labelTemperaturaUM = new XdevLabel();
		this.labelAltezza = new XdevLabel();
		this.textFieldAltezza = new TextFieldInteger();
		this.labelAltezzaUM = new XdevLabel();
		this.labelFrequenza = new XdevLabel();
		this.comboBoxFrequenza = new XdevComboBox<>();
                this.comboBoxAspMan = new XdevComboBox<>();
                this.comboBoxStaTot = new XdevComboBox<>();
		this.label3 = new XdevLabel();
		this.labelDensita20C = new XdevLabel();
		this.textFieldDensita020 = new TextFieldDouble();
		this.label4 = new XdevLabel();
		this.labelDensita0m = new XdevLabel();
		this.labelDensitaXXC = new XdevLabel();
		this.labelDensitaXXCXXm = new XdevLabel();
		this.label6 = new XdevLabel();
		this.labelDensitaXXm = new XdevLabel();
		this.labelCorrettoreMotore = new XdevLabel();
		this.integerStepperCorrettorePotenzaMotore = new IntegerStepper();
		this.labelTestMot = new XdevLabel();
		this.checkBoxTestMot = new XdevCheckBox();
		this.labelInstallazione = new XdevLabel();
		this.comboBoxInstallazione = new XdevComboBox<>();
		this.labelConsegna = new XdevLabel();
		this.textFieldConsegna = new XdevTextField();
                this.labelResa = new XdevLabel();
                this.textFieldResa = new XdevTextField();
                this.labelImballo = new XdevLabel();
                this.textFieldImballo = new XdevTextField();
                this.labelRicercaAspMan = new XdevLabel();
                this.labelRicercaTotSta = new XdevLabel();
		this.horizontalLayout3 = new XdevHorizontalLayout();
	
		this.horizontalLayout.setMargin(new MarginInfo(false, true, false, false));
		this.verticalLayout.setCaption("");
		this.gridLayoutSetupUtilizzatore.setCaption("");
		this.gridLayoutSetupUtilizzatore.setCaptionAsHtml(true);
		this.labelTitoloSetupUtilizzatore.setValue("<html><b><center>Setup Utilizzatore</html>");
		this.labelTitoloSetupUtilizzatore.setContentMode(ContentMode.HTML);
		this.labelUser.setValue("User");
		this.labelIndirizzo.setValue("Indirizzo");
		this.labelRiferimento.setValue("Riferimento");
		this.labelRiferimentoInterno.setValue("Riferimento Interno");
		this.labelTelefono.setValue("Telefono");
		this.labelFax.setValue("Fax");
		this.labelmail1.setValue("e-mail 1");
		this.labelmail2.setValue("e-mail 2");
		this.horizontalLayout2.setMargin(new MarginInfo(false));
		this.panel2.setTabIndex(0);
		this.verticalLayout2.setCaption("");
		this.gridLayoutSetupUtilizzatore2.setCaption("");
		this.gridLayoutSetupUtilizzatore2.setStyleName("rgb-gradient");
		this.gridLayoutSetupUtilizzatore2.setCaptionAsHtml(true);
		this.labelTitoloPreferenze.setValue("<html><b><center><center>Preferenze Utilizzatore</html>");
		this.labelTitoloPreferenze.setContentMode(ContentMode.HTML);
		this.labelTemperatura.setValue("Temperatura");
		this.labelTemperaturaUM.setValue("UM");
		this.labelTemperaturaUM.setContentMode(ContentMode.HTML);
		this.labelAltezza.setValue("Altezza");
		this.labelAltezzaUM.setValue("UM");
		this.labelAltezzaUM.setContentMode(ContentMode.HTML);
		this.labelFrequenza.setCaption("");
		this.labelFrequenza.setValue("Frequenza");
		this.label3.setValue("[Hz]");
		this.labelDensita20C.setValue("Desnsita 20C");
		this.label4.setValue("<html>[Kg/m<sup>3</sup>]</html>");
		this.label4.setContentMode(ContentMode.HTML);
		this.labelDensita0m.setValue("0m");
		this.labelDensitaXXC.setValue("Densita XXC");
		this.labelDensitaXXCXXm.setValue("???");
		this.label6.setValue("<html>[Kg/m<sup>3</sup>]</html>");
		this.label6.setContentMode(ContentMode.HTML);
		this.labelDensitaXXm.setValue("XXm");
		this.labelCorrettoreMotore.setValue("Correttore potenza motore");
		this.labelTestMot.setValue("Test mot max");
		this.checkBoxTestMot.setCaption("");
		this.labelInstallazione.setValue("Installazione");
		this.labelConsegna.setValue("Consegna");
		this.labelResa.setValue("Resa");
		this.labelImballo.setValue("Imballo");
                this.labelRicercaAspMan.setValue("Search Default");
                this.labelRicercaTotSta.setValue("Search Default");
		this.horizontalLayout3.setMargin(new MarginInfo(false));
	
		this.gridLayoutSetupUtilizzatore.setColumns(2);
		this.gridLayoutSetupUtilizzatore.setRows(10);
		this.labelTitoloSetupUtilizzatore.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelTitoloSetupUtilizzatore, 0, 0, 1, 0);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelTitoloSetupUtilizzatore, Alignment.MIDDLE_CENTER);
		this.labelUser.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelUser, 0, 1);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelUser, Alignment.TOP_RIGHT);
		this.textFieldUser.setWidth(100, Unit.PERCENTAGE);
		this.textFieldUser.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldUser, 1, 1);
		this.labelIndirizzo.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelIndirizzo, 0, 2);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelIndirizzo, Alignment.TOP_RIGHT);
		this.textFieldIndirizzo.setWidth(100, Unit.PERCENTAGE);
		this.textFieldIndirizzo.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldIndirizzo, 1, 2);
		this.labelRiferimento.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelRiferimento, 0, 3);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelRiferimento, Alignment.TOP_RIGHT);
		this.textFieldRiferimento.setWidth(100, Unit.PERCENTAGE);
		this.textFieldRiferimento.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldRiferimento, 1, 3);
		this.labelRiferimentoInterno.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelRiferimentoInterno, 0, 4);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelRiferimentoInterno, Alignment.TOP_RIGHT);
		this.textFieldRiferimentoInterno.setWidth(100, Unit.PERCENTAGE);
		this.textFieldRiferimentoInterno.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldRiferimentoInterno, 1, 4);
		this.labelTelefono.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelTelefono, 0, 5);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelTelefono, Alignment.TOP_RIGHT);
		this.textFieldTelefono.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTelefono.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldTelefono, 1, 5);
		this.labelFax.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelFax, 0, 6);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelFax, Alignment.TOP_RIGHT);
		this.textFieldFax.setWidth(100, Unit.PERCENTAGE);
		this.textFieldFax.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldFax, 1, 6);
		this.labelmail1.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelmail1, 0, 7);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelmail1, Alignment.TOP_RIGHT);
		this.textFieldmail1.setWidth(100, Unit.PERCENTAGE);
		this.textFieldmail1.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldmail1, 1, 7);
		this.labelmail2.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelmail2, 0, 8);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelmail2, Alignment.TOP_RIGHT);
		this.textFieldmail2.setWidth(100, Unit.PERCENTAGE);
		this.textFieldmail2.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore.addComponent(this.textFieldmail2, 1, 8);
		this.gridLayoutSetupUtilizzatore.setColumnExpandRatio(0, 10.0F);
		this.gridLayoutSetupUtilizzatore.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayoutSetupUtilizzatore_vSpacer = new CustomComponent();
		gridLayoutSetupUtilizzatore_vSpacer.setSizeFull();
		this.gridLayoutSetupUtilizzatore.addComponent(gridLayoutSetupUtilizzatore_vSpacer, 0, 9, 1, 9);
		this.gridLayoutSetupUtilizzatore.setRowExpandRatio(9, 1.0F);
		this.gridLayoutSetupUtilizzatore.setWidth(100, Unit.PERCENTAGE);
		this.gridLayoutSetupUtilizzatore.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.gridLayoutSetupUtilizzatore);
		this.verticalLayout.setComponentAlignment(this.gridLayoutSetupUtilizzatore, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout.setHeight(-1, Unit.PIXELS);
		this.panel.setContent(this.verticalLayout);
		this.gridLayoutSetupUtilizzatore2.setColumns(3);
		this.gridLayoutSetupUtilizzatore2.setRows(17);
		this.labelTitoloPreferenze.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelTitoloPreferenze, 0, 0, 1, 0);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelTitoloPreferenze, Alignment.MIDDLE_CENTER);
		this.labelTemperatura.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelTemperatura, 0, 1);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelTemperatura, Alignment.TOP_RIGHT);
		this.textFieldTemperatura.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTemperatura.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.textFieldTemperatura, 1, 1);
		this.labelTemperaturaUM.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelTemperaturaUM, 2, 1);
		this.labelAltezza.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelAltezza, 0, 2);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelAltezza, Alignment.TOP_RIGHT);
		this.textFieldAltezza.setWidth(100, Unit.PERCENTAGE);
		this.textFieldAltezza.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.textFieldAltezza, 1, 2);
		this.labelAltezzaUM.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelAltezzaUM, 2, 2);
		this.labelFrequenza.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelFrequenza, 0, 3);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelFrequenza, Alignment.TOP_RIGHT);
		this.comboBoxFrequenza.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxFrequenza.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.comboBoxFrequenza, 1, 3);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.comboBoxFrequenza, Alignment.BOTTOM_RIGHT);
		this.label3.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.label3, 2, 3);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.label3, Alignment.BOTTOM_LEFT);
		this.labelDensita20C.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelDensita20C, 0, 4);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelDensita20C, Alignment.TOP_RIGHT);
		this.textFieldDensita020.setWidth(100, Unit.PERCENTAGE);
		this.textFieldDensita020.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.textFieldDensita020, 1, 4);
		this.label4.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.label4, 2, 4);
		this.labelDensita0m.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelDensita0m, 0, 5);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelDensita0m, Alignment.TOP_RIGHT);
		this.labelDensitaXXC.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelDensitaXXC, 0, 6);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelDensitaXXC, Alignment.TOP_RIGHT);
		this.labelDensitaXXCXXm.setWidth(100, Unit.PERCENTAGE);
		this.labelDensitaXXCXXm.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelDensitaXXCXXm, 1, 6);
		this.label6.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.label6, 2, 6);
		this.labelDensitaXXm.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelDensitaXXm, 0, 7);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelDensitaXXm, Alignment.TOP_RIGHT);
		this.labelCorrettoreMotore.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelCorrettoreMotore, 0, 8);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelCorrettoreMotore, Alignment.TOP_RIGHT);
		this.integerStepperCorrettorePotenzaMotore.setWidth(100, Unit.PERCENTAGE);
		this.integerStepperCorrettorePotenzaMotore.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.integerStepperCorrettorePotenzaMotore, 1, 8, 2, 8);
		this.labelTestMot.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelTestMot, 0, 9);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelTestMot, Alignment.TOP_RIGHT);
		this.checkBoxTestMot.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTestMot.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.checkBoxTestMot, 1, 9);
		this.labelInstallazione.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelInstallazione, 0, 10);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelInstallazione, Alignment.TOP_RIGHT);
		this.comboBoxInstallazione.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxInstallazione.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.comboBoxInstallazione, 1, 10);
		this.labelConsegna.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelConsegna, 0, 11);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelConsegna, Alignment.TOP_RIGHT);
		this.textFieldConsegna.setWidth(100, Unit.PERCENTAGE);
		this.textFieldConsegna.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.textFieldConsegna, 1, 11);
		
                this.labelResa.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelResa, 0, 12);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelResa, Alignment.TOP_RIGHT);
		this.textFieldResa.setWidth(100, Unit.PERCENTAGE);
		this.textFieldResa.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.textFieldResa, 1, 12);
		
                this.labelImballo.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelImballo, 0, 13, Alignment.TOP_RIGHT);
		this.textFieldImballo.setWidth(100, Unit.PERCENTAGE);
		this.textFieldImballo.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.textFieldImballo, 1, 13);
		
                this.labelRicercaAspMan.setSizeUndefined();
                this.gridLayoutSetupUtilizzatore2.addComponent(this.labelRicercaAspMan, 0, 14, Alignment.TOP_RIGHT);
                this.comboBoxAspMan.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxAspMan.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.comboBoxAspMan, 1, 14, Alignment.BOTTOM_RIGHT);
                
                this.labelRicercaTotSta.setSizeUndefined();
                this.gridLayoutSetupUtilizzatore2.addComponent(this.labelRicercaTotSta, 0, 15, Alignment.TOP_RIGHT);
                this.comboBoxStaTot.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxStaTot.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.comboBoxStaTot, 1, 15, Alignment.BOTTOM_RIGHT);
                
                this.gridLayoutSetupUtilizzatore2.setColumnExpandRatio(0, 10.0F);
		this.gridLayoutSetupUtilizzatore2.setColumnExpandRatio(1, 10.0F);
		this.gridLayoutSetupUtilizzatore2.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayoutSetupUtilizzatore2_vSpacer = new CustomComponent();
		gridLayoutSetupUtilizzatore2_vSpacer.setSizeFull();
		this.gridLayoutSetupUtilizzatore2.addComponent(gridLayoutSetupUtilizzatore2_vSpacer, 0, 16, 2, 16);
		this.gridLayoutSetupUtilizzatore2.setRowExpandRatio(12, 1.0F);
		this.gridLayoutSetupUtilizzatore2.setWidth(100, Unit.PERCENTAGE);
		this.gridLayoutSetupUtilizzatore2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayoutSetupUtilizzatore2);
		this.verticalLayout2.setComponentAlignment(this.gridLayoutSetupUtilizzatore2, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.horizontalLayout3);
		this.verticalLayout2.setComponentAlignment(this.horizontalLayout3, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout2.setHeight(-1, Unit.PIXELS);
		this.panel2.setContent(this.verticalLayout2);
		this.panel.setWidth(100, Unit.PERCENTAGE);
		this.panel.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.panel);
		this.horizontalLayout.setExpandRatio(this.panel, 10.0F);
		this.panel2.setWidth(100, Unit.PERCENTAGE);
		this.panel2.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.panel2);
		this.horizontalLayout.setExpandRatio(this.panel2, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	
		this.textFieldTemperatura.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				SetupUtilizzatoreView.this.textFieldTemperatura_textChange(event);
			}
		});
		this.textFieldAltezza.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				SetupUtilizzatoreView.this.textFieldAltezza_textChange(event);
			}
		});
		this.textFieldDensita020.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				SetupUtilizzatoreView.this.textFieldDensita020_textChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelTitoloSetupUtilizzatore, labelUser, labelIndirizzo, labelRiferimento, labelRiferimentoInterno,
			labelTelefono, labelFax, labelmail1, labelmail2, labelTitoloPreferenze, labelTemperatura, labelTemperaturaUM,
			labelAltezza, labelAltezzaUM, labelFrequenza, label3, labelDensita20C, label4, labelDensita0m, labelDensitaXXC,
			labelDensitaXXCXXm, label6, labelDensitaXXm, labelCorrettoreMotore, labelTestMot, labelInstallazione,
			labelConsegna, labelImballo, labelResa, labelRicercaAspMan, labelRicercaTotSta;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2, horizontalLayout3;
	private TextFieldDouble textFieldDensita020;
	private XdevPanel panel, panel2;
	private XdevCheckBox checkBoxTestMot;
	private XdevGridLayout gridLayoutSetupUtilizzatore, gridLayoutSetupUtilizzatore2;
	private XdevTextField textFieldUser, textFieldIndirizzo, textFieldRiferimento, textFieldRiferimentoInterno,
			textFieldTelefono, textFieldFax, textFieldmail1, textFieldmail2, textFieldConsegna, textFieldImballo, textFieldResa;
	private XdevVerticalLayout verticalLayout, verticalLayout2;
	private TextFieldInteger textFieldTemperatura, textFieldAltezza;
	private XdevComboBox<CustomComponent> comboBoxFrequenza, comboBoxInstallazione, comboBoxAspMan, comboBoxStaTot;
	private IntegerStepper integerStepperCorrettorePotenzaMotore;
	// </generated-code>


}
