package com.cit.sellfan.ui.view.setup;

import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.GruppoSelezioneSerie;
import cit.sellfan.classi.KitAccessori;
import com.vaadin.ui.Notification;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SetupKitAccessori extends XdevView {
    private VariabiliGlobali l_VG;
    private boolean editEnabled;
    private final Container containerGruppo0 = new IndexedContainer();
    private final Container containerGruppo1 = new IndexedContainer();
    private final Container containerGruppo2 = new IndexedContainer();
    private final Container containerGruppo3 = new IndexedContainer();
    private final Container containerGruppo4 = new IndexedContainer();
    private final Container containerGruppo5 = new IndexedContainer();
    private final Container containerGruppo6 = new IndexedContainer();
    private final Container containerGruppo7 = new IndexedContainer();
    private final Container containerGruppo8 = new IndexedContainer();
    private final Container containerGruppo9 = new IndexedContainer();
    private List<Container> listContainer;
    private List<XdevTable<?>> listTable;
    private List<XdevPanel> listPanel;
    private List<XdevTextField> listTF;
    private List<XdevVerticalLayout> listVl;
    private HashMap<String, String> selezioneAccessori;
    /**
     * 
     */
    public SetupKitAccessori() {
        super();
        this.hlFila1 = new XdevHorizontalLayout();
        hlFila1.setSizeFull();
        this.hlFila2 = new XdevHorizontalLayout();
        this.hlFila3 = new XdevHorizontalLayout();
        this.main = new XdevVerticalLayout();
        hlFila2.setSizeFull();
        hlFila3.setSizeFull();
        main.setSpacing(false);
        
        tblKit0 = new XdevTable<>();
        tfKit0 = new XdevTextField();
        vlp0 = new XdevVerticalLayout();
        tblKit1 = new XdevTable<>();
        tfKit1 = new XdevTextField();
        vlp1 = new XdevVerticalLayout();
        tblKit2 = new XdevTable<>();
        tfKit2 = new XdevTextField();
        vlp2 = new XdevVerticalLayout();
        tblKit3 = new XdevTable<>();
        tfKit3 = new XdevTextField();
        vlp3 = new XdevVerticalLayout();
        tblKit4 = new XdevTable<>();
        tfKit4 = new XdevTextField();
        vlp4 = new XdevVerticalLayout();
        tblKit5 = new XdevTable<>();
        tfKit5 = new XdevTextField();
        vlp5 = new XdevVerticalLayout();
        tblKit6 = new XdevTable<>();
        tfKit6 = new XdevTextField();
        vlp6 = new XdevVerticalLayout();
        tblKit7 = new XdevTable<>();
        tfKit7 = new XdevTextField();
        vlp7 = new XdevVerticalLayout();
        tblKit8 = new XdevTable<>();
        tfKit8 = new XdevTextField();
        vlp8 = new XdevVerticalLayout();
        tblKit9 = new XdevTable<>();
        tfKit9 = new XdevTextField();
        vlp9 = new XdevVerticalLayout();
        panelKit0 = new XdevPanel();
        panelKit1 = new XdevPanel();
        panelKit2 = new XdevPanel();
        panelKit3 = new XdevPanel();
        panelKit4 = new XdevPanel();
        panelKit5 = new XdevPanel();
        panelKit6 = new XdevPanel();
        panelKit7 = new XdevPanel();
        panelKit8 = new XdevPanel();
        panelKit9 = new XdevPanel();
        
        listContainer = Arrays.asList(containerGruppo0, containerGruppo1, containerGruppo2, containerGruppo3, containerGruppo4, containerGruppo5, containerGruppo6, containerGruppo7, containerGruppo8, containerGruppo9);
        listTable = Arrays.asList(tblKit0, tblKit1, tblKit2, tblKit3, tblKit4, tblKit5, tblKit6, tblKit7, tblKit8, tblKit9);
        listPanel = Arrays.asList(panelKit0, panelKit1, panelKit2, panelKit3, panelKit4, panelKit5, panelKit6, panelKit7, panelKit8, panelKit9);
        listVl = Arrays.asList(vlp0, vlp1, vlp2, vlp3, vlp4, vlp5, vlp6, vlp7, vlp8, vlp9);
        listTF = Arrays.asList(tfKit0, tfKit1, tfKit2, tfKit3, tfKit4, tfKit5, tfKit6, tfKit7, tfKit8, tfKit9);
        this.initUI();
        int index = 0;
        for (Container c : listContainer) {
            c.removeAllItems();
            c.addContainerProperty("Selected", XdevCheckBox.class, false);
            c.addContainerProperty("Code", String.class, "");
            c.addContainerProperty("Description", String.class, "");
            listTable.get(index++).setContainerDataSource(c);
        }
    }

    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        
        selezioneAccessori = this.l_VG.utilityCliente.getListaAccessoriKit();
    }

    public void Nazionalizza() {
        for (XdevTextField tf : listTF) {
            tf.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione Kit"));
        }
        for (int i = 0; i<10; i++) {
            listTF.get(i).setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione Kit"));
            listTable.get(i).setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
            listTable.get(i).setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
            listTable.get(i).setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
        }
    }

    public void initSetup() {
        if (this.l_VG.utilityCliente.idSottoClienteIndex != 0) {
            for (int i = 0; i<10; i++) {
                buildTabellaKit(listTable.get(i), this.l_VG.ElencoKitAccessori.get(i));
                listTF.get(i).setValue(this.l_VG.ElencoKitAccessori.get(i).descrizione);
            }
        }
    }

    public void azioneReset() {
        for (int i=0 ; i<10 ; i++) {
            this.l_VG.ElencoKitAccessori.get(i).descrizione = "";
            this.l_VG.ElencoKitAccessori.get(i).elencoAccessori = null;
        }
        initSetup();
    }

    public void azioneOK() {
        for (int i=0; i<10; i++) {
            String descrizione = this.listTF.get(i).getValue().trim();
            if (descrizione.equals("")) {
                this.l_VG.ElencoKitAccessori.get(i).elencoAccessori = null;
                this.l_VG.ElencoKitAccessori.get(i).descrizione = "";
            } else {
                buildKit(descrizione, listContainer.get(i), this.l_VG.ElencoKitAccessori.get(i));
            }
        }
        for (int i=0 ; i<10 ; i++) {
            this.l_VG.dbConfig.storeKitAccessori(this.l_VG.ElencoKitAccessori.get(i), i);
        }
        initSetup();
    }

    private void buildTabellaKit(XdevTable<?> table, KitAccessori kitAccessori) {
        setEditEnabled(false);
        try {
            table.removeAllItems();
        } catch (final Exception e) {
            Notification.show("errore rimuoviTabellaKit");
        }
        for (int i=0; i<selezioneAccessori.keySet().size(); i++) {
            //table.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nome Kit"));
            String code = selezioneAccessori.keySet().toArray()[i].toString();
            final Object item[] = new Object[3];
            final String itemId = Integer.toString(i);
            final XdevCheckBox cb = new XdevCheckBox();
            cb.setValue(false);
            if (kitAccessori.elencoAccessori != null) {
                if (kitAccessori.elencoAccessori.contains(code)) {
                    cb.setValue(true);
                }
            }
            item[0] = cb;
            item[1] = code;
            item[2] = this.l_VG.utilityTraduzioni.TraduciStringa(selezioneAccessori.get(code).toString());
            table.addItem(item, itemId);
        }
        setEditEnabled(true);
    }
    
    private void buildKit(String descrizione, Container contAcc, KitAccessori kitAccessori) {
        final Collection<?> AccessoriID = contAcc.getItemIds();
        setEditEnabled(false);
        if (kitAccessori.elencoAccessori == null) kitAccessori.elencoAccessori = new ArrayList<>();
        kitAccessori.elencoAccessori.clear();
        for (final Object id : AccessoriID) {
            final Item item = contAcc.getItem(id);
            final XdevCheckBox cb = (XdevCheckBox) item.getItemProperty("Selected").getValue();
            if (cb.getValue()) {
                kitAccessori.elencoAccessori.add(item.getItemProperty("Code").getValue().toString());
            }
        }
        if (kitAccessori.elencoAccessori.contains("PAV") || kitAccessori.elencoAccessori.contains("AV")) 
            if (kitAccessori.elencoAccessori.contains("SA")) {}
            else kitAccessori.elencoAccessori.add("SA");
        if (kitAccessori.elencoAccessori.contains("SA")) kitAccessori.elencoAccessori.add("SOS");
        kitAccessori.descrizione = descrizione;
    }
    
    private void initUI() {
 
        for (XdevTable tab : listTable ) {
            tab.setStyleName("mystriped");
            tab.setPageLength(6);
            tab.setSizeFull();
//            tab.setColumnExpandRatio("Selected", 2f);
//            tab.setColumnExpandRatio("Code", 3f);
//            tab.setColumnExpandRatio("Description", 6f);
        }
        for (XdevTextField tf : listTF) {
            tf.setSizeFull();
            tf.setCaption("Kit Name");
            tf.setWidth(100, Unit.PERCENTAGE);
            tf.setHeight(-1, Unit.PIXELS);
        }
       
        for (int i = 0; i<9; i++) {
            listVl.get(i).addComponent(listTF.get(i), Alignment.MIDDLE_LEFT);            
            listVl.get(i).addComponent(listTable.get(i), Alignment.MIDDLE_CENTER);
            
            listPanel.get(i).setContent(listVl.get(i));
            listPanel.get(i).setSizeFull();
            if (i<3){
                hlFila1.addComponent(listPanel.get(i));
            }
            else if (i<6) hlFila2.addComponent(listPanel.get(i));
            else hlFila3.addComponent(listPanel.get(i));
            
        }
        main.addComponent(hlFila1);
        main.addComponent(hlFila2);
        main.addComponent(hlFila3);
        hlFila1.addStyleName("horNoSpace");
        main.setSpacing(false);
        this.setContent(main);
    } // </generated-code>

    public boolean isEditEnabled() {
            return this.editEnabled;
    }

    public void setEditEnabled(final boolean editEnabled) {
            this.editEnabled = editEnabled;
    }
    private XdevVerticalLayout vlp0, vlp1, vlp2, vlp3, vlp4, vlp5, vlp6, vlp7, vlp8, vlp9, main;
    private XdevHorizontalLayout hlFila1, hlFila2, hlFila3;
    private XdevTable<?> tblKit0, tblKit1, tblKit2, tblKit3, tblKit4, tblKit5, tblKit6, tblKit7, tblKit8, tblKit9;
    private XdevPanel panelKit0, panelKit1, panelKit2, panelKit3, panelKit4, panelKit5, panelKit6, panelKit7, panelKit8, panelKit9;
    private XdevTextField tfKit0, tfKit1, tfKit2, tfKit3, tfKit4, tfKit5, tfKit6, tfKit7, tfKit8, tfKit9;

}
