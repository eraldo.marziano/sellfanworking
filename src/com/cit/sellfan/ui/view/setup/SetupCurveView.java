package com.cit.sellfan.ui.view.setup;

import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevColorPicker;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.sellfan.classi.AssiGrafici;
import cit.sellfan.classi.ParametriVari;
import cit.sellfan.classi.titoli.StringheUNI;
import com.vaadin.ui.Notification;

public class SetupCurveView extends XdevView {
	private VariabiliGlobali l_VG;

	/**
	 * 
	 */
	public SetupCurveView() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void setWidth(final int width) {
		this.horizontalLayout2.setWidth(width, Unit.PIXELS);
		//horizontalLayout2.setHeight(height, Unit.PIXELS);
	}
	
	@SuppressWarnings("rawtypes")
	private void nazionalizzaComboBoxAssi(final XdevComboBox<CustomComponent> comboBox) {
		int index = -1;
		Collection itemIDs;
		Object obj[];
		try {
			final Object selected = comboBox.getValue();
	        itemIDs = comboBox.getItemIds();
	        obj = itemIDs.toArray();
	        for (int i=0 ; i<obj.length ; i++) {
	        	if (obj[i].equals(selected)) {
	        		index = i;
	        		break;
	        	}
	        }
			//Notification.show(Integer.toString(installazioneIndex)+"   "+selected.toString());
		} catch (final Exception e) {
			Notification.show("Errore Nazionalizza Combobox");
		}
		comboBox.removeAllItems();
		comboBox.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Lineare"));
		comboBox.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Logaritmico"));
        if (index >= 0) {
        	itemIDs = comboBox.getItemIds();
        	obj = itemIDs.toArray();
        	comboBox.setValue(obj[index]);
        }
	}
	
	public void Nazionalizza() {
		this.labelTitoloSetupVarie.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Varie"));
		this.labelTitoloSetupAssi.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Assi") + "</html>");
		this.labelPortata.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Portata"));
		this.labelPressione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione"));
		this.labelPotenza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza"));
		this.labelTitoloSetupColoriCurve.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Curve") + "</html>");
		this.colorPickerPressioneTotale.setCaption("<html>" + this.l_VG.utilityTraduzioni.TraduciStringa("Curva Pressione Totale") + " - " + StringheUNI.PRESSIONETOTALE + "</html>");
		this.colorPickerPressioneStatica.setCaption("<html>" + this.l_VG.utilityTraduzioni.TraduciStringa("Curva Pressione Statica") + " - " + StringheUNI.PRESSIONESTATICA + "</html>");
		this.colorPickerPotenza.setCaption("<html>" + this.l_VG.utilityTraduzioni.TraduciStringa("Curva Potenza") + " - " + StringheUNI.POTENZAASSE + "</html>");
		this.colorPickerRendimento.setCaption("<html>" + this.l_VG.utilityTraduzioni.TraduciStringa("Curva Rendimento") + " - " + StringheUNI.RENDIMENTO + "</html>");
		this.colorPickerCaratteristica.setCaption("<html>" + this.l_VG.utilityTraduzioni.TraduciStringa("Curva Caratteristica") + "</html>");
		this.colorPickerRumore.setCaption("<html>" + this.l_VG.utilityTraduzioni.TraduciStringa("Curva Rumore") + " - " + StringheUNI.POTENZASONORA + "</html>");
		nazionalizzaComboBoxAssi(this.comboBoxPortata);
		nazionalizzaComboBoxAssi(this.comboBoxPressione);
		nazionalizzaComboBoxAssi(this.comboBoxPotenza);
/*
		int index = -1;
		Collection itemIDs;
		Object obj[];
		try {
			final Object selected = this.comboBoxPortata.getValue();
	        itemIDs = this.comboBoxPortata.getItemIds();
	        obj = itemIDs.toArray();
	        for (int i=0 ; i<obj.length ; i++) {
	        	if (obj[i].equals(selected)) {
	        		index = i;
	        		break;
	        	}
	        }
			//Notification.show(Integer.toString(installazioneIndex)+"   "+selected.toString());
		} catch (final Exception e) {
			//Notification.show(Integer.toString(installazioneIndex)+"   "+"null");
		}
		this.comboBoxPortata.removeAllItems();
		this.comboBoxPortata.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Lineare"));
		this.comboBoxPortata.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Logaritmico"));
        if (index >= 0) {
        	itemIDs = this.comboBoxPortata.getItemIds();
        	obj = itemIDs.toArray();
        	this.comboBoxPortata.setValue(obj[index]);
        }
        index = -1;
		try {
			final Object selected = this.comboBoxPressione.getValue();
	        itemIDs = this.comboBoxPressione.getItemIds();
	        obj = itemIDs.toArray();
	        for (int i=0 ; i<obj.length ; i++) {
	        	if (obj[i].equals(selected)) {
	        		index = i;
	        		break;
	        	}
	        }
			//Notification.show(Integer.toString(installazioneIndex)+"   "+selected.toString());
		} catch (final Exception e) {
			//Notification.show(Integer.toString(installazioneIndex)+"   "+"null");
		}
		this.comboBoxPressione.removeAllItems();
		this.comboBoxPressione.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Lineare"));
		this.comboBoxPressione.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Logaritmico"));
        if (index >= 0) {
        	itemIDs = this.comboBoxPressione.getItemIds();
        	obj = itemIDs.toArray();
        	this.comboBoxPressione.setValue(obj[index]);
        }
        index = -1;
		try {
			final Object selected = this.comboBoxPotenza.getValue();
	        itemIDs = this.comboBoxPotenza.getItemIds();
	        obj = itemIDs.toArray();
	        for (int i=0 ; i<obj.length ; i++) {
	        	if (obj[i].equals(selected)) {
	        		index = i;
	        		break;
	        	}
	        }
			//Notification.show(Integer.toString(installazioneIndex)+"   "+selected.toString());
		} catch (final Exception e) {
			//Notification.show(Integer.toString(installazioneIndex)+"   "+"null");
		}
		this.comboBoxPotenza.removeAllItems();
		this.comboBoxPotenza.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Lineare"));
		this.comboBoxPotenza.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Logaritmico"));
        if (index >= 0) {
        	itemIDs = this.comboBoxPotenza.getItemIds();
        	obj = itemIDs.toArray();
        	this.comboBoxPotenza.setValue(obj[index]);
        }
*/
		this.labelTitoloSetupUM.setValue("<html><b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Unità di Misura") + "</html>");
		this.labelPortata2.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Portata"));
		this.labelPressione2.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione"));
		this.labelPotenza2.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza"));
		this.labelTemperatura.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Temperatura"));
		this.labelAltezza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Altezza sul livello del mare"));
		this.labelDiametro.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Diametro Girante"));

		if (this.comboBoxPortata2.isEmpty()) {
			this.comboBoxPortata2.removeAllItems();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.size() ; i++) {
	            this.comboBoxPortata2.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.get(i).simbolo));
	        }
		}
		if (this.comboBoxPressione2.isEmpty()) {
			this.comboBoxPressione2.removeAllItems();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.size() ; i++) {
	        	this.comboBoxPressione2.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.get(i).simbolo));
	        }
		}
		if (this.comboBoxPotenza2.isEmpty()) {
			this.comboBoxPotenza2.removeAllItems();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPotenza.size() ; i++) {
	        	this.comboBoxPotenza2.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPotenza.get(i).simbolo));
	        }
		}
		if (this.comboBoxTemperatura.isEmpty()) {
	        this.comboBoxTemperatura.removeAllItems();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.size() ; i++) {
	        	this.comboBoxTemperatura.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.get(i).simbolo));
	        }
		}
		if (this.comboBoxAltezza.isEmpty()) {
	        this.comboBoxAltezza.removeAllItems();
	        this.comboBoxDiametro.removeAllItems();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.size() ; i++) {
	        	this.comboBoxAltezza.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(i).simbolo));
	        	this.comboBoxDiametro.addItem(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(i).simbolo));
	        }
		}
        this.labelPotenzaDisponibile.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Potenza Disponibile"));
        this.labelTratteggio.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tratteggio"));
        this.labelCC.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Curva Caratteristica"));
        
        this.labelStepPortata.setValue("Step " + this.l_VG.utilityTraduzioni.TraduciStringa("Portata"));
        this.labelStepPressioneSonora.setValue("Step " + this.l_VG.utilityTraduzioni.TraduciStringa("Distanza") + " " + this.l_VG.utilityTraduzioni.TraduciStringa("Pressione Sonora"));
	}
	
	public void initSetup() {
		String UMDesc;
		String tipoAsse;
		if (this.l_VG.assiGraficiDefault.tipoAssePortata == 0) {
			tipoAsse = this.l_VG.utilityTraduzioni.TraduciStringa("Lineare");
		} else {
			tipoAsse = this.l_VG.utilityTraduzioni.TraduciStringa("Logaritmico");
		}
		this.comboBoxPortata.setValue(tipoAsse);
		if (this.l_VG.assiGraficiDefault.tipoAssePressione == 0) {
			tipoAsse = this.l_VG.utilityTraduzioni.TraduciStringa("Lineare");
		} else {
			tipoAsse = this.l_VG.utilityTraduzioni.TraduciStringa("Logaritmico");
		}
		this.comboBoxPressione.setValue(tipoAsse);
		if (this.l_VG.assiGraficiDefault.tipoAssePotenza == 0) {
			tipoAsse = this.l_VG.utilityTraduzioni.TraduciStringa("Lineare");
		} else {
			tipoAsse = this.l_VG.utilityTraduzioni.TraduciStringa("Logaritmico");
		}
		this.comboBoxPotenza.setValue(tipoAsse);
		Color colore = new Color(this.l_VG.assiGraficiDefault.colorePressioneTotale.getRed(), this.l_VG.assiGraficiDefault.colorePressioneTotale.getGreen(), this.l_VG.assiGraficiDefault.colorePressioneTotale.getBlue());
		this.colorPickerPressioneTotale.setColor(colore);
		colore = new Color(this.l_VG.assiGraficiDefault.colorePressioneStatica.getRed(), this.l_VG.assiGraficiDefault.colorePressioneStatica.getGreen(), this.l_VG.assiGraficiDefault.colorePressioneStatica.getBlue());
		this.colorPickerPressioneStatica.setColor(colore);
		colore = new Color(this.l_VG.assiGraficiDefault.colorePotenza.getRed(), this.l_VG.assiGraficiDefault.colorePotenza.getGreen(), this.l_VG.assiGraficiDefault.colorePotenza.getBlue());
		this.colorPickerPotenza.setColor(colore);
		colore = new Color(this.l_VG.assiGraficiDefault.coloreRendimento.getRed(), this.l_VG.assiGraficiDefault.coloreRendimento.getGreen(), this.l_VG.assiGraficiDefault.coloreRendimento.getBlue());
		this.colorPickerRendimento.setColor(colore);
		colore = new Color(this.l_VG.assiGraficiDefault.coloreCurvaCaratteristica.getRed(), this.l_VG.assiGraficiDefault.coloreCurvaCaratteristica.getGreen(), this.l_VG.assiGraficiDefault.coloreCurvaCaratteristica.getBlue());
		this.colorPickerCaratteristica.setColor(colore);
		colore = new Color(this.l_VG.assiGraficiDefault.colorePotenzaSonora.getRed(), this.l_VG.assiGraficiDefault.colorePotenzaSonora.getGreen(), this.l_VG.assiGraficiDefault.colorePotenzaSonora.getBlue());
		this.colorPickerRumore.setColor(colore);

		UMDesc = this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.get(this.l_VG.UMPortataCorrente.index).simbolo);
		this.comboBoxPortata2.setValue(UMDesc);
		UMDesc = this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.get(this.l_VG.UMPressioneCorrente.index).simbolo);
		this.comboBoxPressione2.setValue(UMDesc);
		UMDesc = this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPotenza.get(this.l_VG.UMPotenzaCorrente.index).simbolo);
		this.comboBoxPotenza2.setValue(UMDesc);
		UMDesc = this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.get(this.l_VG.UMTemperaturaCorrente.index).simbolo);
		this.comboBoxTemperatura.setValue(UMDesc);
		UMDesc = this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(this.l_VG.UMAltezzaCorrente.index).simbolo);
		this.comboBoxAltezza.setValue(UMDesc);
		UMDesc = this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(this.l_VG.UMDiametroCorrente.index).simbolo);
		this.comboBoxDiametro.setValue(UMDesc);
		
		this.checkBoxPotenzaDisponibile.setValue(this.l_VG.ParametriVari.pannelloPrestazionipotenzaInstallataFlag);
		this.checkBoxTratteggio.setValue(this.l_VG.ParametriVari.pannelloPrestazionitratteggioVisibleFlag);
		this.checkBoxCC.setValue(this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag);
		
		this.textFieldStepPortata.setValue(this.l_VG.ParametriVari.stepPortatam3h);
		this.textFieldStepRpm.setValue(this.l_VG.ParametriVari.stepRpm);
		this.textFieldStepPressioneSonora.setValue(this.l_VG.ParametriVari.stepDistanzaRumorem);
	}
	
	public void azioneReset() {
		resetCurve();
		resetUM();
		resetVarie();
		initSetup();
		azioneOK();
	}
	
	public void azioneOK() {
		OKCurve();
		OKUM();
		OKVarie();
	}
	
	private void resetCurve() {
		this.l_VG.assiGraficiDefault = new AssiGrafici(this.l_VG.utilityTraduzioni);
	}
	
	private void resetUM() {
		this.l_VG.UMDefaultIndex = this.l_VG.utilityCliente.getUMDefaultIndex();
		this.l_VG.setUMFromDefaultIndex();
	}
	
	private void resetVarie() {
		this.l_VG.ParametriVari = new ParametriVari();
/*
        this.l_VG.ParametriVari.pannelloPrestazionipotenzaInstallataFlag = this.checkBoxPotenzaDisponibile.getValue();
        this.l_VG.ParametriVari.pannelloPrestazionitratteggioVisibleFlag = this.checkBoxTratteggio.getValue();
        this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag = this.checkBoxCC.getValue();

		
		
		
		this.l_VG.ParametriVari.stepPortatam3h = 10.0;
		this.l_VG.ParametriVari.stepRpm = 100;
		this.l_VG.ParametriVari.stepDistanzaRumorem = 1.0;
*/
	}
	
	private void OKCurve() {
		int tipoAsse = 0;
		if (this.comboBoxPortata.getValue() != null) {
			if (this.comboBoxPortata.getValue().toString().equals(this.l_VG.utilityTraduzioni.TraduciStringa("Lineare"))) {
				tipoAsse = 0;
			} else {
				tipoAsse = 1;
			}
		}
		this.l_VG.assiGraficiDefault.tipoAssePortata = tipoAsse;
		tipoAsse = 0;
		if (this.comboBoxPressione.getValue() != null) {
			if (this.comboBoxPressione.getValue().toString().equals(this.l_VG.utilityTraduzioni.TraduciStringa("Lineare"))) {
				tipoAsse = 0;
			} else {
				tipoAsse = 1;
			}
		}
		this.l_VG.assiGraficiDefault.tipoAssePressione = tipoAsse;
		tipoAsse = 0;
		if (this.comboBoxPotenza.getValue() != null) {
			if (this.comboBoxPotenza.getValue().toString().equals(this.l_VG.utilityTraduzioni.TraduciStringa("Lineare"))) {
				tipoAsse = 0;
			} else {
				tipoAsse = 1;
			}
		}
		this.l_VG.assiGraficiDefault.tipoAssePotenza = tipoAsse;
		Color colore = this.colorPickerPressioneTotale.getColor();
		this.l_VG.assiGraficiDefault.colorePressioneTotale = new java.awt.Color(colore.getRed(), colore.getGreen(), colore.getBlue());
		colore = this.colorPickerPressioneStatica.getColor();
		this.l_VG.assiGraficiDefault.colorePressioneStatica = new java.awt.Color(colore.getRed(), colore.getGreen(), colore.getBlue());
		colore = this.colorPickerPotenza.getColor();
		this.l_VG.assiGraficiDefault.colorePotenza = new java.awt.Color(colore.getRed(), colore.getGreen(), colore.getBlue());
		colore = this.colorPickerRendimento.getColor();
		this.l_VG.assiGraficiDefault.coloreRendimento = new java.awt.Color(colore.getRed(), colore.getGreen(), colore.getBlue());
		colore = this.colorPickerCaratteristica.getColor();
		this.l_VG.assiGraficiDefault.coloreCurvaCaratteristica = new java.awt.Color(colore.getRed(), colore.getGreen(), colore.getBlue());
		colore = this.colorPickerRumore.getColor();
		this.l_VG.assiGraficiDefault.colorePotenzaSonora = new java.awt.Color(colore.getRed(), colore.getGreen(), colore.getBlue());
        this.l_VG.dbConfig.storeAssigrafici(this.l_VG.assiGraficiDefault);
	}
	
	private void OKUM() {
		String UMDesc;
		if (this.comboBoxPortata2.getValue() != null) {
			UMDesc = this.comboBoxPortata2.getValue().toString();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.size() ; i++) {
	        	if (UMDesc.equals(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPortata.get(i).simbolo))) {
	        		this.l_VG.UMPortataCorrente = this.l_VG.utilityUnitaMisura.getPortataUM(i);
	        		break;
	        	}
	        }
		}
		if (this.comboBoxPressione2.getValue() != null) {
	        UMDesc = this.comboBoxPressione2.getValue().toString();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.size() ; i++) {
	        	if (UMDesc.equals(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPressione.get(i).simbolo))) {
	        		this.l_VG.UMPressioneCorrente = this.l_VG.utilityUnitaMisura.getPressioneUM(i);
	        		break;
	        	}
	        }
		}
		if (this.comboBoxPotenza2.getValue() != null) {
	        UMDesc = this.comboBoxPotenza2.getValue().toString();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPotenza.size() ; i++) {
	        	if (UMDesc.equals(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraPotenza.get(i).simbolo))) {
	        		this.l_VG.UMPotenzaCorrente = this.l_VG.utilityUnitaMisura.getPotenzaUM(i);
	        		break;
	        	}
	        }
		}
		if (this.labelTemperatura.getValue() != null) {
	        UMDesc = this.comboBoxTemperatura.getValue().toString();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.size() ; i++) {
	        	if (UMDesc.equals(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraTemperatura.get(i).simbolo))) {
	        		this.l_VG.UMTemperaturaCorrente = this.l_VG.utilityUnitaMisura.getTemperaturaUM(i);
	        		break;
	        	}
	        }
		}
		if (this.comboBoxAltezza.getValue() != null) {
	        UMDesc = this.comboBoxAltezza.getValue().toString();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.size() ; i++) {
	        	if (UMDesc.equals(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(i).simbolo))) {
	        		this.l_VG.UMAltezzaCorrente = this.l_VG.utilityUnitaMisura.getLunghezzaUM(i);
	        		break;
	        	}
	        }
		}
		if (this.comboBoxDiametro.getValue() != null) {
	        UMDesc = this.comboBoxDiametro.getValue().toString();
	        for (int i=0 ; i<this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.size() ; i++) {
	        	if (UMDesc.equals(this.l_VG.utility.filtraHTMLTags(this.l_VG.utilityUnitaMisura.ElencoUnitaMisuraLunghezza.get(i).simbolo))) {
	        		this.l_VG.UMDiametroCorrente = this.l_VG.utilityUnitaMisura.getLunghezzaUM(i);
	        		break;
	        	}
	        }
		}
        this.l_VG.getUMForDefaultIndex();
        this.l_VG.dbConfig.storeUnitaMisura(this.l_VG.UMDefaultIndex);
	}
	
	private void OKVarie() {
		this.l_VG.ParametriVari.pannelloPrestazionipotenzaInstallataFlag = this.checkBoxPotenzaDisponibile.getValue();
        this.l_VG.ParametriVari.pannelloPrestazionitratteggioVisibleFlag = this.checkBoxTratteggio.getValue();
        this.l_VG.ParametriVari.pannelloPrestazionicaratteristicaFlag = this.checkBoxCC.getValue();
        this.l_VG.ParametriVari.stepPortatam3h = this.textFieldStepPortata.getDoubleValue();
        this.l_VG.ParametriVari.stepRpm = this.textFieldStepRpm.getIntegerValue();
        this.l_VG.ParametriVari.stepDistanzaRumorem = this.textFieldStepPressioneSonora.getDoubleValue();
        this.l_VG.dbConfig.storeParametriVari(this.l_VG.ParametriVari);
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonReset_buttonClick(final Button.ClickEvent event) {
		this.l_VG.assiGraficiDefault = new AssiGrafici();
		initSetup();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.panel = new XdevPanel();
		this.verticalLayout = new XdevVerticalLayout();
		this.gridLayout = new XdevGridLayout();
		this.labelTitoloSetupAssi = new XdevLabel();
		this.labelPortata = new XdevLabel();
		this.comboBoxPortata = new XdevComboBox<>();
		this.labelPressione = new XdevLabel();
		this.comboBoxPressione = new XdevComboBox<>();
		this.labelPotenza = new XdevLabel();
		this.comboBoxPotenza = new XdevComboBox<>();
		this.gridLayoutSetupUtilizzatore = new XdevGridLayout();
		this.labelTitoloSetupColoriCurve = new XdevLabel();
		this.colorPickerPressioneTotale = new XdevColorPicker();
		this.colorPickerPressioneStatica = new XdevColorPicker();
		this.colorPickerPotenza = new XdevColorPicker();
		this.colorPickerRendimento = new XdevColorPicker();
		this.colorPickerCaratteristica = new XdevColorPicker();
		this.colorPickerRumore = new XdevColorPicker();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.buttonReset = new XdevButton();
		this.panel2 = new XdevPanel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.gridLayout2 = new XdevGridLayout();
		this.labelTitoloSetupUM = new XdevLabel();
		this.labelPortata2 = new XdevLabel();
		this.comboBoxPortata2 = new XdevComboBox<>();
		this.labelPressione2 = new XdevLabel();
		this.comboBoxPressione2 = new XdevComboBox<>();
		this.labelPotenza2 = new XdevLabel();
		this.comboBoxPotenza2 = new XdevComboBox<>();
		this.labelTemperatura = new XdevLabel();
		this.comboBoxTemperatura = new XdevComboBox<>();
		this.labelAltezza = new XdevLabel();
		this.comboBoxAltezza = new XdevComboBox<>();
		this.labelDiametro = new XdevLabel();
		this.comboBoxDiametro = new XdevComboBox<>();
		this.gridLayoutSetupUtilizzatore2 = new XdevGridLayout();
		this.labelTitoloSetupVarie = new XdevLabel();
		this.labelPotenzaDisponibile = new XdevLabel();
		this.checkBoxPotenzaDisponibile = new XdevCheckBox();
		this.labelTratteggio = new XdevLabel();
		this.checkBoxTratteggio = new XdevCheckBox();
		this.labelCC = new XdevLabel();
		this.checkBoxCC = new XdevCheckBox();
		this.gridLayoutSetupStep = new XdevGridLayout();
		this.labelTitoloSetupStep = new XdevLabel();
		this.labelStepPortata = new XdevLabel();
		this.textFieldStepPortata = new TextFieldDouble();
		this.label = new XdevLabel();
		this.labelStepRpm = new XdevLabel();
		this.textFieldStepRpm = new TextFieldInteger();
		this.label2 = new XdevLabel();
		this.labelStepPressioneSonora = new XdevLabel();
		this.textFieldStepPressioneSonora = new TextFieldDouble();
		this.label3 = new XdevLabel();
	
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.panel.setTabIndex(0);
		this.verticalLayout.setCaption("");
		this.gridLayout.setCaption("");
		this.gridLayout.setCaptionAsHtml(true);
		this.labelTitoloSetupAssi.setValue("Assi");
		this.labelTitoloSetupAssi.setContentMode(ContentMode.HTML);
		this.labelPortata.setValue("Portata");
		this.labelPortata.setContentMode(ContentMode.HTML);
		this.labelPressione.setValue("Pressione");
		this.labelPressione.setContentMode(ContentMode.HTML);
		this.labelPotenza.setValue("Potenza");
		this.labelPotenza.setContentMode(ContentMode.HTML);
		this.gridLayoutSetupUtilizzatore.setCaption("");
		this.gridLayoutSetupUtilizzatore.setCaptionAsHtml(true);
		this.labelTitoloSetupColoriCurve.setValue("Curve");
		this.labelTitoloSetupColoriCurve.setContentMode(ContentMode.HTML);
		this.colorPickerPressioneTotale.setCaption("pt");
		this.colorPickerPressioneTotale.setCaptionAsHtml(true);
		this.colorPickerPressioneStatica.setCaption("ps");
		this.colorPickerPressioneStatica.setCaptionAsHtml(true);
		this.colorPickerPotenza.setCaption("w");
		this.colorPickerPotenza.setCaptionAsHtml(true);
		this.colorPickerRendimento.setCaption("etha");
		this.colorPickerRendimento.setCaptionAsHtml(true);
		this.colorPickerCaratteristica.setCaption("Curva Caratteristica");
		this.colorPickerCaratteristica.setCaptionAsHtml(true);
		this.colorPickerRumore.setCaption("rumore");
		this.colorPickerRumore.setCaptionAsHtml(true);
		this.horizontalLayout2.setMargin(new MarginInfo(true, false, true, false));
		this.buttonReset.setCaption("Reset");
		this.panel2.setTabIndex(0);
		this.verticalLayout2.setCaption("");
		this.gridLayout2.setCaption("");
		this.gridLayout2.setCaptionAsHtml(true);
		this.labelTitoloSetupUM.setValue("UM");
		this.labelTitoloSetupUM.setContentMode(ContentMode.HTML);
		this.labelPortata2.setValue("Portata");
		this.labelPortata2.setContentMode(ContentMode.HTML);
		this.labelPressione2.setValue("Pressione");
		this.labelPressione2.setContentMode(ContentMode.HTML);
		this.labelPotenza2.setValue("Potenza");
		this.labelPotenza2.setContentMode(ContentMode.HTML);
		this.labelTemperatura.setValue("Temperatura");
		this.labelAltezza.setValue("Altezza sul livello del mare");
		this.labelDiametro.setValue("Diametro Girante");
		this.gridLayoutSetupUtilizzatore2.setCaption("");
		this.gridLayoutSetupUtilizzatore2.setCaptionAsHtml(true);
		this.labelTitoloSetupVarie.setValue("Varie");
		this.labelTitoloSetupVarie.setContentMode(ContentMode.HTML);
		this.labelPotenzaDisponibile.setValue("Potenza Disponibile");
		this.checkBoxPotenzaDisponibile.setCaption("visible");
		this.labelTratteggio.setValue("Tratteggio");
		this.checkBoxTratteggio.setCaption("enabled");
		this.labelCC.setValue("Curva Caratteristica");
		this.checkBoxCC.setCaption("visible");
		this.gridLayoutSetupStep.setCaption("");
		this.gridLayoutSetupStep.setCaptionAsHtml(true);
		this.labelTitoloSetupStep.setValue("Step");
		this.labelTitoloSetupStep.setContentMode(ContentMode.HTML);
		this.labelStepPortata.setValue("Portata");
		this.textFieldStepPortata.setColumns(5);
		this.label.setValue("[m<sup>3</sup>/h]");
		this.label.setContentMode(ContentMode.HTML);
		this.labelStepRpm.setValue("Step Rpm");
		this.textFieldStepRpm.setColumns(5);
		this.label2.setValue("[Rpm]");
		this.labelStepPressioneSonora.setValue("Distanza Pressione Sonora");
		this.textFieldStepPressioneSonora.setColumns(5);
		this.label3.setValue("[m]");
	
		this.gridLayout.setColumns(2);
		this.gridLayout.setRows(5);
		this.labelTitoloSetupAssi.setSizeUndefined();
		this.gridLayout.addComponent(this.labelTitoloSetupAssi, 0, 0, 1, 0);
		this.gridLayout.setComponentAlignment(this.labelTitoloSetupAssi, Alignment.MIDDLE_CENTER);
		this.labelPortata.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPortata, 0, 1);
		this.gridLayout.setComponentAlignment(this.labelPortata, Alignment.TOP_RIGHT);
		this.comboBoxPortata.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPortata.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxPortata, 1, 1);
		this.labelPressione.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPressione, 0, 2);
		this.gridLayout.setComponentAlignment(this.labelPressione, Alignment.TOP_RIGHT);
		this.comboBoxPressione.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPressione.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxPressione, 1, 2);
		this.labelPotenza.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPotenza, 0, 3);
		this.gridLayout.setComponentAlignment(this.labelPotenza, Alignment.TOP_RIGHT);
		this.comboBoxPotenza.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPotenza.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxPotenza, 1, 3);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 4, 1, 4);
		this.gridLayout.setRowExpandRatio(4, 1.0F);
		this.gridLayoutSetupUtilizzatore.setColumns(1);
		this.gridLayoutSetupUtilizzatore.setRows(8);
		this.labelTitoloSetupColoriCurve.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.labelTitoloSetupColoriCurve, 0, 0);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.labelTitoloSetupColoriCurve, Alignment.MIDDLE_CENTER);
		this.colorPickerPressioneTotale.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.colorPickerPressioneTotale, 0, 1);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.colorPickerPressioneTotale, Alignment.MIDDLE_CENTER);
		this.colorPickerPressioneStatica.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.colorPickerPressioneStatica, 0, 2);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.colorPickerPressioneStatica, Alignment.MIDDLE_CENTER);
		this.colorPickerPotenza.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.colorPickerPotenza, 0, 3);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.colorPickerPotenza, Alignment.MIDDLE_CENTER);
		this.colorPickerRendimento.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.colorPickerRendimento, 0, 4);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.colorPickerRendimento, Alignment.MIDDLE_CENTER);
		this.colorPickerCaratteristica.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.colorPickerCaratteristica, 0, 5);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.colorPickerCaratteristica, Alignment.MIDDLE_CENTER);
		this.colorPickerRumore.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore.addComponent(this.colorPickerRumore, 0, 6);
		this.gridLayoutSetupUtilizzatore.setComponentAlignment(this.colorPickerRumore, Alignment.MIDDLE_CENTER);
		this.gridLayoutSetupUtilizzatore.setColumnExpandRatio(0, 10.0F);
		final CustomComponent gridLayoutSetupUtilizzatore_vSpacer = new CustomComponent();
		gridLayoutSetupUtilizzatore_vSpacer.setSizeFull();
		this.gridLayoutSetupUtilizzatore.addComponent(gridLayoutSetupUtilizzatore_vSpacer, 0, 7, 0, 7);
		this.gridLayoutSetupUtilizzatore.setRowExpandRatio(7, 1.0F);
		this.buttonReset.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonReset);
		this.horizontalLayout2.setComponentAlignment(this.buttonReset, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.buttonReset, 10.0F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.gridLayoutSetupUtilizzatore.setWidth(100, Unit.PERCENTAGE);
		this.gridLayoutSetupUtilizzatore.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.gridLayoutSetupUtilizzatore);
		this.verticalLayout.setComponentAlignment(this.gridLayoutSetupUtilizzatore, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout.setHeight(-1, Unit.PIXELS);
		this.panel.setContent(this.verticalLayout);
		this.gridLayout2.setColumns(2);
		this.gridLayout2.setRows(8);
		this.labelTitoloSetupUM.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelTitoloSetupUM, 0, 0, 1, 0);
		this.gridLayout2.setComponentAlignment(this.labelTitoloSetupUM, Alignment.MIDDLE_CENTER);
		this.labelPortata2.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPortata2, 0, 1);
		this.gridLayout2.setComponentAlignment(this.labelPortata2, Alignment.TOP_RIGHT);
		this.comboBoxPortata2.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPortata2.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.comboBoxPortata2, 1, 1);
		this.labelPressione2.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPressione2, 0, 2);
		this.gridLayout2.setComponentAlignment(this.labelPressione2, Alignment.TOP_RIGHT);
		this.comboBoxPressione2.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPressione2.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.comboBoxPressione2, 1, 2);
		this.labelPotenza2.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPotenza2, 0, 3);
		this.gridLayout2.setComponentAlignment(this.labelPotenza2, Alignment.TOP_RIGHT);
		this.comboBoxPotenza2.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxPotenza2.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.comboBoxPotenza2, 1, 3);
		this.labelTemperatura.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelTemperatura, 0, 4);
		this.gridLayout2.setComponentAlignment(this.labelTemperatura, Alignment.TOP_RIGHT);
		this.comboBoxTemperatura.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxTemperatura.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.comboBoxTemperatura, 1, 4);
		this.labelAltezza.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelAltezza, 0, 5);
		this.gridLayout2.setComponentAlignment(this.labelAltezza, Alignment.TOP_RIGHT);
		this.comboBoxAltezza.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxAltezza.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.comboBoxAltezza, 1, 5);
		this.labelDiametro.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelDiametro, 0, 6);
		this.gridLayout2.setComponentAlignment(this.labelDiametro, Alignment.TOP_RIGHT);
		this.comboBoxDiametro.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxDiametro.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.comboBoxDiametro, 1, 6);
		this.gridLayout2.setColumnExpandRatio(0, 10.0F);
		this.gridLayout2.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout2_vSpacer = new CustomComponent();
		gridLayout2_vSpacer.setSizeFull();
		this.gridLayout2.addComponent(gridLayout2_vSpacer, 0, 7, 1, 7);
		this.gridLayout2.setRowExpandRatio(7, 1.0F);
		this.gridLayoutSetupUtilizzatore2.setColumns(2);
		this.gridLayoutSetupUtilizzatore2.setRows(5);
		this.labelTitoloSetupVarie.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelTitoloSetupVarie, 0, 0, 1, 0);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelTitoloSetupVarie, Alignment.MIDDLE_CENTER);
		this.labelPotenzaDisponibile.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelPotenzaDisponibile, 0, 1);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelPotenzaDisponibile, Alignment.TOP_RIGHT);
		this.checkBoxPotenzaDisponibile.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxPotenzaDisponibile.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.checkBoxPotenzaDisponibile, 1, 1);
		this.labelTratteggio.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelTratteggio, 0, 2);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelTratteggio, Alignment.TOP_RIGHT);
		this.checkBoxTratteggio.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxTratteggio.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.checkBoxTratteggio, 1, 2);
		this.labelCC.setSizeUndefined();
		this.gridLayoutSetupUtilizzatore2.addComponent(this.labelCC, 0, 3);
		this.gridLayoutSetupUtilizzatore2.setComponentAlignment(this.labelCC, Alignment.TOP_RIGHT);
		this.checkBoxCC.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxCC.setHeight(-1, Unit.PIXELS);
		this.gridLayoutSetupUtilizzatore2.addComponent(this.checkBoxCC, 1, 3);
		this.gridLayoutSetupUtilizzatore2.setColumnExpandRatio(0, 10.0F);
		this.gridLayoutSetupUtilizzatore2.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayoutSetupUtilizzatore2_vSpacer = new CustomComponent();
		gridLayoutSetupUtilizzatore2_vSpacer.setSizeFull();
		this.gridLayoutSetupUtilizzatore2.addComponent(gridLayoutSetupUtilizzatore2_vSpacer, 0, 4, 1, 4);
		this.gridLayoutSetupUtilizzatore2.setRowExpandRatio(4, 1.0F);
		this.gridLayoutSetupStep.setColumns(3);
		this.gridLayoutSetupStep.setRows(5);
		this.labelTitoloSetupStep.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.labelTitoloSetupStep, 0, 0, 2, 0);
		this.gridLayoutSetupStep.setComponentAlignment(this.labelTitoloSetupStep, Alignment.MIDDLE_CENTER);
		this.labelStepPortata.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.labelStepPortata, 0, 1);
		this.gridLayoutSetupStep.setComponentAlignment(this.labelStepPortata, Alignment.TOP_RIGHT);
		this.textFieldStepPortata.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.textFieldStepPortata, 1, 1);
		this.label.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.label, 2, 1);
		this.gridLayoutSetupStep.setComponentAlignment(this.label, Alignment.MIDDLE_LEFT);
		this.labelStepRpm.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.labelStepRpm, 0, 2);
		this.gridLayoutSetupStep.setComponentAlignment(this.labelStepRpm, Alignment.TOP_RIGHT);
		this.textFieldStepRpm.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.textFieldStepRpm, 1, 2);
		this.label2.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.label2, 2, 2);
		this.gridLayoutSetupStep.setComponentAlignment(this.label2, Alignment.MIDDLE_LEFT);
		this.labelStepPressioneSonora.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.labelStepPressioneSonora, 0, 3);
		this.gridLayoutSetupStep.setComponentAlignment(this.labelStepPressioneSonora, Alignment.TOP_RIGHT);
		this.textFieldStepPressioneSonora.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.textFieldStepPressioneSonora, 1, 3);
		this.label3.setSizeUndefined();
		this.gridLayoutSetupStep.addComponent(this.label3, 2, 3);
		this.gridLayoutSetupStep.setColumnExpandRatio(0, 10.0F);
		this.gridLayoutSetupStep.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayoutSetupStep_vSpacer = new CustomComponent();
		gridLayoutSetupStep_vSpacer.setSizeFull();
		this.gridLayoutSetupStep.addComponent(gridLayoutSetupStep_vSpacer, 0, 4, 2, 4);
		this.gridLayoutSetupStep.setRowExpandRatio(4, 1.0F);
		this.gridLayout2.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayout2);
		this.verticalLayout2.setComponentAlignment(this.gridLayout2, Alignment.MIDDLE_CENTER);
		this.gridLayoutSetupUtilizzatore2.setWidth(100, Unit.PERCENTAGE);
		this.gridLayoutSetupUtilizzatore2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayoutSetupUtilizzatore2);
		this.verticalLayout2.setComponentAlignment(this.gridLayoutSetupUtilizzatore2, Alignment.MIDDLE_CENTER);
		this.gridLayoutSetupStep.setWidth(100, Unit.PERCENTAGE);
		this.gridLayoutSetupStep.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.gridLayoutSetupStep);
		this.verticalLayout2.setComponentAlignment(this.gridLayoutSetupStep, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout2.setHeight(-1, Unit.PIXELS);
		this.panel2.setContent(this.verticalLayout2);
		this.panel.setWidth(100, Unit.PERCENTAGE);
		this.panel.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.panel);
		this.horizontalLayout.setExpandRatio(this.panel, 10.0F);
		this.panel2.setWidth(100, Unit.PERCENTAGE);
		this.panel2.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.panel2);
		this.horizontalLayout.setExpandRatio(this.panel2, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	
		this.buttonReset.addClickListener(event -> this.buttonReset_buttonClick(event));
	} // </generated-code>
	// <generated-code name="variables">
	private XdevColorPicker colorPickerPressioneTotale, colorPickerPressioneStatica, colorPickerPotenza,
			colorPickerRendimento, colorPickerCaratteristica, colorPickerRumore;
	private TextFieldInteger textFieldStepRpm;
	private XdevLabel labelTitoloSetupAssi, labelPortata, labelPressione, labelPotenza, labelTitoloSetupColoriCurve,
			labelTitoloSetupUM, labelPortata2, labelPressione2, labelPotenza2, labelTemperatura, labelAltezza,
			labelDiametro, labelTitoloSetupVarie, labelPotenzaDisponibile, labelTratteggio, labelCC, labelTitoloSetupStep,
			labelStepPortata, label, labelStepRpm, label2, labelStepPressioneSonora, label3;
	private XdevButton buttonReset;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2;
	private TextFieldDouble textFieldStepPortata, textFieldStepPressioneSonora;
	private XdevPanel panel, panel2;
	private XdevCheckBox checkBoxPotenzaDisponibile, checkBoxTratteggio, checkBoxCC;
	private XdevGridLayout gridLayout, gridLayoutSetupUtilizzatore, gridLayout2, gridLayoutSetupUtilizzatore2,
			gridLayoutSetupStep;
	private XdevVerticalLayout verticalLayout, verticalLayout2;
	private XdevComboBox<CustomComponent> comboBoxPortata, comboBoxPressione, comboBoxPotenza, comboBoxPortata2,
			comboBoxPressione2, comboBoxPotenza2, comboBoxTemperatura, comboBoxAltezza, comboBoxDiametro;
	// </generated-code>


}
