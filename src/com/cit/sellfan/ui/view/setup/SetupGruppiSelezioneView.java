package com.cit.sellfan.ui.view.setup;

import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.GruppoSelezioneSerie;
import com.vaadin.ui.Notification;

public class SetupGruppiSelezioneView extends XdevView {
	private VariabiliGlobali l_VG;
	private boolean editEnabled;
	private final Container containerGruppo0 = new IndexedContainer();
	private final Container containerGruppo1 = new IndexedContainer();
	private final Container containerGruppo2 = new IndexedContainer();
	private final Container containerGruppo3 = new IndexedContainer();

	/**
	 * 
	 */
	public SetupGruppiSelezioneView() {
		super();
		this.initUI();
		this.containerGruppo0.removeAllItems();
		this.containerGruppo0.addContainerProperty("Selected", XdevCheckBox.class, true);
		this.containerGruppo0.addContainerProperty("Serie", String.class, "");
		this.containerGruppo0.addContainerProperty("Description", String.class, "");
		this.tableSerieGruppo0.setContainerDataSource(this.containerGruppo0);
		this.containerGruppo1.removeAllItems();
		this.containerGruppo1.addContainerProperty("Selected", XdevCheckBox.class, true);
		this.containerGruppo1.addContainerProperty("Serie", String.class, "");
		this.containerGruppo1.addContainerProperty("Description", String.class, "");
		this.tableSerieGruppo1.setContainerDataSource(this.containerGruppo1);
		this.containerGruppo2.removeAllItems();
		this.containerGruppo2.addContainerProperty("Selected", XdevCheckBox.class, true);
		this.containerGruppo2.addContainerProperty("Serie", String.class, "");
		this.containerGruppo2.addContainerProperty("Description", String.class, "");
		this.tableSerieGruppo2.setContainerDataSource(this.containerGruppo2);
		this.containerGruppo3.removeAllItems();
		this.containerGruppo3.addContainerProperty("Selected", XdevCheckBox.class, true);
		this.containerGruppo3.addContainerProperty("Serie", String.class, "");
		this.containerGruppo3.addContainerProperty("Description", String.class, "");
		this.tableSerieGruppo3.setContainerDataSource(this.containerGruppo3);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void setWidth(final int width) {
		//horizontalLayout2.setWidth(width, Unit.PIXELS);
		//horizontalLayout2.setHeight(height, Unit.PIXELS);
	}
	
	public void Nazionalizza() {
		this.labelGruppo0.setValue(this.l_VG.utilityTraduzioni.TraduciStringaOutput("Descrizione Gruppo"));
		this.labelGruppo1.setValue(this.l_VG.utilityTraduzioni.TraduciStringaOutput("Descrizione Gruppo"));
		this.labelGruppo2.setValue(this.l_VG.utilityTraduzioni.TraduciStringaOutput("Descrizione Gruppo"));
		this.labelGruppo3.setValue(this.l_VG.utilityTraduzioni.TraduciStringaOutput("Descrizione Gruppo"));
		this.tableSerieGruppo0.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableSerieGruppo0.setColumnHeader("Serie", this.l_VG.utilityTraduzioni.TraduciStringa("Serie"));
		this.tableSerieGruppo0.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableSerieGruppo1.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableSerieGruppo1.setColumnHeader("Serie", this.l_VG.utilityTraduzioni.TraduciStringa("Serie"));
		this.tableSerieGruppo1.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableSerieGruppo2.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableSerieGruppo2.setColumnHeader("Serie", this.l_VG.utilityTraduzioni.TraduciStringa("Serie"));
		this.tableSerieGruppo2.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableSerieGruppo3.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableSerieGruppo3.setColumnHeader("Serie", this.l_VG.utilityTraduzioni.TraduciStringa("Serie"));
		this.tableSerieGruppo3.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
	}

	public void initSetup() {
		buildTabellaSerie(this.tableSerieGruppo0, this.l_VG.ElencoGruppiSelezioneSerie[0]);
		buildTabellaSerie(this.tableSerieGruppo1, this.l_VG.ElencoGruppiSelezioneSerie[1]);
		buildTabellaSerie(this.tableSerieGruppo2, this.l_VG.ElencoGruppiSelezioneSerie[2]);
		buildTabellaSerie(this.tableSerieGruppo3, this.l_VG.ElencoGruppiSelezioneSerie[3]);
		if (this.l_VG.ElencoGruppiSelezioneSerie[0].descrizione.equals("-")) {
			this.textFieldGruppo0.setValue("");
		} else {
			this.textFieldGruppo0.setValue(this.l_VG.ElencoGruppiSelezioneSerie[0].descrizione);
		}
		if (this.l_VG.ElencoGruppiSelezioneSerie[1].descrizione.equals("-")) {
			this.textFieldGruppo1.setValue("");
		} else {
			this.textFieldGruppo1.setValue(this.l_VG.ElencoGruppiSelezioneSerie[1].descrizione);
		}
		if (this.l_VG.ElencoGruppiSelezioneSerie[2].descrizione.equals("-")) {
			this.textFieldGruppo2.setValue("");
		} else {
			this.textFieldGruppo2.setValue(this.l_VG.ElencoGruppiSelezioneSerie[2].descrizione);
		}
		if (this.l_VG.ElencoGruppiSelezioneSerie[3].descrizione.equals("-")) {
			this.textFieldGruppo3.setValue("");
		} else {
			this.textFieldGruppo3.setValue(this.l_VG.ElencoGruppiSelezioneSerie[3].descrizione);
		}
	}

	public void azioneReset() {
		for (int i=0 ; i<4 ; i++) {
			this.l_VG.ElencoGruppiSelezioneSerie[i].descrizione = "";
			this.l_VG.ElencoGruppiSelezioneSerie[i].elencoSerie = null;
		}
		initSetup();
	}
	
	public void azioneOK() {
		String descrizione = this.textFieldGruppo0.getValue().trim();
		if (!descrizione.equals("") && !descrizione.equals("-")) {
			builGruppo(this.containerGruppo0, this.l_VG.ElencoGruppiSelezioneSerie[0]);
			this.l_VG.ElencoGruppiSelezioneSerie[0].descrizione = descrizione;
		} else {
			this.l_VG.ElencoGruppiSelezioneSerie[0].elencoSerie = null;
			this.l_VG.ElencoGruppiSelezioneSerie[0].descrizione = "-";
		}
		descrizione = this.textFieldGruppo1.getValue().trim();
		if (!descrizione.equals("") && !descrizione.equals("-")) {
			builGruppo(this.containerGruppo1, this.l_VG.ElencoGruppiSelezioneSerie[1]);
			this.l_VG.ElencoGruppiSelezioneSerie[1].descrizione = descrizione;
		} else {
			this.l_VG.ElencoGruppiSelezioneSerie[1].elencoSerie = null;
			this.l_VG.ElencoGruppiSelezioneSerie[1].descrizione = "-";
		}
		descrizione = this.textFieldGruppo2.getValue().trim();
		if (!descrizione.equals("") && !descrizione.equals("-")) {
			builGruppo(this.containerGruppo2, this.l_VG.ElencoGruppiSelezioneSerie[2]);
			this.l_VG.ElencoGruppiSelezioneSerie[2].descrizione = descrizione;
		} else {
			this.l_VG.ElencoGruppiSelezioneSerie[2].elencoSerie = null;
			this.l_VG.ElencoGruppiSelezioneSerie[2].descrizione = "-";
		}
		descrizione = this.textFieldGruppo3.getValue().trim();
		if (!descrizione.equals("") && !descrizione.equals("-")) {
			builGruppo(this.containerGruppo3, this.l_VG.ElencoGruppiSelezioneSerie[3]);
			this.l_VG.ElencoGruppiSelezioneSerie[3].descrizione = descrizione;
		} else {
			this.l_VG.ElencoGruppiSelezioneSerie[3].elencoSerie = null;
			this.l_VG.ElencoGruppiSelezioneSerie[3].descrizione = "-";
		}
/*
		builGruppo(containerGruppo1, l_VG.ElencoGruppiSelezioneSerie[1]);
		builGruppo(containerGruppo2, l_VG.ElencoGruppiSelezioneSerie[2]);
		builGruppo(containerGruppo3, l_VG.ElencoGruppiSelezioneSerie[3]);
		l_VG.ElencoGruppiSelezioneSerie[1].descrizione = textFieldGruppo1.getValue();
		l_VG.ElencoGruppiSelezioneSerie[2].descrizione = textFieldGruppo2.getValue();
		l_VG.ElencoGruppiSelezioneSerie[3].descrizione = textFieldGruppo3.getValue();
*/
		for (int i=0 ; i<4 ; i++) {
			this.l_VG.dbConfig.storeGruppoSelezioneSerie(this.l_VG.ElencoGruppiSelezioneSerie[i], i);
		}
	}
	
	private void builGruppo(final Container containerSerie, final GruppoSelezioneSerie gruppoSelezioneSerie) {
		final Collection<?> Ventilatoriids = containerSerie.getItemIds();
		int nSerie = 0;
		setEditEnabled(false);
//		l_VG.ElencoSerieDisponibili.clear();
		for(final Object id : Ventilatoriids) {
			final Item item = containerSerie.getItem(id);
			final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
			if (cb.getValue()) {
				nSerie++;
/*
				String sr = (String)item.getItemProperty("Serie").getValue();
				for (int i=0 ; i<l_VG.ElencoSerie.size() ; i++) {
					if (l_VG.ElencoSerie.get(i).SerieTrans.equals(sr)) {
						l_VG.ElencoSerieDisponibili.add(cb.getValue());
						l_VG.ElencoSerie.get(i).UtilizzabileRicerca = cb.getValue();
					}
				}
*/
			}
		}
		if (nSerie <= 0) {
			gruppoSelezioneSerie.elencoSerie = null;
			return;
		}
		gruppoSelezioneSerie.elencoSerie = new String[nSerie];
		nSerie = 0;
		for(final Object id : Ventilatoriids) {
			final Item item = containerSerie.getItem(id);
			final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
			final String sr = (String)item.getItemProperty("Serie").getValue();
			if (cb.getValue()) {
				gruppoSelezioneSerie.elencoSerie[nSerie++] = sr;
			}
		}
	}
	
	private void buildTabellaSerie(@SuppressWarnings("rawtypes") final XdevTable tableSerie, final GruppoSelezioneSerie gruppoSelezioneSerie) {
		setEditEnabled(false);
		try {
			tableSerie.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			Notification.show("Problema BuildTabellaSerie");
		}
		for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
			if (!this.l_VG.ElencoSerieDisponibili.get(i)) {
				continue;
			}
			final String itemID = Integer.toString(i);
			final Object obj[] = new Object[3];
			final XdevCheckBox cb = new XdevCheckBox();
/*
			cb.setValue(l_VG.ElencoSerieDisponibili.get(i));
			cb.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
						if (!editEnabled) return;
						editEnabled = false;
						editEnabled = true;
					}
		    });
*/
			cb.setValue(false);
            if (gruppoSelezioneSerie.elencoSerie != null) {
                for (int j=0 ; j<gruppoSelezioneSerie.elencoSerie.length ; j++) {
                    if (this.l_VG.ElencoSerie.get(i).SerieTrans.equals(gruppoSelezioneSerie.elencoSerie[j])) {
                    	cb.setValue(true);
                        break;
                    }
                }
            }
			obj[0] = cb;
			obj[1] = this.l_VG.ElencoSerie.get(i).SerieTrans;
			obj[2] = this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.ElencoSerie.get(i).DescrizioneTrans);
			tableSerie.addItem(obj, itemID);
		}
		setEditEnabled(true);
	}
	/*

	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout3 = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.panelSerie = new XdevPanel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.tableSerieGruppo0 = new XdevTable<>();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.labelGruppo0 = new XdevLabel();
		this.textFieldGruppo0 = new XdevTextField();
		this.panelSerie2 = new XdevPanel();
		this.verticalLayout = new XdevVerticalLayout();
		this.tableSerieGruppo1 = new XdevTable<>();
		this.horizontalLayout5 = new XdevHorizontalLayout();
		this.labelGruppo1 = new XdevLabel();
		this.textFieldGruppo1 = new XdevTextField();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.panelSerie3 = new XdevPanel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.tableSerieGruppo2 = new XdevTable<>();
		this.horizontalLayout6 = new XdevHorizontalLayout();
		this.labelGruppo2 = new XdevLabel();
		this.textFieldGruppo2 = new XdevTextField();
		this.panelSerie4 = new XdevPanel();
		this.verticalLayout5 = new XdevVerticalLayout();
		this.tableSerieGruppo3 = new XdevTable<>();
		this.horizontalLayout7 = new XdevHorizontalLayout();
		this.labelGruppo3 = new XdevLabel();
		this.textFieldGruppo3 = new XdevTextField();
	
		this.panelSerie.setCaption("");
		this.panelSerie.setTabIndex(0);
		this.verticalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.tableSerieGruppo0.setStyleName("mystriped");
		this.labelGruppo0.setValue("Label");
		this.panelSerie2.setCaption("");
		this.panelSerie2.setTabIndex(0);
		this.verticalLayout.setMargin(new MarginInfo(false, true, false, true));
		this.tableSerieGruppo1.setStyleName("mystriped");
		this.labelGruppo1.setValue("Label");
		this.panelSerie3.setCaption("");
		this.panelSerie3.setTabIndex(0);
		this.verticalLayout4.setMargin(new MarginInfo(false, true, false, true));
		this.tableSerieGruppo2.setStyleName("mystriped");
		this.labelGruppo2.setValue("Label");
		this.panelSerie4.setCaption("");
		this.panelSerie4.setTabIndex(0);
		this.verticalLayout5.setMargin(new MarginInfo(false, true, false, true));
		this.tableSerieGruppo3.setStyleName("mystriped");
		this.labelGruppo3.setValue("Label");
	
		this.labelGruppo0.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.labelGruppo0);
		this.horizontalLayout4.setComponentAlignment(this.labelGruppo0, Alignment.MIDDLE_CENTER);
		this.textFieldGruppo0.setWidth(100, Unit.PERCENTAGE);
		this.textFieldGruppo0.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout4.addComponent(this.textFieldGruppo0);
		this.horizontalLayout4.setComponentAlignment(this.textFieldGruppo0, Alignment.MIDDLE_CENTER);
		this.horizontalLayout4.setExpandRatio(this.textFieldGruppo0, 10.0F);
		this.tableSerieGruppo0.setSizeFull();
		this.verticalLayout2.addComponent(this.tableSerieGruppo0);
		this.verticalLayout2.setComponentAlignment(this.tableSerieGruppo0, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.tableSerieGruppo0, 10.0F);
		this.horizontalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.horizontalLayout4);
		this.verticalLayout2.setComponentAlignment(this.horizontalLayout4, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setSizeFull();
		this.panelSerie.setContent(this.verticalLayout2);
		this.labelGruppo1.setSizeUndefined();
		this.horizontalLayout5.addComponent(this.labelGruppo1);
		this.horizontalLayout5.setComponentAlignment(this.labelGruppo1, Alignment.MIDDLE_CENTER);
		this.textFieldGruppo1.setWidth(100, Unit.PERCENTAGE);
		this.textFieldGruppo1.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout5.addComponent(this.textFieldGruppo1);
		this.horizontalLayout5.setComponentAlignment(this.textFieldGruppo1, Alignment.MIDDLE_CENTER);
		this.horizontalLayout5.setExpandRatio(this.textFieldGruppo1, 10.0F);
		this.tableSerieGruppo1.setSizeFull();
		this.verticalLayout.addComponent(this.tableSerieGruppo1);
		this.verticalLayout.setComponentAlignment(this.tableSerieGruppo1, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableSerieGruppo1, 10.0F);
		this.horizontalLayout5.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout5);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout5, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setSizeFull();
		this.panelSerie2.setContent(this.verticalLayout);
		this.panelSerie.setSizeFull();
		this.horizontalLayout.addComponent(this.panelSerie);
		this.horizontalLayout.setExpandRatio(this.panelSerie, 10.0F);
		this.panelSerie2.setSizeFull();
		this.horizontalLayout.addComponent(this.panelSerie2);
		this.horizontalLayout.setExpandRatio(this.panelSerie2, 10.0F);
		this.labelGruppo2.setSizeUndefined();
		this.horizontalLayout6.addComponent(this.labelGruppo2);
		this.horizontalLayout6.setComponentAlignment(this.labelGruppo2, Alignment.MIDDLE_CENTER);
		this.textFieldGruppo2.setWidth(100, Unit.PERCENTAGE);
		this.textFieldGruppo2.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout6.addComponent(this.textFieldGruppo2);
		this.horizontalLayout6.setComponentAlignment(this.textFieldGruppo2, Alignment.MIDDLE_CENTER);
		this.horizontalLayout6.setExpandRatio(this.textFieldGruppo2, 10.0F);
		this.tableSerieGruppo2.setSizeFull();
		this.verticalLayout4.addComponent(this.tableSerieGruppo2);
		this.verticalLayout4.setComponentAlignment(this.tableSerieGruppo2, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.tableSerieGruppo2, 10.0F);
		this.horizontalLayout6.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout6.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.horizontalLayout6);
		this.verticalLayout4.setComponentAlignment(this.horizontalLayout6, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setSizeFull();
		this.panelSerie3.setContent(this.verticalLayout4);
		this.labelGruppo3.setSizeUndefined();
		this.horizontalLayout7.addComponent(this.labelGruppo3);
		this.horizontalLayout7.setComponentAlignment(this.labelGruppo3, Alignment.MIDDLE_CENTER);
		this.textFieldGruppo3.setWidth(100, Unit.PERCENTAGE);
		this.textFieldGruppo3.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout7.addComponent(this.textFieldGruppo3);
		this.horizontalLayout7.setComponentAlignment(this.textFieldGruppo3, Alignment.MIDDLE_CENTER);
		this.horizontalLayout7.setExpandRatio(this.textFieldGruppo3, 10.0F);
		this.tableSerieGruppo3.setSizeFull();
		this.verticalLayout5.addComponent(this.tableSerieGruppo3);
		this.verticalLayout5.setComponentAlignment(this.tableSerieGruppo3, Alignment.MIDDLE_CENTER);
		this.verticalLayout5.setExpandRatio(this.tableSerieGruppo3, 10.0F);
		this.horizontalLayout7.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout5.addComponent(this.horizontalLayout7);
		this.verticalLayout5.setComponentAlignment(this.horizontalLayout7, Alignment.MIDDLE_CENTER);
		this.verticalLayout5.setSizeFull();
		this.panelSerie4.setContent(this.verticalLayout5);
		this.panelSerie3.setSizeFull();
		this.horizontalLayout2.addComponent(this.panelSerie3);
		this.horizontalLayout2.setExpandRatio(this.panelSerie3, 10.0F);
		this.panelSerie4.setSizeFull();
		this.horizontalLayout2.addComponent(this.panelSerie4);
		this.horizontalLayout2.setExpandRatio(this.panelSerie4, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.verticalLayout3.addComponent(this.horizontalLayout);
		this.verticalLayout3.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout3.setExpandRatio(this.horizontalLayout, 10.0F);
		this.horizontalLayout2.setSizeFull();
		this.verticalLayout3.addComponent(this.horizontalLayout2);
		this.verticalLayout3.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout3.setExpandRatio(this.horizontalLayout2, 10.0F);
		this.verticalLayout3.setSizeFull();
		this.setContent(this.verticalLayout3);
		this.setSizeFull();
	} // </generated-code>

	public boolean isEditEnabled() {
		return this.editEnabled;
	}

	public void setEditEnabled(final boolean editEnabled) {
		this.editEnabled = editEnabled;
	}

	// <generated-code name="variables">
	private XdevLabel labelGruppo0, labelGruppo1, labelGruppo2, labelGruppo3;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout4, horizontalLayout5, horizontalLayout2,
			horizontalLayout6, horizontalLayout7;
	private XdevTable<?> tableSerieGruppo0, tableSerieGruppo1, tableSerieGruppo2, tableSerieGruppo3;
	private XdevPanel panelSerie, panelSerie2, panelSerie3, panelSerie4;
	private XdevTextField textFieldGruppo0, textFieldGruppo1, textFieldGruppo2, textFieldGruppo3;
	private XdevVerticalLayout verticalLayout3, verticalLayout2, verticalLayout, verticalLayout4, verticalLayout5;
	// </generated-code>

}
