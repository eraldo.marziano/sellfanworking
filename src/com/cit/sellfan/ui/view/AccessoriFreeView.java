package com.cit.sellfan.ui.view;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.Costanti;
import cit.sellfan.classi.Accessorio;

public class AccessoriFreeView extends XdevView {
	private VariabiliGlobali l_VG;
	private Container containerAccessoriFree = new IndexedContainer();

	/**
	 * 
	 */
	public AccessoriFreeView() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		initContainerAccessoriFree();
		
	}
	
	private void initContainerAccessoriFree() {
		this.containerAccessoriFree = new IndexedContainer();
		this.containerAccessoriFree.removeAllItems();
		this.containerAccessoriFree.addContainerProperty("Selected", CheckBox.class, false);
		this.containerAccessoriFree.addContainerProperty("Code", TextField.class, "");
		this.containerAccessoriFree.addContainerProperty("Description", TextField.class, "");
		this.containerAccessoriFree.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerAccessoriFree.addContainerProperty("Price", TextFieldDouble.class, 0.0);
		this.tableAccessoriFree.setContainerDataSource(this.containerAccessoriFree);
		this.tableAccessoriFree.setColumnWidth("PriceVal", 0);
		this.tableAccessoriFree.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableAccessoriFree.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableAccessoriFree.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableAccessoriFree.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
		this.tableAccessoriFree.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo") + " " + Costanti.EURO);
	}
	
	public void Nazionalizza() {
		this.tableAccessoriFree.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableAccessoriFree.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableAccessoriFree.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableAccessoriFree.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableAccessoriFree.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Accessori"));
	}
	
	public void setVentilatoreCurrent() {
		initContainerAccessoriFree();
		buildTableAccessori();
	}
	
	private void buildTableAccessori() {
/*
		try {
			table.removeAllItems();//resetta anche le selection
		} catch (Exception e) {
			
		}
*/
		this.tableAccessoriFree.setVisible(true);
		this.tableAccessoriFree.setEnabled(false);
		this.tableAccessoriFree.setCaption("");
		for (int i=0 ; i<this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.size() ; i++) {
			final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(i);
			final Object obj[] = new Object[5];
			final String itemID = Integer.toString(i);
			final CheckBox cb = new CheckBox();
			cb.setValue(l_a.Selezionato);
			cb.setData(l_a.Codice);
			cb.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						if (!AccessoriFreeView.this.tableAccessoriFree.isEnabled()) {
							return;
						}
						final int index = Integer.parseInt(itemID);
						final Accessorio l_aloc = AccessoriFreeView.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(index);
						final boolean value = cb.getValue().booleanValue();
				        AccessoriFreeView.this.l_VG.ventilatoreCambiato = true;
				        if (AccessoriFreeView.this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
				        	AccessoriFreeView.this.l_VG.preventivoCurrent.PreventivoCambiato = true;
				        }
				        l_aloc.Selezionato = value;
				        AccessoriFreeView.this.l_VG.MainView.refreshAccessoriPreventivi();
					}
		    });
			obj[0] = cb;
			final TextField code = new TextField();
			code.setValue(l_a.Codice);
			code.setData(l_a.Codice);
			code.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						if (!AccessoriFreeView.this.tableAccessoriFree.isEnabled()) {
							return;
						}
						final int index = Integer.parseInt(itemID);
						final Accessorio l_aloc = AccessoriFreeView.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(index);
						final String value = code.getValue();
				        AccessoriFreeView.this.l_VG.ventilatoreCambiato = true;
				        l_aloc.Codice = value;
				        AccessoriFreeView.this.l_VG.MainView.refreshAccessoriPreventivi();
					}
		    });
			obj[1] = code;
			final TextField desc = new TextField();
			desc.setValue(l_a.Descrizione);
			desc.setData(l_a.Codice);
			desc.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						if (!AccessoriFreeView.this.tableAccessoriFree.isEnabled()) {
							return;
						}
						final int index = Integer.parseInt(itemID);
						final Accessorio l_aloc = AccessoriFreeView.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(index);
						final String value = desc.getValue();
				        AccessoriFreeView.this.l_VG.ventilatoreCambiato = true;
				        l_aloc.Descrizione = value;
				        AccessoriFreeView.this.l_VG.MainView.refreshAccessoriPreventivi();
					}
		    });
			obj[2] = desc;
			//obj[2] = l_a.Descrizione + l_VG.gestioneAccessori.getAccessorioValoreParametro(l_a, "cod");
			final TextFieldInteger tf = new TextFieldInteger();
			tf.setValue(Integer.toString(l_a.qta));
			tf.setData(l_a.Codice);
			tf.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						if (!AccessoriFreeView.this.tableAccessoriFree.isEnabled()) {
							return;
						}
						final int index = Integer.parseInt(itemID);
						final Accessorio l_aloc = AccessoriFreeView.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(index);
						final int value = tf.getIntegerValue();
				        AccessoriFreeView.this.l_VG.ventilatoreCambiato = true;
				        l_aloc.qta = value;
				        AccessoriFreeView.this.l_VG.MainView.refreshAccessoriPreventivi();
					}
			});
			obj[3] = tf;
			
			final TextFieldDouble tdf = new TextFieldDouble();
			tdf.setMaximumFractionDigits(2);
			tdf.setData(itemID);
			tdf.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						if (!AccessoriFreeView.this.tableAccessoriFree.isEnabled()) {
							return;
						}
						final Object itemId = tdf.getData();
						final Accessorio l_aloc = AccessoriFreeView.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(Integer.parseInt(itemId.toString()));
						AccessoriFreeView.this.l_VG.ventilatoreCambiato = true;
						if (tdf.getDoubleValue() > 0.) {
							l_aloc.Prezzo = tdf.getDoubleValue();
						} else if (tdf.getValue().equals("")) {
							l_aloc.Prezzo = 0.;
						} else {
							l_aloc.Prezzo = -1.;
						}
				        AccessoriFreeView.this.l_VG.MainView.refreshAccessoriPreventivi();
					}
			});
			if (l_a.Prezzo > 0.) {
				tdf.setValue(l_a.Prezzo);
			}
			tdf.setMaximumFractionDigits(2);
			obj[4] = tdf;
			this.tableAccessoriFree.addItem(obj, itemID);
		}
		this.tableAccessoriFree.setEnabled(true);
	}
	
	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.tableAccessoriFree = new XdevTable<>();
	
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.tableAccessoriFree.setCaption("Accessori");
		this.tableAccessoriFree.setStyleName("small mystriped");
	
		this.tableAccessoriFree.setSizeFull();
		this.verticalLayout.addComponent(this.tableAccessoriFree);
		this.verticalLayout.setComponentAlignment(this.tableAccessoriFree, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableAccessoriFree, 100.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	} // </generated-code>

	// <generated-code name="variables">
	private XdevTable<CustomComponent> tableAccessoriFree;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>


}
