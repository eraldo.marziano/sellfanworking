package com.cit.sellfan.ui.view;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.pannelli.pannelloRicercaDatiSelezione;
import com.cit.sellfan.ui.pannelli.pannelloRicercaFiltriSelezione;
import com.cit.sellfan.ui.template.StreamFileResource;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevAccordion;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.Costanti;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.personalizzazioni.cliente04.Cliente04Rumore;
import cit.utility.NumeriRomani;
import com.cit.sellfan.ui.pannelli.pannelloRicerca;

public class SearchView extends XdevView {
	public boolean isPannelloInizializzato = false;
	public pannelloRicercaDatiSelezione pannelloDatiSelezione = new pannelloRicercaDatiSelezione();
        public pannelloRicerca pannelloRicerca = new pannelloRicerca();
	public pannelloRicercaFiltriSelezione pannelloFiltriSelezione = new pannelloRicercaFiltriSelezione();
	private VariabiliGlobali l_VG;
	private String currentItemID = null;
	private boolean hidePrezzo = false;
	private final Container containerRisultati = new IndexedContainer();
	private Cliente04Rumore rumore = null;
        private VentilatoreFisica ventilatoreFisica;
	
	/**
	 * 
	 */
	public SearchView() {
		super();
		this.initUI();
		
		this.containerRisultati.removeAllItems();
		this.containerRisultati.addContainerProperty("Prezzo", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("Modello", String.class, "");
		this.containerRisultati.addContainerProperty("Esecuzione", String.class, "");
		this.containerRisultati.addContainerProperty("Motore", String.class, "");
		this.containerRisultati.addContainerProperty("Q", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("Pt", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("Ps", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("W", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("rpm", Integer.class, 0);
		this.containerRisultati.addContainerProperty("eta", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("LW", Double.class, 0.0);
		this.containerRisultati.addContainerProperty("L", Double.class, 0.0);
		this.tableRisultati.setContainerDataSource(this.containerRisultati);
		
		this.pannelloDatiSelezione.setSizeFull();
                this.horizontalLayout.replaceComponent(this.panel2, this.pannelloRicerca);
//		this.accordion.replaceComponent(this.panel2, this.pannelloRicerca);
//		this.pannelloFiltriSelezione.setSizeFull();
//
//		this.accordion.replaceComponent(this.panel4, this.pannelloFiltriSelezione);
	}

	private void setConverterForTable() {
		final myStringToDoubleConverter converter = new myStringToDoubleConverter();
		this.tableRisultati.setConverter("Prezzo", converter);
		this.tableRisultati.setConverter("Q", converter);
		this.tableRisultati.setConverter("Pt", converter);
		this.tableRisultati.setConverter("Ps", converter);
		this.tableRisultati.setConverter("W", converter);
		this.tableRisultati.setConverter("rpm", converter);
		this.tableRisultati.setConverter("eta", converter);
		this.tableRisultati.setConverter("LW", converter);
		this.tableRisultati.setConverter("L", converter);
	}
	
	private class myStringToDoubleConverter extends StringToDoubleConverter {
		public myStringToDoubleConverter() {
			super();
		}
		
		@Override
		public java.text.NumberFormat getFormat(final Locale locale) {
			return SearchView.this.l_VG.fmtNdTable;
		}
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoView[this.l_VG.SearchViewIndex] = this;
            this.pannelloRicerca.setVariabiliGlobali(this.l_VG);
		this.pannelloDatiSelezione.setVariabiliGlobali(this.l_VG);
		this.pannelloFiltriSelezione.setVariabiliGlobali(this.l_VG);
		this.pannelloDatiSelezione.buildComboEsecuzioni();
                //this.tableRisultati.setColumnWidth("Prezzo", 0);
                this.rumore = new Cliente04Rumore(this.l_VG.utilityCliente.ConnCliente, this.l_VG.dbTableQualifier, "mz");
		reset();
		if (! (this.l_VG.currentUserSaaS.idSottoClienteIndex == 1 || this.l_VG.currentUserSaaS.idSottoClienteIndex == 3 || l_VG.isDonaldson() ))
		{
			this.hidePrezzo = true;
			this.tableRisultati.setColumnWidth("Prezzo", 0);
		}
		setConverterForTable();
	}
	
	public void resetView() {
		try {
			this.pannelloFiltriSelezione.resetPannello();
			this.pannelloDatiSelezione.resetPannello();
		} catch (final Exception e) {
			//Notification.show(e.toString());
		}
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		return this.isPannelloInizializzato;
	}
	
	public void init() {
		if (!this.l_VG.utilityCliente.isTabVisible("Ricerca")) {
			return;
		}
                this.pannelloRicerca.update();
		this.pannelloDatiSelezione.init(true);
		this.pannelloFiltriSelezione.init();
		this.pannelloDatiSelezione.buildComboEsecuzioni();
		this.isPannelloInizializzato = true;
	}
        
	public void togglePreventivoButton(boolean value) {
            buttonAggiungiPreventivo.setEnabled(value);
        }
	public void Nazionalizza() {
		this.buttonAggiungiPreventivo.setVisible(this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
                this.pannelloRicerca.Nazionalizza();
		this.pannelloDatiSelezione.Nazionalizza();
		this.pannelloFiltriSelezione.Nazionalizza();
		
		this.panelRisultati.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Risultati selezione"));
		if (this.hidePrezzo) {
			this.tableRisultati.setColumnHeader("Prezzo", "");
		} else {
			this.tableRisultati.setColumnHeader("Prezzo", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo"));
		}
		this.tableRisultati.setColumnHeader("Modello", this.l_VG.utilityTraduzioni.TraduciStringa("Modello"));
		this.tableRisultati.setColumnHeader("Esecuzione", this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
		this.tableRisultati.setColumnHeader("Motore", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.tableRisultati.setColumnHeader("Q", StringheUNI.PORTATA);
                if (l_VG.isDonaldson()) {
                    this.tableRisultati.setColumnHeader("Pt", "Pt");
                    this.tableRisultati.setColumnHeader("Ps", "PSt");
		} else {
                    this.tableRisultati.setColumnHeader("Pt", StringheUNI.PRESSIONETOTALE);
                    this.tableRisultati.setColumnHeader("Ps", StringheUNI.PRESSIONESTATICA);
		}
		this.tableRisultati.setColumnHeader("W", StringheUNI.POTENZAASSE);
		this.tableRisultati.setColumnHeader("rpm", StringheUNI.RPM);
		this.tableRisultati.setColumnHeader("eta", StringheUNI.RENDIMENTO);
		this.tableRisultati.setColumnHeader("LW", StringheUNI.POTENZASONORA);
		this.tableRisultati.setColumnHeader("L", StringheUNI.PRESSIONESONORA);
		this.buttonSelRistretta.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Sel. Ristretta"));
		this.buttonAggiungiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
		this.buttonConfronto.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Confronta"));
	}
	
	private void abilitaBottoni(final Boolean value) {
		//buttonSelRistretta.setEnabled(value);
		//l_VG.setComponentBackground(buttonCopiaPreventivo.getId(), HColor.HGiallo);
		this.buttonAggiungiPreventivo.setEnabled(value);
		this.buttonConfronto.setEnabled(value);
	}
	
	public void reset() {
		this.tableRisultati.setEnabled(false);
		this.currentItemID = null;
		try {
			this.panelRisultati.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Risultati selezione"));
			this.tableRisultati.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
			
		}
		final StreamFileResource streamFileResource = new StreamFileResource(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "sfondoaccessori.png");
		final StreamResource source = new StreamResource(streamFileResource, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		source.setCacheTime(100);
		this.image.setSource(source);
		abilitaBottoni(false);
		this.l_VG.ElencoVentilatoriFromRicerca.clear();
		this.l_VG.MainView.setVentilatoreCorrenteFromRicerca(null);
		this.tableRisultati.setEnabled(true);
	}
	
	@SuppressWarnings("unchecked")
	public void aggiornaTableRisultati() {
            try {
                if (this.currentItemID != null) {
                    final Item selectedItem = this.containerRisultati.getItem(this.currentItemID);
                    if(!this.hidePrezzo) {
                        double Motore = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null ? this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Prezzo : 0;
                        double Prezzo = this.l_VG.VentilatoreCurrent.PrezzoVentilatoreForSort + Motore;
                        selectedItem.getItemProperty("Prezzo").setValue(Prezzo);
                    }
                    selectedItem.getItemProperty("Modello").setValue(this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent));
                    String esec = this.l_VG.VentilatoreCurrent.selezioneCorrente == null ? this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione : "-";
                    selectedItem.getItemProperty("Esecuzione").setValue(esec);
                    if (l_VG.isDonaldson()) {
                        l_VG.fmtNd.setMinimumFractionDigits(2);
                        l_VG.fmtNd.setMaximumFractionDigits(2);
                        selectedItem.getItemProperty("Motore").setValue(l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW) + " Kw");
                       }
                    else {
                        selectedItem.getItemProperty("Motore").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
                    }    
                    selectedItem.getItemProperty("Q").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromm3hToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, this.l_VG.UMPortataCorrente)));
                    selectedItem.getItemProperty("Pt").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale, this.l_VG.UMPressioneCorrente)));
                    selectedItem.getItemProperty("Ps").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica, this.l_VG.UMPressioneCorrente)));
                    selectedItem.getItemProperty("W").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza, this.l_VG.UMPotenzaCorrente)));
                    selectedItem.getItemProperty("rpm").setValue((int)this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
                    selectedItem.getItemProperty("eta").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.VentilatoreCurrent.selezioneCorrente.Rendimento, 2));
                    final double contributoXRumore = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(this.l_VG.VentilatoreCurrent.NBoccheCanalizzateBase, this.l_VG.VentilatoreCurrent.selezioneCorrente.nBoccheCanalizzate);
                    selectedItem.getItemProperty("LW").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaSonora+contributoXRumore, this.l_VG.NDecimaliRumore));
                    selectedItem.getItemProperty("L").setValue(this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaSonora+contributoXRumore, this.l_VG.NDecimaliRumore));
                }
            } catch (final Exception e) {
                    //l_VG.MainView.setMemo(e.toString());
            }
	}
	
	public void buildTableRisultati() {
		abilitaBottoni(false);
		this.tableRisultati.setEnabled(false);
		final StreamFileResource streamFileResource = new StreamFileResource(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "sfondoaccessori.png");
		final StreamResource source = new StreamResource(streamFileResource, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		source.setCacheTime(100);
		this.image.setSource(source);
		try {
			this.tableRisultati.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
		}
		if (this.l_VG.ElencoVentilatoriFromRicerca.size() > 0) {
			this.panelRisultati.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Risultati selezione") + " [" + Integer.toString(this.l_VG.ElencoVentilatoriFromRicerca.size()) + "]");
		} else {
			this.panelRisultati.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Risultati selezione"));
		}
		try {
			this.tableRisultati.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
		}
		for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromRicerca.size() ; i++) {
			final String itemID = Integer.toString(i);
			final Object obj[] = new Object[12];
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromRicerca.get(i);
			if (l_v.Classe.equals("-") && l_v.elencoRpmClassi.size() > 0) {
				l_v.Classe = NumeriRomani.daAraboARomano(Integer.toString(l_v.elencoClassi.get(0)));
			}
                        String Esec = l_v.selezioneCorrente.Esecuzione;
                        double Motore = l_v.selezioneCorrente.MotoreInstallato != null ? l_v.selezioneCorrente.MotoreInstallato.Prezzo : 0;
                        double Prezzo = l_v.PrezzoVentilatoreForSort + Motore;
                        obj[0] = this.hidePrezzo ? 0.0 : Prezzo;
			obj[1] = this.l_VG.buildModelloCompleto(l_v);
                        obj[2] = Esec;
                        if (l_VG.isDonaldson()) {
                            l_VG.fmtNd.setMinimumFractionDigits(2);
                            l_VG.fmtNd.setMaximumFractionDigits(2);
                            obj[3] = l_VG.fmtNd.format(l_v.selezioneCorrente.MotoreInstallato.PotkW) + " Kw";
                        } else {
                            obj[3] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
                        }
			obj[4] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, this.l_VG.UMPortataCorrente));
			obj[5] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, this.l_VG.UMPressioneCorrente));
			obj[6] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneStatica, this.l_VG.UMPressioneCorrente));
			obj[7] = this.l_VG.utilityMath.ArrotondaDouble(this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.Potenza, this.l_VG.UMPotenzaCorrente));
			obj[8] = (int)l_v.selezioneCorrente.NumeroGiri;
			obj[9] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.Rendimento, 2);
			final double contributoXRumore = this.l_VG.utilityCliente.getCorrettorePotenzaSonora(l_v.NBoccheCanalizzateBase, l_v.selezioneCorrente.nBoccheCanalizzate);
			obj[10] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PotenzaSonora+contributoXRumore, this.l_VG.NDecimaliRumore);
			obj[11] = this.l_VG.utilityMath.ArrotondaDouble(l_v.selezioneCorrente.PressioneSonora+contributoXRumore, this.l_VG.NDecimaliRumore);
			this.tableRisultati.addItem(obj, itemID);
		}
		this.tableRisultati.setEnabled(true);
	}
	
	class RemindTask extends TimerTask {
		@Override
		public void run() {
			SearchView.this.buttonAggiungiPreventivo.setIcon(null);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSelRistretta}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	@SuppressWarnings("static-access")
	private void buttonSelRistretta_buttonClick(final Button.ClickEvent event) {
        int index = -1;
        if (this.l_VG.ElencoVentilatoriFromRicerca.size() < 2) {
			return;
		}
        if (this.l_VG.ElencoVentilatoriFromRicerca.size() > 10) {
            this.l_VG.ventilatoreSort.sortCampo = this.l_VG.ventilatoreSort.sortDistanzaPuntoRichiesto;
            this.l_VG.ventilatoreSort.sortCrescente = true;
            this.l_VG.ventilatoreSort.sortRicerca(this.l_VG.ElencoVentilatoriFromRicerca, this.l_VG.VentilatoreSelezionato);
            while (this.l_VG.ElencoVentilatoriFromRicerca.size() > 10) {
                index = this.l_VG.ElencoVentilatoriFromRicerca.size() - 1;
                this.l_VG.ElencoVentilatoriFromRicerca.remove(index);
                this.l_VG.VentilatoreSelezionato.ElencoVentilatori.remove(index);
            }
        }
        this.l_VG.ventilatoreSort.sortCampo = this.l_VG.ventilatoreSort.sortRendimanto;
        this.l_VG.ventilatoreSort.sortCrescente = false;
        this.l_VG.ventilatoreSort.sortRicerca(this.l_VG.ElencoVentilatoriFromRicerca, this.l_VG.VentilatoreSelezionato);
        while (this.l_VG.ElencoVentilatoriFromRicerca.size() > 10) {
            index = this.l_VG.ElencoVentilatoriFromRicerca.size() - 1;
            this.l_VG.ElencoVentilatoriFromRicerca.remove(index);
            this.l_VG.VentilatoreSelezionato.ElencoVentilatori.remove(index);
        }
        buildTableRisultati();
		
		//NotYetImprementedWindow win = new NotYetImprementedWindow();
		//UI.getCurrent().addWindow(win);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAggiungiPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAggiungiPreventivo_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToPreventivo();
		this.buttonAggiungiPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
		final Timer timer = new Timer();
	    timer.schedule(new RemindTask(), 1000);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfronto}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonConfronto_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToCompare();
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableRisultati}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableRisultati_valueChange(final Property.ValueChangeEvent event) {
		if (!this.tableRisultati.isEnabled()) {
			return;
		}
		try {
			if (this.l_VG.debugFlag) {
				Notification.show(event.getProperty().getValue().toString());
			}
			this.currentItemID = event.getProperty().getValue().toString();
                        this.l_VG.MainView.setVentilatoreCorrenteFromRicerca(event.getProperty().getValue().toString());
                        l_VG.CARicerca.rho = this.l_VG.utilityFisica.calcolaDensitaFuido(l_VG.CA020Ricerca.rho, l_VG.CARicerca.temperatura, l_VG.CARicerca.altezza, 0., l_VG.CARicerca.umidita);
                        l_VG.VentilatoreCurrent.CAProgettazione = l_VG.CARicerca;
                        this.l_VG.MainView.aggiornaGrafico();
                        if (l_VG.isDonaldson()) {
                            this.l_VG.MainView.accessori04Donaldson.resetAccessori();
                            l_VG.MainView.accessori04Donaldson.startingIsPtc = (l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PTCPresente);
                        }
                        final PrestazioniView view = (PrestazioniView)this.l_VG.ElencoView[this.l_VG.PrestazioniViewIndex];
			final StreamResource graficosource = new StreamResource(view.grafico, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
			graficosource.setCacheTime(100);
			this.image.setSource(graficosource);
			abilitaBottoni(true);
		} catch (final Exception e) {
			final StreamFileResource streamFileResource = new StreamFileResource(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "sfondoaccessori.png");
			final StreamResource source = new StreamResource(streamFileResource, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
			source.setCacheTime(100);
			this.image.setSource(source);
			abilitaBottoni(false);
			//l_VG.MainView.setMemo(e.toString());
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.accordion = new XdevAccordion();
		this.panel2 = new XdevPanel();
		this.panel4 = new XdevPanel();
		this.panel = new XdevPanel();
		this.verticalLayout = new XdevVerticalLayout();
		this.panelRisultati = new XdevPanel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.tableRisultati = new XdevTable<>();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.buttonSelRistretta = new XdevButton();
		this.buttonAggiungiPreventivo = new XdevButton();
		this.buttonConfronto = new XdevButton();
		this.panelGrafico = new XdevPanel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.image = new XdevImage();
	
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.accordion.setStyleName("opaque");
		this.accordion.setCaptionAsHtml(true);
		this.panel2.setTabIndex(0);
		this.panel4.setTabIndex(0);
		this.panel.setTabIndex(0);
		this.verticalLayout.setSpacing(false);
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.panelRisultati.setCaption("Risultati selezione");
		this.panelRisultati.setTabIndex(0);
		this.panelRisultati.setCaptionAsHtml(true);
		this.verticalLayout4.setMargin(new MarginInfo(false));
		this.tableRisultati.setStyleName("small mystriped");
		this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.buttonSelRistretta.setCaption("SelRistr");
		this.buttonSelRistretta.setStyleName("small");
		this.buttonAggiungiPreventivo.setCaption("Aggiungi Preventivo");
		this.buttonAggiungiPreventivo.setStyleName("big giallo");
		this.buttonAggiungiPreventivo.setId("buttonCopiaPreventivo");
		this.buttonAggiungiPreventivo.setVisible(false);
		this.buttonConfronto.setCaption("Confronto");
		this.buttonConfronto.setStyleName("small");
		this.panelGrafico.setTabIndex(0);
		this.panelGrafico.setCaptionAsHtml(true);
	
		this.panel2.setSizeFull();
//		this.accordion.addTab(this.panel2, "Dati Selezione", null);
//		this.panel4.setSizeFull();
//		this.accordion.addTab(this.panel4, "Filtri di selezione", null);
//		this.accordion.setSelectedTab(this.panel2);
		this.buttonSelRistretta.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonSelRistretta);
		this.horizontalLayout2.setComponentAlignment(this.buttonSelRistretta, Alignment.MIDDLE_CENTER);
		this.buttonAggiungiPreventivo.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonAggiungiPreventivo);
		this.horizontalLayout2.setComponentAlignment(this.buttonAggiungiPreventivo, Alignment.MIDDLE_CENTER);
		this.buttonConfronto.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonConfronto);
		this.horizontalLayout2.setComponentAlignment(this.buttonConfronto, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout2_spacer = new CustomComponent();
		horizontalLayout2_spacer.setSizeFull();
		this.horizontalLayout2.addComponent(horizontalLayout2_spacer);
		this.horizontalLayout2.setExpandRatio(horizontalLayout2_spacer, 1.0F);
		this.tableRisultati.setSizeFull();
		this.verticalLayout4.addComponent(this.tableRisultati);
		this.verticalLayout4.setExpandRatio(this.tableRisultati, 100.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.horizontalLayout2);
		this.verticalLayout4.setExpandRatio(this.horizontalLayout2, 10.0F);
		this.verticalLayout4.setSizeFull();
		this.panelRisultati.setContent(this.verticalLayout4);
		this.image.setWidth(-1, Unit.PIXELS);
		this.image.setHeight(100, Unit.PERCENTAGE);
		this.verticalLayout2.addComponent(this.image);
		this.verticalLayout2.setComponentAlignment(this.image, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.image, 10.0F);
		this.verticalLayout2.setSizeFull();
		this.panelGrafico.setContent(this.verticalLayout2);
		this.panelRisultati.setSizeFull();
		this.verticalLayout.addComponent(this.panelRisultati);
		this.verticalLayout.setExpandRatio(this.panelRisultati, 10.0F);
		this.panelGrafico.setSizeFull();
		this.verticalLayout.addComponent(this.panelGrafico);
		this.verticalLayout.setExpandRatio(this.panelGrafico, 10.0F);
		this.verticalLayout.setSizeFull();
		this.panel.setContent(this.verticalLayout);
		this.accordion.setSizeFull();
		this.horizontalLayout.addComponent(this.panel2);
		this.horizontalLayout.setComponentAlignment(this.panel2, Alignment.TOP_CENTER);
		this.horizontalLayout.setExpandRatio(this.panel2, 10.0F);
		this.panel.setSizeFull();
		this.horizontalLayout.addComponent(this.panel);
		this.horizontalLayout.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.panel, 30.0F);
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	
		this.tableRisultati.addValueChangeListener(event -> this.tableRisultati_valueChange(event));
		this.buttonSelRistretta.addClickListener(event -> this.buttonSelRistretta_buttonClick(event));
		this.buttonAggiungiPreventivo.addClickListener(event -> this.buttonAggiungiPreventivo_buttonClick(event));
		this.buttonConfronto.addClickListener(event -> this.buttonConfronto_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonSelRistretta, buttonAggiungiPreventivo, buttonConfronto;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2;
	private XdevTable<CustomComponent> tableRisultati;
	private XdevImage image;
	private XdevPanel panel2, panel4, panel, panelRisultati, panelGrafico;
	private XdevVerticalLayout verticalLayout, verticalLayout4, verticalLayout2;
	private XdevAccordion accordion;
	// </generated-code>


}
