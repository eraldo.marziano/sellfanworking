package com.cit.sellfan.ui.view;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevProgressBar;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

import cit.utility.UtilityTraduzioni;

public class ProgressView extends XdevView {

	/**
	 * 
	 */
	public ProgressView() {
		super();
		this.initUI();
		Notification.show("ProgrssView");
	}

	public ProgressView(final UtilityTraduzioni utilityTraduzioni) {
		super();
		this.initUI();
		this.labelWork.setValue(utilityTraduzioni.TraduciStringa("Attività in corso") + "...");
		this.labelWait.setValue(utilityTraduzioni.TraduciStringa("Attendere prego"));
		//Notification.show("ProgrssView "+utilityTraduzioni.toString());
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelWork = new XdevLabel();
		this.labelWait = new XdevLabel();
		this.progressBar = new XdevProgressBar();
	
		this.labelWork.setStyleName("h2");
		this.labelWork.setValue("Work in progress...");
		this.labelWait.setStyleName("h2");
		this.labelWait.setValue("Please wait");
		this.progressBar.setIndeterminate(true);
		this.progressBar.setStyleName("big");
	
		this.labelWork.setSizeUndefined();
		this.verticalLayout.addComponent(this.labelWork);
		this.verticalLayout.setComponentAlignment(this.labelWork, Alignment.MIDDLE_CENTER);
		this.labelWait.setSizeUndefined();
		this.verticalLayout.addComponent(this.labelWait);
		this.verticalLayout.setComponentAlignment(this.labelWait, Alignment.MIDDLE_CENTER);
		this.progressBar.setSizeUndefined();
		this.verticalLayout.addComponent(this.progressBar);
		this.verticalLayout.setComponentAlignment(this.progressBar, Alignment.BOTTOM_CENTER);
		final CustomComponent verticalLayout_spacer = new CustomComponent();
		verticalLayout_spacer.setSizeFull();
		this.verticalLayout.addComponent(verticalLayout_spacer);
		this.verticalLayout.setExpandRatio(verticalLayout_spacer, 1.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setWidth(250, Unit.PIXELS);
		this.setHeight(180, Unit.PIXELS);
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelWork, labelWait;
	private XdevProgressBar progressBar;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>

}
