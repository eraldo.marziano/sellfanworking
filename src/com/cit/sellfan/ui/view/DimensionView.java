package com.cit.sellfan.ui.view;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.template.jDisplayImageWeb;
import com.vaadin.data.Property;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.sellfan.Costanti;
import cit.sellfan.classi.Accessorio;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class DimensionView extends XdevView {
    public boolean cambiaImmagine = false;
	public boolean isPannelloInizializzato = false;
	public boolean angoloEnabled = true;
	private VariabiliGlobali l_VG;
	private final XdevLabel parametri[] = new XdevLabel[40];
    private String imgDimensioni;
    private String imgDimensioniQ1;
    private String imgDimensioniQ2;
    private String imgDimensioniQ3;
    private String imgDimensioniQ4;
	private StreamResource streamImageVuota = null;
    private final jDisplayImageWeb displayImage = new jDisplayImageWeb();
    // jDisplayImage combina le 5 imagini in una
	/**
	 * 
	 */
	public DimensionView() {
		super();
		this.initUI();
		int index = 0;
		for (int riga=0 ; riga<5 ; riga++) {
			for (int colonna=0 ; colonna<8 ; colonna++) {
				this.gridLayout.removeComponent(colonna, riga);
				//index = riga * 8 + colonna;
				this.parametri[index] = new XdevLabel();
				this.parametri[index].setContentMode(ContentMode.HTML);
				this.parametri[index].setWidth(100, Unit.PERCENTAGE);
				this.parametri[index].setHeight(-1, Unit.PIXELS);
				this.parametri[index].setValue(Integer.toString(index));
				this.gridLayout.addComponent(this.parametri[index], colonna, riga);
				index++;
			}
		}
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		if (this.angoloEnabled) {
		    this.l_VG.ElencoView[this.l_VG.DimensioniAngoloViewIndex] = this;
		} else {
		    this.l_VG.ElencoView[this.l_VG.DimensioniViewIndex] = this;
		}
	    this.l_VG.ElencoView[this.l_VG.DimensioniViewIndex] = this;
		if (this.l_VG.logostreamresource != null) {
			this.imageLogo.setSource(this.l_VG.logostreamresource);
		}
		this.streamImageVuota = this.l_VG.getStreamResourceForImageFile(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "sfondoaccessori.png");
		this.buttonAngoloMeno.setVisible(this.l_VG.utilityCliente.isTabVisible("DimensioniAngolo"));
		this.buttonAngoloPiu.setVisible(this.l_VG.utilityCliente.isTabVisible("DimensioniAngolo"));
		this.labelAngolo.setVisible(this.l_VG.utilityCliente.isTabVisible("DimensioniAngolo"));
		if (this.l_VG.utilityCliente.idClienteIndex == 4) {
                    imageDefault.setWidth("80%");
		}
	}
        
        public void togglePreventivoButton(boolean value) {
            buttonAggiungiPreventivo.setEnabled(value);
        }
	
	public void Nazionalizza() {
		this.buttonAggiungiPreventivo.setVisible(this.l_VG.utilityCliente.isTabVisible("Offerte") && this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
		this.labelmm.setValue("<html><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Dati in mm") + "</html>");
		this.buttonCambiaOrientamento.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cambia"));
		this.labelEsecuzione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
		this.labelOriReq.setValue("<html><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Orientamento LG-RD a richiesta") + "</html>");
		this.labelCliente.setValue("<html><center>" + this.l_VG.utilityCliente.DenominazioneClienteSRL + "</html");
		this.labelRiserva1.setValue("<html><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("si riserva") + " " + this.l_VG.utilityTraduzioni.TraduciStringa("il diritto di modificare") + " " + this.l_VG.utilityTraduzioni.TraduciStringa("i dati tecnici senza") + " " + this.l_VG.utilityTraduzioni.TraduciStringa("preavviso.") + "</html>");
		this.labelRiserva2.setValue("<html><center>" + this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.utilityCliente.getMemoDimensioniRiserva2()) + "</html");
		this.labelRiserva3.setValue("<html><center>" + this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.utilityCliente.getMemoDimensioniRiserva3()) + "</html");
		this.buttonAggiungiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
		//  + "<html><center>" start html centra la lable	(tag html)
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		if (this.l_VG == null || this.l_VG.VentilatoreCurrent == null || this.l_VG.VentilatoreCurrentIndex < 0) {
			return false;
		}
		if (this.angoloEnabled) {
			if (this.l_VG.utilityCliente.isTabVisible("DimensioniAngolo")) {
				if (!this.l_VG.dbTecnico.caricaDimensioniEsecuzione(this.l_VG.VentilatoreCurrent)) {
					return false;
				}
			} else {
				return false;
			}
			int NN = 0;
			for (int i=0 ; i<this.l_VG.VentilatoreCurrent.DIMOrientamento_n ; i++) {
				if (!this.l_VG.VentilatoreCurrent.DIMOrientamento_Val[i].equals("-")) {
					NN++;
				}
			}
			if (this.l_VG.VentilatoreCurrent.DIMOrientamento_Img == null && NN == 0) {
				return false;
			}
		} else {
			if (this.l_VG.utilityCliente.isTabVisible("Dimensioni")) {
				this.l_VG.dbTecnico.caricaDimensioni(this.l_VG.VentilatoreCurrent);
			} else {
				return false;
			}
			int NN = 0;
			for (int i=0 ; i<this.l_VG.VentilatoreCurrent.DIM_n ; i++) {
				if (!this.l_VG.VentilatoreCurrent.DIM_Val[i].equals("-")) {
					NN++;
				}
			}
			
			if ((this.l_VG.VentilatoreCurrent.DIM_Img == null || this.l_VG.VentilatoreCurrent.DIM_Img.toString().equals("-")) && NN == 0) {
				return false;
			}
		}
		return true;
	}
	
	@SuppressWarnings("unused")
	public void setVentilatoreCurrent() {
            
		this.isPannelloInizializzato = false;
		if (!isPreSetVentilatoreCurrentOK()) {
			return;
		}
		int index = 0;
		int NN;
        this.labelPD2.setVisible(false);
		this.labelPeso.setVisible(false);
        if (this.angoloEnabled) {
        	NN = Math.min(40, this.l_VG.VentilatoreCurrent.DIMOrientamento_n);
            this.imgDimensioni = this.l_VG.VentilatoreCurrent.DIMOrientamento_Img;
            if (this.imgDimensioni == null) {
				return;
			}
            this.imgDimensioniQ1 = (cambiaImmagine)? null : this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ1;
            this.imgDimensioniQ2 = (cambiaImmagine)? null : this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ2;
            this.imgDimensioniQ3 = this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ3;
            this.imgDimensioniQ4 = this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ4;
    		for (int i=0 ; i<NN ; i++) {
    			if (this.l_VG.VentilatoreCurrent.DIMOrientamento_Val[i].equals("-")) {
					continue;
				}
    			if (this.l_VG.VentilatoreCurrent.DIMOrientamento_Label[i].equals("PD2")) {
    				this.labelPD2.setValue("<html><center>" + "PD<sup>2</sup>" + ": <font color='blue'><b>" + this.l_VG.VentilatoreCurrent.DIMOrientamento_Val[i] + "</html>");
    				this.labelPD2.setVisible(true);
    				continue;
    			}
    			if (this.l_VG.VentilatoreCurrent.DIMOrientamento_Label[i].equals("kg")) {
    				this.labelPeso.setValue("<html><center>" + this.l_VG.utility.rimpiazzaLettereGreche(this.l_VG.VentilatoreCurrent.DIMOrientamento_Label[i]) + ": <font color='blue'><b>" + this.l_VG.VentilatoreCurrent.DIMOrientamento_Val[i] + "</html>");
    				//Notification.show("peso "+l_VG.VentilatoreCurrent.DIMOrientamento_Val[i]);
    				this.labelPeso.setVisible(true);
    				continue;
    			}
    			this.parametri[index].setVisible(true);
    			this.parametri[index].setValue("<html><center>" + this.l_VG.utility.rimpiazzaLettereGreche(this.l_VG.VentilatoreCurrent.DIMOrientamento_Label[i]) + ": <font color='blue'><b>" + this.l_VG.VentilatoreCurrent.DIMOrientamento_Val[i] + "</html>");
    			index++;
    		}
        } else {
        	NN = Math.min(40, this.l_VG.VentilatoreCurrent.DIM_n);
        	String str = "[";
        	for (int i=0 ; i<NN ; i++) {
        		str = str + this.l_VG.VentilatoreCurrent.DIM_Val[i] + "][";
        	}
            this.imgDimensioni = this.l_VG.VentilatoreCurrent.DIM_Img;
            if (this.imgDimensioni == null) {
				//return;
			}
            this.imgDimensioniQ1 = null;
            this.imgDimensioniQ2 = null;
            this.imgDimensioniQ3 = null;
            this.imgDimensioniQ4 = null;
    		for (int i=0 ; i<NN ; i++) {
    			if (this.l_VG.VentilatoreCurrent.DIM_Val[i].equals("-")) {
					continue;
				}
    			if (this.l_VG.VentilatoreCurrent.DIM_Label[i].equals("PD2")) {
    				this.labelPD2.setValue("<html><center>" + "PD<sup>2</sup>" + ": <font color='blue'><b>" + this.l_VG.VentilatoreCurrent.DIM_Val[i] + "</html>");
    				this.labelPD2.setVisible(true);
    				continue;
    			}
    			if (this.l_VG.VentilatoreCurrent.DIM_Label[i].equals("kg")) {
    				this.labelPeso.setValue("<html><center>" + this.l_VG.utility.rimpiazzaLettereGreche(this.l_VG.VentilatoreCurrent.DIM_Label[i]) + ": <font color='blue'><b>" + this.l_VG.VentilatoreCurrent.DIM_Val[i] + "</html>");
    				//Notification.show("peso "+l_VG.VentilatoreCurrent.DIM_Val[i]);
    				this.labelPeso.setVisible(true);
    				continue;
    			}
    			this.parametri[index].setVisible(true);
    			this.parametri[index].setValue("<html><center>" + this.l_VG.utility.rimpiazzaLettereGreche(this.l_VG.VentilatoreCurrent.DIM_Label[i]) + ": <font color='blue'><b>" + this.l_VG.VentilatoreCurrent.DIM_Val[i] + "</html>");
    			index++;
    		}
        }
		for ( ; index<40 ; index++) {
			this.parametri[index].setVisible(false);
		}
		try {
			InputStream isImg = null;
			try {
				//Notification.show(l_VG.Paths.rootResources + l_VG.Paths.rootIMG + "dimensioni/" + imgDimensioni);
				isImg = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.imgDimensioni);
			} catch (final Exception e1) {
				
			}
			if (isImg == null) {
				this.streamImageVuota = this.l_VG.getStreamResourceForImageFile(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "sfondoaccessori.png");
				this.imageDefault.setSource(this.streamImageVuota);
				return;
			}
			InputStream isImgQ1 = null;
			try {
				isImgQ1 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.imgDimensioniQ1);
			} catch (final Exception e1) {
				
			}
			InputStream isImgQ2 = null;
			try {
				isImgQ2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.imgDimensioniQ2);
			} catch (final Exception e1) {
				
			}
			InputStream isImgQ3 = null;
			try {
				isImgQ3 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.imgDimensioniQ3);
			} catch (final Exception e1) {
				
			}
			InputStream isImgQ4 = null;
			try {
				isImgQ4 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.imgDimensioniQ4);
			} catch (final Exception e1) {
				
			}
			this.displayImage.DisplayfromInputStream(isImg, isImgQ1, isImgQ2, isImgQ3, isImgQ4);
			this.displayImage.buildImage();
                        if (l_VG.utilityCliente.idClienteIndex == 4) {
                            try {
                                FileInputStream isImgDisclaimer = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/disclaimer.png");
                                BufferedImage disc = ImageIO.read(isImgDisclaimer);
                                Graphics g = displayImage.getImage().getGraphics();
                                if (disc != null) {
                                    g.drawImage(disc, displayImage.getImageWith()-disc.getWidth(null), displayImage.getImageHeight()/2-disc.getHeight(null)/2, disc.getWidth(null), disc.getHeight(null), null);
                                }
                            } catch (Exception e) {
                            }
                        }
			final StreamResource imagesource = new StreamResource(this.displayImage, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
			if (imagesource == null)  {
				this.streamImageVuota = this.l_VG.getStreamResourceForImageFile(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "sfondoaccessori.png");
				this.imageDefault.setSource(this.streamImageVuota);
				return;
			}
			imagesource.setCacheTime(0);
			this.imageDefault.setSource(imagesource);
		} catch (final Exception e) {
			this.imageDefault.setSource(this.streamImageVuota);
		}
		this.labelAngolo.setValue("<html><center><b>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
		this.labelOrientamento.setValue("<html><center><b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Orientamento") + " " + this.l_VG.VentilatoreCurrent.selezioneCorrente.Orientamento + "</html>");
		this.comboBoxEsecuzioni.setEnabled(false);
		this.comboBoxEsecuzioni.removeAllItems();
            for (int i = 0; i < this.l_VG.VentilatoreCurrent.Esecuzioni.length; i++) {
                if (!this.l_VG.VentilatoreCurrent.Esecuzioni[i].equals("-")) {
                    if (l_VG.isDonaldson()) {
                        if (this.l_VG.VentilatoreCurrent.Esecuzioni[i].equals("E04"))
                            this.comboBoxEsecuzioni.addItem("Standing");
                        if (this.l_VG.VentilatoreCurrent.Esecuzioni[i].equals("E05"))
                            this.comboBoxEsecuzioni.addItem("Laying");
                    } else {
                        this.comboBoxEsecuzioni.addItem(this.l_VG.VentilatoreCurrent.Esecuzioni[i]);
                    }
                }
            }
        String esecuzione = this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione;
        esecuzione = (esecuzione.equals("E04") && l_VG.isDonaldson()) ? "Standing" : esecuzione;
        esecuzione = (esecuzione.equals("E05") && l_VG.isDonaldson()) ? "Laying" : esecuzione;
        this.comboBoxEsecuzioni.setValue(esecuzione);
        final Collection<?> coll = this.comboBoxEsecuzioni.getItemIds();
        this.labelDescrizione.setValue("<html><center>" + this.l_VG.getDescrizioneEsecuzione(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione) + "</html>");
        this.labelOriReq.setVisible(this.l_VG.utilityCliente.isOrientamentoARichiestaVisible(this.l_VG.VentilatoreCurrent));
        this.labelOriReqAngolo.setVisible(this.labelOriReq.isVisible());
        this.labelTitolo.setValue("<html><center><font color='blue'><b>" + this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent) + "</html");
        this.comboBoxEsecuzioni.setEnabled(this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo && coll.size() > 1 && this.angoloEnabled);
        this.buttonAggiungiPreventivo.setEnabled(this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo);
        if (l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.size()>0) {
            if (l_VG.VentilatoreCurrent.selezioneCorrente.listaWarning.get(6).warningAttivo)
                this.buttonAggiungiPreventivo.setEnabled(false);
        }
        this.isPannelloInizializzato = true;
        this.panelAngolo.setVisible(this.angoloEnabled);
        //Notification.show(l_VG.VentilatoreCurrent.DIMOrientamento_PesoTotale);
	}

    public void setMPF(Accessorio l_a) {
        cambiaImmagine = l_a.Selezionato;
        if (l_a.Selezionato) {
            buttonAngoloMeno.setEnabled(false);
            buttonAngoloPiu.setEnabled(false);
            this.labelAngolo.setValue("<html><b><center>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
        } else {
            buttonAngoloMeno.setEnabled(true);
            buttonAngoloPiu.setEnabled(true);
            this.labelAngolo.setValue("<html><b><center>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
        }
    }
	
	class RemindTask extends TimerTask {
		@Override
		public void run() {
			DimensionView.this.buttonAggiungiPreventivo.setIcon(null);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAngoloPiu}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAngoloPiu_buttonClick(final Button.ClickEvent event) {
		this.l_VG.cambiaAngolo(1);
		setVentilatoreCurrent();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAngoloMeno}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAngoloMeno_buttonClick(final Button.ClickEvent event) {
		this.l_VG.cambiaAngolo(-1);
		setVentilatoreCurrent();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCambiaOrientamento}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCambiaOrientamento_buttonClick(final Button.ClickEvent event) {
		this.l_VG.cambiaOrientamento();
		setVentilatoreCurrent();
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxEsecuzioni}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxEsecuzioni_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxEsecuzioni.isEnabled()) {
			return;
		}
		if (this.comboBoxEsecuzioni.getValue() == null) {
			return;
		}
                String value = this.comboBoxEsecuzioni.getValue().toString();
                if (value.equals("Standing")) value = "E04";
                if (value.equals("Laying")) value = "E05";
		this.l_VG.cambiaEsecuzione(value);
                if (l_VG.isDonaldson())
                    l_VG.MainView.accessori04Donaldson.resetAccessori();
		setVentilatoreCurrent();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAggiungiPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAggiungiPreventivo_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToPreventivo();
		this.buttonAggiungiPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
		final Timer timer = new Timer();
	    timer.schedule(new RemindTask(), 1000);
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.verticalLayout = new XdevVerticalLayout();
		this.panelForImage = new XdevPanel();
		this.verticalLayout5 = new XdevVerticalLayout();
		this.imageDefault = new XdevImage();
		this.gridLayout = new XdevGridLayout();
		this.label6 = new XdevLabel();
		this.label7 = new XdevLabel();
		this.label10 = new XdevLabel();
		this.label11 = new XdevLabel();
		this.label12 = new XdevLabel();
		this.label13 = new XdevLabel();
		this.label14 = new XdevLabel();
		this.label15 = new XdevLabel();
		this.label5 = new XdevLabel();
		this.label4 = new XdevLabel();
		this.label16 = new XdevLabel();
		this.label17 = new XdevLabel();
		this.label18 = new XdevLabel();
		this.label19 = new XdevLabel();
		this.label20 = new XdevLabel();
		this.label21 = new XdevLabel();
		this.label3 = new XdevLabel();
		this.label2 = new XdevLabel();
		this.label22 = new XdevLabel();
		this.label23 = new XdevLabel();
		this.label24 = new XdevLabel();
		this.label25 = new XdevLabel();
		this.label26 = new XdevLabel();
		this.label27 = new XdevLabel();
		this.label8 = new XdevLabel();
		this.label9 = new XdevLabel();
		this.label28 = new XdevLabel();
		this.label29 = new XdevLabel();
		this.label30 = new XdevLabel();
		this.label31 = new XdevLabel();
		this.label32 = new XdevLabel();
		this.label33 = new XdevLabel();
		this.label34 = new XdevLabel();
		this.label35 = new XdevLabel();
		this.label36 = new XdevLabel();
		this.label37 = new XdevLabel();
		this.label38 = new XdevLabel();
		this.label39 = new XdevLabel();
		this.label40 = new XdevLabel();
		this.label41 = new XdevLabel();
		this.labelPD2 = new XdevLabel();
		this.labelPeso = new XdevLabel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.imageLogo = new XdevImage();
		this.labelTitolo = new XdevLabel();
		this.labelmm = new XdevLabel();
		this.labelCliente = new XdevLabel();
		this.labelRiserva1 = new XdevLabel();
		this.labelRiserva2 = new XdevLabel();
		this.labelRiserva3 = new XdevLabel();
		this.panel = new XdevPanel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.labelOriReq = new XdevLabel();
		this.labelOriReqAngolo = new XdevLabel();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.labelEsecuzione = new XdevLabel();
		this.comboBoxEsecuzioni = new XdevComboBox<>();
		this.labelDescrizione = new XdevLabel();
		this.panelAngolo = new XdevPanel();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.buttonAngoloMeno = new XdevButton();
		this.labelAngolo = new XdevLabel();
		this.buttonAngoloPiu = new XdevButton();
		this.labelOrientamento = new XdevLabel();
		this.buttonCambiaOrientamento = new XdevButton();
		this.buttonAggiungiPreventivo = new XdevButton();
	
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.gridLayout.setMargin(new MarginInfo(false));
		this.label6.setValue("Label");
		this.label6.setContentMode(ContentMode.HTML);
		this.label7.setValue("Label");
		this.label10.setValue("Label");
		this.label11.setValue("Label");
		this.label12.setValue("Label");
		this.label13.setValue("Label");
		this.label14.setValue("Label");
		this.label15.setValue("Label");
		this.label5.setValue("Label");
		this.label4.setValue("Label");
		this.label16.setValue("Label");
		this.label17.setValue("Label");
		this.label18.setValue("Label");
		this.label19.setValue("Label");
		this.label20.setValue("Label");
		this.label21.setValue("Label");
		this.label3.setValue("Label");
		this.label2.setValue("Label");
		this.label22.setValue("Label");
		this.label23.setValue("Label");
		this.label24.setValue("Label");
		this.label25.setValue("Label");
		this.label26.setValue("Label");
		this.label27.setValue("Label");
		this.label8.setValue("Label");
		this.label9.setValue("Label");
		this.label28.setValue("Label");
		this.label29.setValue("Label");
		this.label30.setValue("Label");
		this.label31.setValue("Label");
		this.label32.setValue("Label");
		this.label33.setValue("Label");
		this.label34.setValue("Label");
		this.label35.setValue("Label");
		this.label36.setValue("Label");
		this.label37.setValue("Label");
		this.label38.setValue("Label");
		this.label39.setValue("Label");
		this.label40.setValue("Label");
		this.label41.setValue("Label");
		this.labelPD2.setValue("PD2");
		this.labelPD2.setContentMode(ContentMode.HTML);
		this.labelPeso.setValue("peso");
		this.labelPeso.setContentMode(ContentMode.HTML);
		this.verticalLayout2.setMargin(new MarginInfo(false));
		this.labelTitolo.setValue("Titolo");
		this.labelTitolo.setContentMode(ContentMode.HTML);
		this.labelmm.setValue("mm");
		this.labelmm.setContentMode(ContentMode.HTML);
		this.labelCliente.setValue("Cliente");
		this.labelCliente.setContentMode(ContentMode.HTML);
		this.labelRiserva1.setValue("Riserva1");
		this.labelRiserva1.setContentMode(ContentMode.HTML);
		this.labelRiserva2.setValue("Riserva2");
		this.labelRiserva2.setContentMode(ContentMode.HTML);
		this.labelRiserva3.setValue("Riserva3");
		this.labelRiserva3.setContentMode(ContentMode.HTML);
		this.panel.setTabIndex(0);
		this.verticalLayout4.setMargin(new MarginInfo(false));
		this.labelOriReq.setValue("OriReq");
		this.labelOriReq.setContentMode(ContentMode.HTML);
		this.labelOriReqAngolo.setValue("<html><center>180°-225°</html>");
		this.labelOriReqAngolo.setContentMode(ContentMode.HTML);
		this.horizontalLayout3.setMargin(new MarginInfo(false));
		this.labelEsecuzione.setValue("Esecuzione");
		this.labelDescrizione.setValue("Descrizione");
		this.labelDescrizione.setContentMode(ContentMode.HTML);
		this.panelAngolo.setTabIndex(0);
		this.verticalLayout3.setMargin(new MarginInfo(false));
		this.horizontalLayout2.setMargin(new MarginInfo(false));
		this.buttonAngoloMeno.setCaption("<<");
		this.buttonAngoloMeno.setStyleName("small");
		this.labelAngolo.setValue("0");
		this.labelAngolo.setContentMode(ContentMode.HTML);
		this.buttonAngoloPiu.setCaption(">>");
		this.buttonAngoloPiu.setStyleName("small");
		this.labelOrientamento.setValue("RD");
		this.labelOrientamento.setContentMode(ContentMode.HTML);
		this.buttonCambiaOrientamento.setCaption("Cambia");
		this.buttonCambiaOrientamento.setStyleName("small");
		this.buttonAggiungiPreventivo.setCaption("Aggiungi Preventivo");
		this.buttonAggiungiPreventivo.setStyleName("big giallo");
		this.imageDefault.setWidth(100, Unit.PERCENTAGE);
		this.imageDefault.setHeight(-1, Unit.PIXELS);
		this.verticalLayout5.addComponent(this.imageDefault);
		this.verticalLayout5.setComponentAlignment(this.imageDefault, Alignment.MIDDLE_CENTER);
		this.verticalLayout5.setExpandRatio(this.imageDefault, 10.0F);
		this.verticalLayout5.setSizeFull();
		this.panelForImage.setContent(this.verticalLayout5);
		this.gridLayout.setColumns(8);
		this.gridLayout.setRows(7);
		this.label6.setWidth(100, Unit.PERCENTAGE);
		this.label6.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label6, 0, 0);
		this.label7.setWidth(100, Unit.PERCENTAGE);
		this.label7.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label7, 1, 0);
		this.label10.setWidth(100, Unit.PERCENTAGE);
		this.label10.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label10, 2, 0);
		this.label11.setWidth(100, Unit.PERCENTAGE);
		this.label11.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label11, 3, 0);
		this.label12.setWidth(100, Unit.PERCENTAGE);
		this.label12.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label12, 4, 0);
		this.label13.setWidth(100, Unit.PERCENTAGE);
		this.label13.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label13, 5, 0);
		this.label14.setWidth(100, Unit.PERCENTAGE);
		this.label14.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label14, 6, 0);
		this.label15.setWidth(100, Unit.PERCENTAGE);
		this.label15.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label15, 7, 0);
		this.label5.setSizeUndefined();
		this.gridLayout.addComponent(this.label5, 0, 1);
		this.label4.setSizeUndefined();
		this.gridLayout.addComponent(this.label4, 1, 1);
		this.label16.setSizeUndefined();
		this.gridLayout.addComponent(this.label16, 2, 1);
		this.label17.setSizeUndefined();
		this.gridLayout.addComponent(this.label17, 3, 1);
		this.label18.setSizeUndefined();
		this.gridLayout.addComponent(this.label18, 4, 1);
		this.label19.setSizeUndefined();
		this.gridLayout.addComponent(this.label19, 5, 1);
		this.label20.setSizeUndefined();
		this.gridLayout.addComponent(this.label20, 6, 1);
		this.label21.setSizeUndefined();
		this.gridLayout.addComponent(this.label21, 7, 1);
		this.label3.setSizeUndefined();
		this.gridLayout.addComponent(this.label3, 0, 2);
		this.label2.setSizeUndefined();
		this.gridLayout.addComponent(this.label2, 1, 2);
		this.label22.setSizeUndefined();
		this.gridLayout.addComponent(this.label22, 2, 2);
		this.label23.setSizeUndefined();
		this.gridLayout.addComponent(this.label23, 3, 2);
		this.label24.setSizeUndefined();
		this.gridLayout.addComponent(this.label24, 4, 2);
		this.label25.setSizeUndefined();
		this.gridLayout.addComponent(this.label25, 5, 2);
		this.label26.setSizeUndefined();
		this.gridLayout.addComponent(this.label26, 6, 2);
		this.label27.setSizeUndefined();
		this.gridLayout.addComponent(this.label27, 7, 2);
		this.label8.setSizeUndefined();
		this.gridLayout.addComponent(this.label8, 0, 3);
		this.label9.setSizeUndefined();
		this.gridLayout.addComponent(this.label9, 1, 3);
		this.label28.setSizeUndefined();
		this.gridLayout.addComponent(this.label28, 2, 3);
		this.label29.setSizeUndefined();
		this.gridLayout.addComponent(this.label29, 3, 3);
		this.label30.setSizeUndefined();
		this.gridLayout.addComponent(this.label30, 4, 3);
		this.label31.setSizeUndefined();
		this.gridLayout.addComponent(this.label31, 5, 3);
		this.label32.setSizeUndefined();
		this.gridLayout.addComponent(this.label32, 6, 3);
		this.label33.setSizeUndefined();
		this.gridLayout.addComponent(this.label33, 7, 3);
		this.label34.setSizeUndefined();
		this.gridLayout.addComponent(this.label34, 0, 4);
		this.label35.setSizeUndefined();
		this.gridLayout.addComponent(this.label35, 1, 4);
		this.label36.setSizeUndefined();
		this.gridLayout.addComponent(this.label36, 2, 4);
		this.label37.setSizeUndefined();
		this.gridLayout.addComponent(this.label37, 3, 4);
		this.label38.setSizeUndefined();
		this.gridLayout.addComponent(this.label38, 4, 4);
		this.label39.setSizeUndefined();
		this.gridLayout.addComponent(this.label39, 5, 4);
		this.label40.setSizeUndefined();
		this.gridLayout.addComponent(this.label40, 6, 4);
		this.label41.setSizeUndefined();
		this.gridLayout.addComponent(this.label41, 7, 4);
		this.labelPD2.setWidth(100, Unit.PERCENTAGE);
		this.labelPD2.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelPD2, 4, 5, 5, 5);
		this.labelPeso.setWidth(100, Unit.PERCENTAGE);
		this.labelPeso.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelPeso, 6, 5, 7, 5);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setColumnExpandRatio(2, 10.0F);
		this.gridLayout.setColumnExpandRatio(3, 10.0F);
		this.gridLayout.setColumnExpandRatio(4, 10.0F);
		this.gridLayout.setColumnExpandRatio(5, 10.0F);
		this.gridLayout.setColumnExpandRatio(6, 10.0F);
		this.gridLayout.setColumnExpandRatio(7, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 6, 7, 6);
		this.gridLayout.setRowExpandRatio(6, 1.0F);
		this.panelForImage.setSizeFull();
		this.verticalLayout.addComponent(this.panelForImage);
		this.verticalLayout.setComponentAlignment(this.panelForImage, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.panelForImage, 10.0F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.BOTTOM_CENTER);
		this.labelEsecuzione.setSizeUndefined();
		this.horizontalLayout3.addComponent(this.labelEsecuzione);
		this.horizontalLayout3.setComponentAlignment(this.labelEsecuzione, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout3.setExpandRatio(this.labelEsecuzione, 10.0F);
		this.comboBoxEsecuzioni.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxEsecuzioni.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.comboBoxEsecuzioni);
		this.horizontalLayout3.setComponentAlignment(this.comboBoxEsecuzioni, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.comboBoxEsecuzioni, 10.0F);
		this.labelOriReq.setWidth(100, Unit.PERCENTAGE);
		this.labelOriReq.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.labelOriReq);
		this.verticalLayout4.setComponentAlignment(this.labelOriReq, Alignment.MIDDLE_CENTER);
		this.labelOriReqAngolo.setWidth(100, Unit.PERCENTAGE);
		this.labelOriReqAngolo.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.labelOriReqAngolo);
		this.verticalLayout4.setComponentAlignment(this.labelOriReqAngolo, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.labelOriReqAngolo, 10.0F);
		this.horizontalLayout3.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.horizontalLayout3);
		this.verticalLayout4.setExpandRatio(this.horizontalLayout3, 10.0F);
		this.labelDescrizione.setWidth(100, Unit.PERCENTAGE);
		this.labelDescrizione.setHeight(-1, Unit.PIXELS);
		this.verticalLayout4.addComponent(this.labelDescrizione);
		this.verticalLayout4.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout4.setHeight(-1, Unit.PIXELS);
		this.panel.setContent(this.verticalLayout4);
		this.buttonAngoloMeno.setWidth(100, Unit.PERCENTAGE);
		this.buttonAngoloMeno.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.buttonAngoloMeno);
		this.horizontalLayout2.setComponentAlignment(this.buttonAngoloMeno, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.buttonAngoloMeno, 10.0F);
		this.labelAngolo.setWidth(100, Unit.PERCENTAGE);
		this.labelAngolo.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.labelAngolo);
		this.horizontalLayout2.setComponentAlignment(this.labelAngolo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.labelAngolo, 10.0F);
		this.buttonAngoloPiu.setWidth(100, Unit.PERCENTAGE);
		this.buttonAngoloPiu.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.buttonAngoloPiu);
		this.horizontalLayout2.setComponentAlignment(this.buttonAngoloPiu, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.buttonAngoloPiu, 10.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout2);
		this.verticalLayout3.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);
		this.labelOrientamento.setWidth(100, Unit.PERCENTAGE);
		this.labelOrientamento.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.labelOrientamento);
		this.verticalLayout3.setComponentAlignment(this.labelOrientamento, Alignment.MIDDLE_CENTER);
		this.buttonCambiaOrientamento.setSizeUndefined();
		this.verticalLayout3.addComponent(this.buttonCambiaOrientamento);
		this.verticalLayout3.setComponentAlignment(this.buttonCambiaOrientamento, Alignment.MIDDLE_CENTER);
		final CustomComponent verticalLayout3_spacer = new CustomComponent();
		verticalLayout3_spacer.setSizeFull();
		this.verticalLayout3.addComponent(verticalLayout3_spacer);
		this.verticalLayout3.setExpandRatio(verticalLayout3_spacer, 1.0F);
		this.verticalLayout3.setSizeFull();
		this.panelAngolo.setContent(this.verticalLayout3);
		this.imageLogo.setWidth(100, Unit.PERCENTAGE);
		this.imageLogo.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.imageLogo);
		this.verticalLayout2.setExpandRatio(this.imageLogo, 10.0F);
		this.labelTitolo.setWidth(100, Unit.PERCENTAGE);
		this.labelTitolo.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelTitolo);
		this.labelmm.setSizeFull();
		this.verticalLayout2.addComponent(this.labelmm);
		this.verticalLayout2.setComponentAlignment(this.labelmm, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.labelmm, 10.0F);
		this.labelCliente.setWidth(100, Unit.PERCENTAGE);
		this.labelCliente.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelCliente);
		this.verticalLayout2.setComponentAlignment(this.labelCliente, Alignment.MIDDLE_CENTER);
		this.labelRiserva1.setWidth(100, Unit.PERCENTAGE);
		this.labelRiserva1.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelRiserva1);
		this.verticalLayout2.setComponentAlignment(this.labelRiserva1, Alignment.MIDDLE_CENTER);
		this.labelRiserva2.setWidth(100, Unit.PERCENTAGE);
		this.labelRiserva2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelRiserva2);
		this.verticalLayout2.setComponentAlignment(this.labelRiserva2, Alignment.MIDDLE_CENTER);
		this.labelRiserva3.setWidth(100, Unit.PERCENTAGE);
		this.labelRiserva3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelRiserva3);
		this.verticalLayout2.setComponentAlignment(this.labelRiserva3, Alignment.MIDDLE_CENTER);
		this.panel.setSizeFull();
		this.verticalLayout2.addComponent(this.panel);
		this.verticalLayout2.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.panel, 10.0F);
		this.panelAngolo.setSizeFull();
		this.verticalLayout2.addComponent(this.panelAngolo);
		this.verticalLayout2.setComponentAlignment(this.panelAngolo, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.panelAngolo, 10.0F);
		this.buttonAggiungiPreventivo.setSizeUndefined();
		this.verticalLayout2.addComponent(this.buttonAggiungiPreventivo);
		this.verticalLayout2.setComponentAlignment(this.buttonAggiungiPreventivo, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setSizeFull();
		this.horizontalLayout.addComponent(this.verticalLayout);
		this.horizontalLayout.setComponentAlignment(this.verticalLayout, Alignment.TOP_CENTER);
		this.horizontalLayout.setExpandRatio(this.verticalLayout, 30.0F);
		this.verticalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout2.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.verticalLayout2);
		this.horizontalLayout.setExpandRatio(this.verticalLayout2, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	
		this.comboBoxEsecuzioni.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				DimensionView.this.comboBoxEsecuzioni_valueChange(event);
			}
		});
		this.buttonAngoloMeno.addClickListener(event -> this.buttonAngoloMeno_buttonClick(event));
		this.buttonAngoloPiu.addClickListener(event -> this.buttonAngoloPiu_buttonClick(event));
		this.buttonCambiaOrientamento.addClickListener(event -> this.buttonCambiaOrientamento_buttonClick(event));
		this.buttonAggiungiPreventivo.addClickListener(event -> this.buttonAggiungiPreventivo_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel label6, label7, label10, label11, label12, label13, label14, label15, label5, label4, label16,
			label17, label18, label19, label20, label21, label3, label2, label22, label23, label24, label25, label26,
			label27, label8, label9, label28, label29, label30, label31, label32, label33, label34, label35, label36,
			label37, label38, label39, label40, label41, labelPD2, labelPeso, labelTitolo, labelmm, labelCliente,
			labelRiserva1, labelRiserva2, labelRiserva3, labelOriReq, labelOriReqAngolo, labelEsecuzione, labelDescrizione,
			labelAngolo, labelOrientamento;
	private XdevButton buttonAngoloMeno, buttonAngoloPiu, buttonCambiaOrientamento, buttonAggiungiPreventivo;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout3, horizontalLayout2;
	private XdevImage imageDefault, imageLogo;
	private XdevPanel panelForImage, panel, panelAngolo;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout, verticalLayout5, verticalLayout2, verticalLayout4, verticalLayout3;
	private XdevComboBox<CustomComponent> comboBoxEsecuzioni;
	// </generated-code>


}
