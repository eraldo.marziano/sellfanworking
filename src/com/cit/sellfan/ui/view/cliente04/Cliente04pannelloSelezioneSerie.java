package com.cit.sellfan.ui.view.cliente04;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.pannelli.pannelloSelezioneSerieDefault;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;

import cit.sellfan.classi.Serie;
import cit.sellfan.classi.UtilityCliente;

public class Cliente04pannelloSelezioneSerie extends Panel {
	private VariabiliGlobali l_VG;
	@SuppressWarnings("unused")
	private final Container containerSerie = new IndexedContainer();
	private boolean editEnabled;
    private String[] EsplicitaDAEsplicita = null;
    private String[] EsplicitaTREsplicita = null;
    private String[] CentrifughiDA = null;
    private String[] CentrifughiDAAriaPulitaBassa = null;
    private String[] CentrifughiDAAriaPulitaAlta = null;
    private String[] CentrifughiDAAriaPolverosa = null;
    private String[] CentrifughiDAAriaMoltoPolverosa = null;
    private String[] CentrifughiDAMaterialeSolido = null;
    private String[] CentrifughiTR = null;
    private String[] CentrifughiTRAriaPulitaBassa = null;
    private String[] CentrifughiTRAriaPulitaAlta = null;
    private String[] CentrifughiTRAriaPolverosa = null;
    private String[] CentrifughiTRAriaMoltoPolverosa = null;
    private String[] CentrifughiTRMaterialeSolido = null;
    private String[] AssialiDA = null;
    private String[] AssialiTR = null;
    private String[] TorriniDA = null;
    private pannelloSelezioneSerieDefault pannelloSerie;

	/**
	 * 
	 */
	public Cliente04pannelloSelezioneSerie() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		Nazionalizza();
	}
	
	public void Nazionalizza() {
		this.labelTipologiaVentilatore.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tipologia ventilatore"));
		this.checkBoxAssiali.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Assiali"));
		this.checkBoxCentrifughi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Centrifughi"));
		this.checkBoxTorrini.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Torrini"));
		this.checkBoxTutti.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Selezione esplicita"));
		this.checkBoxDA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Direttamente accoppiati"));
		this.checkBoxTR.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Trasmissione"));

		this.labelFluido.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Fluido"));
		this.checkBoxAriaPulitaBassa.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aria pulita medio bassa pressione"));
		this.checkBoxAriaPulitaAlta.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aria pulita alta pressione"));
		this.checkBoxAriaPolverosa.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aria poco polverosa"));
		this.checkBoxAriaMoltoPolverosa.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aria molto polverosa"));
		this.checkBoxMaterialeSolido.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Materiale solido"));
		this.checkBoxSelezioneEsplicita.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Selezione esplicita"));
		
		this.checkGruppo0.setCaption(this.l_VG.ElencoGruppiSelezioneSerie[0].descrizione);
		this.checkGruppo1.setCaption(this.l_VG.ElencoGruppiSelezioneSerie[1].descrizione);
		this.checkGruppo2.setCaption(this.l_VG.ElencoGruppiSelezioneSerie[2].descrizione);
		this.checkGruppo3.setCaption(this.l_VG.ElencoGruppiSelezioneSerie[3].descrizione);
	}
	
	@SuppressWarnings("unused")
	public void init(final boolean TRFlag, final boolean AssialiFlag, final boolean CentrifighiFlag) {
        final UtilityCliente uc04 = this.l_VG.utilityCliente;
        this.checkGruppo0.setVisible(this.l_VG.ElencoGruppiSelezioneSerie[0].elencoSerie != null && this.l_VG.ElencoGruppiSelezioneSerie[0].elencoSerie.length > 0);
        this.checkGruppo1.setVisible(this.l_VG.ElencoGruppiSelezioneSerie[1].elencoSerie != null && this.l_VG.ElencoGruppiSelezioneSerie[1].elencoSerie.length > 0);
        this.checkGruppo2.setVisible(this.l_VG.ElencoGruppiSelezioneSerie[2].elencoSerie != null && this.l_VG.ElencoGruppiSelezioneSerie[2].elencoSerie.length > 0);
        this.checkGruppo3.setVisible(this.l_VG.ElencoGruppiSelezioneSerie[3].elencoSerie != null && this.l_VG.ElencoGruppiSelezioneSerie[3].elencoSerie.length > 0);
        if (this.EsplicitaDAEsplicita == null) {
            try {
                final Statement stmt = this.l_VG.utilityCliente.ConnCliente.createStatement();
                String query = "select * from " + this.l_VG.dbTableQualifier.dbTableQualifierPreDBUser + this.l_VG.utilityCliente.idCliente + "Categorie" + this.l_VG.dbTableQualifier.dbTableQualifierPostDBUser;
                query += " where Codice = '";
                this.EsplicitaDAEsplicita = loadArray(stmt, query, "EsplicitaDAEsplicita");
                this.EsplicitaTREsplicita = loadArray(stmt, query, "EsplicitaTREsplicita");
                this.TorriniDA = loadArray(stmt, query, "TorriniDA");
                this.CentrifughiDA = loadArray(stmt, query, "CentrifughiDA");
                this.CentrifughiDAAriaPulitaBassa = loadArray(stmt, query, "CentrifughiDAAriaPulitaBassa");
                this.CentrifughiDAAriaPulitaAlta = loadArray(stmt, query, "CentrifughiDAAriaPulitaAlta");
                this.CentrifughiDAAriaPolverosa = loadArray(stmt, query, "CentrifughiDAAriaPolverosa");
                this.CentrifughiDAAriaMoltoPolverosa = loadArray(stmt, query, "CentrifughiDAAriaMoltoPolverosa");
                this.CentrifughiDAMaterialeSolido = loadArray(stmt, query, "CentrifughiDAMaterialeSolido");
                this.CentrifughiTR = loadArray(stmt, query, "CentrifughiTR");
                this.CentrifughiTRAriaPulitaBassa = loadArray(stmt, query, "CentrifughiTRAriaPulitaBassa");
                this.CentrifughiTRAriaPulitaAlta = loadArray(stmt, query, "CentrifughiTRAriaPulitaAlta");
                this.CentrifughiTRAriaPolverosa = loadArray(stmt, query, "CentrifughiTRAriaPolverosa");
                this.CentrifughiTRAriaMoltoPolverosa = loadArray(stmt, query, "CentrifughiTRAriaMoltoPolverosa");
                this.CentrifughiTRMaterialeSolido = loadArray(stmt, query, "CentrifughiTRMaterialeSolido");
                this.AssialiDA = loadArray(stmt, query, "AssialiDA");
                this.AssialiTR = loadArray(stmt, query, "AssialiTR");
                stmt.close();
                this.editEnabled = false;
                this.checkBoxTR.setValue(TRFlag);
                this.checkBoxDA.setValue(!TRFlag);
                if (AssialiFlag && CentrifighiFlag) {
                	this.checkBoxTutti.setValue(true);
                } else if (AssialiFlag) {
                	this.checkBoxAssiali.setValue(true);
                } else if (CentrifighiFlag) {
                	this.checkBoxCentrifughi.setValue(true);
                } else {
                	this.checkBoxTutti.setValue(true);
                }
                abilitaPallini();
                buildTabellaSerie(TRFlag);
                this.editEnabled = true;
            } catch (final Exception e) {
                
            }
        }
	}
	
	public void aggiornaSerieUtilizzabiliRicerca() {
		this.pannelloSerie.aggiornaSerieUtilizzabiliRicerca();
	}
	
	public boolean getTRFlag() {
		return this.checkBoxTR.getValue();
	}
	
	public boolean getAssialiFlag() {
		if (this.checkBoxTutti.getValue()) {
			return true;
		}
		return this.checkBoxAssiali.getValue();
	}
	
	public boolean getCentrifughiFlag() {
		if (this.checkBoxTutti.getValue()) {
			return true;
		}
		return this.checkBoxCentrifughi.getValue();
	}
	
	private void buildTabellaSerie(final Boolean TRFlag) {
        String elenco[] = null;
        if (this.checkGruppo0.getValue()) {
        	elenco = this.l_VG.ElencoGruppiSelezioneSerie[0].elencoSerie;
        } else if (this.checkGruppo1.getValue()) {
           	elenco = this.l_VG.ElencoGruppiSelezioneSerie[1].elencoSerie;
        } else if (this.checkGruppo2.getValue()) {
           	elenco = this.l_VG.ElencoGruppiSelezioneSerie[2].elencoSerie;
        } else if (this.checkGruppo3.getValue()) {
           	elenco = this.l_VG.ElencoGruppiSelezioneSerie[3].elencoSerie;
        } else if (this.checkBoxTutti.getValue()) {
            if (this.checkBoxDA.getValue()) {
                elenco = this.EsplicitaDAEsplicita;
            } else {
                elenco = this.EsplicitaTREsplicita;
            }
        } else if (this.checkBoxTorrini.getValue()) {
            elenco = this.TorriniDA;
        } else if (this.checkBoxCentrifughi.getValue()) {
            if (this.checkBoxDA.getValue()) {
                if (this.checkBoxAriaPulitaBassa.getValue()) {
                    elenco = this.CentrifughiDAAriaPulitaBassa;
                } else if (this.checkBoxAriaPulitaAlta.getValue()) {
                    elenco = this.CentrifughiDAAriaPulitaAlta;
                } else if (this.checkBoxAriaPolverosa.getValue()) {
                    elenco = this.CentrifughiDAAriaPolverosa;
                } else if (this.checkBoxAriaMoltoPolverosa.getValue()) {
                    elenco = this.CentrifughiDAAriaMoltoPolverosa;
                } else if (this.checkBoxMaterialeSolido.getValue()) {
                    elenco = this.CentrifughiDAMaterialeSolido;
                } else {    //tutte
                    elenco = this.CentrifughiDA;
                }
            } else {
                if (this.checkBoxAriaPulitaBassa.getValue()) {
                    elenco = this.CentrifughiTRAriaPulitaBassa;
                } else if (this.checkBoxAriaPulitaAlta.getValue()) {
                    elenco = this.CentrifughiTRAriaPulitaAlta;
                } else if (this.checkBoxAriaPolverosa.getValue()) {
                    elenco = this.CentrifughiTRAriaPolverosa;
                } else if (this.checkBoxAriaMoltoPolverosa.getValue()) {
                    elenco = this.CentrifughiTRAriaMoltoPolverosa;
                } else if (this.checkBoxMaterialeSolido.getValue()) {
                    elenco = this.CentrifughiTRMaterialeSolido;
                } else {    //tutte
                    elenco = this.CentrifughiTR;
                }
            }
        } else if (this.checkBoxAssiali.getValue()) {
            if (this.checkBoxDA.getValue()) {
                elenco = this.AssialiDA;
            } else {
                elenco = this.AssialiTR;
            }
        }
		this.gridLayout2.removeComponent(0, 0);
		this.pannelloSerie = new pannelloSelezioneSerieDefault();
		this.pannelloSerie.setVariabiliGlobali(this.l_VG);
		this.pannelloSerie.Nazionalizza();
		this.pannelloSerie.hideTRDA();
		final ArrayList<Serie> elencoSerieToUse = new ArrayList<>();
		for (int i=0 ; i<elenco.length ; i++) {
			for (int j=0 ; j<this.l_VG.ElencoSerie.size() ; j++) {
				if (elenco[i].equals(this.l_VG.ElencoSerie.get(j).Serie)) {
					elencoSerieToUse.add(this.l_VG.ElencoSerie.get(j));
					break;
				}
			}
		}
		if (elenco != null && elenco.length > 0) {
			this.pannelloSerie.init(TRFlag, elencoSerieToUse);
		}
		this.pannelloSerie.setSizeFull();
		this.gridLayout2.addComponent(this.pannelloSerie, 0, 0);
		this.gridLayout2.setComponentAlignment(this.pannelloSerie, Alignment.MIDDLE_CENTER);
	}

    private String[] loadArray(final Statement stmt, final String query, final String nomeCampo) {
        try {
            final ResultSet rs = stmt.executeQuery(query + nomeCampo + "'");
            rs.next();
            final int NCampi = rs.getInt("N");
            final String retString[] = new String[NCampi];
            for (int i=0 ; i<NCampi ; i++) {
                retString[i] = rs.getString("P" + Integer.toString(i));
            }
            rs.close();
            return retString;
        } catch (final Exception e) {
            return null;
        }
    }

    private void abilitaPallini() {
    	if (this.editEnabled) {
			return;
		}
        if (this.checkBoxTutti.getValue()) {
        	this.checkBoxSelezioneEsplicita.setValue(true);
        	this.checkBoxAriaPulitaBassa.setEnabled(false);
        	this.checkBoxAriaPulitaAlta.setEnabled(false);
        	this.checkBoxAriaPolverosa.setEnabled(false);
        	this.checkBoxAriaMoltoPolverosa.setEnabled(false);
        	this.checkBoxMaterialeSolido.setEnabled(false);
        	this.checkBoxTR.setEnabled(true);
        } else if (this.checkBoxAssiali.getValue()) {
        	this.checkBoxSelezioneEsplicita.setValue(true);
        	this.checkBoxAriaPulitaBassa.setEnabled(false);
        	this.checkBoxAriaPulitaAlta.setEnabled(false);
        	this.checkBoxAriaPolverosa.setEnabled(false);
        	this.checkBoxAriaMoltoPolverosa.setEnabled(false);
        	this.checkBoxMaterialeSolido.setEnabled(false);
        	this.checkBoxTR.setEnabled(true);
        } else if (this.checkBoxCentrifughi.getValue()) {
        	this.checkBoxSelezioneEsplicita.setValue(true);
        	this.checkBoxAriaPulitaBassa.setEnabled(true);
        	this.checkBoxAriaPulitaAlta.setEnabled(true);
        	this.checkBoxAriaPolverosa.setEnabled(true);
        	this.checkBoxAriaMoltoPolverosa.setEnabled(true);
        	this.checkBoxMaterialeSolido.setEnabled(true);
        	this.checkBoxTR.setEnabled(true);
        } else if (this.checkBoxTorrini.getValue()) {
        	this.checkBoxSelezioneEsplicita.setValue(true);
        	this.checkBoxAriaPulitaBassa.setEnabled(false);
        	this.checkBoxAriaPulitaAlta.setEnabled(false);
        	this.checkBoxAriaPolverosa.setEnabled(false);
        	this.checkBoxAriaMoltoPolverosa.setEnabled(false);
        	this.checkBoxTR.setEnabled(false);
        	this.checkBoxDA.setValue(true);
            this.checkBoxTR.setValue(false);
        }
    }
	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxDA}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxDA_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxTR.setValue(false);
		buildTabellaSerie(false);
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTR}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTR_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxDA.setValue(false);
		buildTabellaSerie(true);
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAssiali}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAssiali_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxCentrifughi.setValue(false);
		this.checkBoxTorrini.setValue(false);
		this.checkBoxTutti.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCentrifughi}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCentrifughi_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAssiali.setValue(false);
		this.checkBoxTorrini.setValue(false);
		this.checkBoxTutti.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTorrini}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTorrini_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAssiali.setValue(false);
		this.checkBoxCentrifughi.setValue(false);
		this.checkBoxTutti.setValue(false);
        abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTutti}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTutti_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAssiali.setValue(false);
		this.checkBoxCentrifughi.setValue(false);
		this.checkBoxTorrini.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAriaPulitaBassa}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAriaPulitaBassa_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        //abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAriaPulitaAlta}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAriaPulitaAlta_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        //abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAriaPolverosa}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAriaPolverosa_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        //abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxAriaMoltoPolverosa}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxAriaMoltoPolverosa_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        //abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxMaterialeSolido}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxMaterialeSolido_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        //abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxSelezioneEsplicita}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxSelezioneEsplicita_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        //abilitaPallini();
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkGruppo0}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkGruppo0_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		//checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkGruppo1}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkGruppo1_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkGruppo0.setValue(false);
		//checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkGruppo2}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkGruppo2_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		//checkGruppo2.setValue(false);
		this.checkGruppo3.setValue(false);
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkGruppo3}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkGruppo3_valueChange(final Property.ValueChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.editEnabled = false;
		this.checkBoxSelezioneEsplicita.setValue(false);
		this.checkBoxAriaPulitaAlta.setValue(false);
		this.checkBoxAriaPulitaBassa.setValue(false);
		this.checkBoxAriaPolverosa.setValue(false);
		this.checkBoxAriaMoltoPolverosa.setValue(false);
		this.checkBoxMaterialeSolido.setValue(false);
		this.checkGruppo0.setValue(false);
		this.checkGruppo1.setValue(false);
		this.checkGruppo2.setValue(false);
		//checkGruppo3.setValue(false);
        buildTabellaSerie(this.checkBoxTR.getValue());
		this.editEnabled = true;
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.gridLayout = new XdevGridLayout();
		this.labelTipologiaVentilatore = new XdevLabel();
		this.checkBoxAssiali = new XdevCheckBox();
		this.checkBoxDA = new XdevCheckBox();
		this.checkBoxCentrifughi = new XdevCheckBox();
		this.checkBoxTR = new XdevCheckBox();
		this.checkBoxTorrini = new XdevCheckBox();
		this.checkBoxTutti = new XdevCheckBox();
		this.label = new XdevLabel();
		this.labelFluido = new XdevLabel();
		this.checkBoxSelezioneEsplicita = new XdevCheckBox();
		this.checkBoxAriaPulitaBassa = new XdevCheckBox();
		this.checkBoxAriaPulitaAlta = new XdevCheckBox();
		this.checkBoxAriaPolverosa = new XdevCheckBox();
		this.checkBoxAriaMoltoPolverosa = new XdevCheckBox();
		this.checkBoxMaterialeSolido = new XdevCheckBox();
		this.checkGruppo0 = new XdevCheckBox();
		this.checkGruppo1 = new XdevCheckBox();
		this.checkGruppo2 = new XdevCheckBox();
		this.checkGruppo3 = new XdevCheckBox();
		this.verticalLayout = new XdevVerticalLayout();
		this.gridLayout2 = new XdevGridLayout();
		this.panelToRemove = new XdevPanel();
	
		this.labelTipologiaVentilatore.setValue("Tipologia ventilatore");
		this.labelTipologiaVentilatore.setContentMode(ContentMode.HTML);
		this.checkBoxAssiali.setCaption("Assiali");
		this.checkBoxDA.setCaption("D. accoppianti");
		this.checkBoxCentrifughi.setCaption("Centrifughi");
		this.checkBoxTR.setCaption("Trasmissione");
		this.checkBoxTorrini.setCaption("Torrini");
		this.checkBoxTutti.setCaption("Selezione esplicita");
		this.labelFluido.setValue("Fluido");
		this.labelFluido.setContentMode(ContentMode.HTML);
		this.checkBoxSelezioneEsplicita.setCaption("Selezione esplicita");
		this.checkBoxAriaPulitaBassa.setCaption("Aria pulita medio bassa pressione");
		this.checkBoxAriaPulitaAlta.setCaption("Aria pulita alta pressione");
		this.checkBoxAriaPolverosa.setCaption("Aria poco polverosa");
		this.checkBoxAriaMoltoPolverosa.setCaption("Aria molto polverosa");
		this.checkBoxMaterialeSolido.setCaption("Materiale solido");
		this.checkGruppo0.setCaption("Gruppo0");
		this.checkGruppo1.setCaption("Gruppo1");
		this.checkGruppo2.setCaption("Gruppo2");
		this.checkGruppo3.setCaption("Gruppo3");
		this.verticalLayout.setMargin(new MarginInfo(false, true, false, true));
		this.gridLayout2.setMargin(new MarginInfo(false));
	
		this.gridLayout.setColumns(2);
		this.gridLayout.setRows(18);
		this.labelTipologiaVentilatore.setSizeUndefined();
		this.gridLayout.addComponent(this.labelTipologiaVentilatore, 0, 0, 1, 0);
		this.gridLayout.setComponentAlignment(this.labelTipologiaVentilatore, Alignment.TOP_CENTER);
		this.checkBoxAssiali.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxAssiali, 0, 1);
		this.checkBoxDA.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxDA, 1, 1);
		this.checkBoxCentrifughi.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxCentrifughi, 0, 2);
		this.checkBoxTR.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxTR, 1, 2);
		this.checkBoxTorrini.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxTorrini, 0, 3);
		this.checkBoxTutti.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxTutti, 0, 4);
		this.label.setWidth(-1, Unit.PIXELS);
		this.label.setHeight(18, Unit.PIXELS);
		this.gridLayout.addComponent(this.label, 0, 5);
		this.labelFluido.setSizeUndefined();
		this.gridLayout.addComponent(this.labelFluido, 0, 6);
		this.gridLayout.setComponentAlignment(this.labelFluido, Alignment.MIDDLE_CENTER);
		this.checkBoxSelezioneEsplicita.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxSelezioneEsplicita, 0, 7);
		this.checkBoxAriaPulitaBassa.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxAriaPulitaBassa, 0, 8);
		this.checkBoxAriaPulitaAlta.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxAriaPulitaAlta, 0, 9);
		this.checkBoxAriaPolverosa.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxAriaPolverosa, 0, 10);
		this.checkBoxAriaMoltoPolverosa.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxAriaMoltoPolverosa, 0, 11);
		this.checkBoxMaterialeSolido.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxMaterialeSolido, 0, 12);
		this.checkGruppo0.setSizeUndefined();
		this.gridLayout.addComponent(this.checkGruppo0, 0, 13);
		this.checkGruppo1.setSizeUndefined();
		this.gridLayout.addComponent(this.checkGruppo1, 0, 14);
		this.checkGruppo2.setSizeUndefined();
		this.gridLayout.addComponent(this.checkGruppo2, 0, 15);
		this.checkGruppo3.setSizeUndefined();
		this.gridLayout.addComponent(this.checkGruppo3, 0, 16);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 17, 1, 17);
		this.gridLayout.setRowExpandRatio(17, 1.0F);
		this.gridLayout2.setColumns(1);
		this.gridLayout2.setRows(1);
		this.panelToRemove.setSizeFull();
		this.gridLayout2.addComponent(this.panelToRemove, 0, 0);
		this.gridLayout2.setColumnExpandRatio(0, 10.0F);
		this.gridLayout2.setRowExpandRatio(0, 10.0F);
		this.gridLayout2.setSizeFull();
		this.verticalLayout.addComponent(this.gridLayout2);
		this.verticalLayout.setComponentAlignment(this.gridLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.gridLayout2, 10.0F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.gridLayout);
		this.horizontalLayout.setExpandRatio(this.gridLayout, 10.0F);
		this.verticalLayout.setSizeFull();
		this.horizontalLayout.addComponent(this.verticalLayout);
		this.horizontalLayout.setExpandRatio(this.verticalLayout, 12.0F);
		this.horizontalLayout.setWidth(807, Unit.PIXELS);
		this.horizontalLayout.setHeight(549, Unit.PIXELS);
		this.setContent(this.horizontalLayout);
		this.setWidth(809, Unit.PIXELS);
		this.setHeight(550, Unit.PIXELS);
	
		this.checkBoxAssiali.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxAssiali_valueChange(event);
			}
		});
		this.checkBoxDA.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxDA_valueChange(event);
			}
		});
		this.checkBoxCentrifughi.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxCentrifughi_valueChange(event);
			}
		});
		this.checkBoxTR.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxTR_valueChange(event);
			}
		});
		this.checkBoxTorrini.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxTorrini_valueChange(event);
			}
		});
		this.checkBoxTutti.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxTutti_valueChange(event);
			}
		});
		this.checkBoxSelezioneEsplicita.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxSelezioneEsplicita_valueChange(event);
			}
		});
		this.checkBoxAriaPulitaBassa.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxAriaPulitaBassa_valueChange(event);
			}
		});
		this.checkBoxAriaPulitaAlta.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxAriaPulitaAlta_valueChange(event);
			}
		});
		this.checkBoxAriaPolverosa.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxAriaPolverosa_valueChange(event);
			}
		});
		this.checkBoxAriaMoltoPolverosa.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxAriaMoltoPolverosa_valueChange(event);
			}
		});
		this.checkBoxMaterialeSolido.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkBoxMaterialeSolido_valueChange(event);
			}
		});
		this.checkGruppo0.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkGruppo0_valueChange(event);
			}
		});
		this.checkGruppo1.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkGruppo1_valueChange(event);
			}
		});
		this.checkGruppo2.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkGruppo2_valueChange(event);
			}
		});
		this.checkGruppo3.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloSelezioneSerie.this.checkGruppo3_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelTipologiaVentilatore, label, labelFluido;
	private XdevHorizontalLayout horizontalLayout;
	private XdevCheckBox checkBoxAssiali, checkBoxDA, checkBoxCentrifughi, checkBoxTR, checkBoxTorrini, checkBoxTutti,
			checkBoxSelezioneEsplicita, checkBoxAriaPulitaBassa, checkBoxAriaPulitaAlta, checkBoxAriaPolverosa,
			checkBoxAriaMoltoPolverosa, checkBoxMaterialeSolido, checkGruppo0, checkGruppo1, checkGruppo2,
			checkGruppo3;
	private XdevPanel panelToRemove;
	private XdevGridLayout gridLayout, gridLayout2;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>

}
