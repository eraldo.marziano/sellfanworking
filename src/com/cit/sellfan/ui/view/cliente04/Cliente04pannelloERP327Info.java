package com.cit.sellfan.ui.view.cliente04;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;

import cit.sellfan.classi.titoli.StringheUNI;
import cit.utility.LettereGreche;

public class Cliente04pannelloERP327Info extends Panel {
	private VariabiliGlobali l_VG;

	/**
	 * 
	 */
	public Cliente04pannelloERP327Info() {
		super();
		this.initUI();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	public void Nazionalizza() {
		this.labelPortata.setValue(this.l_VG.utility.giuntaStringHtml(StringheUNI.PORTATA, ":"));
		this.labelPotenza.setValue(this.l_VG.utility.giuntaStringHtml(StringheUNI.POTENZAASSE, ":"));
		this.labelPotenzaI.setValue(this.l_VG.utility.giuntaStringHtml(StringheUNI.POTENZAESTERNA, ":"));
		this.labelPT.setValue(this.l_VG.utility.giuntaStringHtml(StringheUNI.PRESSIONETOTALE, ":"));
		this.labeletaC.setValue("<html><b><font color='blue'>" + LettereGreche.etaMinuscolo + "<sub>E</sub>" + ":</html>");
		this.labelN.setValue("<html><b><font color='blue'>" + "N:</html>");
		this.labeletaGirante.setValue("<html>" + LettereGreche.etaMinuscolo + "<sub>GT</sub>" + ":</html>");
		this.labeletaMotore.setValue("<html>" + LettereGreche.etaMinuscolo + "<sub>M</sub>:</html>");
		this.labeletaTrasmissione.setValue("<html>" + LettereGreche.etaMinuscolo + "<sub>Tr</sub>:</html>");
		this.labeletaCuscinetti.setValue("<html>" + LettereGreche.etaMinuscolo + "<sub>Cu</sub>:</html>");
		this.labelN_15.setValue("Factor eff. grade:");
		this.labelTipoProva.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tipo prova") + ":");
		this.labeleta_15.setValue("<html>" + LettereGreche.etaMinuscolo + "<sub>T</sub>" + " 2015:</html>");
	}
	
	public void setVentilatoreCurrent() {
		this.labelSerie.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", this.l_VG.utilityTraduzioni.TraduciStringa("Serie"), " ", this.l_VG.VentilatoreCurrent.SerieTrans));
		this.labelModello.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent)));
		this.labelRpm.setValue(this.l_VG.utility.giuntaStringHtml("<b><center>", Integer.toString(this.l_VG.VentilatoreCurrent.datiERP327.rpm), " [Rpm]"));
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(3);
        this.labelPortataVal.setValue(this.l_VG.utility.giuntaStringHtml(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.portataRendimentoMaxVentilatorem3h / 3600.), " ",  "[m<sup>3</sup>/s]"));
        this.l_VG.fmtNd.setMaximumFractionDigits(0);
        this.labelPotenzaVal.setValue(this.l_VG.utility.giuntaStringHtml(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.potRendimentoMaxW), " ",  "[W]"));
        this.labelPotenzaIVal.setValue(this.l_VG.utility.giuntaStringHtml(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.Pe), " ",  "[W]"));
        this.labelPTVal.setValue(this.l_VG.utility.giuntaStringHtml(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.ptRendimentoMaxMandataPa), " ",  "[Pa]"));
        if (!this.l_VG.VentilatoreCurrent.datiERP327.regolamentoEnabled) {
			this.labelNOcmpliance.setValue("<b><size='+1'>" + this.l_VG.VentilatoreCurrent.datiERP327.regolamentoEnabledDescrizione + "</html>");
			this.labeletaC.setVisible(false);
			this.labeletaCVal.setVisible(false);
			this.labelN.setVisible(false);
			this.labelNVal.setVisible(false);
			this.labelTipoProva.setVisible(false);
			this.labelTipoProvaVal.setVisible(false);
			this.labelN_15.setVisible(false);
			this.labelN_15Val.setVisible(false);
			this.labeleta_15.setVisible(false);
			this.labeleta_15Val.setVisible(false);
			this.labelRapS.setVisible(false);
			this.labelRapSVal.setVisible(false);
			this.labeletaGirante.setVisible(false);
			this.labeletaGiranteVal.setVisible(false);
			this.labeletaMotore.setVisible(false);
			this.labeletaMotoreVal.setVisible(false);
			this.labeletaTrasmissione.setVisible(false);
			this.labeletaTrasmissioneVal.setVisible(false);
			this.labeletaCuscinetti.setVisible(false);
			this.labeletaCuscinettiVal.setVisible(false);
			this.checkBoxInverter.setVisible(false);
			return;
		} else {
			this.labelNOcmpliance.setValue("<b><font color='red' size='+1'>No ERP compliance");
		}
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(1);
        this.labeletaCVal.setValue("<html><b><font color='blue'>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaComplessivaT * 100.) + " [%]</html>");
        this.labelNVal.setValue("<html><b><font color='blue'>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaComplessivaT * 100. - this.l_VG.VentilatoreCurrent.datiERP327.targetForTarocco2015) + "</html>");
        if (this.l_VG.VentilatoreCurrent.datiERP327.efficienzaComplessivaT < this.l_VG.VentilatoreCurrent.datiERP327.targetEstesoT2015) {
        	this.labelNOcmpliance.setVisible(true);
        } else {
        	this.labelNOcmpliance.setVisible(false);
        }
        this.labelTipoProvaVal.setValue(this.l_VG.VentilatoreCurrent.datiERP327.categoriaMisura + "/" + this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.VentilatoreCurrent.datiERP327.categoriaMisuraPressione));
        this.labelN_15Val.setValue(Integer.toString(this.l_VG.VentilatoreCurrent.datiERP327.NT2015));
        this.labeleta_15Val.setValue("<html>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.targetEstesoT2015 * 100.) + " [%]</html>");
        this.l_VG.fmtNd.setMaximumFractionDigits(4);
        this.labelRapSVal.setValue(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.rapportoS));
        this.l_VG.fmtNd.setMaximumFractionDigits(1);
        this.labeletaGiranteVal.setValue("<html>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaGiranteT * 100.) + " [%]</html>");
        this.labeletaMotoreVal.setValue("<html>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaMotore * 100.) + " [%]</html>");
        this.labeletaTrasmissioneVal.setValue("<html>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione * 100.) + " [%]</html>");
        this.labeletaCuscinettiVal.setValue("<html>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaCuscinetti * 100.) + " [%]</html>");
        boolean visibile = this.l_VG.VentilatoreCurrent.datiERP327.ventilatore327.Trasmissione && this.l_VG.VentilatoreCurrent.datiERP327.efficienzaTrasmissione < 1.;
        this.labeletaTrasmissione.setVisible(visibile);
        this.labeletaTrasmissioneVal.setVisible(visibile);
        visibile = this.l_VG.VentilatoreCurrent.datiERP327.ventilatore327.Trasmissione && this.l_VG.VentilatoreCurrent.datiERP327.efficienzaCuscinetti < 1.;
        this.labeletaCuscinetti.setVisible(visibile);
        this.labeletaCuscinettiVal.setVisible(visibile);
		this.checkBoxInverter.setEnabled(false);
		this.checkBoxInverter.setValue(this.l_VG.VentilatoreCurrent.datiERP327.usaVariatore);
		this.checkBoxInverter.setEnabled(true);
	}
	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxInverter}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxInverter_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxInverter.isEnabled()) {
			return;
		}
		this.l_VG.VentilatoreCurrent.datiERP327.usaVariatore = this.checkBoxInverter.getValue();
        this.l_VG.regolamento327Algoritmo.setVariatore(this.l_VG.VentilatoreCurrent.datiERP327.usaVariatore);
        this.l_VG.gestioneAccessori.setAccessorioSelezionato(this.l_VG.VentilatoreCurrent, "INV", this.l_VG.VentilatoreCurrent.datiERP327.usaVariatore);
//        l_VG.regolamento327Algoritmo.set(dati);
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(1);
        this.labeletaCVal.setValue("<html><b><font color='blue'>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaComplessivaT * 100.) + " [%]</html>");
        this.labelNVal.setValue("<html><b><font color='blue'>" + this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.datiERP327.efficienzaComplessivaT * 100. - this.l_VG.VentilatoreCurrent.datiERP327.targetForTarocco2015) + "</html>");
        if (this.l_VG.VentilatoreCurrent.datiERP327.efficienzaComplessivaT < this.l_VG.VentilatoreCurrent.datiERP327.targetEstesoT2015) {
        	this.labelNOcmpliance.setVisible(true);
        } else {
        	this.labelNOcmpliance.setVisible(false);
        }
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.gridLayout = new XdevGridLayout();
		this.labelSerie = new XdevLabel();
		this.labelModello = new XdevLabel();
		this.labelRpm = new XdevLabel();
		this.labelPortata = new XdevLabel();
		this.labelPortataVal = new XdevLabel();
		this.labelPotenza = new XdevLabel();
		this.labelPotenzaVal = new XdevLabel();
		this.labelPotenzaI = new XdevLabel();
		this.labelPotenzaIVal = new XdevLabel();
		this.labelPT = new XdevLabel();
		this.labelPTVal = new XdevLabel();
		this.labeletaC = new XdevLabel();
		this.labeletaCVal = new XdevLabel();
		this.labelN = new XdevLabel();
		this.labelNVal = new XdevLabel();
		this.labelNOcmpliance = new XdevLabel();
		this.labelTipoProva = new XdevLabel();
		this.labelTipoProvaVal = new XdevLabel();
		this.labelN_15 = new XdevLabel();
		this.labelN_15Val = new XdevLabel();
		this.labeleta_15 = new XdevLabel();
		this.labeleta_15Val = new XdevLabel();
		this.labelRapS = new XdevLabel();
		this.labelRapSVal = new XdevLabel();
		this.labeletaGirante = new XdevLabel();
		this.labeletaGiranteVal = new XdevLabel();
		this.labeletaMotore = new XdevLabel();
		this.labeletaMotoreVal = new XdevLabel();
		this.labeletaTrasmissione = new XdevLabel();
		this.labeletaTrasmissioneVal = new XdevLabel();
		this.labeletaCuscinetti = new XdevLabel();
		this.labeletaCuscinettiVal = new XdevLabel();
		this.checkBoxInverter = new XdevCheckBox();
	
		this.labelSerie.setValue("serie");
		this.labelSerie.setContentMode(ContentMode.HTML);
		this.labelModello.setValue("modello");
		this.labelModello.setContentMode(ContentMode.HTML);
		this.labelRpm.setValue("rpm");
		this.labelRpm.setContentMode(ContentMode.HTML);
		this.labelPortata.setValue("portata");
		this.labelPortata.setContentMode(ContentMode.HTML);
		this.labelPortataVal.setValue("qval");
		this.labelPortataVal.setContentMode(ContentMode.HTML);
		this.labelPotenza.setValue("potenza");
		this.labelPotenza.setContentMode(ContentMode.HTML);
		this.labelPotenzaVal.setValue("pval");
		this.labelPotenzaVal.setContentMode(ContentMode.HTML);
		this.labelPotenzaI.setValue("potenzaI");
		this.labelPotenzaI.setContentMode(ContentMode.HTML);
		this.labelPotenzaIVal.setValue("pival");
		this.labelPotenzaIVal.setContentMode(ContentMode.HTML);
		this.labelPT.setValue("PT");
		this.labelPT.setContentMode(ContentMode.HTML);
		this.labelPTVal.setValue("PTval");
		this.labelPTVal.setContentMode(ContentMode.HTML);
		this.labeletaC.setValue("etaC");
		this.labeletaC.setContentMode(ContentMode.HTML);
		this.labeletaCVal.setValue("rtaCval");
		this.labeletaCVal.setContentMode(ContentMode.HTML);
		this.labelN.setValue("N");
		this.labelN.setContentMode(ContentMode.HTML);
		this.labelNVal.setValue("n");
		this.labelNVal.setContentMode(ContentMode.HTML);
		this.labelNOcmpliance.setValue("<b><font color='red' size='+1'>No ERP compliance");
		this.labelNOcmpliance.setContentMode(ContentMode.HTML);
		this.labelTipoProva.setValue("tipo prova");
		this.labelTipoProvaVal.setValue("prova");
		this.labelN_15.setValue("N15");
		this.labelN_15Val.setValue("n15");
		this.labeleta_15.setValue("eta15");
		this.labeleta_15.setContentMode(ContentMode.HTML);
		this.labeleta_15Val.setValue("eta 15");
		this.labeleta_15Val.setContentMode(ContentMode.HTML);
		this.labelRapS.setValue("Rap S.:");
		this.labelRapSVal.setValue("raps");
		this.labeletaGirante.setValue("etagirante");
		this.labeletaGirante.setContentMode(ContentMode.HTML);
		this.labeletaGiranteVal.setValue("etaval");
		this.labeletaGiranteVal.setContentMode(ContentMode.HTML);
		this.labeletaMotore.setValue("etamotore");
		this.labeletaMotore.setContentMode(ContentMode.HTML);
		this.labeletaMotoreVal.setValue("etval");
		this.labeletaMotoreVal.setContentMode(ContentMode.HTML);
		this.labeletaTrasmissione.setValue("etatrasm");
		this.labeletaTrasmissione.setContentMode(ContentMode.HTML);
		this.labeletaTrasmissioneVal.setValue("etaval");
		this.labeletaTrasmissioneVal.setContentMode(ContentMode.HTML);
		this.labeletaCuscinetti.setValue("cuscinetti");
		this.labeletaCuscinetti.setContentMode(ContentMode.HTML);
		this.labeletaCuscinettiVal.setValue("etaval");
		this.labeletaCuscinettiVal.setContentMode(ContentMode.HTML);
		this.checkBoxInverter.setCaption("Inverter Enabled");
	
		this.gridLayout.setColumns(2);
		this.gridLayout.setRows(20);
		this.labelSerie.setSizeUndefined();
		this.gridLayout.addComponent(this.labelSerie, 0, 0, 1, 0);
		this.gridLayout.setComponentAlignment(this.labelSerie, Alignment.TOP_CENTER);
		this.labelModello.setSizeUndefined();
		this.gridLayout.addComponent(this.labelModello, 0, 1, 1, 1);
		this.gridLayout.setComponentAlignment(this.labelModello, Alignment.TOP_CENTER);
		this.labelRpm.setSizeUndefined();
		this.gridLayout.addComponent(this.labelRpm, 0, 2, 1, 2);
		this.gridLayout.setComponentAlignment(this.labelRpm, Alignment.TOP_CENTER);
		this.labelPortata.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPortata, 0, 3);
		this.gridLayout.setComponentAlignment(this.labelPortata, Alignment.TOP_RIGHT);
		this.labelPortataVal.setWidth(100, Unit.PERCENTAGE);
		this.labelPortataVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelPortataVal, 1, 3);
		this.labelPotenza.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPotenza, 0, 4);
		this.gridLayout.setComponentAlignment(this.labelPotenza, Alignment.TOP_RIGHT);
		this.labelPotenzaVal.setWidth(100, Unit.PERCENTAGE);
		this.labelPotenzaVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelPotenzaVal, 1, 4);
		this.labelPotenzaI.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPotenzaI, 0, 5);
		this.gridLayout.setComponentAlignment(this.labelPotenzaI, Alignment.TOP_RIGHT);
		this.labelPotenzaIVal.setWidth(100, Unit.PERCENTAGE);
		this.labelPotenzaIVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelPotenzaIVal, 1, 5);
		this.labelPT.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPT, 0, 6);
		this.gridLayout.setComponentAlignment(this.labelPT, Alignment.TOP_RIGHT);
		this.labelPTVal.setSizeUndefined();
		this.gridLayout.addComponent(this.labelPTVal, 1, 6);
		this.labeletaC.setSizeUndefined();
		this.gridLayout.addComponent(this.labeletaC, 0, 7);
		this.gridLayout.setComponentAlignment(this.labeletaC, Alignment.TOP_RIGHT);
		this.labeletaCVal.setWidth(100, Unit.PERCENTAGE);
		this.labeletaCVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labeletaCVal, 1, 7);
		this.labelN.setSizeUndefined();
		this.gridLayout.addComponent(this.labelN, 0, 8);
		this.gridLayout.setComponentAlignment(this.labelN, Alignment.TOP_RIGHT);
		this.labelNVal.setWidth(100, Unit.PERCENTAGE);
		this.labelNVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelNVal, 1, 8);
		this.labelNOcmpliance.setSizeUndefined();
		this.gridLayout.addComponent(this.labelNOcmpliance, 0, 9, 1, 9);
		this.gridLayout.setComponentAlignment(this.labelNOcmpliance, Alignment.TOP_CENTER);
		this.labelTipoProva.setSizeUndefined();
		this.gridLayout.addComponent(this.labelTipoProva, 0, 10);
		this.gridLayout.setComponentAlignment(this.labelTipoProva, Alignment.TOP_RIGHT);
		this.labelTipoProvaVal.setWidth(100, Unit.PERCENTAGE);
		this.labelTipoProvaVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelTipoProvaVal, 1, 10);
		this.labelN_15.setSizeUndefined();
		this.gridLayout.addComponent(this.labelN_15, 0, 11);
		this.gridLayout.setComponentAlignment(this.labelN_15, Alignment.TOP_RIGHT);
		this.labelN_15Val.setWidth(100, Unit.PERCENTAGE);
		this.labelN_15Val.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelN_15Val, 1, 11);
		this.labeleta_15.setSizeUndefined();
		this.gridLayout.addComponent(this.labeleta_15, 0, 12);
		this.gridLayout.setComponentAlignment(this.labeleta_15, Alignment.TOP_RIGHT);
		this.labeleta_15Val.setWidth(100, Unit.PERCENTAGE);
		this.labeleta_15Val.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labeleta_15Val, 1, 12);
		this.labelRapS.setSizeUndefined();
		this.gridLayout.addComponent(this.labelRapS, 0, 13);
		this.gridLayout.setComponentAlignment(this.labelRapS, Alignment.TOP_RIGHT);
		this.labelRapSVal.setWidth(100, Unit.PERCENTAGE);
		this.labelRapSVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labelRapSVal, 1, 13);
		this.labeletaGirante.setSizeUndefined();
		this.gridLayout.addComponent(this.labeletaGirante, 0, 14);
		this.gridLayout.setComponentAlignment(this.labeletaGirante, Alignment.TOP_RIGHT);
		this.labeletaGiranteVal.setWidth(100, Unit.PERCENTAGE);
		this.labeletaGiranteVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labeletaGiranteVal, 1, 14);
		this.labeletaMotore.setSizeUndefined();
		this.gridLayout.addComponent(this.labeletaMotore, 0, 15);
		this.gridLayout.setComponentAlignment(this.labeletaMotore, Alignment.TOP_RIGHT);
		this.labeletaMotoreVal.setWidth(100, Unit.PERCENTAGE);
		this.labeletaMotoreVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labeletaMotoreVal, 1, 15);
		this.labeletaTrasmissione.setSizeUndefined();
		this.gridLayout.addComponent(this.labeletaTrasmissione, 0, 16);
		this.gridLayout.setComponentAlignment(this.labeletaTrasmissione, Alignment.TOP_RIGHT);
		this.labeletaTrasmissioneVal.setWidth(100, Unit.PERCENTAGE);
		this.labeletaTrasmissioneVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labeletaTrasmissioneVal, 1, 16);
		this.labeletaCuscinetti.setSizeUndefined();
		this.gridLayout.addComponent(this.labeletaCuscinetti, 0, 17);
		this.gridLayout.setComponentAlignment(this.labeletaCuscinetti, Alignment.TOP_RIGHT);
		this.labeletaCuscinettiVal.setWidth(100, Unit.PERCENTAGE);
		this.labeletaCuscinettiVal.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.labeletaCuscinettiVal, 1, 17);
		this.checkBoxInverter.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxInverter, 0, 18, 1, 18);
		this.gridLayout.setComponentAlignment(this.checkBoxInverter, Alignment.TOP_CENTER);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 20.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 19, 1, 19);
		this.gridLayout.setRowExpandRatio(19, 1.0F);
		this.gridLayout.setWidth(320, Unit.PIXELS);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.setContent(this.gridLayout);
		this.setWidth(320, Unit.PIXELS);
		this.setHeight(100, Unit.PERCENTAGE);
	
		this.checkBoxInverter.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04pannelloERP327Info.this.checkBoxInverter_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelSerie, labelModello, labelRpm, labelPortata, labelPortataVal, labelPotenza, labelPotenzaVal, labelPotenzaI, labelPotenzaIVal, labelPT, labelPTVal, labeletaC,
			labeletaCVal, labelN, labelNVal, labelNOcmpliance, labelTipoProva, labelTipoProvaVal, labelN_15, labelN_15Val, labeleta_15, labeleta_15Val, labelRapS, labelRapSVal,
			labeletaGirante, labeletaGiranteVal, labeletaMotore, labeletaMotoreVal, labeletaTrasmissione, labeletaTrasmissioneVal, labeletaCuscinetti, labeletaCuscinettiVal;
	private XdevCheckBox checkBoxInverter;
	private XdevGridLayout gridLayout;
	// </generated-code>

}
