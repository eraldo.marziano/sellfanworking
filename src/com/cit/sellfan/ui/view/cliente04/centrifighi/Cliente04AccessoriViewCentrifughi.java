package com.cit.sellfan.ui.view.cliente04.centrifighi;

import cit.sellfan.classi.ParametriVari;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.view.AccessoriFreeView;
import com.cit.sellfan.ui.view.AccessoriImageView;
import com.cit.sellfan.ui.view.cliente04.Cliente04AccessoriView3Gruppi;
import com.cit.sellfan.ui.view.cliente04.Cliente04AccessoriView3GruppiMoro;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

public class Cliente04AccessoriViewCentrifughi extends XdevView {
	private VariabiliGlobali l_VG;
	private String modelloOld = "";
	private Cliente04AccessoriViewGruppiC0_1 accessoriView0 = new Cliente04AccessoriViewGruppiC0_1();
	private Cliente04AccessoriView3Gruppi accessoriView1 = new Cliente04AccessoriView3Gruppi();
	private Cliente04AccessoriView3Gruppi accessoriView2 = new Cliente04AccessoriView3Gruppi();
	private Cliente04AccessoriViewGruppoC9 accessoriView3 = new Cliente04AccessoriViewGruppoC9();
	private final Cliente04AccessoriViewSilenziatori accessoriSilenziatori = new Cliente04AccessoriViewSilenziatori();
	private final AccessoriImageView accessoriView4 = new AccessoriImageView();
	private final AccessoriImageView accessoriView5 = new AccessoriImageView();
	private final AccessoriFreeView accessoriFreeView = new AccessoriFreeView();
	private final String coloreBottone = "verde";
	private final String coloreBottoneSelezionato = "verdeselezionato";

	/**
	 * 
	 */
	public Cliente04AccessoriViewCentrifughi() {
		super();
		this.initUI();
		/*
		accessoriView0.setSizeFull();
		tabSheet.replaceComponent(panelTab0, accessoriView0);
		accessoriView1.setSizeFull();
		tabSheet.replaceComponent(panelTab1, accessoriView1);
		accessoriView2.setSizeFull();
		tabSheet.replaceComponent(panelTab2, accessoriView2);
		accessoriView3.setSizeFull();
		tabSheet.replaceComponent(panelTab3, accessoriView3);
		accessoriView4.setSizeFull();
		tabSheet.replaceComponent(panelTab4, accessoriView4);
		accessoriView5.setSizeFull();
		tabSheet.replaceComponent(panelTab5, accessoriView5);
		*/
	}
        
        public void togglePreventivoButton(boolean value) {
            accessoriView0.buttonAggiungiPreventivo.setEnabled(value);
        }
	
	public void refreshAccessori() {
		setVentilatoreCurrent();
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		if (this.l_VG == null) {
			return false;
		}
		if (this.l_VG.VentilatoreCurrent == null || this.l_VG.utilityCliente == null || !this.l_VG.utilityCliente.isTabVisible("Accessori")) {
			return false;
		}
		if (this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size() <= 0) {
			return false;
		}
		return true;
	}
	
	public void setVentilatoreCurrent() {
	    this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex] = this;
	    this.l_VG.ElencoMobileView[this.l_VG.AccessoriViewIndex] = null;
		this.accessoriView0.setVentilatoreCurrentCaratteristiche();
		this.accessoriView0.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(0).idGruppo, this.l_VG.elencoGruppiAccessori.get(1).idGruppo, this.l_VG.elencoGruppiAccessori.get(0).descrizioneGruppo, this.l_VG.elencoGruppiAccessori.get(1).descrizioneGruppo);
		this.accessoriView1.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(4).idGruppo, this.l_VG.elencoGruppiAccessori.get(5).idGruppo, this.l_VG.elencoGruppiAccessori.get(6).idGruppo, this.l_VG.elencoGruppiAccessori.get(4).descrizioneGruppo, this.l_VG.elencoGruppiAccessori.get(5).descrizioneGruppo, this.l_VG.elencoGruppiAccessori.get(6).descrizioneGruppo);
		this.accessoriView2.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(7).idGruppo, this.l_VG.elencoGruppiAccessori.get(8).idGruppo, "", this.l_VG.elencoGruppiAccessori.get(7).descrizioneGruppo, this.l_VG.elencoGruppiAccessori.get(8).descrizioneGruppo, "");
		this.accessoriView3.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(9).idGruppo, this.l_VG.elencoGruppiAccessori.get(9).descrizioneGruppo);
		this.accessoriView4.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(2).idGruppo, this.l_VG.elencoGruppiAccessori.get(2).descrizioneGruppo);
		this.accessoriView5.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(3).idGruppo, this.l_VG.elencoGruppiAccessori.get(3).descrizioneGruppo);
		this.accessoriSilenziatori.setVentilatoreCurrent("", "Silenziatori");
		this.accessoriFreeView.setVentilatoreCurrent();
		if (!this.l_VG.VentilatoreCurrent.ModelloTrans.equals(this.modelloOld)) {
			buttonTab0_buttonClick(null);
		}
		this.modelloOld = this.l_VG.VentilatoreCurrent.ModelloTrans;
	}
	
	public void Nazionalizza() {
		this.buttonTab0.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.elencoGruppiAccessori.get(0).titoloTabGruppo));
		this.buttonTab1.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.elencoGruppiAccessori.get(4).titoloTabGruppo));
		this.buttonTab2.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.elencoGruppiAccessori.get(7).titoloTabGruppo));
		this.buttonTab3.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.elencoGruppiAccessori.get(9).titoloTabGruppo));
		this.buttonTab4.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.elencoGruppiAccessori.get(2).titoloTabGruppo));
		this.buttonTab5.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.elencoGruppiAccessori.get(3).titoloTabGruppo));
		this.buttonSilenziatori.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatori"));
		this.buttonTab6.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Liberi"));
//		tabSheet.getTab(0).setCaption(l_VG.utilityTraduzioni.TraduciStringa(l_VG.elencoGruppiAccessori.get(0).titoloTabGruppo));
//		tabSheet.getTab(1).setCaption(l_VG.utilityTraduzioni.TraduciStringa(l_VG.elencoGruppiAccessori.get(4).titoloTabGruppo));
//		tabSheet.getTab(2).setCaption(l_VG.utilityTraduzioni.TraduciStringa(l_VG.elencoGruppiAccessori.get(7).titoloTabGruppo));
//		tabSheet.getTab(3).setCaption(l_VG.utilityTraduzioni.TraduciStringa(l_VG.elencoGruppiAccessori.get(9).titoloTabGruppo));
//		tabSheet.getTab(4).setCaption(l_VG.utilityTraduzioni.TraduciStringa(l_VG.elencoGruppiAccessori.get(2).titoloTabGruppo));
//		tabSheet.getTab(5).setCaption(l_VG.utilityTraduzioni.TraduciStringa(l_VG.elencoGruppiAccessori.get(3).titoloTabGruppo));
		this.accessoriView0.Nazionalizza();
		this.accessoriView1.Nazionalizza();
		this.accessoriView2.Nazionalizza();
		this.accessoriView3.Nazionalizza();
		this.accessoriView4.Nazionalizza();
		this.accessoriView5.Nazionalizza();
		this.accessoriSilenziatori.Nazionalizza();
		this.accessoriFreeView.Nazionalizza();
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
                if (this.l_VG.ParametriVari.clienteSpeciale == ParametriVari.Client.Moro) {
                    accessoriView0 = new Cliente04AccessoriViewGruppiC0_1Moro();
                    accessoriView1 = new Cliente04AccessoriView3GruppiMoro();
                    accessoriView2 = new Cliente04AccessoriView3GruppiMoro();                    
                }
		//l_VG.AccessoriView = this;
		this.accessoriView0.setVariabiliGlobali(this.l_VG);
		this.accessoriView1.setVariabiliGlobali(this.l_VG);
		this.accessoriView2.setVariabiliGlobali(this.l_VG);
		this.accessoriView3.setVariabiliGlobali(this.l_VG);
		this.accessoriView4.setVariabiliGlobali(this.l_VG);
		this.accessoriView5.setVariabiliGlobali(this.l_VG);
		this.accessoriSilenziatori.setVariabiliGlobali(this.l_VG);
		this.accessoriFreeView.setVariabiliGlobali(this.l_VG);
	}
	
	private void resetColoreSelezionato() {
		resetColoreSelezionato(this.buttonTab0);
		resetColoreSelezionato(this.buttonTab1);
		resetColoreSelezionato(this.buttonTab2);
		resetColoreSelezionato(this.buttonTab3);
		resetColoreSelezionato(this.buttonTab4);
		resetColoreSelezionato(this.buttonTab5);
		resetColoreSelezionato(this.buttonTab6);
		resetColoreSelezionato(this.buttonSilenziatori);
	}
	
	private void resetColoreSelezionato(final XdevButton bottone) {
		if (bottone.getStyleName().contains(this.coloreBottoneSelezionato)) {
			bottone.removeStyleName(this.coloreBottoneSelezionato);
			bottone.addStyleName(this.coloreBottone);
		}
	}
	
	private void buttonSilenziatori_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonSilenziatori.removeStyleName(this.coloreBottone);
		this.buttonSilenziatori.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
                this.accessoriSilenziatori.setVentilatoreCurrent();
		this.accessoriSilenziatori.setSizeFull();
		this.gridLayout.addComponent(this.accessoriSilenziatori, 0, 0);
	}
	
	
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab0}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab0_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab0.removeStyleName(this.coloreBottone);
		this.buttonTab0.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView0.setSizeFull();
		this.gridLayout.addComponent(this.accessoriView0, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab1}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab1_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab1.removeStyleName(this.coloreBottone);
		this.buttonTab1.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView1.setSizeFull();
                this.accessoriView1.setVentilatoreCurrent();
		this.gridLayout.addComponent(this.accessoriView1, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab2}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab2_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab2.removeStyleName(this.coloreBottone);
		this.buttonTab2.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView2.setSizeFull();
                this.accessoriView2.setVentilatoreCurrent();
		this.gridLayout.addComponent(this.accessoriView2, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab3}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab3_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab3.removeStyleName(this.coloreBottone);
		this.buttonTab3.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView3.setSizeFull();
                this.accessoriView3.setVentilatoreCurrent();
		this.gridLayout.addComponent(this.accessoriView3, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab4}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab4_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab4.removeStyleName(this.coloreBottone);
		this.buttonTab4.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView4.setSizeFull();
                this.accessoriView4.setVentilatoreCurrent();
		this.gridLayout.addComponent(this.accessoriView4, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab5}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab5_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab5.removeStyleName(this.coloreBottone);
		this.buttonTab5.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView5.setSizeFull();
                this.accessoriView5.setVentilatoreCurrent();
		this.gridLayout.addComponent(this.accessoriView5, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton} {@link #buttonTab6}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab6_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab6.removeStyleName(this.coloreBottone);
		this.buttonTab6.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriFreeView.setSizeFull();
                this.accessoriView1.setVentilatoreCurrent();
		this.gridLayout.addComponent(this.accessoriFreeView, 0, 0);
	
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.buttonTab0 = new XdevButton();
		this.buttonTab1 = new XdevButton();
		this.buttonTab2 = new XdevButton();
		this.buttonTab3 = new XdevButton();
		this.buttonTab4 = new XdevButton();
		this.buttonTab5 = new XdevButton();
		this.buttonTab6 = new XdevButton();
		this.buttonSilenziatori = new XdevButton();
		this.gridLayout = new XdevGridLayout();
		this.panelToRemove = new XdevPanel();
	
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.buttonTab0.setCaption("Tab0");
		this.buttonTab0.setStyleName("verde");
		this.buttonTab1.setCaption("Tab1");
		this.buttonTab1.setStyleName("verde");
		this.buttonTab2.setCaption("Tab2");
		this.buttonTab2.setStyleName("verde");
		this.buttonTab3.setCaption("Tab3");
		this.buttonTab3.setStyleName("verde");
		this.buttonTab4.setCaption("Tab4");
		this.buttonTab4.setStyleName("verde");
		this.buttonTab5.setCaption("Tab5");
		this.buttonTab5.setStyleName("verde");
		this.buttonTab6.setCaption("Tab6");
		this.buttonTab6.setStyleName("verde");
		this.buttonSilenziatori.setCaption("Silenziatori");
		this.buttonSilenziatori.setStyleName("verde");
		this.gridLayout.setMargin(new MarginInfo(false));
		this.panelToRemove.setTabIndex(0);
	
		this.buttonTab0.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab0);
		this.horizontalLayout.setComponentAlignment(this.buttonTab0, Alignment.MIDDLE_CENTER);
		this.buttonTab1.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab1);
		this.horizontalLayout.setComponentAlignment(this.buttonTab1, Alignment.MIDDLE_CENTER);
		this.buttonTab2.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab2);
		this.horizontalLayout.setComponentAlignment(this.buttonTab2, Alignment.MIDDLE_CENTER);
		this.buttonTab3.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab3);
		this.horizontalLayout.setComponentAlignment(this.buttonTab3, Alignment.MIDDLE_CENTER);
		this.buttonTab4.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab4);
		this.horizontalLayout.setComponentAlignment(this.buttonTab4, Alignment.MIDDLE_CENTER);
		this.buttonTab5.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab5);
		this.horizontalLayout.setComponentAlignment(this.buttonTab5, Alignment.MIDDLE_CENTER);
		this.buttonSilenziatori.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonSilenziatori);
		this.horizontalLayout.setComponentAlignment(this.buttonSilenziatori, Alignment.MIDDLE_CENTER);
		this.buttonTab6.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab6);
		this.horizontalLayout.setComponentAlignment(this.buttonTab6, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout_spacer = new CustomComponent();
		horizontalLayout_spacer.setSizeFull();
		this.horizontalLayout.addComponent(horizontalLayout_spacer);
		this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
		this.gridLayout.setColumns(1);
		this.gridLayout.setRows(1);
		this.panelToRemove.setSizeFull();
		this.gridLayout.addComponent(this.panelToRemove, 0, 0);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setRowExpandRatio(0, 10.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(40, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.gridLayout.setSizeFull();
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.gridLayout, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.buttonTab0.addClickListener(event -> this.buttonTab0_buttonClick(event));
		this.buttonTab1.addClickListener(event -> this.buttonTab1_buttonClick(event));
		this.buttonTab2.addClickListener(event -> this.buttonTab2_buttonClick(event));
		this.buttonTab3.addClickListener(event -> this.buttonTab3_buttonClick(event));
		this.buttonTab4.addClickListener(event -> this.buttonTab4_buttonClick(event));
		this.buttonTab5.addClickListener(event -> this.buttonTab5_buttonClick(event));
		this.buttonTab6.addClickListener(event -> this.buttonTab6_buttonClick(event));
		this.buttonSilenziatori.addClickListener(event -> this.buttonSilenziatori_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonTab0, buttonTab1, buttonTab2, buttonTab3, buttonTab4, buttonTab5, buttonTab6, buttonSilenziatori;
	private XdevHorizontalLayout horizontalLayout;
	private XdevPanel panelToRemove;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>


}
