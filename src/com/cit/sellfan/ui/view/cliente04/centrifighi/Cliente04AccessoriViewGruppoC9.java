package com.cit.sellfan.ui.view.cliente04.centrifighi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.DimensionView;
import com.cit.sellfan.ui.view.cliente04.Cliente04GestioneSilenziatori;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.Box;
import cit.sellfan.classi.Silenziatore;
import com.cit.sellfan.ui.utility.Accessori;
import com.cit.sellfan.ui.utility.TableProperties;
import de.steinwedel.messagebox.MessageBox;

public class Cliente04AccessoriViewGruppoC9 extends XdevView {
	private VariabiliGlobali l_VG;
	private StreamResource streamImageVuota = null;
	private final ArrayList<XdevLabel> elencoParametri = new ArrayList<>();
	private Container containerAccessori = new IndexedContainer();
	private Container containerSilenziatori = new IndexedContainer();
	private final int nMaxAccessori = 16;
	private String idGruppo;
	private String descrizioneGruppo;
	private Cliente04GestioneSilenziatori silenziatori;
	private Silenziatore silenziatoreMandata;
	private Silenziatore silenziatoreAspirazione;
	private Box selectedBox;
        private Accessori accessori = new Accessori();

	/**
	 * 
	 */
	public Cliente04AccessoriViewGruppoC9() {
		super();
		this.initUI();
	}

	public void Nazionalizza() {
		this.labelDatimm.setValue("<b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Dati in mm"));
		this.labelDenominazioneCliente.setValue("<b><center><font size='+1'>" + this.l_VG.utilityCliente.DenominazioneClienteSRL);
		this.labelSiRiserva0.setValue("<center>" + this.l_VG.utilityTraduzioni.TraduciStringa("si riserva"));
		this.labelSiRiserva1.setValue("<center>" + this.l_VG.utilityTraduzioni.TraduciStringa("il diritto di modificare"));
		this.labelSiRiserva2.setValue("<center>" + this.l_VG.utilityTraduzioni.TraduciStringa("i dati tecnici senza") + " " + this.l_VG.utilityTraduzioni.TraduciStringa("preavviso."));
                for (TableProperties tp : accessori.getListaProprieta()) {
                    this.tableAccessori.setColumnHeader(tp.propertyId, tp.traduzione);
                    this.tableAccessori.setColumnHeader(tp.propertyId, tp.traduzione);
                }
		this.tableSilenziatori.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatori"));
		this.buttonPickBox.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Scegli Box"));
		this.buttonPickSilencerInlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Aspirazione"));
		this.buttonPickSilencerOutlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Mandata"));
	}

	private void initContainerAccessori() {
            this.containerAccessori = new IndexedContainer();
            this.containerAccessori.removeAllItems();
            for (TableProperties tp : accessori.getListaProprieta()) {
                this.containerAccessori.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
                this.tableAccessori.setColumnHeader(tp.propertyId, tp.traduzione);
                this.tableAccessori.setColumnExpandRatio(tp.propertyId, tp.larghezza);
            }
            this.tableAccessori.setContainerDataSource(this.containerAccessori);
        }
	
        public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
                accessori.setVariabiliGlobali(l_VG);
            
		this.silenziatori = new Cliente04GestioneSilenziatori(this.l_VG);
		try {
			if (this.l_VG.logostreamresource != null) {
				this.imageLogo.setSource(this.l_VG.logostreamresource);
			}
			this.streamImageVuota = this.l_VG.getStreamResourceForImageFile(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "sfondoaccessori.png");
			if (this.streamImageVuota != null) {
				this.image.setSource(this.streamImageVuota);
			}
		} catch (final Exception e) {
			
		}
		this.elencoParametri.add(this.dim0);
		this.elencoParametri.add(this.dim1);
		this.elencoParametri.add(this.dim2);
		this.elencoParametri.add(this.dim3);
		this.elencoParametri.add(this.dim4);
		this.elencoParametri.add(this.dim5);
		this.elencoParametri.add(this.dim6);
		this.elencoParametri.add(this.dim7);
		this.elencoParametri.add(this.dim8);
		this.elencoParametri.add(this.dim9);
		this.elencoParametri.add(this.dim10);
		this.elencoParametri.add(this.dim11);
		this.elencoParametri.add(this.dim12);
		this.elencoParametri.add(this.dim13);
		this.elencoParametri.add(this.dim14);
		this.elencoParametri.add(this.dim15);
		
                this.containerAccessori.removeAllItems();
                for (TableProperties tp : accessori.getListaProprieta()) {
                    this.containerAccessori.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
                }
		this.tableAccessori.setContainerDataSource(this.containerAccessori);
		this.containerSilenziatori.removeAllItems();
		this.containerSilenziatori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerSilenziatori.addContainerProperty("Code", String.class, "");
		this.containerSilenziatori.addContainerProperty("Description", String.class, "");
		this.tableSilenziatori.setContainerDataSource(this.containerSilenziatori);
	}

	public void setVentilatoreCurrent(final String idGruppoIn, final String descrizioneGruppoIn) {
		this.idGruppo = idGruppoIn;
		this.descrizioneGruppo = descrizioneGruppoIn;
		initContainerAccessori();
		buildTableAccessori(this.tableAccessori, this.containerAccessori, this.idGruppo, this.descrizioneGruppo);
		//this.silenziatori.caricaSilenziatoriDaPreventivo(this.tableSilenziatori);
		for (int i=0 ; i<this.elencoParametri.size() ; i++) {
			this.elencoParametri.get(i).setVisible(false);
		}
        this.labelModello.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent)));
        DimensionView dimView = (DimensionView)this.l_VG.ElencoView[this.l_VG.DimensioniAngoloViewIndex];
        //l_VG.completaCaricamentoVentilatore(l_VG.VentilatoreCurrent);
        dimView.setVentilatoreCurrent();
        dimView = (DimensionView)this.l_VG.ElencoView[this.l_VG.DimensioniViewIndex];
        //l_VG.completaCaricamentoVentilatore(l_VG.VentilatoreCurrent);
        dimView.setVentilatoreCurrent();
        final double peso = this.l_VG.utilityCliente.calcolaPesoVentilatore(this.l_VG.VentilatoreCurrent);
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(1);
        String pesostr = "???";
        if (peso > 0.) {
			pesostr = this.l_VG.fmtNd.format(peso);
		}
        this.labelPesoVentilatore.setValue("<b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Peso Totale") + ": " + pesostr + " [kg]" + "</center></b>");
        this.labelPesoVentilatore1.setValue("<b><center>" + this.l_VG.utilityTraduzioni.TraduciStringa("Peso Totale") + ": " + pesostr + " [kg]" + "</center></b>");
        displayCaricoTotale();
	}
	
    private void displayCaricoTotale() {
        String l_str = this.l_VG.utilityTraduzioni.TraduciStringa("Carico Totale") + ": ";
        if (this.l_VG.gestioneAccessori.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "AV")) {
            final int qta = this.l_VG.gestioneAccessori.getAccessorio(this.l_VG.VentilatoreCurrent, "AV").qta;
            if (qta > 0) {
                final int index = this.l_VG.gestioneAccessori.getIndexAccessorio(this.l_VG.VentilatoreCurrent, "AV");
                final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(index);
                l_str += this.l_VG.utilityCliente.getCaricoSupporto(l_a, qta);
            }
        }
        this.labelCaricoTotale.setValue("<b><center>" + l_str + "</center></b>");
    }
    
	public void setVentilatoreCurrent() {
		initContainerAccessori();
		buildTableAccessori(this.tableAccessori, this.containerAccessori, this.idGruppo, this.descrizioneGruppo);
		for (int i=0 ; i<this.elencoParametri.size() ; i++) {
			this.elencoParametri.get(i).setVisible(false);
		}
        this.labelModello.setValue(this.l_VG.utility.giuntaStringHtml("<b><center><font color='blue' size='+1'>", this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent)));
	}

	private void buildTableAccessori(final XdevTable<CustomComponent> table, final Container container, final String idGruppo, final String descrizioneGruppo) {
		final String titolo = this.l_VG.utilityTraduzioni.TraduciStringa("Accessori Disponibili") + " ";
		if (idGruppo == null || idGruppo.equals("")) {
			table.setVisible(false);
		} else {
			table.setVisible(true);
			if (descrizioneGruppo != null && !descrizioneGruppo.equals("")) {
				table.setCaption(titolo + this.l_VG.utilityTraduzioni.TraduciStringa(descrizioneGruppo));
			} else {
				table.setCaption(titolo);
			}
			for (int i=0 ; i<this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size() ; i++) {
				final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(i);
				if (idGruppo.equals("") || idGruppo.equals(l_a.Gruppo)) {
					final Object obj[] = new Object[5];
					final String itemID = Integer.toString(i);
					final CheckBox cb = new CheckBox();
					cb.setValue(l_a.Selezionato);
					cb.setData(l_a.Codice);
					cb.setImmediate(true);
					cb.addValueChangeListener(new ValueChangeListener() {
						@Override
							public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
								final String accessorioCode = cb.getData().toString();
								final Item item = container.getItem(itemID);
								Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame = Cliente04AccessoriViewGruppoC9.this.l_VG.gestioneAccessori.getAccessorio(Cliente04AccessoriViewGruppoC9.this.l_VG.VentilatoreCurrent, accessorioCode);
						        if (Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame != null) {
						        	if (Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame.Forzato) {
						        		Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame.Selezionato = Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame.ValoreForzatura;
						        		cb.setValue(Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame.Selezionato);
						        		return;
						        	}
							        final boolean value = cb.getValue().booleanValue();
							        Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame.Selezionato = value;
							        Cliente04AccessoriViewGruppoC9.this.l_VG.condizionaAccessori();
							        Cliente04AccessoriViewGruppoC9.this.l_VG.accessoriAzioni.eseguiAzioneAccessorioInEsame(item, Cliente04AccessoriViewGruppoC9.this.l_VG.VentilatoreCurrent, Cliente04AccessoriViewGruppoC9.this.l_VG.accessorioInEsame, Cliente04AccessoriViewGruppoC9.this.l_VG.elencoGruppiAccessori);
							        Cliente04AccessoriViewGruppoC9.this.l_VG.ventilatoreCambiato = true;
						        	setVentilatoreCurrent();
						        }
							}
				    });
					obj[0] = cb;
					obj[1] = l_a.Codice;
					obj[2] = this.l_VG.utilityCliente.nazionalizzaDescrizioneAccessorio(l_a);
					//obj[2] = l_a.Descrizione + l_VG.gestioneAccessori.getAccessorioValoreParametro(l_a, "cod");
					final TextFieldInteger tf = new TextFieldInteger();
					tf.setValue(Integer.toString(l_a.qta));
					tf.setEnabled(l_a.qtaModificabile);
					tf.setData(l_a.Codice);
                                        tf.setWidth("100%");
					tf.addValueChangeListener(new ValueChangeListener() {
						@Override
							public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
								final String accessorioCode = tf.getData().toString();
						        final Accessorio l_aloc = Cliente04AccessoriViewGruppoC9.this.l_VG.gestioneAccessori.getAccessorio(Cliente04AccessoriViewGruppoC9.this.l_VG.VentilatoreCurrent, accessorioCode);
						        if (l_aloc != null) {
							        final int value = tf.getIntegerValue();
									l_aloc.qta = value;
									Cliente04AccessoriViewGruppoC9.this.l_VG.MainView.refreshAccessoriPreventivi();
									Cliente04AccessoriViewGruppoC9.this.l_VG.ventilatoreCambiato = true;
									setVentilatoreCurrent();
						        }
							}
					});
					obj[3] = tf;
                                        obj[4] = this.l_VG.currentCitFont.linguaDisplay.id.equals("UK")? l_a.NoteENG : l_a.NoteITA;
					boolean hide = false;
					if(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza < 80 && !this.l_VG.VentilatoreCurrent.Trasmissione) {
						hide = false;
						final List<String> elenco = Arrays.asList("CCV", "CCV_X", "CCV_I", "CPC", "CPCO", "CCP");
						if (elenco.contains(obj[1].toString())) {
							hide = true;
						}
					}
                                        if(this.l_VG.HideInox302) {
                                            if (l_a.Codice.contains("_I")) hide = true;
                                        }
                                        if(this.l_VG.HideInox304) {
                                            if (l_a.Codice.contains("_X") && (!l_a.Codice.contains("TS_XI"))) hide = true;
                                        }
                                        if(this.l_VG.HideInox302 && this.l_VG.HideInox304) {
                                            if (l_a.Codice.contains("TS_XI")) hide = true;
                                        }
					if (!hide) {
						table.addItem(obj, itemID);
					}
					
				}
			}
		}
	}

        private void tableAccessori_valueChange(final Property.ValueChangeEvent event) {
		try {
			final Item item = this.containerAccessori.getItem(event.getProperty().getValue().toString());
			for (int i=0 ; i<this.elencoParametri.size() ; i++) {
				this.elencoParametri.get(i).setVisible(false);
			}
	        final Accessorio l_a = this.l_VG.gestioneAccessori.getAccessorio(this.l_VG.VentilatoreCurrent, item.getItemProperty("Code").getValue().toString());
	        for (int i=0 ; i<Math.min(l_a.NParametri, this.nMaxAccessori) ; i++) {
	        	this.elencoParametri.get(i).setValue("<b><center>" + this.l_VG.utility.rimpiazzaLettereGreche(l_a.parametriLabel[i]) + ": " + "<font color='blue'>" + l_a.parametriVal[i]);
	        	this.elencoParametri.get(i).setVisible(true);
	        }
	        String fileName = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "accessori/";
	        if (l_a.imgName != null && !l_a.imgName.equals("") && !l_a.imgName.equals("-") && !l_a.imgName.equals("vuota.png")) {
	        	fileName += l_a.imgName;
	        	final StreamResource streamImage = this.l_VG.getStreamResourceForImageFile(fileName);
				if (streamImage != null) {
					this.image.setSource(streamImage);
				}
	        } else {
				if (this.streamImageVuota != null) {
					this.image.setSource(this.streamImageVuota);
				}
	        }
	        displayCaricoTotale();
		} catch (final Exception e) {
			
		}
	}

        private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.verticalLayout = new XdevVerticalLayout();
		this.panelForImage = new XdevPanel();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.image = new XdevImage();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.hlButtons = new XdevHorizontalLayout();
		this.labelPesoVentilatore = new XdevLabel();
		this.labelCaricoTotale = new XdevLabel();
		this.tableAccessori = new XdevTable<>();
		this.tableSilenziatori = new XdevTable<>();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.panelForLogo = new XdevPanel();
		this.verticalLayout7 = new XdevVerticalLayout();
		this.imageLogo = new XdevImage();
		this.labelModello = new XdevLabel();
		this.labelPesoVentilatore1 = new XdevLabel();
		this.labelDatimm = new XdevLabel();
		this.labelDenominazioneCliente = new XdevLabel();
		this.labelSiRiserva0 = new XdevLabel();
		this.labelSiRiserva1 = new XdevLabel();
		this.labelSiRiserva2 = new XdevLabel();
		this.panel = new XdevPanel();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.dim0 = new XdevLabel();
		this.dim1 = new XdevLabel();
		this.dim2 = new XdevLabel();
		this.dim3 = new XdevLabel();
		this.dim4 = new XdevLabel();
		this.dim5 = new XdevLabel();
		this.dim6 = new XdevLabel();
		this.dim7 = new XdevLabel();
		this.dim8 = new XdevLabel();
		this.dim9 = new XdevLabel();
		this.dim10 = new XdevLabel();
		this.dim11 = new XdevLabel();
		this.dim12 = new XdevLabel();
		this.dim13 = new XdevLabel();
		this.dim14 = new XdevLabel();
		this.dim15 = new XdevLabel();
		this.buttonPickBox = new XdevButton();
		this.buttonPickSilencerInlet = new XdevButton();
		this.buttonPickSilencerOutlet = new XdevButton();
	
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.verticalLayout.setSpacing(false);
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.panelForImage.setTabIndex(0);
		this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.hlButtons.setMargin(new MarginInfo(false, true, false, true));
		this.labelPesoVentilatore.setValue("<b><center>???");
		this.labelPesoVentilatore.setContentMode(ContentMode.HTML);
		this.labelCaricoTotale.setValue("<b><center>???");
		this.labelCaricoTotale.setContentMode(ContentMode.HTML);
		this.tableAccessori.setCaption("Accessories available");
		this.tableAccessori.setStyleName("small striped");
		this.buttonPickBox.setCaption("Pick Box");
		this.buttonPickBox.setStyleName("small");
		this.buttonPickSilencerInlet.setCaption("Pick Inlet Silencer");
		this.buttonPickSilencerInlet.setStyleName("small");
		this.buttonPickSilencerOutlet.setCaption("Pick Outlet Silencer");
		this.buttonPickSilencerOutlet.setStyleName("small");
		
		this.tableSilenziatori.setCaption("Silenziatori");
		this.tableSilenziatori.setStyleName("small striped");
		this.buttonPickBox.setSizeUndefined();
		this.buttonPickSilencerInlet.setSizeUndefined();
		this.buttonPickSilencerOutlet.setSizeUndefined();

		this.verticalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.panelForLogo.setTabIndex(0);
		this.verticalLayout7.setMargin(new MarginInfo(false));
		this.labelModello.setValue("<b><center><font color='blue' size='+1'>Label");
		this.labelModello.setContentMode(ContentMode.HTML);
		this.labelPesoVentilatore1.setValue("<b><center>???");
		this.labelPesoVentilatore1.setContentMode(ContentMode.HTML);
		this.labelDatimm.setValue("<b><center>Data [mm]");
		this.labelDatimm.setContentMode(ContentMode.HTML);
		this.labelDenominazioneCliente.setValue("<b><center><font size='+1'>MZ Aspiratori S.P.A.");
		this.labelDenominazioneCliente.setContentMode(ContentMode.HTML);
		this.labelSiRiserva0.setValue("<center>We reserve the right to");
		this.labelSiRiserva0.setContentMode(ContentMode.HTML);
		this.labelSiRiserva1.setValue("<center>modify technical data");
		this.labelSiRiserva1.setContentMode(ContentMode.HTML);
		this.labelSiRiserva2.setValue("<center>without notice.");
		this.labelSiRiserva2.setContentMode(ContentMode.HTML);
		this.panel.setTabIndex(0);
		this.verticalLayout3.setSpacing(false);
		this.verticalLayout3.setMargin(new MarginInfo(false));
		this.dim0.setValue("Label0");
		this.dim0.setContentMode(ContentMode.HTML);
		this.dim1.setValue("Label1");
		this.dim1.setContentMode(ContentMode.HTML);
		this.dim2.setValue("Label2");
		this.dim2.setContentMode(ContentMode.HTML);
		this.dim3.setValue("Label3");
		this.dim3.setContentMode(ContentMode.HTML);
		this.dim4.setValue("Label4");
		this.dim4.setContentMode(ContentMode.HTML);
		this.dim5.setValue("Label5");
		this.dim5.setContentMode(ContentMode.HTML);
		this.dim6.setValue("Label6");
		this.dim6.setContentMode(ContentMode.HTML);
		this.dim7.setValue("Label7");
		this.dim7.setContentMode(ContentMode.HTML);
		this.dim8.setValue("Label8");
		this.dim8.setContentMode(ContentMode.HTML);
		this.dim9.setValue("Label9");
		this.dim9.setContentMode(ContentMode.HTML);
		this.dim10.setValue("Label10");
		this.dim10.setContentMode(ContentMode.HTML);
		this.dim11.setValue("Label11");
		this.dim11.setContentMode(ContentMode.HTML);
		this.dim12.setValue("Label12");
		this.dim12.setContentMode(ContentMode.HTML);
		this.dim13.setValue("Label13");
		this.dim13.setContentMode(ContentMode.HTML);
		this.dim14.setValue("Label14");
		this.dim14.setContentMode(ContentMode.HTML);
		this.dim15.setValue("Label15");
		this.dim15.setContentMode(ContentMode.HTML);
	
		this.image.setWidth(-1, Unit.PIXELS);
		this.image.setHeight(100, Unit.PERCENTAGE);
		this.verticalLayout4.addComponent(this.image);
		this.verticalLayout4.setComponentAlignment(this.image, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.image, 10.0F);
		this.verticalLayout4.setSizeFull();
		this.panelForImage.setContent(this.verticalLayout4);
		this.labelPesoVentilatore.setWidth(100, Unit.PERCENTAGE);
		this.labelPesoVentilatore.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.labelPesoVentilatore);
		this.horizontalLayout2.setComponentAlignment(this.labelPesoVentilatore, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.labelPesoVentilatore, 10.0F);
		this.labelCaricoTotale.setWidth(100, Unit.PERCENTAGE);
		this.labelCaricoTotale.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout2.addComponent(this.labelCaricoTotale);
		this.horizontalLayout2.setComponentAlignment(this.labelCaricoTotale, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.labelCaricoTotale, 10.0F);
		this.panelForImage.setSizeFull();
		this.verticalLayout.addComponent(this.panelForImage);
		this.verticalLayout.setComponentAlignment(this.panelForImage, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.panelForImage, 10.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout2);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.MIDDLE_CENTER);

		this.hlButtons.addComponent(this.buttonPickBox);
		this.hlButtons.setComponentAlignment(this.buttonPickBox, Alignment.MIDDLE_CENTER);
		this.hlButtons.setExpandRatio(this.buttonPickBox, 10.0F);
		this.hlButtons.addComponent(this.buttonPickSilencerInlet);
		this.hlButtons.setComponentAlignment(this.buttonPickSilencerInlet, Alignment.MIDDLE_CENTER);
		this.hlButtons.setExpandRatio(this.buttonPickSilencerInlet, 10.0F);
		this.hlButtons.addComponent(this.buttonPickSilencerOutlet);
		this.hlButtons.setComponentAlignment(this.buttonPickSilencerOutlet, Alignment.MIDDLE_CENTER);
		this.hlButtons.setExpandRatio(this.buttonPickSilencerOutlet, 10.0F);
		this.hlButtons.setWidth(100, Unit.PERCENTAGE);
		this.hlButtons.setHeight(-1, Unit.PIXELS);
		

		
		this.tableAccessori.setSizeFull();
		this.verticalLayout.addComponent(this.tableAccessori);
		this.verticalLayout.setComponentAlignment(this.tableAccessori, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableAccessori, 5.0F);
//		this.verticalLayout.addComponent(this.hlButtons);
//		this.verticalLayout.setComponentAlignment(this.hlButtons, Alignment.MIDDLE_CENTER);
		this.tableSilenziatori.setSizeFull();
//		this.verticalLayout.addComponent(this.tableSilenziatori);
//		this.verticalLayout.setComponentAlignment(this.tableSilenziatori, Alignment.MIDDLE_CENTER);
//		this.verticalLayout.setExpandRatio(this.tableSilenziatori, 6.0F);
		
		this.imageLogo.setWidth(-1, Unit.PIXELS);
		this.imageLogo.setHeight(100, Unit.PERCENTAGE);
		this.verticalLayout7.addComponent(this.imageLogo);
		this.verticalLayout7.setComponentAlignment(this.imageLogo, Alignment.MIDDLE_CENTER);
		this.verticalLayout7.setExpandRatio(this.imageLogo, 10.0F);
		this.verticalLayout7.setSizeFull();
		this.panelForLogo.setContent(this.verticalLayout7);
		this.dim0.setWidth(100, Unit.PERCENTAGE);
		this.dim0.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim0);
		this.verticalLayout3.setComponentAlignment(this.dim0, Alignment.MIDDLE_CENTER);
		this.dim1.setWidth(100, Unit.PERCENTAGE);
		this.dim1.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim1);
		this.verticalLayout3.setComponentAlignment(this.dim1, Alignment.MIDDLE_CENTER);
		this.dim2.setWidth(100, Unit.PERCENTAGE);
		this.dim2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim2);
		this.verticalLayout3.setComponentAlignment(this.dim2, Alignment.MIDDLE_CENTER);
		this.dim3.setWidth(100, Unit.PERCENTAGE);
		this.dim3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim3);
		this.verticalLayout3.setComponentAlignment(this.dim3, Alignment.MIDDLE_CENTER);
		this.dim4.setWidth(100, Unit.PERCENTAGE);
		this.dim4.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim4);
		this.verticalLayout3.setComponentAlignment(this.dim4, Alignment.MIDDLE_CENTER);
		this.dim5.setWidth(100, Unit.PERCENTAGE);
		this.dim5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim5);
		this.verticalLayout3.setComponentAlignment(this.dim5, Alignment.MIDDLE_CENTER);
		this.dim6.setWidth(100, Unit.PERCENTAGE);
		this.dim6.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim6);
		this.verticalLayout3.setComponentAlignment(this.dim6, Alignment.MIDDLE_CENTER);
		this.dim7.setWidth(100, Unit.PERCENTAGE);
		this.dim7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim7);
		this.verticalLayout3.setComponentAlignment(this.dim7, Alignment.MIDDLE_CENTER);
		this.dim8.setWidth(100, Unit.PERCENTAGE);
		this.dim8.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim8);
		this.verticalLayout3.setComponentAlignment(this.dim8, Alignment.MIDDLE_CENTER);
		this.dim9.setWidth(100, Unit.PERCENTAGE);
		this.dim9.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim9);
		this.verticalLayout3.setComponentAlignment(this.dim9, Alignment.MIDDLE_CENTER);
		this.dim10.setWidth(100, Unit.PERCENTAGE);
		this.dim10.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim10);
		this.verticalLayout3.setComponentAlignment(this.dim10, Alignment.MIDDLE_CENTER);
		this.dim11.setWidth(100, Unit.PERCENTAGE);
		this.dim11.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim11);
		this.verticalLayout3.setComponentAlignment(this.dim11, Alignment.MIDDLE_CENTER);
		this.dim12.setWidth(100, Unit.PERCENTAGE);
		this.dim12.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim12);
		this.verticalLayout3.setComponentAlignment(this.dim12, Alignment.MIDDLE_CENTER);
		this.dim13.setWidth(100, Unit.PERCENTAGE);
		this.dim13.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim13);
		this.verticalLayout3.setComponentAlignment(this.dim13, Alignment.MIDDLE_CENTER);
		this.dim14.setWidth(100, Unit.PERCENTAGE);
		this.dim14.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim14);
		this.verticalLayout3.setComponentAlignment(this.dim14, Alignment.MIDDLE_CENTER);
		this.dim15.setWidth(100, Unit.PERCENTAGE);
		this.dim15.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.dim15);
		this.verticalLayout3.setComponentAlignment(this.dim15, Alignment.MIDDLE_CENTER);
		final CustomComponent verticalLayout3_spacer = new CustomComponent();
		verticalLayout3_spacer.setSizeFull();
		this.verticalLayout3.addComponent(verticalLayout3_spacer);
		this.verticalLayout3.setExpandRatio(verticalLayout3_spacer, 1.0F);
		this.verticalLayout3.setSizeFull();
		this.panel.setContent(this.verticalLayout3);
		this.panelForLogo.setSizeFull();
		this.verticalLayout2.addComponent(this.panelForLogo);
		this.verticalLayout2.setComponentAlignment(this.panelForLogo, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.panelForLogo, 10.0F);
		this.labelModello.setWidth(100, Unit.PERCENTAGE);
		this.labelModello.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelModello);
		this.verticalLayout2.setComponentAlignment(this.labelModello, Alignment.MIDDLE_CENTER);
		this.labelPesoVentilatore1.setWidth(100, Unit.PERCENTAGE);
		this.labelPesoVentilatore1.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelPesoVentilatore1);
		this.verticalLayout2.setComponentAlignment(this.labelPesoVentilatore1, Alignment.MIDDLE_CENTER);
		this.labelDatimm.setWidth(100, Unit.PERCENTAGE);
		this.labelDatimm.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelDatimm);
		this.verticalLayout2.setComponentAlignment(this.labelDatimm, Alignment.MIDDLE_CENTER);
		this.labelDenominazioneCliente.setWidth(100, Unit.PERCENTAGE);
		this.labelDenominazioneCliente.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelDenominazioneCliente);
		this.verticalLayout2.setComponentAlignment(this.labelDenominazioneCliente, Alignment.MIDDLE_CENTER);
		this.labelSiRiserva0.setWidth(100, Unit.PERCENTAGE);
		this.labelSiRiserva0.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelSiRiserva0);
		this.verticalLayout2.setComponentAlignment(this.labelSiRiserva0, Alignment.MIDDLE_CENTER);
		this.labelSiRiserva1.setWidth(100, Unit.PERCENTAGE);
		this.labelSiRiserva1.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelSiRiserva1);
		this.verticalLayout2.setComponentAlignment(this.labelSiRiserva1, Alignment.MIDDLE_CENTER);
		this.labelSiRiserva2.setWidth(100, Unit.PERCENTAGE);
		this.labelSiRiserva2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.labelSiRiserva2);
		this.verticalLayout2.setComponentAlignment(this.labelSiRiserva2, Alignment.MIDDLE_CENTER);
		this.panel.setSizeFull();
		this.verticalLayout2.addComponent(this.panel);
		this.verticalLayout2.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.panel, 100.0F);
		this.verticalLayout.setSizeFull();
		this.horizontalLayout.addComponent(this.verticalLayout);
		this.horizontalLayout.setComponentAlignment(this.verticalLayout, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.verticalLayout, 40.0F);
		this.verticalLayout2.setSizeFull();
		this.horizontalLayout.addComponent(this.verticalLayout2);
		this.horizontalLayout.setComponentAlignment(this.verticalLayout2, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.verticalLayout2, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	
                this.tableAccessori.addValueChangeListener(Cliente04AccessoriViewGruppoC9.this::tableAccessori_valueChange);
		this.buttonPickSilencerInlet.addClickListener(event -> this.buttonPickSilencerInlet_buttonClick(event));
		this.buttonPickSilencerOutlet.addClickListener(event -> this.buttonPickSilencerOutlet_buttonClick(event));
	} // </generated-code>


	
	private void buttonPickSilencerInlet_buttonClick(final ClickEvent event) {
		// True mandata, False aspirazione
		this.silenziatori.caricamentoSilenziatori(false, this.tableSilenziatori);

	}
	
	private void buttonPickSilencerOutlet_buttonClick(final ClickEvent event) {
		// True mandata, False aspirazione
		this.silenziatori.caricamentoSilenziatori(true, this.tableSilenziatori);
		

	}

	// <generated-code name="variables">
	private XdevButton buttonPickSilencerOutlet, buttonPickSilencerInlet, buttonPickBox;
	private XdevLabel labelPesoVentilatore, labelCaricoTotale, labelModello, labelPesoVentilatore1, labelDatimm, labelDenominazioneCliente, labelSiRiserva0,
			labelSiRiserva1, labelSiRiserva2, dim0, dim1, dim2, dim3, dim4, dim5, dim6, dim7, dim8, dim9, dim10, dim11,
			dim12, dim13, dim14, dim15;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2, hlButtons;
	private XdevImage image, imageLogo;
	private XdevTable<CustomComponent> tableAccessori, tableSilenziatori;
	private XdevPanel panelForImage, panelForLogo, panel;
	private XdevVerticalLayout verticalLayout, verticalLayout4, verticalLayout2, verticalLayout7, verticalLayout3;
	// </generated-code>

}
