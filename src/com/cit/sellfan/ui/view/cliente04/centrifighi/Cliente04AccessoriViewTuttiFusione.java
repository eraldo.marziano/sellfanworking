package com.cit.sellfan.ui.view.cliente04.centrifighi;

import java.util.Arrays;
import java.util.List;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;
import cit.sellfan.Costanti;
import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import cit.utility.NumeriRomani;
import com.cit.sellfan.ui.utility.Accessori;
import com.cit.sellfan.ui.utility.TableProperties;
import de.steinwedel.messagebox.MessageBox;

public class Cliente04AccessoriViewTuttiFusione extends XdevView {

    private VariabiliGlobali l_VG;
    private Container containerAccessoriFusione = new IndexedContainer();
    private String descrizioneGruppo;
    private Accessori accessori = new Accessori();

    public Cliente04AccessoriViewTuttiFusione() {
        super();
        this.initUI();
    }

    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        accessori.setVariabiliGlobali(l_VG);
        this.containerAccessoriFusione.removeAllItems();
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.containerAccessoriFusione.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
        }
        this.tableAccessoriGruppoA.setContainerDataSource(this.containerAccessoriFusione);
        this.comboBoxRDLD.setEnabled(false);
        this.comboBoxRDLD.addItem("RD");
        this.comboBoxRDLD.addItem("LG");
        this.comboBoxRDLD.setEnabled(true);
    }

    private void initcontainerAccessoriFusione() {
        this.containerAccessoriFusione = new IndexedContainer();
        this.containerAccessoriFusione.removeAllItems();
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.containerAccessoriFusione.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
            this.tableAccessoriGruppoA.setColumnHeader(tp.propertyId, tp.traduzione);
            this.tableAccessoriGruppoA.setColumnExpandRatio(tp.propertyId, tp.larghezza);
        }
        this.tableAccessoriGruppoA.setContainerDataSource(this.containerAccessoriFusione);
    }
    
    public void Nazionalizza() {
        this.buttonAggiungiPreventivo.setVisible(this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
        this.labelEsecuzione.setValue("<html><b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione") + "</html>");
        //labelClasse.setValue("<html><b>" + l_VG.utilityTraduzioni.TraduciStringa("Classe") + "</html>");
        this.verticalLayoutMotore.setCaption("<html><b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Motore") + "</html>");
        this.gridLayoutInstallazione.setCaption("<html><b>" + this.l_VG.utilityTraduzioni.TraduciStringa("Installazione") + "</html>");
        this.buttonCambiaMotore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cambia Motore"));
        this.buttonResetAccessori.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset Accessori"));
        this.buttonAggiungiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.tableAccessoriGruppoA.setColumnHeader(tp.propertyId, tp.traduzione);
        }
        final UtilityCliente l_u = this.l_VG.utilityCliente;
        this.checkBoxA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(l_u.installazioneDesc[0]));
        this.checkBoxB.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(l_u.installazioneDesc[1]));
        this.checkBoxC.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(l_u.installazioneDesc[2]));
        this.checkBoxD.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(l_u.installazioneDesc[3]));
    }

    public void setVentilatoreCurrentCaratteristiche() {
        this.comboBoxClassi.setEnabled(false);
        this.comboBoxClassi.removeAllItems();
        for (int i = 0; i < this.l_VG.VentilatoreCurrent.elencoClassi.size(); i++) {
            this.comboBoxClassi.addItem("Class " + NumeriRomani.daAraboARomano(this.l_VG.VentilatoreCurrent.elencoClassi.get(i)));
        }
        this.comboBoxClassi.setValue("Class " + this.l_VG.VentilatoreCurrent.Classe);
        this.comboBoxClassi.setEnabled(true);
        this.comboBoxRDLD.setEnabled(false);
        this.comboBoxRDLD.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.Orientamento);
        this.comboBoxRDLD.setEnabled(true);
        this.comboBoxEsecuzioni.setEnabled(false);
        this.comboBoxEsecuzioni.removeAllItems();
        for (int i = 0; i < this.l_VG.VentilatoreCurrent.Esecuzioni.length; i++) {
            if (!this.l_VG.VentilatoreCurrent.Esecuzioni[i].equals("-")) {
                this.comboBoxEsecuzioni.addItem(this.l_VG.VentilatoreCurrent.Esecuzioni[i]);
            }
        }
        this.comboBoxEsecuzioni.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
        this.comboBoxEsecuzioni.setEnabled(this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo);
        this.labelAngolo.setValue("<html><b><center>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
        this.l_VG.fmtNd.setMaximumFractionDigits(3);
        if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null && !this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente.equals("-")) {
            this.labelCodiceMotore.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
            this.labelPotenzaMotore.setValue(this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW) + " [kW]");
            this.labelDescrizioneMotore.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Descrizione);
        } else {
            this.labelCodiceMotore.setValue("");
            this.labelPotenzaMotore.setValue("");
            this.labelDescrizioneMotore.setValue("");
        }
        setInstallazione("");
        this.buttonAggiungiPreventivo.setEnabled(this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo);
    }

    public void setVentilatoreCurrent(final String descrizioneGruppoIn) {
        this.descrizioneGruppo = descrizioneGruppoIn;
        initcontainerAccessoriFusione();
        buildTableAccessori(this.tableAccessoriGruppoA, this.containerAccessoriFusione, "", this.descrizioneGruppo);
    }

    public void setVentilatoreCurrent() {
        initcontainerAccessoriFusione();
        buildTableAccessori(this.tableAccessoriGruppoA, this.containerAccessoriFusione, "", this.descrizioneGruppo);
        if (!l_VG.gestioneAccessori.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "MPF")) {
            buttonAngoloMeno.setEnabled(true);
            buttonAngoloPiu.setEnabled(true);
        }
    }

    private void setInstallazione(final String tipo) {
        final Cliente04VentilatoreCampiCliente l_vcc = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
        this.checkBoxA.setEnabled(false);
        this.checkBoxB.setEnabled(false);
        this.checkBoxC.setEnabled(false);
        this.checkBoxD.setEnabled(false);
        l_vcc.TipoInstallazione = tipo;
        if (!tipo.equals("")) {
            this.l_VG.utilityCliente.gestioneVentilatori.setInstallazione(this.l_VG.VentilatoreCurrent, l_vcc.TipoInstallazione);
        }
        this.checkBoxA.setValue(false);
        this.checkBoxB.setValue(false);
        this.checkBoxC.setValue(false);
        this.checkBoxD.setValue(false);
        switch (l_vcc.TipoInstallazione) {
            case "A":
                this.checkBoxA.setValue(true);
                break;
            case "B":
                this.checkBoxB.setValue(true);
                break;
            case "C":
                this.checkBoxC.setValue(true);
                break;
            case "D":
                this.checkBoxD.setValue(true);
                break;
            default:
                break;
        }
        this.checkBoxA.setEnabled(true);
        this.checkBoxB.setEnabled(true);
        this.checkBoxC.setEnabled(true);
        this.checkBoxD.setEnabled(true);
        setVentilatoreCurrent();
    }

    private void buildTableAccessori(final XdevTable<CustomComponent> table, final Container container, final String idGruppo, final String descrizioneGruppo) {
        final String titolo = this.l_VG.utilityTraduzioni.TraduciStringa("Accessori Disponibili") + " ";
        if (idGruppo == null) {
            table.setVisible(false);
        } else {
            table.setVisible(true);
            if (descrizioneGruppo != null && !descrizioneGruppo.equals("")) {
                table.setCaption(titolo + this.l_VG.utilityTraduzioni.TraduciStringa(descrizioneGruppo));
            } else {
                table.setCaption(titolo);
            }
            for (int i = 0; i < this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); i++) {
                final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(i);
                if (idGruppo.equals("") || idGruppo.equals(l_a.Gruppo)) {
                    final Object obj[] = new Object[5];
                    final String itemID = Integer.toString(i);
                    final CheckBox cb = new CheckBox();
                    cb.setValue(l_a.Selezionato);
                    cb.setData(l_a.Codice);
                    cb.setImmediate(true);
                    cb.addValueChangeListener(new ValueChangeListener() {
                        @Override
                        public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                            final String accessorioCode = cb.getData().toString();
                            final Item item = container.getItem(itemID);
                            Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame = Cliente04AccessoriViewTuttiFusione.this.l_VG.gestioneAccessori.getAccessorio(Cliente04AccessoriViewTuttiFusione.this.l_VG.VentilatoreCurrent, accessorioCode);
                            if (Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame != null) {
                                if (Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame.Forzato) {
                                    Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame.Selezionato = Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame.ValoreForzatura;
                                    cb.setValue(Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame.Selezionato);
                                    return;
                                }
                                final boolean value = cb.getValue().booleanValue();
                                Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame.Selezionato = value;
                                Cliente04AccessoriViewTuttiFusione.this.l_VG.condizionaAccessori();
                                Cliente04AccessoriViewTuttiFusione.this.l_VG.accessoriAzioni.eseguiAzioneAccessorioInEsame(item, Cliente04AccessoriViewTuttiFusione.this.l_VG.VentilatoreCurrent, Cliente04AccessoriViewTuttiFusione.this.l_VG.accessorioInEsame, Cliente04AccessoriViewTuttiFusione.this.l_VG.elencoGruppiAccessori);
                                Cliente04AccessoriViewTuttiFusione.this.l_VG.ventilatoreCambiato = true;
                                setVentilatoreCurrent();
                            }
                        }
                    });
                    obj[0] = cb;
                    obj[1] = l_a.Codice;
                    obj[2] = this.l_VG.utilityCliente.nazionalizzaDescrizioneAccessorio(l_a);
                    //obj[2] = l_VG.utilityTraduzioni.TraduciStringa(l_a.DescrizioneIT) + l_VG.gestioneAccessori.getAccessorioValoreParametro(l_a, "cod");
                    final TextFieldInteger tf = new TextFieldInteger();
                    tf.setValue(Integer.toString(l_a.qta));
                    tf.setEnabled(l_a.qtaModificabile);
                    tf.setData(l_a.Codice);
                    tf.setWidth("100%");
                    tf.addValueChangeListener((final com.vaadin.data.Property.ValueChangeEvent event) -> {
                        final String accessorioCode = tf.getData().toString();
                        final Accessorio l_aloc = Cliente04AccessoriViewTuttiFusione.this.l_VG.gestioneAccessori.getAccessorio(Cliente04AccessoriViewTuttiFusione.this.l_VG.VentilatoreCurrent, accessorioCode);
                        if (l_aloc != null) {
                            final int value = tf.getIntegerValue();
                            l_aloc.qta = value;
                            Cliente04AccessoriViewTuttiFusione.this.l_VG.MainView.refreshAccessoriPreventivi();
                            Cliente04AccessoriViewTuttiFusione.this.l_VG.ventilatoreCambiato = true;
                            setVentilatoreCurrent();
                        }
                    });
                    obj[3] = tf;
                    obj[4] = l_VG.utilityTraduzioni.getLinguaOutput().equals("English")? l_a.NoteENG : l_a.NoteITA;
                    boolean hide = false;
                    if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza < 80 && !this.l_VG.VentilatoreCurrent.Trasmissione) {
                        hide = false;
                        final List<String> elenco = Arrays.asList("CCV", "CCV_X", "CCV_I", "CPC", "CPCO", "CCP");
                        if (elenco.contains(obj[1].toString())) {
                            hide = true;
                        }
                    }
                    if (!hide) {
                        table.addItem(obj, itemID);
                    }
                }
            }
        }
    }
private void buttonAngoloMeno_buttonClick(final Button.ClickEvent event) {
        this.l_VG.cambiaAngolo(-1);
        this.labelAngolo.setValue("<html><b><center>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
    }

private void buttonAngoloPiu_buttonClick(final Button.ClickEvent event) {
        this.l_VG.cambiaAngolo(1);
        this.labelAngolo.setValue("<html><b><center>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
    }

private void buttonCambiaMotore_buttonClick(final Button.ClickEvent event) {
        final String warningStr = this.l_VG.utilityCliente.getWarning(this.l_VG.VentilatoreCurrent, 0);
        if (warningStr != null) {
            final MessageBox msgBox = MessageBox.create();
            msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(warningStr));
            msgBox.withOkButton(() -> {
                this.l_VG.cambiaManualmenteMotore();
            });
            msgBox.open();
        } else {
            this.l_VG.cambiaManualmenteMotore();
        }
    }

private void buttonResetAccessori_buttonClick(final Button.ClickEvent event) {
        boolean ventilatoreSelezionato = true;
        boolean motoreSelezionato = true;
        if (this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            ventilatoreSelezionato = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).VentilatoreSelezionato;
            motoreSelezionato = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).MotoreSelezionato;
        }
        this.l_VG.utilityCliente.loadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
        this.l_VG.regolamento327Algoritmo.setVariatore(this.l_VG.VentilatoreCurrent.datiERP327.usaVariatore);
        if (this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            this.l_VG.preventivoCurrent.PreventivoCambiato = true;
        }
        this.l_VG.VentilatoreCurrent.VentilatoreCambiato = true;
        if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
            final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
            if (l_vcc04.Tipo.equals("centrifugo")) {
                final Cliente04AccessoriViewCentrifughi view = (Cliente04AccessoriViewCentrifughi) this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex];
                view.setVentilatoreCurrent();
            } else if (l_vcc04.Tipo.equals("fusione")) {
                final Cliente04AccessoriViewFusione view = (Cliente04AccessoriViewFusione) this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex];
                view.setVentilatoreCurrent();
            } else if (l_vcc04.Tipo.equals("assiale")) {
            }
        }
}

private void buttonAggiungiPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.addVentilatoreCorrenteToPreventivo();
    }

private void checkBoxA_valueChange(final Property.ValueChangeEvent event) {
        if (!this.checkBoxA.isEnabled()) {
            return;
        }
        if (this.checkBoxA.getValue()) {
            setInstallazione("A");
        }
    }

private void checkBoxB_valueChange(final Property.ValueChangeEvent event) {
        if (!this.checkBoxB.isEnabled()) {
            return;
        }
        if (this.checkBoxB.getValue()) {
            setInstallazione("B");
        }
    }

private void checkBoxC_valueChange(final Property.ValueChangeEvent event) {
        if (!this.checkBoxC.isEnabled()) {
            return;
        }
        if (this.checkBoxC.getValue()) {
            setInstallazione("C");
        }
    }

private void checkBoxD_valueChange(final Property.ValueChangeEvent event) {
        if (!this.checkBoxD.isEnabled()) {
            return;
        }
        if (this.checkBoxD.getValue()) {
            setInstallazione("D");
        }
    }

private void comboBoxEsecuzioni_valueChange(final Property.ValueChangeEvent event) {
        if (!this.comboBoxEsecuzioni.isEnabled()) {
            return;
        }
        this.l_VG.cambiaEsecuzione(this.comboBoxEsecuzioni.getValue().toString());
    }

private void comboBoxClassi_valueChange(final Property.ValueChangeEvent event) {
        if (!this.comboBoxClassi.isEnabled()) {
            return;
        }
        this.l_VG.cambiaClasse(this.comboBoxClassi.getValue().toString());
    }

private void comboBoxRDLD_valueChange(final Property.ValueChangeEvent event) {
        if (!this.comboBoxRDLD.isEnabled()) {
            return;
        }
        this.l_VG.cambiaOrientamento();
    }

private void initUI() {
        this.verticalLayout = new XdevVerticalLayout();
        this.panel = new XdevPanel();
        this.horizontalLayout = new XdevHorizontalLayout();
        this.verticalLayout2 = new XdevVerticalLayout();
        this.horizontalLayout8 = new XdevHorizontalLayout();
        this.labelEsecuzione = new XdevLabel();
        this.comboBoxEsecuzioni = new XdevComboBox<>();
        this.comboBoxClassi = new XdevComboBox<>();
        this.horizontalLayout2 = new XdevHorizontalLayout();
        this.comboBoxRDLD = new XdevComboBox<>();
        this.buttonAngoloMeno = new XdevButton();
        this.labelAngolo = new XdevLabel();
        this.buttonAngoloPiu = new XdevButton();
        this.verticalLayoutMotore = new XdevVerticalLayout();
        this.horizontalLayout5 = new XdevHorizontalLayout();
        this.labelCodiceMotore = new XdevLabel();
        this.labelPotenzaMotore = new XdevLabel();
        this.horizontalLayout6 = new XdevHorizontalLayout();
        this.labelDescrizioneMotore = new XdevLabel();
        this.horizontalLayout7 = new XdevHorizontalLayout();
        this.buttonCambiaMotore = new XdevButton();
        this.gridLayoutInstallazione = new XdevGridLayout();
        this.checkBoxA = new XdevCheckBox();
        this.checkBoxB = new XdevCheckBox();
        this.checkBoxC = new XdevCheckBox();
        this.checkBoxD = new XdevCheckBox();
        this.verticalLayout4 = new XdevVerticalLayout();
        this.buttonResetAccessori = new XdevButton();
        this.buttonAggiungiPreventivo = new XdevButton();
        this.tableAccessoriGruppoA = new XdevTable<>();
        this.verticalLayout.setMargin(new MarginInfo(false));
        this.horizontalLayout.setMargin(new MarginInfo(false));
        this.verticalLayout2.setMargin(new MarginInfo(true, false, true, false));
        this.horizontalLayout8.setMargin(new MarginInfo(false, true, false, true));
        this.labelEsecuzione.setValue("<html><b>Esecuzione</html>");
        this.labelEsecuzione.setContentMode(ContentMode.HTML);
        this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
        this.buttonAngoloMeno.setCaption("<<");
        this.buttonAngoloMeno.setStyleName("small");
        this.labelAngolo.setValue("360°");
        this.labelAngolo.setContentMode(ContentMode.HTML);
        this.buttonAngoloPiu.setCaption(">>");
        this.buttonAngoloPiu.setStyleName("small");
        this.verticalLayoutMotore.setCaption("Motore");
        this.verticalLayoutMotore.setSpacing(false);
        this.verticalLayoutMotore.setCaptionAsHtml(true);
        this.verticalLayoutMotore.setMargin(new MarginInfo(false));
        this.horizontalLayout5.setMargin(new MarginInfo(false));
        this.labelCodiceMotore.setValue("PippoFranco");
        this.labelCodiceMotore.setContentMode(ContentMode.HTML);
        this.labelPotenzaMotore.setValue("3.8 kW");
        this.horizontalLayout6.setMargin(new MarginInfo(false));
        this.labelDescrizioneMotore.setStyleName("small");
        this.labelDescrizioneMotore.setValue("Descrizione");
        this.labelDescrizioneMotore.setContentMode(ContentMode.HTML);
        this.horizontalLayout7.setSpacing(false);
        this.horizontalLayout7.setMargin(new MarginInfo(false));
        this.buttonCambiaMotore.setCaption("Cambia Motore");
        this.buttonCambiaMotore.setStyleName("small");
        this.gridLayoutInstallazione.setCaption("Installazione");
        this.gridLayoutInstallazione.setCaptionAsHtml(true);
        this.gridLayoutInstallazione.setMargin(new MarginInfo(false));
        this.checkBoxA.setCaption("A");
        this.checkBoxB.setCaption("B");
        this.checkBoxC.setCaption("C");
        this.checkBoxD.setCaption("D");
        this.verticalLayout4.setMargin(new MarginInfo(true, true, false, true));
        this.buttonResetAccessori.setCaption("Reset Accessori");
        this.buttonResetAccessori.setStyleName("small");
        this.buttonAggiungiPreventivo.setCaption("Aggiungi al Preventivo");
        this.buttonAggiungiPreventivo.setStyleName("big giallo");
        this.tableAccessoriGruppoA.setCaption("Accessories available");
        this.tableAccessoriGruppoA.setStyleName("small mystriped");

        this.labelEsecuzione.setWidth(100, Unit.PERCENTAGE);
        this.labelEsecuzione.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout8.addComponent(this.labelEsecuzione);
        this.horizontalLayout8.setComponentAlignment(this.labelEsecuzione, Alignment.MIDDLE_CENTER);
        this.horizontalLayout8.setExpandRatio(this.labelEsecuzione, 15.0F);
        this.comboBoxEsecuzioni.setWidth(100, Unit.PERCENTAGE);
        this.comboBoxEsecuzioni.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout8.addComponent(this.comboBoxEsecuzioni);
        this.horizontalLayout8.setComponentAlignment(this.comboBoxEsecuzioni, Alignment.MIDDLE_CENTER);
        this.horizontalLayout8.setExpandRatio(this.comboBoxEsecuzioni, 15.0F);
        this.comboBoxClassi.setWidth(100, Unit.PERCENTAGE);
        this.comboBoxClassi.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout8.addComponent(this.comboBoxClassi);
        this.horizontalLayout8.setComponentAlignment(this.comboBoxClassi, Alignment.MIDDLE_CENTER);
        this.horizontalLayout8.setExpandRatio(this.comboBoxClassi, 15.0F);
        this.comboBoxRDLD.setWidth(100, Unit.PERCENTAGE);
        this.comboBoxRDLD.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout2.addComponent(this.comboBoxRDLD);
        this.horizontalLayout2.setComponentAlignment(this.comboBoxRDLD, Alignment.MIDDLE_CENTER);
        this.horizontalLayout2.setExpandRatio(this.comboBoxRDLD, 10.0F);
        this.buttonAngoloMeno.setWidth(100, Unit.PERCENTAGE);
        this.buttonAngoloMeno.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout2.addComponent(this.buttonAngoloMeno);
        this.horizontalLayout2.setComponentAlignment(this.buttonAngoloMeno, Alignment.MIDDLE_CENTER);
        this.horizontalLayout2.setExpandRatio(this.buttonAngoloMeno, 5.0F);
        this.labelAngolo.setWidth(100, Unit.PERCENTAGE);
        this.labelAngolo.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout2.addComponent(this.labelAngolo);
        this.horizontalLayout2.setComponentAlignment(this.labelAngolo, Alignment.MIDDLE_CENTER);
        this.horizontalLayout2.setExpandRatio(this.labelAngolo, 5.0F);
        this.buttonAngoloPiu.setWidth(100, Unit.PERCENTAGE);
        this.buttonAngoloPiu.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout2.addComponent(this.buttonAngoloPiu);
        this.horizontalLayout2.setComponentAlignment(this.buttonAngoloPiu, Alignment.MIDDLE_CENTER);
        this.horizontalLayout2.setExpandRatio(this.buttonAngoloPiu, 5.0F);
        this.horizontalLayout8.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayout8.setHeight(-1, Unit.PIXELS);
        this.verticalLayout2.addComponent(this.horizontalLayout8);
        this.verticalLayout2.setComponentAlignment(this.horizontalLayout8, Alignment.TOP_CENTER);
        this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
        this.verticalLayout2.addComponent(this.horizontalLayout2);
        this.verticalLayout2.setComponentAlignment(this.horizontalLayout2, Alignment.TOP_CENTER);
        this.labelCodiceMotore.setWidth(100, Unit.PERCENTAGE);
        this.labelCodiceMotore.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout5.addComponent(this.labelCodiceMotore);
        this.horizontalLayout5.setExpandRatio(this.labelCodiceMotore, 10.0F);
        this.labelPotenzaMotore.setWidth(100, Unit.PERCENTAGE);
        this.labelPotenzaMotore.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout5.addComponent(this.labelPotenzaMotore);
        this.horizontalLayout5.setExpandRatio(this.labelPotenzaMotore, 10.0F);
        this.labelDescrizioneMotore.setWidth(100, Unit.PERCENTAGE);
        this.labelDescrizioneMotore.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout6.addComponent(this.labelDescrizioneMotore);
        this.horizontalLayout6.setExpandRatio(this.labelDescrizioneMotore, 10.0F);
        this.buttonCambiaMotore.setSizeUndefined();
        this.horizontalLayout7.addComponent(this.buttonCambiaMotore);
        this.horizontalLayout7.setExpandRatio(this.buttonCambiaMotore, 10.0F);
        this.horizontalLayout5.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayout5.setHeight(-1, Unit.PIXELS);
        this.verticalLayoutMotore.addComponent(this.horizontalLayout5);
        this.verticalLayoutMotore.setComponentAlignment(this.horizontalLayout5, Alignment.MIDDLE_CENTER);
        this.horizontalLayout6.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayout6.setHeight(-1, Unit.PIXELS);
        this.verticalLayoutMotore.addComponent(this.horizontalLayout6);
        this.verticalLayoutMotore.setComponentAlignment(this.horizontalLayout6, Alignment.MIDDLE_CENTER);
        this.horizontalLayout7.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayout7.setHeight(-1, Unit.PIXELS);
        this.verticalLayoutMotore.addComponent(this.horizontalLayout7);
        this.verticalLayoutMotore.setComponentAlignment(this.horizontalLayout7, Alignment.MIDDLE_CENTER);
        this.gridLayoutInstallazione.setColumns(1);
        this.gridLayoutInstallazione.setRows(5);
        this.checkBoxA.setWidth(31, Unit.PIXELS);
        this.checkBoxA.setHeight(-1, Unit.PIXELS);
        this.gridLayoutInstallazione.addComponent(this.checkBoxA, 0, 0);
        this.checkBoxB.setWidth(31, Unit.PIXELS);
        this.checkBoxB.setHeight(-1, Unit.PIXELS);
        this.gridLayoutInstallazione.addComponent(this.checkBoxB, 0, 1);
        this.checkBoxC.setWidth(31, Unit.PIXELS);
        this.checkBoxC.setHeight(-1, Unit.PIXELS);
        this.gridLayoutInstallazione.addComponent(this.checkBoxC, 0, 2);
        this.checkBoxD.setWidth(31, Unit.PIXELS);
        this.checkBoxD.setHeight(-1, Unit.PIXELS);
        this.gridLayoutInstallazione.addComponent(this.checkBoxD, 0, 3);
        this.gridLayoutInstallazione.setColumnExpandRatio(0, 10.0F);
        final CustomComponent gridLayoutInstallazione_vSpacer = new CustomComponent();
        gridLayoutInstallazione_vSpacer.setSizeFull();
        this.gridLayoutInstallazione.addComponent(gridLayoutInstallazione_vSpacer, 0, 4, 0, 4);
        this.gridLayoutInstallazione.setRowExpandRatio(4, 1.0F);
        this.buttonResetAccessori.setSizeUndefined();
        this.verticalLayout4.addComponent(this.buttonResetAccessori);
        this.verticalLayout4.setComponentAlignment(this.buttonResetAccessori, Alignment.MIDDLE_CENTER);
        this.buttonAggiungiPreventivo.setSizeUndefined();
        this.verticalLayout4.addComponent(this.buttonAggiungiPreventivo);
        this.verticalLayout4.setComponentAlignment(this.buttonAggiungiPreventivo, Alignment.MIDDLE_CENTER);
        this.verticalLayout2.setWidth(100, Unit.PERCENTAGE);
        this.verticalLayout2.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout.addComponent(this.verticalLayout2);
        this.horizontalLayout.setComponentAlignment(this.verticalLayout2, Alignment.TOP_CENTER);
        this.horizontalLayout.setExpandRatio(this.verticalLayout2, 20.0F);
        this.verticalLayoutMotore.setWidth(100, Unit.PERCENTAGE);
        this.verticalLayoutMotore.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout.addComponent(this.verticalLayoutMotore);
        this.horizontalLayout.setComponentAlignment(this.verticalLayoutMotore, Alignment.TOP_CENTER);
        this.horizontalLayout.setExpandRatio(this.verticalLayoutMotore, 15.0F);
        this.gridLayoutInstallazione.setWidth(100, Unit.PERCENTAGE);
        this.gridLayoutInstallazione.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout.addComponent(this.gridLayoutInstallazione);
        this.horizontalLayout.setComponentAlignment(this.gridLayoutInstallazione, Alignment.TOP_CENTER);
        this.horizontalLayout.setExpandRatio(this.gridLayoutInstallazione, 10.0F);
        this.verticalLayout4.setWidth(100, Unit.PERCENTAGE);
        this.verticalLayout4.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout.addComponent(this.verticalLayout4);
        this.horizontalLayout.setComponentAlignment(this.verticalLayout4, Alignment.TOP_CENTER);
        this.horizontalLayout.setExpandRatio(this.verticalLayout4, 10.0F);
        this.horizontalLayout.setSizeFull();
        this.panel.setContent(this.horizontalLayout);
        this.panel.setSizeFull();
        this.verticalLayout.addComponent(this.panel);
        this.verticalLayout.setComponentAlignment(this.panel, Alignment.TOP_CENTER);
        this.verticalLayout.setExpandRatio(this.panel, 10.0F);
        this.tableAccessoriGruppoA.setSizeFull();
        this.verticalLayout.addComponent(this.tableAccessoriGruppoA);
        this.verticalLayout.setComponentAlignment(this.tableAccessoriGruppoA, Alignment.TOP_CENTER);
        this.verticalLayout.setExpandRatio(this.tableAccessoriGruppoA, 20.0F);
        this.verticalLayout.setSizeFull();
        this.setContent(this.verticalLayout);
        this.setSizeFull();

        this.comboBoxEsecuzioni.addValueChangeListener(Cliente04AccessoriViewTuttiFusione.this::comboBoxEsecuzioni_valueChange);
        this.comboBoxClassi.addValueChangeListener(Cliente04AccessoriViewTuttiFusione.this::comboBoxClassi_valueChange);
        this.comboBoxRDLD.addValueChangeListener(Cliente04AccessoriViewTuttiFusione.this::comboBoxRDLD_valueChange);
        this.buttonAngoloMeno.addClickListener(event -> this.buttonAngoloMeno_buttonClick(event));
        this.buttonAngoloPiu.addClickListener(event -> this.buttonAngoloPiu_buttonClick(event));
        this.buttonCambiaMotore.addClickListener(event -> this.buttonCambiaMotore_buttonClick(event));
        this.checkBoxA.addValueChangeListener(Cliente04AccessoriViewTuttiFusione.this::checkBoxA_valueChange);
        this.checkBoxB.addValueChangeListener(Cliente04AccessoriViewTuttiFusione.this::checkBoxB_valueChange);
        this.checkBoxC.addValueChangeListener(Cliente04AccessoriViewTuttiFusione.this::checkBoxC_valueChange);
        this.checkBoxD.addValueChangeListener(Cliente04AccessoriViewTuttiFusione.this::checkBoxD_valueChange);
        this.buttonResetAccessori.addClickListener(event -> this.buttonResetAccessori_buttonClick(event));
        this.buttonAggiungiPreventivo.addClickListener(event -> this.buttonAggiungiPreventivo_buttonClick(event));
    } // </generated-code>

    // <generated-code name="variables">
    private XdevLabel labelEsecuzione, labelAngolo, labelCodiceMotore, labelPotenzaMotore, labelDescrizioneMotore;
    private XdevButton buttonAngoloMeno, buttonAngoloPiu, buttonCambiaMotore, buttonResetAccessori;
    public  XdevButton buttonAggiungiPreventivo;
    private XdevHorizontalLayout horizontalLayout, horizontalLayout8, horizontalLayout2, horizontalLayout5,
            horizontalLayout6, horizontalLayout7;
    private XdevTable<CustomComponent> tableAccessoriGruppoA;
    private XdevPanel panel;
    private XdevCheckBox checkBoxA, checkBoxB, checkBoxC, checkBoxD;
    private XdevGridLayout gridLayoutInstallazione;
    private XdevVerticalLayout verticalLayout, verticalLayout2, verticalLayoutMotore, verticalLayout4;
    private XdevComboBox<CustomComponent> comboBoxEsecuzioni, comboBoxClassi, comboBoxRDLD;
    // </generated-code>

    void setMPF(Accessorio acc) {
        l_VG.dbTecnico.aggiornaDimensioniOrientamentoEsecuzione(l_VG.VentilatoreCurrent, acc.Selezionato);
        if (acc.Selezionato) {
            buttonAngoloMeno.setEnabled(false);
            buttonAngoloPiu.setEnabled(false);
            l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo = 225;
            this.l_VG.cambiaAngolo(1);
            this.labelAngolo.setValue("<html><b><center>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
        } else {
            buttonAngoloMeno.setEnabled(true);
            buttonAngoloPiu.setEnabled(true);
            this.labelAngolo.setValue("<html><b><center>" + Integer.toString(this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) + "°</html>");
        }
    }

}
