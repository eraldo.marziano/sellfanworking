package com.cit.sellfan.ui.view.cliente04.centrifighi;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.cliente04.Cliente04GestioneSilenziatori;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.Accessorio;
import de.steinwedel.messagebox.MessageBox;
import java.util.List;
import java.util.Map;

public class Cliente04AccessoriViewSilenziatori extends XdevView {
	private VariabiliGlobali l_VG;
	private final StreamResource streamImageVuota = null;
	private final ArrayList<XdevLabel> elencoParametri = new ArrayList<>();
	private Container containerSilenziatoriAspirazione = new IndexedContainer();
	private Container containerSilenziatoriMandata = new IndexedContainer();
	private Container containerBox = new IndexedContainer();
	private Cliente04GestioneSilenziatori silenziatori;

	/**
	 * 
	 */
	public Cliente04AccessoriViewSilenziatori() {
		super();
		this.initUI();
	}

	public void Nazionalizza() {
		this.tableSilenziatoriAspirazione.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aspirazione"));
		this.tableSilenziatoriMandata.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Mandata"));
		this.buttonPickBox.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Scegli Box"));
		this.buttonPickSilencerInlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Aspirazione"));
		this.buttonPickSilencerOutlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Mandata"));
	}

	private void initContainerSilenziatori() {
		
		this.containerBox = new IndexedContainer();
		this.containerBox.removeAllItems();
		this.containerBox.addContainerProperty("Selezionato", CheckBox.class, false);
		this.containerBox.addContainerProperty("Code", String.class, "");
		this.containerBox.addContainerProperty("Description", String.class, "");
		this.containerBox.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerBox.addContainerProperty("Details", Button.class, 0.0);
		
		this.tableBox.setColumnExpandRatio("Selezionato", 0.5F);
		this.tableBox.setColumnExpandRatio("Code", 1.5F);
		this.tableBox.setColumnExpandRatio("Description", 6.5F);
		this.tableBox.setColumnExpandRatio("Qta", 2F);
		this.tableBox.setColumnExpandRatio("Details", 2F);
		
		this.tableBox.setColumnHeader("Selezionato", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableBox.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableBox.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableBox.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
		this.tableBox.setColumnHeader("Details", this.l_VG.utilityTraduzioni.TraduciStringa("Dettagli"));
		
		
		this.containerSilenziatoriAspirazione = new IndexedContainer();
		this.containerSilenziatoriAspirazione.removeAllItems();
		this.containerSilenziatoriAspirazione.addContainerProperty("Selezionato", CheckBox.class, false);
		this.containerSilenziatoriAspirazione.addContainerProperty("Code", String.class, "");
		this.containerSilenziatoriAspirazione.addContainerProperty("Description", String.class, "");
		this.containerSilenziatoriAspirazione.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerSilenziatoriAspirazione.addContainerProperty("Speed", String.class, 0.0);
		this.containerSilenziatoriAspirazione.addContainerProperty("Details", Button.class, 0.0);
		
		this.tableSilenziatoriAspirazione.setColumnExpandRatio("Selezionato", 0.5F);
		this.tableSilenziatoriAspirazione.setColumnExpandRatio("Code", 2);
		this.tableSilenziatoriAspirazione.setColumnExpandRatio("Description", 6);
		this.tableSilenziatoriAspirazione.setColumnExpandRatio("Qta", 2);
		this.tableSilenziatoriAspirazione.setColumnExpandRatio("Speed", 1);
		this.tableSilenziatoriAspirazione.setColumnExpandRatio("Details", 2);
		
		this.tableSilenziatoriAspirazione.setColumnHeader("Selezionato", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableSilenziatoriAspirazione.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableSilenziatoriAspirazione.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableSilenziatoriAspirazione.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
		this.tableSilenziatoriAspirazione.setColumnHeader("Speed", this.l_VG.utilityTraduzioni.TraduciStringa("Velocità"));
		this.tableSilenziatoriAspirazione.setColumnHeader("Details", this.l_VG.utilityTraduzioni.TraduciStringa("Dettagli"));

		
		this.containerSilenziatoriMandata = new IndexedContainer();
		this.containerSilenziatoriMandata.removeAllItems();
		this.containerSilenziatoriMandata.addContainerProperty("Selezionato", CheckBox.class, false);
		this.containerSilenziatoriMandata.addContainerProperty("Code", String.class, "");
		this.containerSilenziatoriMandata.addContainerProperty("Description", String.class, "");
		this.containerSilenziatoriMandata.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerSilenziatoriMandata.addContainerProperty("Speed", String.class, 0.0);
		this.containerSilenziatoriMandata.addContainerProperty("Details", Button.class, 0.0);
				
		this.tableSilenziatoriMandata.setColumnExpandRatio("Selezionato", 0.5F);
		this.tableSilenziatoriMandata.setColumnExpandRatio("Code", 2);
		this.tableSilenziatoriMandata.setColumnExpandRatio("Description", 6);
		this.tableSilenziatoriMandata.setColumnExpandRatio("Qta", 2);
		this.tableSilenziatoriMandata.setColumnExpandRatio("Speed", 1);
		this.tableSilenziatoriMandata.setColumnExpandRatio("Details", 2);
		
		this.tableSilenziatoriMandata.setColumnHeader("Selezionato", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableSilenziatoriMandata.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableSilenziatoriMandata.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableSilenziatoriMandata.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
		this.tableSilenziatoriMandata.setColumnHeader("Speed", this.l_VG.utilityTraduzioni.TraduciStringa("Velocità"));
		this.tableSilenziatoriMandata.setColumnHeader("Details", this.l_VG.utilityTraduzioni.TraduciStringa("Dettagli"));
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		this.silenziatori = new Cliente04GestioneSilenziatori(this.l_VG);
		
		this.containerBox.removeAllItems();
		this.containerBox.addContainerProperty("Selezionato", CheckBox.class, false);
		this.containerBox.addContainerProperty("Code", String.class, "");
		this.containerBox.addContainerProperty("Description", String.class, "");
		this.containerBox.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerBox.addContainerProperty("Details", Button.class, 0.0);
		
		this.containerSilenziatoriAspirazione.removeAllItems();
		this.containerSilenziatoriAspirazione.addContainerProperty("Selezionato", CheckBox.class, false);
		this.containerSilenziatoriAspirazione.addContainerProperty("Code", String.class, "");
		this.containerSilenziatoriAspirazione.addContainerProperty("Description", String.class, "");
		this.containerSilenziatoriAspirazione.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerSilenziatoriAspirazione.addContainerProperty("Speed", String.class, 0.0);
		this.containerSilenziatoriAspirazione.addContainerProperty("Details", Button.class, 0.0);
		
		this.containerSilenziatoriMandata.removeAllItems();
		this.containerSilenziatoriMandata.addContainerProperty("Selezionato", CheckBox.class, false);
		this.containerSilenziatoriMandata.addContainerProperty("Code", String.class, "");
		this.containerSilenziatoriMandata.addContainerProperty("Description", String.class, "");
		this.containerSilenziatoriMandata.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerSilenziatoriMandata.addContainerProperty("Speed", String.class, 0.0);
		this.containerSilenziatoriMandata.addContainerProperty("Details", Button.class, 0.0);
		
		this.tableBox.setContainerDataSource(this.containerBox);
		this.tableSilenziatoriAspirazione.setContainerDataSource(this.containerSilenziatoriAspirazione);
		this.tableSilenziatoriMandata.setContainerDataSource(this.containerSilenziatoriMandata);    
	}

	public void setVentilatoreCurrent(final String idGruppoIn, final String descrizioneGruppoIn) {
		initContainerSilenziatori();
                this.silenziatori.caricaBoxConAccessori(tableBox, this.l_VG.VentilatoreCurrent.selezioneCorrente.box);
		this.silenziatori.caricaSilenziatoreInTabellaWithInfo(this.tableSilenziatoriAspirazione, false);
		this.silenziatori.caricaSilenziatoreInTabellaWithInfo(this.tableSilenziatoriMandata, true);
                if (l_VG.VentilatoreCurrent.Trasmissione || !l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E04"))
                    buttonPickBox.setEnabled(false);
		//this.silenziatori.caricaBox(this.tableBox);
        }
    
	public void setVentilatoreCurrent() {
		initContainerSilenziatori();
		this.silenziatori.caricaSilenziatoreInTabellaWithInfo(this.tableSilenziatoriAspirazione, false);
		this.silenziatori.caricaSilenziatoreInTabellaWithInfo(this.tableSilenziatoriMandata, true);
		for (int i=0 ; i<this.elencoParametri.size() ; i++) {
			this.elencoParametri.get(i).setVisible(false);
		}
                if (l_VG.VentilatoreCurrent.Trasmissione || !l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E04"))
                    buttonPickBox.setEnabled(false);
     }

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.verticalLayout = new XdevVerticalLayout();
		this.verticalLayout4 = new XdevVerticalLayout();
		this.image = new XdevImage();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.hlButtons = new XdevHorizontalLayout();
		this.tableSilenziatoriAspirazione = new XdevTable<>();
		this.tableSilenziatoriMandata = new XdevTable<>();
		this.tableBox = new XdevTable<>();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.buttonPickBox = new XdevButton();
		this.buttonPickSilencerInlet = new XdevButton();
		this.buttonPickSilencerOutlet = new XdevButton();
	
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.verticalLayout.setSpacing(false);
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.hlButtons.setMargin(new MarginInfo(false, true, false, true));
		this.buttonPickBox.setCaption("Pick Box");
		this.buttonPickBox.setStyleName("mybuttonstyle");
		this.buttonPickSilencerInlet.setCaption("Pick Inlet Silencer");
		this.buttonPickSilencerInlet.setStyleName("mybuttonstyle");
		this.buttonPickSilencerOutlet.setCaption("Pick Outlet Silencer");
		this.buttonPickSilencerOutlet.setStyleName("mybuttonstyle");
		
		this.tableBox.setCaption("Box");
		this.tableBox.setStyleName("small striped");
		this.tableSilenziatoriAspirazione.setCaption("Aspirazione");
		this.tableSilenziatoriAspirazione.setStyleName("small striped");
		this.tableSilenziatoriMandata.setCaption("Mandata");
		this.tableSilenziatoriMandata.setStyleName("small striped");
		this.buttonPickBox.setSizeUndefined();
		this.buttonPickSilencerInlet.setSizeUndefined();
		this.buttonPickSilencerOutlet.setSizeUndefined();

		this.verticalLayout2.setMargin(new MarginInfo(false, true, false, true));
		//this.panelForLogo.setTabIndex(0);
		/*this.verticalLayout7.setMargin(new MarginInfo(false));
		this.panel.setTabIndex(0);
		this.verticalLayout3.setSpacing(false);
		this.verticalLayout3.setMargin(new MarginInfo(false));*/
		this.image.setWidth(-1, Unit.PIXELS);
		this.image.setHeight(100, Unit.PERCENTAGE);
		this.verticalLayout4.addComponent(this.image);
		this.verticalLayout4.setComponentAlignment(this.image, Alignment.MIDDLE_CENTER);
		this.verticalLayout4.setExpandRatio(this.image, 10.0F);
		this.verticalLayout4.setSizeFull();
		this.hlButtons.addComponent(this.buttonPickSilencerInlet);
		this.hlButtons.setComponentAlignment(this.buttonPickSilencerInlet, Alignment.MIDDLE_CENTER);
		this.hlButtons.setExpandRatio(this.buttonPickSilencerInlet, 10.0F);
		this.hlButtons.addComponent(this.buttonPickBox);
		this.hlButtons.setComponentAlignment(this.buttonPickBox, Alignment.MIDDLE_CENTER);
		this.hlButtons.setExpandRatio(this.buttonPickBox, 10.0F);
		this.hlButtons.addComponent(this.buttonPickSilencerOutlet);
		this.hlButtons.setComponentAlignment(this.buttonPickSilencerOutlet, Alignment.MIDDLE_CENTER);
		this.hlButtons.setExpandRatio(this.buttonPickSilencerOutlet, 10.0F);
		this.hlButtons.setWidth(100, Unit.PERCENTAGE);
		this.hlButtons.setHeight(-1, Unit.PIXELS);
		

		
		this.verticalLayout.addComponent(this.hlButtons);
		this.verticalLayout.setComponentAlignment(this.hlButtons, Alignment.MIDDLE_CENTER);
		this.tableBox.setSizeFull();
		this.verticalLayout.addComponent(this.tableBox);
		this.verticalLayout.setComponentAlignment(this.tableBox, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableBox, 8F);
		this.tableSilenziatoriAspirazione.setSizeFull();
		this.verticalLayout.addComponent(this.tableSilenziatoriAspirazione);
		this.verticalLayout.setComponentAlignment(this.tableSilenziatoriAspirazione, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableSilenziatoriAspirazione, 10.0F);
		this.tableSilenziatoriMandata.setSizeFull();
		this.verticalLayout.addComponent(this.tableSilenziatoriMandata);
		this.verticalLayout.setComponentAlignment(this.tableSilenziatoriMandata, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableSilenziatoriMandata, 10.0F);
		
		/*this.panel.setSizeFull();
		this.verticalLayout2.addComponent(this.panel);
		this.verticalLayout2.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.panel, 100.0F);
		this.verticalLayout.setSizeFull();*/
		this.verticalLayout.setSizeFull();
		this.horizontalLayout.addComponent(this.verticalLayout);
		this.horizontalLayout.setComponentAlignment(this.verticalLayout, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.verticalLayout, 40.0F);
		this.verticalLayout2.setSizeFull();
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	
		/*this.tableSilenziatori.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				Cliente04AccessoriViewSilenziatori.this.tableSilenziatori_valueChange(event);
			}
		});*/
		
		this.buttonPickSilencerInlet.addClickListener(event -> this.buttonPickSilencerInlet_buttonClick(event));
		this.buttonPickSilencerOutlet.addClickListener(event -> this.buttonPickSilencerOutlet_buttonClick(event));
		this.buttonPickBox.addClickListener(event -> this.buttonPickBox_buttonClick(event));
	} // </generated-code>

	private void buttonPickBox_buttonClick(final ClickEvent event) {
                final List<Map<String, Object>> esiste = this.l_VG.dbTecnico.getBox(
				this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo,
				this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza,
				this.l_VG.VentilatoreCurrent.Modello,
                                this.l_VG.VentilatoreCurrent.Serie);
                
                ArrayList<Integer> misure = new ArrayList<>();
                // ho estratto tutti i box per la dimensione corrente. Devo controllare S M o L
                String codiceCliente = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente;
                String letteraFinale = codiceCliente.charAt(3) + "";
                if (!"S".equals(letteraFinale) && !"M".equals(letteraFinale) && !"L".equals(letteraFinale)) letteraFinale = ""; 
                int codiceBox = -1;
                for (int i = 0; i<esiste.size(); i++ ) {
                    String Max = "0";
                    if (!((String)esiste.get(i).get("MaxSizeL")).equals("")) {
                       Max = (String)esiste.get(i).get("MaxSizeL"); 
                    }
                    if ("".equals(letteraFinale)) {
                        switch (this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) {
                            case 0:
                                codiceBox = (int)esiste.get(i).get("Rot0");
                                misure.add((int)esiste.get(i).get("AltMis0"));
                                misure.add((int)esiste.get(i).get("AltIng0"));
                                misure.add((int)esiste.get(i).get("Peso0"));
                                break;
                            case 90:
                                codiceBox = (int)esiste.get(i).get("Rot26");
                                misure.add((int)esiste.get(i).get("AltMis26"));
                                misure.add((int)esiste.get(i).get("AltIng26"));
                                misure.add((int)esiste.get(i).get("Peso26"));
                                break;
                            case 270:
                                codiceBox = (int)esiste.get(i).get("Rot26");
                                misure.add((int)esiste.get(i).get("AltMis26"));
                                misure.add((int)esiste.get(i).get("AltIng26"));
                                misure.add((int)esiste.get(i).get("Peso26"));
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                    if (letteraFinale.charAt(0) >= Max.charAt(0)) {
                        switch (this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo) {
                            case 0:
                                codiceBox = (int)esiste.get(i).get("Rot0");
                                misure.add((int)esiste.get(i).get("AltMis0"));
                                misure.add((int)esiste.get(i).get("AltIng0"));
                                misure.add((int)esiste.get(i).get("Peso0"));
                                break;
                            case 90:
                                codiceBox = (int)esiste.get(i).get("Rot26");
                                misure.add((int)esiste.get(i).get("AltMis26"));
                                misure.add((int)esiste.get(i).get("AltIng26"));
                                misure.add((int)esiste.get(i).get("Peso26"));
                                break;
                            case 270:
                                codiceBox = (int)esiste.get(i).get("Rot26");
                                misure.add((int)esiste.get(i).get("AltMis26"));
                                misure.add((int)esiste.get(i).get("AltIng26"));
                                misure.add((int)esiste.get(i).get("Peso26"));
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                    codiceBox = -1;
                }
                if (esiste.size()==0 || codiceBox<0) {
			final MessageBox msgBox = MessageBox.createWarning();
			msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Attenzione"));
    		msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Box non disponibile per questa selezione") + System.lineSeparator() + this.l_VG.utilityTraduzioni.TraduciStringa("Verificare fattibilità con ufficio tecnico"));
    		msgBox.withOkButton();
    		msgBox.open();
		} else {
                    this.l_VG.impostaBox(this.tableBox, codiceBox, misure);
		}
	}
	
	private void buttonPickSilencerInlet_buttonClick(final ClickEvent event) {
		// True mandata, False aspirazione
		this.silenziatori.caricamentoSilenziatori(false, this.tableSilenziatoriAspirazione);

	}
	
	private void buttonPickSilencerOutlet_buttonClick(final ClickEvent event) {
		// True mandata, False aspirazione
		this.silenziatori.caricamentoSilenziatori(true, this.tableSilenziatoriMandata);
		

	}

	// <generated-code name="variables">
	private XdevButton buttonPickSilencerOutlet, buttonPickSilencerInlet, buttonPickBox;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2, hlButtons;
	private XdevImage image;
	private XdevTable<CustomComponent> tableSilenziatoriMandata, tableSilenziatoriAspirazione, tableBox;
	private XdevPanel panel;
	private XdevVerticalLayout verticalLayout, verticalLayout4, verticalLayout2, verticalLayout7, verticalLayout3;
	// </generated-code>

}
