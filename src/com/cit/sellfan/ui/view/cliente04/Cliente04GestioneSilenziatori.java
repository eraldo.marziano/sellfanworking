package com.cit.sellfan.ui.view.cliente04;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.pannelli.pannelloDettagli;
import com.vaadin.data.Property;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.Box;
import cit.sellfan.classi.SelezioneCorrente;
import cit.sellfan.classi.Silenziatore;
import cit.sellfan.classi.Sostegno;
import cit.sellfan.classi.Tramoggia;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import de.steinwedel.messagebox.MessageBox;
import java.util.List;
import java.util.Map;

public class Cliente04GestioneSilenziatori {
	public VariabiliGlobali l_VG;
	public ArrayList<Silenziatore> elencoSilenziatoriPossibiliAspirazione = new ArrayList<>();
	public ArrayList<Silenziatore> elencoSilenziatoriPossibiliMandata = new ArrayList<>();
	public Cliente04GestioneSilenziatori(final VariabiliGlobali l_VG) {
		super();
		this.l_VG = l_VG;
	}
	
	/*public void caricaSilenziatoriDaPreventivo(final XdevTable<CustomComponent> tableSilenziatori) {
		tableSilenziatori.removeAllItems();
		final DecimalFormat dec = new DecimalFormat("#0.0");
		final SelezioneCorrente selCor = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		
		// Mandata
		if (selCor.silenziatoreMandata != null && !selCor.silenziatoreMandata.equals("")) {
			final String dimensioniFlangiaMandata = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).AxBMandata;
			final ArrayList<Object> datiSilMan = this.l_VG.dbTecnico.getSilenziatoreFromCode(selCor.silenziatoreMandata);
			final int diametroCircolare = (int)datiSilMan.get(3);
			final ArrayList<Integer> datiTraMan = this.l_VG.dbTecnico.getTramoggia(dimensioniFlangiaMandata, diametroCircolare);
			final Tramoggia t = new Tramoggia(datiTraMan.get(0), datiTraMan.get(1), datiTraMan.get(2), datiTraMan.get(3));
			t.dimensioni = dimensioniFlangiaMandata;
			final Silenziatore s = new Silenziatore(true, (Integer)datiSilMan.get(2), (Integer)datiSilMan.get(3), t, null, (String)datiSilMan.get(0), (String)datiSilMan.get(1));
			
			if (selCor.vuoleSilMandata) {
				final Object silMan[] = new Object[4];
				silMan[0] = this.l_VG.utilityTraduzioni.TraduciStringa("Mandata");
				silMan[1] = s.codiceSilenziatore;
				silMan[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Cilindrico") + " " + s.descrizioneSilenziatore;
				silMan[3] = dec.format(this.l_VG.getVeloc(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker,s.diametroCircolare));
				tableSilenziatori.addItem(silMan, null);
			}
			if (selCor.vuoleTramoggiaMandata) {
				final Object traMan[] = new Object[4];
				final String tramDesc = dimensioniFlangiaMandata + " DN= " + s.tramoggiaConnessa.diametroCircolare + " L= " + s.tramoggiaConnessa.lunghezza;
	    		traMan[0] = this.l_VG.utilityTraduzioni.TraduciStringa("Mandata");
				traMan[1] = "";
				traMan[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Tramoggia") + ": " + dimensioniFlangiaMandata + selCor.descTramMandata;
				traMan[3] = "";
				tableSilenziatori.addItem(traMan, null);
			}
		}
		
		//Aspirazione
		if (selCor.silenziatoreAspirazione != null && !selCor.silenziatoreAspirazione.equals("")) {
			final String dimensioniFlangiaAspirazione = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).FiAspirazione;
			final ArrayList<Object> datiSilAsp = this.l_VG.dbTecnico.getSilenziatoreFromCode(selCor.silenziatoreAspirazione);
			final int diametroCircolare = (int)(datiSilAsp.get(3));
			final ArrayList<Integer> datiTraAsp = this.l_VG.dbTecnico.getTramoggiaAspirazione(dimensioniFlangiaAspirazione, diametroCircolare);
			final Tramoggia t = new Tramoggia(datiTraAsp.get(0), datiTraAsp.get(1), datiTraAsp.get(2), datiTraAsp.get(3));
			t.dimensioni = dimensioniFlangiaAspirazione;
			final Sostegno sos = createSostegno(diametroCircolare);
			final Silenziatore s = new Silenziatore(false, (Integer)(datiSilAsp.get(2)), (Integer)(datiSilAsp.get(3)), t, sos, (String)datiSilAsp.get(0), (String)datiSilAsp.get(1));
			
			if (selCor.vuoleSilAspirazione) {
				final Object silAsp[] = new Object[4];
				silAsp[0] = this.l_VG.utilityTraduzioni.TraduciStringa("Aspirazione");
				silAsp[1] = s.codiceSilenziatore;
				silAsp[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Cilindrico") + " " + s.descrizioneSilenziatore;
				silAsp[3] = dec.format(this.l_VG.getVeloc(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker,s.diametroCircolare));
				tableSilenziatori.addItem(silAsp, null);
			}
			if (selCor.vuoleTramoggiaAspirazione) {
				final Object traAsp[] = new Object[4];
				final String tramDesc = dimensioniFlangiaAspirazione + " DN= " + s.tramoggiaConnessa.diametroCircolare + " L= " + s.tramoggiaConnessa.lunghezza;
	    		traAsp[0] = this.l_VG.utilityTraduzioni.TraduciStringa("Aspirazione");
	    		traAsp[1] = "";
	    		traAsp[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Riduzione Circolare") + ": " + tramDesc;
	    		traAsp[3] = "";
	    		tableSilenziatori.addItem(traAsp, null);
			}
			if (selCor.vuoleSostegno) {
				final Object sosAsp[] = new Object[4];
				sosAsp[0] = "Aspirazione";
				sosAsp[1] = s.sostegnoSilenziatore.codiceSostegno;
				sosAsp[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Supporto anteriore per silenziatore in Aspirazione") + ": " + s.sostegnoSilenziatore.descrizioneSostegno;
				sosAsp[3] = "";
				tableSilenziatori.addItem(sosAsp, null);
			}
			
		}
		
		
		//Box
		if (this.l_VG.VentilatoreCurrent.selezioneCorrente.codiceBox != null && !this.l_VG.VentilatoreCurrent.selezioneCorrente.codiceBox.equals("")) {
			final String CodiceBox = this.l_VG.VentilatoreCurrent.selezioneCorrente.codiceBox;
			final int rotazione = this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo;
			final int taglia = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza;
			final String modello = this.l_VG.VentilatoreCurrent.Modello;
			final ArrayList<Object> datiBox = this.l_VG.dbTecnico.getBox(rotazione, taglia, modello);
			final Box boxSelezionato = new Box(
					CodiceBox,
					datiBox.get(1).toString(),
					datiBox.get(4).toString(),
					datiBox.get(2).toString(),
					(int)datiBox.get(3),
					(int)datiBox.get(5),
					(int)datiBox.get(6),
					(int)datiBox.get(7),
					(int)datiBox.get(8)
					);
			if (selCor.vuoleSostegno) {
			final Object boxTable[] = new Object[4];
			boxTable[0] = "---";
			boxTable[1] = boxSelezionato.codiceBox;
			boxTable[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Box insonorizzato");
			boxTable[3] = "";
			tableSilenziatori.addItem(boxTable, null);
			}
		}
			
			
	}*/
	public void caricaSilenziatoreInTabellaWithInfo(final XdevTable<CustomComponent> tableSilenziatori, final boolean isMandata) {
		tableSilenziatori.removeAllItems();
		final SelezioneCorrente selCor = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		final DecimalFormat dec = new DecimalFormat("#0.0");
		final Object sil[] = new Object[6];
		Silenziatore actualSil;
		String descrizioneTramoggia;
		String flangia;
		boolean voluto;
		if (isMandata) {
			actualSil = selCor.silMandata;
			flangia = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).AxBMandata;
			voluto = selCor.vuoleSilMandata;
			}
		else {
			actualSil = selCor.silAspirazione;
			flangia = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).FiAspirazione;
			voluto = selCor.vuoleSilAspirazione;
			}
		if (actualSil != null) {
			if (voluto) {
			final CheckBox cb = new CheckBox();
			cb.setValue(true);
			cb.setEnabled(false);
			final TextFieldInteger tf = new TextFieldInteger();
			tf.setValue(Integer.toString(actualSil.qta));
			tf.setEnabled(true);
			tf.setData(actualSil.codiceSilenziatore);
			tf.setWidth("100%");
			tf.addValueChangeListener(e -> {
				actualSil.qta = Integer.parseInt(tf.getValue());
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			
			final Button but = new Button(this.l_VG.utilityTraduzioni.TraduciStringa("Dati Tecnici"));
			but.addClickListener(e -> {
				
				final pannelloDettagli pan = new pannelloDettagli(this.l_VG);
				pan.Nazionalizza();
				pan.loadSilenziatore(actualSil);
				final MessageBox Mbox = MessageBox.create();
				Mbox.withWidth("1200px");
				Mbox.withMessage(pan);
				Mbox.withCloseButton();
				Mbox.open();
				
			});
			
			
			
			sil[0] = cb;
			sil[1] = actualSil.codiceSilenziatore;
			sil[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Cilindrico") + " " + actualSil.descrizioneSilenziatore;
			sil[3] = tf;
			sil[4] = dec.format(this.l_VG.getVeloc(selCor.Marker, actualSil.diametroCircolare));
			sil[5] = but;
			tableSilenziatori.addItem(sil, null);
			
			final Object tra[] = new Object[6];
			final CheckBox cbTra = new CheckBox();
			cbTra.setValue(selCor.vuoleTramoggiaAspirazione);
			cbTra.setImmediate(true);
			cbTra.addValueChangeListener(e -> {
				selCor.vuoleTramoggiaAspirazione = cbTra.getValue().booleanValue();
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			final TextFieldInteger tfTra = new TextFieldInteger();
			tfTra.setValue(Integer.toString(actualSil.tramoggiaConnessa.qta));
			tfTra.setEnabled(true);
			tfTra.setData(actualSil.codiceSilenziatore);
			tfTra.setWidth("100%");
			tfTra.addValueChangeListener(e -> {
				actualSil.tramoggiaConnessa.qta = Integer.parseInt(tfTra.getValue());
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			if (isMandata) {
				descrizioneTramoggia = this.l_VG.utilityTraduzioni.TraduciStringa("Tramoggia") + ": " + flangia + actualSil.tramoggiaConnessa.descrizione;
			} else {
				descrizioneTramoggia = this.l_VG.utilityTraduzioni.TraduciStringa("Riduzione Circolare") + ": " + flangia + actualSil.tramoggiaConnessa.descrizione;
			}

			final com.vaadin.ui.Button butTra = new Button(this.l_VG.utilityTraduzioni.TraduciStringa("Dati Tecnici"));
			butTra.addClickListener(e -> {
				
				final pannelloDettagli pan = new pannelloDettagli(this.l_VG);
				pan.Nazionalizza();
				pan.loadTramoggia(actualSil);
				final MessageBox Mbox = MessageBox.create();
				Mbox.withWidth("1200px");
				Mbox.withMessage(pan);
				Mbox.withCloseButton();
				Mbox.open();
				
			});
			tra[0] = cbTra;
			tra[1] = "";
			tra[2] = descrizioneTramoggia;
			tra[3] = tfTra;
			tra[4] = "";
			tra[5] = butTra;
			tableSilenziatori.addItem(tra, null);
			
			if (!isMandata) {
			final Object sos[] = new Object[6];
			final CheckBox cbSos = new CheckBox();
			cbSos.setValue(selCor.vuoleSostegno);
			cbSos.setEnabled(true);
			cbSos.setImmediate(true);
			cbSos.addValueChangeListener(e -> {
				selCor.vuoleSostegno = cbSos.getValue().booleanValue();
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			final TextFieldInteger tfSos = new TextFieldInteger();
			tfSos.setValue(Integer.toString(actualSil.sostegnoSilenziatore.qta));
			tfSos.setEnabled(true);
			tfSos.setData(actualSil.sostegnoSilenziatore.codiceSostegno);
			tfSos.setWidth("100%");
			tfSos.addValueChangeListener(e -> {
				this.l_VG.VentilatoreCurrent.selezioneCorrente.silAspirazione.sostegnoSilenziatore.qta = Integer.parseInt(tfSos.getValue());
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			final Button butSos = new Button(this.l_VG.utilityTraduzioni.TraduciStringa("Dati Tecnici"));
			sos[0] = cbSos;
			sos[1] = actualSil.sostegnoSilenziatore.codiceSostegno;
			sos[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Supporto anteriore per silenziatore in Aspirazione") + ": " + actualSil.sostegnoSilenziatore.descrizioneSostegno;
			sos[3] = tfSos;
			sos[4] = "";
			sos[5] = butSos;
			butSos.setEnabled(false);
			tableSilenziatori.addItem(sos, null);
			}
			
			//inseriamo gli accessori
			for (final Accessorio a : actualSil.listaAccessori) {
					final Object obj[] = new Object[6];
					final CheckBox cbAcc = new CheckBox();
					cbAcc.setValue(a.Selezionato);
					cbAcc.setData(a.Codice);
					cbAcc.setImmediate(true);
					cbAcc.setEnabled(a.Forzato);
					cbAcc.addValueChangeListener(e -> {
						final String codeAccessorio = cbAcc.getData().toString();
						final Accessorio acc = actualSil.getAccessorio(codeAccessorio);
						acc.Selezionato = cbAcc.getValue().booleanValue();
						verificaAccessoriSilenziatori(tableSilenziatori, isMandata, e.getProperty());
						this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
						caricaSilenziatoreInTabellaWithInfo(tableSilenziatori, isMandata);
					});
					final Button butAcc = new Button(this.l_VG.utilityTraduzioni.TraduciStringa("Dati Tecnici"));
					butAcc.setData(a);
					butAcc.addClickListener(e -> {
						final pannelloDettagli pan = new pannelloDettagli(this.l_VG);
						pan.Nazionalizza();
						final Button acc = (Button) e.getSource();
						final Accessorio caller = (Accessorio) acc.getData();
						pan.loadAccessorio(caller);
						final MessageBox Mbox = MessageBox.create();
						Mbox.withWidth("1200px");
						Mbox.withMessage(pan);
						Mbox.withCloseButton();
						Mbox.open();
						
					});
					obj[0] = cbAcc;
					obj[1] = a.Codice;
					obj[2] = this.l_VG.utilityCliente.nazionalizzaDescrizioneAccessorio(a);
					final TextFieldInteger tfAcc = new TextFieldInteger();
					tfAcc.setValue(Integer.toString(a.qta));
					tfAcc.setEnabled(a.Forzato);
					tfAcc.setData(a.Codice);
					tfAcc.setWidth("100%");
					tfAcc.addValueChangeListener(e -> {
						final String codeAccessorio = tfAcc.getData().toString();
						final Accessorio acc = actualSil.getAccessorio(codeAccessorio);
						if (acc != null) {
							final int value = tfAcc.getIntegerValue();
							acc.qta = value;
							this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
						}
					});
					obj[3] = tfAcc;
					obj[4] = "";
					obj[5] = butAcc;
					if (a.Codice.contains("_") || a.Codice.equals("GAC") || a.Codice.equals("GAT")) {
						butAcc.setEnabled(false);
					}
                                        boolean hide = false;
                                        if(this.l_VG.HideInox302) {
                                            if (a.Codice.contains("_I")) hide = true;
                                        }
                                        if(this.l_VG.HideInox304) {
                                            if (a.Codice.contains("_X") && (!a.Codice.contains("TS_XI"))) hide = true;
                                        }
                                        if (!hide) tableSilenziatori.addItem(obj, null);
			}
			}
			}
	}
	
	public void caricaSilenziatoreInTabella(final XdevTable<CustomComponent> tableSilenziatori, final boolean isMandata) {
		tableSilenziatori.removeAllItems();
		final SelezioneCorrente selCor = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		final DecimalFormat dec = new DecimalFormat("#0.0");
		final Object sil[] = new Object[5];
		Silenziatore actualSil;
		String descrizioneTramoggia;
		String flangia;
		boolean voluto;
		if (isMandata) {
			actualSil = selCor.silMandata;
			flangia = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).AxBMandata;
			voluto = selCor.vuoleSilMandata;
			}
		else {
			actualSil = selCor.silAspirazione;
			flangia = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).FiAspirazione;
			voluto = selCor.vuoleSilAspirazione;
			}
		if (actualSil != null) {
			if (voluto) {
			final CheckBox cb = new CheckBox();
			cb.setValue(true);
			cb.setEnabled(false);
			final TextFieldInteger tf = new TextFieldInteger();
			tf.setValue(Integer.toString(actualSil.qta));
			tf.setEnabled(true);
			tf.setData(actualSil.codiceSilenziatore);
			tf.addValueChangeListener(e -> {
				actualSil.qta = Integer.parseInt(tf.getValue());
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			sil[0] = cb;
			sil[1] = actualSil.codiceSilenziatore;
			sil[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Cilindrico") + " " + actualSil.descrizioneSilenziatore;
			sil[3] = tf;
			sil[4] = dec.format(this.l_VG.getVeloc(selCor.Marker, actualSil.diametroCircolare));
			tableSilenziatori.addItem(sil, null);
			
			final Object tra[] = new Object[5];
			final CheckBox cbTra = new CheckBox();
			cbTra.setValue(selCor.vuoleTramoggiaAspirazione);
			cbTra.setImmediate(true);
			cbTra.addValueChangeListener(e -> {
				selCor.vuoleTramoggiaAspirazione = cbTra.getValue().booleanValue();
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			final TextFieldInteger tfTra = new TextFieldInteger();
			tfTra.setValue(Integer.toString(actualSil.tramoggiaConnessa.qta));
			tfTra.setEnabled(true);
			tfTra.setData(actualSil.codiceSilenziatore);
			tfTra.addValueChangeListener(e -> {
				actualSil.tramoggiaConnessa.qta = Integer.parseInt(tfTra.getValue());
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			if (isMandata) {
				descrizioneTramoggia = this.l_VG.utilityTraduzioni.TraduciStringa("Tramoggia") + ": " + flangia + actualSil.tramoggiaConnessa.descrizione;
			} else {
				descrizioneTramoggia = this.l_VG.utilityTraduzioni.TraduciStringa("Riduzione Circolare") + ": " + flangia + actualSil.tramoggiaConnessa.descrizione;
			}
			tra[0] = cbTra;
			tra[1] = "";
			tra[2] = descrizioneTramoggia;
			tra[3] = tfTra;
			tra[4] = "";
			tableSilenziatori.addItem(tra, null);
			
			if (!isMandata) {
			final Object sos[] = new Object[5];
			final CheckBox cbSos = new CheckBox();
			cbSos.setValue(selCor.vuoleSostegno);
			cbSos.setEnabled(true);
			cbSos.setImmediate(true);
			cbSos.addValueChangeListener(e -> {
				selCor.vuoleSostegno = cbSos.getValue().booleanValue();
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			final TextFieldInteger tfSos = new TextFieldInteger();
			tfSos.setValue(Integer.toString(actualSil.sostegnoSilenziatore.qta));
			tfSos.setEnabled(true);
			tfSos.setData(actualSil.sostegnoSilenziatore.codiceSostegno);
			tfSos.addValueChangeListener(e -> {
				this.l_VG.VentilatoreCurrent.selezioneCorrente.silAspirazione.sostegnoSilenziatore.qta = Integer.parseInt(tfSos.getValue());
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			sos[0] = cbSos;
			sos[1] = actualSil.sostegnoSilenziatore.codiceSostegno;
			sos[2] = this.l_VG.utilityTraduzioni.TraduciStringa("Supporto anteriore per silenziatore in Aspirazione") + ": " + actualSil.sostegnoSilenziatore.descrizioneSostegno;
			sos[3] = tfSos;
			sos[4] = "";
			tableSilenziatori.addItem(sos, null);
			}
			
			//inseriamo gli accessori
			for (final Accessorio a : actualSil.listaAccessori) {
					final Object obj[] = new Object[5];
					final CheckBox cbAcc = new CheckBox();
					cbAcc.setValue(a.Selezionato);
					cbAcc.setData(a.Codice);
					cbAcc.setImmediate(true);
					cbAcc.setEnabled(a.Forzato);
					cbAcc.addValueChangeListener(e -> {
						final String codeAccessorio = cbAcc.getData().toString();
						final Accessorio acc = actualSil.getAccessorio(codeAccessorio);
						acc.Selezionato = cbAcc.getValue().booleanValue();
						verificaAccessoriSilenziatori(tableSilenziatori, isMandata, e.getProperty());
						this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
						caricaSilenziatoreInTabellaWithInfo(tableSilenziatori, isMandata);
					});
					obj[0] = cbAcc;
					obj[1] = a.Codice;
					obj[2] = this.l_VG.utilityCliente.nazionalizzaDescrizioneAccessorio(a);
					final TextFieldInteger tfAcc = new TextFieldInteger();
					tfAcc.setValue(Integer.toString(a.qta));
					tfAcc.setEnabled(a.Forzato);
					tfAcc.setData(a.Codice);
					tfAcc.addValueChangeListener(e -> {
						final String codeAccessorio = tfAcc.getData().toString();
						final Accessorio acc = actualSil.getAccessorio(codeAccessorio);
						if (acc != null) {
							final int value = tfAcc.getIntegerValue();
							acc.qta = value;
							this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
						}
					});
					obj[3] = tfAcc;
					obj[4] = "";
					/*if (a.Codice.contains("_")) {
						cbAcc.setEnabled(false);
						tfAcc.setEnabled(false);
					}*/
					tableSilenziatori.addItem(obj, null);
			}
			}
			}
	}

	private void verificaAccessoriSilenziatori(final XdevTable<CustomComponent> tableSilenziatori, final boolean isMandata, final Property property) {
		Silenziatore sil;
		final CheckBox ev = (CheckBox) property;
		final String acc = ev.getData().toString();
		if (isMandata) {
			sil = this.l_VG.VentilatoreCurrent.selezioneCorrente.silMandata;
		} else {
			sil = this.l_VG.VentilatoreCurrent.selezioneCorrente.silAspirazione;
		}
		if (acc.equals("CFA")) {
			if (ev.getValue()) {
				sil.activateAcc("CFA_X", true);
				sil.activateAcc("CFA_I", true);
				sil.activateAcc("CFA_Z", true);
			} else {
				sil.activateAcc("CFA_X", false);
				sil.activateAcc("CFA_I", false);
				sil.activateAcc("CFA_Z", false);
				sil.selectAcc("CFA_X", false);
				sil.selectAcc("CFA_I", false);
				sil.selectAcc("CFA_Z", false);
			}
		}
		if (acc.equals("CFA_X") && ev.getValue()) {
			sil.selectAcc("CFA_I", false);
			sil.selectAcc("CFA_Z", false);
		}
		if (acc.equals("CFA_I") && ev.getValue()) {
			sil.selectAcc("CFA_X", false);
			sil.selectAcc("CFA_Z", false);
		}
		if (acc.equals("CFA_Z") && ev.getValue()) {
			sil.selectAcc("CFA_I", false);
			sil.selectAcc("CFA_X", false);
		}
		if (acc.equals("GA3")) {
			if (ev.getValue()) {
				sil.activateAcc("GA3_X", true);
				sil.activateAcc("GA3_I", true);
			} else {
				sil.activateAcc("GA3_X", false);
				sil.activateAcc("GA3_I", false);
				sil.selectAcc("GA3_X", false);
				sil.selectAcc("GA3_I", false);
			}
		}
		if (acc.equals("GA3_X") && ev.getValue()) {
			sil.selectAcc("GA3_I", false);
		}
		if (acc.equals("GA3_I") && ev.getValue()) {
			sil.selectAcc("GA3_X", false);
		}
		
		if (acc.equals("GA3")) {
			if (ev.getValue()) {
				sil.activateAcc("GA3_X", true);
				sil.activateAcc("GA3_I", true);
				sil.selectAcc("GA6", false);
				sil.activateAcc("GA6_X", false);
				sil.activateAcc("GA6_I", false);
				sil.selectAcc("GA6_X", false);
				sil.selectAcc("GA6_I", false);
				
			} else {
				sil.activateAcc("GA3_X", false);
				sil.activateAcc("GA3_I", false);
				sil.selectAcc("GA3_X", false);
				sil.selectAcc("GA3_I", false);
			}
		}
		if (acc.equals("GA3_X") && ev.getValue()) {
			sil.selectAcc("GA3_I", false);
		}
		if (acc.equals("GA3_I") && ev.getValue()) {
			sil.selectAcc("GA6_X", false);
		}
		
		if (acc.equals("GA6")) {
			if (ev.getValue()) {
				sil.activateAcc("GA6_X", true);
				sil.activateAcc("GA6_I", true);
				sil.selectAcc("GA3", false);
				sil.activateAcc("GA3_X", false);
				sil.activateAcc("GA3_I", false);
				sil.selectAcc("GA3_X", false);
				sil.selectAcc("GA3_I", false);
				
			} else {
				sil.activateAcc("GA6_X", false);
				sil.activateAcc("GA6_I", false);
				sil.selectAcc("GA6_X", false);
				sil.selectAcc("GA6_I", false);
			}
		}
		if (acc.equals("GA6_X") && ev.getValue()) {
			sil.selectAcc("GA6_I", false);
		}
		if (acc.equals("GA6_I") && ev.getValue()) {
			sil.selectAcc("GA6_X", false);
		}
		if (acc.equals("GAS") && ev.getValue()) {
			sil.selectAcc("GAT", false);
		}
		if (acc.equals("GAT") && ev.getValue()) {
			sil.selectAcc("GAS", false);
		}
		if (acc.equals("RETA")) {
			if (ev.getValue()) {
				sil.activateAcc("RETA_X", true);
				sil.activateAcc("RETA_I", true);
			} else {
				sil.activateAcc("RETA_X", false);
				sil.activateAcc("RETA_I", false);
				sil.selectAcc("RETA_X", false);
				sil.selectAcc("RETA_I", false);
			}
		}
		
		if (acc.equals("RETA_X") && ev.getValue()) {
			sil.selectAcc("RETA_I", false);
		}
		if (acc.equals("RETA_I") && ev.getValue()) {
			sil.selectAcc("RETA_X", false);
		}
	}

    public Silenziatore loadSilenziatoreMandata() {
        Cliente04VentilatoreCampiCliente campi_locale;
        campi_locale = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
        final String dimensioniFlangiaMandata = campi_locale.AxBMandata;
        final Double portata = this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker;
        final ArrayList<Integer> listaTramMan = this.l_VG.dbTecnico.getTramoggeMandata(dimensioniFlangiaMandata);
        final ArrayList<Tramoggia> elencoTramoggeMandata = new ArrayList<>();
        for (int i = 0; i < listaTramMan.size() - 4;) {
            final Integer portataMax = listaTramMan.get(i++);
            final Integer diametro = listaTramMan.get(i++);
            final Integer prezzo = listaTramMan.get(i++);
            final Integer lunghezza = listaTramMan.get(i++);
            if (portataMax >= portata) {
                elencoTramoggeMandata.add(new Tramoggia(portataMax, diametro, prezzo, lunghezza));
            }
        }
        ArrayList<Object> listaDatiSilenziatoriMandata = new ArrayList<>();
        this.elencoSilenziatoriPossibiliMandata.clear();
        for (final Tramoggia tra : elencoTramoggeMandata) {
            listaDatiSilenziatoriMandata = this.l_VG.dbTecnico.getSilenziatore(tra.diametroCircolare);
            final double velocita = this.l_VG.getVeloc(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, (Integer) listaDatiSilenziatoriMandata.get(0));
            if (velocita >= 9.5 && velocita <= 13.5) {
                final Silenziatore s = new Silenziatore(true,
                        (Integer) listaDatiSilenziatoriMandata.get(0),
                        (Integer) listaDatiSilenziatoriMandata.get(1),
                        (Integer) listaDatiSilenziatoriMandata.get(2),
                        (Integer) listaDatiSilenziatoriMandata.get(3),
                        (Integer) listaDatiSilenziatoriMandata.get(4),
                        (Integer) listaDatiSilenziatoriMandata.get(5),
                        (Integer) listaDatiSilenziatoriMandata.get(7),
                        tra,
                        null,
                        (String) listaDatiSilenziatoriMandata.get(6));
                this.elencoSilenziatoriPossibiliMandata.add(s);
            }
        }
        return elencoSilenziatoriPossibiliMandata.size()> 0 ? elencoSilenziatoriPossibiliMandata.get(0) : null;
    }
   	public void caricamentoSilenziatori(final boolean isMandata, final XdevTable<CustomComponent> tableSilenziatori) {
		//andare a prendere i dati del ventilatore utili
            Cliente04VentilatoreCampiCliente campi_locale;
            campi_locale = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
            final String dimensioniFlangiaMandata = campi_locale.AxBMandata;
            final String dimensioniFlangiaAspirazione = campi_locale.FiAspirazione;
            final Double portata = this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker;
            final Double fattoreConversionePortata = this.l_VG.UMPortataCorrente.fattoreConversione;
            //cercare nella tabella i dati delle possibili Tramogge, e creare la lista
            final ArrayList<Integer> listaTramMan = this.l_VG.dbTecnico.getTramoggeMandata(dimensioniFlangiaMandata);
            final ArrayList<Integer> listaTramAsp = this.l_VG.dbTecnico.getTramoggeAspirazione(dimensioniFlangiaAspirazione);
            final ArrayList<Tramoggia> elencoTramoggeMandata = new ArrayList<>();
            for (int i = 0; i < listaTramMan.size() - 4;) {
                final Integer portataMax = listaTramMan.get(i++);
                final Integer diametro = listaTramMan.get(i++);
                final Integer prezzo = listaTramMan.get(i++);
                final Integer lunghezza = listaTramMan.get(i++);
                if (portataMax >= portata) {
                    elencoTramoggeMandata.add(new Tramoggia(portataMax, diametro, prezzo, lunghezza));
                }
            }
            final ArrayList<Tramoggia> elencoTramoggeAspirazione = new ArrayList<>();
            for (int i = 0; i < listaTramAsp.size() - 4;) {
                final Integer portataMax = listaTramAsp.get(i++);
                final Integer diametro = listaTramAsp.get(i++);
                final Integer prezzo = listaTramAsp.get(i++);
                final Integer lunghezza = listaTramAsp.get(i++);
                if (portataMax >= portata) {
                    elencoTramoggeAspirazione.add(new Tramoggia(portataMax, diametro, prezzo, lunghezza));
                }
            }

            // Creare le liste di silenziatori
            ArrayList<Object> listaDatiSilenziatoriMandata = new ArrayList<>();
            ArrayList<Object> listaDatiSilenziatoriAspirazione = new ArrayList<>();
            this.elencoSilenziatoriPossibiliMandata.clear();
            this.elencoSilenziatoriPossibiliAspirazione.clear();

            for (final Tramoggia tra : elencoTramoggeMandata) {
                listaDatiSilenziatoriMandata = this.l_VG.dbTecnico.getSilenziatore(tra.diametroCircolare);
                final double velocita = this.l_VG.getVeloc(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, (Integer) listaDatiSilenziatoriMandata.get(0));
                if (velocita >= 9.5 && velocita <= 13.5) {
                    final Silenziatore s = new Silenziatore(true,
                            (Integer) listaDatiSilenziatoriMandata.get(0),
                            (Integer) listaDatiSilenziatoriMandata.get(1),
                            (Integer) listaDatiSilenziatoriMandata.get(2),
                            (Integer) listaDatiSilenziatoriMandata.get(3),
                            (Integer) listaDatiSilenziatoriMandata.get(4),
                            (Integer) listaDatiSilenziatoriMandata.get(5),
                            (Integer) listaDatiSilenziatoriMandata.get(7),
                            tra,
                            null,
                            (String) listaDatiSilenziatoriMandata.get(6));
                    this.elencoSilenziatoriPossibiliMandata.add(s);
                }
            }
            for (final Tramoggia tra : elencoTramoggeAspirazione) {
                listaDatiSilenziatoriAspirazione = this.l_VG.dbTecnico.getSilenziatore(tra.diametroCircolare);
                final double velocita = this.l_VG.getVeloc(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, (Integer) listaDatiSilenziatoriAspirazione.get(0));
                if (velocita >= 9.5 && velocita <= 13.5) {
                    final Sostegno sos = createSostegno((Integer) listaDatiSilenziatoriAspirazione.get(0));
                    final Silenziatore s = new Silenziatore(false,
                            (Integer) listaDatiSilenziatoriAspirazione.get(0),
                            (Integer) listaDatiSilenziatoriAspirazione.get(1),
                            (Integer) listaDatiSilenziatoriAspirazione.get(2),
                            (Integer) listaDatiSilenziatoriAspirazione.get(3),
                            (Integer) listaDatiSilenziatoriAspirazione.get(4),
                            (Integer) listaDatiSilenziatoriAspirazione.get(5),
                            (Integer) listaDatiSilenziatoriAspirazione.get(7),
                            tra,
                            sos,
                            (String) listaDatiSilenziatoriAspirazione.get(6));
                    this.elencoSilenziatoriPossibiliAspirazione.add(s);
                }
            }
//            if (elencoSilenziatoriPossibiliAspirazione.isEmpty()) {
//                int diametroAspirazione = Integer.parseInt(dimensioniFlangiaAspirazione.split("x")[0])-5;
//                listaDatiSilenziatoriAspirazione = l_VG.dbTecnico.getSilenziatore(diametroAspirazione);
//                if (!listaDatiSilenziatoriAspirazione.isEmpty()) {
//                    final double velocita = this.l_VG.getVeloc(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker, (Integer) listaDatiSilenziatoriAspirazione.get(0));
//                    if (velocita >= 9.5 && velocita <= 13.5) {
//                        final Sostegno sos = createSostegno((Integer) listaDatiSilenziatoriAspirazione.get(0));
//                        final Silenziatore s = new Silenziatore(false,
//                                (Integer) listaDatiSilenziatoriAspirazione.get(0),
//                                (Integer) listaDatiSilenziatoriAspirazione.get(1),
//                                (Integer) listaDatiSilenziatoriAspirazione.get(2),
//                                (Integer) listaDatiSilenziatoriAspirazione.get(3),
//                                (Integer) listaDatiSilenziatoriAspirazione.get(4),
//                                (Integer) listaDatiSilenziatoriAspirazione.get(5),
//                                (Integer) listaDatiSilenziatoriAspirazione.get(7),
//                                null,
//                                sos,
//                                (String) listaDatiSilenziatoriAspirazione.get(6));
//                        this.elencoSilenziatoriPossibiliAspirazione.add(s);
//                    }    
//                }
//            }
            if (!isMandata) {
                this.l_VG.impostaSilenziatoreAspirazione(this.elencoSilenziatoriPossibiliAspirazione, tableSilenziatori);
            } else {
                this.l_VG.impostaSilenziatoreMandata(this.elencoSilenziatoriPossibiliMandata, tableSilenziatori);
            }
	}
	private Sostegno createSostegno(final int diametro) {
		final ArrayList<Object> valueList = this.l_VG.dbTecnico.getSostegno(diametro);
		final Sostegno result = new Sostegno(
				valueList.get(0).toString(),
				valueList.get(1).toString(),
				Integer.valueOf(valueList.get(2).toString()));
		return result;
	}
	
	public Box getBox() {
		final List<Map<String, Object>> lista = this.l_VG.dbTecnico.getBox(
				this.l_VG.VentilatoreCurrent.selezioneCorrente.OrientamentoAngolo,
				this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza,
				this.l_VG.VentilatoreCurrent.Modello,
                                this.l_VG.VentilatoreCurrent.Serie);
		final Box result = null;
		return result;
	}
	public Silenziatore getSilenziatore(final String codice, final boolean isMandata) {
		final ArrayList<Object> dataSil = this.l_VG.dbTecnico.getSilenziatoreFromCode(codice);
		final String dimensioniFlangiaMandata = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).AxBMandata;
		final String dimensioniFlangiaAspirazione = ((Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente).FiAspirazione;
		Silenziatore result = null;
		Tramoggia t = null;
		Sostegno s = null;
		if (isMandata) {
			final ArrayList<Integer> datiTraMan = this.l_VG.dbTecnico.getTramoggia(dimensioniFlangiaMandata, (int)dataSil.get(3));
			t = new Tramoggia(datiTraMan.get(0), datiTraMan.get(1), datiTraMan.get(2), datiTraMan.get(3));
			t.dimensioni = dimensioniFlangiaMandata;
		} else {
			final ArrayList<Integer> datiTraAsp = this.l_VG.dbTecnico.getTramoggiaAspirazione(dimensioniFlangiaAspirazione, (int)dataSil.get(3));
			t = new Tramoggia(datiTraAsp.get(0), datiTraAsp.get(1), datiTraAsp.get(2), datiTraAsp.get(3));
			if (this.l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSostegno) {
				final ArrayList<Object> datiSost = this.l_VG.dbTecnico.getSostegno((int)dataSil.get(3));
				s = new Sostegno(datiSost.get(0).toString(), datiSost.get(1).toString(), (int)datiSost.get(2));
			}
			t.dimensioni = dimensioniFlangiaAspirazione;
		}
		result = new Silenziatore(isMandata,
				(Integer)dataSil.get(0),
				(Integer)dataSil.get(1),
				(Integer)dataSil.get(2),
				(Integer)dataSil.get(3),
				(Integer)dataSil.get(4),
				(Integer)dataSil.get(5),
				(Integer)dataSil.get(7),
				t,
				s,
				(String)dataSil.get(6));
		return result;
	}
	
	public Silenziatore searchSilenziatore(final String codice, final boolean isMandata) {
		ArrayList<Silenziatore> lista;
		if (isMandata) {
			lista = this.elencoSilenziatoriPossibiliMandata;
		} else {
			lista = this.elencoSilenziatoriPossibiliAspirazione;
		}
		for (final Silenziatore sil : lista) {
			if (sil.codiceSilenziatore.equals(codice)) {
				return sil;
			}
		}
		return null;
	}

        public void caricaBoxConAccessori(final XdevTable<CustomComponent> tableBox, Box boxScelto) {
                tableBox.removeAllItems();
		final SelezioneCorrente selCor = this.l_VG.VentilatoreCurrent.selezioneCorrente;
		final DecimalFormat dec = new DecimalFormat("#0.0");
                if (boxScelto!=null) {
		final Object box[] = new Object[5];
		final CheckBox cb = new CheckBox();
                        selCor.vuoleBox = true;
			cb.setValue(selCor.vuoleBox);
			cb.setEnabled(true);
			cb.addValueChangeListener(e -> {
				selCor.vuoleBox = cb.getValue();
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
			});
		final TextFieldInteger tf = new TextFieldInteger();
			tf.setValue(Integer.toString(boxScelto.qta));
			tf.setEnabled(true);
			tf.setWidth("100%");
			tf.addValueChangeListener(e -> {
				selCor.box.qta = Integer.parseInt(tf.getValue());
				this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);});
			final Button but = new Button("Dati Tecnici");
			but.addClickListener(e -> {
				final pannelloDettagli pan = new pannelloDettagli(this.l_VG);
				pan.Nazionalizza();
				pan.loadBox(selCor.box);
				final MessageBox Mbox = MessageBox.create();
				Mbox.withWidth("1200px");
				Mbox.withMessage(pan);
				Mbox.withCloseButton();
				Mbox.open();
			});
			
			box[0] = cb;
			box[1] = selCor.box.codiceBox;
			box[2] = this.l_VG.utilityTraduzioni.TraduciStringa(selCor.box.descrizione);
			box[3] = tf;
			box[4] = but;
			tableBox.addItem(box, null);
                        
                
                final CheckBox cbInclusi = new CheckBox() {{setValue(true); setEnabled(false);}};
                final TextFieldInteger tfInclusi = new TextFieldInteger() {{setValue(1); setEnabled(false); setWidth("100%");}};
                final Object inclusi[] = new Object[] {
		cbInclusi, "GA3 + GP3 + AV", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"), tfInclusi, null};
                tableBox.addItem(inclusi, null);

                final CheckBox cb123 = new CheckBox() {{
                        setValue(selCor.box.onsrv123);
			setEnabled(false);
			addValueChangeListener(e -> { selCor.box.onsrv123 = this.getValue(); l_VG.dbCRMv2.storePreventivoFan(l_VG.preventivoCurrent, l_VG.ElencoVentilatoriFromPreventivo);});}};
                final TextFieldInteger tf123 = new TextFieldInteger() {{setValue(1); setEnabled(false); setWidth("100%");}};
                final Object srv123[] = new Object[] { cb123, "Srv123", this.l_VG.utilityTraduzioni.TraduciStringa("Servo Standard") + ": " + selCor.box.srv123, tf123, null};
                tableBox.addItem(srv123, null);
                
                final CheckBox cb3 = new CheckBox() {{
                        setValue(selCor.box.onsrv3);
			setEnabled(false);
			addValueChangeListener(e -> { selCor.box.onsrv3 = this.getValue(); l_VG.dbCRMv2.storePreventivoFan(l_VG.preventivoCurrent, l_VG.ElencoVentilatoriFromPreventivo);});}};
                final TextFieldInteger tf3 = new TextFieldInteger() {{setValue(1); setEnabled(false); setWidth("100%");}};
                final Object srv3[] = new Object[] { cb3, "Srv3", this.l_VG.utilityTraduzioni.TraduciStringa("Servo Maggiorato"), tf3, null};
                if (selCor.box.srv3>0 && selCor.MotoreInstallato.Grandezza>279) {
                    cb123.setValue(false);
                    cb123.setEnabled(false);
                    cb3.setValue(true);
                    tableBox.addItem(srv3, null);
                }              
                
                final CheckBox cbA = new CheckBox() {{
                        setValue(selCor.box.onsrA);
			setEnabled(true);
			addValueChangeListener(e -> { selCor.box.onsrA = this.getValue(); l_VG.dbCRMv2.storePreventivoFan(l_VG.preventivoCurrent, l_VG.ElencoVentilatoriFromPreventivo);});}};
                final TextFieldInteger tfA = new TextFieldInteger() {{setValue(1); setEnabled(false); setWidth("100%");}};
                final Object srvA[] = new Object[] { cbA, "SrA", this.l_VG.utilityTraduzioni.TraduciStringa("Servo ATEX 3GD"), tfA, null};
                tableBox.addItem(srvA, null);
                
                final CheckBox cbGATGPT = new CheckBox() {{
                        setValue(selCor.box.onGATGPT);
			setEnabled(true);
			addValueChangeListener(e -> { selCor.box.onGATGPT = this.getValue(); l_VG.dbCRMv2.storePreventivoFan(l_VG.preventivoCurrent, l_VG.ElencoVentilatoriFromPreventivo);});}};
                final TextFieldInteger tfGATGPT = new TextFieldInteger() {{setValue(1); setEnabled(false); setWidth("100%");}};
                final Object GATGPT[] = new Object[] { cbGATGPT, "GAT + GPT", this.l_VG.utilityTraduzioni.TraduciStringa("Giunti teflonati antistatici"), tfGATGPT, null};
                tableBox.addItem(GATGPT, null);
                
                final CheckBox cbBTS = new CheckBox() {{
                        setValue(selCor.box.onBTS);
			setEnabled(true);
			addValueChangeListener(e -> { selCor.box.onBTS = this.getValue(); l_VG.dbCRMv2.storePreventivoFan(l_VG.preventivoCurrent, l_VG.ElencoVentilatoriFromPreventivo);});}};
                final TextFieldInteger tfBTS = new TextFieldInteger() {{setValue(1); setEnabled(false); setWidth("100%");}};
                final Object srvBTS[] = new Object[] { cbBTS, "BTS", this.l_VG.utilityTraduzioni.TraduciStringa("Tappo Scarico"), tfBTS, null};
                tableBox.addItem(srvBTS, null);
                
                final CheckBox cbBTC = new CheckBox() {{
                        setValue(selCor.box.onBTC);
			setEnabled(true);
			addValueChangeListener(e -> { selCor.box.onBTC = this.getValue(); l_VG.dbCRMv2.storePreventivoFan(l_VG.preventivoCurrent, l_VG.ElencoVentilatoriFromPreventivo);});}};
                final TextFieldInteger tfBTC = new TextFieldInteger() {{setValue(1); setEnabled(false); setWidth("100%");}};
                final Object srvBTC[] = new Object[] { cbBTC, "BTC", this.l_VG.utilityTraduzioni.TraduciStringa("Telaio camino portante"), tfBTC, null};
                tableBox.addItem(srvBTC, null);
                
			}
            }
	}

