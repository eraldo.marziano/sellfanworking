package com.cit.sellfan.ui.view.cliente04;

import java.util.ArrayList;

import com.cit.sellfan.business.AccessoriAzioni;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.pannelli.pannelloAzioneNumerico;
import com.cit.sellfan.ui.view.cliente04.assiali.Cliente04AccessoriViewAssiali;
import com.cit.sellfan.ui.view.cliente04.centrifighi.Cliente04AccessoriViewCentrifughi;
import com.cit.sellfan.ui.view.cliente04.centrifighi.Cliente04AccessoriViewFusione;
import com.vaadin.data.Item;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.UI;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.sellfan.classi.AccessoriGruppo;
import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.GestioneAccessori;
import cit.sellfan.classi.Silenziatore;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.personalizzazioni.cliente04.Cliente04GestioneVentilatori;
import cit.sellfan.personalizzazioni.cliente04.Cliente04UtilityCliente;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import de.steinwedel.messagebox.MessageBox;

public class Cliente04AccessoriAzioni extends AccessoriAzioni {
	private Cliente04UtilityCliente utilityCliente = null;
	
	public Cliente04AccessoriAzioni(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}
	
	@Override
	public void init(final UtilityCliente utilityClienteIn) {
		this.utilityCliente = (Cliente04UtilityCliente)utilityClienteIn;
	}
	
	@SuppressWarnings("unused")
	@Override
	public void eseguiAzioneAccessorioInEsame(final Item item, final Ventilatore l_v, final Accessorio l_a, final ArrayList<AccessoriGruppo> elencoGruppiAccessori) {
        //l_VG.MainView.setMemo("Azione: "+l_a.azioneAssociata+" sel:"+Boolean.toString(l_a.Selezionato));
		final Cliente04GestioneVentilatori gestioneVentilatori = this.utilityCliente.gestioneVentilatori;
        final GestioneAccessori gestioneAccessori = this.utilityCliente.gestioneAccessori;
        //memo=l_a.Codice+" "+l_a.azioneAssociata;
        //l_VG.MainView.setMemo(memo);
        if (l_a.Forzato) {
			l_a.Selezionato = l_a.ValoreForzatura;
		}
        if (l_a.azioneAssociata.equals(Accessorio.accessorioAzioneNessuna)) {
        	gestioneVentilatori.condizionaQtaAccessori(l_v);
        	//if (gestioneVentilatori.condizionaQtaAccessori(l_v)) l_VG.aggiornaAccessoriQtaSelezioni();
        } else if (l_a.azioneAssociata.equals("inverter")) {
            l_v.datiERP327.usaVariatore = gestioneAccessori.isAccessorioSelected(l_v, "INV");
            this.l_VG.regolamento327Algoritmo.setVariatore(l_v.datiERP327.usaVariatore);
        } else if (l_a.azioneAssociata.startsWith("speciale")) {
        	gestioneVentilatori.eseguiAzioneSpecialeAccessorioInEsame(l_v, l_a);
        } else if (l_a.azioneAssociata.startsWith("reload")) {
        	this.utilityCliente.reloadAccessori(l_v, elencoGruppiAccessori);
                if (l_a.azioneAssociata.contains("fusione")) {
                    l_VG.MainView.accessori04Fusione.setMPF(l_a);
                    l_VG.MainView.dimensioni.setMPF(l_a);
                    l_VG.MainView.dimensioniAngolo.setMPF(l_a);
                }
        	if (l_a.azioneAssociata.contains("speciale") && l_a.Selezionato) {
        		gestioneVentilatori.eseguiAzioneSpecialeAccessorioInEsame(l_v, l_a);
        	}
                if (l_a.azioneAssociata.contains("dialogmemo") && l_a.Selezionato) {
        		final MessageBox msgBox = MessageBox.createWarning();
        		msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
        		msgBox.withOkButton();
        		msgBox.open();
        	}
                if (l_a.azioneAssociata.contains("dialogstring")) {
        		final MessageBox msgBox = MessageBox.createQuestion();
        		msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Introdurre") + " " + this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, ""));
        		final XdevTextField msgText = new XdevTextField();
        		msgText.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
                msgBox.withMessage(msgText);
                msgBox.withOkButton(() -> { cambiaDescrizione(item, l_a, msgText.getValue().toString()); });
                msgBox.open();
    		}
        } else if (l_a.azioneAssociata.startsWith("montaggiomotore")) {
        	//memo+=" test atex "+utilityCliente.cliente04FunzioniSpecifiche.getClass().getName()+" ";
            final String atex = this.utilityCliente.cliente04FunzioniSpecifiche.isVentilatoreATEX(l_v, gestioneAccessori);
            if (atex != null) {
                //{"2G", "2D", "2GD", "3G", "3D", "3GD"};
                switch (atex) {
                    case "2G":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2G;
                        break;
                    case "2D":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2D;
                        break;
                    case "2GD":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2GD;
                        break;
                    case "3G":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX3G;
                        break;
                    case "3D":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX3D;
                        break;
                    case "3GD":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX3GD;
                        break;
                    case "2-3D":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2_3D;
                        break;
                    case "2-3G":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2_3G;
                        break;
                    case "2-3GD":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2_3GD;
                        break;
                    default:
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipoMotoreDefault;
                        break;
                }
            } else {
                this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipoMotoreDefault;
            }
			this.utilityCliente.caricaPrezziVentilatore(l_v);
            boolean motoreOK = false;
            if (l_v.selezioneCorrente.MotoreInstallato.CodiceCliente.endsWith("/" + this.utilityCliente.tipiMotoriAmmessi[0])) {
                motoreOK = true;
            }
            boolean needReload = true;
            if (!motoreOK) {
                for (int i=0 ; i<this.utilityCliente.tipiMotoriAmmessi.length ; i++) {
                    final String newMotore = this.utilityCliente.getMotoreAtex(l_v.selezioneCorrente.MotoreInstallato.CodiceCliente, this.utilityCliente.tipiMotoriAmmessi[i]);
                    if (!(newMotore.equals("") || newMotore.equals("-"))) {
                        this.utilityCliente.tipoMotoreCurrent = this.utilityCliente.tipiMotoriAmmessi[i];
                        this.l_VG.cambiaMotore(newMotore);
                        if (this.l_VG.cambiaMotore(newMotore)) {
                        	needReload = false;
                            break;
                        }
                    }
                }
            }
            if (needReload) {
                this.utilityCliente.reloadAccessori(l_v, elencoGruppiAccessori);
            }
/*    da fare anche nel programma stand alone
            if (l_a.azioneAssociata.endsWith("moduloatex")) {
                if (l_a.Selezionato) {
                    if (Cliente04frmModuloAtexForm == null) {
                        Cliente04frmModuloAtexForm = new Cliente04frmModuloAtex(l_VG);
                        Cliente04frmModuloAtexForm.setLocationRelativeTo(null);
                    }
                    Cliente04frmModuloAtexForm.setTitle(l_VG.utilityTraduzioni.TraduciTitolo("Modulo Atex"));
                    Cliente04frmModuloAtexForm.inizializeForm();
                }
            }
*/
        } else if (l_a.azioneAssociata.startsWith("dialogspecial1param") && l_a.Selezionato) {
/*   	fa dare solo per un assiale
            if (dialogMemoSpecialForm == null) {
                dialogMemoSpecialForm = new Cliente04frmDialogSpecialMemo(l_VG);
                dialogMemoSpecialForm.setLocationRelativeTo(null);
            }
            dialogMemoSpecialForm.setTitle(l_VG.utilityTraduzioni.TraduciStringa("Introdurre") + " " + utilityCliente.sostituisciValue(l_a.Descrizione, ""));
            dialogMemoSpecialForm.lblMemo.setText(utilityCliente.getDialogString(l_a.DialogID));
            dialogMemoSpecialForm.inizializeForm();
            dialogMemoSpecialForm.setVisible(true);
            if (dialogMemoSpecialForm.retOK) {
                l_a.Descrizione = utilityCliente.sostituisciValue(l_a.Descrizione, dialogMemoSpecialForm.retValue);
                l_VG.pannelloAccessori.aggiornaDescrizioni();
                if (l_VG.ElencoVentilatoriVisualizzatiFrom == Costanti.vieneDaPreventivo) {
                    l_VG.pannelloContoEconomico.aggiornaDescrizioni();
                }
            }
*/
        } else if (l_a.azioneAssociata.equals("giunto") && l_a.Selezionato) {
        	final Cliente04SceltaGiuntoWindow win = new Cliente04SceltaGiuntoWindow();
        	win.setVariabiliGlobali(this.l_VG);
        	win.init(item, l_a);
        	UI.getCurrent().addWindow(win);
        } else if (l_a.azioneAssociata.startsWith("dialog") && l_a.Selezionato) {
    		if (l_a.azioneAssociata.endsWith("memo")) {
        		final MessageBox msgBox = MessageBox.createWarning();
        		msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
        		msgBox.withOkButton();
        		msgBox.open();
    		} else if (l_a.azioneAssociata.contains("double-")) {
        		final MessageBox msgBox = MessageBox.createQuestion();
        		msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Introdurre") + " " + this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, ""));
        		final pannelloAzioneNumerico pannello = new pannelloAzioneNumerico();
        		final UnitaMisura l_um = this.l_VG.utilityUnitaMisura.getUM(l_a.azioneAssociata.replace("dialogdouble-", ""));
        		pannello.setUnitaMisura(l_um);
        		pannello.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
        		msgBox.withMessage(pannello);
        		msgBox.withOkButton(() -> { cambiaDescrizione(item, l_a, pannello.getValue()); });
        		msgBox.open();
    		} else if (l_a.azioneAssociata.contains("combo-")) {
                final String comboValues[] = this.utilityCliente.getComboValues(Integer.parseInt(l_a.azioneAssociata.replaceAll("dialogcombo-", "")));
                if (comboValues.length > 0) {
            		final MessageBox msgBox = MessageBox.createQuestion();
            		msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Selezionare") + " " + this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, ""));
                    if (comboValues == null) { return; }
                    final XdevComboBox<CustomComponent> msgCombo = new XdevComboBox<>();
                    msgCombo.setSizeFull();
                    msgCombo.setNullSelectionAllowed(false);
                    for (int i= 0; i<comboValues.length ; i++) {
                    	msgCombo.addItem(comboValues[i]);
                        if (comboValues[i]==null || comboValues[i].toString().equals("")) {
                            msgCombo.setNullSelectionItemId(msgCombo.getItem(i));
                        }
                    }
                    msgCombo.setValue(comboValues[0]);
                    msgCombo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
                    msgBox.withMessage(msgCombo);
                    msgBox.withWidth("40%");
                    msgBox.withOkButton(() -> {cambiaDescrizione(item, l_a, msgCombo.getValue().toString()); });
                    msgBox.open();
    			}
                
    		} else if (l_a.azioneAssociata.contains("string")) {
        		final MessageBox msgBox = MessageBox.createQuestion();
        		msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Introdurre") + " " + this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, ""));
        		final XdevTextField msgText = new XdevTextField();
        		msgText.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
                msgBox.withMessage(msgText);
                msgBox.withOkButton(() -> { cambiaDescrizione(item, l_a, msgText.getValue().toString()); });
                msgBox.open();
    		}
        }
		this.l_VG.MainView.refreshAccessori();
	}
        
    public void filtraAccessoriDonaldson(Ventilatore VentilatoreCurrent) {
        final GestioneAccessori gestioneAccessori = this.utilityCliente.gestioneAccessori;
        if (gestioneAccessori.isAccessorioSelected(VentilatoreCurrent, "FS1"))
            gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "FSTD", false);
        
        if  (VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E04")) {
            gestioneAccessori.forzaAccessorio(VentilatoreCurrent, "SA", true);
        }
        if (gestioneAccessori.isAccessorioSelected(VentilatoreCurrent, "OFR"))
            gestioneAccessori.getAccessorio(VentilatoreCurrent, "COL").enabled = false;
        else
            gestioneAccessori.getAccessorio(VentilatoreCurrent, "COL").enabled = true;
        
        if  (gestioneAccessori.isAccessorioSelected(VentilatoreCurrent, "FS1"))
            gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "FSTD", false);
            
    }
    
    
    public void caricaPopup(Item item, Ventilatore VentilatoreCurrent, Accessorio l_a, ArrayList<AccessoriGruppo> elencoGruppiAccessori) {
        final Cliente04GestioneVentilatori gestioneVentilatori = this.utilityCliente.gestioneVentilatori;
        final GestioneAccessori gestioneAccessori = this.utilityCliente.gestioneAccessori;
        if (!l_a.Codice.equals("OFR") && l_a.azioneAssociata.startsWith("dialog") && l_a.Selezionato) {
            if (l_a.azioneAssociata.endsWith("memo")) {
                final MessageBox msgBox = MessageBox.createWarning();
                msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
                msgBox.withOkButton();
                msgBox.open();
            } else if (l_a.azioneAssociata.contains("double-")) {
                final MessageBox msgBox = MessageBox.createQuestion();
                msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Introdurre") + " " + this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, ""));
                final pannelloAzioneNumerico pannello = new pannelloAzioneNumerico();
                final UnitaMisura l_um = this.l_VG.utilityUnitaMisura.getUM(l_a.azioneAssociata.replace("dialogdouble-", ""));
                pannello.setUnitaMisura(l_um);
                pannello.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
                msgBox.withMessage(pannello);
                msgBox.withOkButton(() -> { cambiaDescrizione(item, l_a, pannello.getValue()); });
                msgBox.open();
            } else if (l_a.azioneAssociata.contains("combo-")) {
                final String comboValues[] = this.utilityCliente.getComboValues(Integer.parseInt(l_a.azioneAssociata.replaceAll("dialogcombo-", "")));
                if (comboValues.length > 0) {
                    final MessageBox msgBox = MessageBox.createQuestion();
                    msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Selezionare") + " " + this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, ""));
                    if (comboValues == null) { return; }
                    final XdevComboBox<CustomComponent> msgCombo = new XdevComboBox<>();
                    msgCombo.setSizeFull();
                    msgCombo.setNullSelectionAllowed(false);
                    for (int i= 0; i<comboValues.length ; i++) {
                        msgCombo.addItem(comboValues[i]);
                        if (comboValues[i].toString().equals("")) {
                            msgCombo.setNullSelectionItemId(msgCombo.getItem(i));
                        }
                    }
                    msgCombo.setValue(comboValues[0]);
                    msgCombo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
                    msgBox.withMessage(msgCombo);
                    msgBox.withWidth("40%");
                    msgBox.withOkButton(() -> {cambiaDescrizione(item, l_a, msgCombo.getValue().toString()); });
                    msgBox.open();
                }
            } else if (l_a.azioneAssociata.contains("string")) {
                final MessageBox msgBox = MessageBox.createQuestion();
                msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Introdurre") + " " + this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, ""));
                final XdevTextField msgText = new XdevTextField();
                msgText.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.utilityCliente.getDialogString(l_a.DialogID)));
                msgBox.withMessage(msgText);
                msgBox.withOkButton(() -> { cambiaDescrizione(item, l_a, msgText.getValue().toString()); });
                msgBox.open();
            }
        }
        else if (l_a.azioneAssociata.startsWith("dialog") && !l_a.Selezionato) {
            String[] pieces = l_a.Descrizione.split("- ");
            String replace = pieces[1];
            String nuovaDescrizione = l_a.Descrizione.replace(pieces[1], "???");
            l_a.Descrizione = nuovaDescrizione;
            if (item!=null)
                item.getItemProperty("Description").setValue(l_a.Descrizione);
        }
        else if (l_a.azioneAssociata.startsWith("montaggiomotore")) {
        	//memo+=" test atex "+utilityCliente.cliente04FunzioniSpecifiche.getClass().getName()+" ";
            final String atex = l_a.Codice;
            if (atex != null) {
                //{"2G", "2D", "2GD", "3G", "3D", "3GD"};
                switch (atex) {
                    case "2G":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2G;
                        break;
                    case "2D":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2D;
                        break;
                    case "2GD":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2GD;
                        break;
                    case "3G":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX3G;
                        break;
                    case "3D":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX3D;
                        break;
                    case "3GD":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX3GD;
                        break;
                    case "2-3D":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2_3D;
                        break;
                    case "2-3G":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2_3G;
                        break;
                    case "2-3GD":
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipiMotoriATEX2_3GD;
                        break;
                    default:
                        this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipoMotoreDefault;
                        break;
                }
            } else {
                this.utilityCliente.tipiMotoriAmmessi = this.utilityCliente.tipoMotoreDefault;
            }
			this.utilityCliente.caricaPrezziVentilatore(VentilatoreCurrent);
            boolean motoreOK = false;
            if (VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente.endsWith("/" + this.utilityCliente.tipiMotoriAmmessi[0])) {
                motoreOK = true;
            }
            boolean needReload = true;
            if (!motoreOK) {
                for (int i=0 ; i<this.utilityCliente.tipiMotoriAmmessi.length ; i++) {
                    final String newMotore = this.utilityCliente.getMotoreAtex(VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente, this.utilityCliente.tipiMotoriAmmessi[i]);
                    if (!(newMotore.equals("") || newMotore.equals("-"))) {
                        this.utilityCliente.tipoMotoreCurrent = this.utilityCliente.tipiMotoriAmmessi[i];
                        this.l_VG.cambiaMotore(newMotore);
                        if (this.l_VG.cambiaMotore(newMotore)) {
                        	needReload = false;
                            break;
                        }
                    }
                }
            }
        }
        this.l_VG.MainView.refreshAccessori();
    }
        
        
    @SuppressWarnings("unchecked")
	private void cambiaDescrizione(final Item item, final Accessorio l_a, final String paramValue) {
    	if (paramValue == null || paramValue.equals("")) {
			return;
		}
    	l_a.Descrizione = this.utilityCliente.sostituisciValueParamAccessorio(l_a.Descrizione, paramValue);
    	if (item != null) {
    		item.getItemProperty("Description").setValue(l_a.Descrizione);
    	}
        if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
			final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente)this.l_VG.VentilatoreCurrent.campiCliente;
                    if (l_VG.isDonaldson()) {
                        final Cliente04AccessoriViewDonaldson view = (Cliente04AccessoriViewDonaldson)l_VG.ElencoView[l_VG.AccessoriViewIndex];
                        view.setVentilatoreCurrent();
                    } else {
                        if (l_vcc04.Tipo.equals("centrifugo")) {
                        final Cliente04AccessoriViewCentrifughi view = (Cliente04AccessoriViewCentrifughi)this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex];
                        view.setVentilatoreCurrent();
                            } else if (l_vcc04.Tipo.equals("fusione")) {
                        final Cliente04AccessoriViewFusione view = (Cliente04AccessoriViewFusione)this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex];
                        view.setVentilatoreCurrent();
                            } else if (l_vcc04.Tipo.equals("assiale")) {
                        final Cliente04AccessoriViewAssiali view = (Cliente04AccessoriViewAssiali)this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex];
                        view.setVentilatoreCurrent();
                            }
                    }
        }
    }
}
