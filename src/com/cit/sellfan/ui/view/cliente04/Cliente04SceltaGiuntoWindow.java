package com.cit.sellfan.ui.view.cliente04;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Window;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.UtilityCliente;
import cit.sellfan.personalizzazioni.cliente04.Cliente04UtilityCliente;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;

public class Cliente04SceltaGiuntoWindow extends Window {
	private VariabiliGlobali l_VG;
	public String oldCodice;
	public String newCodice;
	private final ArrayList<CheckBox> ElencochkSelezionato = new ArrayList<>();
	private final ArrayList<String> ElencoCodici = new ArrayList<>();
	private final Container containerGiunti = new IndexedContainer();
	private Accessorio giunto;
	private Item item;
	/**
	 * 
	 */
	public Cliente04SceltaGiuntoWindow() {
		super();
		this.initUI();
		setHeight("50%");
		setWidth("50%");
		center();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		this.containerGiunti.removeAllItems();
		this.containerGiunti.addContainerProperty("Selezionato", CheckBox.class, false);
		this.containerGiunti.addContainerProperty("Descrizione", String.class, "");
		this.containerGiunti.addContainerProperty("Coppia", String.class, "");
		this.containerGiunti.addContainerProperty("Foro", String.class, "");
		this.tableGiunti.setContainerDataSource(this.containerGiunti);
		Nazionalizza();
	}
	
	public void init(final Item itemIn, final Accessorio l_a) {
		this.item = itemIn;
		this.giunto = l_a;
		this.oldCodice = l_a.parametriVal[0];
		this.newCodice = this.oldCodice;
		initTableGiunti();
	}
	
	public void Nazionalizza() {
		if (this.l_VG.gestioneAccessori.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "INV")) {
			this.labelTitolo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Selezione Giunto per Avviamento Dolce (Inverter)"));
		} else {
			this.labelTitolo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Selezione Giunto per Avviamento Diretto"));
		}
		this.tableGiunti.setColumnHeader("Selezionato", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableGiunti.setColumnHeader("Descrizione", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableGiunti.setColumnHeader("Coppia", this.l_VG.utilityTraduzioni.TraduciStringa("Coppia Nominale [Nm]"));
		this.tableGiunti.setColumnHeader("Foro", this.l_VG.utilityTraduzioni.TraduciStringa("Foro Max [mm]"));
	}
	
	private void abilitaOK() {
		this.buttonOK.setEnabled(false);
		for (int i=0 ; i<this.ElencochkSelezionato.size() ; i++) {
			if (this.ElencochkSelezionato.get(i).getValue()) {
				this.buttonOK.setEnabled(true);
				return;
			}
		}
	}
	
	public void initTableGiunti() {
		boolean inverterSelezionato = false;
		final UtilityCliente l_u = this.l_VG.utilityCliente;
        final Cliente04VentilatoreCampiCliente l_vcc = (Cliente04VentilatoreCampiCliente)this.l_VG.VentilatoreCurrent.campiCliente;
        if (l_u.elencoGiunti.size() <= 0) {
			l_u.loadElencoGiunti();
		}
		if (this.l_VG.gestioneAccessori.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "INV")) {
			inverterSelezionato = true;
		}
        final double coppiaVentilatore = (60. * this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW * 1000.)/(2. * Math.PI * this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
        boolean utilizzabile = false;
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
		for (int i=0 ; i<l_u.elencoGiunti.size() ; i++) {
            if (inverterSelezionato) {
            	utilizzabile = l_u.isGiuntoSelezionabile(l_u.elencoGiunti.get(i), coppiaVentilatore, this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.DiametroAlberomm, l_vcc.DiametroMonobloccomm, true);
            } else {
            	utilizzabile = l_u.isGiuntoSelezionabile(l_u.elencoGiunti.get(i), coppiaVentilatore, this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.DiametroAlberomm, l_vcc.DiametroMonobloccomm, false);
            }
            if (utilizzabile) {
    			final Object obj[] = new Object[4];
            	final String itemID = Integer.toString(i);
            	final CheckBox chkSel = new CheckBox();
    			if (l_u.elencoGiunti.get(i).Modello.equals(this.oldCodice)) {
    				chkSel.setValue(true);
    			}
    			chkSel.setData(l_u.elencoGiunti.get(i).Modello);
    			chkSel.addValueChangeListener(new ValueChangeListener() {
					@Override
						public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
							if (!chkSel.isEnabled()) {
								return;
							}
							Cliente04SceltaGiuntoWindow.this.newCodice = chkSel.getData().toString();
							for (int i=0 ; i<Cliente04SceltaGiuntoWindow.this.ElencochkSelezionato.size() ; i++) {
								if (!Cliente04SceltaGiuntoWindow.this.ElencoCodici.get(i).equals(Cliente04SceltaGiuntoWindow.this.newCodice)) {
									Cliente04SceltaGiuntoWindow.this.ElencochkSelezionato.get(i).setEnabled(false);
									Cliente04SceltaGiuntoWindow.this.ElencochkSelezionato.get(i).setValue(false);
									Cliente04SceltaGiuntoWindow.this.ElencochkSelezionato.get(i).setEnabled(true);
								}
							}
							abilitaOK();
						}
			    });
    			this.ElencochkSelezionato.add(chkSel);
    			this.ElencoCodici.add(l_u.elencoGiunti.get(i).Modello);
    			obj[0] = chkSel;
    			obj[1] = this.l_VG.utilityTraduzioni.TraduciStringa(l_u.elencoGiunti.get(i).Descrizione) + " - " + l_u.elencoGiunti.get(i).Modello;
    			if (inverterSelezionato) {
    				obj[2] = this.l_VG.fmtNd.format(l_u.elencoGiunti.get(i).CoppiaNominaleInverter);
    			} else {
    				obj[2] = this.l_VG.fmtNd.format(l_u.elencoGiunti.get(i).CoppiaNominale);
    			}
    			obj[3] = this.l_VG.fmtNd.format(l_u.elencoGiunti.get(i).ForoMax);
    			this.tableGiunti.addItem(obj, itemID);
            }
		}
		abilitaOK();
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonOK}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	@SuppressWarnings("unchecked")
	private void buttonOK_buttonClick(final Button.ClickEvent event) {
		final Cliente04UtilityCliente l_u = (Cliente04UtilityCliente)this.l_VG.utilityCliente;
		final String descAtex = l_u.Atex != null ? " + extra Atex " + l_u.Atex : "";
		final double multAtex = l_u.Atex != null ? 1.25 : 1.0;
		this.giunto.parametriVal[0] = this.newCodice;
		l_u.gestioneVentilatori.fillDatiGiunto(this.l_VG.VentilatoreCurrent, this.giunto);
		this.giunto.Prezzo *= multAtex;
		this.giunto.Descrizione = this.l_VG.utilityTraduzioni.TraduciStringa(this.giunto.DescrizioneIT) + " - " + this.newCodice + descAtex;
		l_u.gestioneVariazioneRpm(this.l_VG.VentilatoreCurrent);
    	if (this.item != null) {
			this.item.getItemProperty("Description").setValue(this.giunto.Descrizione);
		}
		close();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.labelTitolo = new XdevLabel();
		this.tableGiunti = new XdevTable<>();
		this.buttonOK = new XdevButton();
	
		this.setModal(true);
		this.labelTitolo.setValue("LabelTitolo");
		this.tableGiunti.setStyleName("small striped");
		this.buttonOK.setCaption("OK");
	
		this.labelTitolo.setSizeUndefined();
		this.verticalLayout.addComponent(this.labelTitolo);
		this.verticalLayout.setComponentAlignment(this.labelTitolo, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.labelTitolo, 10.0F);
		this.tableGiunti.setSizeFull();
		this.verticalLayout.addComponent(this.tableGiunti);
		this.verticalLayout.setComponentAlignment(this.tableGiunti, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.tableGiunti, 100.0F);
		this.buttonOK.setSizeUndefined();
		this.verticalLayout.addComponent(this.buttonOK);
		this.verticalLayout.setComponentAlignment(this.buttonOK, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.buttonOK, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.buttonOK.addClickListener(event -> this.buttonOK_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevTable<CustomComponent> tableGiunti;
	private XdevLabel labelTitolo;
	private XdevButton buttonOK;
	private XdevVerticalLayout verticalLayout; // </generated-code>


}
