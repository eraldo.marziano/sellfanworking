package com.cit.sellfan.ui.view.cliente04;

import cit.sellfan.classi.ParametriVari;
import java.util.Collection;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;

import de.steinwedel.messagebox.MessageBox;

public class Cliente04SetupProtettiView extends XdevView {
    private VariabiliGlobali l_VG;
    private final Container containerSerie = new IndexedContainer();
    private boolean editEnabled;

    public Cliente04SetupProtettiView() {
        super();
        this.initUI();
        this.checkBoxMenuStyle.setVisible(false);
        this.containerSerie.removeAllItems();
        this.containerSerie.addContainerProperty("Selected", XdevCheckBox.class, true);
        this.containerSerie.addContainerProperty("Serie", String.class, "");
        this.containerSerie.addContainerProperty("Description", String.class, "");
        this.tableSerie.setContainerDataSource(this.containerSerie);
    }

    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        this.checkBoxHideMz.setValue(this.l_VG.ParametriVari.hideMzPerProtetti);
    }

    public void Nazionalizza() {
        this.panelSerie.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Selezionare le serie di ventilatori che si desidera utilizzare"));
        this.buttonTutte.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Seleziona tutte"));
        this.buttonNessuna.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Deseleziona tutte"));
        this.tableSerie.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
        this.tableSerie.setColumnHeader("Serie", this.l_VG.utilityTraduzioni.TraduciStringa("Serie"));
        this.tableSerie.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
        this.labelMemo.setValue("<htnl><b><font color='blue'>" + this.l_VG.utilityTraduzioni.TraduciStringa("Serie selezionate"+":") + " 0</html>");
        this.checkBoxGrafici.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Visualizza Sfondo Grafici"));
        this.checkBoxSpettro.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Visualizza Sfondo Spettro Rumore"));
        this.checkBoxPoliPot.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Abilita controllo Poli/Potenza"));
        this.checkBoxClassi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Disabilita Controllo Classi"));
        this.checkBoxEditVentola.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Abilita scelta diametro ventola") +": " +this.l_VG.utilityTraduzioni.TraduciStringa("Verificare fattibilità con ufficio tecnico"));
        this.checkBoxHideMz.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nascondi MZ da datasheet"));
        this.checkBoxMotorCustom.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Abilita selezione motore Speciale"));
        this.labelTolleranzaCentrifugo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tolleranza su Portata"));
        this.labelTolleranzaAssiale.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tolleranza Ricerca Ventilatori Assiali"));
        this.TolleranzaPressione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Tolleranza su Pressione"));
        this.comboDefaultTolleranza.removeAllItems();
        this.comboDefaultTolleranza.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Tolleranza su"));
        this.comboDefaultTolleranza.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Pressione"));
        this.comboDefaultTolleranza.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Portata"));
        this.comboDefaultTolleranza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.ParametriVari.defaultTolleranza));
    }

    public void initSetup() {
        this.checkBoxGrafici.setValue(this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag);
        this.checkBoxSpettro.setValue(this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag);
        this.checkBoxClassi.setValue(this.l_VG.ParametriVari.pannelloCatalogoTutteLeClassiFlag);
        this.checkBoxPoliPot.setValue(!this.l_VG.ParametriVari.controlloPoliPotenza);
        this.textOverRpm.setValue(this.l_VG.ParametriVari.pannelloCondizioniOverRpm);
        this.checkBoxMenuStyle.setValue(this.l_VG.ParametriVariHTML.menuStyleEnabled);
        this.checkBoxMotorCustom.setValue(l_VG.ParametriVari.showCustomMotorButton);
        buildTabellaSerie();
        try {
            if (this.l_VG.ParametriVari.pannelloRicercaTolleranzaPressioneMeno != 0.0) {
            TolleranzaPressioneMeno.setValue((int)(l_VG.ParametriVari.pannelloRicercaTolleranzaPressioneMeno * 100));
            TolleranzaPressionePiu.setValue((int)(l_VG.ParametriVari.pannelloRicercaTolleranzaPressionePiu * 100));
            } else {
                TolleranzaPressioneMeno.setValue(15);
                TolleranzaPressionePiu.setValue(10);
            }
            this.tfTolleranzaCentPiu.setValue((int)(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezionePIU * 100.));
            this.tfTolleranzaCentMeno.setValue((int)(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO * 100.));
            this.tfTolleranzaAssialePiu.setValue((int)(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssPIU * 100.));
            this.tfTolleranzaAssialeMeno.setValue((int)(this.l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssMENO * 100.));
            String defaultT = (this.l_VG.ParametriVari.defaultTolleranza == null) ? "Pressione" : this.l_VG.ParametriVari.defaultTolleranza;
            this.comboDefaultTolleranza.setValue(this.l_VG.utilityTraduzioni.TraduciStringa(defaultT));
        }
        catch (Exception e) {
            Notification.show("Manca ParametriVari");
        }
    }

    public void azioneReset() {
        this.checkBoxGrafici.setValue(true);
        this.checkBoxSpettro.setValue(true);
        this.checkBoxPoliPot.setValue(true);
        this.checkBoxClassi.setValue(false);
        this.textOverRpm.setValue(1.1);
        this.checkBoxMenuStyle.setValue(false);
        this.tfTolleranzaCentPiu.setValue(15);
        this.tfTolleranzaCentMeno.setValue(5);
        this.tfTolleranzaAssialePiu.setValue(20);
        this.tfTolleranzaAssialeMeno.setValue(10);
        TolleranzaPressioneMeno.setValue(15);
        TolleranzaPressionePiu.setValue(10);
        azioneOK();
        final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
        this.editEnabled = false;
        this.l_VG.ElencoSerieDisponibili.clear();
        for(final Object id : Ventilatoriids){
            final Item item = this.containerSerie.getItem(id);
            final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
            cb.setValue(true);
        }
        contaSerieSelezionate();
        this.editEnabled = true;
        serieOK();
    }

    public void azioneOK() {
        variOK();
        serieOK();
    }

    private void serieOK() {
        final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
        this.editEnabled = false;
        this.l_VG.ElencoSerieDisponibili.clear();
        for(final Object id : Ventilatoriids) {
            final Item item = this.containerSerie.getItem(id);
            final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
            final String sr = (String)item.getItemProperty("Serie").getValue();
            for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
                if (this.l_VG.ElencoSerie.get(i).SerieTrans.equals(sr)) {
                    this.l_VG.ElencoSerieDisponibili.add(cb.getValue());
                    this.l_VG.ElencoSerie.get(i).UtilizzabileRicerca = cb.getValue();
                }
            }
        }
        this.editEnabled = true;
        this.l_VG.dbConfig.storeSerieDisponibili(this.l_VG.ElencoSerieDisponibili);
    }

    private void variOK() {
        l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag = checkBoxGrafici.getValue();
        l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag = checkBoxSpettro.getValue();
        l_VG.ParametriVari.pannelloCatalogoTutteLeClassiFlag = checkBoxClassi.getValue();
        l_VG.ParametriVari.controlloPoliPotenza = !checkBoxPoliPot.getValue();
        l_VG.editVentola = checkBoxEditVentola.getValue();
        l_VG.ParametriVari.pannelloRicercaTolleranzaSelezionePIU = tfTolleranzaCentPiu.getIntegerValue() / 100.;
        l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneMENO = tfTolleranzaCentMeno.getIntegerValue() / 100.;
        l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssPIU = tfTolleranzaAssialePiu.getIntegerValue() / 100.;
        l_VG.ParametriVari.pannelloRicercaTolleranzaSelezioneAssMENO = tfTolleranzaAssialeMeno.getIntegerValue() / 100.;
        l_VG.ParametriVari.pannelloRicercaTolleranzaPressioneMeno = TolleranzaPressioneMeno.getDoubleValue() / 100;
        l_VG.ParametriVari.pannelloRicercaTolleranzaPressionePiu = TolleranzaPressionePiu.getDoubleValue() / 100;
        l_VG.dbTecnico.pannelloCatalogoTutteLeClassiFlag = l_VG.ParametriVari.pannelloCatalogoTutteLeClassiFlag;
        l_VG.ParametriVari.pannelloCondizioniOverRpm = textOverRpm.getDoubleValue();
        l_VG.ParametriVariHTML.menuStyleEnabled = checkBoxMenuStyle.getValue();
        l_VG.mzNascosta = checkBoxHideMz.getValue();
        l_VG.ParametriVari.hideMzPerProtetti = checkBoxHideMz.getValue();
        l_VG.ParametriVari.showCustomMotorButton = checkBoxMotorCustom.getValue();
        String defaultT = (comboDefaultTolleranza.getValue().equals("Pressione") || comboDefaultTolleranza.getValue().equals("Pressure") || comboDefaultTolleranza.getValue().equals("")) ? "Pressione" : "Portata";
        l_VG.SelezioneDati.tolleranzaPortataFlag = !defaultT.equals("Portata");
        l_VG.ParametriVari.defaultTolleranza = defaultT;
        l_VG.dbConfig.storeParametriVari(l_VG.ParametriVari);
        l_VG.dbConfig.storeParametriVariHTML(l_VG.ParametriVariHTML);
        l_VG.MainView.Nazionalizza();
    }

    private void buildTabellaSerie() {
        editEnabled = false;
        try {
            tableSerie.removeAllItems();//resetta anche le selection
        } catch (final Exception e) { }
        for (int i=0 ; i<l_VG.ElencoSerie.size() ; i++) {
            final String itemID = Integer.toString(i);
            final Object obj[] = new Object[3];
            final XdevCheckBox cb = new XdevCheckBox();
            cb.setValue(l_VG.ElencoSerieDisponibili.get(i));
            cb.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                if (!Cliente04SetupProtettiView.this.editEnabled) {
                    return;
                }
                Cliente04SetupProtettiView.this.editEnabled = false;
                contaSerieSelezionate();
                Cliente04SetupProtettiView.this.editEnabled = true;
            }
        });
            obj[0] = cb;
            obj[1] = l_VG.ElencoSerie.get(i).SerieTrans;
            obj[2] = l_VG.utilityTraduzioni.TraduciStringa(l_VG.ElencoSerie.get(i).DescrizioneTrans);
            tableSerie.addItem(obj, itemID);
        }
        contaSerieSelezionate();
        editEnabled = true;
    }

    private void contaSerieSelezionate() {
        final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
        int nSerieSelezionate = 0;
        this.editEnabled = false;
        for(final Object id : Ventilatoriids){
            final Item item = this.containerSerie.getItem(id);
            final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
            if (cb.getValue()) {
                nSerieSelezionate++;
            }
        }
        this.editEnabled = true;
        this.labelMemo.setValue("<htnl><b><font color='blue'>" + this.l_VG.utilityTraduzioni.TraduciStringa("Serie selezionate"+":") + " " + Integer.toString(nSerieSelezionate) + "</html>");
    }

    private void buttonTutte_buttonClick(final Button.ClickEvent event) {
        final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
        this.editEnabled = false;
        this.tableSerie.setEnabled(false);
        for(final Object id : Ventilatoriids){
            final Item item = this.containerSerie.getItem(id);
            final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
            cb.setValue(true);
        }
        this.tableSerie.setEnabled(true);
        this.editEnabled = true;
        contaSerieSelezionate();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonNessuna}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonNessuna_buttonClick(final Button.ClickEvent event) {
            final Collection<?> Ventilatoriids = this.containerSerie.getItemIds();
            this.editEnabled = false;
            this.tableSerie.setEnabled(false);
            for(final Object id : Ventilatoriids){
                    final Item item = this.containerSerie.getItem(id);
                    final XdevCheckBox cb = (XdevCheckBox)item.getItemProperty("Selected").getValue();
                    cb.setValue(false);
            }
            this.tableSerie.setEnabled(true);
            this.editEnabled = true;
            contaSerieSelezionate();
    }

    private void checkBoxEditVentola_valueChange(final Property.ValueChangeEvent event) {
            if (this.checkBoxEditVentola.getValue()) {
                    final MessageBox msgBox = MessageBox.createWarning();
                    msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Attenzione"));
            msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Verificare fattibilità con ufficio tecnico"));
            msgBox.withOkButton();
            msgBox.open();
            }
    }

    /*
     * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
     * the UI designer.
     */
    // <generated-code name="initUI">
    private void initUI() {
            this.horizontalLayout = new XdevHorizontalLayout();
            this.panel = new XdevPanel();
            this.verticalLayout2 = new XdevVerticalLayout();
            this.checkBoxMenuStyle = new XdevCheckBox();
            this.checkBoxGrafici = new XdevCheckBox();
            this.checkBoxSpettro = new XdevCheckBox();
            this.checkBoxClassi = new XdevCheckBox();
            this.checkBoxEditVentola = new XdevCheckBox();
            this.checkBoxHideMz = new XdevCheckBox();
            this.checkBoxPoliPot = new XdevCheckBox();
            this.horizontalLayout3 = new XdevHorizontalLayout();
            this.checkBoxMotorCustom = new XdevCheckBox();
            this.label = new XdevLabel();
            this.textOverRpm = new TextFieldDouble();
            this.horizontalLayout4 = new XdevHorizontalLayout();
            this.horizontalLayout5 = new XdevHorizontalLayout();
            this.labelTolleranzaCentrifugo = new XdevLabel();
            this.labelTolleranzaAssiale = new XdevLabel();
            this.labelMenoAss = new XdevLabel();
            this.labelMenoCen = new XdevLabel();
            this.tfTolleranzaCentMeno = new TextFieldInteger();
            this.tfTolleranzaAssialeMeno = new TextFieldInteger();
            this.labelPiuAss = new XdevLabel();
            this.labelPiuCen = new XdevLabel();
            this.labelPerc1Ass = new XdevLabel();
            this.labelPerc1Cen = new XdevLabel();
            this.tfTolleranzaCentPiu = new TextFieldInteger();
            this.tfTolleranzaAssialePiu = new TextFieldInteger();
            this.labelPerc2Ass = new XdevLabel();
            this.labelPerc2Cen = new XdevLabel();
            this.panelSerie = new XdevPanel();
            this.verticalLayout = new XdevVerticalLayout();
            this.tableSerie = new XdevTable<>();
            this.horizontalLayout2 = new XdevHorizontalLayout();
            this.labelMemo = new XdevLabel();
            this.buttonTutte = new XdevButton();
            this.buttonNessuna = new XdevButton();
            this.comboDefaultTolleranza = new XdevComboBox<CustomComponent>();
            TolleranzaPressioneMenoLabel = new XdevLabel(" -");
            TolleranzaPressioneMeno = new TextFieldInteger();
            TolleranzaPressionePerc1 = new XdevLabel("%");
            TolleranzaPressionePiuLabel = new XdevLabel("+");
            TolleranzaPressionePiu = new TextFieldInteger();
            TolleranzaPressionePerc2 = new XdevLabel("%");
            TolleranzaPressione = new XdevLabel("Tolleranza su Pressione");

            this.checkBoxMenuStyle.setCaption("Menu Style");
            this.checkBoxGrafici.setCaption("Visualizza Sfondo Grafici");
            this.checkBoxSpettro.setCaption("Visualizza Sfondo Spettro Rumore");
            this.checkBoxClassi.setCaption("Disabilita Controllo Classi");
            this.checkBoxEditVentola.setCaption("Abilita scelta diametro ventola");
            this.checkBoxHideMz.setCaption("Nascondi MZ da datasheet");
            this.checkBoxPoliPot.setCaption("Attiva controllo Poli/Potenza");
            this.checkBoxMotorCustom.setCaption("Abilita selezione motore Speciale");
            this.label.setValue("Over Rpm");
            this.labelTolleranzaCentrifugo.setValue("toller");
            this.labelTolleranzaCentrifugo.setContentMode(ContentMode.HTML);
            this.labelTolleranzaAssiale.setValue("toller");
            this.labelTolleranzaAssiale.setContentMode(ContentMode.HTML);
            this.labelMenoAss.setValue(" -");
            this.labelMenoCen.setValue(" -");
            this.labelPerc1Ass.setValue("%");
            this.labelPerc1Cen.setValue("%");
            this.labelPiuAss.setValue(" +");
            this.labelPiuCen.setValue(" +");
            this.labelPerc2Ass.setValue("%");
            this.labelPerc2Cen.setValue("%");
            this.panelSerie.setCaption("titolo");
            this.verticalLayout.setMargin(new MarginInfo(false, true, false, true));
            this.tableSerie.setStyleName("mystriped");
            this.labelMemo.setValue("memo");
            this.labelMemo.setContentMode(ContentMode.HTML);
            this.buttonTutte.setCaption("Tutte");
            this.buttonNessuna.setCaption("nessuna");
            this.comboDefaultTolleranza.setCaption("Default tolleranza su");

            this.label.setWidth(100, Unit.PERCENTAGE);
            this.label.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout3.addComponent(this.label);
            this.horizontalLayout3.setComponentAlignment(this.label, Alignment.MIDDLE_CENTER);
            this.horizontalLayout3.setExpandRatio(this.label, 10.0F);
            this.textOverRpm.setWidth(100, Unit.PERCENTAGE);
            this.textOverRpm.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout3.addComponent(this.textOverRpm);
            this.horizontalLayout3.setComponentAlignment(this.textOverRpm, Alignment.MIDDLE_CENTER);
            this.horizontalLayout3.setExpandRatio(this.textOverRpm, 10.0F);
            this.labelTolleranzaCentrifugo.setWidth(100, Unit.PERCENTAGE);
            this.labelTolleranzaCentrifugo.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout4.addComponent(this.labelTolleranzaCentrifugo );
            this.horizontalLayout4.setComponentAlignment(this.labelTolleranzaCentrifugo, Alignment.TOP_RIGHT);
            this.horizontalLayout4.setExpandRatio(this.labelTolleranzaCentrifugo, 10.0F);
            this.horizontalLayout4.addComponent(this.labelMenoCen);
            this.horizontalLayout4.addComponent(this.tfTolleranzaCentMeno);
            this.horizontalLayout4.addComponent(this.labelPerc1Cen);
            this.horizontalLayout4.addComponent(this.labelPiuCen);
            this.horizontalLayout4.addComponent(this.tfTolleranzaCentPiu);
            this.horizontalLayout4.addComponent(this.labelPerc2Cen);

            this.tfTolleranzaCentPiu.setWidth(50, Unit.PIXELS);
            this.tfTolleranzaCentPiu.setHeight(-1, Unit.PIXELS);
            this.tfTolleranzaCentMeno.setWidth(50, Unit.PIXELS);
            this.tfTolleranzaCentMeno.setHeight(-1, Unit.PIXELS);
            this.labelMenoCen.setSizeUndefined();
            this.labelPerc1Cen.setSizeUndefined();
            this.labelPiuCen.setSizeUndefined();
            this.labelPerc2Cen.setSizeUndefined();
            TolleranzaPressione.setWidth(100, Unit.PERCENTAGE);
            TolleranzaPressione.setHeight(-1, Unit.PIXELS);
            TolleranzaPressioneMeno.setWidth(50, Unit.PIXELS);
            TolleranzaPressioneMeno.setHeight(-1, Unit.PIXELS);
            TolleranzaPressionePiu.setWidth(50, Unit.PIXELS);
            TolleranzaPressionePiu.setHeight(-1, Unit.PIXELS);
            TolleranzaPressioneMenoLabel.setSizeUndefined();
            TolleranzaPressionePerc1.setSizeUndefined();
            TolleranzaPressionePerc2.setSizeUndefined();
            TolleranzaPressionePiuLabel.setSizeUndefined();
            horizontalLayout6 = new XdevHorizontalLayout();
            horizontalLayout6.addComponent(TolleranzaPressione, Alignment.TOP_RIGHT);
            horizontalLayout6.setExpandRatio(TolleranzaPressione, 10.0F);
            horizontalLayout6.addComponents(TolleranzaPressioneMenoLabel, TolleranzaPressioneMeno, TolleranzaPressionePerc1, TolleranzaPressionePiuLabel, TolleranzaPressionePiu, TolleranzaPressionePerc2);
            this.labelTolleranzaAssiale.setWidth(100, Unit.PERCENTAGE);
            this.labelTolleranzaAssiale.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout5.addComponent(this.labelTolleranzaAssiale);
            this.horizontalLayout5.setComponentAlignment(this.labelTolleranzaAssiale, Alignment.TOP_RIGHT);
            this.horizontalLayout5.setExpandRatio(this.labelTolleranzaAssiale, 10.0F);
            this.horizontalLayout5.addComponent(this.labelMenoAss);
            this.horizontalLayout5.addComponent(this.tfTolleranzaAssialeMeno);
            this.horizontalLayout5.addComponent(this.labelPerc1Ass);
            this.horizontalLayout5.addComponent(this.labelPiuAss);
            this.horizontalLayout5.addComponent(this.tfTolleranzaAssialePiu);
            this.horizontalLayout5.addComponent(this.labelPerc2Ass);
            this.tfTolleranzaAssialePiu.setWidth(50, Unit.PIXELS);
            this.tfTolleranzaAssialePiu.setHeight(-1, Unit.PIXELS);
            this.tfTolleranzaAssialeMeno.setWidth(50, Unit.PIXELS);
            this.tfTolleranzaAssialeMeno.setHeight(-1, Unit.PIXELS);
            this.labelMenoAss.setSizeUndefined();
            this.labelPerc1Ass.setSizeUndefined();
            this.labelPiuAss.setSizeUndefined();
            this.labelPerc2Ass.setSizeUndefined();
            this.checkBoxMenuStyle.setSizeUndefined();
            this.checkBoxGrafici.setSizeUndefined();
            this.checkBoxSpettro.setSizeUndefined();
            this.checkBoxClassi.setSizeUndefined();
            this.checkBoxEditVentola.setSizeUndefined();
            this.checkBoxHideMz.setSizeUndefined();
            this.checkBoxPoliPot.setSizeUndefined();
            this.verticalLayout2.addComponent(this.checkBoxMenuStyle, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.checkBoxGrafici, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.checkBoxSpettro, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.checkBoxClassi, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.checkBoxEditVentola, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.checkBoxHideMz, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.checkBoxPoliPot, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(checkBoxMotorCustom, Alignment.MIDDLE_CENTER);

            this.horizontalLayout3.setWidth(100, Unit.PERCENTAGE);
            this.horizontalLayout3.setHeight(-1, Unit.PIXELS);
            this.verticalLayout2.addComponent(this.horizontalLayout3, Alignment.MIDDLE_CENTER);
            this.horizontalLayout4.setWidth(100, Unit.PERCENTAGE);
            this.horizontalLayout4.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout5.setWidth(100, Unit.PERCENTAGE);
            this.horizontalLayout5.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout6.setWidth(100, Unit.PERCENTAGE);
            this.horizontalLayout6.setHeight(-1, Unit.PIXELS);
            this.verticalLayout2.addComponent(this.horizontalLayout4);
            this.verticalLayout2.setComponentAlignment(this.horizontalLayout4, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.horizontalLayout5);
            this.verticalLayout2.setComponentAlignment(this.horizontalLayout5, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.addComponent(this.horizontalLayout6);
            this.verticalLayout2.setComponentAlignment(this.horizontalLayout6, Alignment.MIDDLE_CENTER);
            this.verticalLayout2.setWidth(100, Unit.PERCENTAGE);
            this.verticalLayout2.setHeight(-1, Unit.PIXELS);
            this.verticalLayout2.addComponent(this.comboDefaultTolleranza);
            this.verticalLayout2.setComponentAlignment(this.comboDefaultTolleranza, Alignment.MIDDLE_CENTER);
            this.panel.setContent(this.verticalLayout2);
            this.labelMemo.setWidth(100, Unit.PERCENTAGE);
            this.labelMemo.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout2.addComponent(this.labelMemo);
            this.horizontalLayout2.setComponentAlignment(this.labelMemo, Alignment.MIDDLE_CENTER);
            this.horizontalLayout2.setExpandRatio(this.labelMemo, 10.0F);
            this.buttonTutte.setSizeUndefined();
            this.horizontalLayout2.addComponent(this.buttonTutte);
            this.horizontalLayout2.setComponentAlignment(this.buttonTutte, Alignment.MIDDLE_RIGHT);
            this.horizontalLayout2.setExpandRatio(this.buttonTutte, 10.0F);
            this.buttonNessuna.setSizeUndefined();
            this.horizontalLayout2.addComponent(this.buttonNessuna);
            this.horizontalLayout2.setComponentAlignment(this.buttonNessuna, Alignment.MIDDLE_RIGHT);
            this.horizontalLayout2.setExpandRatio(this.buttonNessuna, 10.0F);
            this.tableSerie.setSizeFull();
            this.verticalLayout.addComponent(this.tableSerie);
            this.verticalLayout.setComponentAlignment(this.tableSerie, Alignment.MIDDLE_CENTER);
            this.verticalLayout.setExpandRatio(this.tableSerie, 10.0F);
            this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
            this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
            this.verticalLayout.addComponent(this.horizontalLayout2);
            this.verticalLayout.setComponentAlignment(this.horizontalLayout2, Alignment.BOTTOM_RIGHT);
            this.verticalLayout.setSizeFull();
            this.panelSerie.setContent(this.verticalLayout);
            this.panel.setWidth(100, Unit.PERCENTAGE);
            this.panel.setHeight(-1, Unit.PIXELS);
            this.horizontalLayout.addComponent(this.panel);
            this.horizontalLayout.setExpandRatio(this.panel, 10.0F);
            this.panelSerie.setSizeFull();
            this.horizontalLayout.addComponent(this.panelSerie);
            this.horizontalLayout.setExpandRatio(this.panelSerie, 20.0F);
            this.horizontalLayout.setSizeFull();
            this.setContent(this.horizontalLayout);
            this.setSizeFull();


            this.buttonTutte.addClickListener(event -> this.buttonTutte_buttonClick(event));
            this.buttonNessuna.addClickListener(event -> this.buttonNessuna_buttonClick(event));
    } // </generated-code>

    // <generated-code name="variables">
    private XdevLabel TolleranzaPressione, TolleranzaPressioneMenoLabel, TolleranzaPressionePerc1, TolleranzaPressionePiuLabel, TolleranzaPressionePerc2, label, labelTolleranzaCentrifugo, labelTolleranzaAssiale, labelMenoAss, labelMenoCen, labelPiuAss, labelPiuCen, labelPerc1Ass, labelPerc1Cen, labelPerc2Ass, labelPerc2Cen, labelMemo;
    private XdevButton buttonTutte, buttonNessuna;
    private XdevHorizontalLayout horizontalLayout, horizontalLayout3, horizontalLayout4, horizontalLayout2, horizontalLayout5, horizontalLayout6;
    private TextFieldDouble textOverRpm;
    private XdevTable<?> tableSerie;
    private XdevPanel panel, panelSerie;
    private XdevCheckBox checkBoxMenuStyle, checkBoxGrafici, checkBoxSpettro, checkBoxClassi, checkBoxEditVentola, checkBoxHideMz, checkBoxPoliPot, checkBoxMotorCustom;
    private XdevVerticalLayout verticalLayout2, verticalLayout;
    private XdevComboBox<CustomComponent> comboDefaultTolleranza;
    private TextFieldInteger tfTolleranzaAssialeMeno, tfTolleranzaAssialePiu, tfTolleranzaCentMeno, tfTolleranzaCentPiu, TolleranzaPressioneMeno, TolleranzaPressionePiu;
    // </generated-code>

}
