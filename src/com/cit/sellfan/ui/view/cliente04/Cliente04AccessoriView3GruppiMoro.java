package com.cit.sellfan.ui.view.cliente04;

import java.util.Arrays;
import java.util.List;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;
import cit.sellfan.classi.Accessorio;
import com.cit.sellfan.ui.utility.Accessori;
import com.cit.sellfan.ui.utility.TableProperties;

public class Cliente04AccessoriView3GruppiMoro extends Cliente04AccessoriView3Gruppi {

    private VariabiliGlobali l_VG;
    private Container containerAccessoriGruppoA = new IndexedContainer();
    private Container containerAccessoriGruppoB = new IndexedContainer();
    private Container containerAccessoriGruppoC = new IndexedContainer();
    private String idGruppoA;
    private String idGruppoB;
    private String idGruppoC;
    private String descrizioneGruppoA;
    private String descrizioneGruppoB;
    private String descrizioneGruppoC;
    private Accessori accessori = new Accessori();
    
    public Cliente04AccessoriView3GruppiMoro() {
        super();
        this.initUI();
    }

    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        accessori.setVariabiliGlobali(l_VG);
        this.containerAccessoriGruppoA.removeAllItems();
        this.containerAccessoriGruppoB.removeAllItems();
        this.containerAccessoriGruppoC.removeAllItems();
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.containerAccessoriGruppoA.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
            this.containerAccessoriGruppoB.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
            this.containerAccessoriGruppoC.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
        }
        this.tableAccessoriGruppoA.setContainerDataSource(this.containerAccessoriGruppoA);
        this.tableAccessoriGruppoB.setContainerDataSource(this.containerAccessoriGruppoB);
        this.tableAccessoriGruppoC.setContainerDataSource(this.containerAccessoriGruppoC);
    }

    private void initContainerAccessoriGruppoA() {
        this.containerAccessoriGruppoA = new IndexedContainer();
        this.containerAccessoriGruppoA.removeAllItems();
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.containerAccessoriGruppoA.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
            this.tableAccessoriGruppoA.setColumnHeader(tp.propertyId, tp.traduzione);
            this.tableAccessoriGruppoA.setColumnExpandRatio(tp.propertyId, tp.larghezza);
        }
        this.tableAccessoriGruppoA.setContainerDataSource(this.containerAccessoriGruppoA);
    }

    private void initContainerAccessoriGruppoB() {
        this.containerAccessoriGruppoB = new IndexedContainer();
        this.containerAccessoriGruppoB.removeAllItems();
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.containerAccessoriGruppoB.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
            this.tableAccessoriGruppoB.setColumnHeader(tp.propertyId, tp.traduzione);
            this.tableAccessoriGruppoB.setColumnExpandRatio(tp.propertyId, tp.larghezza);
        }
        this.tableAccessoriGruppoB.setContainerDataSource(this.containerAccessoriGruppoB);
    }

    private void initContainerAccessoriGruppoC() {
        this.containerAccessoriGruppoC = new IndexedContainer();
        this.containerAccessoriGruppoC.removeAllItems();
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.containerAccessoriGruppoC.addContainerProperty(tp.propertyId, tp.type, tp.defaultValue);
            this.tableAccessoriGruppoC.setColumnHeader(tp.propertyId, tp.traduzione);
            this.tableAccessoriGruppoC.setColumnExpandRatio(tp.propertyId, tp.larghezza);
        }
        this.tableAccessoriGruppoC.setContainerDataSource(this.containerAccessoriGruppoC);
    }

    public void Nazionalizza() {
        for (TableProperties tp : accessori.getListaProprieta()) {
            this.tableAccessoriGruppoA.setColumnHeader(tp.propertyId, tp.traduzione);
            this.tableAccessoriGruppoB.setColumnHeader(tp.propertyId, tp.traduzione);
            this.tableAccessoriGruppoC.setColumnHeader(tp.propertyId, tp.traduzione);
        }
    }

    public void setVentilatoreCurrent(final String idGruppoAIn, final String idGruppoBIn, final String idGruppoCIn, final String descrizioneGruppoAIn, final String descrizioneGruppoBIn, final String descrizioneGruppoCIn) {
        this.idGruppoA = idGruppoAIn;
        this.idGruppoB = idGruppoBIn;
        this.idGruppoC = idGruppoCIn;
        this.descrizioneGruppoA = descrizioneGruppoAIn;
        this.descrizioneGruppoB = descrizioneGruppoBIn;
        this.descrizioneGruppoC = descrizioneGruppoCIn;
        initContainerAccessoriGruppoA();
        buildTableAccessori(this.tableAccessoriGruppoA, this.containerAccessoriGruppoA, this.idGruppoA, this.descrizioneGruppoA);
        initContainerAccessoriGruppoB();
        buildTableAccessori(this.tableAccessoriGruppoB, this.containerAccessoriGruppoB, this.idGruppoB, this.descrizioneGruppoB);
        initContainerAccessoriGruppoC();
        buildTableAccessori(this.tableAccessoriGruppoC, this.containerAccessoriGruppoC, this.idGruppoC, this.descrizioneGruppoC);
    }

    public void setVentilatoreCurrent() {
        initContainerAccessoriGruppoA();
        buildTableAccessori(this.tableAccessoriGruppoA, this.containerAccessoriGruppoA, this.idGruppoA, this.descrizioneGruppoA);
        initContainerAccessoriGruppoB();
        buildTableAccessori(this.tableAccessoriGruppoB, this.containerAccessoriGruppoB, this.idGruppoB, this.descrizioneGruppoB);
        initContainerAccessoriGruppoC();
        buildTableAccessori(this.tableAccessoriGruppoC, this.containerAccessoriGruppoC, this.idGruppoC, this.descrizioneGruppoC);
    }

    private void buildTableAccessori(final XdevTable<CustomComponent> table, final Container container, final String idGruppo, final String descrizioneGruppo) {
        final String titolo = this.l_VG.utilityTraduzioni.TraduciStringa("Accessori Disponibili") + " ";
        if (idGruppo == null || idGruppo.equals("")) {
            table.setVisible(false);
        } else {
            table.setVisible(true);
            if (descrizioneGruppo != null && !descrizioneGruppo.equals("")) {
                table.setCaption(titolo + this.l_VG.utilityTraduzioni.TraduciStringa(descrizioneGruppo));
            } else {
                table.setCaption(titolo);
            }
            for (int i = 0; i < this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); i++) {
                final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(i);
                if (idGruppo.equals("") || idGruppo.equals(l_a.Gruppo)) {
                    final Object obj[] = new Object[5];
                    final String itemID = Integer.toString(i);
                    final CheckBox cb = new CheckBox();
                    cb.setValue(l_a.Selezionato);
                    cb.setData(l_a.Codice);
                    cb.setImmediate(true);
                    cb.addValueChangeListener((final com.vaadin.data.Property.ValueChangeEvent event) -> {
                        final String accessorioCode = cb.getData().toString();
                        final Item item = container.getItem(itemID);
                        Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame = Cliente04AccessoriView3GruppiMoro.this.l_VG.gestioneAccessori.getAccessorio(Cliente04AccessoriView3GruppiMoro.this.l_VG.VentilatoreCurrent, accessorioCode);
                        if (Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame != null) {
                            if (Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame.Forzato && !Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame.Codice.equals("GUS")) {
                                Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame.Selezionato = Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame.ValoreForzatura;
                                cb.setValue(Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame.Selezionato);
                                return;
                            }
                            final boolean value = cb.getValue();
                            Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame.Selezionato = value;
                            Cliente04AccessoriView3GruppiMoro.this.l_VG.condizionaAccessori();
                            Cliente04AccessoriView3GruppiMoro.this.l_VG.accessoriAzioni.eseguiAzioneAccessorioInEsame(item, Cliente04AccessoriView3GruppiMoro.this.l_VG.VentilatoreCurrent, Cliente04AccessoriView3GruppiMoro.this.l_VG.accessorioInEsame, Cliente04AccessoriView3GruppiMoro.this.l_VG.elencoGruppiAccessori);
                            Cliente04AccessoriView3GruppiMoro.this.l_VG.ventilatoreCambiato = true;
                            setVentilatoreCurrent();
                        }
                    });
                    obj[0] = cb;
                    obj[1] = l_a.Codice;
                    obj[2] = this.l_VG.utilityCliente.nazionalizzaDescrizioneAccessorio(l_a);
                    //obj[2] = l_a.Descrizione + l_VG.gestioneAccessori.getAccessorioValoreParametro(l_a, "cod");
                    final TextFieldInteger tf = new TextFieldInteger();
                    tf.setValue(Integer.toString(l_a.qta));
                    tf.setEnabled(l_a.qtaModificabile);
                    tf.setData(l_a.Codice);
                    tf.setWidth("100%");
                    tf.addValueChangeListener((final com.vaadin.data.Property.ValueChangeEvent event) -> {
                        final String accessorioCode = tf.getData().toString();
                        final Accessorio l_aloc = Cliente04AccessoriView3GruppiMoro.this.l_VG.gestioneAccessori.getAccessorio(Cliente04AccessoriView3GruppiMoro.this.l_VG.VentilatoreCurrent, accessorioCode);
                        if (l_aloc != null) {
                            final int value = tf.getIntegerValue();
                            l_aloc.qta = value;
                            Cliente04AccessoriView3GruppiMoro.this.l_VG.MainView.refreshAccessoriPreventivi();
                            Cliente04AccessoriView3GruppiMoro.this.l_VG.ventilatoreCambiato = true;
                            setVentilatoreCurrent();
                        }
                    });
                    obj[3] = tf;
                    obj[4] = this.l_VG.currentCitFont.linguaDisplay.id.equals("UK")? l_a.NoteENG : l_a.NoteITA;
                    boolean hide = false;
                    if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Grandezza < 80 && !this.l_VG.VentilatoreCurrent.Trasmissione) {
                        hide = false;
                        final List<String> elenco = Arrays.asList("CCV", "CCV_X", "CCV_I", "CPC", "CPCO", "CCP");
                        if (elenco.contains(obj[1].toString())) {
                            hide = true;
                        }
                    }
                    if (this.l_VG.HideInox302) {
                        if (l_a.Codice.contains("_I")) {
                            hide = true;
                        }
                    }
                    if (this.l_VG.HideInox304) {
                        if (l_a.Codice.contains("_X") && (!l_a.Codice.contains("TS_XI"))) {
                            hide = true;
                        }
                    }
                    if (this.l_VG.HideInox302 && this.l_VG.HideInox304) {
                        if (l_a.Codice.contains("TS_XI")) {
                            hide = true;
                        }
                    }
                    if (this.l_VG.VentilatoreCurrent.Serie.contains("E")) {
                        if (this.l_VG.VentilatoreCurrent.Trasmissione) {
                            final List<String> elenco = Arrays.asList("C9M", "OVAG", "OVS", "OPV", "OPI");
                            if (elenco.contains(obj[1].toString())) {
                                hide = true;
                            }
                        } else {
                            final List<String> elenco = Arrays.asList("OIA", "OVAG", "OVS", "OPV", "OPI");
                            if (elenco.contains(obj[1].toString())) {
                                hide = true;
                            }
                        }
                    }
                    if (!hide) {
                        table.addItem(obj, itemID);
                    }

                }
            }
        }
    }

    /*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
     */
    // <generated-code name="initUI">
    private void initUI() {
        this.verticalLayout = new XdevVerticalLayout();
        this.tableAccessoriGruppoA = new XdevTable<>();
        this.tableAccessoriGruppoB = new XdevTable<>();
        this.tableAccessoriGruppoC = new XdevTable<>();

        this.verticalLayout.setMargin(new MarginInfo(false));
        this.tableAccessoriGruppoA.setCaption("Accessories available");
        this.tableAccessoriGruppoA.setStyleName("small mystriped");
        this.tableAccessoriGruppoB.setCaption("Accessories available");
        this.tableAccessoriGruppoB.setStyleName("small mystriped");
        this.tableAccessoriGruppoC.setCaption("Accessories available");
        this.tableAccessoriGruppoC.setStyleName("small mystriped");

        this.tableAccessoriGruppoA.setSizeFull();
        this.verticalLayout.addComponent(this.tableAccessoriGruppoA);
        this.verticalLayout.setComponentAlignment(this.tableAccessoriGruppoA, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableAccessoriGruppoA, 100.0F);
        this.tableAccessoriGruppoB.setSizeFull();
        this.verticalLayout.addComponent(this.tableAccessoriGruppoB);
        this.verticalLayout.setComponentAlignment(this.tableAccessoriGruppoB, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableAccessoriGruppoB, 100.0F);
        this.tableAccessoriGruppoC.setSizeFull();
        this.verticalLayout.addComponent(this.tableAccessoriGruppoC);
        this.verticalLayout.setComponentAlignment(this.tableAccessoriGruppoC, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableAccessoriGruppoC, 100.0F);
        this.verticalLayout.setSizeFull();
        this.setContent(this.verticalLayout);
        this.setSizeFull();
    } // </generated-code>

    // <generated-code name="variables">
    private XdevTable<CustomComponent> tableAccessoriGruppoA, tableAccessoriGruppoB, tableAccessoriGruppoC;
    private XdevVerticalLayout verticalLayout; // </generated-code>

}
