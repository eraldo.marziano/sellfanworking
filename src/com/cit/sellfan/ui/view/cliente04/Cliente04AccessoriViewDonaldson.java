package com.cit.sellfan.ui.view.cliente04;

import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.cliente04.Cliente04GestioneSilenziatori;
import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.GestioneAccessori;
import cit.sellfan.classi.Motore;
import cit.sellfan.classi.Silenziatore;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.personalizzazioni.cliente04.Cliente04StampaOffertaPoiXLSX;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import com.cit.sellfan.ui.pannelli.pannelloSelezioneMotore;
import com.cit.sellfan.ui.utility.Accessori;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.xdev.ui.XdevTextField;
import de.steinwedel.messagebox.MessageBox;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;

public class Cliente04AccessoriViewDonaldson extends XdevView {

    private VariabiliGlobali l_VG;
    private final ArrayList<XdevLabel> elencoParametri = new ArrayList<>();
    private Container containerAccessori = new IndexedContainer();
    private Container containerAtex = new IndexedContainer();
    private Container containerMotori = new IndexedContainer();
    private Cliente04GestioneSilenziatori silenziatori;
    private Accessori accessori = new Accessori();
    private final ArrayList<String> ElencoFileXls = new ArrayList<>();
    private FileDownloader fileDownloader;
    private Accessorio Silenziatore;
    private double PrezzoPortello;
    Accessorio sil = new Accessorio();
    Accessorio supporto = new Accessorio();
    Accessorio tramoggia = new Accessorio();
    String codiceSupporto;
    ArrayList<String> codiciAccessoriATEX = new ArrayList<>(Arrays.asList("ANT_C", "2G", "2D", "2GD", "3G", "3D", "3GD", "2-3D", "2-3G", "2-3GD"));
    Silenziatore selez;
    Motore motor, motorPTC;
    public Boolean startingIsPtc;
    public Boolean startingIsPtcAtex = true;
    CheckBox cbMotor = new CheckBox();
//

    public Cliente04AccessoriViewDonaldson() {
        super();
        this.initUI();
    }
    
    public void resetAccessori() {
        for (int i = 0; i < this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); i++) {
            final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(i);
            l_a.Selezionato = false;
        }
        sil.Selezionato = false;
        tramoggia.Selezionato = false;
    }

    public void Nazionalizza() {
        this.tableAccessori.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Accessori"));
        this.tableAtex.setCaption("Atex");
    }

    private void initContainerAccessori() {
        this.containerAccessori = new IndexedContainer();
        this.containerAccessori.removeAllItems();
        this.containerAccessori.addContainerProperty("Selected", CheckBox.class, false);
        this.containerAccessori.addContainerProperty("Description", String.class, "");
        this.containerAccessori.addContainerProperty("Price", String.class, 0.0);

        this.containerAtex = new IndexedContainer();
        this.containerAtex.removeAllItems();
        this.containerAtex.addContainerProperty("Selected", CheckBox.class, false);
        this.containerAtex.addContainerProperty("Description", String.class, "");
        this.containerAtex.addContainerProperty("Price", String.class, 0.0);

        this.containerMotori = new IndexedContainer();
        this.containerMotori.removeAllItems();
        this.containerMotori.addContainerProperty("Code", String.class, "");
        this.containerMotori.addContainerProperty("Description", String.class, "");
        this.containerMotori.addContainerProperty("Kw", String.class, "");
        this.containerMotori.addContainerProperty("PTC", CheckBox.class, false);
        this.containerMotori.addContainerProperty("Price", String.class, "");
        
        this.tableAccessori.setColumnExpandRatio("Selected", 0.5F);
        this.tableAccessori.setColumnExpandRatio("Description", 6);
        this.tableAccessori.setColumnExpandRatio("Price", 3);

        this.tableAccessori.setColumnHeader("Selected", "Selected");
        this.tableAccessori.setColumnHeader("Description", "Description");
        this.tableAccessori.setColumnHeader("Price", "Price");

        this.tableAtex.setColumnExpandRatio("Selected", 0.5F);
        this.tableAtex.setColumnExpandRatio("Description", 6);
        this.tableAtex.setColumnExpandRatio("Price", 3);

        this.tableAtex.setColumnHeader("Selected", "Selected");
        this.tableAtex.setColumnHeader("Description", "Description");
        this.tableAtex.setColumnHeader("Price", "Price");

        this.tableMotori.setColumnExpandRatio("Code", 1.5F);
        this.tableMotori.setColumnExpandRatio("Description", 5);
        this.tableMotori.setColumnExpandRatio("KW", 1);
        this.tableMotori.setColumnExpandRatio("PTC", 0.5F);
        this.tableMotori.setColumnExpandRatio("Price", 3);
        
        this.tableMotori.setColumnHeader("Code", "Code");
        this.tableMotori.setColumnHeader("Description", "Description");
        this.tableMotori.setColumnHeader("KW", "KW");
        this.tableMotori.setColumnHeader("PTC", "PTC");
        this.tableMotori.setColumnHeader("Price", "Price");
        
        this.tableAccessori.setContainerDataSource(this.containerAccessori);
        this.tableAtex.setContainerDataSource(this.containerAtex);
        this.tableMotori.setContainerDataSource(this.containerMotori);
    }

    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        this.silenziatori = new Cliente04GestioneSilenziatori(this.l_VG);
        this.containerAccessori.removeAllItems();
        this.containerAccessori.addContainerProperty("Selected", CheckBox.class, false);
        this.containerAccessori.addContainerProperty("Description", String.class, "");
        this.containerAccessori.addContainerProperty("Price", String.class, 0.0);

        this.containerAtex.removeAllItems();
        this.containerAtex.addContainerProperty("Selected", CheckBox.class, false);
        this.containerAtex.addContainerProperty("Description", String.class, "");
        this.containerAtex.addContainerProperty("Price", String.class, 0.0);
        
        this.containerMotori.removeAllItems();
        this.containerMotori.addContainerProperty("Code", String.class, "");
        this.containerMotori.addContainerProperty("Description", String.class, "");
        this.containerMotori.addContainerProperty("Kw", String.class, "");
        this.containerMotori.addContainerProperty("PTC", CheckBox.class, false);
        this.containerMotori.addContainerProperty("Price", String.class, "");
        
        
        accessori.setVariabiliGlobali(l_VG);
        this.tableAccessori.setContainerDataSource(this.containerAccessori);
        this.tableAtex.setContainerDataSource(this.containerAtex);
        this.tableMotori.setContainerDataSource(this.containerMotori);
    }

    public void setVentilatoreCurrent() {
        this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex] = this;
        initContainerAccessori();
        final GestioneAccessori gestioneAccessori = this.l_VG.utilityCliente.gestioneAccessori;
        for (String s : codiciAccessoriATEX) {
            if (!s.equals("ANT_C")) {
                gestioneAccessori.getAccessorio(l_VG.VentilatoreCurrent, s).enabled = !(l_VG.CARicerca.temperatura > 60);
            }
        }
        buildTableAccessori();

    }

    private void buildTableAccessori() {
        tableAccessori.setVisible(true);
        tableAtex.setVisible(true);
        Accessorio supporto1 = l_VG.gestioneAccessori.getAccessorio(l_VG.VentilatoreCurrent, "SA");
        Accessorio supporto2 = l_VG.gestioneAccessori.getAccessorio(l_VG.VentilatoreCurrent, "SOS");
        supporto = (supporto1 == null) ? supporto2 : supporto1;
        codiceSupporto = supporto != null ? supporto.Codice : "SA";
        l_VG.gestioneAccessori.forzaAccessorio(l_VG.VentilatoreCurrent, codiceSupporto, l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E04"));
        int iDon = 0;
        for (int i = 0; i < this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); i++) {
            final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(i);
            if (l_a.Donaldson) {
                addAccToTable(l_a, iDon++, false);
            }
        }
        if (sil.Codice.equals("")) {
            selez = silenziatori.loadSilenziatoreMandata();
            if (selez != null) {
                sil.NoteITA = "";
                sil.NoteENG = "";
                sil.NonConforme = false;
                sil.Selezionato = false;
                sil.Codice = selez.codiceSilenziatore;
                sil.Descrizione = this.l_VG.utilityTraduzioni.TraduciStringa("Silenziatore Cilindrico") + " " + selez.descrizioneSilenziatore;
                sil.DescrizioneIT = sil.Descrizione;
                sil.Prezzo = selez.prezzo;// + selez.sostegnoSilenziatore.prezzoSostegno;
                sil.Donaldson = true;
                sil.azioneAssociata = "Silenziatore";
                Silenziatore = sil;
                tramoggia.Selezionato = false;
                tramoggia.Codice = "---";
                tramoggia.Descrizione = this.l_VG.utilityTraduzioni.TraduciStringa("Tramoggia") + " " + selez.tramoggiaConnessa.descrizione;
                tramoggia.DescrizioneIT = tramoggia.Descrizione;
                tramoggia.Prezzo = selez.tramoggiaConnessa.prezzo;
                tramoggia.Donaldson = true;
            }
        }
        addAccToTable(sil, iDon++, true);
        addAccToTable(tramoggia, iDon++, true);
        loadMotors();
        setMotor();
    }

    private void addAccToTable(Accessorio acc, int iDon, boolean isSilenziatore) {
        final Object obj[] = new Object[3];
        final String itemID = Integer.toString(iDon);
        final CheckBox cb = new CheckBox();
        cb.setValue(acc.Selezionato || acc.Forzato);
        cb.setData(acc.Codice);
        cb.setEnabled(acc.enabled);
        cb.setImmediate(true);
        cb.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                final String accessorioCode = cb.getData().toString();
                final Item item = containerAccessori.getItem(itemID);
                final boolean value = cb.getValue();
                l_VG.accessorioInEsame = l_VG.gestioneAccessori.getAccessorio(l_VG.VentilatoreCurrent, accessorioCode);
                if (l_VG.accessorioInEsame != null) {
                    if (l_VG.accessorioInEsame.Forzato) {
                        l_VG.accessorioInEsame.Selezionato = l_VG.accessorioInEsame.ValoreForzatura;
                        cb.setValue(l_VG.accessorioInEsame.Selezionato);
                        return;
                    }
                    l_VG.accessorioInEsame.Selezionato = value;
                    l_VG.accessoriAzioni.caricaPopup(item, l_VG.VentilatoreCurrent, l_VG.accessorioInEsame, l_VG.elencoGruppiAccessori);
                    l_VG.ventilatoreCambiato = true;
                }
                if (accessorioCode.startsWith("S") && accessorioCode.endsWith("0Z")) {
                    sil.Selezionato = value;
                    l_VG.VentilatoreCurrent.selezioneCorrente.vuoleSilMandata = value;
                    l_VG.VentilatoreCurrent.selezioneCorrente.silMandata = selez;
                }
                if (accessorioCode.equals("---")) {
                    tramoggia.Selezionato = value;
                    l_VG.VentilatoreCurrent.selezioneCorrente.silMandata = selez;
                    l_VG.VentilatoreCurrent.selezioneCorrente.vuoleTramoggiaMandata = value;
                }
                FiltraAccessori(l_VG.VentilatoreCurrent, l_VG.accessorioInEsame);
                setVentilatoreCurrent();
            }
        });
        obj[0] = cb;
        if (isSilenziatore) {
            obj[1] = acc.Descrizione;
        } else {
            obj[1] = l_VG.utilityCliente.nazionalizzaDescrizioneAccessorio(acc);
        }
        l_VG.fmtNd.setMaximumFractionDigits(2);
        l_VG.fmtNd.setMinimumFractionDigits(2);
        if (acc.Prezzo > 0) {
            obj[2] = l_VG.fmtNd.format(acc.Prezzo) + " €";
        } else {
            obj[2] = "---";
        }
        if (codiciAccessoriATEX.contains(acc.Codice) || acc.Codice.equals("ANT_C")) {
            tableAtex.addItem(obj, itemID);
        } else if (show(l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione, acc)) {
            tableAccessori.addItem(obj, itemID);
        }
    }

    private void FiltraAccessori(Ventilatore VentilatoreCurrent, Accessorio acc) {
        final GestioneAccessori gestioneAccessori = this.l_VG.utilityCliente.gestioneAccessori;
        boolean check;
        if (VentilatoreCurrent.selezioneCorrente.Esecuzione.equals("E04")) {
            gestioneAccessori.forzaAccessorio(VentilatoreCurrent, codiceSupporto, true);
        }
        //OFR
        check = gestioneAccessori.isAccessorioSelected(VentilatoreCurrent, "OFR");
        gestioneAccessori.getAccessorio(VentilatoreCurrent, "COL").enabled = !check;
        gestioneAccessori.getAccessorio(VentilatoreCurrent, "GA3").enabled = !check;
        gestioneAccessori.getAccessorio(VentilatoreCurrent, "GP3").enabled = !check;
        Silenziatore.enabled = !check;
        sil.enabled = !check;
        tramoggia.enabled = !check;
        if (check) {
            gestioneAccessori.getAccessorio(VentilatoreCurrent, "COL").Selezionato = false;
            gestioneAccessori.getAccessorio(VentilatoreCurrent, "GA3").Selezionato = false;
            gestioneAccessori.getAccessorio(VentilatoreCurrent, "GP3").Selezionato = false;
            sil.Selezionato = false;
            tramoggia.Selezionato = false;
        }
        //COL
        check = gestioneAccessori.isAccessorioSelected(VentilatoreCurrent, "COL");
        gestioneAccessori.getAccessorio(VentilatoreCurrent, "GA3").enabled = !check;
        if (check) {
            gestioneAccessori.getAccessorio(VentilatoreCurrent, "GA3").Selezionato = false;
        }
        //FS1
        check = gestioneAccessori.isAccessorioSelected(VentilatoreCurrent, "FS1");
        gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "FSTD", !check);

        if (tramoggia.Selezionato == true) {
            sil.Selezionato = true;
        }

        //Atex
        if (acc != null) {
            PrezzoPortello = gestioneAccessori.getAccessorio(VentilatoreCurrent, "PI").Prezzo == 0 ? PrezzoPortello : gestioneAccessori.getAccessorio(VentilatoreCurrent, "PI").Prezzo;
            check = codiciAccessoriATEX.contains(acc.Codice);
            if (check) {
                for (String s : codiciAccessoriATEX) {
                    if (!s.equals(acc.Codice)) {
                        gestioneAccessori.getAccessorio(VentilatoreCurrent, s).Selezionato = false;
                    }
                }
                if (acc.Selezionato) {
                    gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "MNE", false);
                    gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "MA", true);
                    gestioneAccessori.forzaAccessorio(VentilatoreCurrent, "PI", true, 0.0);
                    gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "PI", true);
                } else {
                    gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "MNE", true);
                    gestioneAccessori.setAccessorioSelezionato(VentilatoreCurrent, "MA", false);
                    gestioneAccessori.forzaAccessorio(VentilatoreCurrent, "PI", false);
                    gestioneAccessori.setAccessorioPrezzo(VentilatoreCurrent, "PI", PrezzoPortello);
                }
            }
        }
    }

    private void generaPreventivo() {
        try {
            final Ventilatore l_v = l_VG.VentilatoreCurrent;
            String ModelloCompleto = l_VG.buildModelloCompleto(l_v);
            InputStream inwb = null;
            String NomeOfferta = TF_NomeOfferta.getValue();
            inwb = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + this.l_VG.utilityCliente.idCliente + "_template_offerta.xlsx");
            this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
            this.l_VG.stampaOfferta.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.ventilatoreFisicaParameter, inwb);
            this.l_VG.stampaOfferta.initUtility(this.l_VG.currentCitFont, this.l_VG.utility, this.l_VG.utilityTraduzioni, this.l_VG.utilityUnitaMisura, null, null);
            final InputStream isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoofferta.png");
            final InputStream isCurve[] = new InputStream[14];
            if (this.l_VG.sfondoGraficiPrestazioni != null && this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
                for (int i = 0; i < 14; i++) {
                    isCurve[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                }
            }
            final InputStream isRumore[] = new InputStream[14];
            final InputStream isRumore1[] = new InputStream[14];
            if (this.l_VG.sfondoGraficiSpettro != null && this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag) {
                for (int i = 0; i < 14; i++) {
                    isRumore[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                }
                for (int i = 0; i < 14; i++) {
                    isRumore1[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                }

            }
            this.l_VG.stampaOfferta.initStampaApacheSingolo(ModelloCompleto, l_VG.utilityCliente, l_VG.gestioneAccessori, this.l_VG.currentUtilizzatore, isLogo, isCurve, this.l_VG.Paths, this.l_VG.assiGraficiDefault, isRumore, isRumore1);
            this.l_VG.stampaOfferta.eseguiStampaSingola(this.l_VG.Paths.rootResources, this.ElencoFileXls, null, l_v, this.l_VG.catalogoCondizioniUmidita, this.l_VG.ParametriVari, NomeOfferta);
            if (this.ElencoFileXls.size() > 0) {
                this.l_VG.dbCRMv2.storePreventivoFanStampato(this.l_VG.preventivoCurrent, this.ElencoFileXls.get(0));
                final String memo = this.l_VG.utilityTraduzioni.TraduciStringa("Generato") + " " + this.ElencoFileXls.get(0);
                this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneStampaOfferta, memo);
                this.buttonDownloadPreventivo.setEnabled(true);
                final Resource res = new FileResource(new File(this.ElencoFileXls.get(0)));
                if (fileDownloader == null) {
                    fileDownloader = new FileDownloader(res);
                    fileDownloader.extend(this.buttonDownloadPreventivo);
                } else {
                    fileDownloader.setFileDownloadResource(res);
                }
                MessageBox msgBox = MessageBox.createInfo();
                msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo Correttamente Generato"));
                msgBox.withOkButton();
                msgBox.open();
            } else {
                this.buttonDownloadPreventivo.setEnabled(false);
            }
        } catch (final Exception e) {
            l_VG.MainView.setMemo(e.toString());
        }
    }

    /*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
     */
    // <generated-code name="initUI">
    private void initUI() {
        this.horizontalLayout = new XdevHorizontalLayout();
        this.verticalLayout = new XdevVerticalLayout();
        this.verticalLayout4 = new XdevVerticalLayout();
        this.image = new XdevImage();
        this.horizontalLayout2 = new XdevHorizontalLayout();
        this.hlButtons = new XdevHorizontalLayout();
        this.tableAccessori = new XdevTable<>();
        this.tableAtex = new XdevTable<>();
        this.tableMotori = new XdevTable<>();
        this.verticalLayout2 = new XdevVerticalLayout();
        this.buttonGeneraPreventivo = new XdevButton();
        this.buttonDownloadPreventivo = new XdevButton();
        this.horizontalLayout.setSpacing(false);
        this.horizontalLayout.setMargin(new MarginInfo(false));
        this.verticalLayout.setSpacing(false);
        this.verticalLayout.setMargin(new MarginInfo(false));
        this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
        this.hlButtons.setMargin(new MarginInfo(false, true, false, true));
        this.TF_NomeOfferta = new XdevTextField();
        this.TF_NomeOfferta.setCaption("Insert Offer Code");
        this.buttonGeneraPreventivo.setCaption("Create Offer");
        this.buttonGeneraPreventivo.setStyleName("mybuttonstyle");
        this.buttonDownloadPreventivo.setCaption("Download Offer");
        this.buttonDownloadPreventivo.setStyleName("mybuttonstyle");
        this.buttonDownloadPreventivo.setEnabled(false);

        
        this.tableMotori.setCaption("Motor");
        this.tableMotori.setStyleName("small striped");
        this.tableAccessori.setCaption("Accessories");
        this.tableAccessori.setStyleName("small striped");
        this.tableAtex.setCaption("Atex Accessories");
        this.tableAtex.setStyleName("small striped");
        this.buttonGeneraPreventivo.setSizeUndefined();
        this.verticalLayout2.setMargin(new MarginInfo(false, true, false, true));
        this.image.setWidth(-1, Sizeable.Unit.PIXELS);
        this.image.setHeight(100, Sizeable.Unit.PERCENTAGE);
        this.hlButtons.addComponent(this.buttonGeneraPreventivo);
        this.hlButtons.setComponentAlignment(this.buttonGeneraPreventivo, Alignment.MIDDLE_CENTER);
        this.hlButtons.setExpandRatio(this.buttonGeneraPreventivo, 10.0F);
        TF_NomeOfferta.setWidthUndefined();
        this.hlButtons.addComponent(TF_NomeOfferta);
        this.hlButtons.addComponent(this.buttonDownloadPreventivo);
        this.hlButtons.setComponentAlignment(this.buttonDownloadPreventivo, Alignment.MIDDLE_CENTER);
        this.hlButtons.setExpandRatio(this.buttonDownloadPreventivo, 10.0F);
        this.hlButtons.setWidth(100, Sizeable.Unit.PERCENTAGE);
        this.hlButtons.setHeight(-1, Sizeable.Unit.PIXELS);

        this.verticalLayout.addComponent(this.hlButtons);
        this.verticalLayout.setComponentAlignment(this.hlButtons, Alignment.MIDDLE_CENTER);
        this.tableMotori.setSizeFull();
        this.verticalLayout.addComponent(this.tableMotori);
        this.verticalLayout.setComponentAlignment(this.tableMotori, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableMotori, 2F);
        this.tableAccessori.setSizeFull();
        this.verticalLayout.addComponent(this.tableAccessori);
        this.verticalLayout.setComponentAlignment(this.tableAccessori, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableAccessori, 8F);
        this.tableAtex.setSizeFull();
        this.verticalLayout.addComponent(this.tableAtex);
        this.verticalLayout.setComponentAlignment(this.tableAtex, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableAtex, 8F);

        this.verticalLayout.setSizeFull();
        this.horizontalLayout.addComponent(this.verticalLayout);
        this.horizontalLayout.setComponentAlignment(this.verticalLayout, Alignment.MIDDLE_CENTER);
        this.horizontalLayout.setExpandRatio(this.verticalLayout, 40.0F);
        this.verticalLayout2.setSizeFull();
        this.horizontalLayout.setSizeFull();
        this.setContent(this.horizontalLayout);
        this.setSizeFull();

        this.buttonGeneraPreventivo.addClickListener(event -> this.buttonGeneraPreventivo_buttonClick(event));
    } // </generated-code>

    private void buttonGeneraPreventivo_buttonClick(final ClickEvent event) {
        if (this.l_VG.stampaOfferta == null) {
            this.l_VG.stampaOfferta = new Cliente04StampaOffertaPoiXLSX();
        }
        generaPreventivo();
    }
    // <generated-code name="variables">
    private XdevButton buttonGeneraPreventivo, buttonDownloadPreventivo;
    private XdevHorizontalLayout horizontalLayout, horizontalLayout2, hlButtons;
    private XdevImage image;
    private XdevTable<CustomComponent> tableAccessori, tableAtex, tableMotori;
    private XdevPanel panel;
    private XdevVerticalLayout verticalLayout, verticalLayout4, verticalLayout2;
    private TextField TF_NomeOfferta;
    // </generated-code>

    public void refreshAccessori() {
        FiltraAccessori(l_VG.VentilatoreCurrent, null);
        setVentilatoreCurrent();
    }

    private boolean show(String Exec, Accessorio l_a) {
        ArrayList<String> listaNascondiE04 = new ArrayList((Arrays.asList("OFR", "COL")));
        ArrayList<String> listaNascondiE05 = new ArrayList((Arrays.asList("AV", "TS", "SA")));
        if (Exec.equals("E04")) {
            if (listaNascondiE04.contains(l_a.Codice)) {
                return false;
            }
        }
        if (Exec.equals("E05")) {
            if (listaNascondiE05.contains(l_a.Codice)) {
                return false;
            }
        }
        return true;
    }

    private void setMotor() {
        Motore motorInt, motorOther;
        Boolean isPtc = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PTCPresente;
        if (isPtc) {
            motorInt = motorPTC;
            motorOther = motor;
        } else {
            motorInt = motor;
            motorOther = motorPTC;
        }
        final Object obj[] = new Object[5];
        this.containerMotori.addContainerProperty("Code", String.class, "");
        this.containerMotori.addContainerProperty("Description", String.class, "");
        this.containerMotori.addContainerProperty("Kw", String.class, "");
        this.containerMotori.addContainerProperty("PTC", CheckBox.class, false);
        this.containerMotori.addContainerProperty("Price", String.class, "");
        obj[0] = motorInt.CodiceCliente;
        obj[1] = motorInt.Descrizione;
        l_VG.fmtNd.setMinimumFractionDigits(2);
        l_VG.fmtNd.setMaximumFractionDigits(2);
        obj[2] = (l_VG.fmtNd.format(motorInt.PotkW) + " Kw"); 
        cbMotor = new CheckBox();
        cbMotor.setValue(isPtc);
        cbMotor.setData(motorInt);
        cbMotor.setEnabled(checkMotor());
        cbMotor.setImmediate(true);
        cbMotor.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                l_VG.utilityCliente.loadMotore(l_VG.VentilatoreCurrent, motorOther.CodiceCliente);
                setVentilatoreCurrent();
            }
        });
        obj[3] = cbMotor;
        String prezzo;
        if (motorInt.Prezzo>0)
            prezzo = l_VG.fmtNd.format(motorInt.Prezzo) + " €";
        else {
            prezzo = "---";
            showPopupMZ("Motor not in catalog");
        }
        obj[4] = l_VG.fmtNd.format(motorInt.Prezzo) + " €";
        
        tableMotori.addItem(obj, 0);
    }

    private boolean checkMotor() {
        boolean value = true;
        if (startingIsPtc)
            value = false;
        if (startingIsPtc && l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.isAtex)
            value = !l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PTCPresente;
        if (motor == null || motorPTC == null)
            value = false;
        if (l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.isAtex && l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PTCPresente)
            value = false;
        return value;
    }
    
    private void loadMotors() {
        if (l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PTCPresente) {
            motorPTC = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato;
        } else {
            motor = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato;
        }
        if (l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.isAtex) 
            if (l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PTCPresente)
                motorPTC = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato;
            else
                motor = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato;
        
        if (!l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.isAtex && !startingIsPtc) {
            if (motor != null) {
                String codice = motor.CodiceCliente.endsWith("P") ? motor.CodiceCliente.substring(0, motor.CodiceCliente.length() - 1) : motor.CodiceCliente;
                String query = "select * from Motori where CodiceCliente like '" + codice + "%'";
                String codiceMotore;
                try {
                    final Statement stmt = this.l_VG.conTecnica.createStatement();
                    final ResultSet rs = stmt.executeQuery(query);
                    while (rs.next()) {
                        Motore m = new Motore();
                        codiceMotore = rs.getString("CodiceCliente");
                        m.CodiceCliente = codiceMotore;
                        l_VG.utilityCliente.loadClassFromRs.loadMotoreFromRs(m, rs);
                        m.Esecuzione = l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione;
                        m.Prezzo = l_VG.utilityCliente.getPrezzoMotore(codiceMotore, l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
                        if (m.PTCPresente) {
                            motorPTC = m;
                        } else {
                            motor = m;
                        }
                    }
                } catch (final Exception e) {
                    Notification.show(e.toString());
                }
            }
        }
        checkMotor();
    }
    public void showPopupMZ(String message) {
        final MessageBox msgBox = MessageBox.createInfo();
        msgBox.withCaption("Warning: " + message);
        msgBox.withMessage("Please ask MZ for this special request.");
        msgBox.withOkButton();
        msgBox.open();
    }
}
