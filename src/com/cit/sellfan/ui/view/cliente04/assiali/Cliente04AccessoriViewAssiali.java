package com.cit.sellfan.ui.view.cliente04.assiali;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.view.AccessoriFreeView;
import com.cit.sellfan.ui.view.AccessoriImageView;
import com.cit.sellfan.ui.view.cliente04.Cliente04AccessoriView3Gruppi;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

import cit.sellfan.classi.GestioneAccessori;

public class Cliente04AccessoriViewAssiali extends XdevView {
	private VariabiliGlobali l_VG;
	private String modelloOld = "";
	private final Cliente04AccessoriViewGruppiA0_1 accessoriView0 = new Cliente04AccessoriViewGruppiA0_1();
	private final AccessoriImageView accessoriView1 = new AccessoriImageView();
	private final Cliente04AccessoriView3Gruppi accessoriView2 = new Cliente04AccessoriView3Gruppi();
	private final AccessoriFreeView accessoriFreeView = new AccessoriFreeView();
	private final String coloreBottone = "verde";
	private final String coloreBottoneSelezionato = "verdeselezionato";

	/**
	 * 
	 */
	public Cliente04AccessoriViewAssiali() {
		super();
		this.initUI();
		/*
		accessoriView0.setSizeFull();
		tabSheet.replaceComponent(panelTab0, accessoriView0);
		accessoriView1.setSizeFull();
		tabSheet.replaceComponent(panelTab1, accessoriView1);
		accessoriView2.setSizeFull();
		tabSheet.replaceComponent(panelTab2, accessoriView2);
		*/
	}
	
	public void refreshAccessori() {
		setVentilatoreCurrent();
		final GestioneAccessori gA = new GestioneAccessori();
		if (!gA.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "GAL") && !gA.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "GPP") && !gA.isAccessorioSelected(this.l_VG.VentilatoreCurrent, "PAGAS")) {
			gA.setAccessorioSelezionato(this.l_VG.VentilatoreCurrent, "GAL", true);
			this.l_VG.utilityCliente.changeMaxRPM(this.l_VG.VentilatoreCurrent, "AL");
		}
	}
        
        public void togglePreventivoButton(boolean value) {
            accessoriView0.buttonAggiungiPreventivo.setEnabled(value);
        }
	
	public boolean isPreSetVentilatoreCurrentOK() {
		if (this.l_VG == null) {
			return false;
		}
		if (this.l_VG.VentilatoreCurrent == null || this.l_VG.utilityCliente == null || !this.l_VG.utilityCliente.isTabVisible("Accessori")) {
			return false;
		}
		if (this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size() <= 0) {
			return false;
		}
		return true;
	}
	
	public void setVentilatoreCurrent() {
	    this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex] = this;
	    this.l_VG.ElencoMobileView[this.l_VG.AccessoriViewIndex] = null;
		this.accessoriView0.setVentilatoreCurrentCaratteristiche();
		this.accessoriView0.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(11).idGruppo, this.l_VG.elencoGruppiAccessori.get(12).idGruppo, this.l_VG.elencoGruppiAccessori.get(11).descrizioneGruppo, this.l_VG.elencoGruppiAccessori.get(12).descrizioneGruppo);
		this.accessoriView1.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(13).idGruppo, this.l_VG.elencoGruppiAccessori.get(13).descrizioneGruppo);
		this.accessoriView2.setVentilatoreCurrent(this.l_VG.elencoGruppiAccessori.get(14).idGruppo, this.l_VG.elencoGruppiAccessori.get(15).idGruppo, this.l_VG.elencoGruppiAccessori.get(16).idGruppo, this.l_VG.elencoGruppiAccessori.get(14).descrizioneGruppo, this.l_VG.elencoGruppiAccessori.get(15).descrizioneGruppo, this.l_VG.elencoGruppiAccessori.get(16).descrizioneGruppo);
		this.accessoriFreeView.setVentilatoreCurrent();
		if (!this.l_VG.VentilatoreCurrent.ModelloTrans.equals(this.modelloOld)) {
			buttonTab0_buttonClick(null);
		}
		this.modelloOld = this.l_VG.VentilatoreCurrent.ModelloTrans;
	}
	
	public void Nazionalizza() {
		this.buttonTab0.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.elencoGruppiAccessori.get(11).titoloTabGruppo));
		this.buttonTab1.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Accessori"));
		this.buttonTab2.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Finitura / Opt"));
		this.buttonTab3.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Liberi"));
		this.accessoriView0.Nazionalizza();
		this.accessoriView1.Nazionalizza();
		this.accessoriView2.Nazionalizza();
		this.accessoriFreeView.Nazionalizza();
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		//l_VG.AccessoriView = this;
		this.accessoriView0.setVariabiliGlobali(this.l_VG);
		this.accessoriView1.setVariabiliGlobali(this.l_VG);
		this.accessoriView2.setVariabiliGlobali(this.l_VG);
		this.accessoriFreeView.setVariabiliGlobali(this.l_VG);
	}
	
	private void resetColoreSelezionato() {
		resetColoreSelezionato(this.buttonTab0);
		resetColoreSelezionato(this.buttonTab1);
		resetColoreSelezionato(this.buttonTab2);
		resetColoreSelezionato(this.buttonTab3);
	}
	
	private void resetColoreSelezionato(final XdevButton bottone) {
		if (bottone.getStyleName().contains(this.coloreBottoneSelezionato)) {
			bottone.removeStyleName(this.coloreBottoneSelezionato);
			bottone.addStyleName(this.coloreBottone);
		}
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab0}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab0_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab0.removeStyleName(this.coloreBottone);
		this.buttonTab0.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView0.setSizeFull();
		this.gridLayout.addComponent(this.accessoriView0, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonTab1}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab1_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab1.removeStyleName(this.coloreBottone);
		this.buttonTab1.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView1.setSizeFull();
		this.gridLayout.addComponent(this.accessoriView1, 0, 0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton} {@link #buttonTab3}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab3_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab3.removeStyleName(this.coloreBottone);
		this.buttonTab3.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriFreeView.setSizeFull();
		this.gridLayout.addComponent(this.accessoriFreeView, 0, 0);
	
	}

	/**
	 * Event handler delegate method for the {@link XdevButton} {@link #buttonTab2}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonTab2_buttonClick(final Button.ClickEvent event) {
		resetColoreSelezionato();
		this.buttonTab2.removeStyleName(this.coloreBottone);
		this.buttonTab2.addStyleName(this.coloreBottoneSelezionato);
		this.gridLayout.removeComponent(0, 0);
		this.accessoriView2.setSizeFull();
		this.gridLayout.addComponent(this.accessoriView2, 0, 0);
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.buttonTab0 = new XdevButton();
		this.buttonTab1 = new XdevButton();
		this.buttonTab2 = new XdevButton();
		this.buttonTab3 = new XdevButton();
		this.gridLayout = new XdevGridLayout();
		this.panelToRemove = new XdevPanel();
	
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.horizontalLayout.setSpacing(false);
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.buttonTab0.setCaption("Tab0");
		this.buttonTab0.setStyleName("verde");
		this.buttonTab1.setCaption("Tab1");
		this.buttonTab1.setStyleName("verde");
		this.buttonTab2.setCaption("Tab2");
		this.buttonTab2.setStyleName("verde");
		this.buttonTab3.setCaption("Tab3");
		this.buttonTab3.setStyleName("verde");
		this.gridLayout.setMargin(new MarginInfo(false));
		this.panelToRemove.setTabIndex(0);
	
		this.buttonTab0.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab0);
		this.horizontalLayout.setComponentAlignment(this.buttonTab0, Alignment.MIDDLE_CENTER);
		this.buttonTab1.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab1);
		this.horizontalLayout.setComponentAlignment(this.buttonTab1, Alignment.MIDDLE_CENTER);
		this.buttonTab2.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab2);
		this.horizontalLayout.setComponentAlignment(this.buttonTab2, Alignment.MIDDLE_CENTER);
		this.buttonTab3.setSizeUndefined();
		this.horizontalLayout.addComponent(this.buttonTab3);
		this.horizontalLayout.setComponentAlignment(this.buttonTab3, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout_spacer = new CustomComponent();
		horizontalLayout_spacer.setSizeFull();
		this.horizontalLayout.addComponent(horizontalLayout_spacer);
		this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
		this.gridLayout.setColumns(1);
		this.gridLayout.setRows(1);
		this.panelToRemove.setSizeFull();
		this.gridLayout.addComponent(this.panelToRemove, 0, 0);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setRowExpandRatio(0, 10.0F);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(40, Unit.PIXELS);
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.gridLayout.setSizeFull();
		this.verticalLayout.addComponent(this.gridLayout);
		this.verticalLayout.setComponentAlignment(this.gridLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.gridLayout, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.buttonTab0.addClickListener(event -> this.buttonTab0_buttonClick(event));
		this.buttonTab1.addClickListener(event -> this.buttonTab1_buttonClick(event));
		this.buttonTab2.addClickListener(event -> this.buttonTab2_buttonClick(event));
		this.buttonTab3.addClickListener(event -> this.buttonTab3_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonTab0, buttonTab1, buttonTab2, buttonTab3;
	private XdevHorizontalLayout horizontalLayout;
	private XdevPanel panelToRemove;
	private XdevGridLayout gridLayout;
	private XdevVerticalLayout verticalLayout;
	// </generated-code>


}
