package com.cit.sellfan.ui.view.preventivi;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.pannelli.pannelloCliente;
import com.cit.sellfan.ui.view.PreventiviView;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.classi.Clientev21;
import cit.sellfan.classi.Preventivo;
import de.steinwedel.messagebox.MessageBox;

public class PreventiviCliente extends XdevView {
	private VariabiliGlobali l_VG;
	private final pannelloCliente cliente = new pannelloCliente();
	private final Container containerClienti = new IndexedContainer();
	private PreventiviView preventiviView;
	private Clientev21 clienteOld;
	private Clientev21 clienteInUse;
	/**
	 * 
	 */
	public PreventiviCliente() {
		super();
		this.initUI();
		this.cliente.setSizeFull();
		
		this.cliente.setSizeFull();
		this.horizontalLayout.replaceComponent(this.panelClienteGosth, this.cliente);
		this.horizontalLayout.setComponentAlignment(this.cliente, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.cliente, 20.0F);
		
/*
		horizontalLayout.replaceComponent(panelClienteGosth, cliente);
		horizontalLayout.setComponentAlignment(cliente, Alignment.MIDDLE_CENTER);
		horizontalLayout.setExpandRatio(cliente, 0.2F);
*/
		this.containerClienti.removeAllItems();
		this.containerClienti.addContainerProperty("Check", XdevImage.class, "");
		//this.containerClienti.addContainerProperty("Code", String.class, "");
		this.containerClienti.addContainerProperty("Ragione", String.class, "");
		this.tableClienti.setContainerDataSource(this.containerClienti);
		this.tableClienti.setColumnWidth("Check", 30);
		this.tableClienti.setColumnHeader("Check", "Sel");
		//this.tableClienti.setColumnWidth("Code", 40);

		this.buttonModifica.setVisible(false);
		this.buttonCancella.setVisible(false);
		this.buttonSeleziona.setVisible(false);
		this.buttonSalva.setVisible(false);
		this.buttonAnnulla.setVisible(false);
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
		this.cliente.setVariabiliGlobali(this.l_VG);
		if (this.l_VG.MobileMode) {
			this.horizontalLayout.removeComponent(this.cliente);
		}
		initTableClienti();
	}
	
	public void initTableClienti() {
		this.preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		this.tableClienti.setEnabled(false);
		try {
			this.tableClienti.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
		}
		for (int i=0 ; i<this.l_VG.ElencoClienti.size() ; i++) {
			final String itemID = Integer.toString(this.l_VG.ElencoClienti.get(i).IDCliente);
			final Object obj[] = new Object[2];
			final XdevImage chk = new XdevImage();
			if (this.l_VG.currentCliente != null && this.l_VG.currentCliente.IDCliente == this.l_VG.ElencoClienti.get(i).IDCliente ) {
				chk.setSource(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
			}
			chk.setWidth(15, Unit.PIXELS);
			chk.setHeight(15, Unit.PIXELS);
			obj[0] = chk;
			obj[1] = this.l_VG.ElencoClienti.get(i).ragionesociale;
			this.tableClienti.addItem(obj, itemID);
		}
		this.tableClienti.setEnabled(true);
	}
	
	public void Nazionalizza() {
		this.cliente.Nazionalizza();
		this.tableClienti.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Clienti"));
		//this.tableClienti.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableClienti.setColumnHeader("Ragione", this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale"));
		this.buttonNuovo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nuovo"));
		this.buttonModifica.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Modifica"));
		this.buttonCancella.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella"));
		this.buttonSeleziona.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Seleziona"));
		this.buttonSalva.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Salva"));
		this.buttonAnnulla.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Annulla"));
	}
	
	public void setCheckTableClienti() {
			final XdevImage chk = new XdevImage();
			chk.setSource(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
			chk.setWidth(16, Unit.PIXELS);
			chk.setHeight(16, Unit.PIXELS);
			for(final Object element : this.tableClienti.getItemIds()) {
				if (element.toString().equals(String.valueOf(this.l_VG.currentCliente.IDCliente))) {
					this.tableClienti.getContainerProperty(element.toString(), "Check").setValue(chk);
				} else {
					this.tableClienti.getContainerProperty(element.toString(), "Check").setValue(null);
				}
			}
	}
	
	private void setCliente(final Clientev21 clienteIn) {
		if (clienteIn == null) {
			this.clienteInUse = this.l_VG.currentCliente;
		} else {
			this.clienteInUse = clienteIn;
		}
		this.cliente.setClienteInUse(this.clienteInUse);
		this.tableClienti.setEnabled(false);
		if (this.clienteInUse.IDCliente != -1) {
			this.tableClienti.select(Integer.toString(this.clienteInUse.IDCliente));
			this.buttonModifica.setVisible(true);
			this.buttonCancella.setVisible(!isUtilizzato());
			this.buttonSeleziona.setVisible(true);
		} else {
			initTableClienti();
			this.buttonModifica.setVisible(false);
			this.buttonCancella.setVisible(false);
			this.buttonSeleziona.setVisible(false);
		}
		this.tableClienti.setEnabled(true);
	}
	
	public void setCliente() {
		setCliente(null);
	}
	
	private boolean isUtilizzato() {
		for (int i=0 ; i<this.l_VG.ElencoPreventivi.size() ; i++) {
			final Preventivo l_p = this.l_VG.ElencoPreventivi.get(i);
			if (l_p.IDCliente == this.clienteInUse.IDCliente) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isOnEdit() {
		if (!this.buttonSalva.isVisible()) {
			return false;
		} else {
			if (this.cliente.getRagioneSociale().equals("")) {
	    		final MessageBox msgBox = MessageBox.createError();
	    		msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale è Obbligatoria."));
	    		msgBox.withOkButton();
	    		msgBox.open();
				return true;
			}
			buttonSalva_buttonClick(null);
			return false;
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonNuovo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonNuovo_buttonClick(final Button.ClickEvent event) {
		this.tableClienti.setEnabled(false);
		this.buttonNuovo.setVisible(false);
		this.buttonModifica.setVisible(false);
		this.buttonCancella.setVisible(false);
		this.buttonSeleziona.setVisible(false);
		this.clienteOld = (Clientev21)this.l_VG.currentCliente.clone();
		this.clienteInUse = new Clientev21();
		this.cliente.setClienteInUse(this.clienteInUse);
		this.cliente.enableFields(true);
		this.buttonSalva.setVisible(true);
		this.buttonAnnulla.setVisible(true);
		if (this.l_VG.MobileMode) {
			this.cliente.setSizeFull();
			this.verticalLayout2.replaceComponent(this.tableClienti, this.cliente);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSalva}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonSalva_buttonClick(final Button.ClickEvent event) {
		if (this.cliente.getRagioneSociale().equals("")) {
    		final MessageBox msgBox = MessageBox.createError();
    		msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale è Obbligatoria."));
    		msgBox.withOkButton();
    		msgBox.open();
			return;
		}
		if (this.clienteInUse.IDCliente == -1) {
			this.clienteInUse.IDCliente = this.l_VG.dbCRMv2.getNewAnagraficaClienteCodice();
		}
		this.cliente.getClienteInUse(this.clienteInUse);
		this.l_VG.dbCRMv2.storeClienteOnAnagrafica(this.clienteInUse);
		if (this.l_VG.preventivoCurrent.IDCliente == this.clienteInUse.IDCliente) {
			this.l_VG.preventivoCurrent.ragioneSocialeCliente = this.cliente.getRagioneSociale();
			this.l_VG.preventivoCurrent.Sconto = this.clienteInUse.scontocliente;
		}
/*
		l_VG.preventivoCurrent.IDCliente = clienteInUse.IDCliente;
		l_VG.preventivoCurrent.ragioneSocialeCliente = cliente.getRagioneSociale();
		l_VG.preventivoCurrent.Sconto = clienteInUse.scontocliente;
*/
		this.l_VG.dbCRMv2.loadElencoAnagraficaClienti(this.l_VG.ElencoClienti);
		initTableClienti();
		this.tableClienti.setEnabled(false);
		this.tableClienti.select(Integer.toString(this.clienteInUse.IDCliente));
		this.tableClienti.setEnabled(true);
		this.l_VG.currentCliente = this.clienteInUse;
		this.preventiviView.aggiornaPreventivo();
		this.buttonNuovo.setVisible(true);
		this.buttonModifica.setVisible(true);
		this.buttonCancella.setVisible(!isUtilizzato());
		this.buttonSeleziona.setVisible(true);
		this.buttonSalva.setVisible(false);
		this.buttonAnnulla.setVisible(false);
		if (this.l_VG.MobileMode) {
			this.tableClienti.setSizeFull();
			this.verticalLayout2.replaceComponent(this.cliente, this.tableClienti);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonModifica}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonModifica_buttonClick(final Button.ClickEvent event) {
		this.tableClienti.setEnabled(false);
		this.buttonNuovo.setVisible(false);
		this.buttonModifica.setVisible(false);
		this.buttonCancella.setVisible(false);
		this.buttonSeleziona.setVisible(false);
		//cliente.setCurrentCliente();
		this.cliente.enableFields(true);
		this.buttonSalva.setVisible(true);
		this.buttonAnnulla.setVisible(false);
		if (this.l_VG.MobileMode) {
			this.cliente.setSizeFull();
			this.verticalLayout2.replaceComponent(this.tableClienti, this.cliente);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableClienti}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableClienti_valueChange(final Property.ValueChangeEvent event) {
		try {
		if (!this.tableClienti.isEnabled()) {
			return;
		}
		final int idCli = Integer.parseInt(event.getProperty().getValue().toString());
		setCliente(this.l_VG.dbCRMv2.setCurrentClienteFromCodice(idCli, this.l_VG.ElencoClienti));
		} catch (final Exception e) {
			
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAnnulla}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAnnulla_buttonClick(final Button.ClickEvent event) {
		initTableClienti();
		this.tableClienti.setEnabled(false);
		this.tableClienti.select(Integer.toString(this.l_VG.currentCliente.IDCliente));
		this.tableClienti.setEnabled(true);
		this.preventiviView.aggiornaPreventivo();
		this.buttonNuovo.setVisible(true);
		this.buttonModifica.setVisible(true);
		this.buttonCancella.setVisible(!isUtilizzato());
		this.buttonSeleziona.setVisible(true);
		this.buttonSalva.setVisible(false);
		this.buttonAnnulla.setVisible(false);
		if (this.l_VG.MobileMode) {
			this.tableClienti.setSizeFull();
			this.verticalLayout2.replaceComponent(this.cliente, this.tableClienti);
		}
		this.l_VG.currentCliente = this.clienteOld;
		setCliente();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSeleziona}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonSeleziona_buttonClick(final Button.ClickEvent event) {
		this.l_VG.currentCliente = this.clienteInUse;
		this.l_VG.preventivoCurrent.IDCliente = this.l_VG.currentCliente.IDCliente;
		this.l_VG.preventivoCurrent.ragioneSocialeCliente = this.l_VG.currentCliente.ragionesociale;
		this.l_VG.preventivoCurrent.Sconto = this.l_VG.currentCliente.scontocliente;
		this.l_VG.preventivoCurrent.IndirizzoSpedizione = this.l_VG.currentCliente.indirizzospedizione;
		if (!this.l_VG.currentCliente.resa.equals(""))
                    this.l_VG.preventivoCurrent.Resa = this.l_VG.currentCliente.resa;
                if (!this.l_VG.currentCliente.imballo.equals(""))
                    this.l_VG.preventivoCurrent.Imballo = this.l_VG.currentCliente.imballo;
		this.l_VG.preventivoCurrent.Pagamento = this.l_VG.currentCliente.pagamento;
		this.tableClienti.setEnabled(false);
		this.tableClienti.select(Integer.toString(this.l_VG.currentCliente.IDCliente));
		setCheckTableClienti();
		//this.tableClienti.g
		this.tableClienti.setEnabled(true);
		this.preventiviView.aggiornaPreventivo();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCancella}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCancella_buttonClick(final Button.ClickEvent event) {
		this.l_VG.dbCRMv2.deleteClienteOnAnagrafica(this.clienteInUse);
		this.l_VG.dbCRMv2.loadElencoAnagraficaClienti(this.l_VG.ElencoClienti);
		initTableClienti();
		this.clienteInUse = null;
		setCliente(this.clienteInUse);
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.panel = new XdevPanel();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.tableClienti = new XdevTable<>();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.buttonNuovo = new XdevButton();
		this.buttonSeleziona = new XdevButton();
		this.buttonModifica = new XdevButton();
		this.buttonCancella = new XdevButton();
		this.buttonSalva = new XdevButton();
		this.buttonAnnulla = new XdevButton();
		this.panelClienteGosth = new XdevPanel();
	
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.verticalLayout2.setMargin(new MarginInfo(false));
		this.tableClienti.setCaption("Clienti");
		this.tableClienti.setStyleName("small mystriped");
		this.buttonNuovo.setCaption("New");
		this.buttonNuovo.setStyleName("small");
		this.buttonSeleziona.setCaption("Sel");
		this.buttonSeleziona.setStyleName("small");
		this.buttonModifica.setCaption("Mod");
		this.buttonModifica.setStyleName("small");
		this.buttonCancella.setCaption("Del");
		this.buttonCancella.setStyleName("small");
		this.buttonSalva.setCaption("Save");
		this.buttonSalva.setStyleName("small");
		this.buttonAnnulla.setCaption("Abort");
		this.buttonAnnulla.setStyleName("small");
	
		this.buttonNuovo.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonNuovo);
		this.horizontalLayout2.setComponentAlignment(this.buttonNuovo, Alignment.MIDDLE_CENTER);
		this.buttonSeleziona.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonSeleziona);
		this.horizontalLayout2.setComponentAlignment(this.buttonSeleziona, Alignment.MIDDLE_CENTER);
		this.buttonModifica.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonModifica);
		this.horizontalLayout2.setComponentAlignment(this.buttonModifica, Alignment.MIDDLE_CENTER);
		this.buttonCancella.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonCancella);
		this.horizontalLayout2.setComponentAlignment(this.buttonCancella, Alignment.MIDDLE_CENTER);
		this.buttonSalva.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonSalva);
		this.horizontalLayout2.setComponentAlignment(this.buttonSalva, Alignment.MIDDLE_CENTER);
		this.buttonAnnulla.setSizeUndefined();
		this.horizontalLayout2.addComponent(this.buttonAnnulla);
		this.horizontalLayout2.setComponentAlignment(this.buttonAnnulla, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout2_spacer = new CustomComponent();
		horizontalLayout2_spacer.setSizeFull();
		this.horizontalLayout2.addComponent(horizontalLayout2_spacer);
		this.horizontalLayout2.setExpandRatio(horizontalLayout2_spacer, 1.0F);
		this.tableClienti.setSizeFull();
		this.verticalLayout2.addComponent(this.tableClienti);
		this.verticalLayout2.setComponentAlignment(this.tableClienti, Alignment.MIDDLE_CENTER);
		this.verticalLayout2.setExpandRatio(this.tableClienti, 100.0F);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.horizontalLayout2);
		this.verticalLayout2.setSizeFull();
		this.panel.setContent(this.verticalLayout2);
		this.panel.setSizeFull();
		this.horizontalLayout.addComponent(this.panel);
		this.horizontalLayout.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.panel, 10.0F);
		this.panelClienteGosth.setSizeFull();
		this.horizontalLayout.addComponent(this.panelClienteGosth);
		this.horizontalLayout.setComponentAlignment(this.panelClienteGosth, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.panelClienteGosth, 20.0F);
		this.horizontalLayout.setSizeFull();
		this.verticalLayout.addComponent(this.horizontalLayout);
		this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.horizontalLayout, 10.0F);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.tableClienti.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				PreventiviCliente.this.tableClienti_valueChange(event);
			}
		});
		this.buttonNuovo.addClickListener(event -> this.buttonNuovo_buttonClick(event));
		this.buttonSeleziona.addClickListener(event -> this.buttonSeleziona_buttonClick(event));
		this.buttonModifica.addClickListener(event -> this.buttonModifica_buttonClick(event));
		this.buttonCancella.addClickListener(event -> this.buttonCancella_buttonClick(event));
		this.buttonSalva.addClickListener(event -> this.buttonSalva_buttonClick(event));
		this.buttonAnnulla.addClickListener(event -> this.buttonAnnulla_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonNuovo, buttonSeleziona, buttonModifica, buttonCancella, buttonSalva, buttonAnnulla;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2;
	private XdevTable<CustomComponent> tableClienti;
	private XdevPanel panel, panelClienteGosth;
	private XdevVerticalLayout verticalLayout, verticalLayout2;
	// </generated-code>


}
