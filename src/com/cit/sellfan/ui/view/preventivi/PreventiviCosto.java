package com.cit.sellfan.ui.view.preventivi;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.PreventiviView;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TextField;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.Costanti;
import cit.sellfan.classi.Accessorio;
import cit.sellfan.classi.ventilatore.VentilatorePreventivo;

public class PreventiviCosto extends XdevView {

    private VariabiliGlobali l_VG;
    private Container containerItem = new IndexedContainer();
    private final Container containerItemFree = new IndexedContainer();
    private String aRichiestaTradotto = null;
    private VentilatorePreventivo ventilatorePreventivo;
    //private double prezzoVentilatore;
    private double prezzoMotore;
    private PreventiviView preventiviView;

    /**
     *
     *
     */
    public PreventiviCosto() {
        super();
        this.initUI();

        this.containerItem.removeAllItems();
        this.containerItem.addContainerProperty("Selected", CheckBox.class, false);
        this.containerItem.addContainerProperty("Code", String.class, "");
        this.containerItem.addContainerProperty("Description", String.class, "");
        this.containerItem.addContainerProperty("Qta", TextFieldInteger.class, "0");
        this.containerItem.addContainerProperty("Price", String.class, "");
        this.containerItem.addContainerProperty("PriceVal", Double.class, 0.0);
        this.tableItem.setContainerDataSource(this.containerItem);
        this.tableItem.setColumnWidth("PriceVal", 0);

        this.containerItemFree.removeAllItems();
        this.containerItemFree.addContainerProperty("Selected", CheckBox.class, false);
        this.containerItemFree.addContainerProperty("Code", TextField.class, "");
        this.containerItemFree.addContainerProperty("Description", TextField.class, "");
        this.containerItemFree.addContainerProperty("Qta", TextFieldInteger.class, "0");
        this.containerItemFree.addContainerProperty("Price", TextFieldDouble.class, 0.0);
        this.tableItemFree.setContainerDataSource(this.containerItemFree);
    }

    private void initContainerItem() {
        this.containerItem = new IndexedContainer();
        this.containerItem.addContainerProperty("Selected", CheckBox.class, false);
        this.containerItem.addContainerProperty("Code", String.class, "");
        this.containerItem.addContainerProperty("Description", String.class, "");
        this.containerItem.addContainerProperty("Qta", TextFieldInteger.class, "0");
        this.containerItem.addContainerProperty("Price", String.class, "");
        this.containerItem.addContainerProperty("PriceVal", Double.class, 0.0);
        this.tableItem.setContainerDataSource(this.containerItem);
        this.tableItem.setColumnWidth("PriceVal", 0);
        this.tableItem.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
        this.tableItem.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
        this.tableItem.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
        this.tableItem.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
        this.tableItem.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo") + " " + Costanti.EURO);
        this.tableItem.setColumnHeader("PriceVal", "");
    }

    @SuppressWarnings("unused")
    private void initContainerItemFree() {
        this.containerItemFree.addContainerProperty("Selected", CheckBox.class, false);
        this.containerItemFree.addContainerProperty("Code", TextField.class, "");
        this.containerItemFree.addContainerProperty("Description", TextField.class, "");
        this.containerItemFree.addContainerProperty("Qta", TextFieldInteger.class, "0");
        this.containerItemFree.addContainerProperty("Price", TextFieldDouble.class, 0.0);
        this.tableItemFree.setContainerDataSource(this.containerItemFree);
        this.tableItemFree.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Accessori Liberi"));
        this.tableItemFree.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
        this.tableItemFree.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
        this.tableItemFree.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
        this.tableItemFree.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
        this.tableItemFree.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo") + " " + Costanti.EURO);
    }

    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
    }

    public void Nazionalizza() {
        this.aRichiestaTradotto = this.l_VG.utilityTraduzioni.TraduciStringa("a richiesta");
        this.labelPrezzoTotale.setValue("<html><b><font size='+1'>" + this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo Totale") + ":</html>");
        this.labelPrezzoAcquisto.setValue("<html><b><font size='+1'>" + this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo di vendita") + ":</html>");

        this.tableItem.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Conto Economico"));
        this.tableItem.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
        this.tableItem.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
        this.tableItem.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
        this.tableItem.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
        this.tableItem.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo") + " " + Costanti.EURO);
        this.tableItem.setColumnHeader("PriceVal", "");

        this.tableItemFree.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Accessori Liberi"));
        this.tableItemFree.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
        this.tableItemFree.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
        this.tableItemFree.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
        this.tableItemFree.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
        this.tableItemFree.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo") + " " + Costanti.EURO);

        setCostoVentilatore();
    }

    public void refreshAccessori() {
        setCostoVentilatore();
    }

    public void setCostoVentilatore() {
        int deltaRow = 0;
        Object obj[];
        @SuppressWarnings("unused")
        double prezzo;
        this.preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        this.tableItem.setEnabled(false);
        this.tableItemFree.setEnabled(false);
        try {
            this.tableItem.removeAllItems();//resetta anche le selection
        } catch (final Exception e) {

        }
        obj = new Object[6];
        if (this.l_VG.VentilatoreCurrentIndex < 0) {
            return;
        }
        try {
            this.ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
        } catch (final Exception e) {
            return;
        }
        final CheckBox cbv = new CheckBox();
        cbv.setData("0");
        cbv.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                if (!PreventiviCosto.this.tableItem.isEnabled()) {
                    return;
                }
                final boolean value = cbv.getValue().booleanValue();
                PreventiviCosto.this.ventilatorePreventivo.VentilatoreSelezionato = value;
                calcolaPrezzoTotale();
            }
        });
        cbv.setValue(this.ventilatorePreventivo.VentilatoreSelezionato);
        obj[0] = cbv;
        obj[1] = this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent, this.l_VG.parametriModelloCompletoForPrint);
        obj[2] = this.l_VG.VentilatoreCurrent.DescrizioneTrans;
        final TextFieldInteger tiv = new TextFieldInteger();
        tiv.setData("0");
        tiv.setEnabled(false);
        tiv.setValue(1);
        obj[3] = tiv;
        prezzo = -1;
        /*
		prezzoVentilatore = 0.;
		for (int i=0 ; i<l_VG.VentilatoreCurrent.Esecuzioni.length ; i++) {
			if (l_VG.VentilatoreCurrent.Esecuzioni[i].equals(l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione)) {
				prezzoVentilatore = l_VG.VentilatoreCurrent.PrezzoEsecuzioni[i];
				break;
			}
		}
         */
        this.l_VG.fmtNd.setMinimumFractionDigits(2);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);

        if (this.l_VG.VentilatoreCurrent.PrezzoVentilatoreForSort > 0.) {
            int esecuzione = -1;
            for (int i = 0; i < this.l_VG.VentilatoreCurrent.Esecuzioni.length; i++) {
                if (this.l_VG.VentilatoreCurrent.Esecuzioni[i] == this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione.toString()) {
                    esecuzione = i;
                }
            }
            obj[4] = this.l_VG.fmtNd.format(this.l_VG.VentilatoreCurrent.PrezzoEsecuzioni[esecuzione]);
            obj[5] = this.l_VG.VentilatoreCurrent.PrezzoEsecuzioni[esecuzione];
        } else if (this.l_VG.VentilatoreCurrent.PrezzoVentilatoreForSort == 0.) {
            obj[4] = "---";
            obj[5] = 0.0;
        } else {
            this.l_VG.VentilatoreCurrent.PrezzoVentilatoreForSort = 0.;
            obj[4] = this.aRichiestaTradotto;
            obj[5] = 0.0;
        }
        this.tableItem.addItem(obj, "0");
        deltaRow++;
        this.prezzoMotore = 0.;
        if (this.ventilatorePreventivo.HaMotore) {
            obj = new Object[6];
            final CheckBox cbm = new CheckBox();
            cbm.setData("1");
            cbm.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItem.isEnabled()) {
                        return;
                    }
                    final boolean value = cbm.getValue().booleanValue();
                    PreventiviCosto.this.ventilatorePreventivo.MotoreSelezionato = value;
                    calcolaPrezzoTotale();
                }
            });
            cbm.setValue(this.ventilatorePreventivo.MotoreSelezionato);
            obj[0] = cbm;
            obj[1] = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente;
            String descrizioneMotore = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Descrizione;
            if (descrizioneMotore.equals("-")) {
                descrizioneMotore = this.l_VG.utilityTraduzioni.TraduciStringa("Motore");
            }
            obj[2] = descrizioneMotore;
            final TextFieldInteger tim = new TextFieldInteger();
            tim.setData("1");
            tim.setEnabled(false);
            tim.setValue(1);
            obj[3] = tim;
            this.l_VG.fmtNd.setMinimumFractionDigits(2);
            this.l_VG.fmtNd.setMaximumFractionDigits(2);
            this.prezzoMotore = this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.Prezzo;
            if (this.prezzoMotore > 0.) {
                obj[4] = this.l_VG.fmtNd.format(this.prezzoMotore);
                obj[5] = this.prezzoMotore;
            } else if (this.prezzoMotore == 0.) {
                obj[4] = "---";
                obj[5] = 0.0;
            } else {
                this.prezzoMotore = 0.;
                obj[4] = this.aRichiestaTradotto;
                obj[5] = 0.0;
            }
            this.tableItem.addItem(obj, "1");
            deltaRow++;
        }
        this.l_VG.fmtNd.setMinimumFractionDigits(2);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);

        for (int i = 0; i < this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.size(); i++) {
            final String itemID = Integer.toString(i + deltaRow);
            final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessori.get(i);
            obj = new Object[6];
            final CheckBox cba = new CheckBox();
            cba.setData(l_a.Codice);
            cba.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItem.isEnabled()) {
                        return;
                    }
                    final String accessorioCode = cba.getData().toString();
                    final Item item = PreventiviCosto.this.containerItem.getItem(itemID);
                    PreventiviCosto.this.l_VG.accessorioInEsame = PreventiviCosto.this.l_VG.gestioneAccessori.getAccessorio(PreventiviCosto.this.l_VG.VentilatoreCurrent, accessorioCode);
                    if (PreventiviCosto.this.l_VG.accessorioInEsame != null) {
                        if (PreventiviCosto.this.l_VG.accessorioInEsame.Forzato) {
                            PreventiviCosto.this.l_VG.accessorioInEsame.Selezionato = PreventiviCosto.this.l_VG.accessorioInEsame.ValoreForzatura;
                            cba.setValue(PreventiviCosto.this.l_VG.accessorioInEsame.Selezionato);
                            return;
                        }
                        final boolean value = cba.getValue().booleanValue();
                        PreventiviCosto.this.l_VG.accessorioInEsame.Selezionato = value;
                        PreventiviCosto.this.l_VG.condizionaAccessori();
                        PreventiviCosto.this.l_VG.accessoriAzioni.eseguiAzioneAccessorioInEsame(item, PreventiviCosto.this.l_VG.VentilatoreCurrent, PreventiviCosto.this.l_VG.accessorioInEsame, PreventiviCosto.this.l_VG.elencoGruppiAccessori);
                        PreventiviCosto.this.l_VG.ventilatoreCambiato = true;
                        if (!PreventiviCosto.this.l_VG.accessorioInEsame.azioneAssociata.startsWith("dialog")) {
                            initContainerItem();
                            setCostoVentilatore();
                        }
                        calcolaPrezzoTotale();
                    }
                    /*
						Object itemId = cba.getData();
				        Accessorio l_aloc = l_VG.gestioneAccessori.getAccessorio(l_VG.VentilatoreCurrent, itemId.toString());
				        if (l_aloc != null) {
					        boolean value = ((Boolean)cba.getValue()).booleanValue();
					        l_aloc.Selezionato = value;
					        calcolaPrezzoTotale();
				        }
                     */
                }
            });
            cba.setValue(l_a.Selezionato);
            obj[0] = cba;
            obj[1] = l_a.Codice;
            obj[2] = this.l_VG.utilityCliente.nazionalizzaDescrizioneAccessorio(l_a);
            //obj[2] = l_a.Descrizione + l_VG.gestioneAccessori.getAccessorioValoreParametro(l_a, "cod");
            final TextFieldInteger tia = new TextFieldInteger();
            tia.setData(l_a.Codice);
            tia.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItem.isEnabled()) {
                        return;
                    }
                    final Object itemId = tia.getData();
                    //Notification.show(itemId.toString());
                    final Accessorio l_aloc = PreventiviCosto.this.l_VG.gestioneAccessori.getAccessorio(PreventiviCosto.this.l_VG.VentilatoreCurrent, itemId.toString());
                    if (l_aloc != null) {
                        final int value = tia.getIntegerValue();
                        l_aloc.qta = value;
                        calcolaPrezzoTotale();
                    }
                }
            });
            tia.setEnabled(l_a.qtaModificabile);
            tia.setValue(l_a.qta);
            obj[3] = tia;
            if (l_a.Prezzo > 0.) {
                obj[4] = this.l_VG.fmtNd.format(l_a.Prezzo);
                obj[5] = l_a.Prezzo;
            } else if (l_a.Prezzo == 0.) {
                obj[4] = "---";
                obj[5] = 0.0;
            } else {
                obj[4] = this.aRichiestaTradotto;
                obj[5] = 0.0;
            }
            this.tableItem.addItem(obj, itemID);
        }
//load tabella itemFree
        try {
            this.tableItemFree.removeAllItems();//resetta anche le selection
        } catch (final Exception e) {

        }
        for (int i = 0; i < this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.size(); i++) {
            obj = new Object[5];
            final String itemID = Integer.toString(i);
            final Accessorio l_a = this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(i);
            final CheckBox cbf = new CheckBox();
            cbf.setData(itemID);
            cbf.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItemFree.isEnabled()) {
                        return;
                    }
                    final Object itemId = cbf.getData();
                    final Accessorio l_aloc = PreventiviCosto.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(Integer.parseInt(itemId.toString()));
                    final boolean value = cbf.getValue().booleanValue();
                    l_aloc.Selezionato = value;
                    calcolaPrezzoTotale();
                }
            });
            cbf.setValue(l_a.Selezionato);
            obj[0] = cbf;
            final TextField tfc = new TextField();
            tfc.setData(itemID);
            tfc.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItemFree.isEnabled()) {
                        return;
                    }
                    final Object itemId = tfc.getData();
                    final Accessorio l_aloc = PreventiviCosto.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(Integer.parseInt(itemId.toString()));
                    l_aloc.Codice = tfc.getValue();
                }
            });
            tfc.setValue(l_a.Codice);
            obj[1] = tfc;
            final TextField tfd = new TextField();
            tfd.setData(itemID);
            tfd.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItemFree.isEnabled()) {
                        return;
                    }
                    final Object itemId = tfd.getData();
                    final Accessorio l_aloc = PreventiviCosto.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(Integer.parseInt(itemId.toString()));
                    l_aloc.Descrizione = tfd.getValue();
                }
            });
            tfd.setValue(l_a.Descrizione);
            obj[2] = tfd;
            final TextFieldInteger tif = new TextFieldInteger();
            tif.setData(itemID);
            tif.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItemFree.isEnabled()) {
                        return;
                    }
                    final Object itemId = tif.getData();
                    final Accessorio l_aloc = PreventiviCosto.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(Integer.parseInt(itemId.toString()));
                    l_aloc.qta = tif.getIntegerValue();
                    calcolaPrezzoTotale();
                }
            });
            tif.setValue(l_a.qta);
            obj[3] = tif;
            final TextFieldDouble tdf = new TextFieldDouble();
            tdf.setMaximumFractionDigits(2);
            tdf.setData(itemID);
            tdf.addValueChangeListener(new ValueChangeListener() {
                @Override
                public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
                    if (!PreventiviCosto.this.tableItemFree.isEnabled()) {
                        return;
                    }
                    final Object itemId = tdf.getData();
                    final Accessorio l_aloc = PreventiviCosto.this.l_VG.VentilatoreCurrent.selezioneCorrente.ElencoAccessoriFree.get(Integer.parseInt(itemId.toString()));
                    if (tdf.getDoubleValue() > 0.) {
                        l_aloc.Prezzo = tdf.getDoubleValue();
                    } else if (tdf.getValue().equals("")) {
                        l_aloc.Prezzo = 0.;
                    } else {
                        l_aloc.Prezzo = -1.;
                    }
                    calcolaPrezzoTotale();
                }
            });
            if (l_a.Prezzo > 0.) {
                tdf.setValue(l_a.Prezzo);
            }
            tdf.setMaximumFractionDigits(2);
            obj[4] = tdf;
            this.tableItemFree.addItem(obj, itemID);
        }
        this.tableItem.setEnabled(true);
        this.tableItemFree.setEnabled(true);
        calcolaPrezzoTotale();
    }

    public void calcolaPrezzoTotale() {
        if (this.ventilatorePreventivo == null || this.l_VG.VentilatoreCurrent == null) {
            return;
        }
        this.l_VG.calcolaCostoVentilatore(this.ventilatorePreventivo, this.l_VG.VentilatoreCurrent);
        final PreventiviView preventivi = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        this.l_VG.preventivoCurrent.CostoTotale = preventivi.calcolaCostoPreventivo();
        this.l_VG.fmtNd.setMinimumFractionDigits(2);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
        this.labelPrezzoTotaleVal.setValue("<html><b><font color='blue' size='+1'>" + this.l_VG.fmtNd.format(this.ventilatorePreventivo.CostoVentilatore) + " " + Costanti.EURO + "</html>");
        //this.ventilatorePreventivo.CostoVentilatore *= (100. - this.l_VG.preventivoCurrent.Sconto) / 100.;
        this.labelPrezzoAcquistoVal.setValue("<html><b><font color='blue' size='+1'>" + this.l_VG.fmtNd.format(this.ventilatorePreventivo.CostoVentilatore * (100. - this.l_VG.preventivoCurrent.Sconto) / 100.) + " " + Costanti.EURO + "</html>");
        this.l_VG.fmtNd.setMinimumFractionDigits(0);
        this.l_VG.fmtNd.setMaximumFractionDigits(1);
        this.labelScontoVal.setValue(this.l_VG.fmtNd.format(this.l_VG.preventivoCurrent.Sconto));
        this.preventiviView.aggiornaPreventivo();
        //Notification.show("calcolaPrezzoTotale   "+Double.toString(ventilatorePreventivo.CostoVentilatore));
    }

    /*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
     */
    // <generated-code name="initUI">
    private void initUI() {
        this.verticalLayout = new XdevVerticalLayout();
        this.tableItem = new XdevTable<>();
        this.tableItemFree = new XdevTable<>();
        this.horizontalLayout = new XdevHorizontalLayout();
        this.labelPrezzoTotale = new XdevLabel();
        this.labelPrezzoTotaleVal = new XdevLabel();
        this.label6 = new XdevLabel();
        this.labelSconto = new XdevLabel();
        this.labelScontoVal = new XdevLabel();
        this.label3 = new XdevLabel();
        this.labelPrezzoAcquisto = new XdevLabel();
        this.labelPrezzoAcquistoVal = new XdevLabel();

        this.verticalLayout.setMargin(new MarginInfo(false, true, false, true));
        this.tableItem.setCaption("Conto Economico");
        this.tableItem.setStyleName("small mystriped");
        this.tableItemFree.setCaption("Accessori Liberi");
        this.tableItemFree.setStyleName("small mystriped");
        this.horizontalLayout.setMargin(new MarginInfo(false, true, false, true));
        this.labelPrezzoTotale.setValue("<b><font size='+1'>Prezzo Totale:");
        this.labelPrezzoTotale.setContentMode(ContentMode.HTML);
        this.labelPrezzoTotaleVal.setValue("<b><font color='blue' size='+1'>???");
        this.labelPrezzoTotaleVal.setContentMode(ContentMode.HTML);
        this.labelSconto.setValue("Sconto %");
        this.labelScontoVal.setValue("???");
        this.labelPrezzoAcquisto.setValue("<b><font size='+1'>Prezzo di vendita:");
        this.labelPrezzoAcquisto.setContentMode(ContentMode.HTML);
        this.labelPrezzoAcquistoVal.setValue("<b><font color='blue' size='+1'>???");
        this.labelPrezzoAcquistoVal.setContentMode(ContentMode.HTML);

        this.labelPrezzoTotale.setSizeUndefined();
        this.horizontalLayout.addComponent(this.labelPrezzoTotale);
        this.horizontalLayout.setComponentAlignment(this.labelPrezzoTotale, Alignment.MIDDLE_LEFT);
        this.labelPrezzoTotaleVal.setSizeUndefined();
        this.horizontalLayout.addComponent(this.labelPrezzoTotaleVal);
        this.horizontalLayout.setComponentAlignment(this.labelPrezzoTotaleVal, Alignment.MIDDLE_LEFT);
        this.label6.setWidth(30, Unit.PIXELS);
        this.label6.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout.addComponent(this.label6);
        this.horizontalLayout.setComponentAlignment(this.label6, Alignment.MIDDLE_CENTER);
        this.labelSconto.setSizeUndefined();
        this.horizontalLayout.addComponent(this.labelSconto);
        this.horizontalLayout.setComponentAlignment(this.labelSconto, Alignment.MIDDLE_CENTER);
        this.labelScontoVal.setSizeUndefined();
        this.horizontalLayout.addComponent(this.labelScontoVal);
        this.horizontalLayout.setComponentAlignment(this.labelScontoVal, Alignment.MIDDLE_CENTER);
        this.label3.setWidth(30, Unit.PIXELS);
        this.label3.setHeight(-1, Unit.PIXELS);
        this.horizontalLayout.addComponent(this.label3);
        this.horizontalLayout.setComponentAlignment(this.label3, Alignment.MIDDLE_CENTER);
        this.labelPrezzoAcquisto.setSizeUndefined();
        this.horizontalLayout.addComponent(this.labelPrezzoAcquisto);
        this.horizontalLayout.setComponentAlignment(this.labelPrezzoAcquisto, Alignment.MIDDLE_CENTER);
        this.labelPrezzoAcquistoVal.setSizeUndefined();
        this.horizontalLayout.addComponent(this.labelPrezzoAcquistoVal);
        this.horizontalLayout.setComponentAlignment(this.labelPrezzoAcquistoVal, Alignment.MIDDLE_LEFT);
        final CustomComponent horizontalLayout_spacer = new CustomComponent();
        horizontalLayout_spacer.setSizeFull();
        this.horizontalLayout.addComponent(horizontalLayout_spacer);
        this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
        this.tableItem.setSizeFull();
        this.verticalLayout.addComponent(this.tableItem);
        this.verticalLayout.setComponentAlignment(this.tableItem, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableItem, 100.0F);
        this.tableItemFree.setSizeFull();
        this.verticalLayout.addComponent(this.tableItemFree);
        this.verticalLayout.setComponentAlignment(this.tableItemFree, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setExpandRatio(this.tableItemFree, 50.0F);
        this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayout.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.horizontalLayout);
        this.verticalLayout.setComponentAlignment(this.horizontalLayout, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setSizeFull();
        this.setContent(this.verticalLayout);
        this.setSizeFull();
    } // </generated-code>

    // <generated-code name="variables">
    private XdevLabel labelPrezzoTotale, labelPrezzoTotaleVal, label6, labelSconto, labelScontoVal, label3, labelPrezzoAcquisto, labelPrezzoAcquistoVal;
    private XdevTable<CustomComponent> tableItem, tableItemFree;
    private XdevHorizontalLayout horizontalLayout;
    private XdevVerticalLayout verticalLayout;
    // </generated-code>

}
