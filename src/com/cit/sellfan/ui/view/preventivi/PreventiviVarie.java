package com.cit.sellfan.ui.view.preventivi;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DateField;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextArea;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

public class PreventiviVarie extends XdevView {
	private VariabiliGlobali l_VG;
	private final DateField textDataPreventivo = new DateField();
	private final DateField textDataConsegna = new DateField();
	private boolean editEnabled = false;
	/**
	 * 
	 */
	public PreventiviVarie() {
		super();
		this.initUI();
		this.textDataPreventivo.setDateFormat("dd/MM/yyyy");
		this.textDataConsegna.setDateFormat("dd/MM/yyyy");
		this.textDataPreventivo.setWidth(100, Unit.PERCENTAGE);
		this.textDataPreventivo.setHeight(-1, Unit.PIXELS);
		this.gridLayout.replaceComponent(this.textDataPreventivoGosth, this.textDataPreventivo);
		this.textDataConsegna.setWidth(100, Unit.PERCENTAGE);
		this.textDataConsegna.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.replaceComponent(this.textDataConsegnaGosth, this.textDataConsegna);
		this.textDataPreventivo.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				if (!PreventiviVarie.this.editEnabled) {
					return;
				}
				PreventiviVarie.this.l_VG.preventivoCurrent.PreventivoCambiato = true;
				PreventiviVarie.this.l_VG.preventivoCurrent.Data = PreventiviVarie.this.textDataPreventivo.getValue();
			}
		});
		this.textDataConsegna.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				if (!PreventiviVarie.this.editEnabled) {
					return;
				}
				PreventiviVarie.this.l_VG.preventivoCurrent.dataConsegnaInserita = true; // alla modifica del valore della data, imposta a true il flag per la scrittura
				PreventiviVarie.this.l_VG.preventivoCurrent.PreventivoCambiato = true;
				PreventiviVarie.this.l_VG.preventivoCurrent.ConsegnaRichiesta = PreventiviVarie.this.textDataConsegna.getValue();
			}
		});
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	}

	public void Nazionalizza() {
		this.panelDatiGenerali.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Dati Generali"));
		this.labelCodiceOfferta.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Codice Offerta"));
		this.labelCodiceOrdine.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Codice Ordine"));
		this.labelDataPreventivo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Data Preventivo"));
		this.labelNotePreventivo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Note Preventivo"));
		this.panelCondizioniFornitura.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Condizioni Fornitura"));
		this.labelDataConsegna.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Consegna Richiesta"));
		this.labelPagamento.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Pagamento"));
		this.labelSconto.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Sconto") + " %");
		this.labelBanca.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Banca"));
		this.labelIBAN.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("IBAN"));
		this.labelPorto.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Porto"));
		this.labelImballo.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Imballo"));
		this.labelCorriere.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Corriere"));
		this.labelResa.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Resa"));
		this.labelIndirizzoSpedizione.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Indirizzo di spedizione"));
		this.checkBoxImballoCompreso.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Compreso"));
//		this.panelDocumentazione.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Documentazione Richiesta"));
		//this.labelNumeroCopie.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Numero copie"));
		//this.labelLinguaDoc.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("In lingua"));
		this.labelUletrioriNote.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Ulterioni note"));
		this.checkBoxDisegniIngombro.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Disegni ingombro"));
		//this.checkBoxCurveCaratteristiche.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Curve Caratteristiche"));
		//this.labelNumeroCopie.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Numero copie"));
		this.labelDataConsegna.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Consegna Richiesta"));
	}
	
	public void setPreventivo() {
		this.editEnabled = false;
		this.textFieldCodiceOfferta.setValue(this.l_VG.preventivoCurrent.NumOfferta);
		this.textFieldCodiceOrdine.setValue(this.l_VG.preventivoCurrent.NumOrdine);
		this.labelNotePreventivo.setValue(this.l_VG.preventivoCurrent.Note);
		this.textFieldNoteConsegna.setValue(this.l_VG.preventivoCurrent.NoteConsegna);
		this.textFieldPagamento.setValue(this.l_VG.preventivoCurrent.Pagamento);
		this.textFieldSconto.setValue(this.l_VG.preventivoCurrent.Sconto);
		this.textFieldBanca.setValue(this.l_VG.preventivoCurrent.Banca);
		this.textFieldIBAN.setValue(this.l_VG.preventivoCurrent.IBAN);
		this.textFieldPorto.setValue(this.l_VG.preventivoCurrent.Porto);
		this.textFieldImballo.setValue(this.l_VG.preventivoCurrent.Imballo);
		this.textFieldCorriere.setValue(this.l_VG.preventivoCurrent.Corriere);
		this.textFieldResa.setValue(this.l_VG.preventivoCurrent.Resa);
		this.textFieldIndirizzoSpedizione.setValue(this.l_VG.preventivoCurrent.IndirizzoSpedizione);
		//this.textFieldNumeroCopie.setValue(this.l_VG.preventivoCurrent.DocNCopie);
		//this.textFieldLinguaDoc.setValue(this.l_VG.preventivoCurrent.DocNCopieLingua);
		this.textAreaUlterioriNote.setValue(this.l_VG.preventivoCurrent.UlterioriNote);
		this.checkBoxImballoCompreso.setValue(this.l_VG.preventivoCurrent.ImballoCompreso);
		this.checkBoxDisegniIngombro.setValue(this.l_VG.preventivoCurrent.DocDisegniIngombro);
		//this.checkBoxCurveCaratteristiche.setValue(this.l_VG.preventivoCurrent.DocCurveCaratteristiche);
		this.textDataPreventivo.setValue(this.l_VG.preventivoCurrent.Data);
		this.textDataConsegna.setValue(this.l_VG.preventivoCurrent.ConsegnaRichiesta);
		this.editEnabled = true;
	}
	
	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldCodiceOfferta}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldCodiceOfferta_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.NumOfferta = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldCodiceOrdine}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldCodiceOrdine_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.NumOrdine = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNotePreventivo}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNotePreventivo_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Note = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNoteConsegna}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNoteConsegna_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.NoteConsegna = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPagamento}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPagamento_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Pagamento = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldBanca}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldBanca_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Banca = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldIBAN}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldIBAN_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.IBAN = this.textFieldIBAN.getValue();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPorto}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPorto_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Porto = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldImballo}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldImballo_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Imballo = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldCorriere}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldCorriere_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Corriere = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldResa}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldResa_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Resa = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldIndirizzoSpedizione}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldIndirizzoSpedizione_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.IndirizzoSpedizione = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextArea}
	 * {@link #textAreaUlterioriNote}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textAreaUlterioriNote_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.UlterioriNote = event.getText();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldSconto}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldSconto_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.Sconto = this.textFieldSconto.getDoubleValue();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNumeroCopie}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
/*	private void textFieldNumeroCopie_textChange(final FieldEvents.TextChangeEvent event) {
		if (!this.editEnabled) {
			return;
		}
		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
		this.l_VG.preventivoCurrent.DocNCopie = this.textFieldNumeroCopie.getIntegerValue();
	}*/

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldLinguaDoc}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
//	private void textFieldLinguaDoc_textChange(final FieldEvents.TextChangeEvent event) {
//		if (!this.editEnabled) {
//			return;
//		}
//		this.l_VG.preventivoCurrent.PreventivoCambiato = true;
//		this.l_VG.preventivoCurrent.DocNCopieLingua = this.textFieldLinguaDoc.getIntegerValue();
//	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.panelDatiGenerali = new XdevPanel();
		this.gridLayout = new XdevGridLayout();
		this.labelCodiceOfferta = new XdevLabel();
		this.textFieldCodiceOfferta = new XdevTextField();
		this.labelCodiceOrdine = new XdevLabel();
		this.textFieldCodiceOrdine = new XdevTextField();
		this.labelDataPreventivo = new XdevLabel();
		this.textDataPreventivoGosth = new XdevTextField();
		this.labelNotePreventivo = new XdevLabel();
		this.textFieldNotePreventivo = new XdevTextField();
		this.panelCondizioniFornitura = new XdevPanel();
		this.gridLayout2 = new XdevGridLayout();
		this.labelDataConsegna = new XdevLabel();
		this.textDataConsegnaGosth = new XdevTextField();
		this.textFieldNoteConsegna = new XdevTextField();
		this.labelPagamento = new XdevLabel();
		this.textFieldPagamento = new XdevTextField();
		this.labelSconto = new XdevLabel();
		this.textFieldSconto = new TextFieldDouble();
		this.labelBanca = new XdevLabel();
		this.textFieldBanca = new XdevTextField();
		this.labelIBAN = new XdevLabel();
		this.textFieldIBAN = new XdevTextField();
		this.labelPorto = new XdevLabel();
		this.textFieldPorto = new XdevTextField();
		this.labelImballo = new XdevLabel();
		this.textFieldImballo = new XdevTextField();
		this.checkBoxImballoCompreso = new XdevCheckBox();
		this.labelCorriere = new XdevLabel();
		this.textFieldCorriere = new XdevTextField();
		this.labelResa = new XdevLabel();
		this.textFieldResa = new XdevTextField();
		this.labelIndirizzoSpedizione = new XdevLabel();
		this.textFieldIndirizzoSpedizione = new XdevTextField();
//		this.panelDocumentazione = new XdevPanel();
		this.gridLayout3 = new XdevGridLayout();
//		this.labelNumeroCopie = new XdevLabel();
		//this.textFieldNumeroCopie = new TextFieldInteger();
		this.labelLinguaDoc = new XdevLabel();
//		this.textFieldLinguaDoc = new TextFieldInteger();
		this.checkBoxDisegniIngombro = new XdevCheckBox();
//		this.checkBoxCurveCaratteristiche = new XdevCheckBox();
		this.labelUletrioriNote = new XdevLabel();
		this.textAreaUlterioriNote = new XdevTextArea();
	
		this.panelDatiGenerali.setCaption("Dati Generali");
		this.gridLayout.setMargin(new MarginInfo(false));
		this.labelCodiceOfferta.setValue("CodiceOfferta");
		this.labelCodiceOrdine.setValue("Codice Ordine");
		this.labelDataPreventivo.setValue("Data Preventivo");
		this.labelNotePreventivo.setValue("Note Preventivo");
		this.labelNotePreventivo.setVisible(false);
		this.textFieldNotePreventivo.setVisible(false);
		this.panelCondizioniFornitura.setCaption("Condizioni Fornitura");
		this.gridLayout2.setMargin(new MarginInfo(false));
		this.labelDataConsegna.setValue("Consegna Richiesta");
		this.labelPagamento.setValue("Pagamento");
		this.labelSconto.setValue("Sconto %");
		this.labelBanca.setValue("Banca");
		this.labelIBAN.setValue("IBAN");
		this.labelPorto.setValue("Porto");
		this.labelImballo.setValue("Imballo");
		this.checkBoxImballoCompreso.setCaption("Compreso");
		this.labelCorriere.setValue("Corriere");
		this.labelResa.setValue("Resa");
		this.labelIndirizzoSpedizione.setValue("Indirizzo Spedizione");
//		this.panelDocumentazione.setCaption("Documentazione Richiesta");
		this.gridLayout3.setMargin(new MarginInfo(false));
//		this.labelNumeroCopie.setValue("Numero copie");
		this.labelLinguaDoc.setValue("In lingua");
		this.checkBoxDisegniIngombro.setCaption("Disegni Ingombro");
		this.checkBoxDisegniIngombro.setVisible(false);
//		this.checkBoxCurveCaratteristiche.setCaption("Curve Caratteristiche");
		this.labelUletrioriNote.setValue("Note Preventivo");
		this.labelUletrioriNote.setVisible(false);
		this.textAreaUlterioriNote.setVisible(false);
		this.textAreaUlterioriNote.setRows(2);
	
		this.gridLayout.setColumns(4);
		this.gridLayout.setRows(4);
		this.labelCodiceOfferta.setSizeUndefined();
		this.gridLayout.addComponent(this.labelCodiceOfferta, 0, 0);
		this.gridLayout.setComponentAlignment(this.labelCodiceOfferta, Alignment.TOP_RIGHT);
		this.textFieldCodiceOfferta.setWidth(100, Unit.PERCENTAGE);
		this.textFieldCodiceOfferta.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldCodiceOfferta, 1, 0);
		this.labelCodiceOrdine.setSizeUndefined();
		this.gridLayout.addComponent(this.labelCodiceOrdine, 2, 0);
		this.gridLayout.setComponentAlignment(this.labelCodiceOrdine, Alignment.TOP_RIGHT);
		this.textFieldCodiceOrdine.setWidth(100, Unit.PERCENTAGE);
		this.textFieldCodiceOrdine.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldCodiceOrdine, 3, 0);
		this.labelDataPreventivo.setSizeUndefined();
		this.gridLayout.addComponent(this.labelDataPreventivo, 0, 1);
		this.gridLayout.setComponentAlignment(this.labelDataPreventivo, Alignment.TOP_RIGHT);
		this.textDataPreventivoGosth.setWidth(100, Unit.PERCENTAGE);
		this.textDataPreventivoGosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textDataPreventivoGosth, 1, 1);
		this.labelNotePreventivo.setSizeUndefined();
		this.gridLayout.addComponent(this.labelNotePreventivo, 0, 2);
		this.gridLayout.setComponentAlignment(this.labelNotePreventivo, Alignment.TOP_RIGHT);
		this.textFieldNotePreventivo.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNotePreventivo.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldNotePreventivo, 1, 2, 3, 2);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		this.gridLayout.setColumnExpandRatio(2, 10.0F);
		this.gridLayout.setColumnExpandRatio(3, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 3, 3, 3);
		this.gridLayout.setRowExpandRatio(3, 1.0F);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.panelDatiGenerali.setContent(this.gridLayout);
		this.gridLayout2.setColumns(5);
		this.gridLayout2.setRows(7);
		this.labelDataConsegna.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelDataConsegna, 0, 0);
		this.gridLayout2.setComponentAlignment(this.labelDataConsegna, Alignment.TOP_RIGHT);
		this.textDataConsegnaGosth.setWidth(100, Unit.PERCENTAGE);
		this.textDataConsegnaGosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textDataConsegnaGosth, 1, 0);
		this.textFieldNoteConsegna.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNoteConsegna.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldNoteConsegna, 2, 0, 4, 0);
		this.gridLayout2.setComponentAlignment(this.textFieldNoteConsegna, Alignment.MIDDLE_CENTER);
		this.labelPagamento.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPagamento, 0, 1);
		this.gridLayout2.setComponentAlignment(this.labelPagamento, Alignment.TOP_RIGHT);
		this.textFieldPagamento.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPagamento.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPagamento, 1, 1, 2, 1);
		this.labelSconto.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelSconto, 3, 1);
		this.gridLayout2.setComponentAlignment(this.labelSconto, Alignment.TOP_RIGHT);
		this.textFieldSconto.setWidth(100, Unit.PERCENTAGE);
		this.textFieldSconto.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldSconto, 4, 1);
		this.labelBanca.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelBanca, 0, 2);
		this.gridLayout2.setComponentAlignment(this.labelBanca, Alignment.TOP_RIGHT);
		this.textFieldBanca.setWidth(100, Unit.PERCENTAGE);
		this.textFieldBanca.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldBanca, 1, 2);
		this.labelIBAN.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelIBAN, 2, 2);
		this.gridLayout2.setComponentAlignment(this.labelIBAN, Alignment.TOP_RIGHT);
		this.textFieldIBAN.setWidth(100, Unit.PERCENTAGE);
		this.textFieldIBAN.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldIBAN, 3, 2, 4, 2);
		this.labelPorto.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPorto, 0, 3);
		this.gridLayout2.setComponentAlignment(this.labelPorto, Alignment.TOP_RIGHT);
		this.textFieldPorto.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPorto.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPorto, 1, 3);
		this.labelImballo.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelImballo, 2, 3);
		this.gridLayout2.setComponentAlignment(this.labelImballo, Alignment.TOP_RIGHT);
		this.textFieldImballo.setWidth(100, Unit.PERCENTAGE);
		this.textFieldImballo.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldImballo, 3, 3);
		this.checkBoxImballoCompreso.setSizeUndefined();
		this.gridLayout2.addComponent(this.checkBoxImballoCompreso, 4, 3);
		this.labelCorriere.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelCorriere, 0, 4);
		this.gridLayout2.setComponentAlignment(this.labelCorriere, Alignment.TOP_RIGHT);
		this.textFieldCorriere.setWidth(100, Unit.PERCENTAGE);
		this.textFieldCorriere.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldCorriere, 1, 4, 2, 4);
		this.labelResa.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelResa, 3, 4);
		this.gridLayout2.setComponentAlignment(this.labelResa, Alignment.TOP_RIGHT);
		this.textFieldResa.setWidth(100, Unit.PERCENTAGE);
		this.textFieldResa.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldResa, 4, 4);
		this.labelIndirizzoSpedizione.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelIndirizzoSpedizione, 0, 5);
		this.gridLayout2.setComponentAlignment(this.labelIndirizzoSpedizione, Alignment.TOP_RIGHT);
		this.textFieldIndirizzoSpedizione.setWidth(100, Unit.PERCENTAGE);
		this.textFieldIndirizzoSpedizione.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldIndirizzoSpedizione, 1, 5, 4, 5);
		this.gridLayout2.setColumnExpandRatio(1, 10.0F);
		this.gridLayout2.setColumnExpandRatio(2, 10.0F);
		this.gridLayout2.setColumnExpandRatio(3, 10.0F);
		this.gridLayout2.setColumnExpandRatio(4, 10.0F);
		final CustomComponent gridLayout2_vSpacer = new CustomComponent();
		gridLayout2_vSpacer.setSizeFull();
		this.gridLayout2.addComponent(gridLayout2_vSpacer, 0, 6, 4, 6);
		this.gridLayout2.setRowExpandRatio(6, 1.0F);
		this.gridLayout2.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout2.setHeight(-1, Unit.PIXELS);
		this.panelCondizioniFornitura.setContent(this.gridLayout2);
		//this.gridLayout3.setColumns(6);
		//this.gridLayout3.setRows(3);
//		this.labelNumeroCopie.setSizeUndefined();
		//this.gridLayout3.addComponent(this.labelNumeroCopie, 0, 0);
		//this.gridLayout3.setComponentAlignment(this.labelNumeroCopie, Alignment.TOP_RIGHT);
		//this.textFieldNumeroCopie.setWidth(100, Unit.PERCENTAGE);
		//this.textFieldNumeroCopie.setHeight(-1, Unit.PIXELS);
		//this.gridLayout3.addComponent(this.textFieldNumeroCopie, 1, 0);
//		this.labelLinguaDoc.setSizeUndefined();
//		this.gridLayout3.addComponent(this.labelLinguaDoc, 2, 0);
//		this.gridLayout3.setComponentAlignment(this.labelLinguaDoc, Alignment.TOP_RIGHT);
//		this.textFieldLinguaDoc.setWidth(100, Unit.PERCENTAGE);
//		this.textFieldLinguaDoc.setHeight(-1, Unit.PIXELS);
//		this.gridLayout3.addComponent(this.textFieldLinguaDoc, 3, 0);
		this.checkBoxDisegniIngombro.setSizeUndefined();
		this.gridLayout3.addComponent(this.checkBoxDisegniIngombro, 4, 0);
//		this.checkBoxCurveCaratteristiche.setSizeUndefined();
//		this.gridLayout3.addComponent(this.checkBoxCurveCaratteristiche, 5, 0);
		this.labelUletrioriNote.setSizeUndefined();
		this.gridLayout3.addComponent(this.labelUletrioriNote, 0, 1);
		this.gridLayout3.setComponentAlignment(this.labelUletrioriNote, Alignment.TOP_RIGHT);
		this.textAreaUlterioriNote.setWidth(100, Unit.PERCENTAGE);
		this.textAreaUlterioriNote.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.textAreaUlterioriNote, 1, 1, 5, 1);
		this.gridLayout3.setColumnExpandRatio(1, 10.0F);
		this.gridLayout3.setColumnExpandRatio(2, 10.0F);
		this.gridLayout3.setColumnExpandRatio(3, 10.0F);
		this.gridLayout3.setColumnExpandRatio(4, 10.0F);
		this.gridLayout3.setColumnExpandRatio(5, 10.0F);
		final CustomComponent gridLayout3_vSpacer = new CustomComponent();
		gridLayout3_vSpacer.setSizeFull();
		this.gridLayout3.addComponent(gridLayout3_vSpacer, 0, 2, 5, 2);
		this.gridLayout3.setRowExpandRatio(2, 1.0F);
		this.gridLayout3.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout3.setHeight(-1, Unit.PIXELS);
//		this.panelDocumentazione.setContent(this.gridLayout3);
		this.panelDatiGenerali.setSizeFull();
		this.verticalLayout.addComponent(this.panelDatiGenerali);
		this.verticalLayout.setExpandRatio(this.panelDatiGenerali, 10.0F);
		this.panelCondizioniFornitura.setSizeFull();
		this.verticalLayout.addComponent(this.panelCondizioniFornitura);
		this.verticalLayout.setComponentAlignment(this.panelCondizioniFornitura, Alignment.MIDDLE_CENTER);
		this.verticalLayout.setExpandRatio(this.panelCondizioniFornitura, 10.0F);
//		this.panelDocumentazione.setSizeFull();
//		this.verticalLayout.addComponent(this.panelDocumentazione);
//		this.verticalLayout.setComponentAlignment(this.panelDocumentazione, Alignment.MIDDLE_CENTER);
//		this.verticalLayout.setExpandRatio(this.panelDocumentazione, 10.0F);
		this.verticalLayout.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayout.setHeight(-1, Unit.PIXELS);
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.textFieldCodiceOfferta.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldCodiceOfferta_textChange(event);
			}
		});
		this.textFieldCodiceOrdine.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldCodiceOrdine_textChange(event);
			}
		});
		this.textFieldNotePreventivo.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldNotePreventivo_textChange(event);
			}
		});
		this.textFieldNoteConsegna.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldNoteConsegna_textChange(event);
			}
		});
		this.textFieldPagamento.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldPagamento_textChange(event);
			}
		});
		this.textFieldSconto.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldSconto_textChange(event);
			}
		});
		this.textFieldBanca.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldBanca_textChange(event);
			}
		});
		this.textFieldIBAN.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldIBAN_textChange(event);
			}
		});
		this.textFieldPorto.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldPorto_textChange(event);
			}
		});
		this.textFieldImballo.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldImballo_textChange(event);
			}
		});
		this.textFieldCorriere.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldCorriere_textChange(event);
			}
		});
		this.textFieldResa.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldResa_textChange(event);
			}
		});
		this.textFieldIndirizzoSpedizione.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textFieldIndirizzoSpedizione_textChange(event);
			}
		});
//		this.textFieldNumeroCopie.addTextChangeListener(new FieldEvents.TextChangeListener() {
//			@Override
//			public void textChange(final FieldEvents.TextChangeEvent event) {
//				PreventiviVarie.this.textFieldNumeroCopie_textChange(event);
//			}
//		});
//		this.textFieldLinguaDoc.addTextChangeListener(new FieldEvents.TextChangeListener() {
//			@Override
//			public void textChange(final FieldEvents.TextChangeEvent event) {
//				PreventiviVarie.this.textFieldLinguaDoc_textChange(event);
//			}
//		});
		this.textAreaUlterioriNote.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				PreventiviVarie.this.textAreaUlterioriNote_textChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelCodiceOfferta, labelCodiceOrdine, labelDataPreventivo, labelNotePreventivo, labelDataConsegna,
			labelPagamento, labelSconto, labelBanca, labelIBAN, labelPorto, labelImballo, labelCorriere, labelResa,
			labelIndirizzoSpedizione, labelNumeroCopie, labelLinguaDoc, labelUletrioriNote;
	private XdevTextArea textAreaUlterioriNote;
	private XdevPanel panelDatiGenerali, panelCondizioniFornitura, panelDocumentazione;
	private XdevCheckBox checkBoxImballoCompreso, checkBoxDisegniIngombro, checkBoxCurveCaratteristiche;
	private XdevGridLayout gridLayout, gridLayout2, gridLayout3;
	private XdevTextField textFieldCodiceOfferta, textFieldCodiceOrdine, textDataPreventivoGosth, textFieldNotePreventivo,
			textDataConsegnaGosth, textFieldNoteConsegna, textFieldPagamento, textFieldBanca, textFieldIBAN, textFieldPorto,
			textFieldImballo, textFieldCorriere, textFieldResa, textFieldIndirizzoSpedizione;
	private XdevVerticalLayout verticalLayout;
	private TextFieldInteger textFieldNumeroCopie, textFieldLinguaDoc;
	private TextFieldDouble textFieldSconto;
	// </generated-code>


}