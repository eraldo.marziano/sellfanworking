package com.cit.sellfan.ui.view.preventivi;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.pannelli.pannelloEditPreventivo;
import com.cit.sellfan.ui.pannelli.pannelloPreventivoEditNew;
import com.cit.sellfan.ui.view.PreventiviView;
import com.cit.sellfan.ui.view.ProgressView;
import com.cit.sellfan.ui.view.cliente04.Cliente04pannelloPagineOfferta;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.classi.Clientev21;
import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.sellfan.Costanti;
import cit.sellfan.classi.Preventivo;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.classi.ventilatore.VentilatorePreventivo;
import cit.sellfan.personalizzazioni.cliente04.Cliente04StampaOffertaPoiXLSX;
import de.steinwedel.messagebox.MessageBox;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

public class PreventiviVentilatori extends XdevView {
    private VariabiliGlobali l_VG;
    private final TextFieldInteger textFieldCodiceCliente = new TextFieldInteger();
    private final DateField textFieldDataIn = new DateField();
    private final DateField textFieldDataOut = new DateField();
    private Container containerPreventivi;
    private Container containerVentilatori;
    private final ArrayList<String> ElencoFileXls = new ArrayList<>();
    private FileDownloader fileDownloader;
    private final ArrayList<XdevTextField> elencoNoteVentilatori = new ArrayList<>();
    private MessageBox msgBox;
    private String memoDownload;
    public java.text.NumberFormat fmtNd = java.text.NumberFormat.getInstance();
    private static Logger logger;

    public PreventiviVentilatori() {
        super();
        LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
        File file = new File("C:\\JavaLogs\\log4j2.xml");
        context.setConfigLocation(file.toURI());
        logger = LogManager.getLogger(PreventiviVentilatori.class);
        this.initUI();
        initContainerPreventivi();
        initContainerVentilatori();
        this.buttonCambiaMotore.setEnabled(false);
        this.buttonAzione0.setEnabled(this.buttonCambiaMotore.isEnabled());
        this.buttonAzione1.setEnabled(this.buttonCambiaMotore.isEnabled());
        this.buttonAzione2.setEnabled(this.buttonCambiaMotore.isEnabled());
    }
	
    public void setPreventivoSelected(final String idPreventivo) {
        this.tablePreventivi.select(idPreventivo);
    }
	
    private void initContainerPreventivi() {
        this.containerPreventivi = new IndexedContainer();
        this.containerPreventivi.addContainerProperty("Offerta", String.class, "");
        this.containerPreventivi.addContainerProperty("Cliente", Integer.class, 0);
        this.containerPreventivi.addContainerProperty("RSoc", String.class, "");
        this.containerPreventivi.addContainerProperty("Data", Date.class, null);
        this.containerPreventivi.addContainerProperty("Progetto", String.class, "");
        this.containerPreventivi.addContainerProperty("Price", Double.class, "");
        this.containerPreventivi.addContainerProperty("PriceUser", Double.class, "");
        this.containerPreventivi.addContainerProperty("N", Integer.class, 0);
        this.containerPreventivi.addContainerProperty("Note", String.class, "");
        this.containerPreventivi.addContainerProperty("Codice", Integer.class, 0);
        this.tablePreventivi.setContainerDataSource(this.containerPreventivi);
    }
	
    private void initContainerVentilatori() {
        this.containerVentilatori = new IndexedContainer();
        this.containerVentilatori.addContainerProperty("Selected", CheckBox.class, false);
        this.containerVentilatori.addContainerProperty("Qta", TextFieldInteger.class, 0);
        this.containerVentilatori.addContainerProperty("Ventilatore", String.class, "");
        this.containerVentilatori.addContainerProperty("Item", String.class, "");
        this.containerVentilatori.addContainerProperty("Rotazione", String.class, "");
        this.containerVentilatori.addContainerProperty("Esecuzione", String.class, "");
        this.containerVentilatori.addContainerProperty("Q", XdevLabel.class, "");
        this.containerVentilatori.addContainerProperty("Pt", XdevLabel.class, "");
        this.containerVentilatori.addContainerProperty("Densità", XdevLabel.class, "");
        this.containerVentilatori.addContainerProperty("Potenza", String.class, "");
        this.containerVentilatori.addContainerProperty("Rpm", String.class, "");
        this.containerVentilatori.addContainerProperty("Frequenza", String.class, "");
        this.containerVentilatori.addContainerProperty("Motore", String.class, "");
        this.containerVentilatori.addContainerProperty("Note", XdevTextField.class, "");
        this.containerVentilatori.addContainerProperty("Prezzo", String.class, "");
        this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
    }

    private void setConverterForTable() {
        final myStringToDoubleConverter converter = new myStringToDoubleConverter();
        this.tablePreventivi.setConverter("Price", converter);
        this.tablePreventivi.setConverter("PriceUser", converter);
        this.tablePreventivi.setConverter("N", converter);
    }

    private void changeNotes(Ventilatore l_v, String value) {
        if (!value.equals("")) {
            l_v.Note = value;
        }
    }
	
    private class myStringToDoubleConverter extends StringToDoubleConverter {

        public myStringToDoubleConverter() {
            super();
        }

        @Override
        public java.text.NumberFormat getFormat(final Locale locale) {
            return PreventiviVentilatori.this.l_VG.fmtNdTable;
        }
    }
	
    public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
        this.l_VG = variabiliGlobali;
        if (this.l_VG.pannelloPreventivoAzioniSpeciali0Desc == null) {
            this.buttonAzione0.setVisible(false);
        }
        if (this.l_VG.pannelloPreventivoAzioniSpeciali1Desc == null) {
            this.buttonAzione1.setVisible(false);
        }
        if (this.l_VG.pannelloPreventivoAzioniSpeciali2Desc == null) {
            this.buttonAzione2.setVisible(false);
        }
        setConverterForTable();
    }
	
    public void Nazionalizza() {
        this.textFieldCodiceCliente.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Codice Cliente"));
        this.textFieldRagioneSociale.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale"));
        this.textFieldDataIn.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ricerca dal"));
        this.textFieldDataOut.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("al"));
        this.buttonResetFiltri.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset Filtri"));
        this.buttonFiltra.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Filtra Preventivi"));
        this.buttonModificaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Modifica dettagli Preventivo"));
        this.buttonDuplicaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Duplica Preventivo"));
        this.buttonSalvaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Salva e Aggiorna Preventivo"));
        this.buttonChiudiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Chiudi Preventivo"));
        this.buttonCancellaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella Preventivo"));
        this.buttonNuovoPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nuovo Preventivo"));
        this.buttonResetPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset Preventivo"));
        this.buttonCancellaVentilatore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Rimuovi Ventilatore"));
        this.buttonAzione0.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Azione0"));
        this.buttonAzione1.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Azione1"));
        this.buttonAzione2.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Azione2"));
        this.buttonCambiaMotore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Seleziona Motore"));
        this.buttonGeneraPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Genera Preventivo"));
        this.buttonDownloadPreventivo.setCaption("Download " + this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo"));
        this.tablePreventivi.setColumnHeader("Codice", "");
        this.tablePreventivi.setColumnHeader("Data", this.l_VG.utilityTraduzioni.TraduciStringa("Data"));
        this.tablePreventivi.setColumnHeader("Cliente", this.l_VG.utilityTraduzioni.TraduciStringa("Cliente"));
        this.tablePreventivi.setColumnHeader("RSoc", this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale"));
        this.tablePreventivi.setColumnHeader("Offerta", this.l_VG.utilityTraduzioni.TraduciStringa("Offerta"));
        this.tablePreventivi.setColumnHeader("Progetto", this.l_VG.utilityTraduzioni.TraduciStringa("Progetto"));
        this.tablePreventivi.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo Totale") + " " + Costanti.EURO);
        this.tablePreventivi.setColumnHeader("PriceUser", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo di vendita") + " " + Costanti.EURO);
        this.tablePreventivi.setColumnHeader("N", this.l_VG.utilityTraduzioni.TraduciStringa("N. Vent."));
        this.tablePreventivi.setColumnHeader("Note", this.l_VG.utilityTraduzioni.TraduciStringa("Note"));
        this.tableVentilatori.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
        this.tableVentilatori.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
        this.tableVentilatori.setColumnHeader("Ventilatore", this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatore"));
        this.tableVentilatori.setColumnHeader("Rotazione", this.l_VG.utilityTraduzioni.TraduciStringa("Rotazione"));
        this.tableVentilatori.setColumnHeader("Esecuzione", this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
        this.tableVentilatori.setColumnHeader("Q", StringheUNI.PORTATA);
        this.tableVentilatori.setColumnHeader("Pt", StringheUNI.PRESSIONETOTALE);
        this.tableVentilatori.setColumnHeader("Densità", this.l_VG.utilityTraduzioni.TraduciStringa("Densità"));
        this.tableVentilatori.setColumnHeader("Potenza", this.l_VG.utilityTraduzioni.TraduciStringa("Potenza"));
        this.tableVentilatori.setColumnHeader("Rpm", this.l_VG.utilityTraduzioni.TraduciStringa("Rpm"));
        this.tableVentilatori.setColumnHeader("Frequenza", this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza"));
        this.tableVentilatori.setColumnHeader("Motore", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
        this.tableVentilatori.setColumnHeader("Note", this.l_VG.utilityTraduzioni.TraduciStringa("Note"));
        this.tableVentilatori.setColumnHeader("Prezzo", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo"));

        if (this.l_VG.pannelloPreventivoAzioniSpeciali0Desc != null) {
            this.buttonAzione0.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.pannelloPreventivoAzioniSpeciali0Desc));
        }
        if (this.l_VG.pannelloPreventivoAzioniSpeciali1Desc != null) {
            this.buttonAzione1.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.pannelloPreventivoAzioniSpeciali1Desc));
        }
        if (this.l_VG.pannelloPreventivoAzioniSpeciali2Desc != null) {
            this.buttonAzione2.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.pannelloPreventivoAzioniSpeciali2Desc));
        }
    }
	
    public void storePreventivo() {
        this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
        aggiornaCostoPreventivoCorrente();
    }

    public void aggiornaVentilatoreCorrente() {
        final Collection<?> Ventilatoriids = this.containerVentilatori.getItemIds();
        int i = 0;
        this.tableVentilatori.setEnabled(false);
        for (final Object id : Ventilatoriids) {
            final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i++);
            final Item item = this.containerVentilatori.getItem(id);
            item.getItemProperty("Ventilatore").setValue(this.l_VG.buildModelloCompleto(l_v));
            XdevLabel lb = (XdevLabel) item.getItemProperty("Q").getValue();
            lb.setValue(String.format("%s %s",
                    arrotondaValore(l_v.UMPortata.nDecimaliStampa, l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, l_v.UMPortata)),
                    l_v.UMPortata.simbolo));
            lb = (XdevLabel) item.getItemProperty("Pt").getValue();
            lb.setValue(String.format("%s %s",
                    arrotondaValore(l_v.UMPressione.nDecimaliStampa, l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, l_v.UMPressione)),
                    l_v.UMPressione.simbolo));
            try {
                this.elencoNoteVentilatori.get(this.l_VG.VentilatoreCurrentIndex).setValue(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).Note);
            } catch (final Property.ReadOnlyException e) {
                logger.error("File was readOnly: " + e.getMessage());
            }
        }
        storePreventivo();
        this.tableVentilatori.setEnabled(true);
    }
	
    public void aggiornaRigaPreventivoCorrente() {
        if (this.l_VG.preventivoCurrent == null) {
            return;
        }
        final Item item = this.containerPreventivi.getItem(Integer.toString(l_VG.preventivoCurrent.IDPreventivo));
        if (item == null) {
            this.l_VG.MainView.setMemo(this.l_VG.utilityTraduzioni.TraduciStringa("Nessuna offerta selezionata - "));
            return;
        }
        this.l_VG.MainView.setMemo(String.format("%s %s - ", l_VG.utilityTraduzioni.TraduciStringa("Offerta selezionata:"), l_VG.preventivoCurrent.NumOfferta));
        item.getItemProperty("Offerta").setValue(l_VG.preventivoCurrent.NumOfferta);
        item.getItemProperty("Progetto").setValue(l_VG.preventivoCurrent.NumOrdine);
        final Date d = new Date();
        d.setTime(l_VG.preventivoCurrent.Data.getTime());
        item.getItemProperty("Data").setValue(d);
        item.getItemProperty("Cliente").setValue(l_VG.preventivoCurrent.IDCliente);
        item.getItemProperty("RSoc").setValue(l_VG.preventivoCurrent.ragioneSocialeCliente);
        item.getItemProperty("N").setValue(l_VG.preventivoCurrent.Ventilatori.size());
        item.getItemProperty("Note").setValue(l_VG.preventivoCurrent.Note);
        item.getItemProperty("Price").setValue(l_VG.preventivoCurrent.CostoTotale);
        final double prezzoVendita = l_VG.preventivoCurrent.CostoTotale * (100. - l_VG.preventivoCurrent.Sconto) / 100.;
        item.getItemProperty("PriceUser").setValue(prezzoVendita);
        storePreventivo();
    }

     public void aggiornaCostoPreventivoCorrente() {
        final Item item = this.containerPreventivi.getItem(Integer.toString(this.l_VG.preventivoCurrent.IDPreventivo));
        if (item == null) {
            return;
        }
        item.getItemProperty("Price").setValue(this.l_VG.preventivoCurrent.CostoTotale);
        final double prezzoVendita = this.l_VG.preventivoCurrent.CostoTotale * (100. - this.l_VG.preventivoCurrent.Sconto) / 100.;
        item.getItemProperty("PriceUser").setValue(prezzoVendita);
    }
	
    public void aggiornaPreventivoCorrente() {
        aggiornaRigaPreventivoCorrente();
        this.tableVentilatori.setEnabled(false);
        initTabellaVentilatori();
        abilitaBottoni();
        this.tableVentilatori.setEnabled(true);
    }
	
    public void initTabellaPreventivi() {
        initTabellaPreventivi(false);
    }
	
    public void initTabellaPreventivi(final boolean preventivoNew) {
        if (!preventivoNew && this.l_VG.preventivoCurrent != null && this.l_VG.preventivoCurrentIndex != -1) {
            aggiornaPreventivoCorrente();
            return;
        }
        this.horLay_TabellaPreventivi.removeAllComponents();
        this.l_VG.preventivoCurrent = null;
        this.l_VG.preventivoCurrentIndex = -1;
        this.tablePreventivi.setEnabled(false);
        initContainerPreventivi();
        try {
            for (int i = 0; i < this.l_VG.ElencoPreventivi.size(); i++) {
                final Object obj[] = new Object[10];
                final Preventivo l_p = this.l_VG.ElencoPreventivi.get(i);
                final String itemID = Integer.toString(l_p.IDPreventivo);
                obj[0] = l_p.NumOfferta;
                obj[1] = l_p.IDCliente;
                obj[2] = l_p.ragioneSocialeCliente;
                obj[3] = new Date(l_p.Data.getTime());
                obj[4] = l_p.NumOrdine;
                obj[5] = l_p.CostoTotale;
                obj[6] = l_p.CostoTotale * (100. - l_p.Sconto) / 100.;
                obj[7] = l_p.Ventilatori.size();
                obj[8] = l_p.Note;
                obj[9] = l_p.IDPreventivo;
                if (!l_p.Banca.equals("Nascondi")) {
                    this.tablePreventivi.addItem(obj, itemID);
                }
            }
        } catch (final UnsupportedOperationException e) {
            logger.error("Error in creating container: " + e.getMessage());
        }
        initTabellaVentilatori();
        abilitaBottoni();
        this.tablePreventivi.setEnabled(true);
        this.tablePreventivi.setSizeFull();
        this.tablePreventivi.setColumnExpandRatio("Offerta", 1.5F);
        this.tablePreventivi.setColumnExpandRatio("Cliente", 0.4F);
        this.tablePreventivi.setColumnExpandRatio("RSoc", 2.0F);
        this.tablePreventivi.setColumnExpandRatio("Data", 1.2F);
        this.tablePreventivi.setColumnExpandRatio("Progetto", 1);
        this.tablePreventivi.setColumnExpandRatio("Price", 1);
        this.tablePreventivi.setColumnExpandRatio("PriceUser", 1);
        this.tablePreventivi.setColumnExpandRatio("N", 0.5F);
        this.tablePreventivi.setColumnExpandRatio("Note", 3);
        this.tablePreventivi.setColumnCollapsingAllowed(true);
        this.tablePreventivi.setColumnCollapsed("Codice", true);
        this.horLay_TabellaPreventivi.addComponent(this.tablePreventivi);
        this.horLay_TabellaPreventivi.setComponentAlignment(this.tablePreventivi, Alignment.MIDDLE_CENTER);
        this.horLay_TabellaPreventivi.setExpandRatio(this.tablePreventivi, 10.0F);
    }

    private void abilitaBottoni() {
        final PreventiviView preventiviView = (PreventiviView) l_VG.ElencoView[l_VG.PreventiviViewIndex];
        buttonModificaPreventivo.setEnabled(l_VG.preventivoCurrent != null);
        buttonDuplicaPreventivo.setEnabled(l_VG.preventivoCurrent != null);
        buttonSalvaPreventivo.setEnabled(l_VG.preventivoCurrent != null);
        buttonChiudiPreventivo.setEnabled(l_VG.preventivoCurrent != null);
        buttonCancellaPreventivo.setEnabled(l_VG.preventivoCurrent != null);
        buttonResetPreventivo.setEnabled(l_VG.preventivoCurrent != null);
        buttonGeneraPreventivo.setEnabled(l_VG.preventivoCurrent != null);
        buttonDownloadPreventivo.setEnabled(false);
        boolean enabled = false;
        try {
            if (this.l_VG.VentilatoreCurrent == null) {
                enabled = false;
            } else {
                enabled = !this.l_VG.utilityCliente.isAlberoNudo(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
            }
        } catch (final Exception e) {
            logger.error(e.getMessage());
        }
        buttonCambiaMotore.setEnabled(enabled);
        buttonCancellaVentilatore.setEnabled(preventiviView.ventilatoreIndex >= 0);
        buttonAzione0.setEnabled(preventiviView.ventilatoreIndex >= 0);
        buttonAzione1.setEnabled(preventiviView.ventilatoreIndex >= 0);
        buttonAzione2.setEnabled(preventiviView.ventilatoreIndex >= 0);
        buttonVentilatoreUp.setEnabled(preventiviView.ventilatoreIndex > 0 && l_VG.ElencoVentilatoriFromPreventivo.size() > 0);
        buttonVentilatoreDown.setEnabled(preventiviView.ventilatoreIndex >= 0 && preventiviView.ventilatoreIndex < (l_VG.ElencoVentilatoriFromPreventivo.size() - 1));
        preventiviView.abilitaTab();
    }
	
    private void initTabellaVentilatori() {
        double v;
        this.tableVentilatori.setEnabled(false);
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        preventiviView.ventilatoreIndex = -1;
        initContainerVentilatori();
        if (this.l_VG.preventivoCurrent == null) {
            return;
        }
        this.elencoNoteVentilatori.clear();
        for (int i = 0; i < this.l_VG.ElencoVentilatoriFromPreventivo.size(); i++) {
            final Object obj[] = new Object[15];
            final String itemID = Integer.toString(i);
            final String ID = Integer.toString(i + 1);
            final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
            final VentilatorePreventivo l_vp = this.l_VG.preventivoCurrent.Ventilatori.get(i);
            final CheckBox cb = new CheckBox();
            cb.setData(itemID);
            cb.setValue(l_v.Selezionato);
            cb.addValueChangeListener(event ->
            {
                l_v.Selezionato = cb.getValue();
                l_vp.HaPreventivo = l_v.Selezionato;
                l_v.VentilatoreCambiato = true;
                final PreventiviView preventivoTemp = (PreventiviView) PreventiviVentilatori.this.l_VG.ElencoView[PreventiviVentilatori.this.l_VG.PreventiviViewIndex];
                PreventiviVentilatori.this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();
                final PreventiviView preventiviView1 = (PreventiviView) PreventiviVentilatori.this.l_VG.ElencoView[PreventiviVentilatori.this.l_VG.PreventiviViewIndex];
                preventiviView1.aggiornaPreventivo();
                preventiviView1.aggiornaPreventivo();
                preventiviView1.abilitaTab();
            });
            obj[0] = cb;
            final TextFieldInteger tfQuantity = new TextFieldInteger();
            tfQuantity.setValue(Integer.toString(l_vp.Qta));
            tfQuantity.setData(itemID);
            tfQuantity.addValueChangeListener(event ->
            {
                l_vp.Qta = tfQuantity.getIntegerValue();
                l_v.VentilatoreCambiato = true;
                final PreventiviView preventivoTemp = (PreventiviView) PreventiviVentilatori.this.l_VG.ElencoView[PreventiviVentilatori.this.l_VG.PreventiviViewIndex];
                PreventiviVentilatori.this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();
                final PreventiviView preventiviView1 = (PreventiviView) PreventiviVentilatori.this.l_VG.ElencoView[PreventiviVentilatori.this.l_VG.PreventiviViewIndex];
                preventiviView1.aggiornaPreventivo();
            });
            tfQuantity.setSizeFull();
            obj[1] = tfQuantity;
            obj[2] = l_VG.buildModelloCompleto(l_v);
            obj[3] = ID;
            obj[4] = String.format("%s %d", l_v.selezioneCorrente.Orientamento, l_v.selezioneCorrente.OrientamentoAngolo);
            obj[5] = l_v.selezioneCorrente.Esecuzione;
            final XdevLabel lblQ = new XdevLabel(String.format("%s %s", 
                    arrotondaValore(l_v.UMPortata.nDecimaliStampa, l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, l_v.UMPortata)),
                    l_v.UMPortata.simbolo), ContentMode.HTML);
            obj[6] = lblQ;
            final XdevLabel lblPt = new XdevLabel(String.format("%s %s", 
                    arrotondaValore(l_v.UMPressione.nDecimaliStampa, l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, l_v.UMPressione)),
                    l_v.UMPressione.simbolo), ContentMode.HTML);
            lblPt.setSizeFull();
            obj[7] = lblPt;
            final XdevLabel lblRho = new XdevLabel(String.format("%s [kg/m<sup>3</sup>]", arrotondaValore(3, l_v.CAProgettazione.rho)), ContentMode.HTML);
            obj[8] = lblRho;
            this.l_VG.fmtNd.setMaximumFractionDigits(l_v.UMPotenza.nDecimaliStampa);
            obj[9] = String.format("%s %s", arrotondaValore(l_v.UMPotenza.nDecimaliStampa, l_v.selezioneCorrente.MotoreInstallato.PotkW * 1000. / l_v.UMPotenza.fattoreConversione), l_v.UMPotenza.simbolo);
            this.l_VG.fmtNd.setMaximumFractionDigits(0);
            obj[10] = arrotondaValore(0, l_v.selezioneCorrente.NumeroGiri);
            obj[11] = String.format("%d Hz", l_v.selezioneCorrente.MotoreInstallato.Freq);
            if (l_vp.Motore != null && !l_vp.Motore.equals(""))
                obj[12] = l_vp.Motore;
            else
                obj[12] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
            final XdevTextField tf = new XdevTextField(itemID);
            tf.setSizeFull();
            tf.setEnabled(true);
            tf.setValue(l_v.Note);
            tf.addValueChangeListener(event -> {
                changeNotes(l_v, tf.getValue());
                l_v.VentilatoreCambiato = true;
            });
            this.elencoNoteVentilatori.add(tf);
            obj[13] = tf;
            this.fmtNd.setMaximumFractionDigits(1);
            obj[14] = arrotondaValore(1, this.l_VG.preventivoCurrent.Ventilatori.get(i).Qta * this.l_VG.preventivoCurrent.Ventilatori.get(i).CostoVentilatore);
            this.tableVentilatori.addItem(obj, itemID);
        }
        this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
        this.tableVentilatori.setEnabled(true);
        this.tableVentilatori.setSizeFull();
        this.tableVentilatori.setColumnExpandRatio("Selected", 0.5F);
        this.tableVentilatori.setColumnExpandRatio("Qta", 0.4F);
        this.tableVentilatori.setColumnExpandRatio("Ventilatore", 1.5F);
        this.tableVentilatori.setColumnExpandRatio("Item", 0.2F);
        this.tableVentilatori.setColumnExpandRatio("Rotazione", 0.5F);
        this.tableVentilatori.setColumnExpandRatio("Esecuzione", 0.5F);
        this.tableVentilatori.setColumnExpandRatio("Q", 1F);
        this.tableVentilatori.setColumnExpandRatio("Pt", 1F);
        this.tableVentilatori.setColumnExpandRatio("Densità", 1F);
        this.tableVentilatori.setColumnExpandRatio("Potenza", 0.6F);
        this.tableVentilatori.setColumnExpandRatio("Rpm", 0.6F);
        this.tableVentilatori.setColumnExpandRatio("Frequenza", 0.5F);
        this.tableVentilatori.setColumnExpandRatio("Motore", 1F);
        this.tableVentilatori.setColumnExpandRatio("Note", 1F);
        this.tableVentilatori.setColumnExpandRatio("Prezzo", 0.6F);
    }

    private void generaPreventivo() {
        try {
            final String lingua = this.l_VG.currentCitFont.idLinguaDisplay;
            if (!this.l_VG.stampaOfferta.linguaStampa.equals("")) {
                String Lingua = this.l_VG.stampaOfferta.linguaStampa;
                switch (Lingua) {
                    case "Italiano":
                        this.l_VG.setLinguaFromId("IT");
                        break;
                    case "English":
                        this.l_VG.setLinguaFromId("UK");
                        break;
                    case "Français":
                        this.l_VG.setLinguaFromId("FR");
                        break;
                    case "Deutsche":
                        this.l_VG.setLinguaFromId("DE");
                        break;
                    case "Español":
                        this.l_VG.setLinguaFromId("ES");
                        break;
                }
            }
            final ArrayList<String> ElencoModelloCompleto = new ArrayList<>();
            for (int i = 0; i < this.l_VG.ElencoVentilatoriFromPreventivo.size(); i++) {
                ElencoModelloCompleto.add(this.l_VG.buildModelloCompleto(this.l_VG.ElencoVentilatoriFromPreventivo.get(i), this.l_VG.parametriModelloCompletoForPrint));
            }
            for (int i = 0; i < this.l_VG.ElencoVentilatoriFromPreventivo.size(); i++) {
                final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
                this.l_VG.ventilatoreFisicaCurrent = new VentilatoreFisica();
                this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
                this.l_VG.ventilatoreFisicaCurrent.setVentilatoreFisica(this.l_VG.conTecnica, "", "", l_v, this.l_VG.ventilatoreFisicaParameter);
                this.l_VG.dbTecnico.caricaDimensioniEsecuzione(l_v);
                if (!l_v.DatiCompleti) {
                    this.l_VG.completaCaricamentoVentilatore(l_v);
                }
            }
            InputStream inwb = null;
            inwb = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + this.l_VG.utilityCliente.idCliente + "_template_offerta.xlsx");
            this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
            this.l_VG.stampaOfferta.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.ventilatoreFisicaParameter, inwb);
            this.l_VG.stampaOfferta.initUtility(this.l_VG.currentCitFont, this.l_VG.utility, this.l_VG.utilityTraduzioni, this.l_VG.utilityUnitaMisura, null, null);
            final InputStream isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoofferta.png");
            final InputStream isCurve[] = new InputStream[14];
            if (this.l_VG.sfondoGraficiPrestazioni != null && this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
                for (int i = 0; i < 14; i++) {
                    isCurve[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                }
            }
            final InputStream isRumore[] = new InputStream[14];
            final InputStream isRumore1[] = new InputStream[14];
            if (this.l_VG.sfondoGraficiSpettro != null && this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag) {
                for (int i = 0; i < 14; i++) {
                    isRumore[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                }
                for (int i = 0; i < 14; i++) {
                    isRumore1[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                }
            }
            List<String> ListaValoriRPM = new ArrayList<>();
            for (Ventilatore v : l_VG.ElencoVentilatoriFromPreventivo) {
                l_VG.VentilatoreCurrent = v;
                ListaValoriRPM.add(l_VG.calcolaRpMMax());
            }
            l_VG.stampaOfferta.ListaValoriRpm = ListaValoriRPM;
            this.l_VG.stampaOfferta.initStampaApachePOI(ElencoModelloCompleto, this.l_VG.utilityCliente, this.l_VG.gestioneAccessori, this.l_VG.currentUtilizzatore, isLogo, this.l_VG.Paths, this.l_VG.assiGraficiDefault, isCurve, isRumore, isRumore1);
            this.l_VG.stampaOfferta.eseguiStampa(this.l_VG.Paths.rootResources + this.l_VG.preventivoCurrent.NumOfferta, this.ElencoFileXls, this.l_VG.currentCliente, this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo, this.l_VG.catalogoCondizioniUmidita, this.l_VG.ParametriVari);
            if (this.ElencoFileXls.size() > 0) {
                this.l_VG.dbCRMv2.storePreventivoFanStampato(this.l_VG.preventivoCurrent, this.ElencoFileXls.get(0));
                final String memo = this.l_VG.utilityTraduzioni.TraduciStringa("Generato") + " " + this.ElencoFileXls.get(0);
                this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneStampaOfferta, memo);
                this.buttonDownloadPreventivo.setEnabled(true);
                final Resource res = new FileResource(new File(this.ElencoFileXls.get(0)));
                this.memoDownload = this.ElencoFileXls.get(0);
                if (this.fileDownloader == null) {
                    this.fileDownloader = new FileDownloader(res);
                    this.fileDownloader.extend(this.buttonDownloadPreventivo);
                } else {
                    this.fileDownloader.setFileDownloadResource(res);
                }
                this.msgBox = MessageBox.createInfo();
                this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo Correttamente Generato"));
                this.msgBox.withOkButton();
                this.msgBox.open();
            } else {
                this.buttonDownloadPreventivo.setEnabled(false);
            }
            this.l_VG.setLinguaFromId(lingua);
            this.l_VG.stampaOfferta.linguaStampa = "";
        } catch (final FileNotFoundException e) {
            logger.error("Error in accessing file: " + e.getMessage());
            }
    }

    private void tablePreventivi_valueChange(final Property.ValueChangeEvent event) {
        if (!this.tablePreventivi.isEnabled()) {
            return;
        }
        try {
            if (this.l_VG.preventivoCurrent != null) {
                storePreventivo();
            }
            final Item item = this.containerPreventivi.getItem(event.getProperty().getValue().toString());
            final int codicePreventivo = (int) item.getItemProperty("Codice").getValue();
            for (int i = 0; i < this.l_VG.ElencoPreventivi.size(); i++) {
                if (this.l_VG.ElencoPreventivi.get(i).IDPreventivo == codicePreventivo) {
                    this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(i);
                    this.l_VG.preventivoCurrentIndex = i;
                    break;
                }
            }
            if (this.l_VG.preventivoCurrent.IDCliente == -1) {
                this.l_VG.currentCliente = new Clientev21();
            } else {
                this.l_VG.currentCliente = this.l_VG.dbCRMv2.setCurrentClienteFromCodice(this.l_VG.preventivoCurrent.IDCliente, this.l_VG.ElencoClienti);
            }
            this.l_VG.dbCRMv2.loadElencoVentilatoriPreventivo(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
            for (int i = 0; i < this.l_VG.ElencoVentilatoriFromPreventivo.size(); i++) {
                final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
                l_v.ERPCaricato = false;
                this.l_VG.buildDatiERP327ForSellFan(l_v);
                if (l_v.UMAltezza == null) {
                    l_v.UMAltezza = (UnitaMisura) this.l_VG.UMAltezzaCorrente.clone();
                }
                if (l_v.UMPortata == null) {
                    l_v.UMPortata = (UnitaMisura) this.l_VG.UMPortataCorrente.clone();
                }
                if (l_v.UMPotenza == null) {
                    l_v.UMPotenza = (UnitaMisura) this.l_VG.UMPotenzaCorrente.clone();
                }
                if (l_v.UMPressione == null) {
                    l_v.UMPressione = (UnitaMisura) this.l_VG.UMPressioneCorrente.clone();
                }
                if (l_v.UMTemperatura == null) {
                    l_v.UMTemperatura = (UnitaMisura) this.l_VG.UMTemperaturaCorrente.clone();
                }
            }
            initTabellaVentilatori();
            abilitaBottoni();
            final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
            preventiviView.aggiornaPreventivo();
            preventiviView.abilitaTab();
        } catch (final Exception e) {
            logger.error("Error in TablePreventivi_ValueChange: " + e.getMessage());
            this.l_VG.preventivoCurrent = null;
            this.l_VG.currentCliente = null;
            this.l_VG.ElencoVentilatoriFromPreventivo.clear();
            initTabellaVentilatori();
            this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
            abilitaBottoni();
            final PreventiviView preventiviView = ((PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex]);
            preventiviView.abilitaTab();
        }
    }

    private void tableVentilatori_valueChange(final Property.ValueChangeEvent event) {
        if (!this.tableVentilatori.isEnabled()) {
            return;
        }
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        try {
            storePreventivo();
            this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(event.getProperty().getValue().toString());
            preventiviView.ventilatoreIndex = Integer.parseInt(event.getProperty().getValue().toString());
            preventiviView.aggiornaCosto();
            abilitaBottoni();
        } catch (final Exception e) {
            logger.error("Error in TablePreventivi_ValueChange: " + e.getMessage());
            this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
            abilitaBottoni();
            preventiviView.ventilatoreIndex = -1;
            preventiviView.abilitaTab();
            //Notification.show(e.toString());
        }
    }

    private void buttonCambiaMotore_buttonClick() {
        final String warningStr = this.l_VG.utilityCliente.getWarning(this.l_VG.VentilatoreCurrent, 0);
        if (warningStr != null) {
            this.l_VG.MainView.setAccessoriVentilatoreCurrent();
            this.msgBox = MessageBox.create();
            this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(warningStr));
            this.msgBox.withOkButton(() -> {
                this.l_VG.cambiaManualmenteMotore();
                if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null) {
                    final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
                    ventilatorePreventivo.HaMotore = true;
                    ventilatorePreventivo.MotoreSelezionato = true;
                    ventilatorePreventivo.VentilatoreCambiato = true;
                    final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
                    preventiviView.aggiornaCosto();
                    aggiornaVentilatoreCorrente();
                }
            });
            this.msgBox.open();
        } else {
            this.l_VG.cambiaManualmenteMotore();
        }
    }

    private void buttonVentilatoreDown_buttonClick() {
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        if (preventiviView.ventilatoreIndex + 1 < this.l_VG.ElencoVentilatoriFromPreventivo.size()) {
            final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(preventiviView.ventilatoreIndex);
            final VentilatorePreventivo l_vp = this.l_VG.preventivoCurrent.Ventilatori.get(preventiviView.ventilatoreIndex);
            this.l_VG.ElencoVentilatoriFromPreventivo.remove(preventiviView.ventilatoreIndex);
            this.l_VG.preventivoCurrent.Ventilatori.remove(this.l_VG.VentilatoreCurrentIndex);
            this.l_VG.ElencoVentilatoriFromPreventivo.add(preventiviView.ventilatoreIndex + 1, l_v);
            this.l_VG.preventivoCurrent.Ventilatori.add(preventiviView.ventilatoreIndex + 1, l_vp);
            storePreventivo();
            initTabellaVentilatori();
            abilitaBottoni();
            preventiviView.aggiornaPreventivo();
            preventiviView.abilitaTab();
        }
    }

    private void buttonVentilatoreUp_buttonClick() {
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        if (preventiviView.ventilatoreIndex > 0) {
            final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(preventiviView.ventilatoreIndex);
            final VentilatorePreventivo l_vp = this.l_VG.preventivoCurrent.Ventilatori.get(preventiviView.ventilatoreIndex);
            this.l_VG.ElencoVentilatoriFromPreventivo.remove(preventiviView.ventilatoreIndex);
            this.l_VG.preventivoCurrent.Ventilatori.remove(this.l_VG.VentilatoreCurrentIndex);
            this.l_VG.ElencoVentilatoriFromPreventivo.add(preventiviView.ventilatoreIndex - 1, l_v);
            this.l_VG.preventivoCurrent.Ventilatori.add(preventiviView.ventilatoreIndex - 1, l_vp);
            storePreventivo();
            initTabellaVentilatori();
            abilitaBottoni();
            preventiviView.aggiornaPreventivo();
            preventiviView.abilitaTab();
        }
    }

    private void buttonGeneraPreventivo_buttonClick() {
        aggiornaPreventivo();
        int nv = (int) l_VG.preventivoCurrent.Ventilatori.stream().filter(x -> x.HaPreventivo).count();
        if (nv <= 0) {
            this.msgBox = MessageBox.createInfo();
            this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Nessun ventilatore selezionato"));
            this.msgBox.withOkButton();
            this.msgBox.open();
            return;
        }
        if (this.l_VG.stampaOfferta == null) {
            this.l_VG.stampaOfferta = new Cliente04StampaOffertaPoiXLSX();
        }
        final Cliente04pannelloPagineOfferta pannello = new Cliente04pannelloPagineOfferta();
        pannello.setVariabiliGlobali(this.l_VG);
        this.msgBox = MessageBox.create();
        this.msgBox.withMessage(pannello);
        this.msgBox.withAbortButton();
        this.msgBox.withOkButton(() -> {
            pannello.getFlag();
            final Window popup = PopupWindow.For(new ProgressView(this.l_VG.utilityTraduzioni)).closable(false).draggable(false).resizable(false).modal(true).show();
            UI.getCurrent().push();
            generaPreventivo();
            popup.close();
        });
        this.msgBox.open();
    }

    private void buttonDuplicaPreventivo_buttonClick() {
        this.l_VG.preventivoCurrent.IDPreventivo = -1;
        for (int i = 0; i < this.l_VG.preventivoCurrent.Ventilatori.size(); i++) {
            this.l_VG.preventivoCurrent.Ventilatori.get(i).IDVentilatore = -1;
            this.l_VG.ElencoVentilatoriFromPreventivo.get(i).IDVentilatore = -1;
        }
        this.l_VG.preventivoCurrent.Data = new Date();
        storePreventivo();
        final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0, this.l_VG.currentUserSaaS._ID);
        PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        preventiviView.reinitPreventivi();
        setPreventivoSelected(idPreventivo.toString());
    }
	
    public void reinitTabellaPreventivi() {
        initTabellaPreventivi(true);
        this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
    }
	
    public void newModifyPreventivo(final boolean newMode) {
        if (newMode) {
            if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneNuovaOfferta, this.l_VG.ElencoPreventivi.size())) {
                this.msgBox = MessageBox.createWarning();
                this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
                this.msgBox.withOkButton();
                this.msgBox.open();
                return;
            }
        }
        final pannelloPreventivoEditNew pannello = new pannelloPreventivoEditNew();
        pannello.setVariabiliGlobali(this.l_VG);
        pannello.newMode = newMode;
        pannello.init();
        this.msgBox = MessageBox.create();
        this.msgBox.withMessage(pannello);
        this.msgBox.withAbortButton();
        this.msgBox.withOkButton(() -> {
            if (newMode) {
                this.l_VG.preventivoCurrent = new Preventivo();
                this.l_VG.ElencoVentilatoriFromPreventivo.clear();
            }
            pannello.getValues();
            storePreventivo();
            final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
            this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0, this.l_VG.currentUserSaaS._ID);
            if (newMode) {
                this.l_VG.preventivoCurrentIndex = this.l_VG.ElencoPreventivi.size() - 1;
            }
            this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex);
            PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
            if (newMode) {
                initTabellaPreventivi(true);
                setPreventivoSelected(idPreventivo.toString());
                this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneNuovaOfferta, "ID: " + Integer.toString(idPreventivo));
            } else {
                preventiviView.aggiornaPreventivo();
            }
        });
        this.msgBox.open();
    }

    public void editPreventivo(final boolean newMode) {
        if (newMode) {
            if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneNuovaOfferta, this.l_VG.ElencoPreventivi.size())) {
                this.msgBox = MessageBox.createWarning();
                this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
                this.msgBox.withOkButton();
                this.msgBox.open();
                return;
            }
        }
        final pannelloEditPreventivo pannello = new pannelloEditPreventivo(newMode);
        pannello.setVariabiliGlobali(this.l_VG);
        pannello.init();
        this.msgBox = MessageBox.create();
        this.msgBox.withMessage(pannello);
        this.msgBox.withCancelButton();
        this.msgBox.withSaveButton(() -> {
            if (newMode) {
                this.l_VG.preventivoCurrent = new Preventivo();
                this.l_VG.ElencoVentilatoriFromPreventivo.clear();
            }
            pannello.getValues();
            storePreventivo();
            final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
            this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0, this.l_VG.currentUserSaaS._ID);
            if (newMode) {
                this.l_VG.preventivoCurrentIndex = this.l_VG.ElencoPreventivi.size() - 1;
            }
            this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex);
            final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
            if (newMode) {
                initTabellaPreventivi(true);
                setPreventivoSelected(idPreventivo.toString());
                this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneNuovaOfferta, "ID: " + Integer.toString(idPreventivo));
            } else {
                preventiviView.aggiornaPreventivo();
            }
        });
        this.msgBox.withWidth("800px");
        this.msgBox.open();
    }

    private void buttonFiltra_buttonClick() {
        if (this.l_VG.preventivoCurrent != null) {
            storePreventivo();
        }
        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0, this.l_VG.currentUserSaaS._ID);
        for (int i = this.l_VG.ElencoPreventivi.size() - 1; i >= 0; i--) {
            final Preventivo l_p = this.l_VG.ElencoPreventivi.get(i);
            if (!isFiltroPreventiviOK(l_p)) {
                this.l_VG.ElencoPreventivi.remove(i);
            }
        }
        initTabellaPreventivi(true);
        this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
    }

    private boolean isFiltroPreventiviOK(final Preventivo l_p) {
        if (!this.textFieldRagioneSociale.getValue().equals("")) {
            if (!l_p.ragioneSocialeCliente.contains(this.textFieldRagioneSociale.getValue())) {
                return false;
            }
        }
        if (!this.textFieldCodiceCliente.getValue().equals("")) {
            if (l_p.IDCliente != this.textFieldCodiceCliente.getIntegerValue()) {
                return false;
            }
        }
        final Calendar l_pData = Calendar.getInstance();
        l_pData.setTime(l_p.Data);
        Date l_d = this.textFieldDataIn.getValue();
        final Calendar l_dData = Calendar.getInstance();
        l_dData.setTime(l_d);
        l_dData.set(Calendar.HOUR, 0);
        l_dData.set(Calendar.MINUTE, 0);
        l_dData.set(Calendar.SECOND, 0);
        if (l_dData.after(l_pData)) {
            return false;
        }
        l_d = this.textFieldDataOut.getValue();
        l_dData.setTime(l_d);
        l_dData.set(Calendar.HOUR, 23);
        l_dData.set(Calendar.MINUTE, 59);
        l_dData.set(Calendar.SECOND, 59);
        if (l_dData.before(l_pData)) {
            return false;
        }
        return true;
    }

    private void buttonResetFiltri_buttonClick() {
        this.textFieldCodiceCliente.setValue("");
        this.textFieldRagioneSociale.setValue("");
        this.textFieldDataIn.setValue(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        this.textFieldDataOut.setValue(new Date());
        buttonFiltra_buttonClick();
    }

    private void buttonCancellaVentilatore_buttonClick() {
        this.msgBox = MessageBox.createQuestion();
        this.msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella"));
        this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Cancellazione Ventilatore Selezionato"));
        this.msgBox.withAbortButton();
        this.msgBox.withOkButton(() -> {
            this.l_VG.ElencoVentilatoriFromPreventivo.remove(this.l_VG.VentilatoreCurrentIndex);
            this.l_VG.preventivoCurrent.Ventilatori.remove(this.l_VG.VentilatoreCurrentIndex);
            final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
            this.l_VG.preventivoCurrent.CostoTotale = preventiviView.calcolaCostoPreventivo();
            preventiviView.aggiornaPreventivo();
            storePreventivo();
            initTabellaVentilatori();
            aggiornaPreventivoCorrente();
            abilitaBottoni();
        });
        this.msgBox.open();
    }

    private void buttonResetPreventivo_buttonClick() {
        this.l_VG.ElencoVentilatoriFromPreventivo.clear();
        this.l_VG.preventivoCurrent.Ventilatori.clear();
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        this.l_VG.preventivoCurrent.CostoTotale = preventiviView.calcolaCostoPreventivo();
        preventiviView.aggiornaPreventivo();
        storePreventivo();
        initTabellaVentilatori();
        aggiornaPreventivoCorrente();
        abilitaBottoni();
    }

    private void buttonAzione0_buttonClick() {
        final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(this.l_VG.VentilatoreCurrentIndex);
        this.l_VG.utilityCliente.eseguiPannelloPreventivoAzioneSpeciale0(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex), l_v.selezioneCorrente.MotoreInstallato.CodiceCliente);
        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
        if (ventilatorePreventivo.Motore != null) {
            ventilatorePreventivo.Motore = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
        }
        if (ventilatorePreventivo.HaMotore) {
            ventilatorePreventivo.MotoreSelezionato = true;
        }
        ventilatorePreventivo.VentilatoreCambiato = true;
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        final boolean ventilatoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).VentilatoreSelezionato;
        final boolean motoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).MotoreSelezionato;
        this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
        preventiviView.aggiornaCosto();
        initTabellaVentilatori();
        aggiornaVentilatoreCorrente();
        this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
        abilitaBottoni();
    }

    private void buttonAzione1_buttonClick() {
        this.l_VG.utilityCliente.eseguiPannelloPreventivoAzioneSpeciale1(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex), this.l_VG.pannelloPreventivoAzioniSpeciali1Desc);
        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
        ventilatorePreventivo.MotoreSelezionato = false;
        ventilatorePreventivo.VentilatoreCambiato = true;
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        final boolean ventilatoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).VentilatoreSelezionato;
        final boolean motoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).MotoreSelezionato;
        this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
        preventiviView.aggiornaCosto();
        initTabellaVentilatori();
        aggiornaVentilatoreCorrente();
        this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
        abilitaBottoni();
    }

    private void buttonAzione2_buttonClick() {
        this.l_VG.utilityCliente.eseguiPannelloPreventivoAzioneSpeciale2(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex), this.l_VG.pannelloPreventivoAzioniSpeciali2Desc);
        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
        ventilatorePreventivo.MotoreSelezionato = false;
        ventilatorePreventivo.VentilatoreCambiato = true;
        final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        final boolean ventilatoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).VentilatoreSelezionato;
        final boolean motoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).MotoreSelezionato;
        this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
        preventiviView.aggiornaCosto();
        initTabellaVentilatori();
        aggiornaVentilatoreCorrente();
        this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
        abilitaBottoni();
    }

    public void aggiornaPreventivo() {
        if (this.l_VG.preventivoCurrent != null) {
            {
                for (int i = 0; i < this.l_VG.ElencoPreventivi.size(); i++) {
                    if (this.l_VG.ElencoPreventivi.get(i).IDPreventivo == this.l_VG.preventivoCurrent.IDPreventivo) {
                        this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(i);
                        this.l_VG.preventivoCurrentIndex = i;
                        break;
                    }
                }
                if (this.l_VG.preventivoCurrent.IDCliente == -1) {
                    this.l_VG.currentCliente = new Clientev21();
                } else {
                    this.l_VG.currentCliente = this.l_VG.dbCRMv2.setCurrentClienteFromCodice(this.l_VG.preventivoCurrent.IDCliente, this.l_VG.ElencoClienti);
                }
                this.l_VG.dbCRMv2.loadElencoVentilatoriPreventivo(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
                final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
                for (int i = 0; i < this.l_VG.ElencoVentilatoriFromPreventivo.size(); i++) {
                    final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
                    l_v.ERPCaricato = false;
                    this.l_VG.buildDatiERP327ForSellFan(l_v);
                    if (l_v.UMAltezza == null) {
                        l_v.UMAltezza = (UnitaMisura) this.l_VG.UMAltezzaCorrente.clone();
                    }
                    if (l_v.UMPortata == null) {
                        l_v.UMPortata = (UnitaMisura) this.l_VG.UMPortataCorrente.clone();
                    }
                    if (l_v.UMPotenza == null) {
                        l_v.UMPotenza = (UnitaMisura) this.l_VG.UMPotenzaCorrente.clone();
                    }
                    if (l_v.UMPressione == null) {
                        l_v.UMPressione = (UnitaMisura) this.l_VG.UMPressioneCorrente.clone();
                    }
                    if (l_v.UMTemperatura == null) {
                        l_v.UMTemperatura = (UnitaMisura) this.l_VG.UMTemperaturaCorrente.clone();
                    }
                    this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(Integer.toString(i));
                    preventiviView.ventilatoreIndex = i;
                    preventiviView.aggiornaCosto();
                }
                initTabellaVentilatori();
                abilitaBottoni();

                preventiviView.aggiornaPreventivo();
                preventiviView.abilitaTab();
            }
            final PreventiviView preventiviView = (PreventiviView) this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
            for (final Object component : preventiviView.getVentilatori().containerVentilatori.getItemIds()) {
                storePreventivo();
                this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(component.toString());
                preventiviView.ventilatoreIndex = Integer.parseInt(component.toString());
                preventiviView.aggiornaCosto();
                aggiornaVentilatoreCorrente();
            }
            aggiornaCostoPreventivoCorrente();
        }
    }

    private void buttonDownloadPreventivo_buttonClick() {
        this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneDownloadOfferta, "Download Preventivo " + this.memoDownload);
        this.msgBox = MessageBox.create();
        this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Continuare con il preventivo corrente") + "?");
        this.msgBox.withYesButton();
        this.msgBox.withNoButton(() -> {
            newModifyPreventivo(true);
        });
        this.msgBox.open();
    }

    private void buttonChiudiPreventivo_buttonClick() {
        if (this.l_VG.preventivoCurrent != null) {
            storePreventivo();
            final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
            this.tablePreventivi.unselect(idPreventivo.toString());
            this.l_VG.MainView.resetMainView();
        }
    }
	
    private void buttonCancellaPreventivo_buttonClick() {
        if (this.l_VG.preventivoCurrent != null) {
            this.msgBox = MessageBox.create();
            this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Sicuro di voler cancellare il preventivo?") + "\n" + this.l_VG.utilityTraduzioni.TraduciStringa("L'operazione è irreversibile."));
            this.msgBox.withNoButton();
            this.msgBox.withYesButton(() -> {
                this.l_VG.preventivoCurrent.Banca = "Nascondi";
                storePreventivo();
                reinitTabellaPreventivi();
                this.l_VG.preventivoCurrent = null;
            });
            this.msgBox.open();
        }
    }
    
    private String arrotondaValore(int cifre, double valore) {
        BigDecimal number = new BigDecimal(valore);
        return number.setScale(cifre, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString().replace(".", ",");
    }

    private void initUI() {
        this.verticalLayout = new XdevVerticalLayout();
        this.panel_Filtri = new XdevPanel();
        this.verLay_Filtri = new XdevVerticalLayout();
        this.horLay_FiltriPreventivi = new XdevHorizontalLayout();
        this.labelCodiceCliente2 = new XdevLabel();
        this.textFieldCodiceClienteGosth2 = new XdevTextField();
        this.labelDataCompresa2 = new XdevLabel();
        this.labelTra2 = new XdevLabel();
        this.spacerLowerMenu = new XdevLabel();
        this.spacerFinal = new XdevLabel();
        this.spacerFilters = new XdevLabel();
        this.textFieldDataInGosth2 = new XdevTextField();
        this.labele2 = new XdevLabel();
        this.textFieldDataOutGosth2 = new XdevTextField();
        this.horizontalLayout6 = new XdevHorizontalLayout();
        this.labelRagioneSociale = new XdevLabel();
        this.textFieldRagioneSociale = new XdevTextField();
        this.horLay_BottoniFiltriPreventivi = new XdevHorizontalLayout();
        this.buttonResetFiltri = new XdevButton();
        this.buttonFiltra = new XdevButton();
        this.panel_TabellaPreventivi = new XdevPanel();
        this.horLay_TabellaPreventivi = new XdevHorizontalLayout();
        this.tablePreventivi = new XdevTable<>();
        this.panel3 = new XdevPanel();
        this.horLay_BottoniControllo = new XdevHorizontalLayout();
        this.buttonModificaPreventivo = new XdevButton();
        this.buttonDuplicaPreventivo = new XdevButton();
        this.buttonChiudiPreventivo = new XdevButton();
        this.buttonCancellaPreventivo = new XdevButton();
        this.buttonSalvaPreventivo = new XdevButton();
        this.buttonNuovoPreventivo = new XdevButton();
        this.panel4 = new XdevPanel();
        this.horizontalLayout = new XdevHorizontalLayout();
        this.verLay_ventilatoreUpDown = new XdevVerticalLayout();
        this.buttonVentilatoreUp = new XdevButton();
        this.buttonVentilatoreDown = new XdevButton();
        this.tableVentilatori = new XdevTable<>();
        this.panel5 = new XdevPanel();
        this.horizontalLayout4 = new XdevHorizontalLayout();
        this.buttonResetPreventivo = new XdevButton();
        this.buttonCancellaVentilatore = new XdevButton();
        this.buttonAzione0 = new XdevButton();
        this.buttonAzione1 = new XdevButton();
        this.buttonAzione2 = new XdevButton();
        this.panel7 = new XdevPanel();
        this.horizontalLayout8 = new XdevHorizontalLayout();
        this.buttonCambiaMotore = new XdevButton();
        this.buttonGeneraPreventivo = new XdevButton();
        this.buttonDownloadPreventivo = new XdevButton();
        this.verticalLayout.setSpacing(false);
        this.verticalLayout.setMargin(new MarginInfo(false));
        this.verLay_Filtri.setSpacing(false);
        this.verLay_Filtri.setMargin(new MarginInfo(false));
        this.labelCodiceCliente2.setValue("Codice Cliente");
        this.labelCodiceCliente2.setContentMode(ContentMode.HTML);
        this.textFieldCodiceClienteGosth2.setColumns(5);
        this.labelDataCompresa2.setValue("Data Compresa");
        this.labelDataCompresa2.setContentMode(ContentMode.HTML);
        this.labelTra2.setValue("tra");
        this.labelTra2.setContentMode(ContentMode.HTML);
        this.textFieldDataInGosth2.setColumns(5);
        this.labele2.setValue("e");
        this.labele2.setContentMode(ContentMode.HTML);
        this.spacerLowerMenu.setValue("");
        this.spacerFinal.setValue("");
        this.spacerFilters.setValue("");
        this.textFieldDataOutGosth2.setColumns(5);
        this.horizontalLayout6.setMargin(new MarginInfo(false, true, false, true));
        this.buttonResetFiltri.setCaption("Reset Filtri");
        this.buttonResetFiltri.setStyleName("small");
        this.buttonFiltra.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/funnel.png"));
        this.buttonFiltra.setCaption("Filtra Preventivi");
        this.buttonFiltra.setStyleName("small");
        this.horLay_TabellaPreventivi.setMargin(new MarginInfo(false, true, false, true));
        this.tablePreventivi.setStyleName("small mystriped");
        this.buttonModificaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Edit.png"));
        this.buttonModificaPreventivo.setCaption("Rinomina Preventivo");
        this.buttonDuplicaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/copy.png"));
        this.buttonDuplicaPreventivo.setCaption("Duplica Preventivo");
        this.buttonCancellaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Delete16.png"));
        this.buttonCancellaPreventivo.setCaption("Cancella Preventivo");
        this.buttonChiudiPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/chest16.png"));
        this.buttonChiudiPreventivo.setCaption("Chiudi Preventivo");
        this.buttonChiudiPreventivo.setStyleName("small");
        this.buttonSalvaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/floppy_disk.png"));
        this.buttonSalvaPreventivo.setCaption("Salva e Aggiorna Preventivo");
        this.buttonNuovoPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/newspaper_add.png"));
        this.buttonNuovoPreventivo.setCaption("Nuovo Preventivo");
        this.buttonNuovoPreventivo.setStyleName("giallo");
        this.horizontalLayout.setMargin(new MarginInfo(false, true, false, false));
        this.verLay_ventilatoreUpDown.setMargin(new MarginInfo(false, true, false, true));
        this.buttonVentilatoreUp.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/nav_up_blue.png"));
        this.buttonVentilatoreUp.setCaption("");
        this.buttonVentilatoreUp.setStyleName("small");
        this.buttonVentilatoreDown.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/nav_down_blue.png"));
        this.buttonVentilatoreDown.setCaption("");
        this.buttonVentilatoreDown.setStyleName("small");
        this.tableVentilatori.setStyleName("small mystriped");
        this.buttonResetPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/replace2.png"));
        this.buttonResetPreventivo.setCaption("Reset Preventivo");
        this.buttonResetPreventivo.setStyleName("small");
        this.buttonCancellaVentilatore.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Delete16.png"));
        this.buttonCancellaVentilatore.setCaption("Cancella");
        this.buttonCancellaVentilatore.setStyleName("small");
        this.buttonAzione0.setCaption("Azione0");
        this.buttonAzione0.setStyleName("small");
        this.buttonAzione1.setCaption("Azione1");
        this.buttonAzione1.setStyleName("small");
        this.buttonAzione2.setCaption("Azione2");
        this.buttonAzione2.setStyleName("small");
        this.panel7.setTabIndex(0);
        this.buttonCambiaMotore.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/documents_exchange.png"));
        this.buttonCambiaMotore.setCaption("Cambia Motore");
        this.buttonCambiaMotore.setStyleName("big");
        this.buttonGeneraPreventivo.setCaption("Genera Preventivo");
        this.buttonGeneraPreventivo.setStyleName("big azzurro");
        this.buttonDownloadPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Download.png"));
        this.buttonDownloadPreventivo.setCaption("Download Preventivo");
        this.buttonDownloadPreventivo.setStyleName("big");
        this.textFieldCodiceCliente.setSizeFull();
        this.textFieldCodiceCliente.setCaption("Codice Cliente");
        this.textFieldRagioneSociale.setSizeFull();
        this.textFieldRagioneSociale.setCaption("Ragione Sociale");
        this.textFieldDataIn.setDateFormat("dd/MM/yyyy");
        this.textFieldDataOut.setDateFormat("dd/MM/yyyy");
        this.textFieldDataIn.setSizeFull();
        this.textFieldDataIn.setCaption("Ricerca dal");
        this.textFieldDataOut.setSizeFull();
        this.textFieldDataOut.setCaption("al");
        this.textFieldDataIn.setValue(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        this.textFieldDataOut.setValue(new Date());
        this.spacerFilters.setSizeFull();
        this.buttonFiltra.setSizeUndefined();
        this.buttonResetFiltri.setSizeUndefined();
        this.horLay_FiltriPreventivi.addComponent(this.textFieldCodiceCliente, Alignment.MIDDLE_LEFT);
        this.horLay_FiltriPreventivi.addComponent(this.textFieldRagioneSociale, Alignment.MIDDLE_LEFT);
        this.horLay_FiltriPreventivi.addComponent(this.textFieldDataIn, Alignment.MIDDLE_LEFT);
        this.horLay_FiltriPreventivi.addComponent(this.textFieldDataOut, Alignment.MIDDLE_LEFT);
        this.horLay_FiltriPreventivi.addComponent(this.spacerFilters, Alignment.MIDDLE_LEFT);
        this.horLay_FiltriPreventivi.setWidth(100, Unit.PERCENTAGE);
        this.horLay_FiltriPreventivi.setHeight(-1, Unit.PIXELS);
        this.horLay_FiltriPreventivi.setExpandRatios(1.2F, 1.2F, 1.0F, 1.0F, 3.6F);
        final CustomComponent horizontalLayout7_spacer = new CustomComponent();
        horizontalLayout7_spacer.setSizeFull();
        final CustomComponent verticalLayout3_spacer = new CustomComponent();
        verticalLayout3_spacer.setSizeFull();
        this.horLay_BottoniFiltriPreventivi.addComponent(this.buttonFiltra);
        this.horLay_BottoniFiltriPreventivi.addComponent(this.buttonResetFiltri);
        this.horLay_BottoniFiltriPreventivi.addComponent(horizontalLayout7_spacer);
        this.horLay_BottoniFiltriPreventivi.setExpandRatio(horizontalLayout7_spacer, 1.0F);
        this.horLay_BottoniFiltriPreventivi.setMargin(new MarginInfo(false, true, true, true));
        this.horLay_BottoniFiltriPreventivi.setWidth(100, Unit.PERCENTAGE);
        this.horLay_BottoniFiltriPreventivi.setHeight(-1, Unit.PIXELS);
        this.verLay_Filtri.addComponent(this.horLay_FiltriPreventivi);
        this.verLay_Filtri.addComponent(this.horLay_BottoniFiltriPreventivi);
        this.verLay_Filtri.addComponent(verticalLayout3_spacer);
        this.verLay_Filtri.setExpandRatio(verticalLayout3_spacer, 1.0F);
        this.verLay_Filtri.setSizeFull();
        this.panel_Filtri.setContent(this.verLay_Filtri);
        this.tablePreventivi.setSizeFull();
        this.horLay_TabellaPreventivi.addComponent(this.tablePreventivi, Alignment.MIDDLE_CENTER);
//        this.horizontalLayout2.setExpandRatio(this.tablePreventivi, 10.0F);
        this.horLay_TabellaPreventivi.setSizeFull();
        this.panel_TabellaPreventivi.setContent(this.horLay_TabellaPreventivi);
        this.buttonNuovoPreventivo.setSizeFull();
        this.buttonModificaPreventivo.setSizeFull();
        this.buttonDuplicaPreventivo.setSizeFull();
        this.buttonSalvaPreventivo.setSizeFull();
        this.buttonCancellaPreventivo.setSizeFull();
        this.horLay_BottoniControllo.addComponent(this.buttonNuovoPreventivo, Alignment.MIDDLE_LEFT);
        this.horLay_BottoniControllo.addComponent(this.buttonModificaPreventivo, Alignment.MIDDLE_LEFT);
        this.horLay_BottoniControllo.addComponent(this.buttonCancellaPreventivo, Alignment.MIDDLE_LEFT);
        this.horLay_BottoniControllo.addComponent(this.buttonDuplicaPreventivo, Alignment.MIDDLE_LEFT);
        this.horLay_BottoniControllo.addComponent(this.buttonSalvaPreventivo, Alignment.MIDDLE_LEFT);
        this.horLay_BottoniControllo.setExpandRatios(10F, 10F, 10F, 10F, 10F);
        this.horLay_BottoniControllo.setSizeFull();
        this.panel3.setContent(this.horLay_BottoniControllo);
        this.buttonVentilatoreUp.setWidth(64, Unit.PIXELS);
        this.buttonVentilatoreUp.setHeight(-1, Unit.PIXELS);
        this.buttonVentilatoreDown.setWidth(64, Unit.PIXELS);
        this.buttonVentilatoreDown.setHeight(-1, Unit.PIXELS);
        this.verLay_ventilatoreUpDown.addComponent(this.buttonVentilatoreUp);
        this.verLay_ventilatoreUpDown.addComponent(this.buttonVentilatoreDown, Alignment.BOTTOM_LEFT);
        this.verLay_ventilatoreUpDown.setExpandRatios(10F, 10F);
        this.verLay_ventilatoreUpDown.setSizeUndefined();
        this.horizontalLayout.addComponent(this.verLay_ventilatoreUpDown);
        this.horizontalLayout.setComponentAlignment(this.verLay_ventilatoreUpDown, Alignment.MIDDLE_CENTER);
        this.tableVentilatori.setSizeFull();
        this.horizontalLayout.addComponent(this.tableVentilatori);
        this.horizontalLayout.setComponentAlignment(this.tableVentilatori, Alignment.MIDDLE_CENTER);
        this.horizontalLayout.setExpandRatio(this.tableVentilatori, 10.0F);
        this.horizontalLayout.setSizeFull();
        this.panel4.setContent(this.horizontalLayout);
        this.buttonResetPreventivo.setSizeFull();
        this.horizontalLayout4.addComponent(this.buttonResetPreventivo);
        this.horizontalLayout4.setComponentAlignment(this.buttonResetPreventivo, Alignment.MIDDLE_LEFT);
        this.horizontalLayout4.setExpandRatio(this.buttonResetPreventivo, 20.0F);
        this.buttonCancellaVentilatore.setSizeFull();
        this.horizontalLayout4.addComponent(this.buttonCancellaVentilatore);
        this.horizontalLayout4.setComponentAlignment(this.buttonCancellaVentilatore, Alignment.MIDDLE_LEFT);
        this.horizontalLayout4.setExpandRatio(this.buttonCancellaVentilatore, 20.0F);
        this.spacerLowerMenu.setSizeFull();
        this.horizontalLayout4.addComponent(this.spacerLowerMenu);
        this.horizontalLayout4.setComponentAlignment(this.spacerLowerMenu, Alignment.MIDDLE_LEFT);
        this.horizontalLayout4.setExpandRatio(this.spacerLowerMenu, 25.0F);
        this.buttonAzione0.setSizeFull();
        this.horizontalLayout4.addComponent(this.buttonAzione0);
        this.horizontalLayout4.setComponentAlignment(this.buttonAzione0, Alignment.MIDDLE_RIGHT);
        this.horizontalLayout4.setExpandRatio(this.buttonAzione0, 10.0F);
        this.buttonAzione1.setSizeFull();
        this.horizontalLayout4.addComponent(this.buttonAzione1);
        this.horizontalLayout4.setComponentAlignment(this.buttonAzione1, Alignment.MIDDLE_RIGHT);
        this.horizontalLayout4.setExpandRatio(this.buttonAzione1, 10.0F);
        this.buttonAzione2.setSizeFull();
        this.horizontalLayout4.addComponent(this.buttonAzione2);
        this.horizontalLayout4.setComponentAlignment(this.buttonAzione2, Alignment.MIDDLE_RIGHT);
        this.horizontalLayout4.setExpandRatio(this.buttonAzione2, 10.0F);
        this.horizontalLayout4.setSizeFull();
        this.panel5.setContent(this.horizontalLayout4);
        this.horizontalLayout4.setSpacing(true);
        this.buttonCambiaMotore.setSizeFull();
        this.horizontalLayout8.addComponent(this.buttonCambiaMotore);
        this.horizontalLayout8.setComponentAlignment(this.buttonCambiaMotore, Alignment.MIDDLE_LEFT);
        this.horizontalLayout8.setExpandRatio(this.buttonCambiaMotore, 6.0F);
        this.spacerFinal.setSizeFull();
        this.horizontalLayout8.addComponent(this.spacerFinal);
        this.horizontalLayout8.setComponentAlignment(this.spacerFinal, Alignment.MIDDLE_LEFT);
        this.horizontalLayout8.setExpandRatio(this.spacerFinal, 25.0F);
        this.buttonGeneraPreventivo.setSizeFull();
        this.horizontalLayout8.addComponent(this.buttonGeneraPreventivo);
        this.horizontalLayout8.setComponentAlignment(this.buttonGeneraPreventivo, Alignment.MIDDLE_RIGHT);
        this.horizontalLayout8.setExpandRatio(this.buttonGeneraPreventivo, 6.0F);
        this.buttonDownloadPreventivo.setSizeFull();
        this.horizontalLayout8.addComponent(this.buttonDownloadPreventivo);
        this.horizontalLayout8.setComponentAlignment(this.buttonDownloadPreventivo, Alignment.MIDDLE_RIGHT);
        this.horizontalLayout8.setExpandRatio(this.buttonDownloadPreventivo, 6.0F);
        this.horizontalLayout8.setSizeFull();
        this.panel7.setContent(this.horizontalLayout8);
        this.panel_Filtri.setWidth(100, Unit.PERCENTAGE);
        this.panel_Filtri.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.panel_Filtri);
        this.panel_TabellaPreventivi.setSizeFull();
        this.verticalLayout.addComponent(this.panel_TabellaPreventivi);
        this.verticalLayout.setExpandRatio(this.panel_TabellaPreventivi, 10.0F);
        this.panel3.setWidth(100, Unit.PERCENTAGE);
        this.panel3.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.panel3);
        this.panel4.setSizeFull();
        this.verticalLayout.addComponent(this.panel4);
        this.verticalLayout.setExpandRatio(this.panel4, 10.0F);
        this.panel5.setWidth(100, Unit.PERCENTAGE);
        this.panel5.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.panel5);
        this.panel7.setWidth(100, Unit.PERCENTAGE);
        this.panel7.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.panel7);
        this.verticalLayout.setSizeFull();
        this.setContent(this.verticalLayout);
        this.setSizeFull();
        this.buttonResetFiltri.addClickListener(event -> this.buttonResetFiltri_buttonClick());
        this.buttonFiltra.addClickListener(event -> this.buttonFiltra_buttonClick());
        this.tablePreventivi.addValueChangeListener(event -> this.tablePreventivi_valueChange(event));
        this.buttonModificaPreventivo.addClickListener(event -> editPreventivo(false));
        this.buttonDuplicaPreventivo.addClickListener(event -> this.buttonDuplicaPreventivo_buttonClick());
        this.buttonChiudiPreventivo.addClickListener(event -> this.buttonChiudiPreventivo_buttonClick());
        this.buttonSalvaPreventivo.addClickListener(event -> aggiornaPreventivo());
        this.buttonCancellaPreventivo.addClickListener(event -> this.buttonCancellaPreventivo_buttonClick());
        this.buttonNuovoPreventivo.addClickListener(event -> editPreventivo(true));
        this.buttonVentilatoreUp.addClickListener(event -> this.buttonVentilatoreUp_buttonClick());
        this.buttonVentilatoreDown.addClickListener(event -> this.buttonVentilatoreDown_buttonClick());
        this.tableVentilatori.addValueChangeListener(event -> this.tableVentilatori_valueChange(event));
        this.buttonResetPreventivo.addClickListener(event -> this.buttonResetPreventivo_buttonClick());
        this.buttonCancellaVentilatore.addClickListener(event -> this.buttonCancellaVentilatore_buttonClick());
        this.buttonAzione0.addClickListener(event -> this.buttonAzione0_buttonClick());
        this.buttonAzione1.addClickListener(event -> this.buttonAzione1_buttonClick());
        this.buttonAzione2.addClickListener(event -> this.buttonAzione2_buttonClick());
        this.buttonCambiaMotore.addClickListener(event -> this.buttonCambiaMotore_buttonClick());
        this.buttonGeneraPreventivo.addClickListener(event -> this.buttonGeneraPreventivo_buttonClick());
        this.buttonDownloadPreventivo.addClickListener(event -> this.buttonDownloadPreventivo_buttonClick());
	} // </generated-code>

	private XdevLabel labelCodiceCliente2, labelDataCompresa2, labelTra2, labele2, labelRagioneSociale, spacerLowerMenu, spacerFinal, spacerFilters;
	private XdevButton buttonResetFiltri, buttonFiltra, buttonModificaPreventivo, buttonDuplicaPreventivo,
			buttonChiudiPreventivo, buttonCancellaPreventivo, buttonSalvaPreventivo, buttonNuovoPreventivo, buttonVentilatoreUp,
			buttonVentilatoreDown, buttonResetPreventivo, buttonCancellaVentilatore, buttonAzione0, buttonAzione1,
			buttonAzione2, buttonCambiaMotore, buttonGeneraPreventivo, buttonDownloadPreventivo;
	private XdevHorizontalLayout horLay_FiltriPreventivi, horizontalLayout6, horLay_BottoniFiltriPreventivi, horLay_TabellaPreventivi,
			horLay_BottoniControllo, horizontalLayout, horizontalLayout4, horizontalLayout8;
	private XdevTable<CustomComponent> tablePreventivi, tableVentilatori;
	private XdevPanel panel_Filtri, panel_TabellaPreventivi, panel3, panel4, panel5, panel7;
	private XdevTextField textFieldCodiceClienteGosth2, textFieldDataInGosth2, textFieldDataOutGosth2,
			textFieldRagioneSociale;
	private XdevVerticalLayout verticalLayout, verLay_Filtri, verLay_ventilatoreUpDown;
	
}