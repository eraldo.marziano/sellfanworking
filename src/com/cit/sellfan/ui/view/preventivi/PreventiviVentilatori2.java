package com.cit.sellfan.ui.view.preventivi;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.pannelli.pannelloEditPreventivo;
import com.cit.sellfan.ui.pannelli.pannelloPreventivoEditNew;
import com.cit.sellfan.ui.view.PreventiviView;
import com.cit.sellfan.ui.view.ProgressView;
import com.cit.sellfan.ui.view.cliente04.Cliente04pannelloPagineOfferta;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.classi.Clientev21;
import cit.myjavalib.UtilityFisica.UnitaMisura;
import cit.sellfan.Costanti;
import cit.sellfan.classi.Preventivo;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.classi.ventilatore.VentilatorePreventivo;
import cit.sellfan.personalizzazioni.cliente04.Cliente04StampaOffertaPoiXLSX;
import de.steinwedel.messagebox.MessageBox;

public class PreventiviVentilatori2 extends XdevView {
	private VariabiliGlobali l_VG;
	private final TextFieldInteger textFieldCodiceCliente = new TextFieldInteger();
	private final DateField textFieldDataIn = new DateField();
	private final DateField textFieldDataOut = new DateField();
	private Container containerPreventivi;
	private Container containerVentilatori;
    private final ArrayList<String> ElencoFileXls = new ArrayList<>();
//	private PreventiviView preventiviView;
	private FileDownloader fileDownloader;
	private final ArrayList<XdevTextField> elencoNoteVentilatori = new ArrayList<>();
	private MessageBox msgBox;
	private String memoDownload;
	
	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	public PreventiviVentilatori2() {
		super();
		this.initUI();
//		this.textFieldDataIn.setDateFormat("dd/MM/yyyy");
//		this.textFieldDataOut.setDateFormat("dd/MM/yyyy");
////		this.textFieldCodiceCliente.setSizeUndefined();
////		this.textFieldCodiceCliente.setCaption("Codice Cliente");
////		this.horizontalLayout9.replaceComponent(this.textFieldCodiceClienteGosth2, this.textFieldCodiceCliente);
////		this.horizontalLayout9.setComponentAlignment(this.textFieldCodiceCliente, Alignment.MIDDLE_LEFT);
//		this.textFieldDataIn.setSizeUndefined();
//		this.textFieldDataIn.setCaption("Ricerca dal");
//		this.horizontalLayout9.replaceComponent(this.textFieldDataInGosth2, this.textFieldDataIn);
//		this.textFieldDataOut.setSizeUndefined();
//		this.textFieldDataOut.setCaption("al");
//		this.horizontalLayout9.setComponentAlignment(this.textFieldDataIn, Alignment.MIDDLE_LEFT);
//		this.horizontalLayout9.replaceComponent(this.textFieldDataOutGosth2, this.textFieldDataOut);
//		this.horizontalLayout9.setComponentAlignment(this.textFieldDataOut, Alignment.MIDDLE_LEFT);
//		this.textFieldDataIn.setValue(new Date("1/1/2017"));
//		this.textFieldDataOut.setValue(new Date());
		initContainerPreventivi();

		initContainerVentilatori();
		this.buttonCambiaMotore.setEnabled(false);
		this.buttonAzione0.setEnabled(this.buttonCambiaMotore.isEnabled());
		this.buttonAzione1.setEnabled(this.buttonCambiaMotore.isEnabled());
		this.buttonAzione2.setEnabled(this.buttonCambiaMotore.isEnabled());
	}
	
	public void setPreventivoSelected(final String idPreventivo) {
		this.tablePreventivi.select(idPreventivo);
	}
	
	private void initContainerPreventivi() {
		this.containerPreventivi = new IndexedContainer();
		this.containerPreventivi.addContainerProperty("Offerta", String.class, "");
		this.containerPreventivi.addContainerProperty("Cliente", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("RSoc", String.class, "");
		this.containerPreventivi.addContainerProperty("Data", Date.class, null);
		this.containerPreventivi.addContainerProperty("Progetto", String.class, "");
		this.containerPreventivi.addContainerProperty("Price", Double.class, "");
		this.containerPreventivi.addContainerProperty("PriceUser", Double.class, "");
		this.containerPreventivi.addContainerProperty("N", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("Note", String.class, "");
		this.containerPreventivi.addContainerProperty("Codice", Integer.class, 0);
		this.tablePreventivi.setContainerDataSource(this.containerPreventivi);
	}
	
	private void initContainerVentilatori() {
		this.containerVentilatori = new IndexedContainer();
		this.containerVentilatori.addContainerProperty("Selected", CheckBox.class, false);
		this.containerVentilatori.addContainerProperty("Qta", TextFieldInteger.class, 0);
		this.containerVentilatori.addContainerProperty("Ventilatore", String.class, "");
		this.containerVentilatori.addContainerProperty("Item", String.class, "");
		this.containerVentilatori.addContainerProperty("Rotazione", String.class, "");
		this.containerVentilatori.addContainerProperty("Esecuzione", String.class, "");
		this.containerVentilatori.addContainerProperty("Q", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Pt", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Densità", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Potenza", XdevLabel.class, "");
		this.containerVentilatori.addContainerProperty("Rpm", String.class, "");
		this.containerVentilatori.addContainerProperty("Frequenza", String.class, "");
		this.containerVentilatori.addContainerProperty("Motore", String.class, "");
		this.containerVentilatori.addContainerProperty("Note", XdevTextField.class, "");
		this.containerVentilatori.addContainerProperty("Note2", XdevTextField.class, "");
		this.containerVentilatori.addContainerProperty("Prezzo", String.class, "");
		this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
	}

	private void setConverterForTable() {
		final myStringToDoubleConverter converter = new myStringToDoubleConverter();
		this.tablePreventivi.setConverter("Price", converter);
		this.tablePreventivi.setConverter("PriceUser", converter);
		this.tablePreventivi.setConverter("N", converter);
	}
	
	private class myStringToDoubleConverter extends StringToDoubleConverter {
		public myStringToDoubleConverter() {
			super();
		}
		
		@Override
		public java.text.NumberFormat getFormat(final Locale locale) {
			return PreventiviVentilatori2.this.l_VG.fmtNdTable;
		}
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
        if (this.l_VG.pannelloPreventivoAzioniSpeciali0Desc == null) {
			this.buttonAzione0.setVisible(false);
		}
        if (this.l_VG.pannelloPreventivoAzioniSpeciali1Desc == null) {
			this.buttonAzione1.setVisible(false);
		}
        if (this.l_VG.pannelloPreventivoAzioniSpeciali2Desc == null) {
			this.buttonAzione2.setVisible(false);
		}
        setConverterForTable();
	}
	
	public void Nazionalizza() {
		this.textFieldCodiceCliente.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Codice Cliente"));
		this.textFieldRagioneSociale.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale"));
		this.textFieldDataIn.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ricerca dal"));
		this.textFieldDataOut.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("al"));
		this.buttonResetFiltri.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset Filtri"));
		this.buttonFiltra.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Filtra Preventivi"));
		this.buttonModificaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Modifica dettagli Preventivo"));
		this.buttonDuplicaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Duplica Preventivo"));
		this.buttonSalvaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Salva e Aggiorna Preventivo"));
		this.buttonChiudiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Chiudi Preventivo"));
		this.buttonCancellaPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella Preventivo"));
		this.buttonNuovoPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Nuovo Preventivo"));
		
		this.buttonResetPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset Preventivo"));
		this.buttonCancellaVentilatore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Rimuovi Ventilatore"));
		this.buttonAzione0.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Azione0"));
		this.buttonAzione1.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Azione1"));
		this.buttonAzione2.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Azione2"));
		
		this.buttonCambiaMotore.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cambia Motore"));
		this.buttonGeneraPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Genera Preventivo"));
		this.buttonDownloadPreventivo.setCaption("Download " + this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo"));

		this.tablePreventivi.setColumnHeader("Codice", "");
		this.tablePreventivi.setColumnHeader("Data", this.l_VG.utilityTraduzioni.TraduciStringa("Data"));
		this.tablePreventivi.setColumnHeader("Cliente", this.l_VG.utilityTraduzioni.TraduciStringa("Cliente"));
		this.tablePreventivi.setColumnHeader("RSoc", this.l_VG.utilityTraduzioni.TraduciStringa("Ragione Sociale"));
		this.tablePreventivi.setColumnHeader("Offerta", this.l_VG.utilityTraduzioni.TraduciStringa("Offerta"));
		this.tablePreventivi.setColumnHeader("Progetto", this.l_VG.utilityTraduzioni.TraduciStringa("Progetto"));
		this.tablePreventivi.setColumnHeader("Price", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo Totale") + " " + Costanti.EURO);
		this.tablePreventivi.setColumnHeader("PriceUser", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo di vendita") + " " + Costanti.EURO);
		this.tablePreventivi.setColumnHeader("N", this.l_VG.utilityTraduzioni.TraduciStringa("N. Vent."));
		this.tablePreventivi.setColumnHeader("Note", this.l_VG.utilityTraduzioni.TraduciStringa("Note"));
		//tablePreventivi.setColumnWidth("Codice", 0);
		
		this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
		
		
		this.tableVentilatori.setColumnHeader("Selected", this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"));
		this.tableVentilatori.setColumnHeader("Qta", this.l_VG.utilityTraduzioni.TraduciStringa("Qta"));
		this.tableVentilatori.setColumnHeader("Ventilatore", this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatore"));
		this.tableVentilatori.setColumnHeader("Rotazione", this.l_VG.utilityTraduzioni.TraduciStringa("Rotazione"));
		this.tableVentilatori.setColumnHeader("Esecuzione", this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
		this.tableVentilatori.setColumnHeader("Q", StringheUNI.PORTATA);
		this.tableVentilatori.setColumnHeader("Pt", StringheUNI.PRESSIONETOTALE);
		this.tableVentilatori.setColumnHeader("Densità", this.l_VG.utilityTraduzioni.TraduciStringa("Densità"));
		this.tableVentilatori.setColumnHeader("Potenza", this.l_VG.utilityTraduzioni.TraduciStringa("Potenza"));
		this.tableVentilatori.setColumnHeader("Rpm", this.l_VG.utilityTraduzioni.TraduciStringa("Rpm"));
		this.tableVentilatori.setColumnHeader("Frequenza", this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza"));
		this.tableVentilatori.setColumnHeader("Motore", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.tableVentilatori.setColumnHeader("Note", this.l_VG.utilityTraduzioni.TraduciStringa("Note"));
		this.tableVentilatori.setColumnHeader("Note2", this.l_VG.utilityTraduzioni.TraduciStringa("Note2"));
		this.tableVentilatori.setColumnHeader("Prezzo", this.l_VG.utilityTraduzioni.TraduciStringa("Prezzo"));

        if (this.l_VG.pannelloPreventivoAzioniSpeciali0Desc != null) {
			this.buttonAzione0.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.pannelloPreventivoAzioniSpeciali0Desc));
		}
        if (this.l_VG.pannelloPreventivoAzioniSpeciali1Desc != null) {
			this.buttonAzione1.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.pannelloPreventivoAzioniSpeciali1Desc));
		}
        if (this.l_VG.pannelloPreventivoAzioniSpeciali2Desc != null) {
			this.buttonAzione2.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.pannelloPreventivoAzioniSpeciali2Desc));
		}
	}
	
	public void storePreventivo() {
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
		aggiornaCostoPreventivoCorrente();
	}
	
	@SuppressWarnings("unchecked")
	public void aggiornaVentilatoreCorrente() {
		//Notification.show("aggiornaVentilatoreCorrente");
		final Collection<?> Ventilatoriids = this.containerVentilatori.getItemIds();
		int i = 0;
		this.tableVentilatori.setEnabled(false);
		for(final Object id : Ventilatoriids) {
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i++);
			final Item item = this.containerVentilatori.getItem(id);
			item.getItemProperty("Ventilatore").setValue(this.l_VG.buildModelloCompleto(l_v));
			item.getItemProperty("Motore").setValue(l_v.selezioneCorrente.MotoreInstallato.CodiceCliente);
			double v = this.l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, l_v.UMPortata);
			this.l_VG.fmtNd.setMaximumFractionDigits(l_v.UMPortata.nDecimaliStampa);
			XdevLabel lb = (XdevLabel)item.getItemProperty("Q").getValue();
			lb.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPortata.simbolo);
			v = this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, l_v.UMPressione);
			lb = (XdevLabel)item.getItemProperty("Pt").getValue();
			lb.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPressione.simbolo);
			try {
				this.elencoNoteVentilatori.get(this.l_VG.VentilatoreCurrentIndex).setValue(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).Note);
			} catch (final Exception e) {
				
			}
		}
		storePreventivo();
		this.tableVentilatori.setEnabled(true);
	}
	
	@SuppressWarnings("unchecked")
	public void aggiornaRigaPreventivoCorrente() {
		if (this.l_VG.preventivoCurrent == null) {
			return;
		}
//		this.tablePreventivi.setEnabled(false);
		final Item item = this.containerPreventivi.getItem(Integer.toString(this.l_VG.preventivoCurrent.IDPreventivo));
		if (item == null) {
			this.l_VG.MainView.setMemo(this.l_VG.utilityTraduzioni.TraduciStringa("Nessuna offerta selezionata - "));
			return;
		}
		this.l_VG.MainView.setMemo(this.l_VG.utilityTraduzioni.TraduciStringa("Offerta selezionata:") + " " + this.l_VG.preventivoCurrent.NumOfferta + " - ");
		item.getItemProperty("Offerta").setValue(this.l_VG.preventivoCurrent.NumOfferta);
		item.getItemProperty("Progetto").setValue(this.l_VG.preventivoCurrent.NumOrdine);
		final Date d = new Date();
		d.setTime(this.l_VG.preventivoCurrent.Data.getTime());
		item.getItemProperty("Data").setValue(d);
		item.getItemProperty("Cliente").setValue(this.l_VG.preventivoCurrent.IDCliente);
		item.getItemProperty("RSoc").setValue(this.l_VG.preventivoCurrent.ragioneSocialeCliente);
		item.getItemProperty("N").setValue(this.l_VG.preventivoCurrent.Ventilatori.size());
		item.getItemProperty("Note").setValue(this.l_VG.preventivoCurrent.Note);
        this.l_VG.fmtNd.setMinimumFractionDigits(2);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
		//item.getItemProperty("Price").setValue(this.l_VG.fmtNd.format(this.l_VG.preventivoCurrent.CostoTotale));
		item.getItemProperty("Price").setValue(this.l_VG.preventivoCurrent.CostoTotale);
		final double prezzoVendita = this.l_VG.preventivoCurrent.CostoTotale * (100. - this.l_VG.preventivoCurrent.Sconto) / 100.;
		//item.getItemProperty("PriceUser").setValue(this.l_VG.fmtNd.format(prezzoVendita));
		item.getItemProperty("PriceUser").setValue(prezzoVendita);
		storePreventivo();
//		this.tablePreventivi.setEnabled(true);
	}

	@SuppressWarnings("unchecked")
	public void aggiornaCostoPreventivoCorrente() {
//		this.tablePreventivi.setEnabled(false);
		final Item item = this.containerPreventivi.getItem(Integer.toString(this.l_VG.preventivoCurrent.IDPreventivo));
		if (item == null) {
			return;
		}
        this.l_VG.fmtNd.setMinimumFractionDigits(2);
        this.l_VG.fmtNd.setMaximumFractionDigits(2);
		//item.getItemProperty("Price").setValue(this.l_VG.fmtNd.format(this.l_VG.preventivoCurrent.CostoTotale));
		item.getItemProperty("Price").setValue(this.l_VG.preventivoCurrent.CostoTotale);
		final double prezzoVendita = this.l_VG.preventivoCurrent.CostoTotale * (100. - this.l_VG.preventivoCurrent.Sconto) / 100.;
		//item.getItemProperty("PriceUser").setValue(this.l_VG.fmtNd.format(prezzoVendita));
		item.getItemProperty("PriceUser").setValue(prezzoVendita);
//		this.tablePreventivi.setEnabled(true);
	}
	
	public void aggiornaPreventivoCorrente() {
		aggiornaRigaPreventivoCorrente();
		this.tableVentilatori.setEnabled(false);
		initTabellaVentilatori();
		abilitaBottoni();
		this.tableVentilatori.setEnabled(true);
	}
	
	public void initTabellaPreventivi() {
		initTabellaPreventivi(false);
	}
	
	public void initTabellaPreventivi(final boolean preventivoNew) {
		if (!preventivoNew && this.l_VG.preventivoCurrent != null && this.l_VG.preventivoCurrentIndex != -1) {
			aggiornaPreventivoCorrente();
			return;
		}
		this.horizontalLayout2.removeAllComponents();
		@SuppressWarnings("unused")
		final Item item;
		this.l_VG.preventivoCurrent = null;
		this.l_VG.preventivoCurrentIndex = -1;
		this.tablePreventivi.setEnabled(false);
		initContainerPreventivi();
		try {
			for (int i=0 ; i<this.l_VG.ElencoPreventivi.size() ; i++) {
				final Object obj[] = new Object[10];
				final Preventivo l_p = this.l_VG.ElencoPreventivi.get(i);
				final String itemID = Integer.toString(l_p.IDPreventivo);
				obj[0] = l_p.NumOfferta;
				obj[1] = l_p.IDCliente;
				obj[2] = l_p.ragioneSocialeCliente;
				final Date d = new Date();
				d.setTime(l_p.Data.getTime());
				obj[3] = d;
				obj[4] = l_p.NumOrdine;
	            this.l_VG.fmtNd.setMinimumFractionDigits(2);
	            this.l_VG.fmtNd.setMaximumFractionDigits(2);
	            //obj[5] = this.l_VG.fmtNd.format(l_p.CostoTotale);
	            obj[5] = l_p.CostoTotale;
	    		final double prezzoVendita = l_p.CostoTotale * (100. - l_p.Sconto) / 100.;
	    		//obj[6] = this.l_VG.fmtNd.format(prezzoVendita);
	    		obj[6] = prezzoVendita;
				obj[7] = l_p.Ventilatori.size();
//				final XdevTextField tf = new XdevTextField();
//				tf.setSizeFull();
//				tf.setValue(l_p.Note);
//				tf.setData(itemID);
//				tf.setEnabled(true);
//				tf.addValueChangeListener(new ValueChangeListener(){
//					   @Override
//					public void valueChange(final Property.ValueChangeEvent event){
//						   l_p.Note = tf.getValue();
//						   l_p.PreventivoCambiato = true;
//					   }
//				});
				obj[8] = l_p.Note;
				obj[9] = l_p.IDPreventivo;
				if (!l_p.Banca.equals("Nascondi")) {
					this.tablePreventivi.addItem(obj, itemID);
				}
			}
		} catch (final Exception e) {
			this.l_VG.MainView.setMemo(e.toString());
		}
		initTabellaVentilatori();
		abilitaBottoni();
		this.tablePreventivi.setEnabled(true);
		this.tablePreventivi.setSizeFull();
		this.tablePreventivi.setColumnExpandRatio("Offerta", 1.5F);
		this.tablePreventivi.setColumnExpandRatio("Cliente", 0.4F);
		this.tablePreventivi.setColumnExpandRatio("RSoc", 2.0F);
		this.tablePreventivi.setColumnExpandRatio("Data", 1.2F);
		this.tablePreventivi.setColumnExpandRatio("Progetto", 1);
		this.tablePreventivi.setColumnExpandRatio("Price", 1);
		this.tablePreventivi.setColumnExpandRatio("PriceUser", 1);
		this.tablePreventivi.setColumnExpandRatio("N", 0.5F);
		this.tablePreventivi.setColumnExpandRatio("Note", 3);
		this.tablePreventivi.setColumnCollapsingAllowed(true);
		this.tablePreventivi.setColumnCollapsed("Codice", true);
	/*		this.containerPreventivi.addContainerProperty("Offerta", String.class, "");
		this.containerPreventivi.addContainerProperty("Cliente", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("RSoc", String.class, "");
		this.containerPreventivi.addContainerProperty("Data", Date.class, null);
		this.containerPreventivi.addContainerProperty("Progetto", String.class, "");
		this.containerPreventivi.addContainerProperty("Price", Double.class, "");
		this.containerPreventivi.addContainerProperty("PriceUser", Double.class, "");
		this.containerPreventivi.addContainerProperty("N", Integer.class, 0);
		this.containerPreventivi.addContainerProperty("Note", XdevTextField.class, "");
	 * 
	 * 
	 */
	
		this.horizontalLayout2.addComponent(this.tablePreventivi);
		this.horizontalLayout2.setComponentAlignment(this.tablePreventivi, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.tablePreventivi, 10.0F);
	}

	private void abilitaBottoni() {
		boolean value = false;
		if (this.l_VG.preventivoCurrent != null) {
			value = true;
		}
		this.buttonModificaPreventivo.setEnabled(value);
		this.buttonDuplicaPreventivo.setEnabled(value);
		this.buttonSalvaPreventivo.setEnabled(value);
		this.buttonChiudiPreventivo.setEnabled(value);
		this.buttonCancellaPreventivo.setEnabled(value);
		this.buttonResetPreventivo.setEnabled(value);
		this.buttonGeneraPreventivo.setEnabled(value);
		this.buttonDownloadPreventivo.setEnabled(false);
		value = false;
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		if (preventiviView.ventilatoreIndex >= 0) {
			value = true;
		}
		boolean enabled = false;
		try {
	        enabled = !this.l_VG.utilityCliente.isAlberoNudo(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
		} catch (final Exception e) {
			
		}
		this.buttonCambiaMotore.setEnabled(enabled);
		this.buttonAzione0.setEnabled(this.buttonCambiaMotore.isEnabled());
		this.buttonAzione1.setEnabled(this.buttonCambiaMotore.isEnabled());
		this.buttonAzione2.setEnabled(this.buttonCambiaMotore.isEnabled());
		this.buttonCancellaVentilatore.setEnabled(value);
		this.buttonAzione0.setEnabled(value);
		this.buttonAzione1.setEnabled(value);
		this.buttonAzione2.setEnabled(value);
		if (preventiviView.ventilatoreIndex > 0 && this.l_VG.ElencoVentilatoriFromPreventivo.size() > 0) {
			this.buttonVentilatoreUp.setEnabled(true);
		} else {
			this.buttonVentilatoreUp.setEnabled(false);
		}
		if (preventiviView.ventilatoreIndex >= 0 && preventiviView.ventilatoreIndex < (this.l_VG.ElencoVentilatoriFromPreventivo.size() - 1)) {
			this.buttonVentilatoreDown.setEnabled(true);
		} else {
			this.buttonVentilatoreDown.setEnabled(false);
		}
		preventiviView.abilitaTab();
	}
	
	private void initTabellaVentilatori() {
		double v;
		this.tableVentilatori.setEnabled(false);
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		preventiviView.ventilatoreIndex = -1;
		initContainerVentilatori();
		if (this.l_VG.preventivoCurrent == null) {
			return;
		}
		this.elencoNoteVentilatori.clear();
		for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
			final Object obj[] = new Object[16];
			final String itemID = Integer.toString(i);
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
			final VentilatorePreventivo l_vp = this.l_VG.preventivoCurrent.Ventilatori.get(i);
			final CheckBox cb = new CheckBox();
			cb.setData(itemID);
			cb.setValue(l_v.Selezionato);
			cb.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						l_v.Selezionato = cb.getValue().booleanValue();
						l_vp.HaPreventivo = l_v.Selezionato;
						l_v.VentilatoreCambiato = true;
						final PreventiviView preventivoTemp = (PreventiviView)PreventiviVentilatori2.this.l_VG.ElencoView[PreventiviVentilatori2.this.l_VG.PreventiviViewIndex];
						PreventiviVentilatori2.this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();;
						final PreventiviView preventiviView = (PreventiviView)PreventiviVentilatori2.this.l_VG.ElencoView[PreventiviVentilatori2.this.l_VG.PreventiviViewIndex];
						preventiviView.aggiornaPreventivo();
						preventiviView.aggiornaPreventivo();
						preventiviView.abilitaTab();
					}
		    });
			obj[0] = cb;
			final TextFieldInteger tfi = new TextFieldInteger();
//			VentilatorePreventivo l_vp = l_VG.preventivoCurrent.Ventilatori.get(i);
			tfi.setValue(Integer.toString(l_vp.Qta));
			tfi.setData(itemID);
			tfi.addValueChangeListener(new ValueChangeListener() {
				@Override
					public void valueChange(final com.vaadin.data.Property.ValueChangeEvent event) {
						l_vp.Qta = tfi.getIntegerValue();
						l_v.VentilatoreCambiato = true;
						final PreventiviView preventivoTemp = (PreventiviView)PreventiviVentilatori2.this.l_VG.ElencoView[PreventiviVentilatori2.this.l_VG.PreventiviViewIndex];
						PreventiviVentilatori2.this.l_VG.preventivoCurrent.CostoTotale = preventivoTemp.calcolaCostoPreventivo();;
						final PreventiviView preventiviView = (PreventiviView)PreventiviVentilatori2.this.l_VG.ElencoView[PreventiviVentilatori2.this.l_VG.PreventiviViewIndex];
						preventiviView.aggiornaPreventivo();
					}
			});
			obj[1] = tfi;
			obj[2] = this.l_VG.buildModelloCompleto(l_v);
			obj[3] = itemID;
			obj[4] = l_v.selezioneCorrente.Orientamento + " " + Integer.toString(l_v.selezioneCorrente.OrientamentoAngolo);
			obj[5] = l_v.selezioneCorrente.Esecuzione;
			final XdevLabel lblQ = new XdevLabel();
			lblQ.setContentMode(ContentMode.HTML);
			v = this.l_VG.utilityUnitaMisura.fromm3hToXX(l_v.selezioneCorrente.Marker, l_v.UMPortata);
			this.l_VG.fmtNd.setMaximumFractionDigits(l_v.UMPortata.nDecimaliStampa);
			lblQ.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPortata.simbolo);
			obj[6] = lblQ;
			final XdevLabel lblPt = new XdevLabel();
			lblPt.setContentMode(ContentMode.HTML);
			v = this.l_VG.utilityUnitaMisura.fromPaToXX(l_v.selezioneCorrente.PressioneTotale, l_v.UMPressione);
			this.l_VG.fmtNd.setMaximumFractionDigits(l_v.UMPressione.nDecimaliStampa);
			lblPt.setValue(this.l_VG.fmtNd.format(v) + " " + l_v.UMPressione.simbolo);
			obj[7] = lblPt;
			final XdevLabel lblDens = new XdevLabel();
			lblDens.setContentMode(ContentMode.HTML);
			lblDens.setValue(this.l_VG.fmtNd.format(l_v.CAProgettazione.rho) + " kg/m<sup>3</sup>");
			obj[8] = lblDens;
			final XdevLabel lblPow = new XdevLabel();
			lblPow.setContentMode(ContentMode.HTML);
			lblPow.setValue(this.l_VG.fmtNd.format(l_v.selezioneCorrente.MotoreInstallato.PotkW * 1000. / l_v.UMPotenza.fattoreConversione) + " " + l_v.UMPotenza.simbolo);
			obj[9] = lblPow;
			obj[10] = l_v.selezioneCorrente.NumeroGiri;
			obj[11] = l_v.selezioneCorrente.MotoreInstallato.Freq + " Hz";
			obj[12] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
			final XdevTextField tf = new XdevTextField();
			tf.setValue(l_v.Note);
			tf.setData(itemID);
			tf.setEnabled(true);
			tf.addValueChangeListener(new ValueChangeListener() {
				   @Override
				public void valueChange(final Property.ValueChangeEvent event){
					   l_v.Note = tf.getValue();
					   l_v.VentilatoreCambiato = true;
				   }
			});
			this.elencoNoteVentilatori.add(tf);
			obj[13] = tf;
			final XdevTextField tf2 = new XdevTextField();
			tf2.setValue(l_v.Note2);
			tf2.setData(itemID);
			tf2.setEnabled(true);
			tf2.addValueChangeListener(new ValueChangeListener() {
				   @Override
				public void valueChange(final Property.ValueChangeEvent event){
					   l_v.Note2 = tf2.getValue();
					   l_v.VentilatoreCambiato = true;
				   }
			});
			obj[14] = tf;
			obj[15] = this.l_VG.preventivoCurrent.Ventilatori.get(i).Qta * this.l_VG.preventivoCurrent.Ventilatori.get(i).CostoVentilatore;
			this.tableVentilatori.addItem(obj, itemID);
		}
		this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
		this.tableVentilatori.setEnabled(true);
	}
/*
	private double calcolaCostoPreventivo() {
		double costoPreventivo = 0.0;
		for (int i=0 ; i<l_VG.preventivoCurrent.Ventilatori.size() ; i++) {
			if (l_VG.preventivoCurrent.Ventilatori.get(i).HaPreventivo) {
				costoPreventivo += l_VG.preventivoCurrent.Ventilatori.get(i).Qta * l_VG.preventivoCurrent.Ventilatori.get(i).CostoVentilatore;
			}
		}
		return costoPreventivo;
	}
*/
	private void generaPreventivo() {
		try {
			if (this.l_VG.stampaOfferta.forzaLinguaIngleseFlag) {
				this.l_VG.setLinguaFromId("UK");
			}
			final ArrayList<String> ElencoModelloCompleto = new ArrayList<>();
	        for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
	            ElencoModelloCompleto.add(this.l_VG.buildModelloCompleto(this.l_VG.ElencoVentilatoriFromPreventivo.get(i), this.l_VG.parametriModelloCompletoForPrint));
	        }
	        for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
	            final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
	            this.l_VG.ventilatoreFisicaCurrent = new VentilatoreFisica();
	            this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
		        this.l_VG.ventilatoreFisicaCurrent.setVentilatoreFisica(this.l_VG.conTecnica, "", "", l_v, this.l_VG.ventilatoreFisicaParameter);
		       // this.l_VG.ElencoVentilatoriFromPreventivo.get(i).selezioneCorrente.PressioneTotale = this.l_VG.ventilatoreFisicaCurrent.getPressioneTotalePa();
		        //this.l_VG.ElencoVentilatoriFromPreventivo.get(i).selezioneCorrente.PressioneStatica = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaPa();
	    		this.l_VG.dbTecnico.caricaDimensioniEsecuzione(l_v);
	            if (!l_v.DatiCompleti) {
	                this.l_VG.completaCaricamentoVentilatore(l_v);
	            }
	        }
	        InputStream inwb = null;
	        inwb = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + this.l_VG.utilityCliente.idCliente + "_template_offerta.xlsx");
	        this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
            this.l_VG.stampaOfferta.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.ventilatoreFisicaParameter, inwb);
            this.l_VG.stampaOfferta.initUtility(this.l_VG.currentCitFont, this.l_VG.utility, this.l_VG.utilityTraduzioni, this.l_VG.utilityUnitaMisura, null, null);
	        final InputStream isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoofferta.png");
	        final InputStream isCurve[] = new InputStream[14];
	        if (this.l_VG.sfondoGraficiPrestazioni != null && this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
	        	for (int i=0 ; i<14 ; i++) {
					isCurve[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
				}
	        }
	        final InputStream isRumore[] = new InputStream[14];
	        final InputStream isRumore1[] = new InputStream[14];
	        if (this.l_VG.sfondoGraficiSpettro != null && this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag) {
	        	for (int i=0 ; i<14 ; i++) {
					isRumore[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
				}
	        	for (int i=0 ; i<14 ; i++) {
					isRumore1[i] = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
				}
	        	
	        }
	        this.l_VG.stampaOfferta.initStampaApachePOI(ElencoModelloCompleto, this.l_VG.utilityCliente, this.l_VG.gestioneAccessori, this.l_VG.currentUtilizzatore, isLogo, this.l_VG.Paths, this.l_VG.assiGraficiDefault, isCurve, isRumore, isRumore1);
	        this.l_VG.stampaOfferta.eseguiStampa(this.l_VG.Paths.rootResources + this.l_VG.preventivoCurrent.NumOfferta, this.ElencoFileXls, this.l_VG.currentCliente, this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo, this.l_VG.catalogoCondizioniUmidita, this.l_VG.ParametriVari);
	        if (this.ElencoFileXls.size() > 0) {
	        	this.l_VG.dbCRMv2.storePreventivoFanStampato(this.l_VG.preventivoCurrent, this.ElencoFileXls.get(0));
	        	final String memo = this.l_VG.utilityTraduzioni.TraduciStringa("Generato") + " " + this.ElencoFileXls.get(0);
	            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneStampaOfferta, memo);
	            this.buttonDownloadPreventivo.setEnabled(true);
		        final Resource res = new FileResource(new File(this.ElencoFileXls.get(0)));
		        this.memoDownload = this.ElencoFileXls.get(0);
		        if (this.fileDownloader == null) {
		        	this.fileDownloader = new FileDownloader(res);
		    		this.fileDownloader.extend(this.buttonDownloadPreventivo);
		        } else {
		        	this.fileDownloader.setFileDownloadResource(res);
		        }
				this.msgBox = MessageBox.createInfo();
				this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo Correttamente Generato"));
				this.msgBox.withOkButton();
				this.msgBox.open();
	        } else {
	        	this.buttonDownloadPreventivo.setEnabled(false);
	        }
	        if (this.l_VG.stampaOfferta.forzaLinguaIngleseFlag)
			 {
				this.l_VG.setLinguaFromId("IT");
				//l_VG.MainView.setMemo(l_VG.stampaOfferta.traceMessage);
				//l_VG.MainView.setMemo(ElencoFileXls.get(0));
			}
		} catch (final Exception e) {
	        //l_VG.MainView.setMemo(e.toString());
			if (this.l_VG.debugFlag) {
				Notification.show(e.toString());
			}
		}
	}
	
	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tablePreventivi}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tablePreventivi_valueChange(final Property.ValueChangeEvent event) {
		if (!this.tablePreventivi.isEnabled()) {
			return;
		}
		try {
			if (this.l_VG.preventivoCurrent != null) {
				//l_VG.MainView.setMemo("store preventivo "+l_VG.preventivoCurrent.NumOfferta);
				storePreventivo();
			}
			final Item item = this.containerPreventivi.getItem(event.getProperty().getValue().toString());
			final int codicePreventivo = (int)item.getItemProperty("Codice").getValue();
			for (int i=0 ; i<this.l_VG.ElencoPreventivi.size() ; i++) {
				if (this.l_VG.ElencoPreventivi.get(i).IDPreventivo == codicePreventivo) {
					this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(i);
					this.l_VG.preventivoCurrentIndex = i;
					break;
				}
			}
			if (this.l_VG.preventivoCurrent.IDCliente == -1) {
				this.l_VG.currentCliente = new Clientev21();
			} else {
				this.l_VG.currentCliente = this.l_VG.dbCRMv2.setCurrentClienteFromCodice(this.l_VG.preventivoCurrent.IDCliente, this.l_VG.ElencoClienti);
			}
			this.l_VG.dbCRMv2.loadElencoVentilatoriPreventivo(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
			for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
				final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
				l_v.ERPCaricato = false;
				this.l_VG.buildDatiERP327ForSellFan(l_v);
				if (l_v.UMAltezza == null) {
					l_v.UMAltezza = (UnitaMisura)this.l_VG.UMAltezzaCorrente.clone();
				}
				if (l_v.UMPortata == null) {
					l_v.UMPortata = (UnitaMisura)this.l_VG.UMPortataCorrente.clone();
				}
				if (l_v.UMPotenza == null) {
					l_v.UMPotenza = (UnitaMisura)this.l_VG.UMPotenzaCorrente.clone();
				}
				if (l_v.UMPressione == null) {
					l_v.UMPressione = (UnitaMisura)this.l_VG.UMPressioneCorrente.clone();
				}
				if (l_v.UMTemperatura == null) {
					l_v.UMTemperatura = (UnitaMisura)this.l_VG.UMTemperaturaCorrente.clone();
				}
			}
			initTabellaVentilatori();
			abilitaBottoni();
			final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
			preventiviView.aggiornaPreventivo();
			preventiviView.abilitaTab();
		} catch (final Exception e) {
//			this.l_VG.MainView.setMemo(e.toString());
			this.l_VG.preventivoCurrent = null;
			this.l_VG.currentCliente = null;
			this.l_VG.ElencoVentilatoriFromPreventivo.clear();
			initTabellaVentilatori();
			this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
			abilitaBottoni();
			final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
			preventiviView.abilitaTab();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableVentilatori}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableVentilatori_valueChange(final Property.ValueChangeEvent event) {
		if (!this.tableVentilatori.isEnabled()) {
			return;
		}
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		try {
			storePreventivo();
			this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(event.getProperty().getValue().toString());
			preventiviView.ventilatoreIndex = Integer.parseInt(event.getProperty().getValue().toString());
			preventiviView.aggiornaCosto();
			abilitaBottoni();
		} catch (final Exception e) {
			this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
			abilitaBottoni();
			preventiviView.ventilatoreIndex = -1;
			preventiviView.abilitaTab();
			//Notification.show(e.toString());
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCambiaMotore}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCambiaMotore_buttonClick(final Button.ClickEvent event) {
		final String warningStr = this.l_VG.utilityCliente.getWarning(this.l_VG.VentilatoreCurrent, 0);
		if (warningStr != null) {
			this.l_VG.MainView.setAccessoriVentilatoreCurrent();
			this.msgBox = MessageBox.create();
			this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa(warningStr));
			//pippo
			this.msgBox.withOkButton(() -> {
				this.l_VG.cambiaManualmenteMotore();
				if (this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null) {
			        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
			        ventilatorePreventivo.HaMotore = true;
			        ventilatorePreventivo.MotoreSelezionato = true;
			        ventilatorePreventivo.VentilatoreCambiato = true;
					final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
					preventiviView.aggiornaCosto();
			        aggiornaVentilatoreCorrente();
				}
			});
			this.msgBox.open();
		} else {
			this.l_VG.cambiaManualmenteMotore();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonVentilatoreDown}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonVentilatoreDown_buttonClick(final Button.ClickEvent event) {
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		if (preventiviView.ventilatoreIndex + 1 < this.l_VG.ElencoVentilatoriFromPreventivo.size()) {
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(preventiviView.ventilatoreIndex);
			final VentilatorePreventivo l_vp = this.l_VG.preventivoCurrent.Ventilatori.get(preventiviView.ventilatoreIndex);
			this.l_VG.ElencoVentilatoriFromPreventivo.remove(preventiviView.ventilatoreIndex);
			this.l_VG.preventivoCurrent.Ventilatori.remove(this.l_VG.VentilatoreCurrentIndex);
			this.l_VG.ElencoVentilatoriFromPreventivo.add(preventiviView.ventilatoreIndex + 1, l_v);
			this.l_VG.preventivoCurrent.Ventilatori.add(preventiviView.ventilatoreIndex + 1, l_vp);
			storePreventivo();
			initTabellaVentilatori();
			abilitaBottoni();
			preventiviView.aggiornaPreventivo();
			preventiviView.abilitaTab();
		}
/*
		Collection Ventilatoriids = containerVentilatori.getItemIds();
		Object idobj[] = Ventilatoriids.toArray();
		if (preventiviView.ventilatoreIndex + 1 < idobj.length) {
			tableVentilatori.select(idobj[preventiviView.ventilatoreIndex + 1]);
		}
*/
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonVentilatoreUp}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonVentilatoreUp_buttonClick(final Button.ClickEvent event) {
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		if (preventiviView.ventilatoreIndex > 0) {
			final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(preventiviView.ventilatoreIndex);
			final VentilatorePreventivo l_vp = this.l_VG.preventivoCurrent.Ventilatori.get(preventiviView.ventilatoreIndex);
			this.l_VG.ElencoVentilatoriFromPreventivo.remove(preventiviView.ventilatoreIndex);
			this.l_VG.preventivoCurrent.Ventilatori.remove(this.l_VG.VentilatoreCurrentIndex);
			this.l_VG.ElencoVentilatoriFromPreventivo.add(preventiviView.ventilatoreIndex - 1, l_v);
			this.l_VG.preventivoCurrent.Ventilatori.add(preventiviView.ventilatoreIndex - 1, l_vp);
			storePreventivo();
			initTabellaVentilatori();
			abilitaBottoni();
			preventiviView.aggiornaPreventivo();
			preventiviView.abilitaTab();
		}
/*
		Collection Ventilatoriids = containerVentilatori.getItemIds();
		Object idobj[] = Ventilatoriids.toArray();
		if (preventiviView.ventilatoreIndex > 0) {
			tableVentilatori.select(idobj[preventiviView.ventilatoreIndex - 1]);
		}
*/
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonGeneraPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonGeneraPreventivo_buttonClick(final Button.ClickEvent event) {
		int nv = 0;
		for (int i=0 ; i<this.l_VG.preventivoCurrent.Ventilatori.size() ; i++) {
			if (this.l_VG.preventivoCurrent.Ventilatori.get(i).HaPreventivo) {
				nv++;
			}
		}
		if (nv <= 0) {
			this.msgBox = MessageBox.createInfo();
			this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Nessun ventilatore selezionato"));
			this.msgBox.withOkButton();
			this.msgBox.open();
			return;
		}
        if (this.l_VG.stampaOfferta == null) {
       		this.l_VG.stampaOfferta = new Cliente04StampaOffertaPoiXLSX();
        }
		final Cliente04pannelloPagineOfferta pannello = new Cliente04pannelloPagineOfferta();
		pannello.setVariabiliGlobali(this.l_VG);
		this.msgBox = MessageBox.create();
		this.msgBox.withMessage(pannello);
		this.msgBox.withAbortButton();
		this.msgBox.withOkButton(() -> {
			pannello.getFlag();
	        final Window popup = PopupWindow.For(new ProgressView(this.l_VG.utilityTraduzioni)).closable(false).draggable(false).resizable(false).modal(true).show();
	        UI.getCurrent().push();
			generaPreventivo();
			popup.close();
			});
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonDuplicaPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonDuplicaPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.preventivoCurrent.IDPreventivo = -1;
        for (int i=0 ; i<this.l_VG.preventivoCurrent.Ventilatori.size() ; i++) {
            this.l_VG.preventivoCurrent.Ventilatori.get(i).IDVentilatore = -1;
            this.l_VG.ElencoVentilatoriFromPreventivo.get(i).IDVentilatore = -1;
        }
        this.l_VG.preventivoCurrent.Data = new Date();
		storePreventivo();
        final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
		PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        preventiviView.reinitPreventivi();
        setPreventivoSelected(idPreventivo.toString());
	}
	
	public void reinitTabellaPreventivi() {
        initTabellaPreventivi(true);
		this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
	}
	
	public void NewModifyPrenentivo(final boolean newMode) {
		if (newMode) {
	        if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneNuovaOfferta, this.l_VG.ElencoPreventivi.size())) {
	        	this.msgBox = MessageBox.createWarning();
	        	this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
	        	this.msgBox.withOkButton();
	        	this.msgBox.open();
	            return;
	        }
		}
		final pannelloPreventivoEditNew pannello = new pannelloPreventivoEditNew();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.newMode = newMode;
		pannello.init();
		this.msgBox = MessageBox.create();
		this.msgBox.withMessage(pannello);
		this.msgBox.withAbortButton();
		this.msgBox.withOkButton(() -> {
			if (newMode) {
				this.l_VG.preventivoCurrent = new Preventivo();
				this.l_VG.ElencoVentilatoriFromPreventivo.clear();
			}
			pannello.getValues();
			storePreventivo();
	        final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
	        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
	        if (newMode) {
	        	this.l_VG.preventivoCurrentIndex = this.l_VG.ElencoPreventivi.size() - 1;
	        }
	        this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex);
			PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
	        preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
	        if (newMode) {
	        	initTabellaPreventivi(true);
//	        	this.preventiviView.initPreventivi();
	        	setPreventivoSelected(idPreventivo.toString());
	            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneNuovaOfferta, "ID: " + Integer.toString(idPreventivo));
	        } else {
	        	preventiviView.aggiornaPreventivo();
	        }
		});
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonNuovoPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonNuovoPreventivo_buttonClick(final Button.ClickEvent event) {
		this.editPreventivo(true);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonModificaPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonModificaPreventivo_buttonClick(final Button.ClickEvent event) {
		this.editPreventivo(false);
	}

	private void editPreventivo(final boolean newMode) {
		if (newMode) {
	        if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneNuovaOfferta, this.l_VG.ElencoPreventivi.size())) {
	        	this.msgBox = MessageBox.createWarning();
	        	this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
	        	this.msgBox.withOkButton();
	        	this.msgBox.open();
	            return;
	        }
		}
		final pannelloEditPreventivo pannello = new pannelloEditPreventivo(newMode);
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.init();
		this.msgBox = MessageBox.create();
		this.msgBox.withMessage(pannello);
		this.msgBox.withCancelButton();
		this.msgBox.withSaveButton(() -> {
			if (newMode) {
				this.l_VG.preventivoCurrent = new Preventivo();
				this.l_VG.ElencoVentilatoriFromPreventivo.clear();
			}
			pannello.getValues();
			storePreventivo();
			final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
	        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
	        if (newMode) {
	        	this.l_VG.preventivoCurrentIndex = this.l_VG.ElencoPreventivi.size() - 1;
	        }this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex);
			final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
	        if (newMode) {
	        	initTabellaPreventivi(true);
	        	setPreventivoSelected(idPreventivo.toString());
	            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneNuovaOfferta, "ID: " + Integer.toString(idPreventivo));
	        } else {
	        	preventiviView.aggiornaPreventivo();
	        }
		});
		this.msgBox.withWidth("800px");
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonFiltra}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonFiltra_buttonClick(final Button.ClickEvent event) {
        if (this.l_VG.preventivoCurrent != null) {
			storePreventivo();
		}
        this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
        for (int i=this.l_VG.ElencoPreventivi.size()-1 ; i>=0 ; i--) {
            final Preventivo l_p = this.l_VG.ElencoPreventivi.get(i);
            if (!isFiltroPreventiviOK(l_p)) {
                this.l_VG.ElencoPreventivi.remove(i);
            }
        }
        initTabellaPreventivi(true);
		this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(null);
	}

    private boolean isFiltroPreventiviOK(final Preventivo l_p) {
        if (!this.textFieldRagioneSociale.getValue().equals("")) {
            if (!l_p.ragioneSocialeCliente.contains(this.textFieldRagioneSociale.getValue())) {
            	return false;
            }
        }
        if (!this.textFieldCodiceCliente.getValue().equals("")) {
            if (l_p.IDCliente != this.textFieldCodiceCliente.getIntegerValue()) {
            	return false;
            }
        }
        final Calendar l_pData = Calendar.getInstance();
        l_pData.setTime(l_p.Data);
        Date l_d = this.textFieldDataIn.getValue();
        final Calendar l_dData = Calendar.getInstance();
        l_dData.setTime(l_d);
        l_dData.set(Calendar.HOUR, 0);
        l_dData.set(Calendar.MINUTE, 0);
        l_dData.set(Calendar.SECOND, 0);
        if (l_dData.after(l_pData)) {
        	return false;
        }
        l_d = this.textFieldDataOut.getValue();
        l_dData.setTime(l_d);
        l_dData.set(Calendar.HOUR, 23);
        l_dData.set(Calendar.MINUTE, 59);
        l_dData.set(Calendar.SECOND, 59);
        if (l_dData.before(l_pData)) {
        	return false;
        }
        return true;
    }

    /**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonResetFiltri}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	@SuppressWarnings("deprecation")
	private void buttonResetFiltri_buttonClick(final Button.ClickEvent event) {
		this.textFieldCodiceCliente.setValue("");
		this.textFieldRagioneSociale.setValue("");
		this.textFieldDataIn.setValue(new Date("01/01/2017"));
		this.textFieldDataOut.setValue(new Date());
		buttonFiltra_buttonClick(null);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCancellaVentilatore}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCancellaVentilatore_buttonClick(final Button.ClickEvent event) {
		this.msgBox = MessageBox.createQuestion();
		this.msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Cancella"));
		this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Cancellazione Ventilatore Selezionato"));
		this.msgBox.withAbortButton();
		this.msgBox.withOkButton(() -> {
	        this.l_VG.ElencoVentilatoriFromPreventivo.remove(this.l_VG.VentilatoreCurrentIndex);
	        this.l_VG.preventivoCurrent.Ventilatori.remove(this.l_VG.VentilatoreCurrentIndex);
			final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
			this.l_VG.preventivoCurrent.CostoTotale = preventiviView.calcolaCostoPreventivo();;
			preventiviView.aggiornaPreventivo();
			storePreventivo();
			initTabellaVentilatori();
			aggiornaPreventivoCorrente();
			abilitaBottoni();
			});
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonResetPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonResetPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.ElencoVentilatoriFromPreventivo.clear();
        this.l_VG.preventivoCurrent.Ventilatori.clear();
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
		this.l_VG.preventivoCurrent.CostoTotale = preventiviView.calcolaCostoPreventivo();;
		preventiviView.aggiornaPreventivo();
		storePreventivo();
		initTabellaVentilatori();
		aggiornaPreventivoCorrente();
		abilitaBottoni();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAzione0}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAzione0_buttonClick(final Button.ClickEvent event) {
        this.l_VG.utilityCliente.eseguiPannelloPreventivoAzioneSpeciale0(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex), this.l_VG.pannelloPreventivoAzioniSpeciali0Desc);
        final String note = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).Note;
        //note = l_VG.utilityTraduzioni.TraduciStringa(note);
        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
        ventilatorePreventivo.Note = note;
        if (ventilatorePreventivo.HaMotore) {
        	ventilatorePreventivo.MotoreSelezionato = true;
        }
        ventilatorePreventivo.VentilatoreCambiato = true;
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        final boolean ventilatoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).VentilatoreSelezionato;
        final boolean motoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).MotoreSelezionato;
		this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
		preventiviView.aggiornaCosto();
        aggiornaVentilatoreCorrente();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAzione1}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAzione1_buttonClick(final Button.ClickEvent event) {
        this.l_VG.utilityCliente.eseguiPannelloPreventivoAzioneSpeciale1(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex), this.l_VG.pannelloPreventivoAzioniSpeciali1Desc);
        final String note = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).Note;
        //note = l_VG.utilityTraduzioni.TraduciStringa(note);
        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
        ventilatorePreventivo.Note = note;
       	ventilatorePreventivo.MotoreSelezionato = false;
        ventilatorePreventivo.VentilatoreCambiato = true;
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        final boolean ventilatoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).VentilatoreSelezionato;
        final boolean motoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).MotoreSelezionato;
		this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
		preventiviView.aggiornaCosto();
        aggiornaVentilatoreCorrente();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAzione2}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAzione2_buttonClick(final Button.ClickEvent event) {
        this.l_VG.utilityCliente.eseguiPannelloPreventivoAzioneSpeciale2(this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex), this.l_VG.pannelloPreventivoAzioniSpeciali2Desc);
        final String note = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).Note;
        //note = l_VG.utilityTraduzioni.TraduciStringa(note);
        final VentilatorePreventivo ventilatorePreventivo = this.l_VG.preventivoCurrent.Ventilatori.get(this.l_VG.VentilatoreCurrentIndex);
        ventilatorePreventivo.Note = note;
       	ventilatorePreventivo.MotoreSelezionato = false;
        ventilatorePreventivo.VentilatoreCambiato = true;
		final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
        final boolean ventilatoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).VentilatoreSelezionato;
        final boolean motoreSelezionato = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex).Ventilatori.get(this.l_VG.VentilatoreCurrentIndex).MotoreSelezionato;
		this.l_VG.utilityCliente.reloadAccessori(this.l_VG.VentilatoreCurrent, this.l_VG.elencoGruppiAccessori, ventilatoreSelezionato, motoreSelezionato);
		preventiviView.aggiornaCosto();
        aggiornaVentilatoreCorrente();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSalvaPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonSalvaPreventivo_buttonClick(final Button.ClickEvent event) {
		if (this.l_VG.preventivoCurrent != null) {
			{
				for (int i=0 ; i<this.l_VG.ElencoPreventivi.size() ; i++) {
					if (this.l_VG.ElencoPreventivi.get(i).IDPreventivo == this.l_VG.preventivoCurrent.IDPreventivo) {
						this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(i);
						this.l_VG.preventivoCurrentIndex = i;
						break;
					}
				}
				if (this.l_VG.preventivoCurrent.IDCliente == -1) {
					this.l_VG.currentCliente = new Clientev21();
				} else {
					this.l_VG.currentCliente = this.l_VG.dbCRMv2.setCurrentClienteFromCodice(this.l_VG.preventivoCurrent.IDCliente, this.l_VG.ElencoClienti);
				}
				this.l_VG.dbCRMv2.loadElencoVentilatoriPreventivo(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
				final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
				for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; i++) {
					final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromPreventivo.get(i);
					l_v.ERPCaricato = false;
					this.l_VG.buildDatiERP327ForSellFan(l_v);
					if (l_v.UMAltezza == null) {
						l_v.UMAltezza = (UnitaMisura)this.l_VG.UMAltezzaCorrente.clone();
					}
					if (l_v.UMPortata == null) {
						l_v.UMPortata = (UnitaMisura)this.l_VG.UMPortataCorrente.clone();
					}
					if (l_v.UMPotenza == null) {
						l_v.UMPotenza = (UnitaMisura)this.l_VG.UMPotenzaCorrente.clone();
					}
					if (l_v.UMPressione == null) {
						l_v.UMPressione = (UnitaMisura)this.l_VG.UMPressioneCorrente.clone();
					}
					if (l_v.UMTemperatura == null) {
						l_v.UMTemperatura = (UnitaMisura)this.l_VG.UMTemperaturaCorrente.clone();
					}
					this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(Integer.toString(i));
					preventiviView.ventilatoreIndex = i;
					preventiviView.aggiornaCosto();
				}
				initTabellaVentilatori();
				abilitaBottoni();

				preventiviView.aggiornaPreventivo();
				preventiviView.abilitaTab();
			}}
			
			
//			//l_VG.MainView.setMemo("store preventivo "+l_VG.preventivoCurrent.NumOfferta);
//			final PreventiviView preventiviView = (PreventiviView)this.l_VG.ElencoView[this.l_VG.PreventiviViewIndex];
//			for ( final Object component : preventiviView.getVentilatori().containerVentilatori.getItemIds()) {
//				storePreventivo();
//				this.l_VG.MainView.setVentilatoreCorrenteFromPreventivo(component.toString());
//				preventiviView.ventilatoreIndex = Integer.parseInt(component.toString());
//				preventiviView.aggiornaCosto();
//		        aggiornaVentilatoreCorrente();
//			}
//
//	        aggiornaCostoPreventivoCorrente();
//		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonDownloadPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonDownloadPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneDownloadOfferta, "Download Preventivo " + this.memoDownload);
        this.msgBox = MessageBox.create();
		this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Continuare con il preventivo corrente") + "?");
		this.msgBox.withYesButton();
		this.msgBox.withNoButton(() -> {
			NewModifyPrenentivo(true);
		});
		this.msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonChiudiPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonChiudiPreventivo_buttonClick(final Button.ClickEvent event) {
		if (this.l_VG.preventivoCurrent != null) {
			//l_VG.MainView.setMemo("store preventivo "+l_VG.preventivoCurrent.NumOfferta);
			storePreventivo();
			final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
			this.tablePreventivi.unselect(idPreventivo.toString());
			this.l_VG.MainView.resetMainView();
		}
	}
	
	private void buttonCancellaPreventivo_buttonClick(final Button.ClickEvent event) {
		if (this.l_VG.preventivoCurrent != null) {
			this.msgBox = MessageBox.create();
			this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Sicuro di voler cancellare il preventivo?") + "\n" + this.l_VG.utilityTraduzioni.TraduciStringa("L'operazione è irreversibile."));
			this.msgBox.withNoButton();
			this.msgBox.withYesButton(() -> {
				this.l_VG.preventivoCurrent.Banca = "Nascondi";
				storePreventivo();
				
				reinitTabellaPreventivi();
				this.l_VG.preventivoCurrent = null;
			});
			this.msgBox.open();
			}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	@SuppressWarnings("deprecation")
	private void initUI() {
		this.verticalLayout = new XdevVerticalLayout();
		this.panelFiltri = new XdevPanel();
		this.verticalLayout3 = new XdevVerticalLayout();
		this.horizontalLayout9 = new XdevHorizontalLayout();
		this.labelCodiceCliente2 = new XdevLabel();
		this.textFieldCodiceClienteGosth2 = new XdevTextField();
		this.labelDataCompresa2 = new XdevLabel();
		this.labelTra2 = new XdevLabel();
		this.spacerLowerMenu = new XdevLabel();
		this.spacerFinal = new XdevLabel();
		this.spacerFilters = new XdevLabel();
		this.textFieldDataInGosth2 = new XdevTextField();
		this.labele2 = new XdevLabel();
		this.textFieldDataOutGosth2 = new XdevTextField();
		this.horizontalLayout6 = new XdevHorizontalLayout();
		this.labelRagioneSociale = new XdevLabel();
		this.textFieldRagioneSociale = new XdevTextField();
		this.horizontalLayout7 = new XdevHorizontalLayout();
		this.buttonResetFiltri = new XdevButton();
		this.buttonFiltra = new XdevButton();
		this.panel2 = new XdevPanel();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.tablePreventivi = new XdevTable<>();
		this.panel3 = new XdevPanel();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.buttonModificaPreventivo = new XdevButton();
		this.buttonDuplicaPreventivo = new XdevButton();
		this.buttonChiudiPreventivo = new XdevButton();
		this.buttonCancellaPreventivo = new XdevButton();
		this.buttonSalvaPreventivo = new XdevButton();
		this.buttonNuovoPreventivo = new XdevButton();
		this.panel4 = new XdevPanel();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.verticalLayout2 = new XdevVerticalLayout();
		this.buttonVentilatoreUp = new XdevButton();
		this.buttonVentilatoreDown = new XdevButton();
		this.tableVentilatori = new XdevTable<>();
		this.panel5 = new XdevPanel();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.buttonResetPreventivo = new XdevButton();
		this.buttonCancellaVentilatore = new XdevButton();
		this.buttonAzione0 = new XdevButton();
		this.buttonAzione1 = new XdevButton();
		this.buttonAzione2 = new XdevButton();
		this.panel7 = new XdevPanel();
		this.horizontalLayout8 = new XdevHorizontalLayout();
		this.buttonCambiaMotore = new XdevButton();
		this.buttonGeneraPreventivo = new XdevButton();
		this.buttonDownloadPreventivo = new XdevButton();
	
		this.verticalLayout.setSpacing(false);
		this.verticalLayout.setMargin(new MarginInfo(false));
		this.verticalLayout3.setSpacing(false);
		this.verticalLayout3.setMargin(new MarginInfo(false));
		this.labelCodiceCliente2.setValue("Codice Cliente");
		this.labelCodiceCliente2.setContentMode(ContentMode.HTML);
		this.textFieldCodiceClienteGosth2.setColumns(5);
		this.labelDataCompresa2.setValue("Data Compresa");
		this.labelDataCompresa2.setContentMode(ContentMode.HTML);
		this.labelTra2.setValue("tra");
		this.labelTra2.setContentMode(ContentMode.HTML);
		this.textFieldDataInGosth2.setColumns(5);
		this.labele2.setValue("e");
		this.labele2.setContentMode(ContentMode.HTML);
		this.spacerLowerMenu.setValue("");
		this.spacerFinal.setValue("");
		this.spacerFilters.setValue("");
		this.textFieldDataOutGosth2.setColumns(5);
		this.horizontalLayout6.setMargin(new MarginInfo(false, true, false, true));
//		this.labelRagioneSociale.setValue("Ragione Sociale");
//		this.labelRagioneSociale.setContentMode(ContentMode.HTML);
		this.buttonResetFiltri.setCaption("Reset Filtri");
		this.buttonResetFiltri.setStyleName("small");
		//this.buttonFiltra.setIcon(new FileResource( new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/funnel.png")));
		this.buttonFiltra.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/funnel.png"));
		this.buttonFiltra.setCaption("Filtra Preventivi");
		this.buttonFiltra.setStyleName("small");
		this.horizontalLayout2.setMargin(new MarginInfo(false, true, false, true));
		this.tablePreventivi.setStyleName("small mystriped");
//		this.buttonModificaPreventivo.setIcon(new FileResource(
//				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/Edit.png")));
		this.buttonModificaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Edit.png"));
		this.buttonModificaPreventivo.setCaption("Rinomina Preventivo");
//		this.buttonModificaPreventivo.setStyleName("small");
//		this.buttonDuplicaPreventivo.setIcon(new FileResource(
//				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/copy.png")));
		this.buttonDuplicaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/copy.png"));
		this.buttonDuplicaPreventivo.setCaption("Duplica Preventivo");
		this.buttonCancellaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Delete16.png"));
		this.buttonCancellaPreventivo.setCaption("Cancella Preventivo");
//		this.buttonDuplicaPreventivo.setStyleName("small");
/*		this.buttonChiudiPreventivo.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/chest16.png")));*/
		this.buttonChiudiPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/chest16.png"));
		this.buttonChiudiPreventivo.setCaption("Chiudi Preventivo");
		this.buttonChiudiPreventivo.setStyleName("small");
		/*this.buttonSalvaPreventivo.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/floppy_disk.png")));*/
		this.buttonSalvaPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/floppy_disk.png"));
		this.buttonSalvaPreventivo.setCaption("Salva e Aggiorna Preventivo");
	/*	this.buttonNuovoPreventivo.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/newspaper_add.png")));*/
		this.buttonNuovoPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/newspaper_add.png"));
		this.buttonNuovoPreventivo.setCaption("Nuovo Preventivo");
		this.buttonNuovoPreventivo.setStyleName("giallo");
		this.horizontalLayout.setMargin(new MarginInfo(false, true, false, false));
		this.verticalLayout2.setMargin(new MarginInfo(false, true, false, true));
		/*this.buttonVentilatoreUp.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/nav_up_blue.png")));*/
		this.buttonVentilatoreUp.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/nav_up_blue.png"));
		this.buttonVentilatoreUp.setCaption("");
		this.buttonVentilatoreUp.setStyleName("small");
/*		this.buttonVentilatoreDown.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/nav_down_blue.png")));*/
		this.buttonVentilatoreDown.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/nav_down_blue.png"));
		this.buttonVentilatoreDown.setCaption("");
		this.buttonVentilatoreDown.setStyleName("small");
		this.tableVentilatori.setStyleName("small mystriped");
/*		this.buttonResetPreventivo.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/replace2.png")));*/
		this.buttonResetPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/replace2.png"));
		this.buttonResetPreventivo.setCaption("Reset Preventivo");
		this.buttonResetPreventivo.setStyleName("small");
/*		this.buttonCancellaVentilatore.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/Delete24.png")));*/
		this.buttonCancellaVentilatore.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Delete16.png"));
		this.buttonCancellaVentilatore.setCaption("Cancella");
		this.buttonCancellaVentilatore.setStyleName("small");
		this.buttonAzione0.setCaption("Azione0");
		this.buttonAzione0.setStyleName("small");
		this.buttonAzione1.setCaption("Azione1");
		this.buttonAzione1.setStyleName("small");
		this.buttonAzione2.setCaption("Azione2");
		this.buttonAzione2.setStyleName("small");
		this.panel7.setTabIndex(0);
		//this.horizontalLayout8.setMargin(new MarginInfo(false, true, true, true));
/*		this.buttonCambiaMotore.setIcon(new FileResource(new File(VaadinService.getCurrent().getBaseDirectory(),
				"WebContent/resources/img/documents_exchange.png")));*/
		this.buttonCambiaMotore.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/documents_exchange.png"));
		this.buttonCambiaMotore.setCaption("Cambia Motore");
		this.buttonCambiaMotore.setStyleName("big");
		this.buttonGeneraPreventivo.setCaption("Genera Preventivo");
		this.buttonGeneraPreventivo.setStyleName("big azzurro");
/*		this.buttonDownloadPreventivo.setIcon(new FileResource(
				new File(VaadinService.getCurrent().getBaseDirectory(), "WebContent/resources/img/Download.png")));*/
		this.buttonDownloadPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Download.png"));
		this.buttonDownloadPreventivo.setCaption("Download Preventivo");
		this.buttonDownloadPreventivo.setStyleName("big");
	
		//this.labelCodiceCliente2.setSizeUndefined();
		//this.horizontalLayout9.addComponent(this.labelCodiceCliente2);
		//this.horizontalLayout9.setComponentAlignment(this.labelCodiceCliente2, Alignment.TOP_RIGHT);
		//this.textFieldCodiceClienteGosth2.setSizeUndefined();
//		this.textFieldCodiceClienteGosth2.setCaption("Codice Cliente");
		this.textFieldCodiceCliente.setSizeFull();
		this.textFieldCodiceCliente.setCaption("Codice Cliente");
		this.horizontalLayout9.addComponent(this.textFieldCodiceCliente);
		this.horizontalLayout9.setExpandRatio(this.textFieldCodiceCliente, 1.2F);
		this.horizontalLayout9.setComponentAlignment(this.textFieldCodiceCliente, Alignment.MIDDLE_LEFT);
		this.textFieldRagioneSociale.setSizeFull();
		this.textFieldRagioneSociale.setCaption("Ragione Sociale");
		this.horizontalLayout9.addComponent(this.textFieldRagioneSociale);
		this.horizontalLayout9.setExpandRatio(this.textFieldRagioneSociale, 1.2F);
		this.horizontalLayout9.setComponentAlignment(this.textFieldRagioneSociale, Alignment.MIDDLE_LEFT);
//		this.horizontalLayout9.addComponent(this.labelDataCompresa2);
//		this.horizontalLayout9.setComponentAlignment(this.labelDataCompresa2, Alignment.TOP_RIGHT);
//		this.labelTra2.setSizeUndefined();
//		this.horizontalLayout9.addComponent(this.labelTra2);
//		this.horizontalLayout9.setComponentAlignment(this.labelTra2, Alignment.TOP_RIGHT);
		this.textFieldDataIn.setDateFormat("dd/MM/yyyy");
		this.textFieldDataOut.setDateFormat("dd/MM/yyyy");
//		this.textFieldCodiceCliente.setSizeUndefined();
//		this.textFieldCodiceCliente.setCaption("Codice Cliente");
//		this.horizontalLayout9.replaceComponent(this.textFieldCodiceClienteGosth2, this.textFieldCodiceCliente);
//		this.horizontalLayout9.setComponentAlignment(this.textFieldCodiceCliente, Alignment.MIDDLE_LEFT);
		this.textFieldDataIn.setSizeFull();
		this.textFieldDataIn.setCaption("Ricerca dal");
		this.horizontalLayout9.addComponent(this.textFieldDataIn);
		this.horizontalLayout9.setExpandRatio(this.textFieldDataIn, 1.0F);
		this.horizontalLayout9.setComponentAlignment(this.textFieldDataIn, Alignment.MIDDLE_LEFT);
		this.textFieldDataOut.setSizeFull();
		this.textFieldDataOut.setCaption("al");
		this.horizontalLayout9.addComponent(this.textFieldDataOut);
		this.horizontalLayout9.setExpandRatio(this.textFieldDataOut, 1.0F);
		this.horizontalLayout9.setComponentAlignment(this.textFieldDataOut, Alignment.MIDDLE_LEFT);
		this.textFieldDataIn.setValue(new Date("01/01/2017"));
		this.textFieldDataOut.setValue(new Date());
		this.spacerFilters.setSizeFull();
		this.horizontalLayout9.addComponent(this.spacerFilters);
		this.horizontalLayout9.setComponentAlignment(this.spacerFilters, Alignment.MIDDLE_LEFT);
		this.horizontalLayout9.setExpandRatio(this.spacerFilters, 3.6F);
		
//		this.textFieldDataInGosth2.setSizeUndefined();
//		this.horizontalLayout9.addComponent(this.textFieldDataInGosth2);
//		this.horizontalLayout9.setComponentAlignment(this.textFieldDataInGosth2, Alignment.MIDDLE_LEFT);
//		this.labele2.setSizeUndefined();
//		this.horizontalLayout9.addComponent(this.labele2);
//		this.horizontalLayout9.setComponentAlignment(this.labele2, Alignment.TOP_CENTER);
//		this.textFieldDataOutGosth2.setSizeUndefined();
//		this.horizontalLayout9.addComponent(this.textFieldDataOutGosth2);
//		this.horizontalLayout9.setComponentAlignment(this.textFieldDataOutGosth2, Alignment.MIDDLE_LEFT);
//		final CustomComponent horizontalLayout9_spacer = new CustomComponent();
//		horizontalLayout9_spacer.setSizeFull();
//		this.horizontalLayout9.addComponent(horizontalLayout9_spacer);
//		this.horizontalLayout9.setExpandRatio(horizontalLayout9_spacer, 1.0F);
		//this.labelRagioneSociale.setSizeUndefined();
		//this.horizontalLayout6.addComponent(this.labelRagioneSociale);
		//this.horizontalLayout6.setComponentAlignment(this.labelRagioneSociale, Alignment.TOP_RIGHT);
		this.buttonFiltra.setSizeUndefined();
		this.buttonResetFiltri.setSizeUndefined();
		this.horizontalLayout7.addComponent(this.buttonFiltra);
		this.horizontalLayout7.addComponent(this.buttonResetFiltri);


		this.horizontalLayout7.setMargin(new MarginInfo(false, true, true, true));
		final CustomComponent horizontalLayout7_spacer = new CustomComponent();
		horizontalLayout7_spacer.setSizeFull();
		this.horizontalLayout7.addComponent(horizontalLayout7_spacer);
		this.horizontalLayout7.setExpandRatio(horizontalLayout7_spacer, 1.0F);
		this.horizontalLayout9.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout9.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout9);
		this.horizontalLayout6.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout6.setHeight(-1, Unit.PIXELS);
		//this.verticalLayout3.addComponent(this.horizontalLayout6);
		this.horizontalLayout7.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout3.addComponent(this.horizontalLayout7);
		final CustomComponent verticalLayout3_spacer = new CustomComponent();
		verticalLayout3_spacer.setSizeFull();
		this.verticalLayout3.addComponent(verticalLayout3_spacer);
		this.verticalLayout3.setExpandRatio(verticalLayout3_spacer, 1.0F);
		this.verticalLayout3.setSizeFull();
		this.panelFiltri.setContent(this.verticalLayout3);
		this.tablePreventivi.setSizeFull();
		this.horizontalLayout2.addComponent(this.tablePreventivi);
		this.horizontalLayout2.setComponentAlignment(this.tablePreventivi, Alignment.MIDDLE_CENTER);
		this.horizontalLayout2.setExpandRatio(this.tablePreventivi, 10.0F);
		this.horizontalLayout2.setSizeFull();
		this.panel2.setContent(this.horizontalLayout2);
		this.buttonNuovoPreventivo.setSizeFull();
		this.buttonModificaPreventivo.setSizeFull();
		this.buttonDuplicaPreventivo.setSizeFull();
		this.buttonSalvaPreventivo.setSizeFull();
		this.buttonCancellaPreventivo.setSizeFull();
		
		this.horizontalLayout3.addComponent(this.buttonNuovoPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonNuovoPreventivo, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.buttonNuovoPreventivo, 10.0F);
		
		this.horizontalLayout3.addComponent(this.buttonModificaPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonModificaPreventivo, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.buttonModificaPreventivo, 10.0F);

		this.horizontalLayout3.addComponent(this.buttonCancellaPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonCancellaPreventivo, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.buttonCancellaPreventivo, 10.0F);

		this.horizontalLayout3.addComponent(this.buttonDuplicaPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonDuplicaPreventivo, Alignment.MIDDLE_LEFT);
		this.horizontalLayout3.setExpandRatio(this.buttonDuplicaPreventivo, 10.0F);

		this.horizontalLayout3.addComponent(this.buttonSalvaPreventivo);
		this.horizontalLayout3.setComponentAlignment(this.buttonSalvaPreventivo, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.buttonSalvaPreventivo, 10.0F);

		this.horizontalLayout3.setSizeFull();
		
		this.panel3.setContent(this.horizontalLayout3);
		this.buttonVentilatoreUp.setWidth(64, Unit.PIXELS);
		this.buttonVentilatoreUp.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.buttonVentilatoreUp);
		this.verticalLayout2.setExpandRatio(this.buttonVentilatoreUp, 10.0F);
		this.buttonVentilatoreDown.setWidth(64, Unit.PIXELS);
		this.buttonVentilatoreDown.setHeight(-1, Unit.PIXELS);
		this.verticalLayout2.addComponent(this.buttonVentilatoreDown);
		this.verticalLayout2.setComponentAlignment(this.buttonVentilatoreDown, Alignment.BOTTOM_LEFT);
		this.verticalLayout2.setExpandRatio(this.buttonVentilatoreDown, 10.0F);
		this.verticalLayout2.setSizeUndefined();
		this.horizontalLayout.addComponent(this.verticalLayout2);
		this.horizontalLayout.setComponentAlignment(this.verticalLayout2, Alignment.MIDDLE_CENTER);
		this.tableVentilatori.setSizeFull();
		this.horizontalLayout.addComponent(this.tableVentilatori);
		this.horizontalLayout.setComponentAlignment(this.tableVentilatori, Alignment.MIDDLE_CENTER);
		this.horizontalLayout.setExpandRatio(this.tableVentilatori, 10.0F);
		this.horizontalLayout.setSizeFull();
		this.panel4.setContent(this.horizontalLayout);
		
		
		this.buttonResetPreventivo.setSizeFull();
		this.horizontalLayout4.addComponent(this.buttonResetPreventivo);
		this.horizontalLayout4.setComponentAlignment(this.buttonResetPreventivo, Alignment.MIDDLE_LEFT);
		this.horizontalLayout4.setExpandRatio(this.buttonResetPreventivo, 20.0F);
		this.buttonCancellaVentilatore.setSizeFull();
		this.horizontalLayout4.addComponent(this.buttonCancellaVentilatore);
		this.horizontalLayout4.setComponentAlignment(this.buttonCancellaVentilatore, Alignment.MIDDLE_LEFT);
		this.horizontalLayout4.setExpandRatio(this.buttonCancellaVentilatore, 20.0F);
		
		this.spacerLowerMenu.setSizeFull();
		this.horizontalLayout4.addComponent(this.spacerLowerMenu);
		this.horizontalLayout4.setComponentAlignment(this.spacerLowerMenu, Alignment.MIDDLE_LEFT);
		this.horizontalLayout4.setExpandRatio(this.spacerLowerMenu, 25.0F);
		
		this.buttonAzione0.setSizeFull();
		this.horizontalLayout4.addComponent(this.buttonAzione0);
		this.horizontalLayout4.setComponentAlignment(this.buttonAzione0, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout4.setExpandRatio(this.buttonAzione0, 10.0F);
		this.buttonAzione1.setSizeFull();
		this.horizontalLayout4.addComponent(this.buttonAzione1);
		this.horizontalLayout4.setComponentAlignment(this.buttonAzione1, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout4.setExpandRatio(this.buttonAzione1, 10.0F);
		this.buttonAzione2.setSizeFull();
		this.horizontalLayout4.addComponent(this.buttonAzione2);
		this.horizontalLayout4.setComponentAlignment(this.buttonAzione2, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout4.setExpandRatio(this.buttonAzione2, 10.0F);
		this.horizontalLayout4.setSizeFull();
		
		this.panel5.setContent(this.horizontalLayout4);
		this.horizontalLayout4.setSpacing(true);
		
		this.buttonCambiaMotore.setSizeFull();
		this.horizontalLayout8.addComponent(this.buttonCambiaMotore);
		this.horizontalLayout8.setComponentAlignment(this.buttonCambiaMotore, Alignment.MIDDLE_LEFT);
		this.horizontalLayout8.setExpandRatio(this.buttonCambiaMotore, 6.0F);
		this.spacerFinal.setSizeFull();
		this.horizontalLayout8.addComponent(this.spacerFinal);
		this.horizontalLayout8.setComponentAlignment(this.spacerFinal, Alignment.MIDDLE_LEFT);
		this.horizontalLayout8.setExpandRatio(this.spacerFinal, 25.0F);
		this.buttonGeneraPreventivo.setSizeFull();
		this.horizontalLayout8.addComponent(this.buttonGeneraPreventivo);
		this.horizontalLayout8.setComponentAlignment(this.buttonGeneraPreventivo, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout8.setExpandRatio(this.buttonGeneraPreventivo, 6.0F);
		this.buttonDownloadPreventivo.setSizeFull();
		this.horizontalLayout8.addComponent(this.buttonDownloadPreventivo);
		this.horizontalLayout8.setComponentAlignment(this.buttonDownloadPreventivo, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout8.setExpandRatio(this.buttonDownloadPreventivo, 6.0F);
		this.horizontalLayout8.setSizeFull();
		this.panel7.setContent(this.horizontalLayout8);
		
		
		this.panelFiltri.setWidth(100, Unit.PERCENTAGE);
		this.panelFiltri.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panelFiltri);
		this.panel2.setSizeFull();
		this.verticalLayout.addComponent(this.panel2);
		this.verticalLayout.setExpandRatio(this.panel2, 10.0F);
		this.panel3.setWidth(100, Unit.PERCENTAGE);
		this.panel3.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panel3);
		this.panel4.setSizeFull();
		this.verticalLayout.addComponent(this.panel4);
		this.verticalLayout.setExpandRatio(this.panel4, 10.0F);
		this.panel5.setWidth(100, Unit.PERCENTAGE);
		this.panel5.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panel5);
		this.panel7.setWidth(100, Unit.PERCENTAGE);
		this.panel7.setHeight(-1, Unit.PIXELS);
		this.verticalLayout.addComponent(this.panel7);
		this.verticalLayout.setSizeFull();
		this.setContent(this.verticalLayout);
		this.setSizeFull();
	
		this.buttonResetFiltri.addClickListener(event -> this.buttonResetFiltri_buttonClick(event));
		this.buttonFiltra.addClickListener(event -> this.buttonFiltra_buttonClick(event));
		this.tablePreventivi.addValueChangeListener(event -> this.tablePreventivi_valueChange(event));
		this.buttonModificaPreventivo.addClickListener(event -> this.buttonModificaPreventivo_buttonClick(event));
		this.buttonDuplicaPreventivo.addClickListener(event -> this.buttonDuplicaPreventivo_buttonClick(event));
		this.buttonChiudiPreventivo.addClickListener(event -> this.buttonChiudiPreventivo_buttonClick(event));
		this.buttonSalvaPreventivo.addClickListener(event -> this.buttonSalvaPreventivo_buttonClick(event));
		this.buttonCancellaPreventivo.addClickListener(event -> this.buttonCancellaPreventivo_buttonClick(event));
		this.buttonNuovoPreventivo.addClickListener(event -> this.buttonNuovoPreventivo_buttonClick(event));
		this.buttonVentilatoreUp.addClickListener(event -> this.buttonVentilatoreUp_buttonClick(event));
		this.buttonVentilatoreDown.addClickListener(event -> this.buttonVentilatoreDown_buttonClick(event));
		this.tableVentilatori.addValueChangeListener(event -> this.tableVentilatori_valueChange(event));
		this.buttonResetPreventivo.addClickListener(event -> this.buttonResetPreventivo_buttonClick(event));
		this.buttonCancellaVentilatore.addClickListener(event -> this.buttonCancellaVentilatore_buttonClick(event));
		this.buttonAzione0.addClickListener(event -> this.buttonAzione0_buttonClick(event));
		this.buttonAzione1.addClickListener(event -> this.buttonAzione1_buttonClick(event));
		this.buttonAzione2.addClickListener(event -> this.buttonAzione2_buttonClick(event));
		this.buttonCambiaMotore.addClickListener(event -> this.buttonCambiaMotore_buttonClick(event));
		this.buttonGeneraPreventivo.addClickListener(event -> this.buttonGeneraPreventivo_buttonClick(event));
		this.buttonDownloadPreventivo.addClickListener(event -> this.buttonDownloadPreventivo_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevLabel labelCodiceCliente2, labelDataCompresa2, labelTra2, labele2, labelRagioneSociale, spacerLowerMenu, spacerFinal, spacerFilters;
	private XdevButton buttonResetFiltri, buttonFiltra, buttonModificaPreventivo, buttonDuplicaPreventivo,
			buttonChiudiPreventivo, buttonCancellaPreventivo, buttonSalvaPreventivo, buttonNuovoPreventivo, buttonVentilatoreUp,
			buttonVentilatoreDown, buttonResetPreventivo, buttonCancellaVentilatore, buttonAzione0, buttonAzione1,
			buttonAzione2, buttonCambiaMotore, buttonGeneraPreventivo, buttonDownloadPreventivo;
	private XdevHorizontalLayout horizontalLayout9, horizontalLayout6, horizontalLayout7, horizontalLayout2,
			horizontalLayout3, horizontalLayout, horizontalLayout4, horizontalLayout8;
	private XdevTable<CustomComponent> tablePreventivi, tableVentilatori;
	private XdevPanel panelFiltri, panel2, panel3, panel4, panel5, panel7;
	private XdevTextField textFieldCodiceClienteGosth2, textFieldDataInGosth2, textFieldDataOutGosth2,
			textFieldRagioneSociale;
	private XdevVerticalLayout verticalLayout, verticalLayout3, verticalLayout2;
	// </generated-code>
	
}