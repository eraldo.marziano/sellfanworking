package com.cit.sellfan.ui.view;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.pannelli.pannelloPreferenzeMultigiri;
import com.cit.sellfan.ui.template.jDisplayImageWeb;
import com.cit.sellfan.ui.template.jGraficoXYWeb;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevColorPicker;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;

import cit.sellfan.classi.PuntiLavoro;
import cit.sellfan.classi.titoli.StringheUNI;
import cit.sellfan.classi.ventilatore.Ventilatore;
import de.steinwedel.messagebox.MessageBox;

public class MultigiriView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
	public jGraficoXYWeb grafico = new jGraficoXYWeb();
	private final jDisplayImageWeb imagelogoweb = new jDisplayImageWeb();
	private final XdevTextField plName[] = new XdevTextField[8];
	private final TextFieldDouble plQ[] = new TextFieldDouble[8];
	private final TextFieldDouble plPT[] = new TextFieldDouble[8];
	private final TextFieldDouble plPS[] = new TextFieldDouble[8];
	private final XdevColorPicker plC[] = new XdevColorPicker[8];
    private final double rpm[] = new double[8];
    private final double potenza[] = new double[8];
    private final double rendimento[] = new double[8];
    private final double potenzaSonora[] = new double[8];
    private final double pressioneSonora[] = new double[8];
	/**
	 * 
	 */
	public MultigiriView() {
		super();
		this.initUI();
	}
	
	public void setMemo()
	{
		this.l_VG.multigiri.note[0] = this.textFieldNote0.getValue();
		this.l_VG.multigiri.note[1] = this.textFieldNote1.getValue();
		this.l_VG.multigiri.note[2] = this.textFieldNote2.getValue();
		this.l_VG.multigiri.note[3] = this.textFieldNote3.getValue();
		this.l_VG.multigiri.note[4] = this.textFieldNote4.getValue();
		this.l_VG.multigiri.note[5] = this.textFieldNote5.getValue();
	}

	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoView[this.l_VG.MultigiriViewIndex] = this;
		String id;
		for (int i=0 ; i<8 ; i++) {
			id = Integer.toString(i);
			this.plQ[i] = new TextFieldDouble();
			this.plQ[i].setData(id);
			this.plQ[i].setWidth(100, Unit.PERCENTAGE);
			this.plQ[i].setHeight(-1, Unit.PIXELS);
			this.gridLayout2.removeComponent(1, i + 2);
			this.gridLayout2.addComponent(this.plQ[i], 1, i + 2);
			this.plPT[i] = new TextFieldDouble();
			this.plPT[i].setData(id);
			this.plPT[i].setWidth(100, Unit.PERCENTAGE);
			this.plPT[i].setHeight(-1, Unit.PIXELS);
			this.gridLayout2.removeComponent(2, i + 2);
			this.gridLayout2.addComponent(this.plPT[i], 2, i + 2);
			this.plPS[i] = new TextFieldDouble();
			this.plPS[i].setData(id);
			this.plPS[i].setWidth(100, Unit.PERCENTAGE);
			this.plPS[i].setHeight(-1, Unit.PIXELS);
			this.gridLayout2.removeComponent(3, i + 2);
			this.gridLayout2.addComponent(this.plPS[i], 3, i + 2);
			this.plC[i] = new XdevColorPicker();
			this.plC[i].setColor(com.vaadin.shared.ui.colorpicker.Color.BLACK);
			this.plC[i].setWidth(40, Unit.PIXELS);
			this.plC[i].setHeight(27, Unit.PIXELS);
			this.gridLayout2.removeComponent(4, i + 2);
			this.gridLayout2.addComponent(this.plC[i], 4, i + 2);
			switch (i) {
			case 1:
				this.plQ[i].addTextChangeListener(event -> this.textFieldQ1Gosth_textChange(event));
				this.plPT[i].addTextChangeListener(event -> this.textFieldPT1Gosth_textChange(event));
				this.plPS[i].addTextChangeListener(event -> this.textFieldPS1Gosth_textChange(event));
				break;
			case 2:
				this.plQ[i].addTextChangeListener(event -> this.textFieldQ2Gosth_textChange(event));
				this.plPT[i].addTextChangeListener(event -> this.textFieldPT2Gosth_textChange(event));
				this.plPS[i].addTextChangeListener(event -> this.textFieldPS2Gosth_textChange(event));
				break;
			case 3:
				this.plQ[i].addTextChangeListener(event -> this.textFieldQ3Gosth_textChange(event));
				this.plPT[i].addTextChangeListener(event -> this.textFieldPT3Gosth_textChange(event));
				this.plPS[i].addTextChangeListener(event -> this.textFieldPS3Gosth_textChange(event));
				break;
			case 4:
				this.plQ[i].addTextChangeListener(event -> this.textFieldQ4Gosth_textChange(event));
				this.plPT[i].addTextChangeListener(event -> this.textFieldPT4Gosth_textChange(event));
				this.plPS[i].addTextChangeListener(event -> this.textFieldPS4Gosth_textChange(event));
				break;
			case 5:
				this.plQ[i].addTextChangeListener(event -> this.textFieldQ5Gosth_textChange(event));
				this.plPT[i].addTextChangeListener(event -> this.textFieldPT5Gosth_textChange(event));
				this.plPS[i].addTextChangeListener(event -> this.textFieldPS5Gosth_textChange(event));
				break;
			case 6:
				this.plQ[i].addTextChangeListener(event -> this.textFieldQ6Gosth_textChange(event));
				this.plPT[i].addTextChangeListener(event -> this.textFieldPT6Gosth_textChange(event));
				this.plPS[i].addTextChangeListener(event -> this.textFieldPS6Gosth_textChange(event));
				break;
			case 7:
				this.plQ[i].addTextChangeListener(event -> this.textFieldQ7Gosth_textChange(event));
				this.plPT[i].addTextChangeListener(event -> this.textFieldPT7Gosth_textChange(event));
				this.plPS[i].addTextChangeListener(event -> this.textFieldPS7Gosth_textChange(event));
				break;
			}
		}
		int index = 0;
		this.plName[index++] = this.textFieldN0;
		this.plName[index++] = this.textFieldN1;
		this.plName[index++] = this.textFieldN2;
		this.plName[index++] = this.textFieldN3;
		this.plName[index++] = this.textFieldN4;
		this.plName[index++] = this.textFieldN5;
		this.plName[index++] = this.textFieldN6;
		this.plName[index++] = this.textFieldN7;
		try {
	        final FileInputStream fileIn = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logo.png");
	        this.imagelogoweb.DisplayfromInputStream(fileIn);
			final StreamResource logosource = new StreamResource(this.imagelogoweb, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
			logosource.setCacheTime(100);
			this.imageLogo.setSource(logosource);
		} catch (final Exception e) {
			
		}
		this.labelQMax.setValue(StringheUNI.PORTATA + " max");
		this.labelPtMax.setValue(StringheUNI.PRESSIONETOTALE + " max");
	}
	
	public void Nazionalizza() {
		this.checkBoxAsprirazione.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aspirazione"));
		this.checkBoxMandata.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Mandata"));
		this.checkBoxCurvaCatarreristica.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Curva Caratteristica"));
		this.verticalLayoutPL.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Punti di lavoro"));
		this.checkBoxPT.setCaption(StringheUNI.PRESSIONETOTALE);
		this.checkBoxPS.setCaption(StringheUNI.PRESSIONESTATICA);
		this.labelNome.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Nome"));
		this.labelPortata.setValue(StringheUNI.PORTATA);
		this.labelPT.setValue(StringheUNI.PRESSIONETOTALE);
		this.labelPS.setValue(StringheUNI.PRESSIONESTATICA);
		this.buttonReset.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Reset"));
		this.buttonRidisegna.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ridisegna"));
		this.verticalLayoutNote.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Note"));
		this.buttonPreferenze.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Preferenze"));
	}
	
	private void resetpl() {
		for (int i=0 ; i<8 ; i++) {
			this.plName[i].setValue("");
			this.plQ[i].setValue("");
			this.plQ[i].setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
			this.plPT[i].setEnabled(false);
			this.plPS[i].setEnabled(false);
			this.plPT[i].setValue("");
			this.plPS[i].setValue("");
			this.plPT[i].setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPressione.nDecimaliStampa);
			this.plPS[i].setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPressione.nDecimaliStampa);
		}
		this.plName[0].setValue("Design");
		this.plQ[0].setEnabled(false);
		if (this.checkBoxPT.getValue()) {
			for (int i=1 ; i<8 ; i++) {
				this.plPT[i].setEnabled(true);
			}
		} else {
			for (int i=1 ; i<8 ; i++) {
				this.plPS[i].setEnabled(true);
			}
		}
		this.plQ[0].setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
		this.plPT[0].setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione);
		this.plPS[0].setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione);
		this.textFieldDoubleQMax.setValue("");
		this.textFieldDoublePtMax.setValue("");
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		if (this.l_VG == null || !this.l_VG.utilityCliente.isTabVisible("MultiGiri") || this.l_VG.VentilatoreCurrent == null || this.l_VG.ventilatoreFisicaCurrent == null || this.l_VG.multigiri == null) {
			return false;
		}
		return true;
	}
	
	public void setVentilatoreCurrent() {
		this.verticalLayoutParametri.setVisible(!this.l_VG.MobileMode);
		this.isPannelloInizializzato = false;
		if (!this.l_VG.utilityCliente.isTabVisible("MultiGiri") || this.l_VG.VentilatoreCurrent == null) {
			return;
		}
		this.l_VG.MainView.aggiornaGrafico();
		this.checkBoxPT.setEnabled(false);
		this.checkBoxPS.setEnabled(false);
		this.checkBoxPT.setValue(this.l_VG.multigiri.puntiLavoro.ptFlag);
		this.checkBoxPS.setValue(!this.l_VG.multigiri.puntiLavoro.ptFlag);
		this.checkBoxPT.setEnabled(true);
		this.checkBoxPS.setEnabled(true);
		//Notification.show("setVentilatoreCurrent");
		//resetpl();
                if (this.l_VG.multigiri.puntiLavoro.titoliPL[0] == null || this.l_VG.multigiri.puntiLavoro.titoliPL[0].equals(""))
                    this.plName[0].setValue("Design");
		this.plQ[0].setEnabled(false);
		this.plPT[0].setEnabled(false);
		this.plPS[0].setEnabled(false);
		if (this.checkBoxPT.getValue()) {
			for (int i=1 ; i<8 ; i++) {
				this.plPT[i].setEnabled(true);
				this.plPS[i].setEnabled(false);
			}
		} else {
			for (int i=1 ; i<8 ; i++) {
				this.plPS[i].setEnabled(true);
				this.plPT[i].setEnabled(false);
			}
		}
		this.plQ[0].setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker / this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione);
		this.plPT[0].setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione);
		this.plPS[0].setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione);
		for (int i = 0; i<6; i++) {
			this.l_VG.multigiri.note[i] = "";
		}
		this.textFieldNote0.setValue(this.l_VG.multigiri.note[0]);
		this.textFieldNote1.setValue(this.l_VG.multigiri.note[1]);
		this.textFieldNote2.setValue(this.l_VG.multigiri.note[2]);
		this.textFieldNote3.setValue(this.l_VG.multigiri.note[3]);
		this.textFieldNote4.setValue(this.l_VG.multigiri.note[4]);
		this.textFieldNote5.setValue(this.l_VG.multigiri.note[5]);
		this.labelPortataUM.setValue(this.l_VG.VentilatoreCurrent.UMPortata.simbolo);
		this.labelPTUM.setValue(this.l_VG.VentilatoreCurrent.UMPressione.simbolo);
		this.labelPSUM.setValue(this.l_VG.VentilatoreCurrent.UMPressione.simbolo);
        this.l_VG.multigiri.multigiriFromRicerca = this.l_VG.VentilatoreCurrent.FromRicerca;
        this.l_VG.multigiri.multigiriPortataRichiestam3h = this.l_VG.VentilatoreCurrent.PortataRichiestam3h;
        this.l_VG.multigiri.multigiriPressioneRichiestaPa = this.l_VG.VentilatoreCurrent.PressioneRichiestaPa;
        this.l_VG.multigiri.multigiriRedMarker = this.l_VG.VentilatoreCurrent.selezioneCorrente.RedMarker;
        this.checkBoxAsprirazione.setValue(!this.l_VG.VentilatoreCurrent.PressioneMandataFlag);
        this.checkBoxMandata.setValue(this.l_VG.VentilatoreCurrent.PressioneMandataFlag);
        if (!this.l_VG.VentilatoreCurrent.DatiCompleti) {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
        }
       	this.l_VG.currentGraficoPrestazioniMultigiri.init(this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.utilityCliente, this.l_VG.utilityTraduzioni, this.l_VG.currentCitFont.linguaDisplayFont);
        final ArrayList<Ventilatore> elencoVentilatoriSimili = this.l_VG.loadVentilatoriSimiliForMultigiri(this.l_VG.VentilatoreCurrent);
        this.l_VG.currentGraficoPrestazioniMultigiri.initVentilatoriSimili(elencoVentilatoriSimili);
//        l_VG.currentGraficoPrestazioniMultigiri.initDBTecnico(l_VG.dbTecnico, l_VG.nTemperatureLimite, l_VG.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, l_VG.CACatalogo, l_VG.CA020Catalogo, l_VG.idTranscodificaIndex);
        this.l_VG.multigiri.QMax = this.textFieldDoubleQMax.getDoubleValue();
        this.l_VG.multigiri.PtMax = this.textFieldDoublePtMax.getDoubleValue();
        if (this.l_VG.currentGraficoPrestazioniMultigiri.initValoriMultigiro(this.l_VG.VentilatoreCurrent, this.l_VG.ventilatoreFisicaCurrent, this.l_VG.multigiri, false)) {
        	this.l_VG.multigiri.multigiriDiametroPL = 4;
        	this.grafico.ResetGrafico();
            this.l_VG.currentGraficoPrestazioniMultigiri.buildGrafico(this.l_VG.assiGraficiDefault, this.grafico);
            try {
                if (this.l_VG.sfondoGraficiPrestazioni != null && this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
                    final InputStream insfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                    this.grafico.setImgSfondo(insfondo);
                } else {
                	this.grafico.setImgSfondo(null);
                }
            } catch (final Exception e) {
            	
            }
            aggiornaGrafico();
        } else {
            this.grafico.ResetGrafico();
        }
        this.labelQMaxUM.setValue(this.l_VG.VentilatoreCurrent.UMPortata.simbolo);
        this.textFieldDoubleQMax.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPortata.nDecimaliStampa);
        this.labelPtMaxUM.setValue(this.l_VG.VentilatoreCurrent.UMPressione.simbolo);
        this.textFieldDoublePtMax.setMaximumFractionDigits(this.l_VG.VentilatoreCurrent.UMPressione.nDecimaliStampa);
        this.isPannelloInizializzato = true;
 	}
	
    private void aggiornaGrafico() {
    	loadPl();
        this.l_VG.currentGraficoPrestazioniMultigiri.setPuntiLavoro(this.checkBoxCurvaCatarreristica.getValue());
        this.grafico.repaint();
        final StreamResource graficosource = new StreamResource(this.grafico, "myimage" + Integer.toString(this.l_VG.progressivoGenerico++) + ".png");
		graficosource.setCacheTime(100);
		this.image.setSource(graficosource);
    }
    
    private void loadPl() {
    	final PuntiLavoro puntiLavoro = this.l_VG.multigiri.puntiLavoro;
    	puntiLavoro.ptFlag = this.checkBoxPT.getValue();
    	puntiLavoro.okPL[0] = true;
    	puntiLavoro.titoliPL[0] = this.plName[0].getValue();
    	puntiLavoro.portatePLm3h[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.Marker;
    	puntiLavoro.pressioniTotPLPa[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneTotale;
    	puntiLavoro.pressioniStatPLPa[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneStatica;
    	puntiLavoro.colorePL[0] = new Color(this.plC[0].getColor().getRed(), this.plC[0].getColor().getGreen(), this.plC[0].getColor().getBlue());
    	puntiLavoro.rpmPL[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri;
    	puntiLavoro.potenzaPLW[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.Potenza;
    	puntiLavoro.rendimentoPL[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.Rendimento;
    	puntiLavoro.potenzaSonoraPL[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.PotenzaSonora;
    	puntiLavoro.pressioneSonoraPL[0] = this.l_VG.VentilatoreCurrent.selezioneCorrente.PressioneSonora;
    	for (int i=1 ; i<8 ; i++) {
    		puntiLavoro.okPL[i] = false;
            if (puntiLavoro.ptFlag && this.plPT[i].getDoubleValue() <= 0.) {
				continue;
			} else if (!puntiLavoro.ptFlag && this.plPS[i].getDoubleValue() <= 0.) {
				continue;
			}
            puntiLavoro.okPL[i] = true;
            puntiLavoro.titoliPL[i] = this.plName[i].getValue();
            puntiLavoro.portatePLm3h[i] = this.plQ[i].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione;
            puntiLavoro.pressioniTotPLPa[i] = this.plPT[i].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione;
            puntiLavoro.pressioniStatPLPa[i] = this.plPS[i].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione;
        	puntiLavoro.colorePL[i] = new Color(this.plC[i].getColor().getRed(), this.plC[i].getColor().getGreen(), this.plC[i].getColor().getBlue());
            puntiLavoro.rpmPL[i] = this.rpm[i];
            puntiLavoro.potenzaPLW[i] = this.potenza[i];
            puntiLavoro.rendimentoPL[i] = this.rendimento[i];
            puntiLavoro.potenzaSonoraPL[i] = this.potenzaSonora[i];
            puntiLavoro.pressioneSonoraPL[i] = this.pressioneSonora[i];
    	}
    }
    
    private void cambiaPortataPl(final int index) {
    	if (!this.plQ[index].isEnabled()) {
			return;
		}
    	if (this.checkBoxPT.getValue()) {
            if (this.plPT[index].getDoubleValue() <= 0.) {
                this.plPT[index].setValue("");
                return;
            }
            try {
                if (cercaPressioneTotale(this.plQ[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione, this.plPT[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione)) {
                    this.plPS[index].setValue(this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaPa() / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione);
                    this.rpm[index] = this.l_VG.ventilatoreFisicaCurrent.getNumeroGiri();
                    this.potenza[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaW();
                    this.rendimento[index] = this.l_VG.ventilatoreFisicaCurrent.getRendimento();
                    this.potenzaSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaSonora();
                    this.pressioneSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPressioneSonora();
                } else {
                    this.plPS[index].setValue("");
                }
            } catch (final Exception e) {
                
            }
    	} else {
            if (this.plPS[index].getDoubleValue() <= 0.) {
                this.plPT[index].setValue("");
                return;
            }
            try {
                if (cercaPressioneStatica(this.plQ[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione, this.plPS[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione)) {
                    this.plPT[index].setValue(this.l_VG.ventilatoreFisicaCurrent.getPressioneTotalePa() / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione);
                    this.rpm[index] = this.l_VG.ventilatoreFisicaCurrent.getNumeroGiri();
                    this.potenza[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaW();
                    this.rendimento[index] = this.l_VG.ventilatoreFisicaCurrent.getRendimento();
                    this.potenzaSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaSonora();
                    this.pressioneSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPressioneSonora();
                } else {
                    this.plPT[index].setValue("");
                }
            } catch (final Exception e) {
                
            }
    	}
    }
    
    private void cambiaPtPl(final int index) {
    	if (!this.plPT[index].isEnabled()) {
			return;
		}
        try {
            if (cercaPressioneTotale(this.plQ[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione, this.plPT[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione)) {
                final double p = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaPa() / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione;
                if (p > 0.) {
                    this.plPS[index].setValue(p);
                } else {
                    this.plPS[index].setValue("");
                }
                this.rpm[index] = this.l_VG.ventilatoreFisicaCurrent.getNumeroGiri();
                this.potenza[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaW();
                this.rendimento[index] = this.l_VG.ventilatoreFisicaCurrent.getRendimento();
                this.potenzaSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaSonora();
                this.pressioneSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPressioneSonora();
            } else {
                this.plPS[index].setValue("");
            }
        } catch (final Exception e) {
            
        }
    }

    private void cambiaPsPl(final int index) {
        try {
            if (cercaPressioneStatica(this.plQ[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPortata.fattoreConversione, this.plPS[index].getDoubleValue() * this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione)) {
                this.plPT[index].setValue(this.l_VG.ventilatoreFisicaCurrent.getPressioneTotalePa() / this.l_VG.VentilatoreCurrent.UMPressione.fattoreConversione);
                this.rpm[index] = this.l_VG.ventilatoreFisicaCurrent.getNumeroGiri();
                this.potenza[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaW();
                this.rendimento[index] = this.l_VG.ventilatoreFisicaCurrent.getRendimento();
                this.potenzaSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPotenzaSonora();
                this.pressioneSonora[index] = this.l_VG.ventilatoreFisicaCurrent.getPressioneSonora();
            } else {
                this.plPT[index].setValue("");
            }
        } catch (final Exception e) {
            
        }
    }
    
    private boolean cercaPressioneTotale(final double portata, final double target) {
        try {
            double rpmLoc = this.l_VG.multigiri.multigiriRpmMax;
            this.l_VG.ventilatoreFisicaCurrent.setNumeroGiriPortatam3h(rpmLoc, portata);
            double pressione = this.l_VG.ventilatoreFisicaCurrent.getPressioneTotalePa();
            if (pressione < target) {
				return false;
			}
            while (pressione > target) {
                rpmLoc -= 0.5;
                if (rpmLoc < this.l_VG.multigiri.multigiriRpmMin) {
                    return false;
                }
                this.l_VG.ventilatoreFisicaCurrent.setNumeroGiriPortatam3h(rpmLoc, portata);
                pressione = this.l_VG.ventilatoreFisicaCurrent.getPressioneTotalePa();
            }
        } catch (final Exception e) {
            return false;
        }
        return true;
    }

    private boolean cercaPressioneStatica(final double portata, final double target) {
        try {
            double rpmLoc = this.l_VG.multigiri.multigiriRpmMax;
            this.l_VG.ventilatoreFisicaCurrent.setNumeroGiriPortatam3h(rpmLoc, portata);
            double pressione = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaPa();
            if (pressione < target) {
				return false;
			}
            while (pressione > target) {
                rpmLoc -= 0.5;
                if (rpmLoc < this.l_VG.multigiri.multigiriRpmMin) {
                    return false;
                }
                this.l_VG.ventilatoreFisicaCurrent.setNumeroGiriPortatam3h(rpmLoc, portata);
                pressione = this.l_VG.ventilatoreFisicaCurrent.getPressioneStaticaPa();
            }
        } catch (final Exception e) {
            return false;
        }
        return true;
    }
    /**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxCurvaCatarreristica}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxCurvaCatarreristica_valueChange(final Property.ValueChangeEvent event) {
		this.l_VG.VentilatoreCurrent.selezioneCorrente.stampaCurvaCaratteristica = this.checkBoxCurvaCatarreristica.getValue();
		aggiornaGrafico();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPT1Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPT1Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPtPl(1);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPT2Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPT2Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPtPl(2);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPT3Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPT3Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPtPl(3);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPT4Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPT4Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPtPl(4);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPT5Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPT5Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPtPl(5);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPT6Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPT6Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPtPl(6);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPT7Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPT7Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPtPl(7);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPS1Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPS1Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPsPl(1);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPS2Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPS2Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPsPl(2);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPS3Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPS3Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPsPl(3);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPS4Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPS4Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPsPl(4);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPS5Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPS5Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPsPl(5);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPS6Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPS6Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPsPl(6);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldPS7Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldPS7Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPsPl(7);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldQ1Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQ1Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPortataPl(1);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldQ2Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQ2Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPortataPl(2);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldQ3Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQ3Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPortataPl(3);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldQ4Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQ4Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPortataPl(4);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldQ5Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQ5Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPortataPl(5);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldQ6Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQ6Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPortataPl(6);
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldQ7Gosth}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldQ7Gosth_textChange(final FieldEvents.TextChangeEvent event) {
		cambiaPortataPl(7);
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxPT}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxPT_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxPT.isEnabled()) {
			return;
		}
		final boolean pt = this.checkBoxPT.getValue();
		this.l_VG.multigiri.puntiLavoro.ptFlag = pt;
		this.checkBoxPS.setEnabled(false);
		this.checkBoxPS.setValue(!pt);
		this.checkBoxPS.setEnabled(true);
		for (int i=1 ; i<8 ; i++) {
			this.plPT[i].setEnabled(pt);
			this.plPS[i].setEnabled(!pt);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxPS}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxPS_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxPS.isEnabled()) {
			return;
		}
		final boolean ps = this.checkBoxPS.getValue();
		this.l_VG.multigiri.puntiLavoro.ptFlag = !ps;
		this.checkBoxPT.setEnabled(false);
		this.checkBoxPT.setValue(!ps);
		this.checkBoxPT.setEnabled(true);
		for (int i=1 ; i<8 ; i++) {
			this.plPT[i].setEnabled(!ps);
			this.plPS[i].setEnabled(ps);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonRidisegna}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonRidisegna_buttonClick(final Button.ClickEvent event) {
    	this.grafico.ResetGrafico();
        this.l_VG.multigiri.QMax = this.textFieldDoubleQMax.getDoubleValue();
        this.l_VG.multigiri.PtMax = this.textFieldDoublePtMax.getDoubleValue();
        this.l_VG.currentGraficoPrestazioniMultigiri.buildGrafico(this.l_VG.assiGraficiDefault, this.grafico);
        try {
            if (this.l_VG.sfondoGraficiPrestazioni != null && this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
                final InputStream insfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                this.grafico.setImgSfondo(insfondo);
            } else {
            	this.grafico.setImgSfondo(null);
            }
        } catch (final Exception e) {
        	
        }
        aggiornaGrafico();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonReset_buttonClick(final Button.ClickEvent event) {
		resetpl();
		aggiornaGrafico();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonPreferenze}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonPreferenze_buttonClick(final Button.ClickEvent event) {
		final pannelloPreferenzeMultigiri pannello = new pannelloPreferenzeMultigiri();
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.Nazionalizza();
		pannello.init();
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
		msgBox.withAbortButton();
		msgBox.withOkButton(() -> {
			pannello.getValori();
        	this.grafico.ResetGrafico();
            this.l_VG.multigiri.QMax = this.textFieldDoubleQMax.getDoubleValue();
            this.l_VG.multigiri.PtMax = this.textFieldDoublePtMax.getDoubleValue();
            this.l_VG.currentGraficoPrestazioniMultigiri.buildGrafico(this.l_VG.assiGraficiDefault, this.grafico);
            try {
                if (this.l_VG.sfondoGraficiPrestazioni != null && this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
                    final InputStream insfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                    this.grafico.setImgSfondo(insfondo);
                } else {
                	this.grafico.setImgSfondo(null);
                }
            } catch (final Exception e) {
            	
            }
            aggiornaGrafico();
			});
		msgBox.open();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNote0}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNote0_textChange(final FieldEvents.TextChangeEvent event) {
		this.l_VG.multigiri.note[1] = this.textFieldNote1.getValue();
		
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNote1}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNote1_textChange(final FieldEvents.TextChangeEvent event) {
		this.l_VG.multigiri.note[1] = this.textFieldNote1.getValue();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNote2}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNote2_textChange(final FieldEvents.TextChangeEvent event) {
		this.l_VG.multigiri.note[2] = this.textFieldNote2.getValue();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNote3}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNote3_textChange(final FieldEvents.TextChangeEvent event) {
		this.l_VG.multigiri.note[3] = this.textFieldNote3.getValue();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNote4}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNote4_textChange(final FieldEvents.TextChangeEvent event) {
		this.l_VG.multigiri.note[4] = this.textFieldNote4.getValue();
	}

	/**
	 * Event handler delegate method for the {@link XdevTextField}
	 * {@link #textFieldNote5}.
	 *
	 * @see FieldEvents.TextChangeListener#textChange(FieldEvents.TextChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void textFieldNote5_textChange(final FieldEvents.TextChangeEvent event) {
		this.l_VG.multigiri.note[5] = this.textFieldNote5.getValue();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.horizontalLayout = new XdevHorizontalLayout();
		this.verticalLayoutParametri = new XdevVerticalLayout();
		this.imageLogo = new XdevImage();
		this.panel = new XdevPanel();
		this.verticalLayoutPL = new XdevVerticalLayout();
		this.horizontalLayout3 = new XdevHorizontalLayout();
		this.checkBoxPT = new XdevCheckBox();
		this.checkBoxPS = new XdevCheckBox();
		this.gridLayout2 = new XdevGridLayout();
		this.labelNome = new XdevLabel();
		this.labelPortata = new XdevLabel();
		this.labelPT = new XdevLabel();
		this.labelPS = new XdevLabel();
		this.labelPortataUM = new XdevLabel();
		this.labelPTUM = new XdevLabel();
		this.labelPSUM = new XdevLabel();
		this.textFieldN0 = new XdevTextField();
		this.textFieldQ0Gosth = new XdevTextField();
		this.textFieldPT0Gosth = new XdevTextField();
		this.textFieldPS0Gonsth = new XdevTextField();
		this.colorPicker0 = new XdevColorPicker();
		this.textFieldN1 = new XdevTextField();
		this.textFieldQ1Gosth = new XdevTextField();
		this.textFieldPT1Gosth = new XdevTextField();
		this.textFieldPS1Gosth = new XdevTextField();
		this.colorPicker1 = new XdevColorPicker();
		this.textFieldN2 = new XdevTextField();
		this.textFieldQ2Gosth = new XdevTextField();
		this.textFieldPT2Gosth = new XdevTextField();
		this.textFieldPS2Gosth = new XdevTextField();
		this.colorPicker2 = new XdevColorPicker();
		this.textFieldN3 = new XdevTextField();
		this.textFieldQ3Gosth = new XdevTextField();
		this.textFieldPT3Gosth = new XdevTextField();
		this.textFieldPS3Gosth = new XdevTextField();
		this.colorPicker3 = new XdevColorPicker();
		this.textFieldN4 = new XdevTextField();
		this.textFieldQ4Gosth = new XdevTextField();
		this.textFieldPT4Gosth = new XdevTextField();
		this.textFieldPS4Gosth = new XdevTextField();
		this.colorPicker4 = new XdevColorPicker();
		this.textFieldN5 = new XdevTextField();
		this.textFieldQ5Gosth = new XdevTextField();
		this.textFieldPT5Gosth = new XdevTextField();
		this.textFieldPS5Gosth = new XdevTextField();
		this.colorPicker55 = new XdevColorPicker();
		this.textFieldN6 = new XdevTextField();
		this.textFieldQ6Gosth = new XdevTextField();
		this.textFieldPT6Gosth = new XdevTextField();
		this.textFieldPS6Gosth = new XdevTextField();
		this.colorPicker6 = new XdevColorPicker();
		this.textFieldN7 = new XdevTextField();
		this.textFieldQ7Gosth = new XdevTextField();
		this.textFieldPT7Gosth = new XdevTextField();
		this.textFieldPS7Gosth = new XdevTextField();
		this.colorPicker7 = new XdevColorPicker();
		this.horizontalLayout4 = new XdevHorizontalLayout();
		this.buttonReset = new XdevButton();
		this.buttonRidisegna = new XdevButton();
		this.gridLayout = new XdevGridLayout();
		this.checkBoxAsprirazione = new XdevCheckBox();
		this.checkBoxMandata = new XdevCheckBox();
		this.checkBoxCurvaCatarreristica = new XdevCheckBox();
		this.gridLayout3 = new XdevGridLayout();
		this.labelQMax = new XdevLabel();
		this.textFieldDoubleQMax = new TextFieldDouble();
		this.labelQMaxUM = new XdevLabel();
		this.labelPtMax = new XdevLabel();
		this.textFieldDoublePtMax = new TextFieldDouble();
		this.labelPtMaxUM = new XdevLabel();
		this.verticalLayoutNote = new XdevVerticalLayout();
		this.textFieldNote0 = new XdevTextField();
		this.textFieldNote1 = new XdevTextField();
		this.textFieldNote2 = new XdevTextField();
		this.textFieldNote3 = new XdevTextField();
		this.textFieldNote4 = new XdevTextField();
		this.textFieldNote5 = new XdevTextField();
		this.buttonPreferenze = new XdevButton();
		this.horizontalLayoutImmagine = new XdevHorizontalLayout();
		this.image = new XdevImage();
	
		this.verticalLayoutParametri.setMargin(new MarginInfo(false));
		this.imageLogo.setCaption("");
		this.verticalLayoutPL.setCaption("Punti di lavoro");
		this.horizontalLayout3.setMargin(new MarginInfo(false));
		this.checkBoxPT.setCaption("PT");
		this.checkBoxPT.setCaptionAsHtml(true);
		this.checkBoxPS.setCaption("PS");
		this.checkBoxPS.setCaptionAsHtml(true);
		this.gridLayout2.setSpacing(false);
		this.gridLayout2.setMargin(new MarginInfo(false));
		this.labelNome.setValue("Nome");
		this.labelPortata.setValue("Q");
		this.labelPortata.setContentMode(ContentMode.HTML);
		this.labelPT.setValue("PT");
		this.labelPT.setContentMode(ContentMode.HTML);
		this.labelPS.setValue("PS");
		this.labelPS.setContentMode(ContentMode.HTML);
		this.labelPortataUM.setValue("UM");
		this.labelPortataUM.setContentMode(ContentMode.HTML);
		this.labelPTUM.setValue("UM");
		this.labelPTUM.setContentMode(ContentMode.HTML);
		this.labelPSUM.setValue("UM");
		this.labelPSUM.setContentMode(ContentMode.HTML);
		this.horizontalLayout4.setMargin(new MarginInfo(false));
		this.buttonReset.setCaption("Reset");
		this.buttonReset.setStyleName("small");
		this.buttonRidisegna.setCaption("Ridisegna");
		this.buttonRidisegna.setStyleName("small");
		this.gridLayout.setMargin(new MarginInfo(false));
		this.checkBoxAsprirazione.setCaption("Aspirazione");
		this.checkBoxAsprirazione.setEnabled(false);
		this.checkBoxMandata.setCaption("Mandata");
		this.checkBoxMandata.setEnabled(false);
		this.checkBoxCurvaCatarreristica.setCaption("Curva Caratteristica");
		this.gridLayout3.setMargin(new MarginInfo(false));
		this.labelQMax.setValue("Q");
		this.labelQMax.setContentMode(ContentMode.HTML);
		this.labelQMaxUM.setValue("UM");
		this.labelQMaxUM.setContentMode(ContentMode.HTML);
		this.labelPtMax.setValue("Pt");
		this.labelPtMax.setContentMode(ContentMode.HTML);
		this.labelPtMaxUM.setValue("UM");
		this.labelPtMaxUM.setContentMode(ContentMode.HTML);
		this.verticalLayoutNote.setCaption("Note");
		this.verticalLayoutNote.setSpacing(false);
		this.verticalLayoutNote.setMargin(new MarginInfo(false, true, false, true));
		this.buttonPreferenze.setCaption("Preferenze");
		this.buttonPreferenze.setStyleName("small");
		this.horizontalLayoutImmagine.setMargin(new MarginInfo(false));
	
		this.checkBoxPT.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxPT.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxPT);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxPT, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxPT, 10.0F);
		this.checkBoxPS.setWidth(100, Unit.PERCENTAGE);
		this.checkBoxPS.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout3.addComponent(this.checkBoxPS);
		this.horizontalLayout3.setComponentAlignment(this.checkBoxPS, Alignment.MIDDLE_CENTER);
		this.horizontalLayout3.setExpandRatio(this.checkBoxPS, 10.0F);
		this.gridLayout2.setColumns(5);
		this.gridLayout2.setRows(10);
		this.labelNome.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelNome, 0, 0);
		this.labelPortata.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPortata, 1, 0);
		this.labelPT.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPT, 2, 0);
		this.labelPS.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPS, 3, 0);
		this.labelPortataUM.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPortataUM, 1, 1);
		this.labelPTUM.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPTUM, 2, 1);
		this.labelPSUM.setSizeUndefined();
		this.gridLayout2.addComponent(this.labelPSUM, 3, 1);
		this.textFieldN0.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN0.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN0, 0, 2);
		this.textFieldQ0Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ0Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ0Gosth, 1, 2);
		this.textFieldPT0Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT0Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT0Gosth, 2, 2);
		this.textFieldPS0Gonsth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS0Gonsth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS0Gonsth, 3, 2);
		this.colorPicker0.setWidth(40, Unit.PIXELS);
		this.colorPicker0.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker0, 4, 2);
		this.textFieldN1.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN1.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN1, 0, 3);
		this.textFieldQ1Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ1Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ1Gosth, 1, 3);
		this.textFieldPT1Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT1Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT1Gosth, 2, 3);
		this.textFieldPS1Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS1Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS1Gosth, 3, 3);
		this.colorPicker1.setWidth(40, Unit.PIXELS);
		this.colorPicker1.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker1, 4, 3);
		this.textFieldN2.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN2.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN2, 0, 4);
		this.textFieldQ2Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ2Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ2Gosth, 1, 4);
		this.textFieldPT2Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT2Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT2Gosth, 2, 4);
		this.textFieldPS2Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS2Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS2Gosth, 3, 4);
		this.colorPicker2.setWidth(40, Unit.PIXELS);
		this.colorPicker2.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker2, 4, 4);
		this.textFieldN3.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN3.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN3, 0, 5);
		this.textFieldQ3Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ3Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ3Gosth, 1, 5);
		this.textFieldPT3Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT3Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT3Gosth, 2, 5);
		this.textFieldPS3Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS3Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS3Gosth, 3, 5);
		this.colorPicker3.setWidth(40, Unit.PIXELS);
		this.colorPicker3.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker3, 4, 5);
		this.textFieldN4.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN4.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN4, 0, 6);
		this.textFieldQ4Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ4Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ4Gosth, 1, 6);
		this.textFieldPT4Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT4Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT4Gosth, 2, 6);
		this.textFieldPS4Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS4Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS4Gosth, 3, 6);
		this.colorPicker4.setWidth(40, Unit.PIXELS);
		this.colorPicker4.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker4, 4, 6);
		this.textFieldN5.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN5.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN5, 0, 7);
		this.textFieldQ5Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ5Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ5Gosth, 1, 7);
		this.textFieldPT5Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT5Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT5Gosth, 2, 7);
		this.textFieldPS5Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS5Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS5Gosth, 3, 7);
		this.colorPicker55.setWidth(40, Unit.PIXELS);
		this.colorPicker55.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker55, 4, 7);
		this.textFieldN6.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN6.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN6, 0, 8);
		this.textFieldQ6Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ6Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ6Gosth, 1, 8);
		this.textFieldPT6Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT6Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT6Gosth, 2, 8);
		this.textFieldPS6Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS6Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS6Gosth, 3, 8);
		this.colorPicker6.setWidth(40, Unit.PIXELS);
		this.colorPicker6.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker6, 4, 8);
		this.textFieldN7.setWidth(100, Unit.PERCENTAGE);
		this.textFieldN7.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldN7, 0, 9);
		this.textFieldQ7Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldQ7Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldQ7Gosth, 1, 9);
		this.textFieldPT7Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPT7Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPT7Gosth, 2, 9);
		this.textFieldPS7Gosth.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPS7Gosth.setHeight(-1, Unit.PIXELS);
		this.gridLayout2.addComponent(this.textFieldPS7Gosth, 3, 9);
		this.colorPicker7.setWidth(40, Unit.PIXELS);
		this.colorPicker7.setHeight(27, Unit.PIXELS);
		this.gridLayout2.addComponent(this.colorPicker7, 4, 9);
		this.gridLayout2.setColumnExpandRatio(0, 10.0F);
		this.gridLayout2.setColumnExpandRatio(1, 10.0F);
		this.gridLayout2.setColumnExpandRatio(2, 10.0F);
		this.gridLayout2.setColumnExpandRatio(3, 10.0F);
		this.gridLayout2.setRowExpandRatio(5, 10.0F);
		this.buttonReset.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.buttonReset);
		this.horizontalLayout4.setComponentAlignment(this.buttonReset, Alignment.MIDDLE_CENTER);
		this.buttonRidisegna.setSizeUndefined();
		this.horizontalLayout4.addComponent(this.buttonRidisegna);
		this.horizontalLayout4.setComponentAlignment(this.buttonRidisegna, Alignment.MIDDLE_CENTER);
		final CustomComponent horizontalLayout4_spacer = new CustomComponent();
		horizontalLayout4_spacer.setSizeFull();
		this.horizontalLayout4.addComponent(horizontalLayout4_spacer);
		this.horizontalLayout4.setExpandRatio(horizontalLayout4_spacer, 1.0F);
		this.horizontalLayout3.setSizeFull();
		this.verticalLayoutPL.addComponent(this.horizontalLayout3);
		this.verticalLayoutPL.setComponentAlignment(this.horizontalLayout3, Alignment.MIDDLE_CENTER);
		this.gridLayout2.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout2.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutPL.addComponent(this.gridLayout2);
		this.verticalLayoutPL.setComponentAlignment(this.gridLayout2, Alignment.MIDDLE_CENTER);
		this.verticalLayoutPL.setExpandRatio(this.gridLayout2, 10.0F);
		this.horizontalLayout4.setSizeFull();
		this.verticalLayoutPL.addComponent(this.horizontalLayout4);
		this.verticalLayoutPL.setComponentAlignment(this.horizontalLayout4, Alignment.MIDDLE_CENTER);
		this.verticalLayoutPL.setSizeFull();
		this.panel.setContent(this.verticalLayoutPL);
		this.gridLayout.setColumns(2);
		this.gridLayout.setRows(3);
		this.checkBoxAsprirazione.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxAsprirazione, 0, 0);
		this.checkBoxMandata.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxMandata, 1, 0);
		this.checkBoxCurvaCatarreristica.setSizeUndefined();
		this.gridLayout.addComponent(this.checkBoxCurvaCatarreristica, 0, 1);
		this.gridLayout.setColumnExpandRatio(0, 10.0F);
		this.gridLayout.setColumnExpandRatio(1, 10.0F);
		final CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 2, 1, 2);
		this.gridLayout.setRowExpandRatio(2, 1.0F);
		this.gridLayout3.setColumns(3);
		this.gridLayout3.setRows(3);
		this.labelQMax.setSizeUndefined();
		this.gridLayout3.addComponent(this.labelQMax, 0, 0);
		this.gridLayout3.setComponentAlignment(this.labelQMax, Alignment.TOP_RIGHT);
		this.textFieldDoubleQMax.setWidth(100, Unit.PERCENTAGE);
		this.textFieldDoubleQMax.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.textFieldDoubleQMax, 1, 0);
		this.labelQMaxUM.setWidth(100, Unit.PERCENTAGE);
		this.labelQMaxUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.labelQMaxUM, 2, 0);
		this.labelPtMax.setSizeUndefined();
		this.gridLayout3.addComponent(this.labelPtMax, 0, 1);
		this.gridLayout3.setComponentAlignment(this.labelPtMax, Alignment.TOP_RIGHT);
		this.textFieldDoublePtMax.setWidth(100, Unit.PERCENTAGE);
		this.textFieldDoublePtMax.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.textFieldDoublePtMax, 1, 1);
		this.labelPtMaxUM.setWidth(100, Unit.PERCENTAGE);
		this.labelPtMaxUM.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.labelPtMaxUM, 2, 1);
		this.gridLayout3.setColumnExpandRatio(0, 10.0F);
		this.gridLayout3.setColumnExpandRatio(1, 10.0F);
		this.gridLayout3.setColumnExpandRatio(2, 10.0F);
		final CustomComponent gridLayout3_vSpacer = new CustomComponent();
		gridLayout3_vSpacer.setSizeFull();
		this.gridLayout3.addComponent(gridLayout3_vSpacer, 0, 2, 2, 2);
		this.gridLayout3.setRowExpandRatio(2, 1.0F);
		this.textFieldNote0.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNote0.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutNote.addComponent(this.textFieldNote0);
		this.textFieldNote1.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNote1.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutNote.addComponent(this.textFieldNote1);
		this.textFieldNote2.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNote2.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutNote.addComponent(this.textFieldNote2);
		this.textFieldNote3.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNote3.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutNote.addComponent(this.textFieldNote3);
		this.textFieldNote4.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNote4.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutNote.addComponent(this.textFieldNote4);
		this.verticalLayoutNote.setComponentAlignment(this.textFieldNote4, Alignment.MIDDLE_CENTER);
		this.textFieldNote5.setWidth(100, Unit.PERCENTAGE);
		this.textFieldNote5.setHeight(-1, Unit.PIXELS);
		//this.textFieldNote0.setTextChangeEventMode(TextChangeEventMode.TIMEOUT);
		//this.textFieldNote0.setTextChangeTimeout(1000);
		
		
		if (this.l_VG != null) {
			for (int i = 0; i < 6; i++) {
				this.l_VG.multigiri.note[i] = "";
			}
		}
		this.textFieldNote0.setValue("");
		this.textFieldNote1.setValue("");
		this.textFieldNote2.setValue("");
		this.textFieldNote3.setValue("");
		this.textFieldNote4.setValue("");
		this.textFieldNote5.setValue("");
		
		this.verticalLayoutNote.addComponent(this.textFieldNote5);
		this.verticalLayoutNote.setComponentAlignment(this.textFieldNote5, Alignment.MIDDLE_CENTER);
		this.imageLogo.setWidth(100, Unit.PERCENTAGE);
		this.imageLogo.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutParametri.addComponent(this.imageLogo);
		this.panel.setWidth(100, Unit.PERCENTAGE);
		this.panel.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutParametri.addComponent(this.panel);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutParametri.addComponent(this.gridLayout);
		this.verticalLayoutParametri.setComponentAlignment(this.gridLayout, Alignment.BOTTOM_LEFT);
		this.gridLayout3.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout3.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutParametri.addComponent(this.gridLayout3);
		this.verticalLayoutParametri.setComponentAlignment(this.gridLayout3, Alignment.MIDDLE_CENTER);
		this.verticalLayoutNote.setWidth(100, Unit.PERCENTAGE);
		this.verticalLayoutNote.setHeight(-1, Unit.PIXELS);
		this.verticalLayoutParametri.addComponent(this.verticalLayoutNote);
		this.verticalLayoutParametri.setComponentAlignment(this.verticalLayoutNote, Alignment.MIDDLE_CENTER);
		this.buttonPreferenze.setSizeUndefined();
		this.verticalLayoutParametri.addComponent(this.buttonPreferenze);
		final CustomComponent verticalLayoutParametri_spacer = new CustomComponent();
		verticalLayoutParametri_spacer.setSizeFull();
		this.verticalLayoutParametri.addComponent(verticalLayoutParametri_spacer);
		this.verticalLayoutParametri.setExpandRatio(verticalLayoutParametri_spacer, 1.0F);
		this.image.setSizeFull();
		this.horizontalLayoutImmagine.addComponent(this.image);
		this.horizontalLayoutImmagine.setComponentAlignment(this.image, Alignment.MIDDLE_CENTER);
		this.horizontalLayoutImmagine.setExpandRatio(this.image, 10.0F);
		this.verticalLayoutParametri.setSizeFull();
		this.horizontalLayout.addComponent(this.verticalLayoutParametri);
		this.horizontalLayout.setExpandRatio(this.verticalLayoutParametri, 10.0F);
		this.horizontalLayoutImmagine.setSizeFull();
		this.horizontalLayout.addComponent(this.horizontalLayoutImmagine);
		this.horizontalLayout.setExpandRatio(this.horizontalLayoutImmagine, 30.0F);
		this.horizontalLayout.setSizeFull();
		this.setContent(this.horizontalLayout);
		this.setSizeFull();
	
		this.checkBoxPT.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MultigiriView.this.checkBoxPT_valueChange(event);
			}
		});
		this.checkBoxPS.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MultigiriView.this.checkBoxPS_valueChange(event);
			}
		});
		this.textFieldQ1Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldQ1Gosth_textChange(event);
			}
		});
		this.textFieldPT1Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPT1Gosth_textChange(event);
			}
		});
		this.textFieldPS1Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPS1Gosth_textChange(event);
			}
		});
		this.textFieldQ2Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldQ2Gosth_textChange(event);
			}
		});
		this.textFieldPT2Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPT2Gosth_textChange(event);
			}
		});
		this.textFieldPS2Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPS2Gosth_textChange(event);
			}
		});
		this.textFieldQ3Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldQ3Gosth_textChange(event);
			}
		});
		this.textFieldPT3Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPT3Gosth_textChange(event);
			}
		});
		this.textFieldPS3Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPS3Gosth_textChange(event);
			}
		});
		this.textFieldQ4Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldQ4Gosth_textChange(event);
			}
		});
		this.textFieldPT4Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPT4Gosth_textChange(event);
			}
		});
		this.textFieldPS4Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPS4Gosth_textChange(event);
			}
		});
		this.textFieldQ5Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldQ5Gosth_textChange(event);
			}
		});
		this.textFieldPT5Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPT5Gosth_textChange(event);
			}
		});
		this.textFieldPS5Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPS5Gosth_textChange(event);
			}
		});
		this.textFieldQ6Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldQ6Gosth_textChange(event);
			}
		});
		this.textFieldPT6Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPT6Gosth_textChange(event);
			}
		});
		this.textFieldPS6Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPS6Gosth_textChange(event);
			}
		});
		this.textFieldQ7Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldQ7Gosth_textChange(event);
			}
		});
		this.textFieldPT7Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPT7Gosth_textChange(event);
			}
		});
		this.textFieldPS7Gosth.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldPS7Gosth_textChange(event);
			}
		});
		this.buttonReset.addClickListener(event -> this.buttonReset_buttonClick(event));
		this.buttonRidisegna.addClickListener(event -> this.buttonRidisegna_buttonClick(event));
		this.checkBoxCurvaCatarreristica.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(final Property.ValueChangeEvent event) {
				MultigiriView.this.checkBoxCurvaCatarreristica_valueChange(event);
			}
		});
		this.textFieldNote0.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldNote0_textChange(event);
			}
		});
		this.textFieldNote1.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldNote1_textChange(event);
			}
		});
		this.textFieldNote2.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldNote2_textChange(event);
			}
		});
		this.textFieldNote3.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldNote3_textChange(event);
			}
		});
		this.textFieldNote4.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldNote4_textChange(event);
			}
		});
		this.textFieldNote5.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(final FieldEvents.TextChangeEvent event) {
				MultigiriView.this.textFieldNote5_textChange(event);
			}
		});
		this.buttonPreferenze.addClickListener(event -> this.buttonPreferenze_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevColorPicker colorPicker0, colorPicker1, colorPicker2, colorPicker3, colorPicker4, colorPicker55,
			colorPicker6, colorPicker7;
	private XdevLabel labelNome, labelPortata, labelPT, labelPS, labelPortataUM, labelPTUM, labelPSUM, labelQMax, labelQMaxUM,
			labelPtMax, labelPtMaxUM;
	private XdevButton buttonReset, buttonRidisegna, buttonPreferenze;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout3, horizontalLayout4, horizontalLayoutImmagine;
	private XdevImage imageLogo, image;
	private TextFieldDouble textFieldDoubleQMax, textFieldDoublePtMax;
	private XdevPanel panel;
	private XdevCheckBox checkBoxPT, checkBoxPS, checkBoxAsprirazione, checkBoxMandata, checkBoxCurvaCatarreristica;
	private XdevGridLayout gridLayout2, gridLayout, gridLayout3;
	private XdevTextField textFieldN0, textFieldQ0Gosth, textFieldPT0Gosth, textFieldPS0Gonsth, textFieldN1,
			textFieldQ1Gosth, textFieldPT1Gosth, textFieldPS1Gosth, textFieldN2, textFieldQ2Gosth, textFieldPT2Gosth,
			textFieldPS2Gosth, textFieldN3, textFieldQ3Gosth, textFieldPT3Gosth, textFieldPS3Gosth, textFieldN4,
			textFieldQ4Gosth, textFieldPT4Gosth, textFieldPS4Gosth, textFieldN5, textFieldQ5Gosth, textFieldPT5Gosth,
			textFieldPS5Gosth, textFieldN6, textFieldQ6Gosth, textFieldPT6Gosth, textFieldPS6Gosth, textFieldN7,
			textFieldQ7Gosth, textFieldPT7Gosth, textFieldPS7Gosth, textFieldNote0, textFieldNote1, textFieldNote2,
			textFieldNote3, textFieldNote4, textFieldNote5;
	private XdevVerticalLayout verticalLayoutParametri, verticalLayoutPL, verticalLayoutNote;
	// </generated-code>
	
}
