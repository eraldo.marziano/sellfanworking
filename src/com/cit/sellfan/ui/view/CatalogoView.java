package com.cit.sellfan.ui.view;

import java.util.Collection;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldDouble;
import com.cit.sellfan.ui.pannelli.pannelloCondizioni;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.event.FieldEvents;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;

import cit.sellfan.Costanti;
import cit.sellfan.classi.Preventivo;
import cit.sellfan.classi.Serie;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.utility.NumeriRomani;
import com.cit.sellfan.ui.pannelli.PannelloCondizioniNuovo;
import de.steinwedel.messagebox.ButtonType;
import de.steinwedel.messagebox.MessageBox;
import java.util.Arrays;

public class CatalogoView extends XdevView {
	public boolean isPannelloInizializzato = false;
	private VariabiliGlobali l_VG;
	private final Container containerSerie = new IndexedContainer();
	private final Container containerVentilatori = new IndexedContainer();
	private String currentVentilatoreID = null;
	private String currentSerieCode = null;
	private MessageBox msgBox;
	
	/**
	 * 
	 */
	public CatalogoView() {
		super();
		this.initUI();
		this.tableSerie.setEnabled(false);
		this.tableVentilatori.setEnabled(false);
		this.containerSerie.removeAllItems();
		this.containerSerie.addContainerProperty("Code", String.class, "");
		this.containerSerie.addContainerProperty("Description", String.class, "");
		this.tableSerie.setContainerDataSource(this.containerSerie);
		this.containerVentilatori.removeAllItems();
		this.containerVentilatori.addContainerProperty("Check", XdevImage.class, "");
		this.containerVentilatori.addContainerProperty("Code", String.class, "");
		this.containerVentilatori.addContainerProperty("Motor", String.class, "");
		this.containerVentilatori.addContainerProperty("Power", Double.class, 0.0);
		this.containerVentilatori.addContainerProperty("Rpm", Double.class, 0.0);
		this.containerVentilatori.addContainerProperty("Impeller", Double.class, 0.0);
		this.tableVentilatori.setContainerDataSource(this.containerVentilatori);
		this.tableSerie.setEnabled(true);
		this.tableVentilatori.setEnabled(true);


	}
	
	private void setFanVisible() {
            
		this.labelDiametroVentola.setVisible(this.l_VG.editVentola);
		this.textFieldDiametro.setVisible(this.l_VG.editVentola);
	}
	
	private void setConverterForTable() {
		final myStringToDoubleConverter converter = new myStringToDoubleConverter();
		this.tableVentilatori.setConverter("Power", converter);
		this.tableVentilatori.setConverter("Rpm", converter);
		this.tableVentilatori.setConverter("Impeller", converter);
	}
	
	private class myStringToDoubleConverter extends StringToDoubleConverter {
		public myStringToDoubleConverter() {
			super();
		}
		
		@Override
		public java.text.NumberFormat getFormat(final Locale locale) {
			return CatalogoView.this.l_VG.fmtNdTable;
		}
	}
	
	public void setVariabiliGlobali(final VariabiliGlobali variabiliGlobali) {
		this.l_VG = variabiliGlobali;
	    this.l_VG.ElencoView[this.l_VG.CatalogoViewIndex] = this;
		if (this.l_VG.logostreamresource != null) {
			this.imageLogo.setSource(this.l_VG.logostreamresource);
		}
		//imagelogo.setSizeFull();
	    this.checkBoxDA.setEnabled(false);
	    this.checkBoxTR.setEnabled(false);
		this.checkBoxDA.setValue(!this.l_VG.trasmissioneFlag);
		this.checkBoxTR.setValue(this.l_VG.trasmissioneFlag);
	    this.checkBoxDA.setEnabled(true);
	    this.checkBoxTR.setEnabled(true);
	    this.checkBoxOutlet.setEnabled(false);
	    this.checkBoxInlet.setEnabled(false);
		this.checkBoxOutlet.setValue(this.l_VG.fisicaPressioneInMandata);
		this.checkBoxInlet.setValue(!this.l_VG.fisicaPressioneInMandata);
	    this.checkBoxOutlet.setEnabled(true);
	    this.checkBoxInlet.setEnabled(true);
	    try {
	    	buildTableSerie();
	    } catch (final Exception e) {
	    	//Notification.show(e.toString());
	    }
		setButtonVisible(false);
		setConverterForTable();

	}
	
	public void resetView() {
	    try {
	    	buildTableSerie();
	    } catch (final Exception e) {
	    	//Notification.show(e.toString());
	    }
	}
	
	public void Nazionalizza() {
		//l_VG.MainView.setMemo(Integer.toString(l_VG.currentLivelloUtente)+"  "+Integer.toString(Costanti.utenteDefault));
		this.buttonAggiungiPreventivo.setVisible(this.l_VG.utilityCliente.isTabVisible("Offerte") && this.l_VG.currentLivelloUtente == Costanti.utenteDefault);
		this.buttonCompare.setVisible(this.l_VG.utilityCliente.isTabVisible("Confronto"));
		this.checkBoxTR.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Trasmissione"));
		this.checkBoxDA.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ventilatori diretti"));
		this.checkBoxInlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("aspirazione"));
		this.checkBoxOutlet.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("mandata"));
		this.buttonCondition.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Condizioni"));
		this.comboBoxEsecuzioni.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Esecuzione"));
		this.buttonAggiungiPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
		this.buttonCompare.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Confronta"));
		this.comboBoxClassi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Classe"));
		this.labelTemp.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Temperatura") + ": ");
		this.labelAlt.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Altezza") + ": ");
		this.labelFreq.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza") + ": ");
		this.labelDiametroVentola.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Diametro Ventola") + ": ");
		//tableSerie.setColumnHeader("Code", l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		//tableSerie.setColumnHeader("Description", l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableSerie.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableSerie.setColumnHeader("Description", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"));
		this.tableVentilatori.setColumnHeader("Check", this.l_VG.utilityTraduzioni.TraduciStringa("Preventivo"));
		this.tableVentilatori.setColumnHeader("Code", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"));
		this.tableVentilatori.setColumnHeader("Motor", this.l_VG.utilityTraduzioni.TraduciStringa("Motore"));
		this.tableVentilatori.setColumnHeader("Power", this.l_VG.utilityTraduzioni.TraduciStringa("P. inst.") + " [kW]");
		this.tableVentilatori.setColumnHeader("Rpm", this.l_VG.utilityTraduzioni.TraduciStringa("N. giri") + " [Rpm]");
		this.tableVentilatori.setColumnHeader("Impeller", this.l_VG.utilityTraduzioni.TraduciStringa("D. girante") + " [mm]");
		if (!this.l_VG.utilityCliente.isTabVisible("Offerte")) {
			this.tableVentilatori.setColumnWidth("Check", 0);
		}
                if (l_VG.currentUserSaaS.idClienteIndex == 1) {
                    tableVentilatori.setColumnWidth("Impeller", 0);
                }
		this.setFanVisible();
	}
	
	public boolean isPreSetVentilatoreCurrentOK() {
		return this.isPannelloInizializzato;
	}
	
	public void buildTableSerie() {
		if (!this.l_VG.utilityCliente.isTabVisible("Catalogo")) {
			return;
		}
		this.tableSerie.setEnabled(false);
		this.currentVentilatoreID = null;
		this.currentSerieCode = null;
		@SuppressWarnings("unused")
		final Item item;
		this.l_VG.ElencoVentilatoriFromCatalogo.clear();
		buildTableVentilatori(null);
//		containerSerie.removeAllItems();
		try {
			this.tableSerie.removeAllItems();//resetta anche le selection
		} catch (final Exception e) {
			
		}
            for (int i = 0; i < this.l_VG.ElencoSerie.size(); i++) {
                final Object obj[] = new Object[2];
                final Serie l_s = this.l_VG.ElencoSerie.get(i);
                if (l_VG.idClienteIndexVG == 1 && l_VG.CACatalogo.frequenza == 60) {
                    String[] serieEscludere = {"P-S","PN-S","N", "NP", "MVP-M", "MVP-P", "TDAP", "TDA-M", "TDA-P", "EAT", "TCR"};
                    if (Arrays.asList(serieEscludere).contains(l_s.Serie))
                        continue;
                }
                if (!this.l_VG.ElencoSerieDisponibili.get(i)) {
                    continue;
                }

                final String itemID = l_s.Serie;
                if (this.l_VG.trasmissioneFlag) {
                    if (l_s.Trasmissione == 1) {
                        obj[0] = l_s.SerieTrans;
                        obj[1] = this.l_VG.utilityTraduzioni.TraduciStringa(l_s.DescrizioneTrans);
                        this.tableSerie.addItem(obj, itemID);
                    }
                } else {
                    if (l_s.DirettamenteAccoppiato == 1) {
                        obj[0] = l_s.SerieTrans;
                        obj[1] = this.l_VG.utilityTraduzioni.TraduciStringa(l_s.DescrizioneTrans);
                        this.tableSerie.addItem(obj, itemID);
                    }
                }
            }
		this.isPannelloInizializzato = true;
		this.tableSerie.setEnabled(true);
	}
	
	class RemindTask extends TimerTask {
		@Override
		public void run() {
			CatalogoView.this.buttonAggiungiPreventivo.setIcon(null);
		}
	}

	@SuppressWarnings("unchecked")
	public void aggiornaTableVentilatori() {
		try {
			if (this.currentVentilatoreID != null) {
				final Item selectedItem = this.containerVentilatori.getItem(this.currentVentilatoreID);
				selectedItem.getItemProperty("Code").setValue(this.l_VG.buildModelloCompleto(this.l_VG.VentilatoreCurrent));
				selectedItem.getItemProperty("Motor").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
				selectedItem.getItemProperty("Power").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.PotkW);
				selectedItem.getItemProperty("Rpm").setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.NumeroGiri);
				selectedItem.getItemProperty("Impeller").setValue(this.l_VG.VentilatoreCurrent.DiametroGirante * 1000.);
			}
		} catch (final Exception e) {
			//l_VG.MainView.setMemo(e.toString());
		}
	}
	
	public void setCheckTableVentilatori() {
		try {
			if (this.currentVentilatoreID != null) {
				final Item selectedItem = this.containerVentilatori.getItem(this.currentVentilatoreID);
				final XdevImage chk = (XdevImage)selectedItem.getItemProperty("Check").getValue();
				chk.setSource(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
			}
		} catch (final Exception e) {
			this.l_VG.MainView.setMemo(e.toString());
		}
	}
	
	private void setButtonVisible(final boolean value) {
		//l_VG.setComponentBackground(buttonAddToOffer.getId(), HColor.HGiallo);
		this.comboBoxEsecuzioni.setVisible(value);
		this.comboBoxClassi.setVisible(value);
		this.buttonAggiungiPreventivo.setEnabled(value);
		this.buttonCompare.setEnabled(value);
	}
	
    private String getSerieFromTranscodifica(final String serieTrans) {
        for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
            if (serieTrans.equals(this.l_VG.ElencoSerie.get(i).SerieTrans)) {
				return this.l_VG.ElencoSerie.get(i).Serie;
			}
        }
        return null;
    }

    private void buildTableVentilatori(final String serie) {
    	this.tableVentilatori.setEnabled(false);
        this.currentSerieCode = serie;
        this.currentVentilatoreID = null;
        @SuppressWarnings("unused")
        final Item item;
        try {
                this.tableVentilatori.removeAllItems();//resetta anche le selection
        } catch (final Exception e) { }
        this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
        setButtonVisible(false);
        if (serie == null) return;
        this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
        try {
        //this.l_VG.dbTecnico.loadCatalogo(this.l_VG.ElencoVentilatoriFromCatalogo, serie, this.l_VG.nTemperatureLimite + 1, this.l_VG.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, this.l_VG.trasmissioneFlag, this.l_VG.CACatalogo, this.l_VG.CA020Catalogo, this.l_VG.idTranscodificaIndex);
        for (int i=0 ; i<this.l_VG.ElencoVentilatoriFromCatalogo.size() ; i++) {
            final String itemID = Integer.toString(i);
            final Object obj[] = new Object[6];
            final Ventilatore l_v = this.l_VG.ElencoVentilatoriFromCatalogo.get(i);
            l_v.PressioneMandataFlag = this.checkBoxOutlet.getValue();
            if (l_v.Classe.equals("-") && l_v.elencoRpmClassi.size() > 0) {
                l_v.Classe = NumeriRomani.daAraboARomano(Integer.toString(l_v.elencoClassi.get(0)));
            }
            final XdevImage chk = new XdevImage();
            boolean onPreventivo = false;
            if (this.l_VG.ElencoVentilatoriFromPreventivo != null) {
                for (int j=0 ; j<this.l_VG.ElencoVentilatoriFromPreventivo.size() ; j++) {
                    final Ventilatore l_vv = this.l_VG.ElencoVentilatoriFromPreventivo.get(j);
                    if (l_vv.Serie.equals(l_v.Serie) && l_vv.Modello.equals(l_v.Modello) && l_vv.Versione.equals(l_v.Versione)) {
                        onPreventivo = true;
                        break;
                    }
                }
            }
            if (onPreventivo) {
                chk.setSource(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
            } else {
                chk.setSource(new ApplicationResource(this.getClass(), "WebContent/resources/img/empty.png"));
            }
            chk.setWidth(20, Unit.PIXELS);
            chk.setHeight(20, Unit.PIXELS);
            obj[0] = chk;
            obj[1] = this.l_VG.buildModelloCompleto(l_v);
            obj[2] = l_v.selezioneCorrente.MotoreInstallato.CodiceCliente;
            obj[3] = l_v.selezioneCorrente.MotoreInstallato.PotkW;
            obj[4] = l_v.selezioneCorrente.NumeroGiri;
            obj[5] = l_v.DiametroGirante * 1000.;
            this.tableVentilatori.addItem(obj, itemID);
            }
        } catch (final Exception e) {
                Notification.show(e.toString());
        }
        this.tableVentilatori.setEnabled(true);
	}
    
    /**
	 * Event handler delegate method for the {@link XdevTable}
	 * {@link #tableSerie}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void tableSerie_valueChange(final Property.ValueChangeEvent event) {
		if (!this.tableSerie.isEnabled()) {
			return;
		}
		try {
			this.labelAlt.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Altezza") + " " + this.l_VG.utilityUnitaMisura.frommToXX(this.l_VG.CACatalogo.altezza, this.l_VG.UMAltezzaCorrente) + " " + this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.UMAltezzaCorrente.descrizione));
			this.labelTemp.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Temperatura") + " " + this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, this.l_VG.CACatalogo.temperatura) + " " + this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.UMTemperaturaCorrente.descrizione));
			this.labelFreq.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza") + " " + this.l_VG.CACatalogo.frequenza);
			this.l_VG.catalogoCondizioniUmidita = 0;
			final Item item = this.containerSerie.getItem(event.getProperty().getValue().toString());
			if (this.l_VG.debugFlag) {
				Notification.show(item.getItemProperty("Code").getValue().toString());
			}
			this.l_VG.fisicaPressioneInMandata = this.checkBoxOutlet.getValue();
			final String serie = getSerieFromSerieTrans(item.getItemProperty("Code").getValue().toString());
			int freq = -1;
			if (!this.l_VG.catalogoCondizioniFrequenza.equals("")) {
                            freq = l_VG.CACatalogo.frequenza;
//				freq = Integer.parseInt(this.l_VG.catalogoCondizioniFrequenza.split(" ")[0]);
			}
			this.l_VG.dbTecnico.loadCatalogo(this.l_VG.ElencoVentilatoriFromCatalogo, serie, this.l_VG.nTemperatureLimite + 1, this.l_VG.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, this.l_VG.trasmissioneFlag, this.l_VG.CACatalogo, this.l_VG.CA020Catalogo, this.l_VG.idTranscodificaIndex, freq);
	        buildTableVentilatori(getSerieFromTranscodifica(item.getItemProperty("Code").getValue().toString()));
	    	this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
	    	this.buttonAggiungiPreventivo.setIcon(null);
		} catch (final Exception e) {
	        buildTableVentilatori(null);
	    	this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
		}
	}
	
	private String getSerieFromSerieTrans(final String serieTrans) {
		for (int i=0 ; i<this.l_VG.ElencoSerie.size() ; i++) {
			final Serie l_s = this.l_VG.ElencoSerie.get(i);
			if (l_s.Serie.equals(serieTrans)) {
				return l_s.Serie;
			}
			if (l_s.SerieTrans.equals(serieTrans)) {
				return l_s.Serie;
			}
		}
		return "";
	}

	
	private void addNewPreventivo() {
        if (!this.l_VG.traceUser.isActionEnabled(this.l_VG.traceUser.AzioneNuovaOfferta, this.l_VG.ElencoPreventivi.size())) {
        	this.msgBox = MessageBox.createWarning();
        	this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Numero Masssimo Preventivi Disponibili Raggiunto"));
        	this.msgBox.withOkButton();
        	this.msgBox.open();
            return;
        }
        this.l_VG.preventivoCurrent = new Preventivo();
		this.l_VG.preventivoCurrent.NumOfferta = "Temp";
		this.l_VG.preventivoCurrent.NumOrdine = "Temp";
		this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
	    final Integer idPreventivo = this.l_VG.preventivoCurrent.IDPreventivo;
    	this.l_VG.preventivoCurrentIndex = this.l_VG.ElencoPreventivi.size() - 1;
    	this.l_VG.ElencoPreventivi.set(this.l_VG.preventivoCurrentIndex, this.l_VG.preventivoCurrent);
    	//this.l_VG.preventivoCurrent = this.l_VG.ElencoPreventivi.get(this.l_VG.preventivoCurrentIndex);
        this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneNuovaOfferta, "ID: " + Integer.toString(idPreventivo));
	}
	/**
 * Event handler delegate method for the {@link XdevTable}
 * {@link #tableVentilatori}.
 *
 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
 * @eventHandlerDelegate Do NOT delete, used by UI designer!
 */
	private void tableVentilatori_valueChange(final Property.ValueChangeEvent event) {
            if (!this.tableVentilatori.isEnabled()) {
                return;
            }
            try {
                this.currentVentilatoreID = event.getProperty().getValue().toString();
                this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(event.getProperty().getValue().toString());
                this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
                this.l_VG.VentilatoreCurrent.CAProgettazione.altezza = Double.parseDouble(this.l_VG.catalogoCondizioniAltezza);
                this.l_VG.VentilatoreCurrent.CAProgettazione.temperatura = Double.parseDouble(this.l_VG.catalogoCondizioniTemperatura);
                this.l_VG.VentilatoreCurrent.CAProgettazione.umidita = this.l_VG.catalogoCondizioniUmidita;
                this.comboBoxClassi.setEnabled(false);
                this.comboBoxClassi.removeAllItems();
                for (int i = 0; i < this.l_VG.VentilatoreCurrent.elencoClassi.size(); i++) {
                    this.comboBoxClassi.addItem(this.l_VG.utilityTraduzioni.TraduciStringa("Classe") + " " + NumeriRomani.daAraboARomano(this.l_VG.VentilatoreCurrent.elencoClassi.get(i)));
                }
                this.comboBoxClassi.setValue("Class " + this.l_VG.VentilatoreCurrent.Classe);
                Collection<?> coll = this.comboBoxClassi.getItemIds();
                if (coll.size() > 1) {
                    this.comboBoxClassi.setEnabled(true);
                }
                this.comboBoxEsecuzioni.setEnabled(false);
                this.comboBoxEsecuzioni.removeAllItems();
                for (int i = 0; i < this.l_VG.VentilatoreCurrent.Esecuzioni.length; i++) {
                    if (!this.l_VG.VentilatoreCurrent.Esecuzioni[i].equals("-")) {
                        this.comboBoxEsecuzioni.addItem(this.l_VG.VentilatoreCurrent.Esecuzioni[i]);
                    }
                }
                this.comboBoxEsecuzioni.setValue(this.l_VG.VentilatoreCurrent.selezioneCorrente.Esecuzione);
                coll = this.comboBoxEsecuzioni.getItemIds();
                if (coll.size() > 1) {
                    this.comboBoxEsecuzioni.setEnabled(true);
                }
                this.textFieldDiametro.setValue(this.l_VG.VentilatoreCurrent.DiametroGirante);
                if (l_VG.idClienteIndexVG == 1 && l_VG.CACatalogo.frequenza == 60) {
                    msgBox = MessageBox.createInfo();
                    msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("I ventilatori sono da considerarsi a 60Hz"));
                    msgBox.withOkButton();
                    msgBox.open();
                }
                setButtonVisible(true);
            } catch (final Exception e) {
                this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
            }
	}

/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxTR}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxTR_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxTR.isEnabled()) {
			return;
		}
		this.checkBoxDA.setValue(!this.checkBoxTR.getValue());
		this.l_VG.trasmissioneFlag = this.checkBoxTR.getValue();
		buildTableSerie();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxDA}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxDA_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxDA.isEnabled()) {
			return;
		}
		this.checkBoxTR.setValue(!this.checkBoxDA.getValue());
		this.l_VG.trasmissioneFlag = this.checkBoxTR.getValue();
		buildTableSerie();
	}

/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxInlet}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxInlet_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxInlet.isEnabled()) {
			return;
		}
		this.checkBoxOutlet.setValue(!this.checkBoxInlet.getValue());
		this.l_VG.fisicaPressioneInMandata = this.checkBoxOutlet.getValue();
		buildTableSerie();
	}

	/**
	 * Event handler delegate method for the {@link XdevCheckBox}
	 * {@link #checkBoxOutlet}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void checkBoxOutlet_valueChange(final Property.ValueChangeEvent event) {
		if (!this.checkBoxOutlet.isEnabled()) {
			return;
		}
		this.checkBoxInlet.setValue(!this.checkBoxOutlet.getValue());
		this.l_VG.fisicaPressioneInMandata = this.checkBoxOutlet.getValue();
		buildTableSerie();
	}

	private void textFieldDiametro_textChange(final FieldEvents.TextChangeEvent event) {
            if (this.l_VG.VentilatoreCurrent.DiametroGiranteDefault == 0)
                this.l_VG.VentilatoreCurrent.DiametroGiranteDefault = l_VG.VentilatoreCurrent.DiametroGirante;
            this.l_VG.VentilatoreCurrent.DiametroGirante = this.textFieldDiametro.getDoubleValue();
	}
/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxClassi}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void comboBoxClassi_valueChange(final Property.ValueChangeEvent event) {
		if (!this.comboBoxClassi.isEnabled()) {
			return;
		}
		if (this.comboBoxClassi.getValue() == null) {
			return;
		}
		if (this.l_VG.cambiaClasse(event.getProperty().getValue().toString())) {
			aggiornaTableVentilatori();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCondition}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCondition_buttonClick(final Button.ClickEvent event) {
		final PannelloCondizioniNuovo pannello = new PannelloCondizioniNuovo(l_VG.utilityTraduzioni.TraduciStringa("Condizioni Ambientali"));
		pannello.setVariabiliGlobali(this.l_VG);
		pannello.Nazionalizza();
		pannello.initPannello();
                pannello.setStyleName("pannelloPreferenze");
		final MessageBox msgBox = MessageBox.create();
		msgBox.withMessage(pannello);
                msgBox.withNoButton();
		msgBox.withOkButton(() -> {
                    pannello.azioneOK();
                    if (l_VG.idClienteIndexVG == 1) {
                        buildTableSerie();
                    }
                    String temp = this.l_VG.utilityTraduzioni.TraduciStringa("Temperatura") + ": ";
                    temp += this.l_VG.utilityUnitaMisura.fromCelsiusToXX(this.l_VG.UMTemperaturaCorrente.index, Double.parseDouble(this.l_VG.catalogoCondizioniTemperatura)) + " ";
                    temp += this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.UMTemperaturaCorrente.descrizione);
                    String alt = this.l_VG.utilityTraduzioni.TraduciStringa("Altezza") + ": ";
                    alt += this.l_VG.utilityUnitaMisura.frommToXX(Double.parseDouble(this.l_VG.catalogoCondizioniAltezza), this.l_VG.UMAltezzaCorrente) + " ";
                    alt += this.l_VG.utilityTraduzioni.TraduciStringa(this.l_VG.UMAltezzaCorrente.descrizione);
                    this.labelTemp.setValue(temp);
                    this.labelAlt.setValue(alt);
                    this.labelFreq.setValue(this.l_VG.utilityTraduzioni.TraduciStringa("Frequenza") + " " + this.l_VG.catalogoCondizioniFrequenza);
                    if (this.currentSerieCode != null) {
                        int freq = -1;
                        if (!this.l_VG.catalogoCondizioniFrequenza.equals("")) {
                            freq = Integer.parseInt(this.l_VG.catalogoCondizioniFrequenza.split(" ")[0]);
                        }
                        this.l_VG.dbTecnico.loadCatalogo(this.l_VG.ElencoVentilatoriFromCatalogo, this.currentSerieCode, this.l_VG.nTemperatureLimite + 1, this.l_VG.ventilatoreFisicaParameter.ventilatoreFisicaPressioneMandataDefaultFlag, this.l_VG.trasmissioneFlag, this.l_VG.CACatalogo, this.l_VG.CA020Catalogo, this.l_VG.idTranscodificaIndex, freq);
                        buildTableVentilatori(this.currentSerieCode);
                        this.l_VG.MainView.setVentilatoreCorrenteFromCatalogo(null);
                    }
            });
            msgBox.open();

	}

/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAggiungiPreventivo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonAggiungiPreventivo_buttonClick(final Button.ClickEvent event) {
		if (this.l_VG.preventivoCurrent == null) {
		//	addNewPreventivo();
			
		}
                this.l_VG.addToPreventivo();
		this.l_VG.dbCRMv2.loadFullElencoPreventiviFan(this.l_VG.ElencoPreventivi, 0 , this.l_VG.currentUserSaaS._ID);
		setCheckTableVentilatori();
		this.buttonAggiungiPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
		final Timer timer = new Timer();
	    timer.schedule(new RemindTask(), 1000);
	}

/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonCompare}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate Do NOT delete, used by UI designer!
	 */
	private void buttonCompare_buttonClick(final Button.ClickEvent event) {
		this.l_VG.addVentilatoreCorrenteToCompare();
	}

	/**
 * Event handler delegate method for the {@link XdevComboBox}
 * {@link #comboBoxEsecuzioni}.
 *
 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
 * @eventHandlerDelegate Do NOT delete, used by UI designer!
 */
private void comboBoxEsecuzioni_valueChange(final Property.ValueChangeEvent event) {
	if (!this.comboBoxEsecuzioni.isEnabled()) {
		return;
	}
	if (this.comboBoxEsecuzioni.getValue() != null) {
		this.l_VG.cambiaEsecuzione(this.comboBoxEsecuzioni.getValue().toString());
	}
}

/*
 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
 * the UI designer.
 */
// <generated-code name="initUI">
private void initUI() {
	this.verticalLayout = new XdevVerticalLayout();
	this.panel = new XdevPanel();
	this.horizontalLayout3 = new XdevHorizontalLayout();
	this.verticalLayout3 = new XdevVerticalLayout();
	this.checkBoxTR = new XdevCheckBox();
	this.checkBoxInlet = new XdevCheckBox();
	this.verticalLayout4 = new XdevVerticalLayout();
	this.checkBoxDA = new XdevCheckBox();
	this.checkBoxOutlet = new XdevCheckBox();
	this.verticalLayout5 = new XdevVerticalLayout();
	this.verticalLayout6 = new XdevVerticalLayout();
	this.verticalLayout7 = new XdevVerticalLayout();
	this.buttonCondition = new XdevButton();
	this.comboBoxEsecuzioni = new XdevComboBox<>();
	this.verticalLayout8 = new XdevVerticalLayout();
	this.comboBoxClassi = new XdevComboBox<>();
	this.panel2 = new XdevPanel();
	this.horizontalLayout = new XdevHorizontalLayout();
	this.tableSerie = new XdevTable<>();
	this.verticalLayout2 = new XdevVerticalLayout();
	this.panelForLogo = new XdevPanel();
	this.verticalLayout9 = new XdevVerticalLayout();
	this.imageLogo = new XdevImage();
	this.panel5 = new XdevPanel();
	this.panel3 = new XdevPanel();
	this.horizontalLayout2 = new XdevHorizontalLayout();
	this.tableVentilatori = new XdevTable<>();
	this.panel4 = new XdevPanel();
	this.horizontalLayout4 = new XdevHorizontalLayout();
	this.buttonAggiungiPreventivo = new XdevButton();
	this.buttonCompare = new XdevButton();
	this.labelTemp = new XdevLabel();
	this.labelAlt = new XdevLabel();
	this.labelFreq = new XdevLabel();
	this.labelDiametroVentola = new XdevLabel();
	this.textFieldDiametro = new TextFieldDouble();
	this.horizontalLayoutVentola = new XdevHorizontalLayout();

	this.setResponsive(true);
	this.labelTemp.setValue("Temperatura:");
	this.labelAlt.setValue("Altezza:");
	this.labelFreq.setValue("Frequenza:");
	this.labelDiametroVentola.setValue("Diametro Ventola");
	this.labelDiametroVentola.setVisible(false);
	this.verticalLayout.setSpacing(false);
	this.verticalLayout.setMargin(new MarginInfo(false));
	this.verticalLayout.setResponsive(true);
	this.panel.setResponsive(true);
	this.horizontalLayout3.setMargin(new MarginInfo(false, true, false, false));
	this.verticalLayout3.setMargin(new MarginInfo(false));
	this.checkBoxTR.setCaption("Direct driven fans");
	this.checkBoxInlet.setCaption("inlet");
	this.verticalLayout4.setMargin(new MarginInfo(false));
	this.checkBoxDA.setCaption("Belt driven");
	this.checkBoxOutlet.setCaption("outlet");
	this.verticalLayout5.setMargin(new MarginInfo(false));
	this.verticalLayout6.setMargin(new MarginInfo(false));
	this.verticalLayout7.setMargin(new MarginInfo(false));
	this.buttonCondition.setCaption("Condictions");
	this.buttonCondition.setStyleName("small");
	this.comboBoxEsecuzioni.setCaption("Esecuzione");
	this.verticalLayout8.setMargin(new MarginInfo(false));
	this.comboBoxClassi.setCaption("Class");
	this.panel2.setResponsive(true);
	this.horizontalLayout.setSpacing(false);
	this.horizontalLayout.setMargin(new MarginInfo(false));
	this.horizontalLayout.setResponsive(true);
	this.tableSerie.setStyleName("small mystriped");
	this.verticalLayout2.setSpacing(false);
	this.verticalLayout2.setMargin(new MarginInfo(false));
	this.panelForLogo.setTabIndex(0);
	this.panelForLogo.setResponsive(true);
	this.verticalLayout9.setMargin(new MarginInfo(false));
	this.panel3.setResponsive(true);
	this.horizontalLayout2.setMargin(new MarginInfo(false));
	this.tableVentilatori.setStyleName("small mystriped");
	this.horizontalLayout4.setMargin(new MarginInfo(false, true, false, true));
	this.buttonAggiungiPreventivo.setCaption("Aggiungi Preventivo");
	this.buttonAggiungiPreventivo.setStyleName("big giallo");
	this.buttonAggiungiPreventivo.setId("buttonAddToOffer");
	this.buttonCompare.setCaption("Compare");
	this.buttonCompare.setStyleName("big");

	this.checkBoxTR.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxTR.setHeight(-1, Unit.PIXELS);
	this.verticalLayout3.addComponent(this.checkBoxTR);
	this.verticalLayout3.setComponentAlignment(this.checkBoxTR, Alignment.MIDDLE_CENTER);
	this.checkBoxInlet.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxInlet.setHeight(-1, Unit.PIXELS);
	this.verticalLayout3.addComponent(this.checkBoxInlet);
	this.verticalLayout3.setComponentAlignment(this.checkBoxInlet, Alignment.MIDDLE_CENTER);
	
	this.horizontalLayoutVentola.setMargin(new MarginInfo(false));
	this.verticalLayout3.addComponent(this.horizontalLayoutVentola);
	this.horizontalLayoutVentola.addComponent(this.labelDiametroVentola);
	this.horizontalLayoutVentola.setComponentAlignment(this.labelDiametroVentola, Alignment.MIDDLE_CENTER);
	this.horizontalLayoutVentola.addComponent(this.textFieldDiametro);
	this.horizontalLayoutVentola.setComponentAlignment(this.textFieldDiametro, Alignment.MIDDLE_CENTER);
	this.textFieldDiametro.setVisible(false);
	
	final CustomComponent verticalLayout3_spacer = new CustomComponent();
	verticalLayout3_spacer.setSizeFull();
	this.verticalLayout3.addComponent(verticalLayout3_spacer);
	this.verticalLayout3.setExpandRatio(verticalLayout3_spacer, 1.0F);
	this.checkBoxDA.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxDA.setHeight(-1, Unit.PIXELS);
	this.verticalLayout4.addComponent(this.checkBoxDA);
	this.verticalLayout4.setComponentAlignment(this.checkBoxDA, Alignment.MIDDLE_CENTER);
	this.checkBoxOutlet.setWidth(100, Unit.PERCENTAGE);
	this.checkBoxOutlet.setHeight(-1, Unit.PIXELS);
	this.verticalLayout4.addComponent(this.checkBoxOutlet);
	this.verticalLayout4.setComponentAlignment(this.checkBoxOutlet, Alignment.MIDDLE_CENTER);
	final CustomComponent verticalLayout4_spacer = new CustomComponent();
	verticalLayout4_spacer.setSizeFull();
	this.verticalLayout4.addComponent(verticalLayout4_spacer);
	this.verticalLayout4.setExpandRatio(verticalLayout4_spacer, 1.0F);
	this.buttonCondition.setSizeUndefined();

	this.verticalLayout6.addComponent(this.labelTemp);
	this.verticalLayout6.addComponent(this.labelAlt);
	this.verticalLayout6.addComponent(this.labelFreq);
	

	
	
	this.verticalLayout7.addComponent(this.buttonCondition);
	this.verticalLayout7.setComponentAlignment(this.buttonCondition, Alignment.MIDDLE_CENTER);
	this.comboBoxEsecuzioni.setWidth(100, Unit.PERCENTAGE);
	this.comboBoxEsecuzioni.setHeight(-1, Unit.PIXELS);
	this.verticalLayout7.addComponent(this.comboBoxEsecuzioni);
	this.verticalLayout7.setComponentAlignment(this.comboBoxEsecuzioni, Alignment.MIDDLE_CENTER);
	final CustomComponent verticalLayout7_spacer = new CustomComponent();
	verticalLayout7_spacer.setSizeFull();
	this.verticalLayout7.addComponent(verticalLayout7_spacer);
	this.verticalLayout7.setExpandRatio(verticalLayout7_spacer, 1.0F);
	this.comboBoxClassi.setSizeUndefined();
	this.verticalLayout8.addComponent(this.comboBoxClassi);
	this.verticalLayout8.setComponentAlignment(this.comboBoxClassi, Alignment.MIDDLE_CENTER);
	final CustomComponent verticalLayout8_spacer = new CustomComponent();
	verticalLayout8_spacer.setSizeFull();
	this.verticalLayout8.addComponent(verticalLayout8_spacer);
	this.verticalLayout8.setExpandRatio(verticalLayout8_spacer, 1.0F);
	this.verticalLayout3.setSizeFull();
	this.horizontalLayout3.addComponent(this.verticalLayout3);
	this.horizontalLayout3.setComponentAlignment(this.verticalLayout3, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.verticalLayout3, 10.0F);
	this.verticalLayout4.setSizeFull();
	this.horizontalLayout3.addComponent(this.verticalLayout4);
	this.horizontalLayout3.setComponentAlignment(this.verticalLayout4, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.verticalLayout4, 10.0F);
	this.verticalLayout5.setSizeFull();
	this.horizontalLayout3.addComponent(this.verticalLayout5);
	this.horizontalLayout3.setComponentAlignment(this.verticalLayout5, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.verticalLayout5, 10.0F);
	this.verticalLayout6.setSizeFull();
	this.horizontalLayout3.addComponent(this.verticalLayout6);
	this.horizontalLayout3.setComponentAlignment(this.verticalLayout6, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.verticalLayout6, 10.0F);
	this.verticalLayout7.setSizeFull();
	this.horizontalLayout3.addComponent(this.verticalLayout7);
	this.horizontalLayout3.setComponentAlignment(this.verticalLayout7, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.verticalLayout7, 10.0F);
	this.verticalLayout8.setSizeFull();
	this.horizontalLayout3.addComponent(this.verticalLayout8);
	this.horizontalLayout3.setComponentAlignment(this.verticalLayout8, Alignment.MIDDLE_CENTER);
	this.horizontalLayout3.setExpandRatio(this.verticalLayout8, 10.0F);
	this.horizontalLayout3.setSizeFull();
	this.panel.setContent(this.horizontalLayout3);
	this.imageLogo.setWidth(-1, Unit.PIXELS);
	this.imageLogo.setHeight(100, Unit.PERCENTAGE);
	this.verticalLayout9.addComponent(this.imageLogo);
	this.verticalLayout9.setComponentAlignment(this.imageLogo, Alignment.MIDDLE_CENTER);
	this.verticalLayout9.setExpandRatio(this.imageLogo, 10.0F);
	this.verticalLayout9.setSizeFull();
	this.panelForLogo.setContent(this.verticalLayout9);
	this.panelForLogo.setSizeFull();
	this.verticalLayout2.addComponent(this.panelForLogo);
	this.verticalLayout2.setComponentAlignment(this.panelForLogo, Alignment.MIDDLE_CENTER);
	this.verticalLayout2.setExpandRatio(this.panelForLogo, 10.0F);
	this.panel5.setSizeFull();
	this.verticalLayout2.addComponent(this.panel5);
	this.verticalLayout2.setComponentAlignment(this.panel5, Alignment.MIDDLE_CENTER);
	this.verticalLayout2.setExpandRatio(this.panel5, 10.0F);
	this.tableSerie.setSizeFull();
	this.horizontalLayout.addComponent(this.tableSerie);
	this.horizontalLayout.setComponentAlignment(this.tableSerie, Alignment.MIDDLE_CENTER);
	this.horizontalLayout.setExpandRatio(this.tableSerie, 100.0F);
	this.verticalLayout2.setSizeFull();
	this.horizontalLayout.addComponent(this.verticalLayout2);
	this.horizontalLayout.setComponentAlignment(this.verticalLayout2, Alignment.MIDDLE_CENTER);
	this.horizontalLayout.setExpandRatio(this.verticalLayout2, 30.0F);
	this.horizontalLayout.setSizeFull();
	this.panel2.setContent(this.horizontalLayout);
	this.tableVentilatori.setSizeFull();
	this.horizontalLayout2.addComponent(this.tableVentilatori);
	this.horizontalLayout2.setComponentAlignment(this.tableVentilatori, Alignment.MIDDLE_CENTER);
	this.horizontalLayout2.setExpandRatio(this.tableVentilatori, 100.0F);
	this.horizontalLayout2.setSizeFull();
	this.panel3.setContent(this.horizontalLayout2);
	this.buttonAggiungiPreventivo.setSizeUndefined();
	this.horizontalLayout4.addComponent(this.buttonAggiungiPreventivo);
	this.horizontalLayout4.setComponentAlignment(this.buttonAggiungiPreventivo, Alignment.MIDDLE_CENTER);
	this.buttonCompare.setSizeUndefined();
	this.horizontalLayout4.addComponent(this.buttonCompare);
	this.horizontalLayout4.setComponentAlignment(this.buttonCompare, Alignment.MIDDLE_CENTER);
	final CustomComponent horizontalLayout4_spacer = new CustomComponent();
	horizontalLayout4_spacer.setSizeFull();
	this.horizontalLayout4.addComponent(horizontalLayout4_spacer);
	this.horizontalLayout4.setExpandRatio(horizontalLayout4_spacer, 1.0F);
	this.horizontalLayout4.setSizeFull();
	this.panel4.setContent(this.horizontalLayout4);
	this.panel.setSizeFull();
	this.verticalLayout.addComponent(this.panel);
	this.verticalLayout.setComponentAlignment(this.panel, Alignment.MIDDLE_CENTER);
	this.verticalLayout.setExpandRatio(this.panel, 5.0F);
	this.panel2.setSizeFull();
	this.verticalLayout.addComponent(this.panel2);
	this.verticalLayout.setComponentAlignment(this.panel2, Alignment.MIDDLE_CENTER);
	this.verticalLayout.setExpandRatio(this.panel2, 17.0F);
	this.panel3.setSizeFull();
	this.verticalLayout.addComponent(this.panel3);
	this.verticalLayout.setComponentAlignment(this.panel3, Alignment.MIDDLE_CENTER);
	this.verticalLayout.setExpandRatio(this.panel3, 23.0F);
	this.panel4.setSizeFull();
	this.verticalLayout.addComponent(this.panel4);
	this.verticalLayout.setComponentAlignment(this.panel4, Alignment.MIDDLE_CENTER);
	this.verticalLayout.setExpandRatio(this.panel4, 2.0F);
	this.verticalLayout.setSizeFull();
	this.setContent(this.verticalLayout);
	this.setSizeFull();

	this.textFieldDiametro.addTextChangeListener(new FieldEvents.TextChangeListener() {
		@Override
		public void textChange(final FieldEvents.TextChangeEvent event) {
			textFieldDiametro_textChange(event);
		}

	});
	this.checkBoxTR.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.checkBoxTR_valueChange(event);
		}
	});
	this.checkBoxInlet.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.checkBoxInlet_valueChange(event);
		}
	});
	this.checkBoxDA.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.checkBoxDA_valueChange(event);
		}
	});
	this.checkBoxOutlet.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.checkBoxOutlet_valueChange(event);
		}
	});
	this.buttonCondition.addClickListener(event -> this.buttonCondition_buttonClick(event));
	this.comboBoxEsecuzioni.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.comboBoxEsecuzioni_valueChange(event);
		}
	});
	this.comboBoxClassi.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.comboBoxClassi_valueChange(event);
		}
	});
	this.tableSerie.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.tableSerie_valueChange(event);
		}
	});
	this.tableVentilatori.addValueChangeListener(new Property.ValueChangeListener() {
		@Override
		public void valueChange(final Property.ValueChangeEvent event) {
			CatalogoView.this.tableVentilatori_valueChange(event);
		}
	});
	this.buttonAggiungiPreventivo.addClickListener(event -> this.buttonAggiungiPreventivo_buttonClick(event));
	this.buttonCompare.addClickListener(event -> this.buttonCompare_buttonClick(event));
} // </generated-code>

// <generated-code name="variables">
private XdevButton buttonCondition, buttonAggiungiPreventivo, buttonCompare;
private XdevHorizontalLayout horizontalLayout3, horizontalLayout, horizontalLayout2, horizontalLayout4, horizontalLayoutVentola;
private XdevTable<CustomComponent> tableSerie, tableVentilatori;
private XdevImage imageLogo;
private XdevPanel panel, panel2, panelForLogo, panel5, panel3, panel4;
private XdevCheckBox checkBoxTR, checkBoxInlet, checkBoxDA, checkBoxOutlet;
private XdevVerticalLayout verticalLayout, verticalLayout3, verticalLayout4, verticalLayout5, verticalLayout6,
		verticalLayout7, verticalLayout8, verticalLayout2, verticalLayout9;
private XdevComboBox<CustomComponent> comboBoxEsecuzioni, comboBoxClassi;
private XdevLabel labelTemp, labelAlt, labelFreq, labelDiametroVentola;
private TextFieldDouble textFieldDiametro;
// </generated-code>


}