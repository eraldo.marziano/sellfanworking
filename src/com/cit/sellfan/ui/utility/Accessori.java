/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.utility;

import cit.sellfan.classi.Accessorio;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.TextFieldInteger;
import com.cit.sellfan.ui.view.cliente04.assiali.Cliente04AccessoriViewGruppiA0_1;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.entitycomponent.table.XdevTable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Eraldo Marziano
 */
public class Accessori {
    VariabiliGlobali l_VG;
    
    
    public Accessori() {
    }

    public void setVariabiliGlobali(VariabiliGlobali l_VG) {
        this.l_VG = l_VG;
    }
    
    public ArrayList<TableProperties> getListaProprieta () {
        ArrayList<TableProperties> lista = new ArrayList<>();
        lista.add(new TableProperties("Selected", CheckBox.class, false, this.l_VG.utilityTraduzioni.TraduciStringa("Selezionato"), 1F));
        lista.add(new TableProperties("Code", String.class, "", this.l_VG.utilityTraduzioni.TraduciStringa("Codice"), 1.2F));
        lista.add(new TableProperties("Description", String.class, "", this.l_VG.utilityTraduzioni.TraduciStringa("Descrizione"), 5F));
        lista.add(new TableProperties("Qta", TextFieldInteger.class, 0, this.l_VG.utilityTraduzioni.TraduciStringa("Qta"), 0.5F));
        lista.add(new TableProperties("Note", String.class, "", this.l_VG.utilityTraduzioni.TraduciStringa("Note"), 5F));
        return lista;
    }
    
}
