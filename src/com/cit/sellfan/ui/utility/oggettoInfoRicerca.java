/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.utility;

/**
 *
 * @author Eraldo Marziano
 */
public class oggettoInfoRicerca {
    public String TipoTrasmissione, TipoVentilatore;
    public Double DiametroMassimoGirante;
    public Integer GiriMassimiGirante, CorrettorePotenzaMotore;

    public oggettoInfoRicerca(String TipoTrasmissione, String TipoVentilatore, Double DiametroMassimoGirante, Integer GiriMassimiGirante, Integer CorrettorePotenzaMotore) {
        this.TipoTrasmissione = TipoTrasmissione;
        this.TipoVentilatore = TipoVentilatore;
        this.DiametroMassimoGirante = DiametroMassimoGirante;
        this.GiriMassimiGirante = GiriMassimiGirante;
        this.CorrettorePotenzaMotore = CorrettorePotenzaMotore;
    }
    
}
