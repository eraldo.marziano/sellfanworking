/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.utility;

import com.cit.sellfan.ui.TextFieldInteger;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.CheckBox;

/**
 *
 * @author Eraldo Marziano
 */
public class containerAccessori extends IndexedContainer{
    private static final long serialVersionUID = 136554161008035939L;
    public containerAccessori() {
        
    }
    
    public void init() {
        this.removeAllItems();
        this.addContainerProperty("Selected", CheckBox.class, false);
        this.addContainerProperty("Code", String.class, "");
        this.addContainerProperty("Description", String.class, "");
        this.addContainerProperty("Qta", TextFieldInteger.class, 0);
        this.addContainerProperty("Note", String.class, "");
		
    }
    
}
