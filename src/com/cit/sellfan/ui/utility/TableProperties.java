/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.utility;

/**
 *
 * @author Eraldo Marziano
 */
public class TableProperties {
        public Object propertyId;
        public Class<?> type;
        public Object defaultValue;
        public String traduzione;
        public float larghezza;

    public TableProperties(Object propertyId, Class<?> type, Object defaultValue, String traduzione, float larghezza) {
        this.propertyId = propertyId;
        this.type = type;
        this.defaultValue = defaultValue;
        this.traduzione = traduzione;
        this.larghezza = larghezza;
    }
        
        
}
