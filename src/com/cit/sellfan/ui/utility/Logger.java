/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cit.sellfan.ui.utility;

import cit.sellfan.classi.SelezioneDati;
import com.google.gson.Gson;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;

/**
 *
 * @author Eraldo Marziano
 */
public class Logger {
    FileOutputStream outputStream;
  
    public Logger() {
        try{
            outputStream = new FileOutputStream("samplefile.txt", true);
        } catch (Exception e) {
        }
    }
    public void log(String s) {
        byte[] strToBytes = s.getBytes();
        try {    
            outputStream.write(strToBytes);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void log(SelezioneDati sDati) {
        try {
            outputStream.write(new Gson().toJson(sDati).getBytes());
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
