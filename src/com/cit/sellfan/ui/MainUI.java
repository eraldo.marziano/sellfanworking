
package com.cit.sellfan.ui;

import com.cit.sellfan.business.VariabiliGlobali;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.xdev.ui.XdevUI;
import com.xdev.ui.navigation.XdevNavigator;

@Push
@JavaScript({"vaadin://jquery/jquery-3.2.1.js"})
@Theme("SellFan")
@Title("Sellfan")
public class MainUI extends XdevUI {
	
		
	private final VariabiliGlobali l_VG = new VariabiliGlobali();
	
	public MainUI() {
		super();

	}

	public VariabiliGlobali getVariabiliGlobali() {
		return this.l_VG;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(final VaadinRequest request) {
		// http://localhost:8080/TestParameters/?param0=pippo0&param1=pippo1
		this.initUI();
		this.l_VG.attributi = request.getAttributeNames();
		this.l_VG.parametri = request.getParameterMap();
		final String str = request.getParameter("menu");
		if (str != null && str.toLowerCase().equals("true")) {
			this.l_VG.menuStyle = true;
		}
		//this.l_VG.str0 = request.getParameter("param0");
		//this.l_VG.str1 = request.getParameter("param1");
		this.l_VG.remoteUser = request.getRemoteUser();
		this.l_VG.principal = request.getUserPrincipal();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.navigator = new XdevNavigator(this, this);
	
		this.navigator.addView("", GetUserView.class);
		this.navigator.addView("MainView", MainView.class);
	
		this.setSizeFull();
	} // </generated-code>

	// <generated-code name="variables">
	private XdevNavigator navigator; // </generated-code>
	
}