package com.cit.sellfan.ui;

import cit.classi.Lingua;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import com.cit.sellfan.business.GraficoPrestazioniCurveWeb;
import com.cit.sellfan.business.VariabiliGlobali;
import com.cit.sellfan.ui.pannelli.layoutLingue;
import com.cit.sellfan.ui.pannelli.pannelloPagineDataSheet;
import com.cit.sellfan.ui.template.TemplateImage;
import com.cit.sellfan.ui.view.AccessoriImageView;
import com.cit.sellfan.ui.view.BrowserView;
import com.cit.sellfan.ui.view.CatalogoView;
import com.cit.sellfan.ui.view.CompareView;
import com.cit.sellfan.ui.view.DebugView;
import com.cit.sellfan.ui.view.DimensionView;
import com.cit.sellfan.ui.view.Ingombro2ImgView;
import com.cit.sellfan.ui.view.MultigiriView;
import com.cit.sellfan.ui.view.PrestazioniView;
import com.cit.sellfan.ui.view.PreventiviView;
import com.cit.sellfan.ui.view.ProgressView;
import com.cit.sellfan.ui.view.RiepilogoView;
import com.cit.sellfan.ui.view.SearchView;
import com.cit.sellfan.ui.view.SetupView;
import com.cit.sellfan.ui.view.SpettroSonoroView;
import com.cit.sellfan.ui.view.cliente04.Cliente04AccessoriViewDonaldson;
import com.cit.sellfan.ui.view.cliente04.assiali.Cliente04AccessoriViewAssiali;
import com.cit.sellfan.ui.view.cliente04.centrifighi.Cliente04AccessoriViewCentrifughi;
import com.cit.sellfan.ui.view.cliente04.centrifighi.Cliente04AccessoriViewFusione;
import com.cit.sellfan.ui.view.mobile.MobileCatalogoView;
import com.cit.sellfan.ui.view.mobile.MobileDimensionView;
import com.cit.sellfan.ui.view.mobile.MobilePrestazioniView;
import com.cit.sellfan.ui.view.mobile.MobileSearchView;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.xdev.res.ApplicationResource;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevImage;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevMenuBar;
import com.xdev.ui.XdevMenuBar.XdevMenuItem;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.navigation.Navigation;

import cit.myjavalib.jDisplayImage;
import cit.sellfan.Costanti;
import cit.sellfan.classi.DataSheet;
import cit.sellfan.classi.PresetRicerca;
import cit.sellfan.classi.SelezioneDati;
import cit.sellfan.classi.Serie;
import cit.sellfan.classi.grafici.GraficoPrestazioniCurveParameter;
import cit.sellfan.classi.ventilatore.Ventilatore;
import cit.sellfan.classi.ventilatore.VentilatoreFisica;
import cit.sellfan.personalizzazioni.cliente01.MistralDataSheet;
import cit.sellfan.personalizzazioni.cliente04.Cliente04DataSheet;
import cit.sellfan.personalizzazioni.cliente04.Cliente04VentilatoreCampiCliente;
import com.cit.sellfan.ui.view.SpettroSonoroViewMistral;
import com.vaadin.data.Property;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import de.steinwedel.messagebox.MessageBox;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.imageio.ImageIO;

public class MainView extends XdevView {

    private boolean changingLanguage = false;
    private VariabiliGlobali l_VG;
    private final TemplateImage imagehome = new TemplateImage();
    public DebugView debug = new DebugView();
    public SetupView setup = new SetupView();
    public CatalogoView catalogo = new CatalogoView();
    public PrestazioniView prestazioni = new PrestazioniView();
    public Cliente04AccessoriViewCentrifughi accessori04Centrifughi = null;
    public Cliente04AccessoriViewDonaldson accessori04Donaldson = null;
    public Cliente04AccessoriViewAssiali accessori04Assiali = null;
    public Cliente04AccessoriViewFusione accessori04Fusione = null;
    public AccessoriImageView accessoriDefault = null;
    public DimensionView dimensioniAngolo = new DimensionView();
    public DimensionView dimensioni = new DimensionView();
    public PreventiviView preventivi = new PreventiviView();
    public SearchView ricerca = new SearchView();
    public CompareView confronto = new CompareView();
    public MultigiriView multigiri = new MultigiriView();
    public SpettroSonoroView spettrosonoro = new SpettroSonoroView();
    public SpettroSonoroViewMistral spettrosonoromistral = new SpettroSonoroViewMistral();
    public RiepilogoView riepilogo = new RiepilogoView();
    public Ingombro2ImgView ingombro2img = new Ingombro2ImgView();

    public MobileCatalogoView catalogoMobile = new MobileCatalogoView();
    public MobileSearchView ricercaMobile = new MobileSearchView();
    public MobileDimensionView dimensioniAngoloMobile = new MobileDimensionView();
    public MobileDimensionView dimensioniMobile = new MobileDimensionView();
    public MobilePrestazioniView prestazioniMobile = new MobilePrestazioniView();

    private final String coloreBottone = "verde";
    private final String coloreBottoneSelezionato = "verdeselezionato";
    private FileDownloader fileDownloader;
    private MessageBox msgBox;
    //private final ArrayList<XdevButton> bottoniLingue = new ArrayList<>();
    private final layoutLingue pannelloLingue = new layoutLingue();
    private final layoutLingue pannelloLingueMobile = new layoutLingue();

    private final XdevMenuBar.Command commandMenu = new XdevMenuBar.Command() {
        @Override
        public void menuSelected(final MenuItem selectedItem) {
            //Notification.show(selectedItem.getText());
            if (selectedItem.getId() == MainView.this.itemLogout.getId()) {
                commandLogout();
            } else if (selectedItem.getId() == MainView.this.itemSetup.getId()) {
                commandSetup();
//			} else if (selectedItem.getId() == MainView.this.itemLayout.getId()) {
//				commandLayout();
            } else if (selectedItem.getId() == MainView.this.itemCatalogo.getId()) {
                commandCatalogo();
            } else if (selectedItem.getId() == MainView.this.itemRicerca.getId()) {
                commandRicerca();
            } else if (selectedItem.getId() == MainView.this.itemPreventivi.getId()) {
                commandPreventivi();
            } else if (selectedItem.getId() == MainView.this.itemConfronto.getId()) {
                commandConfronto();
            } else if (selectedItem.getId() == MainView.this.itemPrestazioni.getId()) {
                commandPrestazioni();
            } else if (selectedItem.getId() == MainView.this.itemDimensioniAngolo.getId()) {
                commandDimensioniAngolo();
            } else if (selectedItem.getId() == MainView.this.itemDimensioni.getId()) {
                commandDimensioniNoAngolo();
            } else if (selectedItem.getId() == MainView.this.itemAccessori.getId()) {
                commandAccessori();
            } else if (selectedItem.getId() == MainView.this.itemMultigiri.getId()) {
                commandMultigiri();
            } else if (selectedItem.getId() == MainView.this.itemRumore.getId()) {
                commandSpettroSonoro();
            } else if (selectedItem.getId() == MainView.this.itemItaliano.getId()) {
                commandItaliano();
            } else if (selectedItem.getId() == MainView.this.itemInglese.getId()) {
                commandInglese();
            }
        }
    };

    /**
     *
     */
    public MainView() {
        super();
        this.initUI();
        this.horizontalLayoutDatiTecnici.setHeight(25, Unit.PIXELS);
        this.labelMemo.setValue("");
        buildMenu();
        this.dimensioni.angoloEnabled = false;
        this.dimensioniMobile.angoloEnabled = false;
        this.buttonLauout.setVisible(false);
    }

    private void buildMenu() {
        this.itemLogout.setCommand(this.commandMenu);
        this.itemSetup.setCommand(this.commandMenu);
//		this.itemLayout.setCommand(this.commandMenu);
        this.itemCatalogo.setCommand(this.commandMenu);
        this.itemRicerca.setCommand(this.commandMenu);
        this.itemPreventivi.setCommand(this.commandMenu);
        this.itemConfronto.setCommand(this.commandMenu);
        this.itemPrestazioni.setCommand(this.commandMenu);
        this.itemDimensioniAngolo.setCommand(this.commandMenu);
        this.itemAccessori.setCommand(this.commandMenu);
        this.itemMultigiri.setCommand(this.commandMenu);
        this.itemRumore.setCommand(this.commandMenu);
        this.itemItaliano.setCommand(this.commandMenu);
        this.itemInglese.setCommand(this.commandMenu);
        this.itemItaliano.setCheckable(true);
        this.itemInglese.setCheckable(true);
    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
        super.enter(event);
        this.l_VG = ((MainUI) this.getUI()).getVariabiliGlobali();
        this.l_VG.MainView = this;
        this.l_VG.editVentola = false;
        if (l_VG.isDonaldson()) {
            comboLingua.setVisible(false);
        }
        this.pannelloLingue.setVariabiliGlobali(this.l_VG);
        this.pannelloLingueMobile.setVariabiliGlobali(this.l_VG);
        this.pannelloLingue.setSizeUndefined();
        //this.horizontalLayoutComandi.replaceComponent(this.pannelloLingueGosht, this.pannelloLingue);
        //this.horizontalLayoutComandi.setComponentAlignment(this.pannelloLingue, Alignment.MIDDLE_CENTER);
//		this.pannelloLingueMobile.setSizeUndefined();
//		this.horizontalLayoutDatiTecnici.replaceComponent(this.pannelloLingueMobileGosht, this.pannelloLingueMobile);
//		this.horizontalLayoutDatiTecnici.setComponentAlignment(this.pannelloLingueMobile, Alignment.MIDDLE_CENTER);
        Page.getCurrent().setTitle(this.l_VG.utilityCliente.titoloProgrammaCliente);
        if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
            if (l_VG.isDonaldson()) {
                this.accessori04Donaldson = new Cliente04AccessoriViewDonaldson();
                this.accessori04Donaldson.setVariabiliGlobali(l_VG);
            } else {
                this.accessori04Centrifughi = new Cliente04AccessoriViewCentrifughi();
                this.accessori04Centrifughi.setVariabiliGlobali(this.l_VG);
                this.accessori04Assiali = new Cliente04AccessoriViewAssiali();
                this.accessori04Assiali.setVariabiliGlobali(this.l_VG);
                this.accessori04Fusione = new Cliente04AccessoriViewFusione();
                this.accessori04Fusione.setVariabiliGlobali(this.l_VG);
            }
            if (this.l_VG.currentUserSaaS.idSottoClienteIndex == 7) {
                this.l_VG.Paths.rootTemplateOfferta = "mztemplate/fa/";
            }
        } else {
            this.accessoriDefault = new AccessoriImageView();
            this.accessoriDefault.setVariabiliGlobali(this.l_VG);
        }
        this.labelVersion.setValue(this.l_VG.dataVersione);
        this.l_VG.getBrowserAspectRatio();
        this.l_VG.getBrowserAspectRatio();
        if (this.l_VG.forcedDesktop) {
            this.l_VG.MobileMode = false;
        } else {
            if (this.l_VG.browserWidth < this.l_VG.minBrowserWidth || this.l_VG.browserHeight < this.l_VG.minBrowserHeight) {
                this.l_VG.MobileMode = true;
                this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneMobileMode, "Risoluzione " + Integer.toString(this.l_VG.browserWidth) + "x" + Integer.toString(this.l_VG.browserHeight));
            } else {
                this.l_VG.MobileMode = false;
            }
        }
        /*
		if (this.l_VG.MobileMode) {
			this.pannelloLingue.setVisible(false);
			this.pannelloLingueMobile.setVisible(true);
		} else {
			this.pannelloLingue.setVisible(true);
			this.pannelloLingueMobile.setVisible(false);
		}
         */
        this.pannelloLingueMobile.setVisible(false);
        this.l_VG.ElencoView[this.l_VG.MainViewIndex] = this;
        this.l_VG.ElencoMobileView[this.l_VG.MainViewIndex] = this;
        this.buttonDebug.setVisible(this.l_VG.debugFlag);
        /*
	    Page page = UI.getCurrent().getPage();
	    page.addBrowserWindowResizeListener(new BrowserWindowResizeListener() {
	        public void browserWindowResized(BrowserWindowResizeEvent event) {
	    		l_VG.getBrowserAspectRatio();
    			//l_VG.MainView.setMemo(Integer.toString(l_VG.browserWidth));
	    		if (!l_VG.LayoutForced) {
	    			l_VG.MobileMode = false;
	    			if (l_VG.browserWidth < l_VG.minBrowserWidth || l_VG.browserHeight < l_VG.minBrowserHeight) {
	    				l_VG.MobileMode = true;
	    			}
	    		}
	    		resizeView();
	        }
	    });
         */
        this.l_VG.currentGraficoPrestazioniCurveWeb = new GraficoPrestazioniCurveWeb(this.l_VG);
        if (this.l_VG.utilityCliente.idClienteIndex == 1) {
            final GraficoPrestazioniCurveParameter graficoPrestazioniCurveParameter = new GraficoPrestazioniCurveParameter();
            graficoPrestazioniCurveParameter.labelStyle = 0;
            this.l_VG.currentGraficoPrestazioniCurveWeb.graficoPrestazioniCurveParameter = graficoPrestazioniCurveParameter;
        }
        final StreamResource streamImageSfondo = this.l_VG.getStreamResourceForImageFile(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineHome);
        this.imageSfondo.setSource(streamImageSfondo);
//		this.imagehome.setSource(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineHome);
//		this.gridLayout.removeComponent(0, 0);
//		this.gridLayout.addComponent(this.imagehome, 0, 0);
        this.debug.setVariabiliGlobali(this.l_VG);
        this.setup.setVariabiliGlobali(this.l_VG);
        this.catalogo.setVariabiliGlobali(this.l_VG);
        this.prestazioni.setVariabiliGlobali(this.l_VG);
        this.ricerca.setVariabiliGlobali(this.l_VG);
        this.ricerca.init();
        this.preventivi.setVariabiliGlobali(this.l_VG);
        this.preventivi.initPreventivi();
        this.confronto.setVariabiliGlobali(this.l_VG);
        this.confronto.initCompare();
        this.dimensioniAngolo.setVariabiliGlobali(this.l_VG);
        this.dimensioni.setVariabiliGlobali(this.l_VG);
        this.spettrosonoro.setVariabiliGlobali(this.l_VG);
        this.multigiri.setVariabiliGlobali(this.l_VG);
        this.riepilogo.setVariabiliGlobali(this.l_VG);
        this.ingombro2img.setVariabiliGlobali(this.l_VG);
        this.catalogoMobile.setVariabiliGlobali(this.l_VG);
        this.ricercaMobile.setVariabiliGlobali(this.l_VG);
        this.dimensioniAngoloMobile.setVariabiliGlobali(this.l_VG);
        this.dimensioniMobile.setVariabiliGlobali(this.l_VG);
        this.prestazioniMobile.setVariabiliGlobali(this.l_VG);
        abilitaTab();
        Nazionalizza();
//                if (l_VG.utilityCliente.idSottoClienteIndex !=0) {
//                    MessageBox warning = MessageBox.createInfo();
//                    warning.withOkButton();
//                    warning.withMessage(l_VG.utilityTraduzioni.TraduciStringaOutput("prezzi di listino non aggiornati"));
//                    warning.open();
//                }

        this.l_VG.preventivoCurrent = null;
        this.l_VG.preventivoCurrentIndex = -1;
        this.l_VG.ElencoVentilatoriFromPreventivo.clear();
        if (l_VG.presetRicerca != null) {
            commandRicerca();
        }
    }

    public void resetMainView() {
        this.preventivi.initPreventivi();
        this.l_VG.preventivoCurrent = null;
        this.l_VG.preventivoCurrentIndex = -1;
        this.l_VG.ElencoVentilatoriFromPreventivo.clear();
        this.l_VG.SelezioneDati = new SelezioneDati();
        if (this.l_VG.MobileMode) {
            this.catalogoMobile.resetView();
            this.ricercaMobile.resetView();
        } else {
            this.catalogo.resetView();
            this.ricerca.resetView();
        }
        this.confronto.resetView();
        resetColoreSelezionato();
        this.gridLayout.removeComponent(0, 0);
        this.gridLayout.addComponent(this.imagehome, 0, 0);
        this.l_VG.CurrentViewIndex = this.l_VG.MainViewIndex;
    }

    public void Nazionalizza() {
        this.menuBar.setVisible(this.l_VG.ParametriVariHTML.menuStyleEnabled);
        this.horizontalLayoutComandi.setVisible(!this.l_VG.ParametriVariHTML.menuStyleEnabled);
        this.horizontalLayoutDatiTecnici.setVisible(!this.l_VG.ParametriVariHTML.menuStyleEnabled);
        if (l_VG.utilityCliente.idSottoClienteIndex == 0) {
            comboLingua.addItems("Italiano", "English");
        } else {
            if (l_VG.utilityCliente.idSottoClienteIndex == 10) {
                comboLingua.addItems("Donaldson");
            } else {
                comboLingua.addItems("Italiano", "English", "Français", "Deutsche", "Español");
            }
        }
        if (l_VG.utilityCliente.idSottoClienteIndex == 10) {
            Lingua lingua = null;
            for (int i = 0; i < this.l_VG.elencoLingue.size(); i++) {
                if (this.l_VG.elencoLingue.get(i).id.equals("DO")) {
                    lingua = this.l_VG.elencoLingue.get(i);
                    break;
                }
            }
            if (lingua == null) {
                return;
            }
            this.l_VG.currentCitFont.linguaDisplay = (Lingua) lingua.clone();
            this.l_VG.currentCitFont.idLinguaDisplay = lingua.id;
            this.l_VG.currentCitFont.linguaOutput = (Lingua) lingua.clone();
            this.l_VG.currentCitFont.idLinguaOutput = lingua.id;
            this.l_VG.utilityTraduzioni.setLingua(this.l_VG.currentCitFont.linguaDisplay.colonnaLinguaForTraduzioni, this.l_VG.currentCitFont.linguaDisplay.speciale);
            this.l_VG.utilityTraduzioni.setLinguaOutput(this.l_VG.currentCitFont.linguaOutput.colonnaLinguaForTraduzioni, this.l_VG.currentCitFont.linguaOutput.speciale);
            this.l_VG.dbConfig.storeFont(this.l_VG.currentCitFont);
        }
        this.buttonSetup.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Opzioni"));
        this.buttonCatalogo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Catalogo"));
        this.buttonRicerca.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ricerca"));
        this.buttonPreventivi.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Preventivi"));
        this.buttonConfronto.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Confronto"));
        this.buttonPrestazioni.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Prestazioni"));
        this.buttonAccessori.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Accessori"));
        this.buttonDimensioniAngolo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Dimensioni"));
        this.buttonDimensioniNoAngolo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Dimensioni"));
        this.buttonSpettroSonoro.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Spettro Sonoro"));
        this.buttonAddPreventivo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Aggiungi al Preventivo"));
        this.buttonMultigiri.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Multigiri"));
        this.buttonRiepilogo.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Riepilogo"));
        this.buttonIngombro2Img.setCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Ingombro"));
        this.setup.Nazionalizza();
        this.catalogo.Nazionalizza();
        this.prestazioni.Nazionalizza();
        this.ricerca.Nazionalizza();
        this.preventivi.Nazionalizza();
        this.confronto.Nazionalizza();
        if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
            if (l_VG.isDonaldson()) {
                this.accessori04Donaldson.Nazionalizza();
            } else {
                this.accessori04Centrifughi.Nazionalizza();
                this.accessori04Fusione.Nazionalizza();
                this.accessori04Assiali.Nazionalizza();
            }
        } else {
            this.accessoriDefault.Nazionalizza();
        }
        this.dimensioniAngolo.Nazionalizza();
        this.dimensioni.Nazionalizza();
        this.multigiri.Nazionalizza();
        this.spettrosonoro.Nazionalizza();
        this.riepilogo.Nazionalizza();
        this.ingombro2img.Nazionalizza();
        this.catalogoMobile.Nazionalizza();
        this.ricercaMobile.Nazionalizza();
        this.dimensioniAngoloMobile.Nazionalizza();
        this.dimensioniMobile.Nazionalizza();
        this.prestazioniMobile.Nazionalizza();
        setBandiere();
        abilitaTab();
        //itemFileGroup.setText(l_VG.utilityTraduzioni.TraduciStringa("File"));
        this.itemSetup.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Opzioni"));
        //itemLayout.setText(l_VG.utilityTraduzioni.TraduciStringa("Layout"));
        this.itemCatalogo.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Catalogo"));
        this.itemRicerca.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Ricerca"));
        this.itemPreventivi.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Preventivi"));
        this.itemConfronto.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Confronto"));
        this.itemDatiTecniciGroup.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Dati Tecnici"));
        this.itemPrestazioni.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Prestazioni"));
        this.itemDimensioniAngolo.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Dimensioni"));
        this.itemAccessori.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Accessori"));
        this.itemMultigiri.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Multigiri"));
        this.itemRumore.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Spettro Sonoro"));
        this.itemLinguaGroup.setText(this.l_VG.utilityTraduzioni.TraduciStringa("Lingua"));
    }

    private void commandCatalogo() {
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.CatalogoViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.CatalogoViewIndex;
        resetColoreSelezionato();
        this.buttonCatalogo.removeStyleName(this.coloreBottone);
        this.buttonCatalogo.addStyleName(this.coloreBottoneSelezionato);
        /*
		CatalogoView newView = null;
		MobileCatalogoView newMobileView = null;
		if (l_VG.MobileMode) {
			newMobileView = (MobileCatalogoView)l_VG.ElencoMobileView[l_VG.CatalogoViewIndex];
			if (newMobileView == null) return;
		} else {
			newView = (CatalogoView)l_VG.ElencoView[l_VG.CatalogoViewIndex];
			if (newView == null) return;
		}
         */
        if (this.l_VG.MobileMode) {
            this.gridLayout.removeComponent(0, 0);
            this.catalogoMobile.setSizeFull();
            this.gridLayout.addComponent(this.catalogoMobile, 0, 0);
            if (this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaCatalogo) {
                this.catalogoMobile.buildTableSerie();
                setVentilatoreCorrenteFromCatalogo(null);
            } else if (this.l_VG.ventilatoreCambiato) {
                this.catalogoMobile.aggiornaTableVentilatori();
                this.l_VG.ventilatoreCambiato = false;
            }
        } else {
            this.gridLayout.removeComponent(0, 0);
            this.catalogo.setSizeFull();
            this.gridLayout.addComponent(this.catalogo, 0, 0);
            if (this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaCatalogo) {
                this.catalogo.buildTableSerie();
                setVentilatoreCorrenteFromCatalogo(null);
            } else if (this.l_VG.ventilatoreCambiato) {
                catalogo.buildTableSerie();
                //this.catalogo.aggiornaTableVentilatori();
                this.l_VG.ventilatoreCambiato = false;
            }
        }
    }

    private void commandRicerca() {
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.SearchViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.SearchViewIndex;
        resetColoreSelezionato();
        this.buttonRicerca.removeStyleName(this.coloreBottone);
        this.buttonRicerca.addStyleName(this.coloreBottoneSelezionato);
        /*
		SearchView newView = null;
		MobileSearchView newMobileView = null;
		if (l_VG.MobileMode) {
			newMobileView = (MobileSearchView)l_VG.ElencoMobileView[l_VG.SearchViewIndex];
			if (newMobileView == null) return;
		} else {
			newView = (SearchView)l_VG.ElencoView[l_VG.SearchViewIndex];
			if (newView == null) return;
		}
         */
        if (this.l_VG.MobileMode) {
            this.gridLayout.removeComponent(0, 0);
            this.ricercaMobile.setSizeFull();
            this.ricercaMobile.setVariabiliGlobali(this.l_VG);
            this.gridLayout.addComponent(this.ricercaMobile, 0, 0);
            if (this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaRicerca) {
                this.ricercaMobile.init();
                this.ricercaMobile.buildTableRisultati();
                setVentilatoreCorrenteFromRicerca(null);
            } else if (this.l_VG.ventilatoreCambiato) {
                this.ricercaMobile.aggiornaTableRisultati();
                this.l_VG.ventilatoreCambiato = false;
            }
        } else {
            this.gridLayout.removeComponent(0, 0);
            this.ricerca.setSizeFull();
            this.gridLayout.addComponent(this.ricerca, 0, 0);
            //Notification.show(Integer.toString(l_VG.VentilatoreCurrentFrom)+"  "+Integer.toString(l_VG.VentilatoreCurrentFromRicerca)+"  "+Boolean.toString(l_VG.ventilatoreCambiato));
            if (this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaRicerca) {
                this.ricerca.init();
                this.ricerca.buildTableRisultati();
                setVentilatoreCorrenteFromRicerca(null);
            } else if (this.l_VG.ventilatoreCambiato) {
                this.ricerca.aggiornaTableRisultati();
                this.l_VG.ventilatoreCambiato = false;
            }
        }
    }

    private void commandPreventivi() {
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.PreventiviViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.PreventiviViewIndex;
        resetColoreSelezionato();
        this.buttonPreventivi.removeStyleName(this.coloreBottone);
        this.buttonPreventivi.addStyleName(this.coloreBottoneSelezionato);
        this.gridLayout.removeComponent(0, 0);
        this.preventivi.setSizeFull();
        if (this.l_VG.VentilatoreCurrentFromVieneDa != Costanti.vieneDaPreventivo) {
            this.preventivi.initPreventivi();
        }
        this.gridLayout.addComponent(this.preventivi, 0, 0);
    }

    private void commandConfronto() {
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.CompareViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.CompareViewIndex;
        resetColoreSelezionato();
        this.buttonConfronto.removeStyleName(this.coloreBottone);
        this.buttonConfronto.addStyleName(this.coloreBottoneSelezionato);
        try {
            this.confronto.initCompare();
            this.gridLayout.removeComponent(0, 0);
            this.confronto.setSizeFull();
            this.gridLayout.addComponent(this.confronto, 0, 0);
        } catch (final Exception e) {
            //Notification.show(e.toString());
        }
    }

    private void commandLayout() {
        final BrowserView pannello = new BrowserView();
        pannello.setVariabiliGlobali(this.l_VG);
        this.msgBox = MessageBox.create();
        this.msgBox.withMessage(pannello);
        this.msgBox.withAbortButton();
        this.msgBox.withOkButton(() -> {
            this.l_VG.MobileMode = pannello.getMobileRequest();
            this.l_VG.LayoutForced = true;
            //resizeView();
        });
        this.msgBox.open();
    }

    private void commandSetup() {
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.SetupViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.SetupViewIndex;
        resetColoreSelezionato();
        this.buttonSetup.removeStyleName(this.coloreBottone);
        this.buttonSetup.addStyleName(this.coloreBottoneSelezionato);
        try {
            this.setup.initSetup();
            this.gridLayout.removeComponent(0, 0);
            this.setup.setSizeFull();
            this.gridLayout.addComponent(this.setup, 0, 0);
        } catch (final Exception e) {
            e.printStackTrace();
            Notification.show(e.getMessage());
        }
        setVentilatoreCorrenteFromCatalogo(null);
    }

    private void commandLogout() {
        if (this.preventivi.isOnEdit()) {
            return;
        }
        //this.horizontalLayoutComandi.replaceComponent(this.pannelloLingue, this.pannelloLingueGosht);
        //this.horizontalLayoutDatiTecnici.replaceComponent(this.pannelloLingueMobile, this.pannelloLingueMobileGosht);
        this.l_VG.CurrentViewIndex = this.l_VG.MainViewIndex;
        this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneLogOut);
        Navigation.to("").navigate();
    }

    private void commandDebug() {
        resetColoreSelezionato();
        try {
            this.gridLayout.removeComponent(0, 0);
            this.debug.setSizeFull();
            this.gridLayout.addComponent(this.debug, 0, 0);
        } catch (final Exception e) {
            //Notification.show(e.toString());
        }
    }

    private void commandPrestazioni() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.PrestazioniViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.PrestazioniViewIndex;
        resetColoreSelezionato();
        this.buttonPrestazioni.removeStyleName(this.coloreBottone);
        this.buttonPrestazioni.addStyleName(this.coloreBottoneSelezionato);
        try {
            //l_VG.MainView.setMemo("prestazioni.setVentilatoreCurrent");
            //Notification.show("buttonPrestazioni_buttonClick  "+l_VG.VentilatoreCurrent.Classe);
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            if (this.l_VG.MobileMode) {
                this.prestazioniMobile.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.prestazioniMobile.setSizeFull();
                this.gridLayout.addComponent(this.prestazioniMobile, 0, 0);
            } else {
                this.prestazioni.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.prestazioni.setSizeFull();
                this.gridLayout.addComponent(this.prestazioni, 0, 0);
            }
        } catch (final Exception e) {
            //Notification.show(e.toString());
        }
    }

    private void commandIngombro2Img() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.Ingombro2ImgViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.Ingombro2ImgViewIndex;
        resetColoreSelezionato();
        this.buttonIngombro2Img.removeStyleName(this.coloreBottone);
        this.buttonIngombro2Img.addStyleName(this.coloreBottoneSelezionato);
        try {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            this.ingombro2img.setVentilatoreCurrent();
            this.gridLayout.removeComponent(0, 0);
            this.ingombro2img.setSizeFull();
            this.gridLayout.addComponent(this.ingombro2img, 0, 0);
        } catch (final Exception e) {
            Notification.show(e.toString());
        }
    }

    private void commandRiepilogo() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.RiepilogoViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.RiepilogoViewIndex;
        if (this.l_VG.MobileMode) {
            this.prestazioniMobile.setVentilatoreCurrent();
        } else {
            this.prestazioni.setVentilatoreCurrent();
        }
        resetColoreSelezionato();
        this.buttonRiepilogo.removeStyleName(this.coloreBottone);
        this.buttonRiepilogo.addStyleName(this.coloreBottoneSelezionato);
        try {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            this.riepilogo.setVentilatoreCurrent();
            this.gridLayout.removeComponent(0, 0);
            this.riepilogo.setSizeFull();
            this.gridLayout.addComponent(this.riepilogo, 0, 0);
        } catch (final Exception e) {
            Notification.show(e.toString());
        }
    }

    private void commandDimensioniAngolo() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.DimensioniAngoloViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.DimensioniAngoloViewIndex;
        resetColoreSelezionato();
        this.buttonDimensioniAngolo.removeStyleName(this.coloreBottone);
        this.buttonDimensioniAngolo.addStyleName(this.coloreBottoneSelezionato);
        try {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            if (this.l_VG.MobileMode) {
                this.dimensioniAngoloMobile.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.dimensioniAngoloMobile.setSizeFull();
                this.gridLayout.addComponent(this.dimensioniAngoloMobile, 0, 0);
            } else {
                this.dimensioniAngolo.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.dimensioniAngolo.setSizeFull();
                this.gridLayout.addComponent(this.dimensioniAngolo, 0, 0);
            }
        } catch (final Exception e) {
            //Notification.show(e.toString());
        }
    }

    private void commandDimensioniNoAngolo() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.DimensioniViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.DimensioniViewIndex;
        resetColoreSelezionato();
        this.buttonDimensioniAngolo.removeStyleName(this.coloreBottone);
        this.buttonDimensioniAngolo.addStyleName(this.coloreBottoneSelezionato);
        try {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            if (this.l_VG.MobileMode) {
                this.dimensioniMobile.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.dimensioniMobile.setSizeFull();
                this.gridLayout.addComponent(this.dimensioniMobile, 0, 0);
            } else {
                this.dimensioni.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.dimensioni.setSizeFull();
                this.gridLayout.addComponent(this.dimensioni, 0, 0);
            }
        } catch (final Exception e) {
            //Notification.show(e.toString());
        }
    }

    public void setAccessoriVentilatoreCurrent() {
        try {
            if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
                if (l_VG.isDonaldson()) {
                    this.accessori04Donaldson.setVentilatoreCurrent();
                } else {
                    if (l_vcc04.Tipo.equals("centrifugo")) {
                        this.accessori04Centrifughi.setVentilatoreCurrent();
                    } else if (l_vcc04.Tipo.equals("fusione")) {
                        this.accessori04Fusione.setVentilatoreCurrent();
                    } else if (l_vcc04.Tipo.equals("assiale")) {
                        this.accessori04Assiali.setVentilatoreCurrent();
                    } else {
                        return;
                    }
                }
            } else {
                this.accessoriDefault.setVentilatoreCurrent();
            }
        } catch (final Exception e) {
            //this.l_VG.MainView.setMemo(e.toString());
        }
    }

    private void commandAccessori() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.AccessoriViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.AccessoriViewIndex;
        resetColoreSelezionato();
        this.buttonAccessori.removeStyleName(this.coloreBottone);
        this.buttonAccessori.addStyleName(this.coloreBottoneSelezionato);
        try {
            if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                if (this.l_VG.isDonaldson()) {
                    final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
                    this.gridLayout.removeComponent(0, 0);
                    this.accessori04Donaldson.setVentilatoreCurrent();
                    this.accessori04Donaldson.setSizeFull();
                    this.gridLayout.addComponent(this.accessori04Donaldson, 0, 0);
                } else {
                    final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
                    if (l_vcc04.Tipo.equals("centrifugo")) {
                        this.gridLayout.removeComponent(0, 0);
                        this.accessori04Centrifughi.setVentilatoreCurrent();
                        this.accessori04Centrifughi.setSizeFull();
                        this.gridLayout.addComponent(this.accessori04Centrifughi, 0, 0);
                    } else if (l_vcc04.Tipo.equals("fusione")) {
                        this.gridLayout.removeComponent(0, 0);
                        this.accessori04Fusione.setVentilatoreCurrent();
                        this.accessori04Fusione.setSizeFull();
                        this.gridLayout.addComponent(this.accessori04Fusione, 0, 0);
                    } else if (l_vcc04.Tipo.equals("assiale")) {
                        this.gridLayout.removeComponent(0, 0);
                        this.accessori04Assiali.setVentilatoreCurrent();
                        this.accessori04Assiali.setSizeFull();
                        this.gridLayout.addComponent(this.accessori04Assiali, 0, 0);
                    } else {
                        return;
                    }
                }
            } else {
                this.gridLayout.removeComponent(0, 0);
                this.accessoriDefault.setSizeFull();
                this.accessoriDefault.setVentilatoreCurrent();
                this.gridLayout.addComponent(this.accessoriDefault, 0, 0);
                this.l_VG.ElencoView[this.l_VG.AccessoriViewIndex] = this.accessoriDefault;
            }
        } catch (final Exception e) {
            Notification.show("buttonAccessori " + e.toString());
        }
    }

    private void commandMultigiri() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.MultigiriViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.MultigiriViewIndex;
        this.l_VG.VentilatoreCurrent.ApertoMultigiri = true;
        resetColoreSelezionato();
        this.buttonMultigiri.removeStyleName(this.coloreBottone);
        this.buttonMultigiri.addStyleName(this.coloreBottoneSelezionato);
        try {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            this.multigiri.setVentilatoreCurrent();
            this.gridLayout.removeComponent(0, 0);
            this.multigiri.setSizeFull();
            this.gridLayout.addComponent(this.multigiri, 0, 0);
        } catch (final Exception e) {
            //Notification.show(e.toString());
        }
    }

    private void commandSpettroSonoro() {
        this.buttonDownloadDataSheet.setEnabled(false);
        if (this.preventivi.isOnEdit()) {
            return;
        }
        if (this.l_VG.CurrentViewIndex == this.l_VG.SpettroSonoroViewIndex) {
            return;
        }
        this.l_VG.CurrentViewIndex = this.l_VG.SpettroSonoroViewIndex;
        this.l_VG.VentilatoreCurrent.ApertoSpettro = true;
        resetColoreSelezionato();
        this.buttonSpettroSonoro.removeStyleName(this.coloreBottone);
        this.buttonSpettroSonoro.addStyleName(this.coloreBottoneSelezionato);

        if (this.l_VG.utilityCliente.idCliente.equals("mistral")) {
            try {
                this.spettrosonoromistral.setVariabiliGlobali(this.l_VG);
                this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
                this.spettrosonoromistral.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.spettrosonoromistral.setSizeFull();
                this.gridLayout.addComponent(this.spettrosonoromistral, 0, 0);
            } catch (final Exception e) {
                int i = 2;
            }
        } else {
            try {
                this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
                this.spettrosonoro.setVentilatoreCurrent();
                this.gridLayout.removeComponent(0, 0);
                this.spettrosonoro.setSizeFull();
                this.gridLayout.addComponent(this.spettrosonoro, 0, 0);
            } catch (final Exception e) {
            }
        }
    }

    private void commandItaliano() {
        this.l_VG.setItaliano();
        setBandiere();
    }

    private void commandInglese() {
        this.l_VG.setInglese();
        setBandiere();
    }

    private void setBandiere() {
        String lingua = this.l_VG.utilityCliente.utilityTraduzioni.getLingua();
        switch (lingua) {
            case "Lingua1":
                comboLingua.setValue("Italiano");
                break;
            case "Lingua2":
                comboLingua.setValue("English");
                break;
            case "Lingua3":
                comboLingua.setValue("Français");
                break;
            case "Lingua4":
                comboLingua.setValue("Deutsche");
                break;
            case "Lingua5":
                comboLingua.setValue("Español");
                break;
        }
//            this.pannelloLingue.setBandiere();
//            this.pannelloLingueMobile.setBandiere();
    }

    /*
	public void resizeView() {
		//l_VG.MainView.addToMemo("res ");
		if (l_VG.CurrentViewIndex == l_VG.CatalogoViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonCatalogo_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.SearchViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonRicerca_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.PreventiviViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonPreventivi_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.CompareViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonConfronto_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.SetupViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonSetup_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.RiepilogoViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonRiepilogo_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.PrestazioniViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonPrestazioni_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.Ingombro2ImgViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonIngombro2Img_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.DimensioniViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonDimensioni_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.AccessoriViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonAccessori_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.MultigiriViewIndex) {
			l_VG.CurrentViewIndex = l_VG.MainViewIndex;
			buttonMultigiri_buttonClick(null);
		} else if (l_VG.CurrentViewIndex == l_VG.SpettroSonoroViewIndex) {
			spettrosonoro.resizeView();
		}
	}
     */
    public void setMemo(final String value) {
        this.labelMemo.setValue(value);
    }

    public void addToMemo(final String value) {
        this.labelMemo.setValue(this.labelMemo.getValue() + value);
    }

    public void setMemoLogout(final String value) {
        this.buttonLogout.setCaption(value);
    }

    public void aggiornaGrafico() {
        try {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            if (this.l_VG.MobileMode) {
                this.prestazioniMobile.setVentilatoreCurrent();
            } else {
                this.prestazioni.setVentilatoreCurrent();
            }
        } catch (final Exception e) {

        }
    }

    public void aggiornaGraficoMistral() {
        try {
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            this.prestazioni.setVentilatoreCurrent();
        } catch (final Exception e) {

        }
    }

    public void setVentilatoreCorrenteFromCatalogo(final String id) {
        this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
        if (id == null) {
            this.l_VG.ElencoVentilatoriVisualizzati = null;
            this.l_VG.VentilatoreCurrentIndex = -1;
            this.l_VG.VentilatoreCurrent = null;
            this.l_VG.VentilatoreCurrentFromVieneDa = Costanti.vieneDaIndefinito;
        } else {
            this.l_VG.ElencoVentilatoriVisualizzati = this.l_VG.ElencoVentilatoriFromCatalogo;
            this.l_VG.VentilatoreCurrentIndex = Integer.parseInt(id);
            this.l_VG.VentilatoreCurrent = this.l_VG.ElencoVentilatoriVisualizzati.get(this.l_VG.VentilatoreCurrentIndex);
            this.l_VG.VentilatoreCurrentFromVieneDa = Costanti.vieneDaCatalogo;
            this.l_VG.ventilatoreFisicaCurrent = new VentilatoreFisica();
            if (l_VG.currentUserSaaS.idSottoClienteIndex == 6) {
                this.l_VG.ventilatoreFisicaCurrent.setVentilatoreFisica(this.l_VG.conTecnica, "", "", this.l_VG.VentilatoreCurrent, this.l_VG.ventilatoreFisicaParameter, "Moro");
            } else {
                this.l_VG.ventilatoreFisicaCurrent.setVentilatoreFisica(this.l_VG.conTecnica, "", "", this.l_VG.VentilatoreCurrent, this.l_VG.ventilatoreFisicaParameter);
            }
        }
        this.buttonAddPreventivo.setEnabled(true);
        abilitaTab();
    }

    public void setVentilatoreCorrenteFromRicerca(final String id) {
        this.l_VG.dbCRMv2.storePreventivoFan(this.l_VG.preventivoCurrent, this.l_VG.ElencoVentilatoriFromPreventivo);
        if (id == null) {
            this.l_VG.ElencoVentilatoriVisualizzati = null;
            this.l_VG.VentilatoreCurrentIndex = -1;
            this.l_VG.VentilatoreCurrent = null;
            this.l_VG.VentilatoreCurrentFromVieneDa = Costanti.vieneDaIndefinito;
        } else {
            this.l_VG.ElencoVentilatoriVisualizzati = this.l_VG.ElencoVentilatoriFromRicerca;
            this.l_VG.VentilatoreCurrentIndex = Integer.parseInt(id);
            this.l_VG.VentilatoreCurrent = this.l_VG.ElencoVentilatoriVisualizzati.get(this.l_VG.VentilatoreCurrentIndex);
            this.l_VG.VentilatoreCurrent.AccessoriCaricati = false;
            this.l_VG.VentilatoreCurrent.CAProgettazione = this.l_VG.CARicerca;
            this.l_VG.VentilatoreCurrentFromVieneDa = Costanti.vieneDaRicerca;
            this.l_VG.ventilatoreFisicaCurrent = new VentilatoreFisica();
            if (l_VG.utilityCliente.idSottoClienteIndex == 6)
                this.l_VG.ventilatoreFisicaCurrent.setVentilatoreFisica(this.l_VG.conTecnica, "", "", this.l_VG.VentilatoreCurrent, this.l_VG.ventilatoreFisicaParameter, "Moro");
            else
                this.l_VG.ventilatoreFisicaCurrent.setVentilatoreFisica(this.l_VG.conTecnica, "", "", this.l_VG.VentilatoreCurrent, this.l_VG.ventilatoreFisicaParameter);
        }
        this.buttonAddPreventivo.setEnabled(true);
        abilitaTab();
        //Notification.show("setVentilatoreCorrenteFromRicerca "+Integer.toString(l_VG.VentilatoreCurrentFromVieneDa));
    }

    public void setVentilatoreCorrenteFromPreventivo(final String id) {
        if (id == null) {
            this.l_VG.ElencoVentilatoriVisualizzati = null;
            this.l_VG.VentilatoreCurrentIndex = -1;
            this.l_VG.VentilatoreCurrent = null;
            this.l_VG.VentilatoreCurrentFromVieneDa = Costanti.vieneDaIndefinito;
        } else {
            this.l_VG.ElencoVentilatoriVisualizzati = this.l_VG.ElencoVentilatoriFromPreventivo;
            this.l_VG.VentilatoreCurrentIndex = Integer.parseInt(id);
            this.l_VG.VentilatoreCurrent = this.l_VG.ElencoVentilatoriVisualizzati.get(this.l_VG.VentilatoreCurrentIndex);
            this.l_VG.VentilatoreCurrentFromVieneDa = Costanti.vieneDaPreventivo;
            this.l_VG.ventilatoreFisicaCurrent = new VentilatoreFisica();
            this.l_VG.ventilatoreFisicaParameter.PressioneAtmosfericaCorrentePa = this.l_VG.utilityFisica.getpressioneAtmosfericaPa(this.l_VG.CACatalogo.temperatura, this.l_VG.CACatalogo.altezza);
            this.l_VG.ventilatoreFisicaCurrent.setVentilatoreFisica(this.l_VG.conTecnica, "", "", this.l_VG.VentilatoreCurrent, this.l_VG.ventilatoreFisicaParameter);
            //l_VG.MainView.setMemo("Preventivo "+l_VG.VentilatoreCurrent.Modello+"    "+l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente);
            //Notification.show(l_VG.VentilatoreCurrent.toString());
        }
        this.buttonAddPreventivo.setEnabled(false);
        abilitaTab();
        //Notification.show("setVentilatoreCorrenteFromPreventivo "+Integer.toString(l_VG.VentilatoreCurrentFromVieneDa));
    }

    public void abilitaTab() {
        this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
        this.buttonCatalogo.setVisible(this.l_VG.utilityCliente.isTabVisible("Catalogo") && this.catalogo.isPreSetVentilatoreCurrentOK() && !l_VG.isDonaldson());
        this.buttonRicerca.setVisible(this.l_VG.utilityCliente.isTabVisible("Ricerca") && this.ricerca.isPreSetVentilatoreCurrentOK());
        this.buttonPreventivi.setVisible(this.l_VG.utilityCliente.isTabVisible("Offerte") && this.preventivi.isPreSetVentilatoreCurrentOK());
        this.buttonConfronto.setVisible(this.l_VG.utilityCliente.isTabVisible("Confronto") && !this.l_VG.MobileMode && this.confronto.isPreSetVentilatoreCurrentOK());
        boolean ok = false;
        if (this.l_VG.MobileMode) {
            if (this.prestazioniMobile != null) {
                ok = this.prestazioniMobile.isPreSetVentilatoreCurrentOK();
            }
        } else {
            if (this.prestazioni != null) {
                ok = this.prestazioni.isPreSetVentilatoreCurrentOK();
            }
        }
        //String str = "prestazioni:" + Boolean.toString(ok);
        this.buttonPrestazioni.setVisible(this.l_VG.utilityCliente.isTabVisible("Prestazioni") && ok);
        this.buttonAddPreventivo.setVisible(this.l_VG.utilityCliente.isTabVisible("Offerte") && ok && !l_VG.isDonaldson());
        if (l_VG.utilityCliente.idSottoClienteIndex == 2 || l_VG.utilityCliente.idSottoClienteIndex == 6 || l_VG.utilityCliente.idSottoClienteIndex == 9
                || l_VG.utilityCliente.idSottoClienteIndex == 4) {
            this.buttonAddPreventivo.setVisible(false);
        }
        this.buttonAddPreventivo.setIcon(null);

        ok = this.multigiri.isPreSetVentilatoreCurrentOK();
        this.buttonMultigiri.setVisible(this.l_VG.utilityCliente.isTabVisible("MultiGiri") && !l_VG.isDonaldson() && ok);
        this.buttonDimensioniAngolo.setVisible(this.l_VG.utilityCliente.isTabVisible("DimensioniAngolo") && this.dimensioniAngolo.isPreSetVentilatoreCurrentOK());
        this.buttonDimensioniNoAngolo.setVisible(this.l_VG.utilityCliente.isTabVisible("Dimensioni") && this.dimensioni.isPreSetVentilatoreCurrentOK());
        if (this.l_VG.VentilatoreCurrent != null) {
            if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
                if (l_VG.isDonaldson()) {
                    this.buttonAccessori.setVisible(this.l_VG.utilityCliente.isTabVisible("Accessori"));
                } else {
                    if (l_vcc04.Tipo.equals("centrifugo")) {
                        this.buttonAccessori.setVisible(this.l_VG.utilityCliente.isTabVisible("Accessori") && this.accessori04Centrifughi.isPreSetVentilatoreCurrentOK());
                    } else if (l_vcc04.Tipo.equals("fusione")) {
                        this.buttonAccessori.setVisible(this.l_VG.utilityCliente.isTabVisible("Accessori") && this.accessori04Fusione.isPreSetVentilatoreCurrentOK());
                    } else if (l_vcc04.Tipo.equals("assiale")) {
                        this.buttonAccessori.setVisible(this.l_VG.utilityCliente.isTabVisible("Accessori") && this.accessori04Assiali.isPreSetVentilatoreCurrentOK());
                    }
                }
            } else {
                this.buttonAccessori.setVisible(this.l_VG.utilityCliente.isTabVisible("Accessori") && this.accessoriDefault.isPreSetVentilatoreCurrentOK());
            }
        } else {
            this.buttonAccessori.setVisible(false);
        }
        this.buttonSpettroSonoro.setVisible(this.l_VG.utilityCliente.isTabVisible("SpettroSonoro") && !l_VG.isDonaldson() && this.spettrosonoro.isPreSetVentilatoreCurrentOK());
        this.buttonRiepilogo.setVisible(this.l_VG.utilityCliente.isTabVisible("Riepilogo") && this.riepilogo.isPreSetVentilatoreCurrentOK());
        this.buttonIngombro2Img.setVisible(!this.l_VG.MobileMode && this.l_VG.utilityCliente.isTabVisible("Ingombro2Img") && this.ingombro2img.isPreSetVentilatoreCurrentOK());
        //buttonIngombro.setVisible(l_VG.utilityCliente.isTabVisible("Ingombro2Img"));

        this.buttonSetup.setVisible(!this.l_VG.MobileMode);
        this.labelMemo.setVisible(!this.l_VG.MobileMode);
        if (this.l_VG.VentilatoreCurrent == null) {
            this.labelRiempitivo.setVisible(false);
            this.buttonDataSheet.setVisible(false);
            this.buttonDownloadDataSheet.setVisible(false);
            this.itemDatiTecniciGroup.setVisible(false);
        } else {
            this.labelRiempitivo.setVisible(!this.l_VG.MobileMode);
            this.buttonDataSheet.setVisible(!this.l_VG.MobileMode);
            this.buttonDownloadDataSheet.setVisible(!this.l_VG.MobileMode);
            this.buttonDownloadDataSheet.setEnabled(false);
            this.itemDatiTecniciGroup.setVisible(true);
        }
            this.buttonCoral.setVisible(l_VG.currentUserSaaS.idSottoClienteIndex == 3 && l_VG.presetRicerca != null && l_VG.VentilatoreCurrent!=null);
    }

    public void refreshAccessori() {
        if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
            if (l_VG.isDonaldson()) {
                this.accessori04Donaldson.refreshAccessori();
            } else {
                final Cliente04VentilatoreCampiCliente l_vcc04 = (Cliente04VentilatoreCampiCliente) this.l_VG.VentilatoreCurrent.campiCliente;
                if (l_vcc04.Tipo.equals("centrifugo")) {
                    this.accessori04Centrifughi.refreshAccessori();
                } else if (l_vcc04.Tipo.equals("fusione")) {
                    this.accessori04Fusione.refreshAccessori();
                } else if (l_vcc04.Tipo.equals("assiale")) {
                    this.accessori04Assiali.refreshAccessori();
                }
            }
        } else {
            this.accessoriDefault.refreshAccessori();
        }
        refreshAccessoriPreventivi();
    }

    public void refreshAccessoriPreventivi() {
        if (this.l_VG.VentilatoreCurrentFromVieneDa == Costanti.vieneDaPreventivo) {
            this.preventivi.refreshAccessori();
        }
    }

    private void resetColoreSelezionato() {
        resetColoreSelezionato(this.buttonLogout);
        resetColoreSelezionato(this.buttonSetup);
        resetColoreSelezionato(this.buttonCatalogo);
        resetColoreSelezionato(this.buttonRicerca);
        resetColoreSelezionato(this.buttonPreventivi);
        resetColoreSelezionato(this.buttonConfronto);
        resetColoreSelezionato(this.buttonPrestazioni);
        resetColoreSelezionato(this.buttonDimensioniAngolo);
        resetColoreSelezionato(this.buttonDimensioniNoAngolo);
        resetColoreSelezionato(this.buttonAccessori);
        resetColoreSelezionato(this.buttonMultigiri);
        resetColoreSelezionato(this.buttonSpettroSonoro);
        resetColoreSelezionato(this.buttonRiepilogo);
        resetColoreSelezionato(this.buttonIngombro2Img);
    }

    private void resetColoreSelezionato(final XdevButton bottone) {
        if (bottone.getStyleName().contains(this.coloreBottoneSelezionato)) {
            bottone.removeStyleName(this.coloreBottoneSelezionato);
            bottone.addStyleName(this.coloreBottone);
        }
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonPrestazioni}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonPrestazioni_buttonClick(final Button.ClickEvent event) {
        commandPrestazioni();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonAccessori}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonAccessori_buttonClick(final Button.ClickEvent event) {
        commandAccessori();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonSpettroSonoro}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonSpettroSonoro_buttonClick(final Button.ClickEvent event) {
        commandSpettroSonoro();
    }

    private void buttonAddPreventivo_buttonClick(final Button.ClickEvent event) {
        this.l_VG.addToPreventivo();
        this.catalogo.setCheckTableVentilatori();
        this.buttonAddPreventivo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));

    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonMultigiri}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonMultigiri_buttonClick(final Button.ClickEvent event) {
        commandMultigiri();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonDimensioniAngolo}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonDimensioniAngolo_buttonClick(final Button.ClickEvent event) {
        commandDimensioniAngolo();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonLogout}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonLogout_buttonClick(final Button.ClickEvent event) {
        if (this.l_VG.presetRicerca != null) {
            this.l_VG.dbConfig.clearPresetRicerca();
        }
        this.msgBox = MessageBox.createWarning();
        final String msg = this.l_VG.utilityTraduzioni.TraduciStringa("Uscita dal programma.") + "\n\n" + this.l_VG.utilityTraduzioni.TraduciStringa("Confermi?");
        this.msgBox.withMessage(msg);
        this.msgBox.withYesButton(() -> {
            commandLogout();
        });
        this.msgBox.withNoButton();
        this.msgBox.open();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonSetup}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonSetup_buttonClick(final Button.ClickEvent event) {
        commandSetup();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonCatalogo}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonCatalogo_buttonClick(final Button.ClickEvent event) {
        commandCatalogo();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonRicerca}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonRicerca_buttonClick(final Button.ClickEvent event) {
        commandRicerca();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonPreventivi}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonPreventivi_buttonClick(final Button.ClickEvent event) {
        commandPreventivi();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonConfronto}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonConfronto_buttonClick(final Button.ClickEvent event) {
        commandConfronto();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonDebug}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonDebug_buttonClick(final Button.ClickEvent event) {
        commandDebug();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonLauout}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonLauout_buttonClick(final Button.ClickEvent event) {
        commandLayout();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonDataSheet}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonDataSheet_buttonClick(final Button.ClickEvent event) {
        final pannelloPagineDataSheet pannello = new pannelloPagineDataSheet();
        pannello.setVariabiliGlobali(this.l_VG);
        this.msgBox = MessageBox.create();
        this.msgBox.withCaption(this.l_VG.utilityTraduzioni.TraduciStringa("Stampe da Includere"));
        this.msgBox.withMessage(pannello);
        this.msgBox.withAbortButton();
        this.msgBox.withOkButton(() -> {
            final Window popup = PopupWindow.For(new ProgressView(this.l_VG.utilityTraduzioni)).closable(false).draggable(false).resizable(false).modal(true).show();
            UI.getCurrent().push();
            this.l_VG.completaCaricamentoVentilatore(this.l_VG.VentilatoreCurrent);
            if (this.l_VG.MobileMode) {
                this.prestazioniMobile.setVentilatoreCurrent();
            } else {
                this.prestazioni.setVentilatoreCurrent();
            }
            //l_VG.MainView.setMemo(l_VG.multigiri.puntiLavoro.titoliPL[0]);
            //this.multigiri.setVentilatoreCurrent();
            this.multigiri.setMemo();
            generaDataSheet(pannello.getFrontespizio(), pannello.getPrestazioni(), pannello.getSpettro(), pannello.getMultigiri(), pannello.getDimensioni(), pannello.getConfronto());
            this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneStampaDataSheet, "Creazione DataSheet " + this.l_VG.VentilatoreCurrent.ModelloTrans);
            popup.close();
        });
        this.msgBox.open();
    }

    private void generaDataSheet(final boolean frontespizioFlag, final boolean prestazioniFlag, final boolean rumoreFlag, final boolean multigiriFlag, final boolean dimensioniFlag, final boolean confrontoFlag) {
        String trace = "";
        try {
            trace = "start";
            DataSheet dataSheet;
            if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                dataSheet = new Cliente04DataSheet(this.l_VG.mzNascosta);
            } else {
                dataSheet = new MistralDataSheet(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta);
                if (l_VG.idClienteIndexVG == 1 && l_VG.CACatalogo.frequenza == 60) {
                    l_VG.VentilatoreCurrent.FrequenzaMistral = "60 Hz";
                } else 
                    l_VG.VentilatoreCurrent.FrequenzaMistral = "";
            }
            trace = "dataSheet";
            dataSheet.init(this.l_VG.Paths, this.l_VG.conTecnica, this.l_VG.dbTableQualifier, this.l_VG.assiGraficiDefault, this.l_VG.ventilatoreFisicaParameter);
            dataSheet.initUtility(this.l_VG.currentCitFont, this.l_VG.utilityTraduzioni, this.l_VG.utilityCliente);
            //dataSheet.forzaInglese();
            int nTotPage = 0;
            if (frontespizioFlag) {
                nTotPage++;
            }
            if (prestazioniFlag) {
                nTotPage++;
            }
            if (rumoreFlag) {
                nTotPage++;
            }
            if (multigiriFlag) {
                nTotPage++;
            }
            if (dimensioniFlag) {
                if (this.l_VG.utilityCliente.isDatasheetStampabile("DimensioniAngolo") && this.l_VG.MainView.dimensioniAngolo.isPreSetVentilatoreCurrentOK()) {
                    nTotPage++;
                }
                if (this.l_VG.utilityCliente.isDatasheetStampabile("Dimensioni") && this.l_VG.MainView.dimensioni.isPreSetVentilatoreCurrentOK()) {
                    nTotPage++;
                }
            }
            if (confrontoFlag) {
                nTotPage++;
            }
            if (nTotPage <= 0) {
                this.buttonDownloadDataSheet.setEnabled(false);
                return;
            }

            String descSerie = " ";
            final ArrayList<Serie> series = this.l_VG.ElencoSerie;
            for (final Serie s : this.l_VG.ElencoSerie) {
                if (s.Serie.equals(this.l_VG.VentilatoreCurrent.Serie)) {
                    descSerie = s.Descrizione;
                }
            }
            dataSheet.initDataSheet(nTotPage, this.l_VG.utilityCliente.taroccoHz, descSerie);

            trace = "dataSheet init";
            InputStream isTemplate = null;
            InputStream isLogo = null;
            InputStream isLogo2 = null;
            InputStream isSfondo = null;
            String nomeXML;
            int nCurrPage = 1;
            String addToTempleteName = "";
            if (this.l_VG.currentUserSaaS.idClienteIndex == 4) {
                addToTempleteName = this.l_VG.currentUserSaaS.Ambiente.toLowerCase() + "_";
            }
            //frontespizio
            if (frontespizioFlag) {
                nomeXML = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + "data_sheet_frontespizio.xml";
                try {
                    isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineLogoDataSheet);
                    isTemplate = new FileInputStream(nomeXML);
                    if (this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
                        isSfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                    }
                } catch (final Exception e) {

                }
                dataSheet.buildDataSheetFrontespizio(this.l_VG.VentilatoreCurrent, nCurrPage++, isTemplate, isLogo, isSfondo);
                trace = "buildDataSheetFrontespizio";
            }
            //frontespizio fine
            //prestazioni
            if (prestazioniFlag) {
                nomeXML = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + addToTempleteName + "data_sheet_prestazioni.xml";
                try {
                    isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineLogoDataSheet);
                    isTemplate = new FileInputStream(nomeXML);
                    if (this.l_VG.ParametriVari.sfondoGraficiPrestazioniEnabledFlag) {
                        isSfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                    }
                } catch (final Exception e) {

                }
                if (this.l_VG.currentUserSaaS.idClienteIndex != 4) {
                    dataSheet.setFluid(l_VG.SelezioneDati.tipoFluidoRicerca);
                }
                try {
                    if (this.l_VG.currentUserSaaS.idSottoClienteIndex == 7) {
                        isLogo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoFA.png");
                    }
                } catch (final Exception e) {
                }
                dataSheet.buildDataSheetPrestazioni(this.l_VG.VentilatoreCurrent, nCurrPage++, isTemplate, isLogo, isLogo2, isSfondo, this.l_VG.ParametriVari);
                trace = "buildDataSheetPrestazioni";
            }
            //prestazioni fine
            //rumore
            if (rumoreFlag) {
                InputStream isSfondo1 = null;
                InputStream isMemo = null;
                nomeXML = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + addToTempleteName + "data_sheet_rumore.xml";
                try {
                    isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineLogoDataSheet);
                    isTemplate = new FileInputStream(nomeXML);
                    if (this.l_VG.ParametriVari.sfondoIstogrammaRumoreEnabledFlag) {
                        isSfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                        isSfondo1 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                    }
                } catch (final Exception e) {

                }
                try {
                    if (this.l_VG.currentUserSaaS.idSottoClienteIndex == 7) {
                        isLogo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoFA.png");
                    }
                } catch (final Exception e) {
                }
                //commandSpettroSonoro();
                this.spettrosonoro.setVentilatoreCurrent();
                this.spettrosonoro.drawIstogrammi();
                dataSheet.buildDataSheetRumore(this.l_VG.VentilatoreCurrent, nCurrPage++, isTemplate, isLogo, isLogo2, isSfondo, isSfondo1);
                trace = "buildDataSheetRumore";
                try {
                    isMemo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + "memo3db.png");
                } catch (final Exception e) {

                }
                if (isMemo != null) {
                    dataSheet.addImage("rumore-tolleranza", isMemo);
                }
            }
            //rumore fine
            //multigiri
            if (multigiriFlag) {
                nomeXML = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + "data_sheet_multigiri.xml";
                try {
                    isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineLogoDataSheet);
                    isTemplate = new FileInputStream(nomeXML);
                    isSfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiPrestazioni);
                } catch (final Exception e) {

                }
                try {
                    if (this.l_VG.currentUserSaaS.idSottoClienteIndex == 7) {
                        isLogo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoFA.png");
                    }
                } catch (final Exception e) {
                }
                final ArrayList<Ventilatore> elencoVentilatoriSimili = this.l_VG.loadVentilatoriSimiliForMultigiri(this.l_VG.VentilatoreCurrent);
                dataSheet.buildDataSheetMultigiri(this.l_VG.VentilatoreCurrent, nCurrPage++, isTemplate, isLogo, isLogo2, isSfondo, elencoVentilatoriSimili, this.l_VG.multigiri);
                trace = "buildDataSheetMultigiri";
            }
            //multigiri fine
            if (dimensioniFlag) {
                this.l_VG.dbTecnico.caricaDimensioni(this.l_VG.VentilatoreCurrent);
                this.l_VG.dbTecnico.caricaDimensioniEsecuzione(this.l_VG.VentilatoreCurrent);
                //dimensioniangolo
                if (this.l_VG.utilityCliente.isDatasheetStampabile("DimensioniAngolo") && this.l_VG.MainView.dimensioniAngolo.isPreSetVentilatoreCurrentOK()) {
                    nomeXML = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + "data_sheet_dimensioni.xml";
                    try {
                        isTemplate = new FileInputStream(nomeXML);
                        isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineLogoDataSheet);
                    } catch (final Exception e) {
                        return;
                    }
                    try {
                        if (this.l_VG.currentUserSaaS.idSottoClienteIndex == 7) {
                            isLogo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoFA.png");
                        }
                    } catch (final Exception e) {
                    }

                    InputStream isImgSfondo = null;
                    try {
                        if (this.l_VG.utilityCliente.isTabVisible("DimensioniAngolo")) {
                            isImgSfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.VentilatoreCurrent.DIMOrientamento_Img);
                        }

                    } catch (final Exception e1) {
                        //Notification.show(e1.toString());
                    }
                    InputStream isImgQ1 = null;
                    try {
                        isImgQ1 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ1);
                    } catch (final Exception e1) {

                    }
                    InputStream isImgQ2 = null;
                    try {
                        isImgQ2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ2);
                    } catch (final Exception e1) {

                    }
                    InputStream isImgQ3 = null;
                    try {
                        isImgQ3 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ3);
                    } catch (final Exception e1) {

                    }
                    InputStream isImgQ4 = null;
                    try {
                        isImgQ4 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.VentilatoreCurrent.DIMOrientamento_ImgQ4);
                    } catch (final Exception e1) {

                    }
                    InputStream isImg = null;
                    if (isImgSfondo != null) {
                        final jDisplayImage immagineDimensioni = new jDisplayImage();
                        immagineDimensioni.DisplayfromInputStream(isImgSfondo, isImgQ1, isImgQ2, isImgQ3, isImgQ4);
                        immagineDimensioni.buildImage();
                        if (l_VG.utilityCliente.idClienteIndex == 4) {
                            try {
                                FileInputStream isImgDisclaimer = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/disclaimer.png");
                                BufferedImage disc = ImageIO.read(isImgDisclaimer);
                                Graphics g = immagineDimensioni.getImage().getGraphics();
                                if (disc != null) {
                                    g.drawImage(disc, immagineDimensioni.getImageWith()-disc.getWidth(null), immagineDimensioni.getImageHeight()/2-disc.getHeight(null)/2, disc.getWidth(null), disc.getHeight(null), null);
                                }
                            } catch (Exception e) {
                                
                            }
                        }
                        immagineDimensioni.centraImage(1086, 694);
                        isImg = immagineDimensioni.getInputStream();
                    }
                    dataSheet.buildDataSheetDimensioni(this.l_VG.VentilatoreCurrent, nCurrPage++, isTemplate, isLogo, isLogo2, true, isImg, this.l_VG.mapEsecuzioniDesc);
                }
                //dimensioniangolo fine
                if (this.l_VG.utilityCliente.isDatasheetStampabile("Dimensioni") && this.l_VG.MainView.dimensioni.isPreSetVentilatoreCurrentOK()) {
                    //dimensioniangolo
                    nomeXML = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + "data_sheet_dimensioninoangolo.xml";
                    try {
                        isTemplate = new FileInputStream(nomeXML);
                        isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineLogoDataSheet);
                    } catch (final Exception e) {
                        return;
                    }
                    try {
                        if (this.l_VG.currentUserSaaS.idSottoClienteIndex == 7) {
                            isLogo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoFA.png");
                        }
                    } catch (final Exception e) {
                    }
                    InputStream isImgSfondo = null;
                    try {
                        if (this.l_VG.utilityCliente.isTabVisible("Dimensioni")) {
                            if (this.l_VG.VentilatoreCurrent.DIM_Img != null && !this.l_VG.VentilatoreCurrent.DIM_Img.equals("")) {
                                isImgSfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootIMG + "dimensioni/" + this.l_VG.VentilatoreCurrent.DIM_Img);
                            }
                        }

                    } catch (final Exception e1) {
                        //Notification.show(e1.toString());
                    }
                    InputStream isImg = null;
                    if (l_VG.currentUserSaaS.idClienteIndex == 4) {
                        if (isImgSfondo != null) {
                            final jDisplayImage immagineDimensioni = new jDisplayImage();
                            immagineDimensioni.DisplayfromInputStream(isImgSfondo, null, null, null, null);
                            immagineDimensioni.buildImage();
                            immagineDimensioni.centraImage(1086, 694);
                            isImg = immagineDimensioni.getInputStream();
                        }
                    } else {
                        if (isImgSfondo != null) {
                            final jDisplayImage immagineDimensioni = new jDisplayImage();
                            immagineDimensioni.DisplayfromInputStream(isImgSfondo, null, null, null, null);
                            immagineDimensioni.buildImage();
                            isImg = immagineDimensioni.getInputStream();
                        }
                    }
                    dataSheet.buildDataSheetDimensioni(this.l_VG.VentilatoreCurrent, nCurrPage++, isTemplate, isLogo, isLogo2, false, isImg, this.l_VG.mapEsecuzioniDesc);
                    //dimensioniangolo fine

                }
            }
            //confronto
            if (confrontoFlag) {
                nomeXML = this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplateOfferta + "data_sheet_confronto.xml";
                try {
                    isLogo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + this.l_VG.nomiImmagini.immagineLogoDataSheet);
                    isTemplate = new FileInputStream(nomeXML);
                    isSfondo = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootTemplate + this.l_VG.sfondoGraficiSpettro);
                } catch (final Exception e) {

                }
                try {
                    if (this.l_VG.currentUserSaaS.idSottoClienteIndex == 7) {
                        isLogo2 = new FileInputStream(this.l_VG.Paths.rootResources + this.l_VG.Paths.rootLOGO + "logoFA.png");
                    }
                } catch (final Exception e) {
                }
                final CompareView pw = (CompareView) this.l_VG.ElencoView[this.l_VG.CompareViewIndex];
                boolean curve[] = null;
                Color colori[] = null;
                if (pw != null) {
                    curve = pw.getCirve();
                    colori = pw.getColori();
                }
                dataSheet.buildDataSheetConfronto(this.l_VG.ElencoVentilatoriFromCompare, colori, curve, nCurrPage++, isTemplate, isLogo, isLogo2, isSfondo);
            }
            //confronto fine
            final String nomeFileOut = this.l_VG.Paths.rootResources + "DataSheet" + this.l_VG.VentilatoreCurrent.Modello + "-" + this.l_VG.utility.getUniqueId() + ".pdf";
            dataSheet.saveDataSheet(nomeFileOut);
            trace = "saveDataSheet";
            final File fileOutput = new File(nomeFileOut);
            final Resource res = new FileResource(fileOutput);
            if (this.fileDownloader == null) {
                this.fileDownloader = new FileDownloader(res);
                this.fileDownloader.extend(this.buttonDownloadDataSheet);
            } else {
                this.fileDownloader.setFileDownloadResource(res);
            }
            this.buttonDownloadDataSheet.setEnabled(true);
            this.msgBox = MessageBox.createInfo();
            this.msgBox.withMessage(this.l_VG.utilityTraduzioni.TraduciStringa("Data Sheet Correttamente Generato"));
            this.msgBox.withOkButton();
            this.msgBox.open();
            trace = "done";
        } catch (final Exception e) {
            Notification.show(trace + ": " + e.toString());
            //l_VG.MainView.setMemo(trace + ": " + e.toString());
        }
        //l_VG.MainView.setMemo(trace);
    }

    public void togglePreventivoButton(boolean value) {
        buttonAddPreventivo.setEnabled(value);
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonIngombro2Img}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonIngombro2Img_buttonClick(final Button.ClickEvent event) {
        commandIngombro2Img();
    }
    
    private void buttonCoral_buttonClick(final Button.ClickEvent event) {
        //TODO
        l_VG.completaCaricamentoVentilatore(l_VG.VentilatoreCurrent);
        try {
            int rows = -1;
            String prezzo = ""+l_VG.calcolaCostoVentilatore(l_VG.VentilatoreCurrent);
            String motore = l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato != null? l_VG.VentilatoreCurrent.selezioneCorrente.MotoreInstallato.CodiceCliente : "";
            PreparedStatement st = l_VG.conPrincipale.prepareStatement("UPDATE Bridge SET Modello = '"+ l_VG.VentilatoreCurrent.ModelloTrans +"', Motore = '"+ motore +"', Prezzo = '"+ prezzo +"' WHERE UserID = "+ l_VG.currentUserSaaS._ID);
            rows = st.executeUpdate();
            buttonCoral.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/check16.png"));
        } catch (Exception e ) { }
    }
    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonRiepilogo}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonRiepilogo_buttonClick(final Button.ClickEvent event) {
        commandRiepilogo();
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonDownloadDataSheet}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonDownloadDataSheet_buttonClick(final Button.ClickEvent event) {
        this.l_VG.traceUser.traceAzioneUser(this.l_VG.traceUser.AzioneDownloadDataSheet, "Download DataSheet " + this.l_VG.VentilatoreCurrent.ModelloTrans);
    }

    /**
     * Event handler delegate method for the {@link XdevButton}
     * {@link #buttonDimensioniNoAngolo}.
     *
     * @see Button.ClickListener#buttonClick(Button.ClickEvent)
     * @eventHandlerDelegate Do NOT delete, used by UI designer!
     */
    private void buttonDimensioniNoAngolo_buttonClick(final Button.ClickEvent event) {
        commandDimensioniNoAngolo();
    }

    private void comboLingua_valueChange(final Property.ValueChangeEvent event) {
        changingLanguage = true;

        String Lingua = comboLingua.getValue().toString();
        switch (Lingua) {
            case "Italiano":
                this.l_VG.setLinguaFromId("IT");
                break;
            case "English":
                this.l_VG.setLinguaFromId("UK");
                break;
            case "Français":
                this.l_VG.setLinguaFromId("FR");
                break;
            case "Deutsche":
                this.l_VG.setLinguaFromId("DE");
                break;
            case "Español":
                this.l_VG.setLinguaFromId("ES");
                break;
        }
        setBandiere();
        changingLanguage = false;
    }

    /*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated by
	 * the UI designer.
     */
    // <generated-code name="initUI">

    public void activateDatasheet(boolean activate) {
        this.buttonDataSheet.setEnabled(activate);
    }

    private void initUI() {
        comboLingua = new XdevComboBox<>();
        this.verticalLayout = new XdevVerticalLayout();
        this.menuBar = new XdevMenuBar();
        this.itemFileGroup = this.menuBar.addItem("File", null);
        this.itemLogout = this.itemFileGroup.addItem("Logout", null);
        this.itemFileGroup.addSeparator();
        this.itemSetup = this.itemFileGroup.addItem("Setup", null);
        this.itemCatalogo = this.menuBar.addItem("Catalogo", null);
        this.itemRicerca = this.menuBar.addItem("Ricerca", null);
        this.itemPreventivi = this.menuBar.addItem("Preventivi", null);
        this.itemConfronto = this.menuBar.addItem("Confronto", null);
        this.itemDatiTecniciGroup = this.menuBar.addItem("Dati Tecnici", null);
        this.itemPrestazioni = this.itemDatiTecniciGroup.addItem("Prestazioni", null);
        this.itemDimensioniAngolo = this.itemDatiTecniciGroup.addItem("Dimensioni", null);
        this.itemAccessori = this.itemDatiTecniciGroup.addItem("Accessori", null);
        this.itemMultigiri = this.itemDatiTecniciGroup.addItem("Multigiri", null);
        this.itemRumore = this.itemDatiTecniciGroup.addItem("Spettro Sonoro", null);
        this.itemDimensioni = this.itemDatiTecniciGroup.addItem("Dimensioni", null);
        this.itemLinguaGroup = this.menuBar.addItem("Lingua", null);
        this.itemItaliano = this.itemLinguaGroup.addItem("", null);
        this.itemInglese = this.itemLinguaGroup.addItem("", null);
        this.horizontalLayoutComandi = new XdevHorizontalLayout();
        this.buttonCatalogo = new XdevButton();
        this.buttonRicerca = new XdevButton();
        this.buttonPreventivi = new XdevButton();
        this.buttonConfronto = new XdevButton();
        this.buttonLauout = new XdevButton();
        this.buttonSetup = new XdevButton();
        this.buttonLogout = new XdevButton();
        this.buttonDebug = new XdevButton();
//		this.pannelloLingueGosht = new XdevPanel();
//		this.buttonItalianoGosth1 = new XdevButton();
        this.labelMemo = new XdevLabel();
        this.labelVersion = new XdevLabel();
        this.horizontalLayoutDatiTecnici = new XdevHorizontalLayout();
        this.labelRiempitivo = new XdevLabel();
        this.buttonRiepilogo = new XdevButton();
        this.buttonPrestazioni = new XdevButton();
        this.buttonIngombro2Img = new XdevButton();
        this.buttonDimensioniAngolo = new XdevButton();
        this.buttonDimensioniNoAngolo = new XdevButton();
        this.buttonAccessori = new XdevButton();
        this.buttonCoral = new XdevButton();
        this.buttonMultigiri = new XdevButton();
        this.buttonSpettroSonoro = new XdevButton();
//		this.pannelloLingueMobileGosht = new XdevPanel();
//		this.buttonItalianoGosth2 = new XdevButton();
        this.gridLayout = new XdevGridLayout();
        this.imageSfondo = new XdevImage();
        this.horizontalLayoutFooter = new XdevHorizontalLayout();
        this.buttonDataSheet = new XdevButton();
        this.buttonDownloadDataSheet = new XdevButton();
        this.buttonAddPreventivo = new XdevButton();
        this.buttonAddPreventivo.setCaption("Aggiungi Preventivo");
        this.buttonAddPreventivo.setStyleName("big giallo");
        this.buttonAddPreventivo.setId("buttonAddOffer");
        this.buttonCoral.setCaption("Invia a configuratore");
        this.buttonCoral.setStyleName("big coral");
        this.setResponsive(true);
        this.verticalLayout.setMargin(new MarginInfo(false));
        this.menuBar.setIcon(null);
        this.menuBar.setStyleName("big verde");
        this.menuBar.setHtmlContentAllowed(true);
        this.menuBar.setResponsive(true);
        this.itemFileGroup.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/icona16.png"));
        this.itemLogout.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Exit.png"));
        this.itemSetup.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/Config.png"));
        this.itemCatalogo.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/book_bookmark16.png"));
        this.itemRicerca.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/find.png"));
        this.itemPreventivi.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/hand_paper16.png"));
        this.itemConfronto.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/compare16.png"));
        this.itemDatiTecniciGroup
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/document_gear16.png"));
        this.itemPrestazioni.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/stopwatch16.png"));
        this.itemDimensioniAngolo
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/layout_horizontal16.png"));
        this.itemAccessori.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/preferences16.png"));
        this.itemMultigiri
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/document_chart16.png"));
        this.itemRumore.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/loudspeaker16.png"));
        this.itemDimensioni
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/layout_horizontal16.png"));
        this.itemLinguaGroup
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/flag_generic16.png"));
        this.itemItaliano
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/bandiere/flag_italy16.png"));
        this.itemInglese.setIcon(
                new ApplicationResource(this.getClass(), "WebContent/resources/img/bandiere/flag_united_kingdom16.png"));
        this.horizontalLayoutComandi.setMargin(new MarginInfo(false, true, false, true));
        this.buttonCatalogo
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/book_bookmark16.png"));
        this.buttonCatalogo.setCaption("Catalogue");
        this.buttonCatalogo.setStyleName("big verde");
        this.buttonRicerca.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/find.png"));
        this.buttonRicerca.setCaption("Search");
        this.buttonRicerca.setStyleName("big verde");
        this.buttonPreventivi
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/hand_paper16.png"));
        this.buttonPreventivi.setCaption("Preventivi");
        this.buttonPreventivi.setStyleName("big  verde");
        this.buttonConfronto.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/compare16.png"));
        this.buttonConfronto.setCaption("Compare");
        this.buttonConfronto.setStyleName("big  verde");
        this.buttonLauout.setCaption("Layout");
        this.buttonLauout.setStyleName("big verde");
        this.buttonSetup.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/preferences.png"));
        this.buttonSetup.setCaption("Setup");
        this.buttonSetup.setStyleName("big verde");
        this.buttonLogout.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/logout.png"));
        this.buttonLogout.setCaption("Logout");
        this.buttonLogout.setStyleName("big verde");
        this.buttonDebug.setCaption("Debug");
        this.buttonDebug.setStyleName("big");
//		this.buttonItalianoGosth1
//				.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/bandiere/jp.png"));
//		this.buttonItalianoGosth1.setCaption("");
//		this.buttonItalianoGosth1.setStyleName("verde icon-only");
//		this.buttonItalianoGosth1.setImmediate(true);
        this.labelMemo.setValue("memo");
        this.labelVersion.setValue("Version");
        this.horizontalLayoutDatiTecnici.setMargin(new MarginInfo(false, true, false, true));
        this.buttonRiepilogo.setCaption("Riepilogo");
        this.buttonRiepilogo.setStyleName("big  verde");
        this.buttonPrestazioni
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/performance.png"));
        this.buttonPrestazioni.setCaption("Performance");
        this.buttonPrestazioni.setStyleName("big verde");
        this.buttonIngombro2Img.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/page_size.png"));
        this.buttonIngombro2Img.setCaption("Ingombro");
        this.buttonIngombro2Img.setStyleName("big  verde");
        this.buttonDimensioniAngolo
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/page_size.png"));
        this.buttonDimensioniAngolo.setCaption("Dimension");
        this.buttonDimensioniAngolo.setStyleName("big  verde");
        this.buttonDimensioniNoAngolo
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/page_size.png"));
        this.buttonDimensioniNoAngolo.setCaption("Dimension");
        this.buttonDimensioniNoAngolo.setStyleName("big  verde");
        this.buttonAccessori.setCaption("Accessories");
        this.buttonAccessori.setStyleName("big verde");
        this.buttonAccessori.setStyleName("big verde");
        
        this.buttonMultigiri.setCaption("Multigiri");
        this.buttonMultigiri.setStyleName("big verde");
        this.buttonSpettroSonoro
                .setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/megaphone.png"));
        this.buttonSpettroSonoro.setCaption("Rumore");
        this.buttonSpettroSonoro.setStyleName("big verde");
//		this.buttonItalianoGosth2
//				.setIcon(new ApplicationResource(this.getClass(), "WebContent/resources/img/bandiere/jp.png"));
//		this.buttonItalianoGosth2.setCaption("");
//		this.buttonItalianoGosth2.setStyleName("verde icon-only");
//		this.buttonItalianoGosth2.setImmediate(true);
        this.gridLayout.setMargin(new MarginInfo(false));
        this.horizontalLayoutFooter.setMargin(new MarginInfo(false, true, false, true));
        this.buttonDataSheet.setCaption("Data Sheet");
        this.buttonDataSheet.setStyleName("big azzurro");
        this.buttonDownloadDataSheet.setIcon(null);
        this.buttonDownloadDataSheet.setCaption("Download Data Sheet");
        this.buttonDownloadDataSheet.setStyleName("big");
//		this.buttonItalianoGosth1.setSizeUndefined();
//		this.pannelloLingueGosht.setContent(this.buttonItalianoGosth1);
        this.buttonCatalogo.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonCatalogo);
        this.buttonRicerca.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonRicerca);
        this.buttonPreventivi.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonPreventivi);
        this.buttonConfronto.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonConfronto);
        this.buttonLauout.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonLauout);
        this.buttonSetup.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonSetup);
        this.buttonLogout.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonLogout);
        this.buttonDebug.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.buttonDebug);
//		this.pannelloLingueGosht.setSizeUndefined();
//		this.horizontalLayoutComandi.addComponent(this.pannelloLingueGosht);
        comboLingua.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(comboLingua, Alignment.MIDDLE_LEFT);
        this.labelMemo.setSizeUndefined();
        this.horizontalLayoutComandi.addComponent(this.labelMemo);
        this.horizontalLayoutComandi.setComponentAlignment(this.labelMemo, Alignment.MIDDLE_CENTER);
        this.labelVersion.setWidth(100, Unit.PERCENTAGE);
        this.labelVersion.setHeight(-1, Unit.PIXELS);
        this.horizontalLayoutComandi.addComponent(this.labelVersion);
        this.horizontalLayoutComandi.setComponentAlignment(this.labelVersion, Alignment.MIDDLE_RIGHT);
        this.horizontalLayoutComandi.setExpandRatio(this.labelVersion, 10.0F);
//		this.buttonItalianoGosth2.setSizeUndefined();
//		this.pannelloLingueMobileGosht.setContent(this.buttonItalianoGosth2);
        this.labelRiempitivo.setWidth(75, Unit.PIXELS);
        this.labelRiempitivo.setHeight(-1, Unit.PIXELS);
        this.horizontalLayoutDatiTecnici.addComponent(this.labelRiempitivo);
        this.buttonRiepilogo.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonRiepilogo);
        this.buttonPrestazioni.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonPrestazioni);
        this.buttonIngombro2Img.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonIngombro2Img);
        this.buttonDimensioniAngolo.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonDimensioniAngolo);
        this.buttonDimensioniNoAngolo.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonDimensioniNoAngolo);
        this.horizontalLayoutDatiTecnici.setComponentAlignment(this.buttonDimensioniNoAngolo, Alignment.MIDDLE_CENTER);
        this.buttonAccessori.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonAccessori);
        this.buttonMultigiri.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonMultigiri);
        this.buttonSpettroSonoro.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonSpettroSonoro);
        this.buttonAddPreventivo.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonAddPreventivo);
        this.buttonCoral.setSizeUndefined();
        this.horizontalLayoutDatiTecnici.addComponent(this.buttonCoral);
//		this.pannelloLingueMobileGosht.setSizeUndefined();
//		this.horizontalLayoutDatiTecnici.addComponent(this.pannelloLingueMobileGosht);
        final CustomComponent horizontalLayoutDatiTecnici_spacer = new CustomComponent();
        horizontalLayoutDatiTecnici_spacer.setSizeFull();
        this.horizontalLayoutDatiTecnici.addComponent(horizontalLayoutDatiTecnici_spacer);
        this.horizontalLayoutDatiTecnici.setExpandRatio(horizontalLayoutDatiTecnici_spacer, 1.0F);
        this.gridLayout.setColumns(1);
        this.gridLayout.setRows(1);
        this.imageSfondo.setSizeFull();
        this.gridLayout.addComponent(this.imageSfondo, 0, 0);
        this.gridLayout.setComponentAlignment(this.imageSfondo, Alignment.MIDDLE_CENTER);
        this.gridLayout.setColumnExpandRatio(0, 10.0F);
        this.gridLayout.setRowExpandRatio(0, 10.0F);
        this.buttonDataSheet.setSizeUndefined();
        this.horizontalLayoutFooter.addComponent(this.buttonDataSheet);
        this.horizontalLayoutFooter.setComponentAlignment(this.buttonDataSheet, Alignment.MIDDLE_CENTER);
        this.buttonDownloadDataSheet.setSizeUndefined();
        this.horizontalLayoutFooter.addComponent(this.buttonDownloadDataSheet);
        this.horizontalLayoutFooter.setComponentAlignment(this.buttonDownloadDataSheet, Alignment.MIDDLE_CENTER);
        final CustomComponent horizontalLayoutFooter_spacer = new CustomComponent();
        horizontalLayoutFooter_spacer.setSizeFull();
        this.horizontalLayoutFooter.addComponent(horizontalLayoutFooter_spacer);
        this.horizontalLayoutFooter.setExpandRatio(horizontalLayoutFooter_spacer, 1.0F);
        this.menuBar.setWidth(100, Unit.PERCENTAGE);
        this.menuBar.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.menuBar);
        this.verticalLayout.setComponentAlignment(this.menuBar, Alignment.MIDDLE_CENTER);
        this.horizontalLayoutComandi.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayoutComandi.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.horizontalLayoutComandi);
        this.horizontalLayoutDatiTecnici.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayoutDatiTecnici.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.horizontalLayoutDatiTecnici);
        this.gridLayout.setSizeFull();
        this.verticalLayout.addComponent(this.gridLayout);
        this.verticalLayout.setExpandRatio(this.gridLayout, 10.0F);
        this.horizontalLayoutFooter.setWidth(100, Unit.PERCENTAGE);
        this.horizontalLayoutFooter.setHeight(-1, Unit.PIXELS);
        this.verticalLayout.addComponent(this.horizontalLayoutFooter);
        this.verticalLayout.setComponentAlignment(this.horizontalLayoutFooter, Alignment.MIDDLE_CENTER);
        this.verticalLayout.setSizeFull();
        this.setContent(this.verticalLayout);
        this.setSizeFull();

        comboLingua.addValueChangeListener((final Property.ValueChangeEvent event) -> {
            comboLingua_valueChange(event);
        });
        this.buttonCatalogo.addClickListener(event -> this.buttonCatalogo_buttonClick(event));
        this.buttonRicerca.addClickListener(event -> this.buttonRicerca_buttonClick(event));
        this.buttonPreventivi.addClickListener(event -> this.buttonPreventivi_buttonClick(event));
        this.buttonConfronto.addClickListener(event -> this.buttonConfronto_buttonClick(event));
        this.buttonLauout.addClickListener(event -> this.buttonLauout_buttonClick(event));
        this.buttonSetup.addClickListener(event -> this.buttonSetup_buttonClick(event));
        this.buttonLogout.addClickListener(event -> this.buttonLogout_buttonClick(event));
        this.buttonDebug.addClickListener(event -> this.buttonDebug_buttonClick(event));
        this.buttonRiepilogo.addClickListener(event -> this.buttonRiepilogo_buttonClick(event));
        this.buttonPrestazioni.addClickListener(event -> this.buttonPrestazioni_buttonClick(event));
        this.buttonIngombro2Img.addClickListener(event -> this.buttonIngombro2Img_buttonClick(event));
        this.buttonDimensioniAngolo.addClickListener(event -> this.buttonDimensioniAngolo_buttonClick(event));
        this.buttonDimensioniNoAngolo.addClickListener(event -> this.buttonDimensioniNoAngolo_buttonClick(event));
        this.buttonAccessori.addClickListener(event -> this.buttonAccessori_buttonClick(event));
        this.buttonMultigiri.addClickListener(event -> this.buttonMultigiri_buttonClick(event));
        this.buttonAddPreventivo.addClickListener(event -> this.buttonAddPreventivo_buttonClick(event));
        this.buttonCoral.addClickListener(event -> this.buttonCoral_buttonClick(event));
        this.buttonSpettroSonoro.addClickListener(event -> this.buttonSpettroSonoro_buttonClick(event));
        this.buttonDataSheet.addClickListener(event -> this.buttonDataSheet_buttonClick(event));
        this.buttonDownloadDataSheet.addClickListener(event -> this.buttonDownloadDataSheet_buttonClick(event));
    } // </generated-code>

    // <generated-code name="variables">
    private XdevButton buttonCatalogo, buttonRicerca, buttonPreventivi, buttonConfronto, buttonLauout, buttonSetup,
            buttonLogout, buttonDebug, buttonItalianoGosth1, buttonRiepilogo, buttonPrestazioni, buttonIngombro2Img,
            buttonDimensioniAngolo, buttonDimensioniNoAngolo, buttonAccessori, buttonCoral, buttonMultigiri, buttonSpettroSonoro,
            buttonItalianoGosth2, buttonDataSheet, buttonDownloadDataSheet, buttonAddPreventivo;
    private XdevLabel labelMemo, labelVersion, labelRiempitivo;
    private XdevMenuBar menuBar;
    private XdevHorizontalLayout horizontalLayoutComandi, horizontalLayoutDatiTecnici, horizontalLayoutFooter;
    private XdevImage imageSfondo;
    private XdevMenuItem itemFileGroup, itemLogout, itemSetup, itemCatalogo, itemRicerca, itemPreventivi,
            itemConfronto, itemDatiTecniciGroup, itemPrestazioni, itemDimensioniAngolo, itemAccessori, itemMultigiri,
            itemRumore, itemDimensioni, itemLinguaGroup, itemItaliano, itemInglese;
    private XdevPanel pannelloLingueGosht, pannelloLingueMobileGosht;
    private XdevGridLayout gridLayout;
    private XdevVerticalLayout verticalLayout;
    private XdevComboBox<CustomComponent> comboLingua;
    // </generated-code>

}
