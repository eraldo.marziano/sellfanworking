package com.cit.sellfan.ui.template;

import java.io.InputStream;

import com.vaadin.server.StreamResource.StreamSource;

import cit.myjavalib.jDisplayImage;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;


public class jDisplayImageWeb extends jDisplayImage implements StreamSource {
	public InputStream getStream () {
		return super.getInputStream();
	}
    public void addDisclaimer(InputStream disclaimer) {
        BufferedImage disc = null;
        if (disclaimer != null) {
                try {
                    disc = ImageIO.read(disclaimer);
                } catch (Exception e2) {
                    disc = null;
                }
            }
        Graphics g = getImage().getGraphics();
            if (disc != null) {
                g.drawImage(disc, getImageWith()-disc.getWidth(null), getImageHeight()/2-disc.getHeight(null)/2, disc.getWidth(null), disc.getHeight(null), null);
            }
    }
}


