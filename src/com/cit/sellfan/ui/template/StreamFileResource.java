package com.cit.sellfan.ui.template;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.vaadin.server.StreamResource.StreamSource;

public class StreamFileResource extends File implements StreamSource {
	@SuppressWarnings("unused")
	private String filename;
	
	public StreamFileResource(final String filename) {
		super(filename);
	}
	
	@Override
	public InputStream getStream () {
		try {
			return new FileInputStream(this);
		} catch (final Exception e) {
			return null;
		}
	}

}
